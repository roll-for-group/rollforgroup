#!/bin/bash

mkdir app/public/docs

FILES=*.md
#for f in $FILES
for f in docs-site/$FILES
do
  # extension="${f##*.}"
  filename="${f%.*}"
  echo "Converting $f to $filename.html"
  `pandoc -f markdown $f -t html -o $filename.html -B docs-site/bodyheader.html  -H docs-site/header.html -A docs-site/footer.html --css ../css/support.css?v=2`
  cp -a docs-site/*.html app/public/docs
  rm $filename.html
  # uncomment this line to delete the source file.
  # rm $f
done
