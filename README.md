[![pipeline status](https://gitlab.com/jaied/rollforgroup/badges/master/pipeline.svg)](https://gitlab.com/jaied/rollforgroup/commits/master) [![coverage report](https://gitlab.com/jaied/rollforgroup/badges/master/coverage.svg)](https://gitlab.com/jaied/rollforgroup/commits/master)



The RollforGroup webpage is accessible through www.rollforgroup.com

Roll for Group's vision is a united and connected world. We see board games as a way for different people from different backgrounds to come together, sit together, get to know each other, and experience each other's company.


# Setup & Installation

To setup a local copy of the Roll for Group website.



## Required Software 

Install npm, node and docker on your computer.
* https://www.npmjs.com/get-npm
* https://nodejs.org/en/
* https://www.docker.com



## Useful Software

The following software may prove useful. You are welcome to use alternatives, these are just some suggestions.

- [Robo 3T](https://robomongo.org/download) - a GUI for connecting to Mongo DB
- [Typora](https://typora.io/) - a minimal, cross-platform markdown editor



## Downloading files
Git clone this repository `git clone git@gitlab.com:jaied/rollforgroup.git`.
Change to the rollforgroup folder `cd rollforgroup` and install the dependencies with `npm install`.

Note: some dependencies include native modules which need to be built on your system using a C compiler. On OSX the simplest way to get one is with `xcode-select --install` if you don't already have one available.



## Setup Mongo Database

Run `./start_mongodb.sh` which will install the appropriate mongodb database in a docker file.

Note: You need to be logged into Docker Hub for this to work the first time, as it needs to download the Mongo image!

If the need ever comes up for you to clear the Mongo database, run `./stop_mongodb.sh` which will stop and remove the MongoDB docker containers.




## Install Config File 
Run `./install_config.sh` to install a default config file.

To run the services `node_modules/nodemon/bin/nodemon.js emailer/app/app.js` and `node_modules/nodemon/bin/nodemon.js collections/app/app.js`.
To run the main program `node_modules/nodemon/bin/nodemon.js app/server.js`.

Note if you install nodemon globally (`npm install -g nodemon`) you can use `nodemon path/to/file` instead of `node_modules/nodemon/bin/nodemon.js path/to/file`.



## Creating an Account 
Open the browser to `http://localhost:3000` and you should be able to see the front page of the Roll for Group website. Create a new account through the local web page `http://localhost:3000/signup`. 

To activate the account, open the mongodb database `accounts` table and find the record where the `email` field matches the one registered. Change the `isAuthenticated` field from `false` to `true`. You should now be able to log into the account from the `http://localhost:3000/login` login page.





# Supported Browsers

The site supports [all ES5 capable browsers](https://caniuse.com/#feat=es5) including but not limited to:

- Firefox 4+
- Safari 5+
- Chrome 6+
- Internet Explorer 9+



Please note that some pages and areas of the site are implemented with [CSS Grid](https://caniuse.com/#feat=css-grid) which has a stricter set of browsers that support it. These pages have been designed with at least some semblance of graceful fallback in older browsers. We have not set the supported browsers to a more strict subset as all functionality should continue to work, just layout may be slightly less nice on unsupported browsers.



# Helping Out

If have an interest in assisting us build a better way to link board game players please check out [our contributing guide](CONTRIBUTING.md).

Anything from reporting bugs, to fixing bugs, to joining the project as a regular contributor are welcome!
