#!/bin/bash

# This script starts the mongo db in a docker container
# and exposes teh default database port

docker run -p 32768:27017 --name RFG-mongo -d mongo
docker run -p 32767:27017 --name RFG-mongo-emails -d mongo
docker run -p 32766:27017 --name RFG-collections -d mongo
