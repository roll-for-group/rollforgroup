var R     = require('ramda');
var util  = require('util');

var Promise = require('bluebird');
var faker   = require('Faker');
var log   = require('../app/log.js');

var nconf = require('nconf');
nconf.argv().env().file({file: __dirname + '/config.json'});

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var testdata = {
  email: 'play@rollforgroup.com',
  campaign: 'welcome',
  mailplan: [
    {
      template: '001-welcome',
      hours: 0
    }
  ],
  userdata: {
    name: 'Jaie Demaagd',
    url: 'https://www.rollforgroup.com',
    email: 'play@uprightgaming.com'
  }
};



