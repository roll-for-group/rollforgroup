var express = require("express");
var router = express.Router();

var R     = require('ramda');
var util  = require('util');
var log   = require('../app/log.js');
var moment = require('moment');

var Promise = require('bluebird');
var faker   = require('Faker');
var emailer = require('../app/emailer.js');
var email_model = require('../app/email_model.js');
var ajax = require('./ajax.js');

var nconf = require('nconf');
nconf.argv().env().file({file: __dirname + '/config.json'});

var nodemailer  = require('nodemailer');
var stubTransport = require('nodemailer-stub-transport');
var transporter = nodemailer.createTransport(stubTransport());

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

var testapp = express();

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

// ============== AJAX ===============
var ajaxPromise = ajax.AjaxPromise;

// ============== VARS ===============
var url_base = "http://localhost:30204";
var url_api = '/api/v1/emailer';

var server;
const readStatus = R.prop('statusCode');

mongoose.connect(nconf.get('service:emailer:database:uri'));

describe("email", function() {
	describe("http server", function() {

    it("setup server", function(done) {
      // mongo.ConnectDatabase(dbUri);
      router = emailer.setupRouter(testapp);

      testapp.use(url_api, router);
      server = testapp.listen(30204, function() {
        done();
      });
    });

    it("test post email via http", function(done) {
      var requestData = [
        {
          url: url_base + url_api + "/campX",
          method: 'POST',
          data: {
						email	: 'abc@def.com',
						userdata: {
							name		: 'jack',
							company	: 'compx'
						},
						mailplan: [{
							template: 'tempx',
							hours		: 4 
						}]
					}
        }
      ];

      Promise.all(R.map(ajaxPromise, requestData)).then(function(x) {
        expect(x[0].body.email).toBe('abc@def.com');
        expect(x[0].body.campaign).toBe('campX');
        expect(x[0].body.userdata.company).toBe('compx');
        expect(x[0].body.userdata.name).toBe('jack');
				expect(readStatus(x[0])).toBe(200);
        done();
      });

    });

    it("test patch mailplan via http", function(done) {
      var requestData = [
        {
          url: url_base + url_api + "/campX",
          method: 'PATCH',
          data: {
						mailplan: [{
							template: 'temp3',
							hours		: 3 
						}]
					}
        }
      ];

      Promise.all(R.map(ajaxPromise, requestData)).then(function(x) {
        expect(x[0].body.ok).toBe(1);
        expect(x[0].body.n).toBe(x[0].body.nModified);
        expect(x[0].body.n).toBeGreaterThan(0);
				expect(readStatus(x[0])).toBe(200);
        done();
      });
    });

    it("stop emailing to user", function(done) {
      var requestData = [
        {
          url: url_base + url_api,
          method: 'PATCH',
          data: {
						email: 'abc@def.com',
						status: false
					}
        }
      ];

      Promise.all(R.map(ajaxPromise, requestData)).then(function(x) {
        expect(x[0].body.ok).toBe(1);
        expect(x[0].body.n).toBe(x[0].body.nModified);
        expect(x[0].body.n).toBeGreaterThan(0);
				expect(readStatus(x[0])).toBe(200);
        done();
      });
    });

	});


	describe("send mail functions", function() {
    it("SMTPSettings", function(done) {
      expect(emailer.SMTPSettings(nconf).host).toBe(nconf.get('service:emailer:smtp:host')); 
      expect(emailer.SMTPSettings(nconf).port).toBe(nconf.get('service:emailer:smtp:port')); 
      expect(emailer.SMTPSettings(nconf).auth.user).toBe(nconf.get('service:emailer:smtp:user'));
      expect(emailer.SMTPSettings(nconf).auth.pass).toBe(nconf.get('service:emailer:smtp:pass'));
      done();
    })

		it("sendWithSMTP", function(done) {
			emailer.SendWithSMTP(transporter, {from: "from@mail.com", to: "to@mail.com", subject: "subject", html: "message"}).then(function (respond) {
				expect(respond.envelope.from).toBe('from@mail.com');
				expect(respond.envelope.to[0]).toBe('to@mail.com');
				done();
			});
		});

		it("sendSMTPmail", function(done) {
			emailer.SendSMTPmail(transporter, "from@mail.com", "to@mail.com", "subject", "message", 'html').then(function (respond) {
				expect(respond.envelope.from).toBe('from@mail.com');
				expect(respond.envelope.to[0]).toBe('to@mail.com');
				done();
			});
		});

    it("from", function(done) {
      expect(emailer.From('a')).toBe('Roll for Group <a>');
      expect(emailer.From('z')).toBe('Roll for Group <z>');
      done();
    });

    var directory1 = __dirname + '/templates/email_test_1'
    var directory2 = __dirname + '/templates/email_test_2'

    it("readFile", function(done) {
      R.sequence(Task.of, [emailer.readFile(directory1 + '/email.html'), emailer.readFile(directory2 + '/email.html')]).fork(
        err => log.clos('err', err),
        data => {
          expect(data[0]).toBe('<p>This is the email - test 1</p>\n');
          expect(data[1]).toBe('<p>This is the email - test 2</p>\n<style>p{color:red;}</style>\n');
          done();
        }
      );
    });

    it("renderHTML", function(done) {
      var directory1 = __dirname + '/templates/template_test_1';
      var data1 = {
        name: 'Henry',
        boardgame: 'Ford'
      };
      var data2 = {
        name: 'Cat',
        boardgame: 'Stevens'
      };

      emailer.compileEmailTemplate(directory1).fork(
        err => log.clos('err', err),
        fx  => {
          expect(fx(data1)).toBe('<p>Hello Henry, I hope you have fun playing Ford</p>\n');
          expect(fx(data2)).toBe('<p>Hello Cat, I hope you have fun playing Stevens</p>\n');
          done();
        }
      );

    });

    it("readEmailTemplate", function(done) {

      var directory1 = __dirname + '/templates/template_test_1';
      var directory2 = __dirname + '/templates/template_test_2';
      var namedata = [{
        name: 'Henry',
        boardgame: 'Ford'
      }, {
        name: 'Cat',
        boardgame: 'Stevens'
      }];

      emailer.readEmailTemplate(directory1, namedata).fork(
        err   => log.clos('err', err),
        data  => {
          expect(data[0]).toBe('<p>Hello Henry, I hope you have fun playing Ford</p>\n');
          expect(data[1]).toBe('<p>Hello Cat, I hope you have fun playing Stevens</p>\n');
          emailer.readEmailTemplate(directory2, namedata).fork(
            err   => log.clos('err', err),
            newdata  => {
              expect(newdata[0]).toBe('<p style="color: red;">Hello Henry, I hope you have fun playing Ford</p>\n\n');
              expect(newdata[1]).toBe('<p style="color: red;">Hello Cat, I hope you have fun playing Stevens</p>\n\n');
              done();
            }
          );

        }
      );

    });

    it("compileEmailSubjectTemplate", function(done) {

      var directory1 = __dirname + '/templates/template_test_1';
      var namedata1 = {
        name: 'Henry',
        boardgame: 'Ford'
      };
      var namedata2 = {
        name: 'Cat',
        boardgame: 'Stevens'
      };

      emailer.compileEmailSubjectTemplate(directory1).fork(
        err => log.clos('err', err),
        fx  => {
          expect(fx(namedata1)).toBe('Hi Henry! Welcome to Roll for group\n');
          expect(fx(namedata2)).toBe('Hi Cat! Welcome to Roll for group\n');
          done();
        }
      );

    });

    it("compileEmailTextTemplate", function(done) {

      var directory1 = __dirname + '/templates/template_test_1';
      var namedata1 = {
        name: 'Henry',
        boardgame: 'Ford'
      };
      var namedata2 = {
        name: 'Cat',
        boardgame: 'Stevens'
      };

      emailer.compileEmailTextTemplate(directory1).fork(
        err => log.clos('err', err),
        fx  => {
          expect(fx(namedata1)).toBe('Hello Henry, I hope you have fun playing Ford\n');
          expect(fx(namedata2)).toBe('Hello Cat, I hope you have fun playing Stevens\n');
          done();
        }
      );

    });

    it("buildEmailSendData", function(done) {

      var from = 'abc@xyz.com';
      var directory1 = __dirname + '/templates/template_test_1';
      var directory2 = __dirname + '/templates/template_test_2';
      var namedata1 = {
        email: 'henry.ford@uprightgaming.com',
        name: 'Henry',
        boardgame: 'Ford',
      };
      var namedata2 = {
        email: 'cat.stevens@uprightgaming.com',
        name: 'Cat',
        boardgame: 'Stevens'
      };

      R.sequence(Task.of, [emailer.buildEmailSendData(from, directory2, namedata1), emailer.buildEmailSendData(from, directory2, namedata2)]).fork(
        err => log.clos('err', err),
        newdata => {
          expect(newdata[0]).toEqual({
            to: 'henry.ford@uprightgaming.com',
            from: 'abc@xyz.com',
            subject: 'Hi Henry! Welcome to Roll for Group\n',
            text: 'Hello Henry, I hope you have fun playing Ford\n',
            html: '<p style="color: red;">Hello Henry, I hope you have fun playing Ford</p>\n\n'
          });
          expect(newdata[1]).toEqual({
            to: 'cat.stevens@uprightgaming.com',
            from: 'abc@xyz.com',
            subject: 'Hi Cat! Welcome to Roll for Group\n',
            text: 'Hello Cat, I hope you have fun playing Stevens\n',
            html: '<p style="color: red;">Hello Cat, I hope you have fun playing Stevens</p>\n\n'
          });
          done();
        }
      );
    });

    var campaign = 'mycampagin';

    it('prepare', function(done) {
      email_model.remove({})
        .exec()
        .then((x) => {
          done();
        })
    })

    it('addUserToCampaign', function(done) {

      var email1 = faker.Internet.email();
      var email2 = faker.Internet.email();
      var email3 = faker.Internet.email();

      var mailplan = [{
        template: 'a1',
        hours: -1 
      }, {
        template: 'a2',
        hours: 1
      }];

      var userdata = {userdata:'user'};

      R.sequence(Task.of, [
        emailer.addUserToCampaign(campaign, mailplan, userdata, email1),
        emailer.addUserToCampaign(campaign, mailplan, userdata, email2),
        emailer.addUserToCampaign(campaign, mailplan, userdata, email3)
      ]).fork(
        err => log.clos('err', err),
        data => {

          expect(data[0].email).toBe(email1);
          expect(data[1].email).toBe(email2);
          expect(data[2].email).toBe(email3);

          expect(data[0].campaign).toBe(campaign);
          expect(data[1].campaign).toBe(campaign);
          expect(data[2].campaign).toBe(campaign);

          expect(data[0].userdata).toEqual(userdata);
          expect(data[1].userdata).toEqual(userdata);
          expect(data[2].userdata).toEqual(userdata);

          expect(data[0].complete).toBe(false);
          expect(data[0].complete).toBe(false);

          expect(data[0].mailplan[0].template).toBe('a1');
          expect(data[0].mailplan[1].template).toBe('a2');

          expect(data[0].mailplan[0].hours).toBe(-1);
          expect(data[0].mailplan[1].hours).toBe(1);

          expect(data[0].mailplan[0].status).toBe('waiting');
          expect(data[0].mailplan[1].status).toBe('waiting');

          expect(moment(data[0].sendNextAt).diff(new Date())).toBeLessThan(-3600000);
          expect(moment(data[0].sendNextAt).diff(new Date())).toBeGreaterThan(-3601000);

          done();

        }
      );
    });


    it('appendMailplanToCampaign', function(done) {
      var plan = {
        template: 'a3',
        hours: 2
      };
      emailer.appendMailplanToCampaign(campaign, plan).fork(
        err => log.clos('append.err', err),
        data => {
          expect(data.n).toBe(3);
          expect(data.nModified).toBe(3);
          expect(data.ok).toBe(1);

          email_model.findOne({campaign:campaign})
            .exec()
            .then((x) => {
              expect(x.mailplan[2].template).toBe('a3');
              expect(x.mailplan[2].hours).toBe(2);
              expect(x.mailplan[2].status).toBe('waiting');
              done();
            }).catch((err) => log.clos('catch.err', err));
        }
      )
    });

    it('updateUserEmailPermission', function(done) {

      var email1 = faker.Internet.email();
      var mailplan = [{
        template: 'a1',
        hours: 0
      }, {
        template: 'a2',
        hours: 1
      }];
      var userdata = {user: 'data'};

      emailer.addUserToCampaign(campaign, mailplan, userdata, email1).fork(
        err => log.clos('updateUserEmailPermission.add.err', err),
        data => {
          emailer.updateUserEmailPermission(email1, false).fork(
            err => log.clos('updateUserEmailPermission.update.err', err),
            x => {
              expect(x.n).toBe(1);
              expect(x.nModified).toBe(1);
              expect(x.ok).toBe(1);
              done();
            }
          )
        }
      );

    });

    it('getCampaignUsersEmailQueue', function(done) {
      emailer.getCampaignUsersEmailQueue(10).fork(
        err => log.clos('getCampaignUsersEmailQueue.err', err),
        x   => {
          expect(x.length).toBe(3);
          expect(x[0].sendNextAt).toBeLessThan(new Date());
          done();
        }
      )
    });

    it('getNewDate', function(done) {
      expect(emailer.getNewDate({}).isNothing).toBeTruthy();
      expect(emailer.getNewDate({mailplan:[]}).isNothing).toBeTruthy();
      expect(emailer.getNewDate({mailplan:[{status:'waiting'}]}).isNothing).toBeTruthy();
      expect(emailer.getNewDate({mailplan:[{status:'waiting'}, {status:'waiting'}]}).isNothing).toBeTruthy();
      expect(emailer.getNewDate({mailplan:[{status:'waiting'}, {status:'waiting', hours:1}]}).isJust).toBeTruthy();
      done();
    });

    it('findTemplate', function(done) {
      expect(emailer.findTemplate({}).isNothing).toBeTruthy();
      expect(emailer.findTemplate({mailplan: []}).isNothing).toBeTruthy();
      expect(emailer.findTemplate({mailplan: [{status: 'waiting'}, {status:'sent'}]}).isNothing).toBeTruthy();
      expect(emailer.findTemplate({mailplan: [{status: 'waiting', template: 'abc'}, {status: 'waiting'}, {status:'sent'}]}).get()).toBe('abc');
      done();
    });

    it('sendCampaignEmailToUser', function(done) {

      var email1    = faker.Internet.email();
      var from      = 'abc@xyz.com';
      var campaign  = 'sendCampaignEmailToUser'; 
      var userdata  = {userdata:'user'};
      var mailplan  = [{
        template: 'sendCampaignEmailToUser-01',
        hours: -1 
      }, {
        template: 'sendCampaignEmailToUser-02',
        hours: 1 
      }];

      emailer.addUserToCampaign(campaign, mailplan, userdata, email1).fork(
        err => log.clos('updateUserEmailPermission.add.err', err),
        data => {
          emailer.sendCampaignEmailToUser(transporter, from, __dirname + '/templates/', campaign, email1).fork(
            err   => log.clos('getCampaignUsersEmailQueue.err', err),
            data  => {
              expect(moment().diff(data.sendNextAt)).toBeLessThan(3602000);
              expect(data.mailplan[1].status).toBe('waiting');
              expect(data.email).toBe(email1);
              done();
            }
          )  
        }
      )

    });

    it("close server", function(done) {
			server.close(function() {
				mongoose.connection.close(function() {
					console.log('Mongoose Closed')
					done();
				});
      });
    });

  });
});
