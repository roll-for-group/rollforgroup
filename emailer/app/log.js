var exports = module.exports = {};
var util = require('util');
var R = require('ramda');

/* *********************************
 *
 * consoleLogObject sends an object to the console for debugging
 * and returns the object so that it can be inserted into Promise chains
 * with ease
 *
 */

var consoleLogObjectSection = R.curry(function(section, object) {
    console.log("======================= " + section + "_START =================");
    console.log(util.inspect(object, {showHidden: false, depth: null}));
    console.log("======================= " + section + "_END =================");
    return object;
});
exports.ConsoleLogObjectSection = consoleLogObjectSection;
exports.clos = consoleLogObjectSection;

var consoleLogObject = consoleLogObjectSection('GENERIC');
exports.ConsoleLogObject = consoleLogObject;
