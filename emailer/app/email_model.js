var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EmailChain = new Schema({
  email       : {type: String, index: 'text'},
  campaign    : {type: String, index: 'text'},
  mailplan    : [{
    template    : String,
    hours       : Number,
    status      : {
      type: String,
      enum: [
        'waiting', 'sent'
      ]
    }
  }],
  sendNextAt    : Date,
  hasPermission : Boolean,
  complete      : Boolean,
  userdata      : Schema.Types.Mixed,
  sendlog : [{
    template    : String,
    date        : Date,
  }]
},
{
  timestamps: true
});

module.exports = mongoose.model('EmailChain', EmailChain);
