# EmailChain Purpose

## General Concept
An email chain is a series of emails to be sent to a user in succession, often over a series of days for a campaign.

Create an object to begin an email chain, as they are sent the sendlog collection logs what was sent when.

## Attribute Breakdown and intention
### Email
The email address to send emails to 

### Campaign
A unique name of the campaign 

### Mailplan 
The plan of how the emails will be sent consisting of the following
> template filename of the template to us
> hours     - the number of hours to wait until sending the next email in sequence 
> sequence  - the stage the campaign is up to
> status    - waiting, ready, sent

### sendNextAt
When the next email should be sent, used for querying times greater than now

### haspermission
Whether the person still has permission to send the mail, set to false if the user denies permission to receive emails

### complete 
If the mailplan sequence is completed, stops the system from continually returning this entry in a query. When appending a new step to an existing plan, st back to false. 

### userdata  
Data to integrate into the template, such as the users name

### sendlog
A history of email sent
> template  - name of the template sent
> date      - when the template was emailed 
