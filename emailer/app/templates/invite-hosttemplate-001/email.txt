Hi {{hostname}},
You're hosting a board game event on Roll for Group. This is a template email for your event.
Simply delete this top section, and then forward this email to friends to invite them.



Hi,

You're invited to {{eventname}}. Come play board games at {{eventlocation}} on {{eventtime}}.
If you'd like to join, please click on the button below to accept or decline the event invitation on the Roll for Group web site.</p>

<a href={{url}}>Join Event</a>

Once signed up, we can vote on the board games each player wants to bring and play.

Kind Regards,
{{hostname}}
