Hi {{name}},

Welcome to the table! We're excited to have you join Roll for Group, a growing community for board gamers. 

Roll for Group was created to help board gamers meet other players, make new friends, and get your board games played. 

Thank you for joining, we need both experienced and new players to we can attain our goals. Each member is another oppurtunity for others to play board games.

Let's get you started

Roll for Group log in page: {{url}}

Your email to log in: {{email}}

Good luck! We'll be in touch.

Roll for Group
PO Box 465, Unanderra NSW 2526, Australia

You received this email because you opted to recieve event notification through Roll for Group. If you no longer wish to receive these emails turn off "Site Notifications" in your profile.
