const Promise = require('bluebird');
const R = require('ramda');
const log = require('./log.js');

const moment = require('moment-timezone');
const emailer = require('./emailer.js');
const S = require('./lambda.js');

const nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

// setup sending email
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport(emailer.SMTPSettings(nconf));

var from = nconf.get('service:emailer:smtp:from');

// setup webpages
var app = require('express')();
var http = require('http').Server(app);

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

const schedule  = require('node-schedule'); 

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// sendEmails ::  
const sendEmails = function() {

  emailer.getCampaignUsersEmailQueue(10).fork(
    err => log.clos('err', err),
    data => {

      // sendTemplate :: o -> Task o
      const sendTemplate = (dataTemp) => 
        R.lift(emailer.sendCampaignEmailToUser(transporter, from, nconf.get('service:emailer:templatepath')))(
          S.prop('campaign', dataTemp), 
          S.prop('email', dataTemp)
        ).getOrElse(taskFail())

			R.sequence(Task.of, data.map(sendTemplate)).fork(err=>{}, data=>{});

    }
  );
};


// ============== START SERVER ===============
var httpPort = nconf.get('service:emailer:http:port');
var emailerRouter = emailer.setupRouter(app);
app.use('/api/v1/emailer', emailerRouter);

http.listen(httpPort, function(){
  console.log('listening on *:' + httpPort);
});

mongoose.connect(
	nconf.get('service:emailer:database:uri'), {
		userMongoClient:true
	}
);

// schedule job
schedule.scheduleJob('0 * * * * *', function() {
  sendEmails();
});

// clean up end program
process.on('SIGTERM', function () {
  server.close(function () {
		mongoose.connection.close(function() {
			process.exit(0);
		});
  });
});
