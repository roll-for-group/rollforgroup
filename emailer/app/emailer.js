var Promise = require('bluebird');
var R = require('ramda');
var chain   = R.chain;
var compose = R.compose;
var curry   = R.curry;
var find    = R.find;
var filter  = R.filter;
var findLast = R.findLast;
var head    = R.head;
var isEmpty = R.isEmpty;
var isNil   = R.isNil;
var init    = R.init;
var join    = R.join;
var last    = R.last;
var length  = R.length;
var lift    = R.lift;
var not     = R.not;
var map     = R.map;
var path    = R.path;
var prop    = R.prop;
var sequence = R.sequence;
var zipObj  = R.zipObj;

var util = require('util');
var log = require('./log.js');
var S = require('./lambda.js');

var email_model = require('./email_model.js');

var moment = require('moment-timezone');
var handlebars = require('handlebars');
var juice = require('juice');

var express     = require('express');
var bodyParser  = require('body-parser');
var jsonParser  = bodyParser.json();

var smtpSettings = function(nconfig) { 
  return {
    host: nconfig.get("service:emailer:smtp:host"),
    port: nconfig.get("service:emailer:smtp:port"),
    secure:true,
    auth: {
      user: nconfig.get("service:emailer:smtp:user"),
      pass: nconfig.get("service:emailer:smtp:pass")
    },
    authMethod: "PLAIN"
  };
};
exports.SMTPSettings = smtpSettings;

// taskFail :: Task s
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var fs = require('fs');

// from :: u -> e
const from = (siteuser) => 'Roll for Group <' + siteuser + '>';
exports.From = from;

var cacheEmailTempalates = [];

// readFile :: s -> Task s
const readFile = function(filename) {
  return new Task(function(reject, resolve) {
    fs.readFile(filename, 'utf8', function (err, data) {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};
exports.readFile = readFile;

// compileEmailSubjectTemplate :: s -> task (s => s)
const compileEmailSubjectTemplate = function(filename) {
  return new Task((reject, resolve) => readFile(filename + '/subject.txt').fork(
    reject, template => resolve(handlebars.compile(template))
  ));
};
exports.compileEmailSubjectTemplate = compileEmailSubjectTemplate;

// compileEmailTextTemplate :: s -> task (s => s)
const compileEmailTextTemplate = function(filename) {
  return new Task((reject, resolve) => readFile(filename + '/email.txt').fork(
    reject, template => resolve(handlebars.compile(template))
  ));
};
exports.compileEmailTextTemplate = compileEmailTextTemplate;

// compileEmailTemplate :: s -> task (s => s)
const compileEmailTemplate = function(filename) {
  return new Task((reject, resolve) => readFile(filename + '/email.html').fork(
    reject, data => resolve(handlebars.compile(data))
  ));
};
exports.compileEmailTemplate = compileEmailTemplate;


// readEmailTempalte :: s -> [o] -> Task [s]
const readEmailTemplate = function(fileName, userObjs) {
  return new Task((reject, resolve) => compileEmailTemplate(fileName).fork(
    reject, fx => {
      resolve(userObjs.map(fx).map(juice));
    }
  ))
};
exports.readEmailTemplate = readEmailTemplate;


// buildEmailSendData :: s -> s -> o - Task o
const buildEmailSendData = curry(function(from, fileName, userObj) {

  const buildSendData = R.curry((to, from, subject, html, text) => ({
    to: to,
    from: from,
    subject: subject,
    html: html,
    text: text,
  }));

  return new Task((reject, resolve) => 
    R.sequence(Task.of, [
      compileEmailSubjectTemplate(fileName),
      compileEmailTemplate(fileName),
      compileEmailTextTemplate(fileName),
    ]).fork(
      reject, fx => {
        const buildEmail = (userObj) => buildSendData(
          S.prop('email', userObj).getOrElse('gaming@rollforgroup.com'), 
          from,
          fx[0](userObj), 
          juice(fx[1](userObj)), 
          fx[2](userObj)
        );
        resolve(buildEmail(userObj));

      }
    )
  )
});
exports.buildEmailSendData = buildEmailSendData;

// sendWithSMTP :: Transport -> Object -> (Promise -> info)
var sendWithSMTP = R.curry(function(MailTransport, mailOptions) {
	return MailTransport.sendMail(mailOptions);
});
exports.SendWithSMTP = sendWithSMTP;

// sendSMTPmail :: Transport -> String -> String -> String -> String -> (Promise -> info)
var sendSMTPmail = R.curry(function(MailTransport, from, to, subject, text, html) {
	var mailOptions = {
		from: from,
		to: to,
		subject: subject,
    text: text,
		html: html 
	};
  return sendWithSMTP(MailTransport, mailOptions);
});
exports.SendSMTPmail = sendSMTPmail;

// readify :: o -> o
const readify = R.merge({status:'waiting'});


// addUserToCampaign :: s -> [o] -> o -> s -> Task email_model 
const addUserToCampaign = curry((campaign, mailplan, userdata, email) => 
  new Task((reject, resolve) => {

    // getNextHours :: [o] -> Maybe num
    const getNextHours = R.compose(R.chain(S.prop('hours')), S.head);

    var logmail = new email_model({
      email         : email, 
      campaign      : campaign,
      mailplan      : mailplan.map(readify),
      hasPermission : true,
      complete      : false,
      sendNextAt    : moment().add(getNextHours(mailplan).getOrElse(0), 'hours'),
      userdata      : userdata
    });

    logmail.save(function(err, logmail) {
      resolve(logmail.toObject());
    });

  })
); 
exports.addUserToCampaign = addUserToCampaign;


// appendMailplanToCampaign :: s -> s -> Task email_model 
const appendMailplanToCampaign = R.curry((campaign, mailplan) => 
  new Task((reject, resolve) => {
    email_model.update(
      {campaign: campaign}, 
      {$addToSet: {mailplan: readify(mailplan)}},
      {multi: true}
    )
      .exec()
      .then(resolve)
      .catch(reject) 
  })
); 
exports.appendMailplanToCampaign = appendMailplanToCampaign;


// updateUserEmailPermission :: s -> b -> Task email_model 
const updateUserEmailPermission = R.curry((email, status) => 
  new Task((reject, resolve) => {
    email_model.update(
      {email: email}, 
      {hasPermission: status},
      {multi: true}
    )
      .exec()
      .then(resolve)
      .catch(reject) 
  })
); 
exports.updateUserEmailPermission = updateUserEmailPermission;


// getCampaignUsersEmailQueue :: n -> Task o 
const getCampaignUsersEmailQueue = (num) => {

  // extractMailPlan :: email_model -> o
  const extractMailPlan = (emodel) => emodel.toObject();

  return new Task((reject, resolve) => {
    email_model.find(
      {hasPermission: true, complete: false, sendNextAt: {$lt: new Date()}}
    )
      .limit(num)
      .exec()
      .then(resolve)
      .catch(reject) 
  })

};
exports.getCampaignUsersEmailQueue = getCampaignUsersEmailQueue;


// appendHoursToNow :: n -> dt
const appendHoursToNow = (hours) => moment(new Date()).add(hours, 'hours');
exports.appendHoursToNow = appendHoursToNow;


// getNewDate :: o -> maybe d
const getNewDate = compose(
  R.map(appendHoursToNow), 
  R.chain(S.prop('hours')), 
  R.chain(S.nth(1)),
  map(R.filter(R.propEq('status', 'waiting'))), 
  S.prop('mailplan')
);
exports.getNewDate = getNewDate;


// findTemplate :: o -> maybe s
const findTemplate = compose(
  R.chain(S.prop('template')), 
  R.chain(S.head),
  map(R.filter(R.propEq('status', 'waiting'))), 
  S.prop('mailplan')
);
exports.findTemplate = findTemplate;


// sendCampaignEmailToUser :: o -> s -> s -> [o] -> o -> s -> Task o
const sendCampaignEmailToUser = R.curry((transporter, from, templatePath, campaign, email) => {

  const liftBuildEmailSendData = R.lift(buildEmailSendData(from));

  // prependTemplatePath :: s -> s
  const prependTemplatePath = (x) => templatePath + x;

  return new Task((reject, resolve) => {

    email_model.findOne({email: email, campaign: campaign, complete: false})
      .exec()
      .then((data) => {

        email_model
          .findOneAndUpdate({
						_id: data.id,
            mailplan: {
              $elemMatch: {
                template: findTemplate(data).getOrElse('')
              }
            }
          }, {
            sendNextAt: getNewDate(data).getOrElse(new Date()),
            complete:   getNewDate(data).isNothing,
            $set: {
              'mailplan.$.status': 'sent'
            },
            $push: {
              sendlog: {
                template: findTemplate(data).getOrElse(''), 
                date: new Date()
              }
            }
          })
          .exec()
          .then((updated) => {
            liftBuildEmailSendData(findTemplate(data).map(prependTemplatePath), S.prop('userdata', data))
              .getOrElse(taskFail())
              .fork(
                err => {},
                data => {
                  sendWithSMTP(transporter, data);
                  resolve(updated);
                } 
              )
          })
          .catch(reject);

      })
      .catch(reject);

  });
});
exports.sendCampaignEmailToUser = sendCampaignEmailToUser;


// setupRouter :: express 
var setupRouter = function(expressApp) {
  var router = express.Router();

	// getCampaign :: {o} -> maybe [a] 
	const getCampaign = S.path(['params', 'campaign']);
	
	// liftAddUserToCampaign :: maybe s -> maybe {o} -> maybe {o} -> maybe s -> maybe task {o}
	const liftAddUserToCampaign = R.lift(addUserToCampaign);

	// liftAppendMailplanToCampaign :: maybe s -> maybe s -> mabye task email_model 
	const liftAppendMailplanToCampaign = R.lift(appendMailplanToCampaign);

	// liftUpdateUserEmailPermission :: maybe s -> maybe b -> maybe Task email_model 
	const liftUpdateUserEmailPermission = R.lift(updateUserEmailPermission);

  router.route('/')
    .patch(jsonParser, function(req, res) {
			liftUpdateUserEmailPermission(
				S.path(['body', 'email'], req), 
				S.path(['body', 'status'], req)
			)
				.getOrElse(taskFail())
				.fork(
					err => 	{
						res.sendStatus(500);
					},
					data => {
						res.json(R.pick(['n', 'nModified', 'ok'], data))
					}
				)

		})

  router.route('/:campaign')
		.get(jsonParser, function(req, res) {
			res.json({test:'ok'});
		})
    .post(jsonParser, function(req, res) {
			liftAddUserToCampaign(
				getCampaign(req), 
				S.path(['body', 'mailplan'], req), 
				S.path(['body', 'userdata'], req),
				S.path(['body', 'email'], req)
			)
				.getOrElse(taskFail())
				.fork(
					err => 	{
						res.sendStatus(500);
					},
					data => {
						res.json(R.pick(['email', 'campaign', 'userdata'], data))
					}
				)

		})
    .patch(jsonParser, function(req, res) {
			liftAppendMailplanToCampaign(
				getCampaign(req), 
				S.path(['body', 'mailplan'], req)
			)
				.getOrElse(taskFail())
				.fork(
					err => res.sendStatus(500),
					data => {
						res.json(R.pick(['n', 'nModified', 'ok'], data))
					}
				)

		})

	return router;

}; 
exports.setupRouter = setupRouter;
