var Promise = require('bluebird');
var R = require('ramda');
var log = require('../app/log.js');

var passport = require('passport');
var genhelper = require('./genhelper.js');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var findmatch_page = require('../app/findmatch_page.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var browser2 = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');
var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');
var boardgame = require('../app/models/boardgame.js');
var boardgames = require('../app/models/boardgames.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');
var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 3091;
var login = require('connect-ensure-login');

// String -> (Promise -> String)
var promisifyZombie = zombieHelper.PromisifyZombie();
var zombieGetBody = zombieHelper.PromiseZombieVisit(browser, "body");

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);
findmatch_page.RouteGamelist(testapp, account, nconf);

var generateUser = genhelper.GenerateUser(account);
var generateHostUser = genhelper.GenerateHostUser(account);

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  testapp.close(function(){
    console.log('matches_service_spec closed on port ' + httpPort);
  });
};

// ============== RUN TESTS ================== 
describe("findmatch_page_spec.js api", function() {

    var server;

    // loginUser :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
    var verifyregister = function(account, zombieb, email, pass, userobj) {
      return new Promise(function(fulfill, reject) {

        accounts.DeleteAccount(account, email).then(function(y) {
          var register = R.composeP(
            accounts.verifyAccount(account), 
            R.prop('authToken'), 
            accounts.registerAccount(account)
          );

          register(email, pass).then(function(x) {
            account.update({email: email}, userobj).exec().then(function(z) {
              zombieb.visit(base_url + "login", function(err) {
                if (err) 
                  reject(err);

                zombieb.fill('input[name="form-username"]', email)
                zombieb.fill('input[name="form-password"]', pass)
                zombieb.pressButton('login', function() {
                  fulfill(x);
                });
              });
            });
          });
        });
      })
    };

    describe("unit tests", function() {

      it("matchlist", function(done) {
        const expectObj = { events : { location : { distance : 20000, nearby : 'e' } } };
        expect(findmatch_page.Matchlist(20000, 'e')).toEqual(expectObj);
        done();
      });

      it("buildOnLoad", function(done) {
        const expectObj = { onload : { ajaxdata : 'u' } } ;
        expect(findmatch_page.BuildOnLoad('u')).toEqual(expectObj);
        done();
      });

      it("maybeDistance", function(done) {
          const testObj = {
              user: {
                  profile: {
                      searchradius: 90
                  }
              }
          };
          expect(findmatch_page.MaybeDistance(testObj)).toEqual(90000);
          expect(findmatch_page.MaybeDistance({})).toEqual(50000);
          done();
      });

      it("createDisplayError", function(done) {
          const expectObj = {warning: 'abc'};
          expect(findmatch_page.CreateDisplayError('abc')).toEqual(expectObj);
          done();
      });

      it("checkErrorMsg", function(done) {
          const expectObj = {warning: 'abc'};
          expect(findmatch_page.CheckErrorMsg({query: {err: 'abc'}})).toEqual(expectObj);
          expect(findmatch_page.CheckErrorMsg({query: 'bad'})).toEqual({});
          done();
      });

      it("buildFaIcon", function(done) {
          const expectObj = { class : 'fa fa-u fa-fw' } ;
          expect(findmatch_page.BuildFaIcon('u')).toEqual(expectObj);
          done();
      });

      it("BuildImage", function(done) {
          const expectObj = { class : 'u', src : 's' } ;
          expect(findmatch_page.BuildImage('u', 's')).toEqual(expectObj);
          done();
      });

      it("BuildHeading", function(done) {
          const expectObj = { name : 'u', s : 'v', h4 : { faicon : 'a', text : 'f' } } ;
          expect(findmatch_page.BuildHeading('u', 's', 'a', 'f', 'v')).toEqual(expectObj);
          done();
      });

      it("BuildPage", function(done) {
          const expectObj = { url : 'u', handlebars : { heading : { name : 's', i : 'v', h4 : { faicon : 'a', text : 'f' } } } } ;
          expect(findmatch_page.BuildPage('u', 's', 'i', 'a', 'f', 'v')).toEqual(expectObj);
          done();
      });

      it("whenInDistance", function(done) {
          var indistance = findmatch_page.WhenInDistance({profile: {searchradius: 30}, distance: 20000});
          var notdistance = findmatch_page.WhenInDistance({profile: {searchradius: 10}, distance: 20000});

          expect(indistance).toBe(true);
          expect(notdistance).toBe(false);

          done();
      });

      it("lookupItem", function(done) {
          var searcharray = [{id: 34, a: 32}, {id:123, x:34}]; 
          expect(findmatch_page.LookupItem(searcharray, [123, 3])).toEqual({id: 123, x:34, count:3})
          expect(findmatch_page.LookupItem(searcharray, [34, 2])).toEqual({id: 34, a:32, count:2})
          expect(findmatch_page.LookupItem(searcharray, [35, 1])).toEqual({count:1})

          done();
      });

      it("makecount", function(done) {
          var searcharray = [{id: 34, a: 32}, {id:123, x:34}, {id:123, x:34}]; 
          expect(findmatch_page.Makecount(searcharray)(searcharray)).toEqual([ { id : 34, a : 32, count : 1 }, { id : 123, x : 34, count : 2 } ])
          done();
      });

      it("buildGameList", function(done) {
          var data1 = [{
              email: 'jaie.demaagd@gmail.com',
              profile: {
                  searchradius: 20,
                  localarea: {
                      loc: {
                          type: 'Point',
                          coordinates: [150.8930607, -34.4278121]
                      },
                      name: 'Wollongong NSW 2500, Australia'
                  }
              },
              games: [{
                  thumbnail: 'https://cf.geekdo-images.com/images/pic2437871_t.jpg',
                  image: 'https://cf.geekdo-images.com/images/pic2437871.jpg',
                  yearpublished: 2017,
                  id: 174430,
                  name: 'Gloomhaven',
                  matching: {
                      own: true,
                      wanttoplay: true
                  }
              },
              {
                  thumbnail: 'https://cf.geekdo-images.com/images/pic3581400_t.png',
                  image: 'https://cf.geekdo-images.com/images/pic3581400.png',
                  yearpublished: 2017,
                  id: 228660,
                  name: 'Betrayal at Baldur\'s Gate',
                  matching: {
                      own: false,
                      wanttoplay: true
                  }
              }]
          },
          {
              email: 'jaie.demaagd2@gmail.com',
              profile: {
                  searchradius: 20,
                  localarea: {
                      loc: {
                          type: 'Point',
                          coordinates: [150.8930607, -34.4278121]
                      },
                      name: 'Wollongong NSW 2500, Australia'
                  }
              },
              games: [{
                  thumbnail: 'https://cf.geekdo-images.com/images/pic2437871_t.jpg',
                  image: 'https://cf.geekdo-images.com/images/pic2437871.jpg',
                  yearpublished: 2017,
                  id: 174430,
                  name: 'Gloomhaven',
                  matching: {
                      own: false,
                      wanttoplay: true
                  }
              }]
          }]; 

          var data2 = [{
              email: 'jaie.demaagd@gmail.com',
              profile: {
                  searchradius: 20,
                  localarea: {
                      loc: {
                          type: 'Point',
                          coordinates: [150.8930607, -34.4278121]
                      },
                      name: 'Wollongong NSW 2500, Australia'
                  }
              },
              games: [{
                  thumbnail: 'https://cf.geekdo-images.com/images/pic2437871_t.jpg',
                  image: 'https://cf.geekdo-images.com/images/pic2437871.jpg',
                  yearpublished: 2017,
                  id: 174430,
                  name: 'Gloomhaven',
                  matching: {
                      own: false,
                      wanttoplay: true
                  }
              },
              {
                  thumbnail: 'https://cf.geekdo-images.com/images/pic3581400_t.png',
                  image: 'https://cf.geekdo-images.com/images/pic3581400.png',
                  yearpublished: 2017,
                  id: 228660,
                  name: 'Betrayal at Baldur\'s Gate',
                  matching: {
                      own: false,
                      wanttoplay: true
                  }
              }]
          },
          {
              email: 'jaie.demaagd2@gmail.com',
              profile: {
                  searchradius: 20,
                  localarea: {
                      loc: {
                          type: 'Point',
                          coordinates: [150.8930607, -34.4278121]
                      },
                      name: 'Wollongong NSW 2500, Australia'
                  }
              },
              games: [{
                  thumbnail: 'https://cf.geekdo-images.com/images/pic3581400_t.png',
                  image: 'https://cf.geekdo-images.com/images/pic3581400.png',
                  yearpublished: 2017,
                  id: 228660,
                  name: 'Betrayal at Baldur\'s Gate',
                  matching: {
                      own: false,
                      wanttoplay: true
                  }
              }]
          }];

          expect(findmatch_page.FilterGamesOwned('jaie.demaagd@gmail.com', data1)).toEqual([174430]);
          expect(findmatch_page.FilterGamesOwned('jaie.demaagd@gmail.com', [])).toEqual([]);
          expect(findmatch_page.FilterGamesOwned('jaie.demaagd@gmail.com', 'hi')).toEqual([]);

          expect(findmatch_page.CheckOwnership([174430], { id : 174430, name : 'Gloomhaven', yearpublished : 2017, thumbnail : 'https://cf.geekdo-images.com/images/pic2437871_t.jpg', count : 2 })).toEqual({ id : 174430, name : 'Gloomhaven', yearpublished : 2017, thumbnail : 'https://cf.geekdo-images.com/images/pic2437871_t.jpg', count : 2, own : true });

          expect(findmatch_page.CheckOwnership([174430], { id : 228660, name : "Betrayal at Baldur's Gate", yearpublished : 2017, thumbnail : 'https://cf.geekdo-images.com/images/pic3581400_t.png', count : 1 })).toEqual({ id : 228660, name : "Betrayal at Baldur's Gate", yearpublished : 2017, thumbnail : 'https://cf.geekdo-images.com/images/pic3581400_t.png', count : 1 });

          expect(findmatch_page.BuildGameList(150.8930607, -34.4278121, data1[0].email, data1)).toEqual({ games : [ { id : 174430, name : 'Gloomhaven', yearpublished : 2017, thumbnail : 'https://cf.geekdo-images.com/images/pic2437871_t.jpg', count : 2, own: true }, { id : 228660, name : "Betrayal at Baldur's Gate", yearpublished : 2017, thumbnail : 'https://cf.geekdo-images.com/images/pic3581400_t.png', count : 1 } ] })

          expect(findmatch_page.BuildGameList(150.8930607, -34.4278121, data1[1].email, data1)).toEqual({ games : [ { id : 174430, name : 'Gloomhaven', yearpublished : 2017, thumbnail : 'https://cf.geekdo-images.com/images/pic2437871_t.jpg', count : 2 }, { id : 228660, name : "Betrayal at Baldur's Gate", yearpublished : 2017, thumbnail : 'https://cf.geekdo-images.com/images/pic3581400_t.png', count : 1 } ] })

          expect(findmatch_page.BuildGameList(150.8930607, -34.4278121, data2[0].email, data2)).toEqual({ games : [ { id : 228660, name : "Betrayal at Baldur's Gate", yearpublished : 2017, thumbnail : 'https://cf.geekdo-images.com/images/pic3581400_t.png', count : 2 }, { id : 174430, name : 'Gloomhaven', yearpublished : 2017, thumbnail : 'https://cf.geekdo-images.com/images/pic2437871_t.jpg', count : 1} ] })
          done();

        });
    });

    describe("web page tests", function() {
      it("setup server", function(done) {
        testapp.get('/', function (req, res) {
          res.send('<html>Home Page</html>')
        });

        login_page.RouteLogins(testapp, passport);
        login_page.PostLogin(testapp, passport);
        login_page.RouteVerify(testapp, account);
        server = testapp.listen(httpPort, function() {
          done();
        });
      });

      it("non logged in user visit /events", function(done) {
        const page = 'events';

        browser.visit(base_url + page, function(err) {
          expect(browser.success).toBe(true);
          expect(browser.html()).toContain('Roll for Group');
          done();
        });
      });

      it("invalid user visit /events", function(done) {

        var userEmail = faker.Internet.email();
        var userPass = 'testpassword';
        var userobj = {};

        verifyregister(account, browser, userEmail, userPass, userobj).then(function(x) {
          const page = 'events';
          browser.visit(base_url + page, function(err) {
            expect(browser.success).toBe(true);
            done();
          });
        });
      });

      it("valid user visit /findmatch", function(done) {

        var userEmail = faker.Internet.email();
        var userPass = 'testpassword';
        var userobj = {profile: {
          searchradius: 10,
          localarea: {
            name: 'area',
            loc: {
              coordinates: [20, 20],
              type: 'Point'
            }
          }
        }};

        verifyregister(account, browser, userEmail, userPass, userobj).then(function(x) {
          const page = 'events';
          browser.visit(base_url + page, function(err) {
            expect(browser.success).toBe(true);
            done();
          });
        });
      });

      it('server close', function(done) {
        server.close(function() {
          done();
        });
      });

      it('db close', function(done) {
        mongo.CloseDatabase();
        done();
      });

  });
});
