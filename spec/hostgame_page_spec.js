var Promise = require('bluebird');
var R = require('ramda');
var util = require('util')
var log = require('../app/log.js');

var passport = require('passport');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var hostgame_page = require('../app/hostgame_page.js');

var testBoardgame = require('../app/models/boardgame.js');
var testBoardgames = require('../app/models/boardgames.js');

var Browser = require("zombie");
var browser = new Browser();
var browser2 = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var match = require('../app/models/match.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');

var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 3025;
var login = require('connect-ensure-login');

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);

describe('hostgame_page.js', function() {
    describe('UNIT Tests', function() {

        // BuildHandlebarUIObject
        describe('BuildHandlebarUIObject', function() {

            it('create test object', function(done) {

                //try {

                var fieldsetid = 'game';
                var inputtype1 = 'textinput';
                var inputtype2 = 'select';

                var id = 'test_id';
                var name = 'test_name';
                var instructable = 'test_instructable';
                var label = 'test_label';
                var placeholder = 'test_placeholder';
                var prefixaddon = 'test_prefixaddon';
                var image = 'image/url';
                var options = ['option1', 'option2'];

                var UIObj = hostgame_page.BuildHandlebarUIObject(fieldsetid); 

                var testFieldset = R.compose(R.whereEq({fieldsetid: fieldsetid}), hostgame_page.BuildHandlebarUIObject);
                expect(testFieldset(fieldsetid, inputtype1, id, name, instructable, label, placeholder, prefixaddon, image, options)).toBeTruthy();
                expect(testFieldset('false', inputtype1, id, name, instructable, label, placeholder, prefixaddon, image, options)).toBeFalsy();

                var testHasTextInput = R.compose(R.has(inputtype1), hostgame_page.BuildHandlebarUIObject);
                var testHasSelect = R.compose(R.has(inputtype2), hostgame_page.BuildHandlebarUIObject);
                expect(testHasTextInput(fieldsetid, inputtype1, id, name, instructable, label, placeholder, prefixaddon, image, options)).toBeTruthy();
                expect(testHasSelect(fieldsetid, inputtype1, id, name, instructable, label, placeholder, prefixaddon, image, options)).toBeFalsy();

                expect(testHasTextInput(fieldsetid, inputtype2, id, name, instructable, label, placeholder, prefixaddon, image, options)).toBeFalsy();
                expect(testHasSelect(fieldsetid, inputtype2, id, name, instructable, label, placeholder, prefixaddon, image, options)).toBeTruthy();
                
                var testObjLabel = R.compose(R.whereEq({label: label}), R.prop('select'), hostgame_page.BuildHandlebarUIObject);
                expect(testObjLabel(fieldsetid, inputtype2, id, name, instructable, label, placeholder, prefixaddon, image, options)).toBeTruthy();
                expect(testObjLabel(fieldsetid, inputtype2, id, name, '', '', placeholder, prefixaddon, image, options)).toBeFalsy();

                var testOptions = R.compose(R.ifElse(R.has('select'), R.pipe(R.prop('select'), R.has('options')), R.F), hostgame_page.BuildHandlebarUIObject);
                expect(testOptions(fieldsetid, inputtype1, id, name, instructable, label, placeholder, prefixaddon, image, options)).toBeFalsy();
                expect(testOptions(fieldsetid, inputtype2, id, name, instructable, label, placeholder, prefixaddon, image, options)).toBeTruthy();

                done();

            });

            it('buildHandlebarUITextImageUIObject', function(done) {
                expect(hostgame_page.BuildHiddenUIObject('f', 'i', 'id', 'n', 'v')).toEqual({ fieldsetid : 'f', i : { id : 'id', name : 'n', value : 'v' } });
                done();
            });

            it('buildHandlebarUITextImageUIObject', function(done) {

                var fieldsetid = 'game';
                var inputtype1 = 'textinputimage';

                var id = 'test_id';
                var name = 'test_name';
                var instructable = 'test_instructable';
                var label = 'test_label';
                var placeholder = 'test_placeholder';
                var prefixaddon = 'test_prefixaddon';

                var testFieldset = R.compose(R.whereEq({fieldsetid: fieldsetid}), hostgame_page.BuildHandlebarTextImageUIObject);
                expect(testFieldset(fieldsetid, id, name, instructable, label, placeholder, prefixaddon)).toBeTruthy();

                expect(testFieldset('false', id, name, instructable, label, placeholder, prefixaddon)).toBeFalsy();

                var testHasTextInput = R.compose(R.has(inputtype1), hostgame_page.BuildHandlebarTextImageUIObject);

                expect(testHasTextInput(fieldsetid, inputtype1, id, name, instructable, label, placeholder, prefixaddon)).toBeTruthy();

                var testObjLabel = R.compose(R.whereEq({label: label}), R.prop(inputtype1), hostgame_page.BuildHandlebarTextImageUIObject);
                expect(testObjLabel(fieldsetid, id, name, instructable, label, placeholder, prefixaddon)).toBeTruthy();

                var testObjHidden = R.compose(R.whereEq({name: name + 'inv', id: id + 'id'}), R.path([inputtype1, 'hiddeninput']), hostgame_page.BuildHandlebarTextImageUIObject);
                expect(testObjHidden(fieldsetid, id, name, instructable, label, placeholder, prefixaddon)).toBeTruthy();

                var testObjImg = R.compose(R.whereEq({id: id + 'img'}), R.path([inputtype1, 'image']), hostgame_page.BuildHandlebarTextImageUIObject);
                expect(testObjImg(fieldsetid, id, name, instructable, label, placeholder, prefixaddon)).toBeTruthy();
                done();

            });

        });

        describe('GetHostGameAddress', function() {

            // Returns object handlebars data for user hostlocation, and default smoking/alcohol preferences 
            it('email does not exist', function(done) {
                var fakeemail = 'notexist@gmail.com';
                hostgame_page.GetHostGameAddress(account, fakeemail).then(done(), function(err) {
                    expect(err).toBe('email not found');
                    done();
                });
            });

            it('host exists and all is ok', function(done) {

                var testUser = 'TemplateMatchesAPIHost'; 
                var testEmail = 'templatemapihost@gmail.com';
                var alcohol = 2;
                var smoking = 1;

                var mapiaccount = {
                    nickname:   testUser,
                    profile: {
                        name:   testUser,
                    }
                };

                // init email
                var newaccount = accounts.RespawnAccountObject(account, testEmail, mapiaccount).then(function(b) { 
                    hostgame_page.GetHostGameAddress(account, testEmail).then(function(x) {

                        var readLabel = R.path(['hiddeninput', 'label']);
                        var readInstructable = R.path(['hiddeninput', 'instructable']);
                        var readDefault = R.compose(R.add(1), R.findIndex(R.propEq('default', true)), R.path(['select', 'options']));

                        expect(readDefault(x[0])).toBe(smoking);
                        expect(readDefault(x[1])).toBe(alcohol);
                        done();

                    });
                });

            });
        });
        
        describe('CreateOptions', function() {

            var objectTest = function(option, value) {
                return R.where({option: R.equals(option), value: R.equals(value)});
            };

            it('test response', function(done) {

                // Returns select options handlebars data, setting the defaults for two options
                var val1 = 'testval1';
                var val2 = 'testval2';

                var makeoptions = R.compose(R.flatten, hostgame_page.CreateOptions);
                var result = makeoptions(val1, val2);
                
                var valdefault = R.where({
                    default: R.equals(true)
                });

                var testFirstValue = R.compose(objectTest(val1, 1), R.nth);
                var testSecondValue = R.compose(objectTest(val2, 2), R.nth);
                var testIsDefault = R.compose(valdefault, R.nth);
                var testHasValue = R.compose(R.prop('value'), R.nth);

                expect(testFirstValue(0, result)).toBeTruthy();
                expect(testSecondValue(0, result)).toBeFalsy();
                expect(testIsDefault(0, result)).toBeTruthy();
                expect(testHasValue(0, result)).toBe(1);

                expect(testFirstValue(1, result)).toBeFalsy();
                expect(testSecondValue(1, result)).toBeTruthy();
                expect(testIsDefault(1, result)).toBeFalsy();
                expect(testHasValue(1, result)).toBe(2);

                expect(testFirstValue(2, result)).toBeTruthy();
                expect(testSecondValue(2, result)).toBeFalsy();
                expect(testIsDefault(2, result)).toBeFalsy();
                expect(testHasValue(2, result)).toBe(1);

                expect(testFirstValue(3, result)).toBeFalsy();
                expect(testSecondValue(3, result)).toBeTruthy();
                expect(testIsDefault(3, result)).toBeTruthy();
                expect(testHasValue(3, result)).toBe(2);

                done();

            });
        });
    });


});


// ============== RUN TESTS ================== 
describe("hostgame_page_spec.js", function() {

    var server;

    // loginUser :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
    var verifyregister = function(account, zombieb, email, pass, userobj) {
        return new Promise(function(fulfill, reject) {

            accounts.DeleteAccount(account, email).then(function(y) {
                var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

                register(email, pass).then(function(x) {
                    account.findOneAndUpdate({email: email}, userobj, {new:true}).exec().then(function(z) {
                        zombieb.visit(base_url + "login", function(err) {
                            if (err) 
                                reject(err);

                            zombieb.fill('input[name="form-username"]', email)
                            zombieb.fill('input[name="form-password"]', pass)
                            zombieb.pressButton('login', function() {
                                fulfill(x);
                            });
                        });
                    });
                });
            });
        })
    };

    // String -> Number -> Number -> Object
    var createHostAccountObj = function(location, lat, lng, avatar) {
        return {
            profile: {
                ishost: 2,
                avataricon: avatar
            },
            hostareas: [
                {
                    name: location,
                    loc: {
                        coordinates: [lat, lng]
                    }
                }
            ],
            tickets: 5,
            ticketshold:0
        };
    };

    // BoardgameSchema -> Number -> Number -> String -> String -> Number -> Promise(Boardgame)
    var cacheGame = R.curry(function(testBoardgame, currentyear, gameid, gamename, thumbnail, gameyear) { 
      return new Promise(function(fulfill, reject) {
          var cacheObject = {
              numplays: 0,
              thumbnail: thumbnail,
              yearpublished: gameyear,
              name: {
                  t: gamename,
                  sortindex: 1
              },
              subtype: 'boardgame',
              objectid: gameid,
              objecttype: 'thing'
          };
          fulfill(testBoardgames.CacheBoardgame(testBoardgame, currentyear, cacheObject));
      });
    });
    var cacheGame2017 = cacheGame(testBoardgame, 2017);


    describe("web page tests", function() {

      it("buildURL", function(done) {
        expect(hostgame_page.BuildURL('a')).toBe('hostgame?event=a');
        expect(hostgame_page.BuildURL('xyz')).toBe('hostgame?event=xyz');
        done();
      });

      it("Flat", function(done) {
        expect(hostgame_page.Flat({}, [['a'],['b']]) ).toEqual(['a', 'b']);
        expect(hostgame_page.Flat({query: {event: 'x'}}, [['a'],['b']]) ).toEqual([{}, {}, ['b']]);
        done();
      });

      it("BuildPageData", function(done) {
        expect(hostgame_page.BuildPageData('a', 'b').sections[0].label).toBe('a')
        expect(hostgame_page.BuildPageData('a', 'b').sections[0].questions).toBe('b')
        done();
      });

      it("BuildConvHeader", function(done) {
        expect(hostgame_page.BuildConvHeader('xyz')).toBe('Convention / xyz');
        done();
      });

      it("errortext", function(done) {
        expect(hostgame_page.ErrorText('xyz')).toBe('Can not host game: xyz');
        done();
      });

      it("setup server", function(done) {

        testapp.get('/', function (req, res) {
          res.send('<html>Home Page</html>')
        });

        hostgame_page.RouteHostgame(testapp, login, match, account, testBoardgame);
        login_page.RouteLogins(testapp, passport);
        login_page.PostLogin(testapp, passport);
        login_page.RouteVerify(testapp, account);
        server = testapp.listen(httpPort, function() {
          done();
        });

      });

      it("non logged in user visit hostgame", function(done) {
        page = 'hostgame?gameid=12345&isowned=1';
        browser.visit(base_url + page, function(err) {
            expect(browser.success).toBe(true);
            expect(browser.html()).toContain('Roll for Group');
            done();
        });
      });

      it("user login", function(done) {
        var userEmail = 'match_webmatchhost@gmail.com';
        var userPass = 'testpassword';
        verifyregister(account, browser, userEmail, userPass, {} ).then(function(x) {
          done();
        });
      }, 10000);

      // TODO: put zombie tests in here
      it("valid user visit /hostgame", function(done) {

        var userEmail = 'match_webmatchhost@gmail.com';
        var userPass = 'testpassword';

        var gameid = 43015;
        var gamename = 'Hansa Teutonica';
        var thumbnail = '//cf.geekdo-images.com/R6eYnXnFgniTevooXHPjbQIB09c=/fit-in/246x300/pic839090.jpg';
        var gameyear = 2016;

        var match1 = {gameid: gameid};
        var gameDate = new Date(2017, 06, 3, 20, 30, 0); 

        var hostAvatar = 'http://localhost:3000/img/test-avatar2.jpg';

        var matchUser = {
          email: userEmail, 
          avataricon: hostAvatar
        };

        var matchPlayers = 4;

        var matchLocName = 'Ularu Australia';
        var lat = -25.344428;
        var lng = 131.036882;
        var coords = [lng, lat];

        var status = 'open';
        var smoking = 2;
        var alcohol = 1;
        var social = {smoking: smoking, alcohol: alcohol};

        var hostaccount = createHostAccountObj(matchLocName, lat, lng, hostAvatar);

        Promise.all([verifyregister(account, browser, userEmail, userPass, hostaccount), match.remove(match1), testBoardgames.DeleteBoardgame(testBoardgame, gameid)]).then(function(y) {
          cacheGame2017(gameid, gamename, thumbnail, gameyear).then(function(y) {
            browser.visit(base_url + 'hostgame?gameid=' + gameid + '&isowned=1', function(err) {
              expect(browser.success).toBe(true);
              expect(browser.html()).toContain('Create Event');
              done();
            });
          });
        });

      }, 10000);

      it('server close', function(done) {
        server.close(function() {
          done();
        });
      });

      it('db close', function(done) {
        mongo.CloseDatabase();
        done();
      });

    });

});
