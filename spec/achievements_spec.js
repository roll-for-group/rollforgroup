var express = require("express"); 
var router = express.Router(); 
var log = require('../app/log.js'); 
var R = require('ramda'); 

var Promise = require('bluebird');
var faker = require('Faker');

var Task    = require('data.task');
var Maybe   = require('data.maybe');
var Either  = require('data.either');

var moment  = require('moment');

var genhelper = require('./genhelper.js');

var account = require('../app/models/account.js');

var achievement = require('../app/models/achievement.js');
var api_achievements = require('../app/models/achievements.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var mongo = require('../app/mongoconnect.js');
var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');

var httpPort = 3629;

var testapp = express();

const ajaxData = zipObj(['url', 'method', 'data']);
var ajax = require('./ajax.js');
var ajaxPromise = ajax.AjaxPromise;

var S = require('../app/lambda.js');

// ============== VARS ===============
var url_base = "http://localhost:" + httpPort;
var server;

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  testapp.close(function(){
    console.log('accounts/:email/achievements closed on port ' + httpPort);
  });
};

// url to test
var url_api = '/api/v1/accounts';
var generateUser = genhelper.GenerateUser(account);


// ============== RUN TESTS ================== 
describe("achievements_spec.js api", function() {
  describe("read basic data", function() {

    it("setup server", function(done) {
      mongo.ConnectDatabase(dbUri);
      router = api_achievements.SetupRouter(express.Router());
      testapp.use(url_api, router);
      server = testapp.listen(httpPort, function() {
        done();
      });
    });

    it('maybeGetQuery', function(done) {
      expect(api_achievements.maybeGetQuery({}).isJust).toBeFalsy();
      expect(api_achievements.maybeGetQuery({'params':{}}).isJust).toBeFalsy();
      expect(api_achievements.maybeGetQuery({'params':{'userkey':'abc'}}).get()).toEqual({userkey:'abc', achieved: true, public: true});
      done();
    });

    it('maybeGetShowcase', function(done) {
      expect(api_achievements.maybeGetShowcase({}).isJust).toBeFalsy();
      expect(api_achievements.maybeGetShowcase({'params':{}}).isJust).toBeFalsy();
      expect(api_achievements.maybeGetShowcase({'params':{'userkey':'def'}}).get()).toEqual({userkey:'def', achieved: true, public: true, showcase: true});
      done();
    });

    it('maybeGetInProgress', function(done) {
      expect(api_achievements.maybeGetInProgress({}).isJust).toBeFalsy();
      expect(api_achievements.maybeGetInProgress({'params':{}}).isJust).toBeFalsy();
      expect(api_achievements.maybeGetInProgress({'params':{'userkey':'def'}}).get()).toEqual({userkey:'def', achieved: false, public: true});
      done();
    });

  });


  describe("api_calls", function() {
    const endpoint = (base, api) => base + api;

    it("api/v1/accounts/:userkey/achievements POST (success)", function(done) {

      // apiEndpoint :: s -> o -> s
      const apiEndpoint = (urlbase, urlapi, user) => urlbase + urlapi + '/' + user.pageslug + '/achievements';

      Promise.all([generateUser({}), generateUser({})]).then(function(u) {

        var requestData = [
          {
            url: apiEndpoint(url_base, url_api, {}),
            method: 'GET',
            data: {}
          }, {
            url: apiEndpoint(url_base, url_api, u[0]),
            method: 'GET',
            data: {}
          }, {
            url: apiEndpoint(url_base, url_api, u[1]),
            method: 'GET',
            data: {}
          }, {
            url: apiEndpoint(url_base, url_api, u[1]),
            method: 'GET',
            data: {showcase: true}
           }, {
            url: apiEndpoint(url_base, url_api, u[1]),
            method: 'GET',
            data: {inprogress: true}
         },
       ];

        achievement.insertMany([{
          userkey: u[0].pageslug
        }, {
          userkey: u[1].pageslug
        }, {
          userkey: u[1].pageslug,
          name: 'inprogress',
          achieved: false 
        }, {
          userkey: u[1].pageslug,
          public: false
        }, {
          userkey: u[1].pageslug,
          name: 'showcase',
          showcase: true 
        }]).then((newA) => {
          Promise.all(R.map(ajaxPromise, requestData)).then(function(x) {
            expect(x[0].statusCode).toBe(200);
            expect(x[0].body.length).toBe(0);

            expect(x[1].statusCode).toBe(200);
            expect(x[1].body.length).toBe(1);

            expect(x[2].statusCode).toBe(200);
            expect(x[2].body.length).toBe(2);

            expect(x[3].statusCode).toBe(200);
            expect(x[3].body[0].name).toBe('showcase');
            expect(x[3].body.length).toBe(1);

            expect(x[4].statusCode).toBe(200);
            expect(x[4].body[0].name).toBe('inprogress');
            expect(x[4].body.length).toBe(1);

            done()
          });
        });

      }); 
    }); 
    


    it("close server", function(done) {
      server.close(function() {
        mongo.CloseDatabase();
        done();
      });
    });

  });

});
