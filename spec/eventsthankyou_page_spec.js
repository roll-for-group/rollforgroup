var Promise = require('bluebird');
var R = require('ramda');
var util = require('util');
var log = require('../app/log.js');

var passport = require('passport');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var eventsthankyou_page = require('../app/eventsthankyou_page.js');

var genhelper = require('./genhelper.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var browser2 = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');
var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');
var boardgame = require('../app/models/boardgame.js');
var boardgames = require('../app/models/boardgames.js');


var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');

var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 23457;
var login = require('connect-ensure-login');

// String -> (Promise -> String)
var zombieGetBody = zombieHelper.PromiseZombieVisit(browser, "body");

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
    testapp.close(function(){
        console.log('matches_service_spec closed on port ' + httpPort);
    });
};

describe('eventsthankyou_page.js', function() {
    describe('UNIT Tests', function() {

    });
});


// ============== RUN TESTS ================== 
describe("eventsthankyou_page.js ", function() {

    var server;

    // String -> Number -> Number -> Object
    var createHostAccountObj = function(location, lat, lng, avatar) {
        return {
            profile: {
                ishost: 2,
                avataricon: avatar
            },
            hostareas: [
                {
                    name: location,
                    loc: {
                        coordinates: [lat, lng]
                    }
                }
            ]
        };
    };


    var generateUser = genhelper.GenerateUser(account);
    var generateHostUser = genhelper.GenerateHostUser(account);

    const generateMatch = genhelper.GenerateFakeHostedMatch(account, boardgame, match);

    // verifyregister :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
    var verifyregister = function(account, zombieb, email, pass, userobj) {
      return new Promise(function(fulfill, reject) {

        accounts.DeleteAccount(account, email).then(function(y) {
          var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

          register(email, pass).then(function(x) {

            account.update({email: email}, userobj).exec().then(function(z) {
              zombieb.visit(base_url + "login", function(err) {
                if (err) 
                  reject(err);

                zombieb.fill('input[name="form-username"]', email)
                zombieb.fill('input[name="form-password"]', pass)
                zombieb.pressButton('login', function() {
                  fulfill(x);
                });

              });
            });
          });
        });
      })
    };


    // BoardgameSchema -> Number -> Number -> String -> String -> Number -> Promise(Boardgame)
    var cacheGame = R.curry(function(testBoardgame, currentyear, gameid, gamename, thumbnail, gameyear) { 
        return new Promise(function(fulfill, reject) {
            var cacheObject = {
                numplays: 0,
                thumbnail: thumbnail,
                yearpublished: gameyear,
                name: {
                    t: gamename,
                    sortindex: 1
                },
                subtype: 'boardgame',
                objectid: gameid,
                objecttype: 'thing'
            };
            fulfill(boardgames.CacheBoardgame(testBoardgame, currentyear, cacheObject));
        });
    });
    var cacheGame2017 = cacheGame(boardgame, 2017);

    var generateRandomHost = accounts.RespawnAccountObject(account, faker.Internet.email(), createHostAccountObj(faker.Address.streetAddress(), faker.Address.latitude(), faker.Address.longitude(), faker.Image.avatar()));

    describe("web page tests", function() {

        it("setup server", function(done) {

			testapp.get('/', function (req, res) {
				res.send('<html>Home Page</html>')
			});

            eventsthankyou_page.RouteEventsThankyou(testapp, login, match, account);
            login_page.RouteLogins(testapp, passport);
            login_page.PostLogin(testapp, passport);
            login_page.RouteVerify(testapp, account);

            server = testapp.listen(httpPort, function() {
                done();
            });
        }, 10000);

		it("non logged in  user visit eventsthankyou?matchkey=1234", function(done) {
            page = 'eventsthankyou?matchkey=1234';
            browser.visit(base_url + page, function(err) {
                expect(browser.success).toBe(true);
                expect(browser.html('.headertext')).toContain('Lets play board games');
                done();
            });
		}, 10000);

		it("valid user visit invalid /eventsthankyou?matchkey=1234", function(done) {
            var userEmail = 'match_web1@gmail.com';
            var userPass = 'testpassword';

            verifyregister(account, browser, userEmail, userPass, {} ).then(function(x) {
                page = 'eventsthankyou?matchkey=1234';
                browser.visit(base_url + page, function(err) {
                    expect(browser.success).toBe(false);
                    expect(browser.html()).toContain('<pre>Cannot GET /events</pre>');
                    done();
                });
            });
		}, 10000);


		it("valid user visit /eventsthankyou", function(done) {

            var userEmail = 'match_web1@gmail.com';
            var userPass = 'testpassword';

            generateMatch('open', 2, 2).then(function(y) {
                verifyregister(account, browser, userEmail, userPass, {} ).then(function(x) {
                    page = 'eventsthankyou?matchkey=' + y[1].key;
                    browser.visit(base_url + page, function(err) {
                        expect(browser.success).toBe(true);
                        done();
                    });
                });
            });
		}, 5000);

        it('server close', function(done) {
            server.close(function() {
                done();
            });
        });

        it('db close', function(done) {
            mongo.CloseDatabase();
            done();
        });
    });

});
