var express = require("express"); 
var router = express.Router(); 
var log = require('../app/log.js'); 
var R = require('ramda'); 

var Promise = require('bluebird');
var faker = require('Faker');

var Task    = require('data.task');
var Maybe   = require('data.maybe');
var Either  = require('data.either');

var moment  = require('moment');

var genhelper = require('./genhelper.js');

var match   = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var account = require('../app/models/account.js');
var testBoardgame = require('../app/models/boardgame.js');

var api_matches = require('../app/matches_service_forum.js');

var emailer = require('../app/emailer.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var mongo = require('../app/mongoconnect.js');
var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');

var httpPort = 3084;

var testapp = express();

var nodemailer = require('nodemailer');
var stubTransport = require('nodemailer-stub-transport');
var transporter = nodemailer.createTransport(stubTransport());

const ajaxData = zipObj(['url', 'method', 'data']);
var ajax = require('./ajax.js');
var ajaxPromise = ajax.AjaxPromise;

var S = require('../app/lambda.js');

// ============== VARS ===============
var url_base = "http://localhost:" + httpPort;
var server;

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  testapp.close(function(){
    console.log('matches_service_spec closed on port ' + httpPort);
  });
};

// createMatchObj :: {o} -> {o}
const createMatchObj = R.compose(
  R.of, 
  matches.Zipmatch, 
  R.zipObj(['id', 'name', 'thumbnail', 'description', 'minplaytime', 'maxplaytime', 'minplayers', 'maxplayers'])
);


// buildHost :: o -> o -> s -> s -> d -> n -> n -> s -> o -> s -> [n, n] -> o -> bool) -> o 
const buildHost = function (match, account, email, key, gamedate, maxplayers, minplayers, status, social, locname, coords, gameObj, isprivate) {

  const host = R.compose(
    matches.HostMatch(match, account, email, key),
    zipObj(['timeslots', 'maxplayers', 'minplayers', 'status', 'social', 'locname', 'coords', 'games', 'isprivate'])
  );

  return host([[{startdatetime: gamedate, enddatetime: moment(gamedate).add(60*60, 's')}], maxplayers, minplayers, status, social, locname, coords, gameObj, isprivate]);

};


// url to test
var url_api = '/api/v1/matches';

var generateUser = genhelper.GenerateUser(account);
var generateHostUser = genhelper.GenerateHostUser(account);
var generateMatch = genhelper.GenerateFakeHostedMatch(account, testBoardgame, match);
var generateGame = genhelper.GenerateFakeBoardgame(testBoardgame);


// ============== RUN TESTS ================== 
describe("matches_service_forum_spec.js api", function() {

  describe("read basic data", function() {

    it("scrub", function(done) {
      const expectObj = {a: 'scrubbed'};
      expect(api_matches.Scrub(R.zipObj(['a'], ['scrubbed']))).toEqual(expectObj);
      expect(api_matches.Scrub(R.zipObj(['a', '_id', '__v'], ['scrubbed', 1, 2]))).toEqual(expectObj);
      done();
    });

    it("setup server", function(done) {
      mongo.ConnectDatabase(dbUri);
      router = api_matches.SetupRouter(nconf.get('smtp:user'), transporter, express.Router());
      testapp.use(url_api, router);
      server = testapp.listen(httpPort, function() {
        done();
      });
    });


    it('allPlayersExcept', function(done) {
      expect(api_matches.allPlayersExcept('abc', {}).isNothing).toBeTruthy();
      expect(api_matches.allPlayersExcept('abc', {players:[]}).get()).toEqual([]);
      expect(api_matches.allPlayersExcept('abc', {players:[{email:'def'}]}).get()).toEqual([{email:'def'}]);
      expect(api_matches.allPlayersExcept('abc', {players:[{email:'abc'}, {email:'def'}]}).get()).toEqual([{email:'def'}]);
      done();
    });

    it('getUserEmail', function(done) {
      expect(api_matches.getUserEmail({})).toBe('');
      expect(api_matches.getUserEmail({user:{}})).toBe('');
      expect(api_matches.getUserEmail({user:{email:'frank'}})).toBe('frank');
      done();
    });

    it('buildMatchURL', function(done) {
      expect(api_matches.buildMatchURL('abc')).toBe('/events/abc');
      expect(api_matches.buildMatchURL('def')).toBe('/events/def');
      done();
    });
  });


  describe("matches_stuff", function() {

    var whereGameIs = function(gameid, gamename, thumbnail) {
      return R.whereEq(matches.defaultGame(gameid, gamename, thumbnail));
    };

    // Number -> String -> Date -> Number -> Number -> Number -> Boolean
    var whereMatchIs = function(locname, gamedate, gameend, players, minplayers, status, smoking, alcohol) {
      // timeslots:  [{startdatetime: gamedate, enddatetime: gameend}],
      return R.whereEq({
        locname:    locname,
        seatmax:    players,
        seatmin:    minplayers,
        status:     status,
        smoking:    smoking,
        alcohol:    alcohol
      });
    };

    it('hashify', function(done) {
      expect(R.has('hash', api_matches.hashify('o', {o:'a'}))).toBeTruthy();
      expect(R.prop('o', api_matches.hashify('o', {o:'a'}))).toBe('a');
      done();
    });

    it("whenUploadLocaliseURL", function(done) {
      expect(api_matches.whenUploadLocaliseURL('abc')).toBe('abc');
      expect(api_matches.whenUploadLocaliseURL('upload/abc')).toBe('/upload/abc');
      done();
    });

    it('MaybeThreadAvatar', function(done) {
      expect(api_matches.MaybeThreadAvatar({})).toEqual({ avataricon : '/img/default-avatar-sq.jpg' });
      expect(api_matches.MaybeThreadAvatar({thread:{}})).toEqual({thread:{}, avataricon : '/img/default-avatar-sq.jpg' });
      expect(api_matches.MaybeThreadAvatar({thread:{avataricon:'a'}})).toEqual({thread:{avataricon:'a'}, avataricon: 'a' });
      done();
    });

    it('UpdateUserProp', function(done) {
        expect(api_matches.UpdateUserProp('c', 'r', 's', 'p', {})([{email:'def'}]).isNothing).toBe(true);
        expect(api_matches.UpdateUserProp('c', 'r', 's', 'p', {thread:{}})([{email:'def'}]).isNothing).toBe(true);
        expect(api_matches.UpdateUserProp('c', 'r', 's', 'p', {thread:{useremail:'abc'}})([]).isNothing).toBe(true);
        expect(api_matches.UpdateUserProp('c', 'r', 's', 'p', {thread:{useremail:'abc'}})([{email:'def'}]).isNothing).toBe(true);
        expect(api_matches.UpdateUserProp('c', 'r', 's', 'p', {thread:{useremail:'abc'}})([{email:'abc'}]).get()).toEqual({ 'c' : 'r' });
        expect(api_matches.UpdateUserProp('c', 'r', 's', 'p', {thread:{useremail:'abc'}})([{email:'abc', premium:{isstaff:true}}]).get()).toEqual({ 'c' : 's' });
        expect(api_matches.UpdateUserProp('c', 'r', 's', 'p', {thread:{useremail:'abc'}})([{email:'abc', premium:{isactive:true}}]).get()).toEqual({ 'c' : 'p' });
        done();
    });


    it('FindMatchByKey', function(done) {

      var minPlayers = 2;
      var players = 3;
      var status = 'draft';
      var smoking = 1;
      var alcohol = 2;
      var social = {smoking, alcohol};

      var playyear = new Date().getFullYear() + 1;
      var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 

      generateHostUser({}).then(function(u) {
        Promise.all([generateGame]).then(function(g) {

          buildHost(match, account, u.email, u.key, gameDate, players, minPlayers, status, social, u.hostareas[0].name, u.hostareas[0].loc.coordinates, createMatchObj([g[0].id, g[0].name, g[0].thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4])).then(function (m) {

            matchATest = (gameEnd) => whereMatchIs(u.hostareas[0].name, gameDate, gameEnd, players, minPlayers, status, smoking, alcohol);
            gameATest = whereGameIs(g[0].id, g[0].name, g[0].thumbnail);

            buildHost(match, account, u.email, u.key, gameDate, players, minPlayers, status, social, u.hostareas[0].name, u.hostareas[0].loc.coordinates, createMatchObj([g[0].id, g[0].name, g[0].thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4])).then(function(h) {

              api_matches.FindMatchByKey(match, h.key).then(function(f) {
                expect(matchATest(f[0].timeend)(f[0])).toBe(true);
                expect(gameATest(f[0].games[0])).toBe(true);
                done();
              });

            });
          });
        });
      });
    });

    it('UpdateUserClass', function(done) {

      expect(api_matches.UpdateUserClass([{email:'def'}], {})).toEqual({ textclass : 'primary-font text-standard', imgclass : 'img-circle user-chat-avatar imglistcircle-standard imglistcircle' });
      expect(api_matches.UpdateUserClass([{email:'def'}], {thread:{}})).toEqual({ textclass : 'primary-font text-standard', imgclass : 'img-circle user-chat-avatar imglistcircle-standard imglistcircle', thread : {  } });
      expect(api_matches.UpdateUserClass([], {thread:{useremail:'abc'}})).toEqual({ textclass : 'primary-font text-standard', imgclass : 'img-circle user-chat-avatar imglistcircle-standard imglistcircle', thread : { useremail : 'abc' } });
      expect(api_matches.UpdateUserClass([{email:'def'}], {thread:{useremail:'abc'}})).toEqual({ textclass : 'primary-font text-standard', imgclass : 'img-circle user-chat-avatar imglistcircle-standard imglistcircle', thread : { useremail : 'abc' } });
      expect(api_matches.UpdateUserClass([{email:'abc'}], {thread:{useremail:'abc'}})).toEqual({ textclass : 'primary-font text-standard', imgclass : 'img-circle user-chat-avatar imglistcircle-standard imglistcircle', thread : { useremail : 'abc' } });
      expect(api_matches.UpdateUserClass([{email:'abc', premium:{isstaff:true}}], {thread:{useremail:'abc'}})).toEqual({ textclass : 'primary-font text-staff', imgclass : 'img-circle user-chat-avatar imglistcircle-staff imglistcircle', thread : { useremail : 'abc' } });
      expect(api_matches.UpdateUserClass([{email:'abc', premium:{isactive:true}}], {thread:{useremail:'abc'}})).toEqual({ textclass : 'primary-font text-premium', imgclass : 'img-circle user-chat-avatar imglistcircle-premium imglistcircle', thread : { useremail : 'abc' } });
      done();
    });

    it('UpdateForumStatus', function(done) {
      expect(api_matches.UpdateForumStatus([])({}).isNothing).toBe(true);
      expect(api_matches.UpdateForumStatus([])({publicforums:[{thread:{avataricon:'a', email:'def'}}]}).get()).toEqual([ { textclass : 'primary-font text-standard', imgclass : 'img-circle user-chat-avatar imglistcircle-standard imglistcircle', thread : { avataricon : 'a', email : 'def' }, avataricon : 'a', fromnow : 'unknown time' } ]);
      expect(api_matches.UpdateForumStatus([{email:'def'}])({}).isNothing).toEqual(true);
      expect(api_matches.UpdateForumStatus([{email:'def', premium:{isstaff:true}}])({publicforums:[{thread:{avataricon:'a', email:'def'}}]}).get()).toEqual([ { textclass : 'primary-font text-standard', imgclass : 'img-circle user-chat-avatar imglistcircle-standard imglistcircle', thread : { avataricon : 'a', email : 'def' }, avataricon : 'a', fromnow : 'unknown time' } ]);
      done();
    });

    it('TaskLookupPlayers', function(done) {
      Promise.all([generateMatch('open', 3, 2), generateUser({})]).then(function(m) {
        const makeTestMatch = R.compose(R.merge({publicforums:[{thread:{useremail:m[1].email, avataricon:'ai', comment:'cm'}}]}), JSON.parse, JSON.stringify, R.head)
        api_matches.TaskLookupPlayers([makeTestMatch(m)]).fork(
          err => log.clos('err', err),
          data => {
            expect(data[0].email).toBe(m[1].email);
            expect(data[0].pageslug).toBe(m[1].pageslug);
            expect(data[0].key).toBe(m[1].key);
            done();
          }
        )
      })
    });

    it('AllPublicMessages', function(done) {
      var errorCode = R.prop('code');
      var comments = [faker.Lorem.paragraph(), faker.Lorem.paragraph(), faker.Lorem.paragraph()];
      var headpost = R.compose(R.prop("thread"), R.head);
      var extractMatch = R.compose(R.nth(1), R.nth(0));
      var extractUsers = R.compose(R.remove(1, 1), R.flatten);
      var liftuser = R.curry((user, x) => R.compose(R.nth(x), extractUsers)(user));
      var readHash = R.curry((m, order) => m.publicforums[order].hash);
      var post = R.curry((genmatch, comment) => api_matches.PublicForumPost(account, match, liftuser(genmatch, 0).email, liftuser(genmatch, 0).key, extractMatch(genmatch).key, comment));

      // fake hosted match
      Promise.all([generateMatch('open', 3, 2)]).then(function(m) {
        var postmatch = post(m);
        Promise.all(R.map(postmatch, comments)).then(function(cm) {
          api_matches.FindMatchByKey(match, extractMatch(m).key).then(function(frm) {
            api_matches.AllPublicMessages(match, extractMatch(m).key).then(function(nm) {
              expect(nm.length).toBe(3);

              api_matches.postThread(match, extractMatch(m).key, 'publicforums', {thread:{comment:'hi there'}}).then(function(pt) {
                expect(R.last(pt.publicforums).thread.comment).toBe('hi there');
                done();
              }); 
            });
          }); 
        });
      });
    });

    it('NewPublicMessages', function(done) {
      var errorCode = R.prop('code');
      var comments = [faker.Lorem.paragraph(), faker.Lorem.paragraph(), faker.Lorem.paragraph()];
      var headpost = R.compose(R.prop("thread"), R.head);
      var lastpost = R.compose(R.prop("thread"), R.last);

      var extractMatch = R.compose(R.nth(1), R.nth(0));
      var extractUsers = R.compose(R.remove(1, 1), R.flatten);

      var liftuser = R.curry((user, x) => R.compose(R.nth(x), extractUsers)(user));
      var readHash = R.curry((m, order) => m.publicforums[order].hash);

      var post = R.curry((genmatch, comment) => api_matches.PublicForumPost(account, match, liftuser(genmatch, 0).email, liftuser(genmatch, 0).key, extractMatch(genmatch).key, comment));

      // fake hosted match
      Promise.all([generateMatch('open', 3, 2)]).then(function(m) {
        var postmatch = post(m);
        Promise.all(R.map(postmatch, comments)).then(function(cm) {
          matches.FindMatchByKey(match, extractMatch(m).key).then(function(frm) {
            var extractKeys = R.map(readHash(frm[0]));
            Promise.all(R.map(api_matches.NewPublicMessages(match, extractMatch(m).key), extractKeys([0,1,2]))).then(function(nm) {
              expect(nm[0].length).toBe(2);
              expect(nm[1].length).toBe(1);
              expect(nm[2].length).toBe(0);

              expect(headpost(nm[0]).comment).not.toBe(lastpost(nm[0]).comment);
              expect(headpost(nm[1]).comment).toBe(lastpost(nm[1]).comment);
              done();

            });
          }); 
        });
      });
    });
  });

  it('threadUserEmail', function(done) {
    expect(api_matches.ThreadUserEmail({})).toBe('');
    expect(api_matches.ThreadUserEmail({thread:{}})).toBe('');
    expect(api_matches.ThreadUserEmail({thread:{useremail:'abc'}})).toBe('abc');
    done();
  });

  it("notifyMatchUsers", function(done) {

    Promise.all([
      generateUser({}), 
      generateUser({}), 
      generateUser({}), 
      generateUser({})
    ]).then(function(y) {

      var matchA = [
        {
          extraplayers: 0,
          email: y[0].email,
          type: 'host',
          avataricon: 'pic1.jpg',
          name: y[0].profile.name,
          playerkey: '3562fd72-bc10-4288-8fde-ab1abe4e7616'
        },
        {
          extraplayers: 0,
          email: y[1].email,
          type: 'invite',
          avataricon: 'pic2.jpg',
          name: y[1].profile.name,
          playerkey: '3562fd72-bc10-4288-8fde-ab1abe4e7617'
        },
        {
          extraplayers: 0,
          email: y[2].email,
          type: 'approve',
          avataricon: 'pic3.jpg',
          name: y[2].profile.name,
          playerkey: '3562fd72-bc10-4288-8fde-ab1abe4e7618'
        },
        {
          extraplayers: 0,
          email: y[3].email,
          type: 'standby',
          avataricon: 'pic4.jpg',
          name: y[3].profile.name,
          playerkey: '3562fd72-bc10-4288-8fde-ab1abe4e7619'
        }
      ];

      // buildMessage :: s -> s 
      const buildMessage = R.curry((a, b) => a + " and " + b); 

      R.sequence(Task.of, [
        api_matches.notifyMatchUsers('txt', Maybe.of('ai'), Maybe.of('123'), matchA)
      ]).fork(()=>{}, (val)=>{

        expect(val[0][0][0].status).toEqual('new');
        expect(val[0][0][0].text).toEqual('txt');
        expect(val[0][0][0].thumbnail).toEqual('ai');
        expect(val[0][0][0].url).toEqual('/events/123');
        expect(val[0][0][0].noticetype).toEqual('forumpost');
        expect(val[0][0][0].openNewTab).toEqual(false);

        expect(val[0][1][0].status).toEqual('new');
        expect(val[0][1][0].text).toEqual('txt');
        expect(val[0][1][0].thumbnail).toEqual('ai');
        expect(val[0][1][0].url).toEqual('/events/123');
        expect(val[0][1][0].noticetype).toEqual('forumpost');
        expect(val[0][1][0].openNewTab).toEqual(false);

        expect(val[0][2][0].status).toEqual('new');
        expect(val[0][2][0].text).toEqual('txt');
        expect(val[0][2][0].thumbnail).toEqual('ai');
        expect(val[0][2][0].url).toEqual('/events/123');
        expect(val[0][2][0].noticetype).toEqual('forumpost');
        expect(val[0][2][0].openNewTab).toEqual(false);

        expect(val[0].length).toBe(3);

        done();

      });

    });
  });

  it('PublicForumPost', function(done) {

    var errorCode = R.prop('code');

    var comments = [faker.Lorem.paragraph(), faker.Lorem.paragraph(), faker.Lorem.paragraph(), faker.Lorem.paragraph()];
    var lastpost = R.compose(R.prop("thread"), R.last);
    var lasthash = R.compose(R.prop("hash"), R.last);

    var extractMatch = R.compose(R.nth(1), R.nth(0));
    var extractUsers = R.compose(R.remove(1, 1), R.flatten);

    var liftuser = R.curry((user, x) => R.compose(R.nth(x), extractUsers)(user));

    var joinusers = [1, 2];

    var joinMatch = R.curry((matchkey, user) => matches.JoinMatch(account, match, matchkey, user.email, user.key));

    var approveUser = R.curry((matchkey, email) => matches.UpdateMatchPlayerType(match, matchkey, email, 'approve')); 

    var post = R.curry((matchkey, comment, email, emailkey) => api_matches.PublicForumPost(account, match, email, emailkey, matchkey, comment));

    // fake hosted match
    Promise.all([generateMatch('open', 3, 2), generateUser({}), generateUser({}), generateUser({})]).then(function(m) {

      Promise.all(R.map(joinMatch(extractMatch(m).key), R.map(liftuser(m), joinusers))).then(function(z) {

        approveUser(extractMatch(m).key, liftuser(m, 1).email).then(function(u) { 

          post(extractMatch(m).key, comments[0], liftuser(m, 0).email, liftuser(m, 0).key).then(function(a) {

            expect(lastpost(a).useremail).toBe(liftuser(m, 0).email); 
            expect(lastpost(a).comment).toBe(comments[0]); 
            expect(lastpost(a).name).toBe(liftuser(m, 0).name); 
            expect(moment().diff(lastpost(a).postdate, 'minutes')).toBe(0);
            expect(lastpost(a).avataricon).toBe(liftuser(m, 0).profile.avataricon); 
            expect(lastpost(a).username).toBe(liftuser(m, 0).profile.name); 
            post(extractMatch(m).key, comments[1], liftuser(m, 1).email, liftuser(m, 1).key).then(function(b) {

              expect(lastpost(b).useremail).toBe(liftuser(m, 1).email); 
              expect(lastpost(b).comment).toBe(comments[1]); 
              expect(moment().diff(lastpost(b).postdate, 'minutes')).toBe(0);
              expect(lastpost(b).avataricon).toBe(liftuser(m, 1).profile.avataricon); 
              expect(lastpost(b).username).toBe(liftuser(m, 1).profile.name); 
              post(extractMatch(m).key, comments[2], liftuser(m, 1).email, "bad user key").then().catch(function(err) {

                expect(errorCode(err)).toBe(401); 
                post(extractMatch(m).key, comments[2], liftuser(m, 2).email, liftuser(m, 2).key).then(function(b) {

                  expect(lastpost(b).useremail).toBe(liftuser(m, 2).email); 
                  expect(lastpost(b).comment).toBe(comments[2]); 
                  expect(moment().diff(lastpost(b).postdate, 'minutes')).toBe(0);
                  expect(lastpost(b).avataricon).toBe(liftuser(m, 2).profile.avataricon); 
                  expect(lastpost(b).username).toBe(liftuser(m, 2).profile.name); 
                  post(extractMatch(m).key, comments[3], liftuser(m, 3).email, liftuser(m, 3).key).then(function(b) {

                    expect(lastpost(b).useremail).toBe(liftuser(m, 3).email); 
                    expect(lastpost(b).comment).toBe(comments[3]); 
                    expect(moment().diff(lastpost(b).postdate, 'minutes')).toBe(0);
                    expect(lastpost(b).avataricon).toBe(liftuser(m, 3).profile.avataricon); 
                    expect(lastpost(b).username).toBe(liftuser(m, 3).profile.name); 

                    api_matches.taskFindPostByKey(match, extractMatch(m).key, lastpost(b).useremail, lasthash(b)).fork(
                      (err) => {},
                      (data) => {
                        if (data)
                          api_matches.taskDeletePost(match, extractMatch(m).key, lasthash(b)).fork(
                            err => {}, 
                            function(x) {
                              expect(x.publicforums.length).toBe(3);
                              expect(x.publicforums[2].thread.useremail).toBe(liftuser(m, 2).email); 

                              post('notakey', comments[3], liftuser(m, 3).email, liftuser(m, 3).key).then().catch(function(err) {
                                expect(errorCode(err)).toBe(403); 
                                done();
                              });
                            }
                          )

                      }
                    );
                  });
                });
              });
            });
          });
        });
      });
    });
  });


  describe("api_calls", function() {
    const endpoint = (base, api) => base + api;
    const hostify = zipObj(['host', 'hostkey', 'playerkey', 'type']);
    const zipEmailKeyType = zipObj(['email', 'userkey', 'type']);

    const createJoinPostData = curry(function(url_base, url_api, email, userkey) {
      return ajaxData([endpoint(url_base, url_api), 'POST', zipEmailKeyType([email, userkey, 'request'])]);
    });

    var datify = zipObj(['games', 'title', 'description', 'date', 'timeend', 'maxplayers', 'minplayers', 'email', 'key', 'experience', 'status', 'smoking', 'alcohol', 'playarea', 'playarealng', 'playarealat', 'isprivate']);

    it("api/v1/matches/:match/publicforums POST (success)", function(done) {

      const getUsers = R.compose(R.remove(1, 1), R.flatten);
      const getMatchkey = R.compose(R.prop('key'), R.nth(1), R.flatten);
      const apiEndpoint = (url_api, u, end) => url_api + '/' + getMatchkey(u) + '/' + end;
      const readUserkey = (users, index, keyname) => R.compose(R.prop(keyname), R.nth(index), getUsers)(users); 
      const readUserProfilekey = (users, index, keyname) => R.compose(R.path(['profile', keyname]), R.nth(index), getUsers)(users); 

      const blankPost = (m) => ajaxData([url_base + apiEndpoint(url_api, m, 'publicforums'), 'POST', {email: readUserkey(m, 2, 'email'), userkey: readUserkey(m, 2, 'key')}]);

      const requestJoinUser = (url_base, u, userIndex) => createJoinPostData(url_base, apiEndpoint(url_api, u, 'users'), readUserkey(u, userIndex, 'email'), readUserkey(u, userIndex, 'key'));

      const readPlayerkey = (u, index) => R.composeP(matches.FindPlayerkey(R.__, 'email', readUserkey(u, index, 'email')), R.nth(0), matches.FindMatchByKey(match))(getMatchkey(u));

      const createApprovePostData = curry((url_base, url_api, host, hostkey, playerkey) => ajaxData([url_base + url_api, 'PATCH', hostify([host, hostkey, playerkey, 'approve'])]));

      const forumGetAfterdata = (u, index, hash) => zipObj(['user', 'key', 'after'], [readUserkey(u, index, 'email'), readUserkey(u, index, 'key'), hash]); 

      const forumPostdata = (u, index, discussion) => zipObj(['user', 'key', 'discussion'], [readUserkey(u, index, 'email'), readUserkey(u, index, 'key'), discussion]); 

      const postDiscussion = R.curry((url_base, url_api, u, index, discussion) => ajaxData([url_base + apiEndpoint(url_api, u, 'publicforums'), 'POST', forumPostdata(u, index, discussion)]));

      const readPosts = R.curry((url_base, url_api, u, index, hash) => ajaxData([url_base + apiEndpoint(url_api, u, 'publicforums'), 'GET', forumGetAfterdata(u, index, hash)]));

      const readAllPosts = R.curry((url_base, url_api, u) => ajaxData([url_base + apiEndpoint(url_api, u, 'publicforums'), 'GET', {}]));

      Promise.all([genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2), generateUser({}), generateUser({}), generateUser({})]).then(function(u) {

        const postDiscussionToURL = postDiscussion(url_base, url_api, u);

        const readPostsToURL = readPosts(url_base, url_api, u);
        const prepareUsers = [blankPost(u), requestJoinUser(url_base, u, 1), requestJoinUser(url_base, u, 2)];

        Promise.all(map(ajaxPromise, prepareUsers)).then(function(r) {
          readPlayerkey(u, 1).then(function(pk) {
            ajaxPromise(createApprovePostData(url_base, apiEndpoint(url_api, u, 'users'), readUserkey(u, 0, 'email'), readUserkey(u, 0, 'key'), pk)).then(function(m) {
              ajaxPromise(postDiscussionToURL(0, 'host discussion')).then(function(h) {

                expect(R.has('_id', h.body[0])).toBe(false);
                expect(h.statusCode).toBe(200);
                expect(h.body[0].thread.comment).toBe('host discussion');
                expect(h.body[0].thread.useremail).toBe(readUserkey(u, 0, 'email'));
                expect(h.body[0].thread.username).toBe(readUserProfilekey(u, 0, 'name'));
                expect(h.body[0].thread.avataricon).toBe(readUserProfilekey(u, 0, 'avataricon'));

                ajaxPromise(postDiscussionToURL(1, 'approved discussion should appear')).then(function(a) {
                  expect(a.statusCode).toBe(200);
                  expect(a.body[1].thread.comment).toBe('approved discussion should appear');
                  expect(a.body[1].thread.useremail).toBe(readUserkey(u, 1, 'email'));
                  expect(a.body[1].thread.username).toBe(readUserProfilekey(u, 1, 'name'));
                  expect(a.body[1].thread.avataricon).toBe(readUserProfilekey(u, 1, 'avataricon'));

                  ajaxPromise(postDiscussionToURL(2, 'public discussion appears')).then(function(d) {
                    expect(d.statusCode).toBe(200);
                    const readHash = (reply, thread) => reply.body[thread].hash;
                    Promise.all(map(ajaxPromise, [readPostsToURL(0, ''), readPostsToURL(0, 'badkey'), readPostsToURL(0, readHash(h, 0)), readPostsToURL(2, readHash(h, 0)), readAllPosts(url_base, url_api, u)])).then(function(rqs) {

                      expect(rqs[0].statusCode).toBe(200);
                      expect(rqs[0].body.length).toBe(0);

                      expect(rqs[1].statusCode).toBe(200);
                      expect(rqs[1].body.length).toBe(0);

                      expect(rqs[2].statusCode).toBe(200);
                      expect(rqs[2].body.length).toBe(2);

                      expect(rqs[2].body[0].thread.comment).toBe('approved discussion should appear');
                      expect(rqs[2].body[0].thread.useremail).toBe(readUserkey(u, 1, 'email'));

                      expect(rqs[3].statusCode).toBe(200);
                      expect(rqs[3].body[0].thread.useremail).toBe(readUserkey(u, 1, 'email'));

                      expect(rqs[4].body.length).toBe(3);

                      var requestData = [
                        {
                          url: url_base + url_api + '/' + u[0][1].key + '/publicforums/' + rqs[4].body[2].hash,
                          method: 'DELETE',
                          data: {
                            user: rqs[4].body[2].thread.useremail,
                            key: u[2].key
                          } 
                        },
                        {
                          url: url_base + url_api + '/' + u[0][1].key + '/publicforums/' + rqs[4].body[2].hash,
                          method: 'DELETE',
                          data: {
                            user: rqs[4].body[2].thread.useremail
                          } 
                        },
                        {
                          url: url_base + url_api + '/' + u[0][1].key + '/publicforums/' + 'xhash',
                          method: 'DELETE',
                          data: {
                            user: rqs[4].body[2].thread.useremail,
                            key: u[2].key
                          } 
                        },
                      ];

                      Promise.all(map(ajaxPromise, requestData)).then(function(x) {
                        expect(x[0].body.length).toBe(2);
                        expect(x[0].statusCode).toBe(200);
                        expect(x[1].statusCode).toBe(401);
                        expect(x[2].statusCode).toBe(403);
                        done()
                      });

                    });
                  });
                });
              });
            });
          });
        });
      }); 
    }); 

    it('maybeMatch', function(done) {
      expect(api_matches.MaybeMatch({}).isNothing).toBe(true); 
      expect(api_matches.MaybeMatch({params:{}}).isNothing).toBe(true); 
      expect(api_matches.MaybeMatch({params:{match:'abc'}}).get()).toBe('abc'); 
      done();
    });

    it("close server", function(done) {
      server.close(function() {
        mongo.CloseDatabase();
        done();
      });
    });

  });

});
