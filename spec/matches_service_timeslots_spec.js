var express = require("express"); 
var router = express.Router(); 
var log = require('../app/log.js'); 
var R = require('ramda'); 

var Promise = require('bluebird');
var faker = require('Faker');

var Task    = require('data.task');
var Maybe   = require('data.maybe');
var Either  = require('data.either');

var moment  = require('moment');

var genhelper = require('./genhelper.js');

var match   = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var account = require('../app/models/account.js');
var testBoardgame = require('../app/models/boardgame.js');

var api_matches_timeslot = require('../app/matches_service_timeslots.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var mongo = require('../app/mongoconnect.js');
var dbUri = nconf.get('database:uri');

var httpPort = 3088;

var testapp = express();

var nodemailer = require('nodemailer');
var stubTransport = require('nodemailer-stub-transport');
var transporter = nodemailer.createTransport(stubTransport());

const ajaxData = zipObj(['url', 'method', 'data']);
var ajax = require('./ajax.js');
var ajaxPromise = ajax.AjaxPromise;

var S = require('../app/lambda.js');

// ============== VARS ===============
var url_base = "http://localhost:" + httpPort;
var server;

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  testapp.close(function(){
    console.log('matches_service_spec closed on port ' + httpPort);
  });
};

// url to test
var url_api = '/api/v1/matches';

var generateUser = genhelper.GenerateUser(account);
var generateMatch = genhelper.GenerateFakeHostedMatch(account, testBoardgame, match);


// ============== RUN TESTS ================== 
describe("matches_service_timeslot_spec.js api", function() {
  describe("intialise functions", function() {

    it("setup server", function(done) {
      mongo.ConnectDatabase(dbUri);
      router = api_matches_timeslot.SetupRouter(nconf.get('smtp:user'), transporter, express.Router());
      testapp.use(url_api, router);
      server = testapp.listen(httpPort, function() {
        done();
      });
    });

    it("isPlayerType", function(done) {
      expect(api_matches_timeslot.isPlayerType('host', 'a@xyz.com')({players: [{ email: 'a@xyz.com', type: 'host'}]}).isJust).toBeTruthy();
      expect(api_matches_timeslot.isPlayerType('approve', 'a@xyz.com')({players: [{ email: 'a@xyz.com', type: 'approve'}]}).isJust).toBeTruthy();

      expect(api_matches_timeslot.isPlayerType('approve', 'a@xyz.com')({players: [{ email: 'a@xyz.com', type: 'adf'}]}).isJust).toBeFalsy();
      expect(api_matches_timeslot.isPlayerType('approve', 'a@xyz.com')({players: [{ email: 'who', type: 'approve'}]}).isJust).toBeFalsy();
      expect(api_matches_timeslot.isPlayerType('approve', 'a@xyz.com')({players: [{type: 'approve'}]}).isJust).toBeFalsy();
      done();
    });

    it('removeMatchVotes', function(done) {
      expect(api_matches_timeslot.removeMatchVotes('e', 20)([]).isJust).toBeFalsy();
      expect(api_matches_timeslot.removeMatchVotes('e', 20)([{timeslots:[]}]).isJust).toBeFalsy();
      expect(api_matches_timeslot.removeMatchVotes('e', 20)([{timeslots:[{id:20}]}]).isJust).toBeFalsy();
      expect(api_matches_timeslot.removeMatchVotes('f', 20)([{timeslots:[{id:20, votes:[{ email : 'e', direction : 1 } ]}]}]).get()).toEqual({ $set : { 'timeslots.$.votes' : [ { email : 'e', direction : 1 } ] } });
      expect(api_matches_timeslot.removeMatchVotes('e', 20)([{timeslots:[{id:20, votes:[{ email : 'e', direction : 1 } ]}]}]).get()).toEqual({ $set : { 'timeslots.$.votes' : [  ] } });
      done();
    });

    it('whenMatchHasVote', function(done) {
      expect(api_matches_timeslot.whenMatchHasVote('e', 1)({}).isJust).toBeFalsy();
      expect(api_matches_timeslot.whenMatchHasVote('e', 1)([]).isJust).toBeFalsy();
      expect(api_matches_timeslot.whenMatchHasVote('e', 1)([{timeslots:[]}]).isJust).toBeFalsy();
      expect(api_matches_timeslot.whenMatchHasVote('e', 1)([{timeslots:[{id: 1}]}]).isJust).toBeFalsy();
      expect(api_matches_timeslot.whenMatchHasVote('e', 1)([{timeslots:[{id: 1, votes:[]}]}]).isJust).toBeFalsy();
      expect(api_matches_timeslot.whenMatchHasVote('e', 1)([{timeslots:[{id: 1, votes:[{email:'e'}]}]}]).getOrElse(false)).toBeTruthy();
      done();
    });

    it('pushVote', function(done) {
      expect(api_matches_timeslot.pushVote(1, 'a', 'user', 'avatar', 'key', [])).toEqual([{email:'a', username:'user', avataricon:'avatar', userkey:'key', direction: 1}]);
      expect(api_matches_timeslot.pushVote(2, 'b', 'userb', 'avatarb', 'keyb', [])).toEqual([{email: 'b', username:'userb', avataricon:'avatarb', userkey:'keyb', direction: 2}]);
      done();
    });

    it('addTimeslotVotes', function(done) {
      expect(api_matches_timeslot.addTimeslotVotes('e', 'u', 'a', 'k', 1, 20)([]).isJust).toBeFalsy();
      expect(api_matches_timeslot.addTimeslotVotes('e', 'u', 'a', 'k', 1, 20)([{timeslots:[]}]).isJust).toBeFalsy();
      expect(api_matches_timeslot.addTimeslotVotes('e', 'u', 'a', 'k', 1, 20)([{timeslots:[{id:20}]}]).isJust).toBeFalsy();
      expect(api_matches_timeslot.addTimeslotVotes('e', 'u', 'a', 'k', 1, 20)([{timeslots:[{id:20, votes:[]}]}]).get()).toEqual({ $set : { 'timeslots.$.votes' : [ { email : 'e', username : 'u', avataricon : 'a', userkey : 'k', direction : 1 } ] } });
      done();
    });

    it('findPlayerInMatch', function(done) {
      expect(api_matches_timeslot.findPlayerInMatch('a')({}).isJust).toBeFalsy();
      expect(api_matches_timeslot.findPlayerInMatch('a')([]).isJust).toBeFalsy();
      expect(api_matches_timeslot.findPlayerInMatch('a')([{players: []}]).isJust).toBeFalsy();
      expect(api_matches_timeslot.findPlayerInMatch('a')([{players: [{email: 'b'}]}]).isJust).toBeFalsy();
      expect(api_matches_timeslot.findPlayerInMatch('a')([{players: [{email: 'a'}]}]).isJust).toBeTruthy();
      done();
    });

  });


  describe("database_calls", function() {

    it('readtimeslot', function(done) {
      Promise.all([genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2), generateUser({}), generateUser({}), generateUser({})]).then(function(u) {

        const startdate = moment().add(5, 'days').toDate();
        const enddate   = moment().add(5, 'days').add(3, 'hours').toDate();

        api_matches_timeslot.findOneMatchByKey(match, u[0][1].key).fork(
          err => {},
          data => {
            expect(data.key).toBe(u[0][1].key);

            // read game timeslot
            api_matches_timeslot.readTimeslots(match, u[0][1].key).fork(
              err => {},
              data => {
                expect(data.length).toBe(1);

                // add game timeslot
                api_matches_timeslot.addTimeslots(match, u[0][1].key, startdate, enddate).fork(
                  err => {},
                  data => {
                    expect(data.length).toBe(2);
                    expect(new Date(data[1].startdatetime).getUTCDate()).toBe(startdate.getUTCDate());
                    expect(new Date(data[1].enddatetime).getUTCDate()).toBe(enddate.getUTCDate());
                    expect(data[1].votes).toEqual([]);

                    // add game timeslot
                    api_matches_timeslot.removeTimeslots(match, u[0][1].key, data[1].id).fork(
                      err => {},
                      data => {
                        expect(data.length).toBe(1);
                        done();

                      }
                    );
                  }
                );
              }
            );
          }
        );
      });
    });

    it('voteForTimeslot', function(done) {
      generateMatch('open', 3, 2).then(function(m) {
        api_matches_timeslot.voteForTimeslot(match, m[1].key, m[1].timeslots[0].id, m[0].email, m[0].profile.name, m[0].profile.avataricon, m[0].pageslug, m[0].key).fork(
          err => {},
          data => {
            expect(data[0].votes.length).toBe(1);
            expect(data[0].votes[0].votetype).toBe('available');
            expect(data[0].votes[0].direction).toBe(1);
            expect(data[0].votes[0].email).toBe(m[0].email);

            api_matches_timeslot.unvoteForTimeslot(account, match, m[1].key, m[1].timeslots[0].id, m[0].email, m[0].key).fork(
              err => {},
              data => {
                expect(data[0].votes.length).toBe(0);
                done();

              }
            )
          }
        )
      });
    });

  });


  describe("api_calls", function() {

    it("api/v1/matches/:match/timeslots GET:POST:DELETE", function(done) {
      Promise.all([genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2), generateUser({}), generateUser({}), generateUser({})]).then(function(u) {

        ajaxPromise({ 
          url: 'http://localhost:' + httpPort + '/api/v1/matches/' + u[0][1].key + '/timeslots',
          method: 'GET',
          data:{ }
        }).then(function(bodydata) {
          expect(bodydata.body.length).toBe(1);

          const startdate = moment().add(5, 'days').toDate();
          const enddate   = moment().add(5, 'days').add(3, 'hours').toDate();

          ajaxPromise({ 
            url: 'http://localhost:' + httpPort + '/api/v1/matches/' + u[0][1].key + '/timeslots',
            method: 'POST',
            data:{ 
              email:    u[0][0].email,
              key:      u[0][0].key,
              startdatetime:  startdate,
              enddatetime:    enddate,
            }
          }).then(function(bodydata) {
            expect(bodydata.body.length).toBe(2);
            expect(new Date(bodydata.body[1].startdatetime).getUTCDate()).toBe(startdate.getUTCDate());
            expect(new Date(bodydata.body[1].enddatetime).getUTCDate()).toBe(enddate.getUTCDate());
            expect(bodydata.body[1].votes).toEqual([]);

            ajaxPromise({ 
              url: 'http://localhost:' + httpPort + '/api/v1/matches/' + u[0][1].key + '/timeslots',
              method: 'POST',
              data:{ 
                email:    'bademail',
                key:      u[0][0].key,
                startdatetime:  startdate,
                enddatetime:    enddate,
              }
            }).then(function(faildata) {
              expect(faildata.statusCode).toBe(404);
              ajaxPromise({ 
                url: 'http://localhost:' + httpPort + '/api/v1/matches/' + u[0][1].key + '/timeslots',
                method: 'POST',
                data:{ 
                  email:    u[0][0].email,
                  key:      'badkey',
                  startdatetime:  startdate,
                  enddatetime:    enddate,
                }
              }).then(function(faildata) {
                expect(faildata.statusCode).toBe(404);
                ajaxPromise({ 
                  url: 'http://localhost:' + httpPort + '/api/v1/matches/' + u[0][1].key + '/timeslots',
                  method: 'POST',
                  data:{ 
                    email:    u[0][0].email,
                    key:      u[0][0].key,
                    enddatetime:    enddate,
                  }
                }).then(function(faildata) {
                  expect(faildata.statusCode).toBe(500);
                  ajaxPromise({ 
                    url: 'http://localhost:' + httpPort + '/api/v1/matches/' + u[0][1].key + '/timeslots/' + bodydata.body[1].id,
                    method: 'DELETE',
                    data:{ 
                      email:    u[0][0].email,
                      key:      u[0][0].key
                    }
                  }).then(function(deletedata) {
                    expect(deletedata.body.length).toBe(1);
                    done();
                  })

                })
              })
            })
          });
        });
      });
    });

    it("match votes POST", function(done) {
      genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2)
        .then((x) => {

          const ajaxVote = curry((method, matchkey, timeslotid, data) => ajaxPromise({
            url: url_base + url_api + '/' + matchkey + '/timeslots/' + timeslotid + '/votes',
            method: method,
            data: data 
          }));

          const ajaxVoteMatch = ajaxVote(
            'POST',
            x[1].key, 
            x[1].timeslots[0].id
          );

          const ajaxUnvoteMatch = ajaxVote(
            'DELETE',
            x[1].key, 
            x[1].timeslots[0].id
          );

          Promise.all([
            ajaxVoteMatch({}),
            ajaxVoteMatch({email: x[0].email}),
            ajaxVoteMatch({
              email: x[0].email, 
              key: x[0].key
            }),
            ajaxVoteMatch({email: x[0].email, key: 'badkey'})

          ]).then((z) => {
            expect(z[1].statusCode).toBe(500);
            expect(z[2].statusCode).toBe(200);
            expect(z[2].body.length).toBe(1);

            expect(R.has('email', z[2].body[0].votes[0])).toBe(false);
            expect(z[2].body[0].votes[0].username).toBe(x[0].profile.name);
            expect(z[2].body[0].votes[0].avataricon).toBe(x[0].profile.avataricon);
            expect(z[2].body[0].votes[0].userkey).toBe(x[0].pageslug);

            expect(z[2].body[0].votes[0].direction).toBe(1);
            expect(z[2].body[0].votes[0].votetype).toBe('available');
            expect(z[3].statusCode).toBe(404);

            ajaxVoteMatch({email: x[0].email, key: x[0].key}).then((y) => {
              expect(y.statusCode).toBe(409);

              Promise.all([
                ajaxUnvoteMatch({}),
                ajaxUnvoteMatch({email: x[0].email}),
                ajaxUnvoteMatch({email: x[0].email, key: x[0].key}),
                ajaxUnvoteMatch({email: x[0].email, key: 'badkey'})
              ]).then((a) => {
                expect(a[0].statusCode).toBe(500);
                expect(a[1].statusCode).toBe(500);
                expect(a[2].statusCode).toBe(200);
                expect(a[2].body[0].votes.length).toBe(0);
                expect(a[3].statusCode).toBe(401);
                done();
              });

            });
          })
          .catch((err) => {
            log.clos('errzzz', err);
          });

        });
    });

  });

  describe("tidy up", function() {

    it("close server", function(done) {
      server.close(function() {
        mongo.CloseDatabase();
        done();
      });
    });

  });
});
