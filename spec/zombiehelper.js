var exports = module.exports = {};

var R = require('ramda');
var Promise = require('promise');
var log = require('../app/log.js');

// Zombie -> String -> String -> (Promise -> String)
var promiseZombieVisit = R.curry(function(zbrowser, html_part, url) {
	return new Promise(function(fulfill, reject) {
		zbrowser.visit(url, function(err) {
			(zbrowser.success) ? fulfill(zbrowser.html(html_part)) : reject(err);
		});
	});
});
exports.PromiseZombieVisit = promiseZombieVisit;


// Zombie -> String -> String -> (Promise -> String)
var promisifyZombie = R.curry(function(zbrowser, url) {
	return new Promise(function(fulfill, reject) {
		zbrowser.visit(url, function(err) {
			(zbrowser.success) ? fulfill(zbrowser) : reject(err);
		});
	});
});
exports.PromisifyZombie = promisifyZombie;
