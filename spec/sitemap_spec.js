var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var mongo = require('../app/mongoconnect.js');
var sitemap = require('../app/sitemap.js');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');

var Browser = require("zombie");
var browser = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var testapp = express();
var httpPort = 3037;

var base_url = "http://localhost:" + httpPort + "/";

var log = require('../app/log.js');

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  testapp.close(function(){
    console.log('sitemap.xml closed on port ' + httpPort);
  });
};


describe("sitemap_spec", function() {

  describe("web page tests", function() {

    var server;

    it("setup server", function(done) {
      mongo.ConnectDatabase(dbUri);
      done();
    });

    it("setup server", function(done) {
      sitemap.RouteSiteMap(testapp);
      server = testapp.listen(httpPort, function() {
        done();
      });
    });

    it('check sitemap.xml', function(done) {
      browser.visit(base_url + "sitemap.xml", function(err) {
        expect(browser.success).toBe(true);
        expect(browser.html()).toContain('http://www.sitemaps.org/schemas/sitemap/0.9');
        done();
      });
    });

    it('close server', function(done) {
      server.close();
      done();
    });

  });


  describe("read basic data", function() {

    it("buildURL", function(done) {
      expect(sitemap.buildURL('a')).toBe('/events/a');
      expect(sitemap.buildURL('z')).toBe('/events/z');
      done();
    });

    it("buildSeriesURL", function(done) {
      expect(sitemap.buildSeriesURL('a')).toBe('/events/series/a');
      expect(sitemap.buildSeriesURL('z')).toBe('/events/series/z');
      done();
    });

    it("maybeBuildEitherURL", function(done) {
      expect(sitemap.maybeBuildEitherURL({updatedAt:'2017-10-04', key:'a', status:'open', seriesid:'dcf'}).get()).toEqual('/events/a');
      expect(sitemap.maybeBuildEitherURL({updatedAt:'2017-09-04', key:'e', status:'open', seriesid:'abc'}).get()).toEqual('/events/e');
      expect(sitemap.maybeBuildEitherURL({updatedAt:'2017-09-04', key:'e', status:'open', reoccur:{enable:true}, seriesid:'abc'}).get()).toEqual('/events/series/abc');
      expect(sitemap.maybeBuildEitherURL({updatedAt:'2017-09-04', key:'e', status:'open', reoccur:{enable:true}, seriesid:'bcd'}).get()).toEqual('/events/series/bcd');
      done();
    });


    it("buildGetKey", function(done) {
      expect(sitemap.buildGetKey({key:'a'}).get()).toEqual({'/events/a':['get']});
      expect(sitemap.buildGetKey({key:'z'}).get()).toEqual({'/events/z':['get']});
      expect(sitemap.buildGetKey({reoccur:{enable:true},seriesid:'a'}).get()).toEqual({'/events/series/a':['get']});
      expect(sitemap.buildGetKey({reoccur:{enable:true},seriesid:'z'}).get()).toEqual({'/events/series/z':['get']});
      done();
    });

    it("buildKeyUrls", function(done) {
      expect(sitemap.buildKeyUrls({a:'b'})([{updatedAt:'2017-10-04', key:'a', status:'open', series:'dcf'}]).get()).toEqual({ map : { a : 'b', '/events/a' : [ 'get' ] } });
      expect(sitemap.buildKeyUrls({c:'d'})([{updatedAt:'2017-09-04', key:'e', status:'open', seriesid:'abc'}]).get()).toEqual({ map : { c : 'd', '/events/e' : [ 'get' ] } });
      expect(sitemap.buildKeyUrls({c:'e'})([{updatedAt:'2017-09-04', key:'e', status:'open', reoccur:{enable:true}, seriesid:'abc'}]).get()).toEqual({ map : { c : 'e', '/events/series/abc' : [ 'get' ] } });
      expect(sitemap.buildKeyUrls({c:'f'})([{updatedAt:'2017-09-04', key:'e', status:'open', reoccur:{enable:true}, seriesid:'bcd'}]).get()).toEqual({ map : { c : 'f', '/events/series/bcd' : [ 'get' ] } });
      done();
    });

    it("buildSiteRoute", function(done) {
      expect(sitemap.buildSiteRoute('2017-12-25', 'open', 'a')).toEqual({ a : { lastmod : '2017-12-25', changefreq : 'hourly', priority : 1 } });
      expect(sitemap.buildSiteRoute('2015-11-24', 'full', 'b')).toEqual({ b : { lastmod : '2015-11-24', changefreq : 'daily', priority : 0.8 } });
      expect(sitemap.buildSiteRoute('2015-11-24', 'played', 'c')).toEqual({ c : { lastmod : '2015-11-24', changefreq : 'monthly', priority : 0.4 } });
      done();
    });

    it("buildSite", function(done) {
      expect(sitemap.buildSite('2017-12-25', 'open', 'a')).toEqual({ '/events/a' : { lastmod : '2017-12-25', changefreq : 'hourly', priority : 1 } });
      expect(sitemap.buildSite('2015-11-24', 'full', 'b')).toEqual({ '/events/b' : { lastmod : '2015-11-24', changefreq : 'daily', priority : 0.8 } });
      expect(sitemap.buildSite('2015-11-24', 'played', 'c')).toEqual( { '/events/c' : { lastmod : '2015-11-24', changefreq : 'monthly', priority : 0.4 } });
      done();
    });

    it("buildSiteSeries", function(done) {
      expect(sitemap.buildSiteSeries('2017-12-25', 'open', 'a')).toEqual({ '/events/series/a' : { lastmod : '2017-12-25', changefreq : 'hourly', priority : 1 } });
      expect(sitemap.buildSiteSeries('2015-11-24', 'full', 'b')).toEqual({ '/events/series/b' : { lastmod : '2015-11-24', changefreq : 'daily', priority : 0.8 } });
      expect(sitemap.buildSiteSeries('2015-11-24', 'played', 'c')).toEqual( { '/events/series/c' : { lastmod : '2015-11-24', changefreq : 'monthly', priority : 0.4 } });
      done();
    });


    it("buildRouteKey", function(done) {
      expect(sitemap.buildRouteKey({updatedAt:'2017-10-04', key:'a', status:'open'}).get()).toEqual({ '/events/a' : { lastmod : '2017-10-04', changefreq : 'hourly', priority : 1 } });
      expect(sitemap.buildRouteKey({updatedAt:'2017-09-04', key:'e', status:'played'}).get()).toEqual({ '/events/e' : { lastmod : '2017-09-04', changefreq : 'monthly', priority : 0.4 } });
      expect(sitemap.buildRouteKey({updatedAt:'2017-09-04', key:'e', status:'played', reoccur:{enable: true}, seriesid:'x'}).get()).toEqual({ '/events/series/x' : { lastmod : '2017-09-04', changefreq : 'monthly', priority : 0.4 } });
      expect(sitemap.buildRouteKey({updatedAt:'2017-09-04', key:'e', status:'played', reoccur:{enable: true}, seriesid:'y'}).get()).toEqual({ '/events/series/y' : { lastmod : '2017-09-04', changefreq : 'monthly', priority : 0.4 } });
      done();
    });

    it("buildRouteUrls", function(done) {
      expect(sitemap.buildRouteUrls([{updatedAt:'2017-10-04', key:'a', status:'open'}]).get()).toEqual({ route : { '/events/a' : { lastmod : '2017-10-04', changefreq : 'hourly', priority : 1 } } });
      expect(sitemap.buildRouteUrls([{updatedAt:'2017-09-04', key:'e', status:'played'}]).get()).toEqual({ route : { '/events/e' : { lastmod : '2017-09-04', changefreq : 'monthly', priority : 0.4 } } });
      expect(sitemap.buildRouteUrls([{updatedAt:'2017-09-04', key:'e', status:'played', seriesid: 'hat', reoccur:{enable:true}}]).get()).toEqual({ route : { '/events/series/hat' : { lastmod : '2017-09-04', changefreq : 'monthly', priority : 0.4 } } });
      expect(sitemap.buildRouteUrls([{updatedAt:'2017-09-04', key:'a', status:'played', seriesid: 'dat', reoccur:{enable:true}}]).get()).toEqual({ route : { '/events/series/dat' : { lastmod : '2017-09-04', changefreq : 'monthly', priority : 0.4 } } });
      done();
    });

    it("buildSiteGet", function(done) {
      expect(sitemap.buildSiteGet('a')).toEqual({a: ['get']});
      expect(sitemap.buildSiteGet('z')).toEqual({z: ['get']});
      done();
    });

    it("close server", function(done) {
      mongo.CloseDatabase();
      done();
    });

  });
});
