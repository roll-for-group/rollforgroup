var Promise = require('bluebird');
var R = require('ramda');
var compose = R.compose, 
    flatten = R.flatten,
    head    = R.head,
    last    = R.last,
    map     = R.map,
    nth     = R.nth,
    path    = R.path,
    prop    = R.prop;
var util = require('util');
var log = require('../app/log.js');

var faker = require('Faker');
var moment = require('moment');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');

var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;

var testAccount = require('../app/models/account.js');
var testAccounts = require('../app/models/accounts.js');

var transaction = require('../app/models/transaction.js');
var transactions = require('../app/models/transactions.js');

var genhelper = require('./genhelper.js');

var currentyear = new Date().getFullYear();

mongo.ConnectDatabase(dbUri);


describe('transactions_spec.js', function() {

    describe('purchaseTicket', function() {
        it('PurchaseTickets', function(done) {
            genhelper.GenerateUser(testAccount, {}).then(function(u) {
                var item = "20 ticket pack";
                var tickets = 20;
                var paymethod = "paypal";
                var payamount = 55.40;
                var paycurrency = "AUD";
                var payreference = "TRANS0234021";
                var promotion = "test.promotion";
                var coupon  = "lsafsljltestcoupon";

                transactions.Purchase(transaction, u.email, item, tickets, paymethod, paycurrency, payamount, payreference, promotion, coupon).then(function(newtl) { 

                    expect(R.whereEq({
                        purchaser:  u.email,
                        to:         u.email,
                        item:       item,
                        tickets:    tickets,
                        promotion:  promotion,
                        coupon:     coupon
                    }, newtl)).toBe(true);

                    expect(R.whereEq({
                        method: paymethod,
                        currency: paycurrency,
                        credit: payamount,
                        reference: payreference
                    }, newtl.payment)).toBe(true);

                    done();
                });
            });
        });
    });


    describe('refundTicket', function() {
        it('RefundTickets', function(done) {
            genhelper.GenerateUser(testAccount, {}).then(function(u) {
                var item = "20 ticket pack";
                var tickets = 20;
                var paymethod = "paypal";
                var payamount = 55.40;
                var paycurrency = "AUD";
                var payreference = "TRANS0234021";
                var promotion = "test.promotion";
                var coupon  = "lsafsljltestcoupon";

                transactions.Refund(transaction, u.email, item, tickets, paymethod, paycurrency, payamount, payreference, promotion, coupon).then(function(newtl) { 

                    expect(R.whereEq({
                        purchaser:  u.email,
                        to:         u.email,
                        item:       item,
                        tickets:    tickets,
                        promotion:  promotion,
                        coupon:     coupon
                    }, newtl)).toBe(true);

                    expect(R.whereEq({
                        method: paymethod,
                        currency: paycurrency,
                        debit: payamount,
                        reference: payreference
                    }, newtl.payment)).toBe(true);

                    done();
                });
            });
        });
    });

});

describe('close server', function() {
    it('connection close', function(done) {
        mongo.CloseDatabase();
        done();
    });
});
