var Promise = require('bluebird');
var R = require('ramda');

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task');

var moment = require('moment');

var genhelper = require('./genhelper.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');

var conversation = require('../app/models/conversation.js');
var conversations = require('../app/models/conversations.js');

mongo.ConnectDatabase(dbUri);


describe('close server', function() {
    it('connection close', function(done) {
        mongo.CloseDatabase();
        done();
    });
});
