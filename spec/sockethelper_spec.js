var log = require('../app/log.js');
var R = require('ramda');

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task'); 
var Promise = require('bluebird');

var sh = require('../app/sockethelper.js');


describe("sockethelper.js", function() {
    it("Remove User", function(done) {
        expect(sh.RemoveUser([], '')).toEqual([]);
        expect(sh.RemoveUser([{socketid: 'abc'}], 'abc')).toEqual([]);
        expect(sh.RemoveUser([{socketid: 'abc'}, {socketid:'def'}], 'def')).toEqual([{socketid:'abc'}]);
        done();
    });

    it("BuildUser", function(done) {
        expect(sh.BuildUser('s', 'a', 'n', 'p', 's', 'l', 'c1', 'c2')).toEqual({ socketid : 's', name : 'l', avataricon : 'a', playlevel : 'c1', pageslug : 'c2', imgclass : 'n', textclass : 'p', link : 's' });
        done();
    });

    it("SafeBuildUser", function(done) {
        expect(sh.SafeBuildUser('1', {profile:{name:'n', playlevel:3}, pageslug:'abc'}).get()).toEqual({ socketid : '1', name : 'n', avataricon : '/img/default-avatar-sq.jpg', playlevel : 3, pageslug : 'abc', imgclass : 'img-circle imglistcircle-standard-thin user-players-avatar vutips', textclass : 'a-name-standard', link : '/users?id=abc' });
        expect(sh.SafeBuildUser('5', {profile:{name:'n', playlevel:3, avataricon:'ai'}, pageslug:'abc'}).get()).toEqual({ socketid : '5', name : 'n', avataricon : 'ai', playlevel : 3, pageslug : 'abc', imgclass : 'img-circle imglistcircle-standard-thin user-players-avatar vutips', textclass : 'a-name-standard', link : '/users?id=abc' });
        done();
    });

    it("UniqueUserList", function(done) {
        expect(sh.UniqueUserList([{}])).toEqual([{}]);
        expect(sh.UniqueUserList([{pageslug:'x'}])).toEqual([{pageslug:'x'}]);
        expect(sh.UniqueUserList([{pageslug:'x', pageslug:'y'}])).toEqual([{pageslug:'x', pageslug:'y'}]);
        expect(sh.UniqueUserList([{pageslug:'x', pageslug:'x'}])).toEqual([{pageslug:'x'}]);
        expect(sh.UniqueUserList([{pageslug:'x', pageslug:'x', pageslug:'y'}])).toEqual([{pageslug:'x', pageslug:'y'}]);
        done();
    });

    it("BuildChatAdminObj", function(done) {
        expect(sh.BuildChatAdminObj([])).toEqual({users:[], playercount:0});
        expect(sh.BuildChatAdminObj([{a:'b'}])).toEqual({users:[{a:'b'}], playercount:1});
        expect(sh.BuildChatAdminObj([{a:'b'}, {a:'c'}])).toEqual({users:[{a:'b'}, {a:'c'}], playercount:2});
        done();
    });

    it("BuildMsgPayLoad", function(done) {

        expect(R.omit(['thread'], sh.BuildMsgPayload({}, 'd'))).toEqual({ avataricon : '/img/default-avatar-sq.jpg', imgclass : 'img-circle imglistcircle-standard-thin user-chat-avatar vutips', textclass : 'a-name-standard', link : '' } );
        expect(R.omit(['postdate'], sh.BuildMsgPayload({}, 'd').thread)).toEqual({ username : 'No Name Set', comment : 'd' });
        expect(R.omit(['postdate'], sh.BuildMsgPayload({profile:{}}, 'd').thread)).toEqual({ username : 'No Name Set', comment : 'd' });
        expect(R.omit(['postdate'], sh.BuildMsgPayload({profile:{name:'abc'}}, 'd').thread)).toEqual({ username : 'abc', comment : 'd' });

        expect(R.omit(['thread'], sh.BuildMsgPayload({profile:{avataricon:'ai'}}, 'd'))).toEqual({ avataricon : 'ai', imgclass : 'img-circle imglistcircle-standard-thin user-chat-avatar vutips', textclass : 'a-name-standard', link : '' } );

        expect(R.omit(['thread'], sh.BuildMsgPayload({premium:{isactive:true}, profile:{avataricon:'ai'}}, 'd'))).toEqual({ avataricon : 'ai', imgclass : 'img-circle imglistcircle-premium-thin user-chat-avatar vutips', textclass : 'a-name-premium', link : '' } );

        expect(R.omit(['thread'], sh.BuildMsgPayload({premium:{isstaff:true}, profile:{avataricon:'ai'}}, 'd'))).toEqual({ avataricon : 'ai', imgclass : 'img-circle imglistcircle-staff-thin user-chat-avatar vutips', textclass : 'a-name-staff', link : '' } );

        done();
    });

});


