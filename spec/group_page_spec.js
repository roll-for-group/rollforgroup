var Promise = require('bluebird');
var R = require('ramda');
var log = require('../app/log.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var passport = require('passport');
var mongo = require('../app/mongoconnect.js');

var login_page = require('../app/login_page.js');
var group_page = require('../app/group_page.js');
var login = require('connect-ensure-login');

var faker = require('Faker');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var groups = require('../app/models/groups.js');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');

var Browser = require("zombie");
var browser = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var testapp = express();
var httpPort = 3039;

var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);

// verifyregister :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
var verifyregister = function(account, zombieb, email, pass, userobj) {
  return new Promise(function(fulfill, reject) {

    accounts.DeleteAccount(account, email).then(function(y) {

      var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

      register(email, pass).then(function(x) {
        account.update({email: email}, userobj).exec().then(function(z) {
          zombieb.visit(base_url + "login", function(err) {

            if (err) 
              reject(err);

            zombieb.fill('input[name="form-username"]', email)
            zombieb.fill('input[name="form-password"]', pass)
            zombieb.pressButton('login', function() {
              fulfill(x);
            });
          });
        });
      });
    });

  })
};


describe("group_page_spec", function() {

  describe("functions", function() {
    it("prepareGroup", function(done) {
      expect(group_page.prepareGroup()).toEqual( { group : { css : [ { csshref : '/css/group.css' } ] } });
      expect(group_page.prepareGroup({a:'x', name:'n', url:'u', description:'d', members:'m'})).toEqual({ group : { css : [ { csshref : '/css/group.css' } ], name : 'n', url : 'u', description : 'd', members : 'm' } });
      done();
    });
  });

  describe("web page tests", function() {

    var server;

    it("setup server", function(done) {
      mongo.ConnectDatabase(dbUri);
      done();
    });

    it("setup server", function(done) {

      login_page.RouteLogins(testapp, passport);
      login_page.PostLogin(testapp, passport);
      login_page.RouteVerify(testapp, account);

      group_page.routeGroup(testapp, login);

      server = testapp.listen(httpPort, function() {
        done();
      });
    });
    
		it("valid user visit /groups", function(done) {

      var userEmail = faker.Internet.email();
      var userPass = 'testpassword';

      var userobj = {
        profile: {
          searchradius: 10,
          localarea: {
            name: 'area',
            loc: {
              coordinates: [20, 20],
              type: 'Point'
            }
          }
        }
      };

      verifyregister(account, browser, userEmail, userPass, userobj).then(function(x) {

        const groupDetails = {
          name: 'groupDetails',
          description: 'group_description',
          playarea: 'group_description',
          playarealng: 20,
          playarealat: 20,
          loc: {
            coordinates: [20, 20],
            type: 'Point'
          }
        };

        groups.createGroup(groupDetails).then((x) => {
          browser.visit(base_url + 'groups/' + x.url, function(err) {

            expect(browser.success).toBe(true);
            browser.visit(base_url + 'groups/abc', function(err) {
              expect(browser.success).toBe(false);
              done();
            });

          });

        })
      });
    });

    it('close server', function(done) {
      server.close();
      done();
    });

    it('db close', function(done) {
        mongo.CloseDatabase();
        done();
    });

  });
});
