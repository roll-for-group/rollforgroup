var S   = require('../app/lambda.js');
var log = require('../app/log.js');

var maybe     = require('data.maybe');
var either    = require('data.either');
var Task      = require('data.task');

describe('rs-online', function() {
  describe('lambda.js', function() {

    it('taskFail', function(done) {
      S.taskFail.fork(
        err   => {
          done();
        },
        data  => log.clos('err.taskFail')
      );
    });

    it('prop', function(done) {
      expect(S.prop('a', {a: 'b'}).isJust).toBe(true);
      expect(S.prop('a', {a: 'b'}).get()).toBe('b');
      expect(S.prop('a', {a: undefined}).isNothing).toBe(true);
      expect(S.prop('a', {c: 'b'}).isNothing).toBe(true);
      expect(S.prop('a', 'b').isNothing).toBe(true);
      done();
    });

    it('propDefaultTo', function(done) {
      expect(S.propDefaultTo('c', 'a', {a: 'b'}).isJust).toBe(true);
      expect(S.propDefaultTo('c', 'a', {a: 'b'}).get()).toBe('b');
      expect(S.propDefaultTo('c', 'a', {a: undefined}).get()).toBe('c');
      expect(S.propDefaultTo('c', 'a', {c: 'b'}).get()).toBe('c');
      expect(S.propDefaultTo('c', 'a', 'b').get()).toBe('c');
      done();
    });

    it('path', function(done) {
      expect(S.path(['a'], {a: 'b'}).isJust).toBe(true);
      expect(S.path(['a'], {a: 'b'}).get()).toBe('b');
      expect(S.prop(['a'], {a: undefined}).isNothing).toBe(true);
      expect(S.path(['a'], {c: 'b'}).isNothing).toBe(true);
      expect(S.prop(['a'], 'b').isNothing).toBe(true);
      expect(S.prop([], {a:'b'}).isNothing).toBe(true);
      done();
    });

    it('pathDefaultTo', function(done) {
      expect(S.pathDefaultTo('c', ['a'], {a: 'b'}).isJust).toBe(true);
      expect(S.pathDefaultTo('c', ['a'], {a: 'b'}).get()).toBe('b');
      expect(S.pathDefaultTo('c', ['a'], {a: undefined}).get()).toBe('c');
      expect(S.pathDefaultTo('c', ['a'], {c: 'b'}).get()).toBe('c');
      expect(S.pathDefaultTo('c', ['a'], 'b').get()).toBe('c');
      expect(S.pathDefaultTo('c', [], {a:'b'}).get()).toBe('c');
      done();
    });



    it('whenPropEq', function(done) {
      expect(S.whenPropEq('s', 'a', {}).isJust).toBe(false);
      expect(S.whenPropEq('s', 'a', {'s': 'a'}).get()).toEqual({'s': 'a'});
      expect(S.whenPropEq('s', 'a', {'s': 'b'}).isJust).toBe(false);
      expect(S.whenPropEq('s', 'a', {'b': 'a'}).isJust).toBe(false);
      done();
    });

    it('pathEq', function(done) {
      expect(S.pathEq(['s'], 'a', {}).isJust).toBe(false);
      expect(S.pathEq(['s'], 'a', {'s': 'a'}).get()).toEqual({'s': 'a'});
      expect(S.pathEq(['s'], 'a', {'s': 'b'}).isJust).toBe(false);
      expect(S.pathEq(['s'], 'a', {'b': 'a'}).isJust).toBe(false);
      done();
    });

    it('filter', function(done) {
      const whenEven = (n) => n % 2 === 0;
      expect(S.filter(whenEven, []).isNothing).toEqual(true);
      expect(S.filter(whenEven, [1, 3]).isNothing).toEqual(true);
      expect(S.filter(whenEven, [0, 1, 2, 3]).get()).toEqual([0, 2]);
      done();
    });

    it('find', function(done) {
      const whenEven = (n) => n % 2 === 0;
      expect(S.find(whenEven, []).isNothing).toEqual(true);
      expect(S.find(whenEven, [1, 3]).isNothing).toEqual(true);
      expect(S.find(whenEven, [0, 1, 2, 3]).get()).toEqual(0);
      done();
    });

    it('head', function(done) {
      expect(S.head(['a', 'b']).get()).toBe('a');
      expect(S.head([]).isNothing).toBe(true);
      done();
    });

    it('last', function(done) {
      expect(S.last(['a', 'b']).get()).toBe('b');
      expect(S.last([]).isNothing).toBe(true);
      done();
    });

    it('nth', function(done) {
      expect(S.nth(0, ['a', 'b']).get()).toBe('a');
      expect(S.nth(1, ['a', 'b']).get()).toBe('b');
      expect(S.nth(2, ['a', 'b']).isNothing).toBe(true);
      expect(S.nth(0, []).isNothing).toBe(true);
      done();
    });

    it('whichever', function(done) {
      expect(S.whichever(maybe.Nothing(), maybe.Nothing()).isJust).toBe(false);
      expect(S.whichever(maybe.of('a'), maybe.Nothing()).get()).toBe('a');
      expect(S.whichever(maybe.Nothing(), maybe.of('b')).get()).toBe('b');
      expect(S.whichever(maybe.of('a'), maybe.of('a')).isJust).toBe(true);
      done();
    });

    it('has', function(done) {
      expect(S.has('a', {a:'x', b:'y'}).get()).toBe(true);
      expect(S.has('z', {a:'x', b:'y'}).isNothing).toBe(true);
      done();
    });

    it('gt', function(done) {
      expect(S.gt(8, 4).get()).toBe(true);
      expect(S.gt(4, 4).isNothing).toBe(true);
      expect(S.gt(4, 8).isNothing).toBe(true);
      done();
    });

    it('equals', function(done) {
      expect(S.equals('a', 'a').get()).toBe('a');
      expect(S.equals('a', 'b').isJust).toBeFalsy();
      done();
    });

  });
});
