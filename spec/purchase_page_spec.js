var Promise = require('bluebird');
var R = require('ramda');
var util = require('util')
var log = require('../app/log.js');

var passport = require('passport');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var purchase_page = require('../app/purchase_page.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var match = require('../app/models/match.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');

var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 3817;
var login = require('connect-ensure-login');

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  testapp.close(function(){
    console.log('matches_service_spec closed on port ' + httpPort);
  });
};

var server;

// ============== RUN TESTS ================== 
describe("purchase_page_spec.js", function() {

  // loginUser :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
  var verifyregister = function(account, zombieb, email, pass, userobj) {
    return new Promise(function(fulfill, reject) {
      accounts.DeleteAccount(account, email).then(function(y) {
        var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

        register(email, pass).then(function(x) {
          account.update({email: email}, userobj).exec().then(function(z) {
            zombieb.visit(base_url + "login", function(err) {
              if (err) 
                reject(err);

              zombieb.fill('input[name="form-username"]', email)
              zombieb.fill('input[name="form-password"]', pass)
              zombieb.pressButton('login', function() {
                fulfill(x);
              });
            });
          });
        });
      });
    });
  };

  describe("web page tests", function() {

    var userEmail = faker.Internet.email();
    var userPass = 'testpassword';

    it("setup server", function(done) {
      testapp.get('/', function (req, res) {
        res.send('<html>Home Page</html>')
      });

      purchase_page.RoutePurchase(testapp, login);
      login_page.RouteLogins(testapp, passport);
      login_page.PostLogin(testapp, passport);
      login_page.RouteVerify(testapp, account);

      server = testapp.listen(httpPort, function() {
        done();
      });
    }, 10000);

    it("non logged in user visit hostgame", function(done) {
      page = 'purchase';
      browser.visit(base_url + page, function(err) {
        expect(browser.success).toBe(true);
        expect(browser.html()).toContain('Roll for Group');
        done();
      });
    }, 10000);

    it("user login", function(done) {
      verifyregister(account, browser, userEmail, userPass, {} ).then(function(x) {
        done();
      });
    });

    it("valid user visit /purchase", function(done) {
      browser.visit(base_url + 'purchase', function(err) {
        expect(browser.success).toBe(true);
        expect(browser.html('.form-labels')).toContain('You have 5 tickets.');
        done();
      });
    });

    it("press purchase fail", function(done) {
      browser.pressButton('#purchase', function(err) {
        expect(browser.success).toBe(true);
        expect(browser.html('.form-labels')).toContain('You have 5 tickets.');
        done();
      });
    });

    it("press purchase succeed", function(done) {
      browser.fill('#like', 'Test like');
      browser.fill('#dislike', 'Test dislike');
      browser.pressButton('#purchase', function(err) {
        expect(browser.success).toBe(true);
        expect(browser.html('.modal-title')).toContain('Purchase Successful');
        done();
      });
    });
  });
});


describe("purchase_page_spec.js", function() {

  it('server close', function(done) {
    server.close(function() {
      done();
    });
  });

  it('db close', function(done) {
    mongo.CloseDatabase();
    done();
  });

});

