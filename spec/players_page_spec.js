var Promise = require('bluebird');
var R = require('ramda');
var util = require('util');
var log = require('../app/log.js');
var path = require('path');

var passport = require('passport');
var genhelper = require('./genhelper.js');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var players_page = require('../app/players_page.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var browser2 = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');
var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');
var boardgame = require('../app/models/boardgame.js');
var boardgames = require('../app/models/boardgames.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');
var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 3091;
var login = require('connect-ensure-login');

// String -> (Promise -> String)
var promisifyZombie = zombieHelper.PromisifyZombie();
var zombieGetBody = zombieHelper.PromiseZombieVisit(browser, "body");

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);
players_page.RoutePlayerList(testapp, login, account);

var generateUser = genhelper.GenerateUser(account);
var generateHostUser = genhelper.GenerateHostUser(account);

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
    testapp.close(function(){
        console.log('matches_service_spec closed on port ' + httpPort);
    });
};

// ============== RUN TESTS ================== 
describe("players_page_spec.js api", function() {

    var server;

    // verifyregister :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
    var verifyregister = function(account, zombieb, email, pass, userobj) {
      return new Promise(function(fulfill, reject) {

        accounts.DeleteAccount(account, email).then(function(y) {
          var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

          register(email, pass).then(function(x) {
            account.update({email: email}, userobj).exec().then(function(z) {
              zombieb.visit(base_url + "login", function(err) {
                if (err) 
                  reject(err);

                zombieb.fill('input[name="form-username"]', email)
                zombieb.fill('input[name="form-password"]', pass)
                zombieb.pressButton('login', function() {
                  fulfill(x);
                });
              });
            });
          });
        });

      })
    };

    describe("unit tests", function() {
        it("maybeDistance", function(done) {
            const testObj = {
                user: {
                    profile: {
                        searchradius: 90
                    }
                }
            };
            expect(players_page.MaybeDistance(testObj)).toEqual(90000);
            expect(players_page.MaybeDistance({})).toEqual(50000);
            done();
        });

        it("CheckVisibility", function(done) {
            expect(players_page.CheckVisibility({a:'b'}, 1)).toEqual({a:'b'});
            expect(players_page.CheckVisibility({a:'b'}, 2)).toEqual({a:'b', novisibility:true});
            done();
        });

        it("ShowWarning", function(done) {
            expect(players_page.ShowWarning({user:{profile: {visibility: 1}}}, {a:'b'})).toEqual({a:'b'});
            expect(players_page.ShowWarning({user:{profile: {visibility: 2}}}, {a:'b'})).toEqual({a:'b', novisibility:true});
            done();
        });

        it("MergePage", function(done) {
            expect(players_page.MergePage({user: {profile: {localarea: {name: 'abc'}}}})('s')({a:'b'})).toEqual( { heading : { name : 'Nearby Players', i : { class : 'fa fa-s fa-fw' }, h4 : { faicon : 'users', text : 'abc' } }, a : 'b' } )
            expect(players_page.MergePage({user: {profile: {localarea: {name: 'text'}}}})('d')({c:'d'})).toEqual( { heading : { name : 'Nearby Players', i : { class : 'fa fa-d fa-fw' }, h4 : { faicon : 'users', text : 'text' } }, c : 'd' } )
            done();
        })

        it("buildFaIcon", function(done) {
            const expectObj = { class : 'fa fa-u fa-fw' } ;
            expect(players_page.BuildFaIcon('u')).toEqual(expectObj);
            done();
        });

        it("BuildImage", function(done) {
            const expectObj = { class : 'u', src : 's' } ;
            expect(players_page.BuildImage('u', 's')).toEqual(expectObj);
            done();
        });

        it("BuildHeading", function(done) {
            const expectObj = { name : 'u', s : 'v', h4 : { faicon : 'a', text : 'f' } } ;
            expect(players_page.BuildHeading('u', 's', 'a', 'f', 'v')).toEqual(expectObj);
            done();
        });

        it("BuildPage", function(done) {
            const expectObj = { url : 'u', handlebars : { heading : { name : 's', i : 'v', h4 : { faicon : 'a', text : 'f' } } } } ;
            expect(players_page.BuildPage('u', 's', 'i', 'a', 'f', 'v')).toEqual(expectObj);
            done();
        });

    });


    describe("web page tests", function() {

      it("setup server", function(done) {
        testapp.get('/', function (req, res) {
          res.send('<html>Home Page</html>')
        });

        login_page.RouteLogins(testapp, passport);
        login_page.PostLogin(testapp, passport);
        login_page.RouteVerify(testapp, account);
        server = testapp.listen(httpPort, function() {
          done();
        });
      });

      it("non logged in user visit player", function(done) {
        const page = 'players';
        browser.visit(base_url + page, function(err) {
          expect(browser.success).toBe(true);
          expect(browser.html()).toContain('Roll for Group');
          done();
        });
      });

		it("invalid user visit /players", function(done) {

      var userEmail = faker.Internet.email();
      var userPass = 'testpassword';

      var userobj = {};

      verifyregister(account, browser, userEmail, userPass, userobj).then(function(x) {
        const page = 'players';
        browser.visit(base_url + page, function(err) {
          expect(browser.success).toBe(true);
          expect(browser.html('#warninglocation')).toContain('Your Profile Needs a Location.');
          done();
        });
      });
    });

		it("valid user visit /players", function(done) {

      var userEmail = faker.Internet.email();
      var userPass = 'testpassword';

      var userobj = {
        profile: {
          searchradius: 10,
          localarea: {
            name: 'area',
            loc: {
              coordinates: [20, 20],
              type: 'Point'
            }
          }
        }
      };

      verifyregister(account, browser, userEmail, userPass, userobj).then(function(x) {
        const page = 'players';
        browser.visit(base_url + page, function(err) {
          expect(browser.success).toBe(true);
          done();
        });
      });
    });

    it('server close', function(done) {
      server.close(function() {
        done();
      });
    });

    it('db close', function(done) {
      mongo.CloseDatabase();
      done();
    });
  });

});



