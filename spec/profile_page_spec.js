var Promise = require('bluebird');
var R = require('ramda');
var util = require('util')
var log = require('../app/log.js');

var passport = require('passport');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var profile_page = require('../app/profile_page.js');

var testBoardgame = require('../app/models/boardgame.js');
var testBoardgames = require('../app/models/boardgames.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var match = require('../app/models/match.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');
var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 3917;
var login = require('connect-ensure-login');

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

const Maybe = require('data.maybe')

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
    testapp.close(function(){
        console.log('matches_service_spec closed on port ' + httpPort);
    });
};


// ============== RUN TESTS ================== 
describe("profile_page_spec.js", function() {

  var server;

  // loginUser :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
  var verifyregister = function(account, zombieb, email, pass, userobj) {
    return new Promise(function(fulfill, reject) {
      accounts.DeleteAccount(account, email).then(function(y) {
        var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

        register(email, pass).then(function(x) {
          account.update({email: email}, userobj).exec().then(function(z) {
            zombieb.visit(base_url + "login", function(err) {
              if (err) 
                reject(err);

              zombieb.fill('input[name="form-username"]', email)
              zombieb.fill('input[name="form-password"]', pass)
              zombieb.pressButton('login', function() {
                fulfill(x);
              });
            });
          });
        });
      });
    });
  };

  describe("web page tests", function() {

    var userEmail = faker.Internet.email().toLowerCase();
    var userPass = 'testpassword';
    var userValues = {};

    it("whenExistsMergeId", function(done) {
      expect(profile_page.whenExistsMergeId('a', 'x', {a: {x:'a'}})).toEqual({a: {x: 'a', id:'a'}});
      expect(profile_page.whenExistsMergeId('a', 'y', {a: {x:'a'}})).toEqual({a: {x:'a'}});
      done();
    });

    it("MergeTextInputValue", function(done) {

      var x = {one: {value: 'test'}};
      var y = {one: {value: 'value'}};
      
      var x2 = {
        textinput: {
          id: 'oldid',
          datapath: ['a', 'b'],
          name: 'newid'
        }
      };

      var valueObj = {a: {b: "othervalue" }};
      var z = {
        textinput: {
          id: 'newid',
          name: 'newid',
          datapath: ['a', 'b'],
          value: 'othervalue'
        }
      };

      expect(profile_page.MergeTextInputValue('one', x, undefined)).toEqual(x);
      expect(profile_page.MergeTextInputValue('one', x, 'value')).toEqual(y);
      expect(profile_page.MergeTextInputValue('textinput', x2, valueObj)).toEqual(z);

      done();

    });

    it("UpdateCheckOption", function(done) {

      var o = {id: 'abc', checked: false};
      var p = {id: 'abc', checked: true};
      expect(profile_page.UpdateCheckOption({}, o)).toEqual(o);
      expect(profile_page.UpdateCheckOption({notexist: true}, o)).toEqual(o);
      expect(profile_page.UpdateCheckOption({abc: false}, o)).toEqual(o);
      expect(profile_page.UpdateCheckOption({abc: true}, o)).not.toEqual(o);
      expect(profile_page.UpdateCheckOption({abc: true}, o)).toEqual(p);
      done();

    });

    it("MergeCheckboxValue", function(done) {

      var o = {'option': 'one', id: 'abc'};
      var p = {'option': 'two', id: 'def'};
      var ocheck = {'option': 'one', id: 'abc', checked: true};

      var checkso = [o, p];
      var defaultData = {checkbox:{options:[]}};
      var checkedData = {checkbox:{options:checkso}};
      var checkedDataTrue = {checkbox:{options:[ocheck, p]}};

      var vals = {abc: false, def: false};

      expect(profile_page.MergeCheckboxValue(defaultData, [], vals)).toEqual(defaultData);
      expect(profile_page.MergeCheckboxValue(defaultData, checkso, {notexist: true})).toEqual(checkedData);
      expect(profile_page.MergeCheckboxValue(defaultData, checkso, {abc: false})).toEqual(checkedData);
      expect(profile_page.MergeCheckboxValue(defaultData, checkso, {abc: true})).not.toEqual(checkedData);
      expect(profile_page.MergeCheckboxValue(defaultData, checkso, {abc: true})).toEqual(checkedDataTrue);

      done();

    });

    it("UpdateSelectOption", function(done) {

      var o = {id: 'abc', value: false};
      var p = {id: 'abc', value: false, default: true};
      expect(profile_page.UpdateSelectOption({}, 'testid', o)).toEqual(o);
      expect(profile_page.UpdateSelectOption({notexist: true}, 'abc', o)).toEqual(o);
      expect(profile_page.UpdateSelectOption({abc: true}, 'abc', o)).toEqual(o);
      expect(profile_page.UpdateSelectOption({abc: false}, 'abc', o)).not.toEqual(o);
      expect(profile_page.UpdateSelectOption({abc: false}, 'abc', o)).toEqual(p);
      done();

    });

    it("MergeSelectValue", function(done) {

      var o = {id: 'abc', value: false};
      var p = {id: 'def', value: true};
      var oselect = {id: 'abc', value: false, default: true};
      var pselect = {id: 'def', value: true, default: true};

      var checkso = [o, p];
      var defaultData = {select:{id: 'slct', options:[]}};
      var selectNothing = {select:{id: 'slct', options:checkso}};

      var selectoData = {select:{id: 'slct', options:[oselect, p]}};
      var selectpData = {select:{id: 'slct', options:[o, pselect]}};

      var vals = {abc: false, def: false};

      expect(profile_page.MergeSelectValue(defaultData, [], vals)).toEqual(defaultData);
      expect(profile_page.MergeSelectValue(defaultData, checkso, {notexist: true})).toEqual(selectNothing);
      expect(profile_page.MergeSelectValue(defaultData, checkso, {slct: 'na'})).toEqual(selectNothing);
      expect(profile_page.MergeSelectValue(defaultData, checkso, {slct: true})).toEqual(selectpData);
      expect(profile_page.MergeSelectValue(defaultData, checkso, {slct: false})).toEqual(selectoData);

      done();

    });

    it("MergeSliderValue", function(done) {

      var vals = {a: 'st.hr', b: 'st.min', c: 'ed.hr', d: 'ed.min'};
      var obj = {slider:{}};
      var transObj = {
        slider: { 
          datastarthour : 'st.hr', 
          datastartminute : 'st.min', 
          dataendhour : 'ed.hr', 
          dataendminute : 'ed.min' 
        }
      };
      var sliderid = {
        values: {
          starthour: 'a',
          startminute: 'b',
          endhour: 'c', 
          endminute: 'd'
        }
      }; 
      expect(profile_page.MergeSliderValue(obj, null, vals)).toEqual(obj);
      expect(profile_page.MergeSliderValue(obj, sliderid, vals)).toEqual(transObj);
      done();

    });

    it("MergeImageValue", function(done) {
      var dataA = {a: 'hello'};
      var dataB = {imageupload: {}};

      var dataBResultA = {imageupload:{src: 'goodbye'}};
      var dataBResultB = {imageupload:{src: 'goodbye', hasfacebook: true}};

      var vals = Maybe.of({avataricon: 'goodbye'});

      expect(profile_page.MergeImageValue(false, dataA, vals)).toEqual(dataA);
      expect(profile_page.MergeImageValue(false, dataB, vals)).toEqual(dataBResultA);
      expect(profile_page.MergeImageValue(true, dataB, vals)).toEqual(dataBResultB);

      done();
    });

    it("setup server", function(done) {
      testapp.get('/', function (req, res) {
        res.send('<html>Home Page</html>')
      });

      profile_page.RouteProfile(testapp, login);

      login_page.RouteLogins(testapp, passport);
      login_page.PostLogin(testapp, passport);
      login_page.RouteVerify(testapp, account);
      server = testapp.listen(httpPort, function() {
        done();
      });
    });

    it("non logged in user visit hostgame", function(done) {
      page = 'profile';
      browser.visit(base_url + page, function(err) {
        expect(browser.success).toBe(true);
        expect(browser.html()).toContain('Roll for Group');
        done();
      });
    });

    it("user login", function(done) {
      verifyregister(account, browser, userEmail, userPass, {}).then(function(x) {
        done();
      });
    });

    var userValues = {};
    it("preload values into accout", function(done) {
      userValues.name = faker.Internet.userName();
      userValues.bggname = faker.Internet.userName();
      userValues.Monday = true;
      userValues.Tuesday = false;
      userValues.profession = faker.Company.companyName;
      userValues.biography = faker.Company.catchPhrase;
      userValues.mondaystarthour = 5;
      userValues.mondaystartminute = 30;
      userValues.mondayendhour = 15;
      userValues.mondayendminute = 45;

      account.findOneAndUpdate({'email': userEmail}, 
        {$set:{'profile':
          {
            'name': userValues.name, 
            'bggname': userValues.bggname, 
            'Monday': userValues.Monday, 
            'Tuesday': userValues.Tuesday,
            'profession': userValues.profession,
            'biography': userValues.biography,
            'mondaystarthour': userValues.mondaystarthour,
            'mondaystartminute': userValues.mondaystartminute,
            'mondayendhour': userValues.mondayendhour,
            'mondayendminute': userValues.mondayendminute,
           }
         }}, {new: true}).exec().then(function(x) {
         done();
      });
    });

    it("valid user visit /profile", function(done) {
      browser.visit(base_url + 'profile', function(err) {
        expect(browser.html('#Tuesday')).not.toContain('checked');
        expect(browser.html('#div4profession')).not.toContain(userValues.profession);
        expect(browser.html('#div4biography')).not.toContain(userValues.biography);
        expect(browser.html('.buttons-area')).toContain('Save');
        expect(browser.html('.alert-danger')).toContain('Delete account');
        expect(browser.success).toBe(true);
        done();
      });
    });

    it('server close', function(done) {
      server.close(function() {
        done();
      });
    });

    it('db close', function(done) {
      mongo.CloseDatabase();
      done();
    });

  });
});
