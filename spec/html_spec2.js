var express = require("express");
var request = require("request");
var app_server = require("../app/server.js");
var log = require('../app/log.js');

var fs = require('fs');
var base_url = "http://localhost:3000/"
var events = require('events');
var R = require('ramda');

var util = require('util');

var test_folder = "spec/benchmark/";

var Browser = require("zombie");
var browser = new Browser();

var url = "http://localhost:3000";

var testAccount = require('../app/models/account.js');
var testAccounts = require('../app/models/accounts.js');
var testGroups = require('../app/models/groups.js');
var testGroup = require('../app/models/group.js');
var testBoardgames = require('../app/models/boardgames.js');
var testBoardgame = require('../app/models/boardgame.js');

var templatedata = require('../app/templatedata.js');
var facebook = require('../app/facebook.js')

var app = express();

var nodemailer = require('nodemailer');
var stubTransport = require('nodemailer-stub-transport');
var transporter = nodemailer.createTransport(stubTransport());

var cachegame2016 = testBoardgames.CacheBoardgame(testBoardgame, 2016);
var zombieHelper = require('./zombiehelper');

// String -> (Promise -> String)
var zombieGetBody = zombieHelper.PromiseZombieVisit(browser, "body");

describe("testing with zombie", function() {

	it("should have defined headless browser", function(next){
        expect(typeof browser != "undefined").toBe(true);
        expect(browser instanceof Browser).toBe(true);
        next();
	});

	it("should visit the site and see the home screen", function(next) {
        browser.visit(base_url, function(err) {
            expect(browser.success).toBe(true);
            next();
        })
	});

	it("should visit the site and see the support screen", function(next) {
			browser.visit(base_url + "support", function(err) {
				expect(browser.success).toBe(true);
				next();
			})
	});

	it("should visit the site and see the login screen", function(next) {
			browser.visit(base_url + "login", function(err) {
					expect(browser.success).toBe(true);
					expect(browser.html("body")).toContain("Enter username and password to log on");
					next();
			})
	});


	it("should visit the site and see the login screen promise", function(next) {
		zombieGetBody(base_url + "login").then(function(html) {
			expect(html).toContain("Enter username and password to log on");
			next();
		})
	});

	it("should visit the site and see the signup screen", function(next) {
		zombieGetBody(base_url + "signup").then(function(html) {
			expect(html).toContain("Fill in the form below to start playing more games:");
			next();
		})
	});

});

describe("file structure tests", function() {
	describe("GET /", function() {

		it("returns status code 200", function(done) {
			request.get(base_url, function(error, response, body) {
				expect(response.statusCode).toBe(200);
				done();
			});
		});

		it("test static index index.html", function(done) {
			browser
				.visit(base_url, function(err) {
					expect(browser.success).toBe(true);
					expect(browser.html("body")).toContain("Play the latest boardgames");
					done();
			});
		});

	});
});


// ------------------ here we want to put tests regarding not being logged in --------------
//

describe('testing logging in', function() {
	describe('GET /', function() {

		it('test not logged in profile', function(done) {
			zombieGetBody(base_url + 'profile').then(function(html) {
				expect(html).toContain('Roll for Group');
				done();
			})
		});

		it('test not logged in schedule', function(done) {
			zombieGetBody(base_url + 'schedule').then(function(html) {
				expect(html).toContain('Login in with Email');
				done();
			})
		});

// ----------------- test the process of logging in ------------------
//
		it('test login and fail', function(done) {
			zombieGetBody(base_url + 'login').then(function(html) {
				expect(html).toContain('Enter username and password to log on:');
				done();
			});
		});

		it('button login fail', function(done) {
			browser
				.fill('input[name="form-username"]', 'test_user@gmail.com')
				.fill('input[name="form-password"]', 'test_password')
				.pressButton('login', function(){
					expect(browser.html("body")).toContain("Your username and password was not found, please try again or");
					done();
				});
		});

		it("test signup page", function(done) {
			zombieGetBody(base_url + 'signup').then(function(html) {
				expect(html).toContain("Fill in the form below to start playing more games:");
				done();
			});
		});

		it("test registration and verification", function(done) {
			testAccounts.registerAccount(testAccount, "test_user3@gmail.com", "test_password2").then(
				function(new_account) {
					expect(new_account.email).toBe("test_user3@gmail.com");
					expect(new_account.isAuthenticated).toBe(false);
					testAccounts.verifyAccount(testAccount, new_account.authToken).then(done())
				}
			);
		});

		it("test registration and verification via website", function(done) {
			testAccounts.registerAccount(testAccount, "test_user4@gmail.com", "test_password2").then(function(new_account) {
				expect(new_account.email).toBe("test_user4@gmail.com");
				expect(new_account.isAuthenticated).toBe(false);
				browser.visit(base_url + 'verify?authToken=' + new_account.authToken, function(err) {
					expect(browser.success).toBe(true);
					expect(browser.html("body")).toContain("Roll for Group"); // /profile page
					done();
				});
			});
		});

		it("button login success", function(done) {
			browser.visit(base_url + "login", function(err) {
				browser.fill('input[name="form-username"]', 'test_user4@gmail.com')
				browser.fill('input[name="form-password"]', 'test_password2')
				browser.pressButton('login', function() {
					expect(browser.html("body")).toContain(templatedata.stage1_page.sections[0].title);
					done();
				});
			});
		});

		it("logged in profile", function(done) {
			zombieGetBody(base_url + "profile").then(function(html) {
                expect(html).toContain("feedback@uprightgaming.com");
                done();
			})
		});

		it("logged in stage1 - form data", function(done) {
			zombieGetBody(base_url + "stage1").then(function(html) {
                expect(html).toContain("Queue for Group");
                done();
			})
		});

		it("logged in stage1 - stage progress", function(done) {
            browser.fill('input[name="party1"]', "test_xxx@gmail.com");
            browser.fill('input[name="party2"]', "test_xyz@gmail.com");
            browser.fill('input[name="bggname"]', "jaied");
			expect(browser.html("body")).toContain("Your Group");
			done();
		});

		it("logged in stage1 - next stage2", function(done) {
            browser.debug = true;
			browser.pressButton('#testpress')
			browser.pressButton('#nextverify')
            browser.wait(3000).then(function(x) {
                //setTimeout(function() {
                //    zombieGetBody(base_url + "stage2").then(function(html) {
                       // expect(html).toContain(templatedata.stage2_page.sections[0].title);
                log.ConsoleLogObjectSection('stage1-2.nextverify.debug', browser.html('#debug'));
                log.ConsoleLogObjectSection('stage1-2.nextverify', browser.html('#nextverify'));
                log.ConsoleLogObjectSection('stage1-2.groupup1', browser.html('#groupup1'));
                expect(browser.html('body')).toContain(templatedata.stage2_page.sections[0].title);
                done();
                //    });
                //}, 3000);
            });
		});

        it('create test discover games', function(done) {
            var cacheObject1 = {
                numplays: 0,
                thumbnail: '//cf.geekdo-images.com/images/pic12345.jpg',
                yearpublished: 2016,
                name: {
                    t: 'Test Discover1 Game',
                    sordindex: 1
                },
                subtype: 'boardgame',
                objectid: 123458,
                objecttype: 'thing'
            };

            var cacheObject2 = {
                numplays: 0,
                thumbnail: '//cf.geekdo-images.com/images/pic12845.jpg',
                yearpublished: 2015,
                name: {
                    t: 'Test Discover2 Game',
                    sordindex: 1
                },
                subtype: 'boardgame',
                objectid: 423458,
                objecttype: 'thing'
            };

            var cacheObject3 = {
                numplays: 0,
                thumbnail: '//cf.geekdo-images.com/images/pic19845.jpg',
                yearpublished: 2016,
                name: {
                    t: 'Test Avoid1 Game',
                    sordindex: 1
                },
                subtype: 'boardgame',
                objectid: 474458,
                objecttype: 'thing'
            };

            var cacheObject4 = {
                numplays: 0,
                thumbnail: '//cf.geekdo-images.com/images/pic479458.jpg',
                yearpublished: 2016,
                name: {
                    t: 'Test Host Game',
                    sordindex: 1
                },
                subtype: 'boardgame',
                objectid: 479458,
                objecttype: 'thing'
            };

            var testMatching = {
                own: true,
                wanttoplay: true,
                avoid: false
            };

            Promise.all(R.map(cachegame2016, [cacheObject1, cacheObject2, cacheObject3, cacheObject4])).then(function(x) {
                var assocObj1 = testAccounts.AssociateGame(testAccount, 'test_user4@gmail.com', cacheObject1.name.t, testMatching)
                var assocObj2 = testAccounts.AssociateGame(testAccount, 'test_user4@gmail.com', cacheObject2.name.t, testMatching)
                var assocObj4 = testAccounts.AssociateGame(testAccount, 'test_user4@gmail.com', cacheObject4.name.t, testMatching)

                Promise.all([assocObj1, assocObj2, assocObj4]).then(function(y){
                    done(); 
                })
            });
        });


		it("logged in stage2 - next stage3", function(done) {
			browser.pressButton('nextstage', function(){
				expect(browser.html("body")).toContain(templatedata.stage3_page.sections[0].title);
				done();
			});
		});

        /*
		// when we log out and return, we should return to the correct stage
		it("logged in stage3 - logout", function(done) {
			browser.visit(base_url + "logout",
				function (err) {
					expect(browser.html("body")).toContain("Play the latest boardgames");
					done();
				}
			)
		});
        */

		it("logged in stage3 - success", function(done) {
			browser.visit(base_url + "login", function(err) {
				browser.fill('input[name="form-username"]', 'test_user4@gmail.com');
				browser.fill('input[name="form-password"]', 'test_password2');
				browser.pressButton('login', function(){
					expect(browser.html("body")).toContain(templatedata.stage3_page.sections[0].title);
                    browser.wait(550).then(function(x) {
                        browser.check('#Monday');
                        browser.check('#Tuesday');
                        browser.wait(250).then(function(y) {
                            browser.check('input[name="Friday"]');
                            browser.fill('#mondaystart', '14:15');
                            browser.fill('#mondayend', '18:45');
                            browser.fill('#fridaystart', '9:00');
                            browser.fill('#fridayend', '15:30');
                            done();

                        });
                    });

				});
			});
		});

		it("logged in stage3 - next stage4", function(done) {
            browser.fill('#playcity', 'Wollongong NSW 2500, Australia');
			browser.pressButton('#nextverify', function(){
				expect(browser.html("body")).toContain(templatedata.stage4_page.sections[0].title);
				done();
			});
		})
        
		it("logged in stage4 - rollforgroup", function(done) {
            browser.fill('#hostaddress', '30 Avon Parade, Mount Kembla NSW 2526, Australia');
			browser.pressButton('#nextverify', function(){
                setTimeout(function() {
                    zombieGetBody(base_url + "rollforgroup").then(function(html) {
                        //browser.wait(1000).then(function(x) {
                        expect(browser.html("body")).toContain("Play More Board Games");
                        done();
                    });
                }, 3000);
			});
		}, 4000)

		it("logged in groups", function(done) {
			zombieGetBody(base_url + "groups").then(function(html) {
                expect(html).toContain("Create Group");
                expect(html).toContain("test_xxx@gmail.com");
                expect(html).toContain("test_xyz@gmail.com");
                done();
			})
		});

		it("logged in collection", function(done) {
			zombieGetBody(base_url + 'collection').then(function(html) {
                expect(html).toContain('Games Owned');
                expect(html).toContain('Cards Against Humanity');
                expect(html).toContain('Mage Knight Board Game');
                browser.pressButton('#letgo62871');
                browser.pressButton('#letgo13');
                browser.wait(500).then(function(x) {
                    expect(browser.html('body')).toContain('<tr id="row62871" style="display: none;">');
                    browser.fill('#gamename', 'Catan');
                    browser.pressButton('#addgame');
                    browser.wait(300).then(function(x) {
                        expect(browser.html('#row13')).toContain('Catan');
                        done();
                    })
                }) ;
			}, 
                log.ConsoleLogObject
            )
		}, 5000);


		it("try to host again wanttoplay host", function(done) {
			zombieGetBody(base_url + 'wanttoplay').then(function(html) {
                expect(browser.html('#row479458')).toContain('join479458');
                expect(browser.html('#row479458')).toContain('host479458');
                
                // the button below trigers the url, but when pressed via Zombie an initButton error occurs and the cause is unknown
                // manually tests show the button triggers the URL ok
                // browser.pressButton('#host479458');
                // browser.wait(300).then(function(x) {
                zombieGetBody(base_url + 'hostgame?gameid=479458&isowned=0').then(function(html) {
                    expect(browser.html('.page-header')).toContain('Host Game');
                    expect(browser.html('#game')).toContain('Test Host Game');
                    expect(browser.html('#game')).toContain('//cf.geekdo-images.com/images/pic479458.jpg');
                    done();
                });

			}, 
                log.ConsoleLogObject
            )
		});


		it("wanttoplay join", function(done) {
			zombieGetBody(base_url + 'wanttoplay').then(function(html) {
                expect(browser.html('#row123458')).toContain('join123458');
                expect(browser.html('#row123458')).toContain('host123458');
                browser.pressButton('#join123458');
                browser.wait(300).then(function(x) {
                    expect(browser.html('.page-header')).toContain('Roll For Group');
                    done();
                })
			}, 
                log.ConsoleLogObject
            )
		});

		it("collection wanttoplay", function(done) {
			zombieGetBody(base_url + 'wanttoplay').then(function(html) {
                expect(browser.html('.page-header')).toContain('Looking To Play List');
                expect(browser.html('#row123458')).toContain('join123458');
                expect(browser.html('#row123458')).toContain('host123458');
                expect(browser.html('#row423458')).not.toContain('join423458');
                expect(browser.html('#row423458')).not.toContain('host423458');
                expect(html).not.toContain('host96848');
                expect(html).not.toContain('Mage Knight Board Game');
                browser.pressButton('#notplay123458');
                browser.wait(500).then(function(x) {
                    expect(browser.html('body')).toContain('<tr id="row123458" style="display: none;">');
                    browser.fill('#gamename', 'Catan');
                    browser.pressButton('#addgame');
                    browser.wait(300).then(function(x) {
                        expect(browser.html('#row13')).toContain('Catan');
                        done();
                    })
                })
			}, 
                log.ConsoleLogObject
            )
		}, 5000);

		it("collection wanttoplay confirm", function(done) {
			zombieGetBody(base_url + 'wanttoplay').then(function(html) {
                expect(browser.html('#row123458')).not.toContain('join123458');
                expect(browser.html('#row123458')).not.toContain('host123458');
                expect(browser.html('#row423458')).not.toContain('join423458');
                expect(browser.html('#row423458')).not.toContain('host423458');
                done();
			}, 
                log.ConsoleLogObject
            )
		});


		it("collection avoid", function(done) {

			zombieGetBody(base_url + 'collection').then(function(html) {

                browser.fill('#gamename', 'Test Avoid1 Game');
                browser.pressButton('#addgame');
                browser.wait(300).then(function(y) {

                    browser.pressButton('#avoid474458');
                    browser.wait(300).then(function(y) {

                        zombieGetBody(base_url + 'avoid').then(function(html) {

                            expect(browser.html('.page-header')).toContain("Do Not Play");
                            browser.pressButton('#unavoid474458');

                            browser.wait(300).then(function(x) {
                                expect(browser.html('body')).toContain('<tr id="row474458" style="display: none;">');
                                done();
                            })

                        })
                    })
                })
            })

        }, 5000);

		it("collection avoid confirm", function(done) {
			zombieGetBody(base_url + "avoid").then(function(html) {
                expect(browser.html('#row474458')).not.toContain("Test Avoid1 Game");
                expect(browser.html('#row1406')).not.toContain("Monopoly");
                done();
			}, 
                log.ConsoleLogObject
            )
		});

		it("logged in schedule", function(done) {
			zombieGetBody(base_url + "schedule").then(function(html) {
                expect(html).toContain("Availability");
                expect(html).toContain("14:15");
                expect(html).toContain("18:45");
                expect(html).toContain("9:00");
                expect(html).toContain("15:30");
                done();
			})
		});

		it("log out", function(done) {
			zombieGetBody(base_url + "logout").then(function(html) {
                expect(html).toContain("Start having fun again, we find and match you with other players who love the same games you do.");
                done();
			})
		});

		it("test that profile logged out", function(done) {
			zombieGetBody(base_url + "profile").then(function(html) {
                expect(html).toContain("Roll for Group");
                done();
			})
		});

		it("test verify email sent page", function(done) {
			zombieGetBody(base_url + "email-verification").then(function(html) {
					expect(html).toContain("An email has been sent to your inbox");
					done();
			})
		});

	});
});



describe("facebook", function() {
	describe("facebook database", function() {

		it("add facebook user", function(done) {
			var facebookuser = {
					id	 			: 12345,
					name			: {
						givenName : "test user",
						familyName 	: "last name",
					},
					emails		: [{
						value	: 'test@gmail.com'
					}],
					photos		: [{
						value : 'test1.img'
					}]
			};

			var newAccount = new testAccount();
			facebook.addFacebookUser(newAccount, "abcdeftoken", facebookuser).then(
				function(result){
					expect(result.facebook.id).toBe('12345');
					expect(result.facebook.token).toBe("abcdeftoken");
					expect(result.facebook.name).toBe("test user last name");
					expect(result.facebook.email).toBe("test@gmail.com");
					expect(result.facebook.photo).toBe("test1.img");
					done();

				},
				function(fail){
                    done(fail);
				}
			);
		});

		it("add facebook user version 2", function(done) {

			var facebookuser = {
					id	 			: 12324,
					name			: {
						givenName : "facebook",
						familyName 	: "loginstyle",
					},
					emails		: [{
						value	: 'test-xxx@gmail.com'
					}],
					photos		: [{
						value : 'test2.img'
					}]

			};

			facebook.facebookLogin(testAccount, "abcdeftokenzzz", facebookuser).then(
				function(result){
					expect(result.facebook.id).toBe('12324');
					expect(result.facebook.token).toBe("abcdeftokenzzz");
					expect(result.facebook.name).toBe("facebook loginstyle");
					expect(result.facebook.email).toBe("test-xxx@gmail.com");
					expect(result.facebook.photo).toBe("test2.img");
					done();
				}
			);
		});

		it("facebook login a user who has facebook login created", function(done) {

			var facebookuser2 = { id : 12345 }
			facebook.facebookLogin(testAccount, "abcdeftoken", facebookuser2).then(
				function(result){
					expect(result.facebook.id).toBe('12345');
					expect(result.facebook.token).toBe("abcdeftoken");
					expect(result.facebook.name).toBe("test user last name");
					expect(result.facebook.email).toBe("test@gmail.com");
					done();
				}
			);
		});

		it("add facebook user who has email local login", function(done) {
			var facebookuser = {
					id	 			: 1234578,
					name			: {
						givenName : "existing",
						familyName 	: "user",
					},
					emails		: [{
						value	: 'test_user3@gmail.com'
					}],
					photos		: [{
						value : 'test3.img'
					}]
			};

			facebook.facebookLogin(testAccount, "abcdeftokengh", facebookuser).then(
				function(result){
					expect(result.facebook.id).toBe('1234578');
					expect(result.facebook.token).toBe("abcdeftokengh");
					expect(result.facebook.name).toBe("existing user");
					expect(result.facebook.email).toBe("test_user3@gmail.com");
					expect(result.facebook.photo).toBe("test3.img");
					expect(result.email).toBe("test_user3@gmail.com");
					done();
				}
			);
		});
	});
});


describe("group", function() {
	describe("create group", function() {
		it("leader and two existing members", function(done) {
			testGroup.remove({}, function() {
				testGroups.CreateGroup(testGroup, testAccount, "test_user3@gmail.com", ["test_user4@gmail.com", "test@gmail.com"]).then(
					function(newgroup){
						expect(newgroup.queuestatus).toBe("pending");
						expect(newgroup.groupstatus).toBe("pending");
						expect(newgroup.members.length).toBe(3);
						done();
					},
					function(error) {
                        done(error);
					}
				);
			})
		});

		it("leader and one existing one non-existing members", function(done) {
			testGroups.CreateGroup(testGroup, testAccount, "test_user3@gmail.com", ["test_user10@gmail.com", "test@gmail.com", "testxyz@gmail.com"]).then(
				function(newgroup){
					expect(newgroup.queuestatus).toBe("pending");
					expect(newgroup.groupstatus).toBe("pending");
					expect(newgroup.members.length).toBe(4);
					done();
				},
				function(error) {
                    done(error);
				}
			)
		});

		it("no valid members", function(done) {
			testGroups.CreateGroup(testGroup, testAccount, "test_user3@gmail.com", [""]).then(
                done,
				function(error) {
					expect(error).toBe("no valid members");
					done();
				}
			)
		});

		it("invalid leader email", function(done) {
			testGroups.CreateGroup(testGroup, testAccount, "invalidleader", ["test_user10@gmail.com", "test@gmail.com", "testxyz@gmail.com"]).then(
                done,
				function(error) {
					expect(error).toBe("invalid leader email");
					done();
				}
			)
		});

		it("leader and valid with invalid emails", function(done) {
			testGroups.CreateGroup(testGroup, testAccount, "test@gmail.com", ["test_user10@gmail.com", "", "notvalidemail"]).then(
				function(newgroup){
					expect(newgroup.queuestatus).toBe("pending");
					expect(newgroup.groupstatus).toBe("pending");
					expect(newgroup.members.length).toBe(2);
					done();
				},
                done
			)
		});

		it("retreive groups", function(done) {

			testGroups.ReadMemberGroupData(testAccount, testGroup, "test_user3@gmail.com").then(
				function(data) {

					expect(data.groups.length).toBe(2);

					expect(data.groups[0].isLeader).toBeTruthy();
					expect(data.groups[0].user.length).toBe(1);
					expect(data.groups[0].team.length).toBe(2);

					expect(data.groups[0].team[0].isPending).toBeTruthy();
					expect(data.groups[0].team[0].avataricon).toBe("img/hour_glass.svg");

					expect(data.groups[1].isLeader).toBeTruthy();
					expect(data.groups[1].user.length).toBe(1);
					expect(data.groups[1].team.length).toBe(3);
					expect(data.groups[1].team[0].isPending).toBeTruthy();
					expect(data.groups[1].team[0].avataricon).toBe("img/hour_glass.svg");

					done();

				},
                done
			);
		});


		it("test relationship", function(done) {

			testAccounts.CreateSaveRelationships (testAccount, "test_user3@gmail.com", ["test1@gmail.com", "test2@gmail.com", "test3@gmail.com"], ["test", "tag"]).then(function(user) {

					expect(user.relationship.length).toBe(3);
					expect(user.relationship[0].email).toBe("test1@gmail.com");
					expect(user.relationship[0].rating).toBe(1);
					expect(user.relationship[0].gameplays).toBe(0);
					expect(user.relationship[0].complaints).toBe(0);

					expect(user.relationship[1].email).toBe("test2@gmail.com");
					expect(user.relationship[1].rating).toBe(1);

					expect(user.relationship[2].email).toBe("test3@gmail.com");
					expect(user.relationship[2].rating).toBe(1);

					done();

				},
                done
			);
		});
	});
});

describe("closing server and DB", function() {
    describe("closing services", function() {
        // ------------------ close server finish --------------
        it("close_app_server", function(done) {
            expect(true).toBe(true);
            app_server.closeServer();
            console.log("closing web server");
            done();
        });
    });
});
