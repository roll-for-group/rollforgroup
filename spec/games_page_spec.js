var Promise = require('bluebird');
var R = require('ramda');
var log = require('../app/log.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var passport = require('passport');
var mongo = require('../app/mongoconnect.js');

var login_page = require('../app/login_page.js');
var games_page = require('../app/games_page.js');
var login = require('connect-ensure-login');

var faker = require('Faker');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');

var Browser = require("zombie");
var browser = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var testapp = express();
var httpPort = 3037;

var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);

// verifyregister :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
var verifyregister = function(account, zombieb, email, pass, userobj) {
  return new Promise(function(fulfill, reject) {

    accounts.DeleteAccount(account, email).then(function(y) {

      var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

      register(email, pass).then(function(x) {
        account.update({email: email}, userobj).exec().then(function(z) {
          zombieb.visit(base_url + "login", function(err) {

            if (err) 
              reject(err);

            zombieb.fill('input[name="form-username"]', email)
            zombieb.fill('input[name="form-password"]', pass)
            zombieb.pressButton('login', function() {
              fulfill(x);
            });
          });
        });
      });
    });

  })
};


describe("games_page_spec", function() {
  describe("web page tests", function() {

    var server;

    it("setup server", function(done) {
      mongo.ConnectDatabase(dbUri);
      done();
    });

    it("setup server", function(done) {
      login_page.RouteLogins(testapp, passport);
      login_page.PostLogin(testapp, passport);
      login_page.RouteVerify(testapp, account);

      games_page.RouteUser(testapp, login);
      server = testapp.listen(httpPort, function() {
        done();
      });
    });

    
		it("valid user visit /players", function(done) {

      var userEmail = faker.Internet.email();
      var userPass = 'testpassword';

      var userobj = {
        profile: {
          searchradius: 10,
          localarea: {
            name: 'area',
            loc: {
              coordinates: [20, 20],
              type: 'Point'
            }
          }
        }
      };

      verifyregister(account, browser, userEmail, userPass, userobj).then(function(x) {
        const page = "games";
        browser.visit(base_url + page, function(err) {
          expect(browser.success).toBe(true);
          expect(browser.html()).toContain('bgg link');
          done();
        });
      });
    });

    it('close server', function(done) {
      server.close();
      done();
    });

    it('db close', function(done) {
        mongo.CloseDatabase();
        done();
    });

  });
});
