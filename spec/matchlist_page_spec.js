var Promise = require('bluebird');
var R = require('ramda');
var util = require('util');
var log = require('../app/log.js');
var path = require('path');

var passport = require('passport');
var genhelper = require('./genhelper.js');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var matchlist_page = require('../app/matchlist_page.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var browser2 = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');
var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');
var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 3048;
var login = require('connect-ensure-login');

// String -> (Promise -> String)
var promisifyZombie = zombieHelper.PromisifyZombie();
var zombieGetBody = zombieHelper.PromiseZombieVisit(browser, "body");

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);
matchlist_page.RouteMatchLists(testapp, login, account, nconf);

var generateUser = genhelper.GenerateUser(account);
var generateHostUser = genhelper.GenerateHostUser(account);


// ============== END SERVER FOR TESTS =======
var closeServer = function() {
	testapp.close(function(){
		console.log('matches_service_spec closed on port ' + httpPort);
	});
};


// ============== RUN TESTS ================== 
describe("matchlist_page_spec.js api", function() {

	var server;

	// loginUser :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
	var verifyregister = function(account, zombieb, email, pass, userobj) {
		return new Promise(function(fulfill, reject) {

			accounts.DeleteAccount(account, email).then(function(y) {
				var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

				register(email, pass).then(function(x) {

					account.update({email: email}, userobj).exec().then(function(z) {
						zombieb.visit(base_url + "login", function(err) {
							if (err) 
								reject(err);

							zombieb.fill('input[name="form-username"]', email)
							zombieb.fill('input[name="form-password"]', pass)
							zombieb.pressButton('login', function() {
								fulfill(x);
							});
						});
					});
				});
			});
		})
	};


	describe("web page tests", function() {

		it("setup server", function(done) {

			testapp.get('/', function (req, res) {
				res.send('<html>Home Page</html>')
			});

			matchlist_page.RouteMatchLists(testapp, login, match, account);
			login_page.RouteLogins(testapp, passport);
			login_page.PostLogin(testapp, passport);
			login_page.RouteVerify(testapp, account);
			server = testapp.listen(httpPort, function() {
				done();
			});

		});

		it('server close', function(done) {
			server.close(function() {
				done();
			});
		});

		it('db close', function(done) {
			mongo.CloseDatabase();
			done();
		});

	});

});
