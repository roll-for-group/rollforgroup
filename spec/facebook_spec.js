var R = require('ramda');
var util = require('util');
var log = require('../app/log.js');

var Promise = require('bluebird');
var faker = require('Faker');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var genhelper = require('./genhelper.js');

var mongo = require('../app/mongoconnect.js');
var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');

mongo.ConnectDatabase(dbUri);
/*
mongoose.connect(
	nconf.get('database:uri'), {
		userMongoClient:true
	}
);
*/

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var match = require('../app/models/match.js');

var facebook = require('../app/facebook.js');

const generateUser = genhelper.GenerateUser(account);


describe("facebook", function() {

  it("updateFacebookPicture", function(done) {
    var img     = "fbimage";
    var user1   = {facebook:{photo:img}, profile:{avataricon:'a'}};
    //var user2   = {facebook:{photo:img}}; <-- Throws Error because no profile
    var user3   = {profile:{avataricon: 'b'}};

    expect(facebook.updateFacebookPicture(user1).profile.avataricon).toBe(img);
    //expect(facebook.updateFacebookPicture(user2).profile.avataricon).toBe(img);
    expect(facebook.updateFacebookPicture(user3).profile.avataricon).toBe('b');

    done();
  });

  it("updateGooglePicture", function(done) {
    var img     = "fbimage";
    var user1   = {google:{photo:img}, profile:{avataricon:'a'}};
    //var user2   = {facebook:{photo:img}}; <-- Throws Error because no profile
    var user3   = {profile:{avataricon: 'b'}};

    expect(facebook.updateGooglePicture(user1).profile.avataricon).toBe(img);
    //expect(facebook.updateGooglePicture(user2).profile.avataricon).toBe(img);
    expect(facebook.updateGooglePicture(user3).profile.avataricon).toBe('b');

    done();
  });

  it("addFacebookUser", function(done) {
    var email = faker.Internet.email().toLowerCase()       

    // createfacebookObj
    var createfacebookObj = R.zipObj(['id', 'name', 'emails', 'photos', 'name'], ['id', 'na', [{value: email}], [{value:'p'}], {givenName:'gn', familyName:'fn'} ]);
    
    // delete person with that email
    accounts.DeleteAccount(account, email).then(
      (x) => {
        facebook.addFacebookUser(new account(), 'tkn', createfacebookObj)
          .then((user) => {
            expect(user.facebook.id).toBe('id');
            expect(user.facebook.token).toBe('tkn');
            expect(user.facebook.name).toBe('gn fn');
            expect(user.facebook.photo).toBe('p');

            expect(user.email).toBe(email);
            expect(user.profile.name).toBe('gn fn');
            expect(user.profile.avataricon).toBe('p');
            expect(user.profile.avataricontype).toBe('facebook');

            done();

          })
      })
  });

    it("addGoogleUser", function(done) {
        var email = faker.Internet.email().toLowerCase()       

        // createfacebookObj
        var creategoogleObj = R.zipObj(['id', 'name', 'emails', 'photos', 'name', 'gender'], ['id', 'na', [{value: email}], [{value:'p'}], {givenName:'gn', familyName:'fn'}, 'male' ]);
        
        // delete person with that email
        accounts.DeleteAccount(account, email).then((x) => facebook.addGoogleUser(new account(), 'tkn', creategoogleObj).then((user) => {

                expect(user.google.id).toBe('id');
                expect(user.google.token).toBe('tkn');
                expect(user.google.name).toBe('gn fn');
                expect(user.google.photo).toBe('p');
                expect(user.google.gender).toBe('male');

                expect(user.email).toBe(email);
                expect(user.profile.name).toBe('gn fn');
                expect(user.profile.avataricon).toBe('p');
                expect(user.profile.avataricontype).toBe('google');

                done();

            })
        )
    });




    it("close server", function(done) {
        mongo.CloseDatabase();
        done();
    });

});
