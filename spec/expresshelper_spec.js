var express = require("express");
var Browser = require("zombie");
var browser = new Browser();
var fs = require('fs');

// setup webpages
var app = express();
var expressHelper = require('../app/expresshelper');
var Promise = require('promise');

var test_folder = "spec/benchmark/";
var zombieHelper = require('./zombiehelper');
// String -> (Promise -> String)
var zombieGetBody = zombieHelper.PromiseZombieVisit(browser, "body");

// ============== START SERVER ===============
var base_url = "http://localhost:3099/"
var server = app.listen(3099, function(){
	  console.log('Testing on Port: 3099');
});

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  server.close(function(){
    console.log('Closing Server on Port 3099');
  });
};


const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

//expressHelper.StaticRouteFolders(app, ["fail.MD", "img", "css", "js", "fonts", "font-awesome", "assets", "less"]);
expressHelper.StaticRouteFolders(app, ["fail.MD", "css"]);

describe("Static route tests", function() {
	describe("function StaticRoutFolders", function() {
    it("file fail.MD", function(done) {
      zombieGetBody(base_url + "fail.MD").then(function(html) {
        expect(html).toContain("This is a test file designed to fail the file compare tests");
        done();
      })
    });

    it("test folder css/", function(done) {
      zombieGetBody(base_url + "css/creative.css").then(function(html) {
        expect(html).toContain('font-family: "Raleway"');
        done();
        closeServer();
      })
		});
  });
});

describe("Express helper", function() {

  it("maybeProfile", function(done) {

    const userall = {
      profile   : { a: 'hello', b: 'there'  },
      facebook  : { c: 'how',   d: 'are'    },
      google    : { e: 'you',   f: 'going'  }
    };

    const usernoprofile = {
      facebook  : { c: 'how',   d: 'are'    },
      google    : { e: 'you',   f: 'going'  }
    };

    const usergoogle = {
      google    : { e: 'you',   f: 'going'  }
    };

    expect(expressHelper.maybeProfile('a', 'c', 'e', 'none', userall).get()).toBe('hello');
    expect(expressHelper.maybeProfile('b', 'd', 'f', 'none', userall).get()).toBe('there');
    expect(expressHelper.maybeProfile('b', 'c', 'e', 'none', {}).get()).toBe('none');

    expect(expressHelper.maybeProfile('a', 'c', 'e', 'none', usernoprofile).get()).toBe('how');
    expect(expressHelper.maybeProfile('b', 'd', 'f', 'none', usernoprofile).get()).toBe('are');

    expect(expressHelper.maybeProfile('a', 'c', 'e', 'none', usergoogle).get()).toBe('you');
    expect(expressHelper.maybeProfile('b', 'd', 'f', 'none', usergoogle).get()).toBe('going');
    done();

  });


  it("LookupUsername", function(done) {
    var testEmail = {
      email: 'testemail',
      profile: {name: 'testname', avataricon: 'testicon'},
      pageslug: 'testemail'
    };
    var testFacebook = {
      email: 'facebookemail',
      facebook: {
        email:  'facebookemail',
        name:   'facebookname',
        photo:  'facebookphoto'
      },
      pageslug: 'testface'
    };
    
    var expectEmail = expressHelper.LookupUsername(testEmail);
    var expectFacebook = expressHelper.LookupUsername(testFacebook);

    expect(expectEmail.name).toBe('testname');
    expect(expectEmail.email).toBe('testemail');
    expect(expectEmail.avataricon).toBe('testicon');
    expect(expectEmail.pageslug).toBe('testemail');

    expect(expectFacebook.name).toBe('facebookname');
    expect(expectFacebook.email).toBe('facebookemail');
    expect(expectFacebook.avataricon).toBe('facebookphoto');
    expect(expectFacebook.pageslug).toBe('testface');

    done();
  });


  it("add user info to data for handlebars", function(done) {
    var facebookreq = {
      user: {
        email: "testuser@email.com",
        facebook: {
          email: "facebook@email.com",
          name: "John Citizen"
        },
        profile: {
          bggname: "fredjones",
          name: "emailname"
        }
      }
    };

    var emailreq = {
      user: {
        email: "testuser@email.com",
        profile: {
          name: "emailname",
          bggname: "fredjones"
        }
      }
    };

    var nullreq = {};

    var webdata = {
      mytest: "data"
    };
    
    var newfbdata = expressHelper.AddReqUserToData(facebookreq, webdata);
    expect(newfbdata.user.name).toBe("John Citizen");
    expect(newfbdata.user.email).toBe("facebook@email.com");
    expect(newfbdata.user.bggname).toBe("fredjones");
    expect(newfbdata.mytest).toBe("data");

    var newemaildata = expressHelper.AddReqUserToData(emailreq, webdata);
    expect(newemaildata.user.name).toBe("emailname");
    expect(newemaildata.user.email).toBe("testuser@email.com");
    expect(newemaildata.user.bggname).toBe("fredjones");
    expect(newemaildata.mytest).toBe("data");

    var newdata = expressHelper.AddReqUserToData(nullreq, webdata);
    expect(newdata).toEqual(webdata);

    done();
  });


  it("buildNavbarEvent", function(done) {
    const buildNav = expressHelper.BuildNavbarEvent;
    expect(buildNav(Maybe.of('b'), 'e', 'events?event=e')).toEqual({ li : [ { a : { link : 'events?event=e', text : 'Events' }, activeclass : true }, { a : { link : 'hostgame?event=e', text : 'Host Event' } }, { a : { link : 'eventslibrary?event=e', text : 'Game Library' } }, { a : { link : 'booking?event=e', text : 'Bookings' } }, { a : { link : 'schedule?event=e', text : 'Schedule' } } ] });
    expect(buildNav(Maybe.of('d'), 'e', 'users?email=d')).toEqual( { li : [ { a : { link : 'events?event=e', text : 'Events' } }, { a : { link : 'hostgame?event=e', text : 'Host Event' } }, { a : { link : 'eventslibrary?event=e', text : 'Game Library' } }, { a : { link : 'booking?event=e', text : 'Bookings' } }, { a : { link : 'schedule?event=e', text : 'Schedule' } } ] } );
    expect(buildNav(Maybe.Nothing(), 'e', 'events?event=e')).toEqual( { li : [ { a : { link : 'events?event=e', text : 'Events' }, activeclass : true }, { a : { link : 'hostgame?event=e', text : 'Host Event' } }, { a : { link : 'eventslibrary?event=e', text : 'Game Library' } }, { a : { link : 'booking?event=e', text : 'Bookings' } }, { a : { link : 'schedule?event=e', text : 'Schedule' } } ] } );

    done();
  });


  it("buildNavbar", function(done) {
    const buildNav = expressHelper.BuildNavbar;

    expect(buildNav('events', 'b').li[0].a.link).toBe('/events');
    expect(buildNav('events', 'b').li[0].activeclass).toBe(true);
    expect(buildNav('events', 'b').li[3].a.link).toBe('/users?email=b');

    expect(buildNav('lounge', 'b').li[0].a.link).toBe('/events');
    expect(buildNav('lounge', '0').li[1].activeclass).toBe(true);
    expect(buildNav('lounge', 'b').li[3].a.link).toBe('/users?email=b');

    expect(buildNav('events', 'd').li[0].a.link).toBe('/events');
    expect(buildNav('events', 'd').li[0].activeclass).toBe(true);
    expect(buildNav('events', 'd').li[3].a.link).toBe('/users?email=d');

    done();

  });


  it("buildNavbarHead", function(done) {
    const buildNav = expressHelper.BuildNavbarHead;
    expect(buildNav('f'))
      .toEqual({ 
        navhead : { 
          dropdown : { 
            text : 'f', 
            menu : [ { 
              text : 'Local Players', 
              href : 'events' 
            } ] 
          } 
        } 
      });

    expect(buildNav('a'))
      .toEqual({ 
        navhead : { 
          dropdown : { 
            text : 'a', 
            menu : [ { 
              text : 'Local Players', 
              href : 'events' } ] 
          } 
        } 
      });

    done();
  });

});
