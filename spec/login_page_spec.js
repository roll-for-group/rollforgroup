var favicon = require('serve-favicon');
var Promise = require('bluebird');
var R = require('ramda');
var log = require('../app/log.js');
var path = require('path');

var express = require('express');
var exphbs = require('express-handlebars');
var expressHelper = require('../app/expresshelper');

var passport = require('passport');
var login_page = require('../app/login_page.js');

var genhelper = require('./genhelper.js');
var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var Maybe = require('data.maybe');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');
var mongo = require('../app/mongoconnect');
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 3031;

// String -> (Promise -> String)
var zombieGetBody = zombieHelper.PromiseZombieVisit(browser, "body");

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + '/';

login_page.SetupPassportStrategies(passport, account);


function setupWebpages(expressApp) {
	expressApp.engine('handlebars', exphbs({defaultLayout: 'main'}));
	expressApp.set('view engine', 'handlebars');
	expressApp.use(require('morgan')('combined'));
	expressApp.use(require('cookie-parser')('just rolling for a cookie'));
	expressApp.use(require('body-parser').urlencoded({ extended: true }));
	expressApp.use(require('express-session')({ secret: 'rolling for group session key', resave: true, saveUninitialized: true }));
	expressApp.use(passport.initialize());
	expressApp.use(passport.session());
	expressApp.use(favicon(path.join(__dirname, '..', 'app', 'public','img','favicon.ico')));
}
setupWebpages(testapp);


// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  testapp.close(function(){
    console.log('login_service_spec closed on port ' + httpPort);
  });
};


// ============== RUN TESTS ================== 
describe("login_page_spec.js api", function() {

    var server;
    describe("web page tests", function() {

      it("setup server", function(done) {

        testapp.get('/', function (req, res) {
          res.send('<html>Home Page</html>')
        });

        login_page.RouteLogins(testapp, passport);
        login_page.PostLogin(testapp, passport);
        login_page.RouteVerify(testapp, account);
        login_page.GetReset(testapp, account);
        login_page.PostForgot(testapp, account, base_url);

        server = testapp.listen(httpPort, function() {
          done();
        });
      });


// ----------------- test the process of logging in ------------------
//
		it('test login and fail', function(done) {
			zombieGetBody(base_url + 'login').then(function(html) {
        expect(browser.html('.headertext')).toContain('Lets play board games');
				done();
			});
		});

		it('button login fail', function(done) {
			browser
				.fill('input[name="form-username"]', 'test_user@gmail.com')

      browser
				.fill('input[name="form-password"]', 'test_password')

      browser
				.pressButton('#loginbutton', function(){
          expect(browser.html('.alert')).toContain('The username or password was incorrect!');
					done();
				});
		});

		it("test signup page", function(done) {
			zombieGetBody(base_url + 'signup').then(function(html) {
        expect(browser.html('.header')).toContain('Lets play board games');
				done();
			});
		});

		it("test registration and verification", function(done) {
      var userEmail = 'login_user3@gmail.com';
      var userPass = 'testpassword';

      accounts.DeleteAccount(account, userEmail).then(function(x) {
        accounts.registerAccount(account, userEmail, userPass).then(function(new_account) {
          expect(new_account.email).toBe(userEmail);
          expect(new_account.isAuthenticated).toBe(false);
          accounts.verifyAccount(account, new_account.authToken).then(done())
        });
      });
		});

		it("test registration and verification via website", function(done) {

      var userEmail = 'login_user4@gmail.com';
      var userPass = 'testpassword';

      accounts.DeleteAccount(account, userEmail).then(function(x) {
        accounts.registerAccount(account, userEmail, userPass).then(function(new_account) {
          expect(new_account.email).toBe(userEmail);
          expect(new_account.isAuthenticated).toBe(false);
          browser.visit(base_url + 'verify?authToken=' + new_account.authToken, function(err) {

            expect(browser.success).toBe(true);
            expect(browser.html("body")).toContain("Roll for Group"); // /profile page

            //browser.visit(base_url + "login", function(err) {

              browser.fill('input[name="form-username"]', userEmail)
              browser.fill('input[name="form-password"]', userPass)
              browser.pressButton('login', function() {
                expect(browser.html("html")).toContain("Home Page");
                done();
              });

            //});
          });
				});
			});
		});

		it("visit forgot page", function(done) {
      browser.visit(base_url + 'forgot?error=invalid', function(err) {
        expect(browser.html('.headertext')).toContain('Forgot your password?');
        expect(browser.success).toBe(true);
				done();
			});
		});

		it("visit forgot page", function(done) {
      browser.visit(base_url + 'forgot', function(err) {
        expect(browser.html('.headertext')).toContain('Forgot your password?');
        expect(browser.success).toBe(true);
        done();
			});
		});

		it("visit verification page", function(done) {
      browser.visit(base_url + 'email-verification', function(err) {
        expect(browser.html('.headertext')).toContain('Verify your email');
        expect(browser.success).toBe(true);
        done();
			});
		});

		it("visit verification page with email", function(done) {
      browser.visit(base_url + 'email-verification?email=abc@def.com.au', function(err) {
        expect(browser.html('.headertext')).toContain('Verify your email');
        expect(browser.html('h3')).toContain('abc@def.com.au');
        expect(browser.success).toBe(true);
        done();
			});
		});



		it("visit send email doesn't exist", function(done) {
      browser.visit(base_url + 'forgot', function(err) {
        browser.fill('#email', 'badusername'); 
        expect(browser.success).toBe(true);
				done();
			});
		});

		it("visit reset page", function(done) {
      browser.visit(base_url + 'reset', function(err) {
        expect(browser.html('.headertext')).toContain('Forgot your password?');
        expect(browser.success).toBe(true);
				done();
			});
		});

		it("visit reset page:badauth", function(done) {
      browser.visit(base_url + 'reset?email=test&authToken=abcd', function(err) {
        expect(browser.html('.headertext')).toContain('Forgot your password?');
        expect(browser.success).toBe(true);
				done();
			});
		});

		it("visit reset page:goodauth, expired", function(done) {
      genhelper.GenerateUser(account, {resetPasswordToken:'mytoken'}).then(user => {
        browser.visit(base_url + 'reset?email=' + user.email + '&authToken=mytoken', function(err) {
          expect(browser.html('.headertext')).toContain('Forgot your password?');
          expect(browser.success).toBe(true);
          done();
        });
      });
		});

		it("visit reset page:goodauth", function(done) {
      genhelper.GenerateUser(account, {resetPasswordToken:'mytoken', resetPasswordExpires: Date.now() + 3600} ).then(user => {
        browser.visit(base_url + 'reset?email=' + user.email + '&authToken=mytoken', function(err) {
          expect(browser.html('#formheading')).toContain('Please enter your new password');
          expect(browser.success).toBe(true);
          done();
        });
      });
		});


    it("test cryptotoken", function(done) {
      const a = 30;
      const c = 40;
      const e = 40;
      login_page.CryptoToken(a).fork(
        err => err,
        b => {
          expect(R.length(b)).toBe(a * 2);
          login_page.CryptoToken(c).fork(
            err => err,
            d => {
              expect(R.length(d)).toBe(c * 2);
              login_page.CryptoToken(e).fork(
                err => err,
                f => {
                  expect(R.length(f)).toBe(e * 2);
                  expect(d).not.toBe(f);
                  done();
                }
              ) 
            }
          ) 
        } 
      )
		});

    it('SendEmailTransport-Nothing', function(done) {
      login_page.SendEmailTransport(account, base_url, Maybe.Nothing(), 'tokenabc').fork(
        err => done(),
        x => {}
      );
    });

    it('SendEmailTransport-UnknownUser', function(done) {
      login_page.SendEmailTransport(account, base_url, Maybe.of('to@where.com'), 'tokenabc').fork(
        err => {
          expect(err).toBe('Invalid user');
          done();
        },
        x => {}
      );
    });

    it('SendEmailTransport-KnownUser', function(done) {
      genhelper.GenerateUser(account, {}).then(user => {
        login_page.SendEmailTransport(account, base_url, Maybe.of(user.email), 'tokenabc').fork(
          err => {log.clos('err', err)},
          x => done() 
        );
      });
    });

    it('server close', function(done) {
      server.close(function() {
        done();
      });
    });

    it('db close', function(done) {
      mongo.CloseDatabase();
      done();
    });

  });

});
