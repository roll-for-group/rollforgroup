var Promise = require('bluebird');

var faker = require('Faker');

var R = require('ramda');
var compose  = R.compose,
    composeP = R.composeP,
    isNil    = R.isNil,
    prop     = R.prop,
    sequence = R.sequence,
    split    = R.split,
    tail     = R.tail;

var util = require('util');
var bgg = require('../app/bgg.js');
var log = require('../app/log.js');

var fs = require('fs');

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task');

var testAccount = require('../app/models/account.js');
var testAccounts = require('../app/models/accounts.js');

var gameEvent = require('../app/models/gameevent.js');
var gameEvents = require('../app/models/gameevents.js');

var genhelper = require('./genhelper.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');
mongo.ConnectDatabase(dbUri);

var currentyear = 2017;

var generateUser = genhelper.GenerateUser(testAccount);
var generateHostUser = genhelper.GenerateHostUser(testAccount);


describe('gameevents_spec.js', function() {

  var userEmail = 'testdummy@gmail.com';
  var newUser;

  it('TaskReadEvents', function(done) {

    const name = 'testname';
    const slug = 'slug';

    const eventObj = {
      name: name,
      pageslug: slug,
      loc: {
        coordinates: [faker.Address.longitude(), faker.Address.latitude()],
        type: 'Point'
      }
    };

    const findslug = R.compose(R.filter(R.propEq('pageslug', slug)));

    gameEvent.remove({pageslug:slug}).exec().then(function(x) {
      gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
        err => done(err),
        data => {
          expect(prop('name', data)).toBe(name);
          expect(prop('pageslug', data)).toBe(slug);

          gameEvents.TaskReadEvents(gameEvent).fork(
            err => done(err),
            data => {
              expect(prop('name', findslug(data)[0])).toBe(name);
              expect(prop('pageslug', findslug(data)[0])).toBe(slug);

              gameEvents.TaskReadEvent(gameEvent, findslug(data)[0].key).fork(
                err => done(err),
                data => {
                  expect(prop('name', data)).toBe(name);
                  expect(prop('pageslug', data)).toBe(slug);
                  done()
                }
              )
            }
          )
        }
      )
    });
  });


  it('TaskReadEventsBySlug', function(done) {

      const name = 'testname';
      const slug = 'slugread';

      const eventObj = {
          name: name,
          pageslug: slug,
          loc: {
              coordinates: [faker.Address.longitude(), faker.Address.latitude()],
              type: 'Point'
          }
     };

      gameEvent.remove({pageslug:slug}).exec().then(function(x) {
          gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
              err => done(err),
              data => {
                  expect(prop('name', data)).toBe(name);
                  expect(prop('pageslug', data)).toBe(slug);

                  gameEvents.TaskReadEventBySlug(gameEvent, slug).fork(
                      err => done(err),
                      data => {
                          expect(prop('name', data)).toBe(name);
                          expect(prop('pageslug', data)).toBe(slug);
                          done()
                      }
                  )

              }
          )
      });
  });


  it('TaskBookGameBySlug', function(done) {

    const name = 'taskbookgamebyslug';
    const slug = 'taskbookgamebyslugslug';

    const eventObj = {
      name: name,
      pageslug: slug,
      loc: {
        coordinates: [faker.Address.longitude(), faker.Address.latitude()],
        type: 'Point'
      },
      games:[{
        id: 2987,
        name: "Pirate's Cove",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }]
    };

    const getKey = R.prop('key');
    const getId = R.prop('objectid');

    const useremail = faker.Internet.email();
    const from = new Date().getTime();
    const to = new Date().getTime() + (60*60*1000);

    const fromPlus120 = from + (120*60*1000);
    const toPlus120 = to + (120*60*1000);

    const fromMinus30 = from - (30*60*1000);
    const toMinus30 = to - (30*60*1000);

    const ref = "abc";

    const lastBooking = R.compose(R.last, R.prop('bookings'))

    gameEvent.remove({pageslug:slug}).exec().then(function(x) {
      gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
        err => done(err),
        data => {
          gameEvents.TaskBookGameBySlug(gameEvent, slug, useremail, 2987, to, from, ref).fork(
            err => {
              gameEvents.TaskBookGameBySlug(gameEvent, slug, useremail, 2987, from, to, ref).fork(
                err => done(err),
                bookeddata => {
                  expect(lastBooking(bookeddata).bookingref).toBe(ref);
                  expect(lastBooking(bookeddata).email).toBe(useremail);
                  expect(lastBooking(bookeddata).gameid).toBe(2987);
                  expect(lastBooking(bookeddata).date - from).toBe(0);
                  expect(lastBooking(bookeddata).timeend - to).toBe(0);
                  gameEvents.TaskBookGameBySlug(gameEvent, slug, useremail, 2987, fromPlus120, toPlus120, ref).fork(
                    err => done(err),
                    bookeddata2 => {
                      expect(lastBooking(bookeddata2).bookingref).toBe(ref);
                      expect(lastBooking(bookeddata2).email).toBe(useremail);
                      expect(lastBooking(bookeddata2).gameid).toBe(2987);
                      expect(lastBooking(bookeddata2).date - fromPlus120).toBe(0);
                      expect(lastBooking(bookeddata2).timeend - toPlus120).toBe(0);

                      gameEvents.TaskBookGameBySlug(gameEvent, slug, useremail, 2987, fromMinus30, toMinus30, ref).fork(
                        err => {
                          expect(err).toBe('Double Booked');
                          done();
                        },
                        bookeddata3 => done(bookeddata3)
                      );
                    }
                  );
                }
              )
            }, done 
          )
        }
      );
    });
  });


  it('TaskBookGame', function(done) {

    const name = 'taskbookgame';
    const slug = 'taskbookgameslug';

    const eventObj = {
      name: name,
      pageslug: slug,
      loc: {
        coordinates: [faker.Address.longitude(), faker.Address.latitude()],
        type: 'Point'
      },
      games:[{
        id: 2987,
        name: "Pirate's Cove",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }]
    };

    const getKey = R.prop('key');
    const getId = R.prop('objectid');

    const useremail = faker.Internet.email();
    const from = new Date().getTime();
    const to = new Date().getTime() + (60*60*1000);

    const fromPlus120 = from + (120*60*1000);
    const toPlus120 = to + (120*60*1000);

    const fromMinus30 = from - (30*60*1000);
    const toMinus30 = to - (30*60*1000);

    const ref = "abc";

    const lastBooking = R.compose(R.last, R.prop('bookings'))

    gameEvent.remove({pageslug:slug}).exec().then(function(x) {

      gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
        err => done(err),
        data => {
          gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 2987, to, from, ref).fork(
            err => {
              gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 2987, from, to, ref).fork(
                err => done(err),
                bookeddata => {
                  expect(lastBooking(bookeddata).bookingref).toBe(ref);
                  expect(lastBooking(bookeddata).email).toBe(useremail);
                  expect(lastBooking(bookeddata).gameid).toBe(2987);
                  expect(lastBooking(bookeddata).date - from).toBe(0);
                  expect(lastBooking(bookeddata).timeend - to).toBe(0);
                  gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 2987, fromPlus120, toPlus120, ref).fork(
                    err => done(err),
                    bookeddata2 => {
                      expect(lastBooking(bookeddata2).bookingref).toBe(ref);
                      expect(lastBooking(bookeddata2).email).toBe(useremail);
                      expect(lastBooking(bookeddata2).gameid).toBe(2987);
                      expect(lastBooking(bookeddata2).date - fromPlus120).toBe(0);
                      expect(lastBooking(bookeddata2).timeend - toPlus120).toBe(0);
                      gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 2987, fromMinus30, toMinus30, ref).fork(
                        err => {
                          expect(err).toBe('Double Booked');
                          done();
                        },
                        bookeddata3 => done(bookeddata3)
                      );
                    }
                  );
                }
              )
            }, done 
          )
        }
      );
    });

  });


  it('TaskReadEventPlayerBookingsBySlug', function(done) {

    const name = 'taskreadgame';
    const slug = 'taskreadgameslug';

    const eventObj = {
      name: name,
      pageslug: slug,
      loc: {
        coordinates: [faker.Address.longitude(), faker.Address.latitude()],
        type: 'Point'
      },
      games:[{
        id: 164153,
        name: "Star Wars: Imperial Assault",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }]
    };

    const getKey = R.prop('key');
    const getId = R.prop('objectid');
    const getName = R.path(['name', 't']);
    const getThumb = R.prop('thumbnail');

    const useremail = faker.Internet.email();
    const useremail2 = faker.Internet.email();
    const from = new Date().getTime();
    const to = new Date().getTime() + (60*60*1000);

    const fromPlus120 = from + (120*60*1000);
    const toPlus120 = to + (120*60*1000);

    const ref = "abc";

    const lastBooking = R.compose(R.last, R.prop('bookings'))

    gameEvent.remove({pageslug:slug}).exec().then(function(x) {
      gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
        err => done(err),
        data => {
          R.sequence(Task.of, [gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, from, to, ref), gameEvents.TaskBookGame(gameEvent, getKey(data), useremail2, 164153, fromPlus120, toPlus120, ref)]).fork(
            err => done(err),
            bookeddata => {
              gameEvents.TaskReadEventPlayerBookingsBySlug(gameEvent, slug, useremail).fork(
                err => done(err),
                readdata => {
                  expect(readdata.bookings.length).toBe(1);
                  expect(readdata.bookings[0].gameid).toBe(164153);
                  expect(readdata.bookings[0].email).toBe(useremail);
                  expect(readdata.bookings[0].name).toBe('Star Wars: Imperial Assault');
                  expect(readdata.bookings[0].thumbnail).toBe('https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png');
                  done()
                }
              )
            }
          )
        }
      );
    });

  });


  it('TaskReadEventBookings', function(done) {

    const name = 'taskreadgame';
    const slug = 'taskreadgameslug';

    const eventObj = {
      name: name,
      pageslug: slug,
      loc: {
        coordinates: [faker.Address.longitude(), faker.Address.latitude()],
        type: 'Point'
      },
      games:[{
        id: 164153,
        name: "Star Wars: Imperial Assault",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }, {
        id: 226562,
        name: "Immortals",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }]
    };

    const getKey = R.prop('key');
    const getId = R.prop('objectid');

    const useremail = faker.Internet.email();
    const from = new Date().getTime();
    const to = new Date().getTime() + (60*60*1000);

    const fromPlus120 = from + (120*60*1000);
    const toPlus120 = to + (120*60*1000);

    const ref = "abc";

    const lastBooking = R.compose(R.last, R.prop('bookings'))

    gameEvent.remove({pageslug:slug}).exec().then(function(x) {
      gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
        err => done(err),
        data => {
          R.sequence(Task.of, [gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, from, to, ref), gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, fromPlus120, toPlus120, ref), gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 226562, from, to, ref)]).fork(
            err => done(err),
            bookeddata => {
              gameEvents.TaskReadEventBookings(gameEvent, getKey(data), 164153).fork(
                err => done(err),
                readdata => {
                  expect(readdata.game.id).toBe(164153);
                  expect(readdata.bookings.length).toBe(2);
                  expect(readdata.bookings[0].gameid).toBe(164153);
                  expect(readdata.bookings[1].gameid).toBe(164153);
                  done()
                }
              )
            }
          )
        }
      );
    });

  });


  it('TaskDeleteEventBookingsBySlug', function(done) {

    const name = 'taskdeletegameby';
    const slug = 'taskdeletegamebyslug';

    const useremail = faker.Internet.email();
    const adminemail = faker.Internet.email();

    const eventObj = {
      name: name,
      pageslug: slug,
      loc: {
        coordinates: [faker.Address.longitude(), faker.Address.latitude()],
        type: 'Point'
      },
      users: [
        {email: adminemail, type: 'admin'}
      ],
      games:[{
        id: 164153,
        name: "Star Wars: Imperial Assault",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }]
    };

    const getKey = R.prop('key');
    const getId = R.prop('objectid');

    const from = new Date().getTime();
    const to = new Date().getTime() + (60*60*1000);

    const fromPlus120 = from + (120*60*1000);
    const toPlus120 = to + (120*60*1000);

    const ref = "abc";
    const lastBooking = R.compose(R.last, R.prop('bookings'))

    const bademail = "bad@email.com";

    gameEvent.remove({pageslug:slug}).exec().then(function(x) {
      gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
        err => done(err),
        data => {
          R.sequence(Task.of, [gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, from, to, ref), gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, fromPlus120, toPlus120, ref)]).fork(
            err => done(err),
            bookeddata => gameEvents.TaskReadEventBySlug(gameEvent, slug).fork(
              err => done(err),
              readdata => gameEvents.TaskDeleteEventBookingsBySlug(gameEvent, bademail, slug, readdata.bookings[0].code).fork(
                err => gameEvents.TaskDeleteEventBookingsBySlug(gameEvent, useremail, slug, readdata.bookings[0].code).fork(
                  err => done(err),
                  readdelete => {
                    expect(readdelete.n).toBe(1);
                    gameEvents.TaskReadEventBySlug(gameEvent, slug).fork(
                      err => done(err),
                      datax => {
                        expect(datax.bookings.length).toBe(1);
                        gameEvents.TaskDeleteEventBookingsBySlug(gameEvent, adminemail, slug, readdata.bookings[1].code).fork(
                          err => done(err),
                          datay => {
                            expect(readdelete.n).toBe(1);
                            done()
                          }
                        )
                      }
                    )
                  }
                ),
                baddelete => done(baddelete)
              )
            )
          )
        }
      );
    });
  });


  it('TaskBookingCheckOut', function(done) {

    const name = 'taskbookinggameby';
    const slug = 'taskbookinggamebyslug';

    const useremail = faker.Internet.email();
    const adminemail = faker.Internet.email();

    const eventObj = {
      name: name,
      pageslug: slug,
      loc: {
        coordinates: [faker.Address.longitude(), faker.Address.latitude()],
        type: 'Point'
      },
      users: [
        {email: adminemail, type: 'admin'}
      ],
      games:[{
        id: 164153,
        name: "Star Wars: Imperial Assault",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }]
    };

    const getKey = R.prop('key');

    const from = new Date().getTime();
    const to = new Date().getTime() + (60*60*1000);

    const fromPlus120 = from + (120*60*1000);
    const toPlus120 = to + (120*60*1000);

    const ref = "abc";
    const lastBooking = R.compose(R.last, R.prop('bookings'))

    const bademail = "bad@email.com";

    gameEvent.remove({pageslug:slug}).exec().then(function(x) {
      gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
        err => done(err),
        data => {
          R.sequence(Task.of, [gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, from, to, ref), gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, fromPlus120, toPlus120, ref)]).fork(
            err => done(err),
            bookeddata => gameEvents.TaskBookingCheckOut(gameEvent, adminemail, slug, bookeddata[0].bookings[0].gameid, bookeddata[0].bookings[0].code, 'inuse').fork(
              err => done(err),
              data => {
                expect(data.games[0].status).toBe('inuse');
                expect(data.bookings[0].status).toBe('inuse');
                expect(data.bookings[1].status).toBe('waiting');
                gameEvents.TaskBookingCheckOut(gameEvent, adminemail, slug, bookeddata[0].bookings[0].gameid, bookeddata[0].bookings[0].code, 'returned').fork(
                  err => done(err),
                  data => {
                    expect(data.games[0].status).toBe('returned');
                    expect(data.bookings[0].status).toBe('returned');
                    expect(data.bookings[1].status).toBe('waiting');
                    done();
                  }
                )
              }
            )
          )
        }
      );
    });
  });


  it('TaskReadEventBookingsBySlug', function(done) {

    const name = 'taskreadgameby';
    const slug = 'taskreadgamebyslug';

    const eventObj = {
      name: name,
      pageslug: slug,
      loc: {
        coordinates: [faker.Address.longitude(), faker.Address.latitude()],
        type: 'Point'
      },
      games:[{
        id: 164153,
        name: "Star Wars: Imperial Assault",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }, {
        id: 226562,
        name: "Immortals",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }]

    };

    const getKey = R.prop('key');

    const useremail = faker.Internet.email();
    const from = new Date().getTime();
    const to = new Date().getTime() + (60*60*1000);

    const fromPlus120 = from + (120*60*1000);
    const toPlus120 = to + (120*60*1000);

    const ref = "abc";

    const lastBooking = R.compose(R.last, R.prop('bookings'))

    gameEvent.remove({pageslug:slug}).exec().then(function(x) {
      gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
        err => done(err),
        data => {
          R.sequence(Task.of, [gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, from, to, ref), gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, fromPlus120, toPlus120, ref), gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 226562, from, to, ref)]).fork(
            err => done(err),
            bookeddata => {
              gameEvents.TaskReadEventBookingsBySlug(gameEvent, slug, 164153).fork(
                err => done(err),
                readdata => {
                  expect(readdata.game.id).toBe(164153);
                  expect(readdata.bookings.length).toBe(2);
                  expect(readdata.bookings[0].gameid).toBe(164153);
                  expect(readdata.bookings[1].gameid).toBe(164153);
                  done()
                }
              )
            }
          )
        }
      );
    });
  });

  it('isTimeBlocked', function(done) {
      const d1 = new Date().getTime();
      const d2 = new Date().getTime() + (30 + 60 + 1000);
      const d3 = new Date().getTime() + (60 + 60 + 1000);
      const d4 = new Date().getTime() + (90 + 60 + 1000);
      const d5 = new Date().getTime() + (120 + 60 + 1000);

      expect(gameEvents.IsTimeBlocked(d1, d2, d3, d4)).toBe(true);
      expect(gameEvents.IsTimeBlocked(d1, d2, d4, d5)).toBe(true);
      expect(gameEvents.IsTimeBlocked(d2, d3, d4, d5)).toBe(true);
      expect(gameEvents.IsTimeBlocked(d3, d4, d1, d2)).toBe(true);
      expect(gameEvents.IsTimeBlocked(d4, d5, d1, d2)).toBe(true);

      expect(gameEvents.IsTimeBlocked(d1, d3, d2, d4)).toBe(false);
      expect(gameEvents.IsTimeBlocked(d2, d4, d1, d3)).toBe(false);

      done();
  });

  it('TaskVerifyBookingTimes', function(done) {

    const name = 'taskcheckverify';
    const slug = 'taskcheckverifyslug';

    const eventObj = {
      name: name,
      pageslug: slug,
      loc: {
        coordinates: [faker.Address.longitude(), faker.Address.latitude()],
        type: 'Point'
      },
      games:[{
        id: 164153,
        name: "Star Wars: Imperial Assault",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }, {
        id: 226562,
        name: "Immortals",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }]

    };

    const getKey = R.prop('key');

    const useremail = faker.Internet.email();
    const from = new Date().getTime();
    const to = new Date().getTime() + (60*60*1000);

    const fromPlus120 = from + (120*60*1000);
    const toPlus120 = to + (120*60*1000);

    const ref = "abc";

    const lastBooking = R.compose(R.last, R.prop('bookings'))

    gameEvent.remove({pageslug:slug}).exec().then(function(x) {
      gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
        err => done(err),
        data => {
          R.sequence(Task.of, [gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, from, to, ref), gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, fromPlus120, toPlus120, ref), gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 226562, from, to, ref)]).fork(
            err => done(err),
            bookeddata => {
              R.sequence(Task.of, [gameEvents.TaskVerifyBookingTimes(gameEvent, getKey(data), 164153, fromPlus120, toPlus120), gameEvents.TaskVerifyBookingTimes(gameEvent, getKey(data), 226562, fromPlus120, toPlus120)]).fork(
                err => done(err),
                readdata => {
                  expect(readdata[0]).toBe(false);
                  expect(readdata[1]).toBe(true);
                  done()
                }
              );
            }
          );
      });
    });

  });



  it('TaskVerifyBookingTimesBySlug', function(done) {
    const name = 'taskcheckverifyBS';
    const slug = 'taskcheckverifyBSslug';

    const eventObj = {
      name: name,
      pageslug: slug,
      loc: {
        coordinates: [faker.Address.longitude(), faker.Address.latitude()],
        type: 'Point'
      },
      games:[{
        id: 164153,
        name: "Star Wars: Imperial Assault",
        thumbnail: 'https://cf.geekdo-images.com/jPI3dEW7-ya6ApeFLpRr8FuuTrg=/fit-in/246x300/pic3013621.png',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }, {
        id: 192455,
        name: "First Martians: Adventures on the Red Planet",
        thumbnail: 'https://cf.geekdo-images.com/lmWMoEenbnYExiynQKl2bpoVtVI=/fit-in/246x300/pic3151415.jpg',
        description: 'hello',
        minplaytime: 30,
        maxplaytime: 60,
        minplayers: 2,
        maxplayers: 4
      }]
    };

    const getKey = R.prop('key');

    const useremail = faker.Internet.email();
    const from = new Date().getTime();
    const to = new Date().getTime() + (60*60*1000);

    const fromPlus120 = from + (120*60*1000);
    const toPlus120 = to + (120*60*1000);

    const ref = "abc";

    const lastBooking = R.compose(R.last, R.prop('bookings'))

    gameEvent.remove({pageslug:slug}).exec().then(function(x) {
      gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
        err => done(err),
        data => {
          R.sequence(Task.of, [gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, from, to, ref), gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 164153, fromPlus120, toPlus120, ref), gameEvents.TaskBookGame(gameEvent, getKey(data), useremail, 192455, from, to, ref)]).fork(
            err => done(err),
            bookeddata => {
              R.sequence(Task.of, [gameEvents.TaskVerifyBookingTimesBySlug(gameEvent, slug, 164153, fromPlus120, toPlus120), gameEvents.TaskVerifyBookingTimesBySlug(gameEvent, slug, 192455, fromPlus120, toPlus120)]).fork(
                err => done(err),
                readdata => {
                  expect(readdata[0]).toBe(false);
                  expect(readdata[1]).toBe(true);
                  done()
                }
              )
            }
          )
        }
      );
    });

  });


  it('TaskIsUserAdmin', function(done) {

      const name = 'testadmin';
      const slug = 'testadminslug';

      const useremail = faker.Internet.email();
      const adminemail = faker.Internet.email();

      const eventObj = {
          name: name,
          pageslug: slug,
          loc: {
              coordinates: [faker.Address.longitude(), faker.Address.latitude()],
              type: 'Point'
          },
          users: [
              {
                  email: adminemail,
                  type: 'admin'
              }
          ]
      };

      gameEvent.remove({pageslug:slug}).exec().then(function(x) {
          gameEvents.TaskCreateEvent(gameEvent, eventObj).fork(
              err => done(err),
              event => {
                  R.sequence(Task.of, [ gameEvents.TaskIsUserAdmin(gameEvent, slug, useremail), gameEvents.TaskIsUserAdmin(gameEvent, slug, adminemail)]).fork(
                      err => done(err),
                      data => {
                          expect(data[0]).toBe(false);
                          expect(data[1]).toBe(true);
                          done();
                      }
                  )
              }
          );
      });

  });




  it("close server", function(done) {
      mongo.CloseDatabase();
      done();
  });

});
