var Promise = require('bluebird');
var R = require('ramda');
var util = require('util')
var log = require('../app/log.js');

var passport = require('passport');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var users_page = require('../app/users_page.js');

var testBoardgame = require('../app/models/boardgame.js');
var testBoardgames = require('../app/models/boardgames.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var match = require('../app/models/match.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');
var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 3917;
var login = require('connect-ensure-login');

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);


// ============== END SERVER FOR TESTS =======
var closeServer = function() {
    testapp.close(function(){
        console.log('matches_service_spec closed on port ' + httpPort);
    });
};


// ============== RUN TESTS ================== 
describe("users_page_spec.js", function() {

    var server;

    // loginUser :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
    var verifyregister = function(account, zombieb, email, pass, userobj) {
        return new Promise(function(fulfill, reject) {
            accounts.DeleteAccount(account, email).then(function(y) {
                var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

                register(email, pass).then(function(x) {
                    account.update({email: email}, userobj).exec().then(function(z) {
                        zombieb.visit(base_url + "login", function(err) {
                            if (err) 
                                reject(err);

                            zombieb.fill('input[name="form-username"]', email)
                            zombieb.fill('input[name="form-password"]', pass)
                            zombieb.pressButton('login', function() {
                                fulfill(x);
                            });
                        });
                    });
                });
            });
        });
    };

    describe("web page tests", function() {

        var userEmail = faker.Internet.email().toLowerCase();
        var userPass = 'testpassword';
        var userValues = {};

        it("setup server", function(done) {

			testapp.get('/', function (req, res) {
				res.send('<html>Home Page</html>')
			});

            users_page.RouteUser(testapp, login);

            login_page.RouteLogins(testapp, passport);
            login_page.PostLogin(testapp, passport);
            login_page.RouteVerify(testapp, account);
            server = testapp.listen(httpPort, function() {
                done();
            });
        });


		it("non logged in users visit users", function(done) {
            page = 'users';
            browser.visit(base_url + page, function(err) {
                expect(browser.success).toBe(true);
                expect(browser.html()).toContain('Log in with');
                done();
            });
		});

		it("user login", function(done) {
            verifyregister(account, browser, userEmail, userPass, {}).then(function(x) {
               done();
            });
		});


		it("PushPanel", function(done) {

            var x = 'abc';
            var y = 'def';

            const a = {panels:[]};
            const b = {panels:[x]};
            const c = {};

            expect(users_page.PushPanel(a, x)).toEqual({panels:[x]});
            expect(users_page.PushPanel(a, y)).toEqual({panels:[y]});

            expect(users_page.PushPanel(b, x)).toEqual({panels:[x, x]});
            expect(users_page.PushPanel(b, y)).toEqual({panels:[x, y]});

            expect(users_page.PushPanel(c, x)).toEqual({panels:[x]});
            expect(users_page.PushPanel(c, y)).toEqual({panels:[y]});

            done();

        });

		it("buildTextPanel", function(done) {

            var value2 = 'valueA';
            
            var text = 'te';
            var divclass = 'dc';
            var fa = 'faicon';
            var id = 'id';

            const a = {};

            expect(users_page.BuildUserPanel(value2, id, divclass,text, fa, a)).toEqual({ 
                userpanels : { 
                    id: id,
                    divclass: divclass,
                    heading : { 
                        text : 'te', 
                        faicon : 'faicon' 
                    }, 
                    body : { 
                        text : 'valueA' 
                    } 
                } 
            });

            done();

        });

        it("PushTitleItem", function(done) {

            var lookup = {
                a: {title: 'atitle'}, 
                b: {title: 'btitle'}, 
                c: {title: 'ctitle'}
            };

            var data = {
                a: 'abc', 
                b: 'bcd'
            };

            expect(users_page.PushTitleitem(lookup, data, 'a', [])).toEqual([{title: 'atitle', text: 'abc'}]);
            expect(users_page.PushTitleitem(lookup, data, 'b', ['hithere'])).toEqual(['hithere', {title: 'btitle', text: 'bcd'} ]);
            expect(users_page.PushTitleitem(lookup, data, 'c', ['hithere'])).toEqual(['hithere']);
            done();   

        });

        it("PushALinkItem", function(done) {

            var lookup = {
                a: {url: 'aurl/'}, 
                b: {url: 'burl/'}, 
                c: {url: 'curl/'} 
            };

            var data = {
                a: 'abc', 
                b: 'bcd'
            };

            expect(users_page.PushALinkitem(lookup, data, 'a', [])).toEqual([{url: 'aurl/', a:{href:'aurl/abc', text: 'abc'}}]);
            expect(users_page.PushALinkitem(lookup, data, 'b', ['hithere'])).toEqual(['hithere', {url: 'burl/', a:{href:'burl/bcd', text: 'bcd'}}]);
            expect(users_page.PushALinkitem(lookup, data, 'c', ['hithere'])).toEqual(['hithere']);
            done();   

        });

        it("CreateContactList", function(done) {
            var userData = {
                commpsn: "psnname",
                commskype: "skypename",
                commsteam: "steamname"
            };
            var userDataEmail = {
                commemailvisible: true
            };

            var objExpect = [
                { faicon : 'fa-steam-square',   title: 'Steam',     text : 'steamname' }, 
                { faicon : 'fa-skype',          title: 'Skype',     text : 'skypename' }, 
                { faicon : 'fa-gamepad',        title: 'PSN',       text : 'psnname' }
            ];

            expect(users_page.CreateContactList(userData)).toEqual(objExpect);
            done();

        });

        it("CreateSocialList", function(done) {
            var userData = {
                socialfacebook: "facebookname",
                socialtwitter: "twittername",
                socialwebsite: "website"
            };

            var objExpect = [
                { faicon : 'fa-globe', title: 'Web', url:'http://', a:{href:"http://website", text: 'website' }},
                { faicon : 'fa-twitter-square', title: 'Twitter', url:'http://www.twitter.com/', a:{href:"http://www.twitter.com/twittername", text: 'twittername' }}, 
                { faicon : 'fa-facebook-square', title: 'Facebook', url:'http://www.facebook.com/', a:{href:"http://www.facebook.com/facebookname", text: 'facebookname' }}, 
            ];

            expect(users_page.CreateSocialList(userData)).toEqual(objExpect);
            done();

        });

        it("PushListItem", function(done) {

            var lookup = {a: 'abc', b: 'bcd', c: 'cde'};
            var data = {a: true, b: false};
            expect(users_page.PushListitem(lookup, data, 'a', [])).toEqual(['abc']);
            expect(users_page.PushListitem(lookup, data, 'b', ['hithere'])).toEqual(['hithere']);
            expect(users_page.PushListitem(lookup, data, 'c', ['hithere'])).toEqual(['hithere']);
            done();   

        });
        
        // TODO: add in half times
        it("ColourDay", function(done) {
            expect(users_page.ColourDay(10, false, 8, 0, 20, 0)).toBe(0); 
            expect(users_page.ColourDay(10, true, 8, 0, 20, 0)).toBe(2); 
            expect(users_page.ColourDay(10, true, 10, 0, 20, 0)).toBe(2); 
            expect(users_page.ColourDay(10, true, 10, 30, 20, 0)).toBe(2); 
            expect(users_page.ColourDay(10, true, 12, 0, 20, 0)).toBe(0); 
            expect(users_page.ColourDay(10, true, 8, 0, 9, 0)).toBe(0); 
            expect(users_page.ColourDay(10, true, 8, 0, 10, 0)).toBe(0); 
            expect(users_page.ColourDay(10, true, 8, 0, 10, 30)).toBe(2); 
            done();
        });

        it("FindProps", function(done) {
            var obj = {a: 1, b: 2, c: 3};
            expect(users_page.FindProps(obj, ['a', 'b'])).toEqual([1, 2]);
            expect(users_page.FindProps(obj, ['b', 'c'])).toEqual([2, 3]);
            done();
        });

        it("formatTime", function(done) {
            expect(users_page.FormatTime(0)).toBe('0:00 AM');
            expect(users_page.FormatTime(12)).toBe('12:00 PM');
            expect(users_page.FormatTime(13)).toBe('1:00 PM');
            expect(users_page.FormatTime(22)).toBe('10:00 PM');
            done();
        });

        it("BuildRect", function(done) {
            var expectObj = {x: 'x', y: 'y', width: 'w', height: 'h', fill: 'f', tooltip:{title: 't'}};
            expect(users_page.BuildRect('x', 'y', 'w', 'h', 'f', 't'));
            done();
        });

        it("BuildGYTransform", function(done) {
            var expectObj = { 
                transform : 
                    { left : 'l', top : 't' }, 
                    rects : [ 
                        { x : 0, y : 1, width : 15, height : 15, fill : 2, tooltip : { title : 'a' } }, 
                        { x : 0, y : 3, width : 15, height : 15, fill : 4, tooltip : { title : 'b' } } ] 
            };
            expect(users_page.BuildGYTransform('l', 't', [[1, 2, 'a'], [3, 4, 'b']])).toEqual(expectObj);
            done();
        });

        it("BuildGXTransform", function(done) {
            var expectObj = { transform : { left : 'l', top : 't' }, rects : [ { x : 1, y : 0, width : 15, height : 15, fill : 2, tooltip : { title : 'a' } }, { x : 3, y : 0, width : 15, height : 15, fill : 4, tooltip : { title : 'b' } } ] };
            expect(users_page.BuildGXTransform('l', 't', [[1, 2, 'a'], [3, 4, 'b']])).toEqual(expectObj);
            done();
        });

        it("BuildGDirection", function(done) {
            var expectObj = { direction : { type : 't' }, texts : [ { x : 33, y : 10, anchor : '', class : '', text : '0:00 AM' }, { x : 135, y : 10, anchor : '', class : '', text : '6:00 AM' }, { x : 237, y : 10, anchor : '', class : '', text : '12:00 PM' }, { x : 339, y : 10, anchor : '', class : '', text : '6:00 PM' } ] };
            var times = [ [ 33, '0:00 AM' ], [ 135, '6:00 AM' ], [ 237, '12:00 PM' ], [ 339, '6:00 PM' ] ]; 
            expect(users_page.BuildGDirection('t', times)).toEqual(expectObj);  
            done();
        });

        it("BuildG", function(done) {
            var expectObj = { texts : [ { x : 16, y : 'y', anchor : 'middle', class : '', text : 0 }, { x : 16, y : 'z', anchor : 'middle', class : '', text : 1 } ] };
            expect(users_page.BuildG([['y', 0], ['z', 1]])).toEqual(expectObj);
            done();
        });

        it("BuildText", function(done) {
            var expectObj = {x: 'x', y: 'y', anchor: 'a', class: 'c', text: 't'};
            expect(users_page.BuildText('x', 'y', 'a', 'c', 't')).toEqual(expectObj);
            done();
        });

        it("BuildHourColumn", function(done) {
            var obj = {'Tuesday': true, 'tuesdaystarthour': 8, 'tuesdaystartminute': 20, 'tuesdayendhour': 14, 'tuesdayendminute': 40}; 
            expect(users_page.BuildHourColumn(obj, 4)).toEqual( [4, 101, 0, 0, 0, 0, 0, 0, 0] ); 
            expect(users_page.BuildHourColumn(obj, 8)).toEqual( [8, 169, 0, 2, 0, 0, 0, 0, 0] ); 
            expect(users_page.BuildHourColumn(obj, 10)).toEqual( [10, 203, 0, 2, 0, 0, 0, 0, 0] ); 
            expect(users_page.BuildHourColumn(obj, 14)).toEqual( [14, 271, 0, 2, 0, 0, 0, 0, 0] ); 
            expect(users_page.BuildHourColumn(obj, 22)).toEqual( [22, 407, 0, 0, 0, 0, 0, 0, 0] ); 
            done();
        });

        it("BuildColumn", function(done) {
            var expectObj = { transform : { left : 'x', top : 18 }, rects : [ { x : 0, y : 0, width : 15, height : 15, fill : '#EBF4FC', tooltip : { title : 'Monday 2:00 AM' } }, { x : 0, y : 17, width : 15, height : 15, fill : '#C9E4EC', tooltip : { title : 'Tuesday 2:00 AM' } }, { x : 0, y : 34, width : 15, height : 15, fill : '#96CCD4', tooltip : { title : 'Wednesday 2:00 AM Available' } }, { x : 0, y : 51, width : 15, height : 15, fill : '#52ACB4', tooltip : { title : 'Thursday 2:00 AM Available' } }, { x : 0, y : 68, width : 15, height : 15, fill : '#32686C', tooltip : { title : 'Friday 2:00 AM Available' } }, { x : 0, y : 85, width : 15, height : 15, fill : '#EBF4FC', tooltip : { title : 'Saturday 2:00 AM' } }, { x : 0, y : 102, width : 15, height : 15, fill : '#C9E4EC', tooltip : { title : 'Sunday 2:00 AM' } } ] };
            expect(users_page.BuildColumn(2, 'x', 0, 1, 2, 3, 4, 0, 1)).toEqual(expectObj);
            done();
        });

        it("CreateSVG", function(done) {
            var userData = {
                ishost: 2,
                isteacher: 2,
                nodriving: false,
                playersmoking: 1 
            };

            var checkobj = R.whereEq({
                width: 447,
                height: 167,
                htmlclass: ''
            }); 

            expect(checkobj(users_page.CreateSVG(userData))).toBe(true);
            done();

        });


        it("CreateCommunityList", function(done) {
            var userData = {
                ishost: 2,
                isteacher: 2,
                nodriving: false,
                playersmoking: 1 
            };

			var objExpect = [ { faicon : 'fa-home', text : 'Host', tooltip : { title : 'I have access to a venue for hosting games', placement : 'right' } }, { faicon : 'fa-graduation-cap', text : 'Game Teacher', tooltip : { title : 'I am willing to teach game rules to new players', placement : 'right' } }, { faicon : 'fa-car', text : 'Driver', tooltip : { title : 'I can drive to venues', placement : 'right' } }, { faicon : 'fa-ban', text : 'No Smoking', tooltip : { title : 'I require non-smoking venues', placement : 'right' } } ];

            expect(users_page.CreateCommunityList(userData)).toEqual(objExpect);
            done();

        });

        it("CreateWTPList", function(done) {
            var userData = { wanttoplay: 
			   [ { numplays: 0,
				   thumbnail: 'https://cf.geekdo-images.com/images/pic3581400_t.png',
				   image: 'https://cf.geekdo-images.com/images/pic3581400.png',
				   yearpublished: 2017,
				   id: 228660,
				   name: 'Betrayal at Baldur\'s Gate',
				   bggcache: 
					{ objecttype: 'thing',
					  objectid: 228660,
					  subtype: 'boardgame',
					  collid: 42985013,
					  name: { sortindex: 1, t: 'Betrayal at Baldur\'s Gate' },
					  yearpublished: 2017,
					  image: 'https://cf.geekdo-images.com/images/pic3581400.png',
					  thumbnail: 'https://cf.geekdo-images.com/images/pic3581400_t.png',
					  status: 
					   { own: 0,
						 prevowned: 0,
						 fortrade: 0,
						 want: 0,
						 wanttoplay: 1,
						 wanttobuy: 0,
						 wishlist: 0,
						 preordered: 0,
						 lastmodified: '2017-06-05 00:23:34' },
					  numplays: 0 },
				   _id: '5934eadeed189352a56e682c',
				   matching: 
					{ own: false,
					  wanttoplay: true,
					  avoid: false,
					  matchcount: 0,
					  personalrating: 0,
					  socialrating: 0,
					  skillrating: 0,
					  teach: false },
				   active: true,
				   activestatus: 'active',
				   wanttoplay: 0 },
				 { numplays: 0,
				   thumbnail: 'https://cf.geekdo-images.com/images/pic2437871_t.jpg',
				   image: 'https://cf.geekdo-images.com/images/pic2437871.jpg',
				   yearpublished: 2017,
				   id: 174430,
				   name: 'Gloomhaven',
				   bggcache: 
					{ objecttype: 'thing',
					  objectid: 174430,
					  subtype: 'boardgame',
					  collid: 42155013,
					  name: { sortindex: 1, t: 'Gloomhaven' },
					  yearpublished: 2017,
					  image: 'https://cf.geekdo-images.com/images/pic2437871.jpg',
					  thumbnail: 'https://cf.geekdo-images.com/images/pic2437871_t.jpg',
					  status: 
					   { own: 0,
						 prevowned: 0,
						 fortrade: 0,
						 want: 0,
						 wanttoplay: 1,
						 wanttobuy: 0,
						 wishlist: 0,
						 preordered: 0,
						 lastmodified: '2017-04-27 17:29:13' },
					  numplays: 0 },
				   _id: '5934eadeed189352a56e6831',
				   matching: 
					{ own: false,
					  wanttoplay: true,
					  avoid: false,
					  matchcount: 0,
					  personalrating: 0,
					  socialrating: 0,
					  skillrating: 0,
					  teach: false },
				   active: true,
				   activestatus: 'active',
				   wanttoplay: 0 } ] };

            var objExpect = [ { name : "Betrayal at Baldur's Gate", thumbnail : 'https://cf.geekdo-images.com/images/pic3581400_t.png', yearpublished : 2017, id : 228660, text : "Betrayal at Baldur's Gate (2017)", img : { src : 'https://cf.geekdo-images.com/images/pic3581400_t.png', href : 'https://boardgamegeek.com/boardgame/228660'} }, { name : 'Gloomhaven', thumbnail : 'https://cf.geekdo-images.com/images/pic2437871_t.jpg', yearpublished : 2017, id : 174430, text : 'Gloomhaven (2017)', img : { src : 'https://cf.geekdo-images.com/images/pic2437871_t.jpg', href : 'https://boardgamegeek.com/boardgame/174430' } } ];

            expect(users_page.CreateWTPList(userData)).toEqual(objExpect);
            done();

        });

        it("CreateMotiveList", function(done) {
            var userData = {
                motivecooperation: true,
                motivediscovery: true,
                motivesocialmanipulation: true,
                motivecommunity: true
            };

            var objExpect = [
                {faicon: 'fa-handshake-o', text: 'Cooperation'},
                {faicon: 'fa-gavel', text: 'Social Manipulation'},
                {faicon: 'fa-search-plus', text: 'Discovery'},
                {faicon: 'fa-users', text: 'Community'},
            ];

            expect(users_page.CreateMotiveList(userData)).toEqual(objExpect);
            done();

        });

        it("setSVG", function(done) {
            var svg = "svg";
            var obj = {userpanels: {}};
            var objExpect = {userpanels: {svg: 'svg'}};

            expect(users_page.SetSVG(svg, obj)).toEqual(objExpect);
            done();
        });

        it("setGroup", function(done) {
            var listdata = "listdata";
            var obj = {userpanels: {}};
            var objExpect = {userpanels: {listgroup: 'listdata'}};

            expect(users_page.SetGroup('listgroup', listdata, obj)).toEqual(objExpect);
            done();
        });

		it("assocSVG", function(done) {

      var value1 = '';
      var value2 = 'valueA';
      
      var text = 'te';
      var divclass = 'dc';
      var fa = 'faicon';
      var id = 'id';

      const a = {};

      expect(users_page.AssocSVGPanel(value1, id, divclass, text, fa, 'svga', a)).toEqual({ 
          panels : [ 
              { 
                  userpanels : { 
                      id: id,
                      divclass: divclass,
                      heading : { 
                          text : 'te', 
                          faicon : 'faicon' 
                      }, 
                  body : { 
                      text : '' 
                  },
                  svg: 'svga'
              } 
          }] 
      });

      expect(users_page.AssocSVGPanel(value2, id, divclass,text, fa, 'svgb', a)).toEqual({ 
          panels : [ 
              { 
                  userpanels : { 
                      id: id,
                      divclass: divclass,
                      heading : { 
                          text : 'te', 
                          faicon : 'faicon' 
                      }, 
                  body : { 
                      text : 'valueA' 
                  },
                  svg: 'svgb'
              } 
          }] 
      });

      done();

    });

    it('mergeWhenRender', function(done) {
      expect(users_page.mergeWhenRender(false, {a:'b'})).toEqual({a:'b'});
      expect(users_page.mergeWhenRender(true, {a:'b'})).toEqual({render:true, a:'b'});

      done();
    });


		it("assocTextPanel", function(done) {

      var value1 = '';
      var value2 = 'valueA';
      
      var text = 'te';
      var divclass = 'dc';
      var fa = 'faicon';
      var id = 'id';

      const a = {};

      expect(users_page.AssocTextPanel(value1, id, divclass, text, fa, false, a)).toEqual(a);

      expect(users_page.AssocTextPanel(value2, id, divclass, text, fa, false, a)).toEqual({ 
        panels : [ 
          { 
            userpanels : { 
              id: id,
              divclass: divclass,
              heading : { 
                text : 'te', 
                faicon : 'faicon' 
              }, 
            body : { 
              text : 'valueA' 
            } 
          } 
        }] 
      });

      done();

    });

		it("assocTablePanel", function(done) {

            var value1 = '';
            var value2 = 'valueA';
            
            var text = 'te';
            var divclass = 'dc';
            var fa = 'faicon';
            var id = 'id';

            const a = {};

            expect(users_page.AssocTablePanel(value1, id, divclass, text, fa, 'table', a)).toEqual({
                panels : [ 
                    { 
                        userpanels : { 
                            id: id,
                            divclass: divclass,
                            heading : { 
                                text : 'te', 
                                faicon : 'faicon' 
                            }, 
                            body : { 
                                text : '' 
                            }, 
                            table: 'table'
                        } 
                    }
                ] 
            });

            expect(users_page.AssocTablePanel(value2, id, divclass,text, fa, 'table', a)).toEqual({ 
                panels : [ 
                    { 
                        userpanels : { 
                            id: id,
                            divclass: divclass,
                            heading : { 
                                text : 'te', 
                                faicon : 'faicon' 
                            }, 
                            body : { 
                                text : 'valueA' 
                            }, 
                            table: 'table'
                        } 
                    }
                ] 
            });

            done();

        });



		it("assocListgroupPanel", function(done) {

            var value1 = '';
            var value2 = 'valueA';
            
            var text = 'te';
            var divclass = 'dc';
            var fa = 'faicon';
            var id = 'id';

            const a = {};

            expect(users_page.AssocListgroupPanel(value1, id, divclass, text, fa, 'list', a)).toEqual({
                panels : [ 
                    { 
                        userpanels : { 
                            id: id,
                            divclass: divclass,
                            heading : { 
                                text : 'te', 
                                faicon : 'faicon' 
                            }, 
                            body : { 
                                text : '' 
                            }, 
                            listgroup: 'list'
                        } 
                    }
                ] 
            });

            expect(users_page.AssocListgroupPanel(value2, id, divclass,text, fa, 'list', a)).toEqual({ 
                panels : [ 
                    { 
                        userpanels : { 
                            id: id,
                            divclass: divclass,
                            heading : { 
                                text : 'te', 
                                faicon : 'faicon' 
                            }, 
                            body : { 
                                text : 'valueA' 
                            }, 
                            listgroup: 'list'
                        } 
                    }
                ] 
            });

            done();

        });


		it("AppendWhenExist", function(done) {

            var value1 = '';
            var value2 = 'valueA';
            
            var key = 'te';
            var fa = 'faicon';

            const a1 = [];
            const a2 = [{listitem: {text:'t', faicon:'fa'}}]; 

            expect(users_page.AppendWhenExist(value1, key, fa, a1)).toEqual(a1);
            expect(users_page.AppendWhenExist(value1, key, fa, a2)).toEqual(a2);

            expect(users_page.AppendWhenExist(value2, key, fa, a1)).toEqual([{listitem:{text: key + ": " + value2, faicon: fa}}]);
            expect(users_page.AppendWhenExist(value2, key, fa, a2)).toEqual([{listitem:{text:'t', faicon: 'fa'}}, {listitem:{text: key + ": " + value2, faicon: fa}}]);

            done();

        });


		it("AppendEmail", function(done) {

            var email = 'abc@def.com';
            const a1 = [];
            const a2 = [{listitem: {text:'t', faicon:'fa'}}]; 

            expect(users_page.AppendEmail(true, email, a1)).toEqual(a1);
            expect(users_page.AppendEmail(true, email, a2)).toEqual(a2);

            expect(users_page.AppendEmail(false, email, a1)).toEqual(users_page.AppendWhenExist(email, 'Email', 'fa-envelope', a1));
            expect(users_page.AppendEmail(false, email, a2)).toEqual(users_page.AppendWhenExist(email, 'Email', 'fa-envelope', a2));

            done();

        });

        it("CalculateLevelLimits", function(done) {
            expect(users_page.CalculateLevelLimits(0)).toEqual({ level : 0, xplast : '55', xpnext : '55', xppos : '0', xpvalue : '0', xpperc : '0' });
            expect(users_page.CalculateLevelLimits(60)).toEqual({ level : 1, xplast : '61', xpnext : '116', xppos : '5', xpvalue : '60', xpperc : '8' });
            expect(users_page.CalculateLevelLimits(116)).toEqual({ level : 2, xplast : '67', xpnext : '182', xppos : '0', xpvalue : '116', xpperc : '1' });
            expect(users_page.CalculateLevelLimits(200)).toEqual({ level : 3, xplast : '73', xpnext : '255', xppos : '18', xpvalue : '200', xpperc : '25' } );
            done();
        });

        it("AddHeaderData", function(done) {

            var ud = {
                avataricon: 'avatar',
                name:       'name',
                bggname:    'bggname'
            };

            var wd = {
                web: 'data'
            }

            const viewimage = R.path(['userheader', 'userimage', 'img']);
            const viewname  = R.path(['userheader', 'username', 'div']);
            const viewstats = R.path(['userheader', 'userstats', 'div', 'a']);

            expect(viewimage(users_page.AddHeaderData(ud, wd))).toEqual({ class: 'img-rounded', src: ud.avataricon, alt: ud.name })
            expect(viewname (users_page.AddHeaderData(ud, wd))).toEqual({ class: 'text-center', text: ud.name })
            expect(viewstats(users_page.AddHeaderData(ud, wd))).toEqual({
                href: 'https://www.boardgamegeek.com/user/' + ud.bggname,
                text: ud.bggname,
                target:'_blank'
            })

            done();

        });

        it("readEmail", function(done) {
            expect(users_page.ReadEmail).toEqual();
            done();
        });

        it("userProfile", function(done) {
            expect(users_page.UserProfile({pageslug:'ps', profile:{a:'b'}}).get()).toEqual({a:'b', pageslug:'ps'});
            expect(users_page.UserProfile({pageslug:'pg', profile:{c:'d'}}).get()).toEqual({c:'d', pageslug:'pg'});
            done();
        });

        it("buildEmailLink", function(done) {
            expect(users_page.BuildEmailLink('a')).toBe('users?email=a');
            expect(users_page.BuildEmailLink('d')).toBe('users?email=d');
            done();
        });

        it("isOwnProfile", function(done) {
            expect(users_page.IsOwnProfile({}, {})).toBe(false);
            expect(users_page.IsOwnProfile({user: {email:'b'}}, {email:'a'})).toBe(false);
            expect(users_page.IsOwnProfile({user: {email:'c'}}, {email:'c'})).toBe(true);
            done();
        });

        it("SafePlayerName", function(done) {
            expect(users_page.SafePlayerName({profile:'noname'})).toBe('Player');
            expect(users_page.SafePlayerName({profile:{name:'x'}})).toBe('x');
            expect(users_page.SafePlayerName({profile:{name:'y'}})).toBe('y');
            done();
        });

        it("BuildGemCount", function(done) {
            expect(users_page.BuildGemCount(0)).toEqual({});
            expect(users_page.BuildGemCount(1)).toEqual({gemcount: 1});
            expect(users_page.BuildGemCount(5)).toEqual({gemcount: 5});
            done();
        });

        it("SafeGemCount", function(done) {
            //expect(users_page.SafeGemCount({}, {})).toEqual({btnGiftGems: true});
            expect(users_page.SafeGemCount({}, {}, {})).toEqual({});
            //expect(users_page.SafeGemCount({user:{premium:{creditsshare:0}}}, {})).toEqual({btnGiftGems: true});
            expect(users_page.SafeGemCount({user:{premium:{creditsshare:0}}}, {premium:{creditsshare:0}}, {})).toEqual({});
            expect(users_page.SafeGemCount({user:{premium:{creditsshare:3}}}, {premium:{creditsshare:3}}, {})).toEqual({btnGiftGems: true, gemcount: 3});
            expect(users_page.SafeGemCount({user:{premium:{creditsshare:3}, email:'abc'}}, {premium:{creditsshare:3}}, {email:'abc'})).toEqual({gemcount: 3});
            expect(users_page.SafeGemCount({user:{premium:{creditsshare:3}, email:'abc'}}, {premium:{creditsshare:3}}, {email:'def'})).toEqual({btnGiftGems:true, gemcount: 3});
            done();
        });

        it("MergeWhenOwnProfile", function(done) {
            expect(users_page.MergeWhenOwnProfile({}, {}, {a: 'test'})({b:'default'})).toEqual({b:'default'});
            expect(users_page.MergeWhenOwnProfile({user:{email:'abc'}}, {email:'bcd'}, {a: 'test'})({b:'default'})).toEqual({b:'default'});
            expect(users_page.MergeWhenOwnProfile({user:{email:'abc'}}, {email:'abc'}, {a: 'test'})({b:'default'})).toEqual({a:'test', b:'default'});
            done();
        });


    it("PrepareData", function(done) {

      var ud = {
        avataricon: 'avatar',
        name:       'name',
        bggname:    'bggname'
      };

      var wd = {
          web: 'data'
      }

      var wtp = {
      };

      const viewimage  = R.path(['userheader', 'userimage', 'img']);
      const viewbgg  = R.path(['userheader', 'userstats', 'div', 'a']);
      const viewpanels = R.prop('panels');

      expect(R.prop('web', users_page.PrepareData(ud, wd, wtp))).toBe('data');
      expect(viewimage(users_page.PrepareData(ud, wd, wtp))).toEqual({ class: 'img-rounded', src: 'avatar', alt: 'name' });
      expect(viewbgg(users_page.PrepareData(ud, wd, wtp))).toEqual({ href: 'https://www.boardgamegeek.com/user/bggname', text: 'bggname', target: '_blank' });
      expect(viewpanels(users_page.PrepareData(ud, wd, wtp))[3]).toEqual({ userpanels : { id : 'profession', divclass : 'col-lg-4', heading : { text : 'Profession', faicon : 'fa-briefcase' }, body : {  } } });

      done();

    });


    var userValues = {};

		it("preload values into accout", function(done) {
      userValues.name = faker.Internet.userName();
      userValues.bggname = faker.Internet.userName();
      userValues.profession = faker.Company.companyName();
      userValues.biography = faker.Company.catchPhrase();
      userValues.commreddit = faker.Internet.userName();

      account.findOneAndUpdate({'email': userEmail}, 
       {$set:{'profile':
         {
            'name': userValues.name, 
            'bggname': userValues.bggname, 
            'profession': userValues.profession,
            'biography': userValues.biography,
            'commreddit': userValues.commreddit,
         }
       }}, {new: true}).exec().then(function(x) {
         userValues.pageslug = x.pageslug;
         done();
       }
      );
		});

		it("valid user visit /users", function(done) {
      browser.visit(base_url + 'users?id=' + userValues.pageslug, function(err) {
        expect(browser.html('#username')).toContain(userValues.name);
        expect(browser.html('#bggname')).toContain(userValues.bggname);
        expect(browser.html('#panel-profession')).toContain('Profession');

        expect(browser.success).toBe(true);
        done();
      });
		});

    it('server close', function(done) {
      server.close(function() {
        done();
      });
    });

    it('db close', function(done) {
      mongo.CloseDatabase();
      done();
    });

  });
});
