var express = require("express");
var log = require('../app/log.js');

var faker = require('Faker');

var R = require('ramda');

var Maybe   = require('data.maybe');
var Task    = require('data.task'); 

var passport = require('passport');
var Promise = require('bluebird');

var Browser = require("zombie");
var browser = new Browser();

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var testBoardgame = require('../app/models/boardgame.js');

var transaction = require('../app/models/transaction.js');
var login_page = require('../app/login_page.js');

var ajax = require('./ajax.js');

var genhelper = require('./genhelper.js');

var api_accounts = require('../app/accounts_service.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var mongo = require('../app/mongoconnect.js');
var dbUri = nconf.get('database:uri');

var testapp = express();
var expressHelper = require('../app/expresshelper');

var generateUser = genhelper.GenerateUser(account);
var generateHostUser = genhelper.GenerateHostUser(accounts);
var generateMatch = genhelper.GenerateFakeHostedMatch(account, testBoardgame, match);
var generateGame = genhelper.GenerateFakeBoardgame(testBoardgame);

const readStatus = R.prop('statusCode');


// ============== AJAX ===============
var ajaxPromise = ajax.AjaxPromise;

// ============== VARS ===============
var url_base = "http://localhost:30203";
var server;

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  testapp.close(function(){
    console.log('accounts_service_spec closed on port 30203');
  });
};

// ============== RUN TESTS ================== 
describe("accounts_api", function() {
  describe("read basic data", function() {

    var hasError = R.compose(R.has('error'), JSON.parse);
    var hasProfile = R.compose(R.has('profile'), JSON.parse);
    var readError = R.compose(R.path(['error', 'code']), JSON.parse);
    var readEmail = R.compose(R.path(['profile', 'email']), JSON.parse);

    // url to test
    var url_api = '/api/v1/accounts';

    it("geocodify", function(done) {
      const xdata = {playcity: 'abc', playcitylng: 123, playcitylat: 456}; 
      const faildata = {playcity: 'abc', playcitylat: 456}; 
      expect(api_accounts.SafeGeocodify(xdata).isJust).toBe(true);
      expect(R.prop('name', api_accounts.SafeGeocodify(xdata).get())).toBe('abc');
      expect(R.path(['loc', 'coordinates'], api_accounts.SafeGeocodify(xdata).get())).toEqual([123,456]);
      expect(api_accounts.SafeGeocodify(faildata).isJust).toBe(false);
      expect(api_accounts.SafeGeocodify({}).isJust).toBe(false);

      const testdata = {test: 'data'};
      expect(api_accounts.EitherGeocode(testdata, Maybe.Nothing()).merge()).toEqual(testdata);
      expect(api_accounts.EitherGeocode(testdata, Maybe.of('area')).merge()).toEqual({test: 'data', localarea: 'area'});

      done();

    });

    it("maybeUserProp", function(done) {
      const reqtestobj = {test: {email: 'testkey'}};
      const requserobj = {user: {email: 'userkey'}};
      const pathkey = 'test';
      expect(api_accounts.MaybeUserProp(reqtestobj, 'email', 'test').get()).toEqual('testkey');
      expect(api_accounts.MaybeUserProp(requserobj, 'email', 'test').get()).toEqual('userkey');
      expect(api_accounts.MaybeUserProp({}, 'email', 'test').isNothing).toBe(true);
      done();
    });


    it("maybekey", function(done) {
      const reqtestobj = {test: {key: 'testkey'}};
      const requserobj = {user: {key: 'userkey'}};
      const pathkey = 'test';
      expect(api_accounts.MaybeKey(reqtestobj, 'test').get()).toEqual('testkey');
      expect(api_accounts.MaybeKey(requserobj, 'test').get()).toEqual('userkey');
      expect(api_accounts.MaybeKey({}, 'test').isNothing).toBe(true);
      done();
    });

    it("setup server", function(done) {
      mongo.ConnectDatabase(dbUri);
      login_page.SetupPassportStrategies(passport, account);
      expressHelper.SetupWebpages(testapp, passport);
      accounts.DeleteAccount(account, 'apiuser@gmail.com');

      var router = api_accounts.SetupRouter(nconf, account, transaction);

      testapp.use(url_api, router);
      server = testapp.listen(30203, function() {
        done();
      });
    });



    it("buildIcalEvent", function(done) {
      expect(api_accounts.BuildIcalEvent('u', [], 't', 'k', 'l', 'd', '2017-07-31', '2017-08-01')).toEqual({ start: new Date('2017-07-31T00:00:00.000Z'),
      end: new Date('2017-08-01T00:00:00.000Z'),
      transp: 'OPAQUE',
      summary: 't',
      alarms: [],
      uuid: 'k',
      location: 'l',
      description: 'd',
      url: 'u/events/k' }  );
      done();
    });

    it("valQueryToNumber", function(done) {
      expect(api_accounts.valQueryToNumber('a', {}).isNothing).toBe(true);
      expect(api_accounts.valQueryToNumber('a', {'a': 'z'}).isNothing).toBe(true);
      expect(api_accounts.valQueryToNumber('a', {'query': {'a': '3'}}).get()).toBe(3);
      done();
    });

    it("hasAccumulatedHostXP", function(done) {
      expect(api_accounts.hasAccumulatedHostXP({})).toBe(false);
      expect(api_accounts.hasAccumulatedHostXP({profile:{}})).toBe(false);
      expect(api_accounts.hasAccumulatedHostXP({profile:{hostxp:0}})).toBe(false);
      expect(api_accounts.hasAccumulatedHostXP({profile:{hostxp:10}})).toBe(true);
      done();
    });


    it("mergeImgClass", function(done) {
      expect(api_accounts.mergeImgClass({})).toEqual({imgclass:'imglistcircle img-circle fatooltip imglistcircle-standard'});
      expect(api_accounts.mergeImgClass({premium:{isactive:true}})).toEqual({premium:{isactive:true},imgclass:'imglistcircle img-circle fatooltip imglistcircle-premium'});
      expect(api_accounts.mergeImgClass({premium:{isstaff:true}})).toEqual({premium:{isstaff:true}, imgclass:'imglistcircle img-circle fatooltip imglistcircle-staff'});
      done();
    });

    it("mergeAClass", function(done) {
      expect(api_accounts.mergeAClass({})).toEqual({aclass : 'a-name-standard' });
      expect(api_accounts.mergeAClass({premium:{}})).toEqual({premium:{}, aclass : 'a-name-standard' });
      expect(api_accounts.mergeAClass({premium:{isstaff:false}})).toEqual({premium:{isstaff:false}, aclass : 'a-name-standard' });
      expect(api_accounts.mergeAClass({premium:{isstaff:true}})).toEqual({ premium:{isstaff:true}, aclass : 'a-name-staff' });
      expect(api_accounts.mergeAClass({premium:{isactive:false}})).toEqual({premium:{isactive:false}, aclass : 'a-name-standard' });
      expect(api_accounts.mergeAClass({premium:{isactive:true}})).toEqual({ premium:{isactive:true}, aclass : 'a-name-premium' });
      done();
    });

    it("whenUploadLocaliseURL", function(done) {
      expect(api_accounts.whenUploadLocaliseURL('abc')).toBe('abc');
      expect(api_accounts.whenUploadLocaliseURL('upload/abc')).toBe('/upload/abc');
      done();
    });

    it("sortByPlayXP", function(done) {
      expect(api_accounts.sortByPlayXP([{}])).toEqual([]);
      expect(api_accounts.sortByPlayXP([
        {profile:{playxp:30, hostxp:0, visibility:1}, a:'z'},
        {profile:{playxp:10, hostxp:50, visibility:1}, pageslug:'abc', updatedAt: '456', zxy: 'hello'},
        {profile:{playxp:20, hostxp:100, visibility:1}, email:'szy', updatedAt: '123'},
        {profile:{playxp:20, hostxp:100, visibility:2}, email:'szy', updatedAt: '123'},
      ])).toEqual([ 
        { aclass : 'a-name-standard', imgclass : 'imglistcircle img-circle fatooltip imglistcircle-standard', profile : { playxp : 30, hostxp : 0, visibility : 1 } }, 
        { aclass : 'a-name-standard', imgclass : 'imglistcircle img-circle fatooltip imglistcircle-standard', updatedAt : '123', profile : { playxp : 20, hostxp : 100, visibility : 1 }}, 
        { aclass : 'a-name-standard', imgclass : 'imglistcircle img-circle fatooltip imglistcircle-standard', updatedAt : '456', pageslug : 'abc', profile : { playxp : 10, hostxp : 50, visibility : 1 } } 
      ]);
      done();
    });

    it("sortByHostXP", function(done) {
      expect(api_accounts.sortByHostXP([{}])).toEqual([]);
      expect(api_accounts.sortByHostXP([
        {profile:{playxp:30, hostxp:0, visibility:1}, a:'z'},
        {profile:{playxp:10, hostxp:50, visibility:1}, pageslug:'abc', updatedAt: '456', zxy: 'hello'},
        {profile:{playxp:20, hostxp:100, visibility:1}, email:'szy', updatedAt: '123'},
        {profile:{playxp:20, hostxp:100, visibility:2}, email:'szy', updatedAt: '123'},
      ])).toEqual([
        { aclass : 'a-name-standard', imgclass : 'imglistcircle img-circle fatooltip imglistcircle-standard', updatedAt : '123', profile : { playxp : 20, hostxp : 100, visibility : 1 }}, 
        { aclass : 'a-name-standard', imgclass : 'imglistcircle img-circle fatooltip imglistcircle-standard', updatedAt : '456', pageslug : 'abc', profile : { playxp : 10, hostxp : 50, visibility : 1 } }
      ]);
      done();
    });


    it("userToNewtorkNode", function(done) {
      expect(api_accounts.userToNetworkNode({})).toEqual({ id : '', shape : 'circularImage', image : '/img/default-avatar.jpg', brokenImage : '/img/default-avatar.jpg', label : '??', color : '#cccccc' })
      expect(api_accounts.userToNetworkNode({pageslug:'slug', profile:{avataricon:'avt'}})).toEqual({ id : 'slug', shape : 'circularImage', image : 'avt', brokenImage : '/img/default-avatar.jpg', label : '??', color : '#cccccc' })
      expect(api_accounts.userToNetworkNode({pageslug:'slug', profile:{name:'Jack', avataricon:'avt'}})).toEqual({ id : 'slug', shape : 'circularImage', image : 'avt', brokenImage : '/img/default-avatar.jpg', label : 'Jack', color : '#cccccc' })
      expect(api_accounts.userToNetworkNode({pageslug:'slug', profile:{name:'Jack', avataricon:'avt'}, premium:{'isstaff': true}})).toEqual({ id : 'slug', shape : 'circularImage', image : 'avt', brokenImage : '/img/default-avatar.jpg', label : 'Jack', color : '#99b3ff' })
      done();
    });

    it("findCoplayers", function(done) {

      Promise.all([generateMatch('played', 4, 2), generateMatch('played', 4, 2), generateMatch('played', 4, 2), generateMatch('played', 4, 2), generateUser({}), generateUser({}), generateUser({}), generateUser({})]).then(function(u) {

        var playera = u[0][0];
        var playerb = u[1][0];
        var playerc = u[2][0];
        var playerd = u[3][0];
        var playere = u[4];
        var playerf = u[5];
        var playerg = u[6];
        var playerh = u[7];

        var matcha = u[0][1];
        var matchb = u[1][1];
        var matchc = u[2][1];
        var matchd = u[3][1];

        var joinMatcha = matches.JoinMatch(account, match, matcha.key);

        var joinMatchb = matches.JoinMatch(account, match, matchb.key);

        var joinMatchc = matches.JoinMatch(account, match, matchc.key);

        var joinMatchd = matches.JoinMatch(account, match, matchd.key);

        var userJoinMatcha = R.converge(joinMatcha, [R.prop('email'), R.prop('key')]);
        var userJoinMatchb = R.converge(joinMatchb, [R.prop('email'), R.prop('key')]);
        var userJoinMatchc = R.converge(joinMatchc, [R.prop('email'), R.prop('key')]);
        var userJoinMatchd = R.converge(joinMatchd, [R.prop('email'), R.prop('key')]);

        const findMatch = matches.FindMatchByKey(match);

        const markMatchesPlayed = match.update(
          {$or:[{key:matcha.key}, {key:matchb.key}, {key:matchc.key}, {key:matchd.key}]}, 
          {$set: {autoaccept:true}}, {multi:true}).exec();

        markMatchesPlayed.then(()=>{

          Promise.all(R.flatten([
            R.map(userJoinMatcha, [playere, playerf, playerg]),
            R.map(userJoinMatchb, [playera, playerh]),
            R.map(userJoinMatchc, [playerb, playerg]),
            R.map(userJoinMatchd, [playerf]),
          ])).then(m1 => {

            Promise.all(R.map(findMatch, [matcha.key, matchb.key, matchc.key, matchd.key])).then(matchx => {
              api_accounts.findCoplayers(playera.email, R.flatten(matchx)).fork(()=>{},
                players => {
                  expect(players.length).toBe(6);
                  expect(R.filter(R.propEq('email', players[0].email), matchx[0][0].players).length).toBe(1);

                  api_accounts.taskUserNode(playera.pageslug).fork(()=>{},
                    (playerNode) => {
                      expect(playerNode).toEqual({
                        id: playera.pageslug,
                        shape: 'circularImage',
                        image: playera.profile.avataricon,
                        brokenImage: '/img/default-avatar.jpg',
                        label: playera.profile.name,
                        color: '#cccccc'
                      });

                      api_accounts.taskFindFriends(playera.pageslug).fork(()=>{},
                        (playerNodes) => {
                          expect(playerNodes.nodes.length).toBe(4);
                          expect(playerNodes.edges.length).toBe(4);

                          api_accounts.taskFriendsFromNodes(playerNodes).fork((err)=>{},
                            (playerNodes2) => {

                              // playerfrom :: s -> o -> Bool 
                              const playerfrom = R.curry((id, x) => x.edges[0].from === id);

                              expect(R.find(playerfrom(playerb.pageslug), playerNodes2).nodes.length).toBe(3);
                              expect(R.find(playerfrom(playerb.pageslug), playerNodes2).edges.length).toBe(3);

                              done();
                            }
                          );
                        }
                      );
                    }
                  );
                }
              )
            });
          });

        });

      }).catch(
        (err) => log.clos('catch.err', err)
      );

    }, 10000);

    it("createEdge", function(done) {
      expect(api_accounts.createEdge('from', {pageslug:'to'})).toEqual({from:'from', to:'to'});
      done();
    });

    it("unionWithUniqueId", function(done) {
      expect(api_accounts.unionWithUniqueId({nodes:[{id:'a'}]}, {id:'b'})).toEqual([{id:'a'}, {id:'b'}]);
      expect(api_accounts.unionWithUniqueId({nodes:[{id:'a'}, {id:'b'}]}, {id:'b'})).toEqual([{id:'a'}, {id:'b'}]);
      expect(api_accounts.unionWithUniqueId({nodes:[{id:'a'}, {id:'b'}, {id:'c'}]}, {id:'b'})).toEqual([{id:'a'}, {id:'b'}, {id:'c'}]);
      done();
    });

    it("flatEdges", function(done) {
      expect(api_accounts.flatEdges([])).toEqual([]);
      expect(api_accounts.flatEdges([{edges:'a'}])).toEqual(['a']);
      expect(api_accounts.flatEdges([{edges:'a'}, {edges:'b'}])).toEqual(['a', 'b']);
      done();
    });

    it("compareFromTo", function(done) {
      expect(api_accounts.compareFromTo({from:'a', to:'b'}, {from:'c', to:'d'})).toBeFalsy();
      expect(api_accounts.compareFromTo({from:'a', to:'b'}, {from:'a', to:'b'})).toBeTruthy();
      expect(api_accounts.compareFromTo({from:'a', to:'b'}, {from:'b', to:'a'})).toBeTruthy();
      done();
    });

    it("returns status code 404", function(done) {
      browser.fetch(url_base + url_api).then(function(response) {
        expect(response.status).toBe(404);
        done();
      });
    });

    it("test non existing emails", function(done) {
      var requestData = [
        {
          url: url_base + url_api + '/testdummy@gmail.com',
          method: 'GET',
          data: {} 
        }
      ];
      Promise.all(R.map(ajaxPromise, requestData)).then(function(x) {
        expect(readStatus(x[0])).toBe(401);
        done();
      });
    });

    it("test search for pageslug", function(done) {
      generateUser({}).then(function(x) {
        var requestData = [
          {
            url: url_base + url_api + '/' + x.pageslug,
            method: 'GET',
            data: {} 
          },
          {
            url: url_base + url_api + '/badslug',
            method: 'GET',
            data: {} 
          }
        ];
        Promise.all(R.map(ajaxPromise, requestData)).then(function(ajx) {
          expect(readStatus(ajx[0])).toBe(200);
          expect(ajx[0].body.name).toBe(x.profile.name);
          expect(ajx[0].body.avataricon).toBe(x.profile.avataricon);
          expect(readStatus(ajx[1])).toBe(401);
          done();
        });
      });
    });

    it("test email key missing in request", function(done) {
      var requestData = {
        url: url_base + url_api,
        method: 'GET',
        data: {} 
      };
      ajaxPromise(requestData).then(function(x) {
        expect(readStatus(x)).toBe(404);
        done();
      });
    });

    it('verify Account', function(done) {
      var testEmail = 'apiuser@gmail.com';
      var dummyaccount = {bggname: 'apiname'};

      var newaccount = accounts.RespawnAccountObject(account, testEmail, dummyaccount).then(function(a) {
              
        var requestData = [
          {
            url: url_base + url_api + '/testdummy@gmail.com',
            method: 'GET',
            data: {} 
          },
          {
            url: url_base + url_api + '/' + testEmail,
            method: 'GET',
            data: {key: 'testkey'} 
          },
          {
            url: url_base + url_api + '/fakeemail',
            method: 'GET',
            data: {key: a.key} 
          },
          {
            url: url_base + url_api + '/' + testEmail,
            method: 'GET',
            data: {key: a.key} 
          }
        ];

        Promise.all(R.map(ajaxPromise, requestData)).then(function(x) {
          expect(readStatus(x[0])).toBe(401);
          expect(readStatus(x[1])).toBe(401);
          expect(readStatus(x[2])).toBe(401);
          expect(readStatus(x[3])).toBe(200);
          expect(x[3].body.profile.email).toBe(testEmail);

          done();

        }).catch(function(err) {
          log.clos('err.verify', err);

        });

      });
    }, 20000);


    it("update profile fields", function(done) {
      const readAccount = R.composeP(R.prop('key'), R.head, accounts.ReadAccountFromEmail(account));
      generateUser({profile:{Monday: false, Sunday: true, sundaystarthour:3, sundaystartminute:0, sundayendhour: 15, sundayendminute:45 }}).then(function(x) {

        readAccount(x.email).then(function(accountKey) { 
          requestchange = {
            url: url_base + url_api + '/' + x.email,
            method: 'PATCH',
            data: { Monday: true, mondaystarthour: 6, mondayendhour: 22, key: accountKey }
          };

          ajaxPromise(requestchange).then(function(y) {
            expect(readStatus(y)).toBe(200);
            accounts.ReadAccountFromEmail(account, x.email).then(function(z) {

              expect(R.path(['profile', 'Monday'], z[0])).toBeTruthy();
              expect(R.path(['profile', 'mondaystarthour'], z[0])).toBe(6);
              expect(R.path(['profile', 'mondayendhour'], z[0])).toBe(22);

              expect(R.path(['profile', 'Sunday'], z[0])).toBeTruthy();
              expect(R.path(['profile', 'sundaystarthour'], z[0])).toBe(3);
              expect(R.path(['profile', 'sundayendhour'], z[0])).toBe(15);

              done();

            });
          });
        });
      });
    }, 20000)


    it("update profile bgg field", function(done) {

      var testEmail = 'apiuser@gmail.com';
      var readAccount = R.composeP(R.prop('key'), R.head, accounts.ReadAccountFromEmail(account));
              
      // test reading with key
      readAccount(testEmail).then(function(accountKey) { 
        requestchange = [{
          url: url_base + url_api + '/' + testEmail,
          method: 'PATCH',
          data: {bggname: 'jaied', key: accountKey}
        }, {
          url: url_base + url_api + '/' + testEmail,
          method: 'PATCH',
          data: {bggname: 'jaied'}
        }, {
          url: url_base + url_api + '/' + testEmail,
          method: 'PATCH',
          data: {key: accountKey}
        }];

        Promise.all(R.map(ajaxPromise, requestchange)).then(function(x) {

          expect(readStatus(x[0])).toBe(200);
          expect(readStatus(x[1])).toBe(401);
          expect(readStatus(x[2])).toBe(200);

          done();

        });
      });
    }, 20000);


    it("delete profile bgg field", function(done) {

      var testEmail = 'deleteme@gmail.com';
      var readAccount = R.composeP(R.prop('key'), R.head, accounts.ReadAccountFromEmail(account));

      Promise.all([generateUser({}), generateUser({})]).then(function(u) {
        requestchange = [{
          url: url_base + url_api + '/' + u[0].email,
          method: 'DELETE',
          data: {}
        }, {
          url: url_base + url_api + '/' + u[1].email,
          method: 'DELETE',
          data: {key: u[1].key}
        }];

        Promise.all(R.map(ajaxPromise, requestchange)).then(function(x) {
          expect(readStatus(x[0])).toBe(401);
          expect(readStatus(x[1])).toBe(200);

          Promise.all([
            accounts.ReadAccountFromEmail(account, u[0].email),
            accounts.ReadAccountFromEmail(account, u[1].email)
          ]).then(function(result) {
            expect(result[0].length).toBe(1);
            expect(result[1].length).toBe(0);
            done();
          });

        });
      });
    }, 2000);


    it("GET POST purchase", function(done) {
      generateUser({}).then(function(u) {

        const requestclientid = [{
          url: url_base + url_api + '/' + u.email + '/purchase',
          method: 'GET',
          data: {request: 'clienttoken'} 
        }, {
          url: url_base + url_api + '/' + u.email + '/purchase',
          method: 'GET',
          data: {request: 'aka'} 
        }];

        Promise.all(R.map(ajaxPromise, requestclientid)).then(function(x) {
          expect(readStatus(x[0])).toBe(200);
          expect(x[0].body.clienttoken.length).toBeGreaterThan(1500);
          expect(readStatus(x[1])).toBe(400);
          done();

        });
      });
    });


    it("PATCH purchase", function(done) {
      Promise.all([generateUser({premium:{creditsshare: 2}}), generateUser({}), generateUser({})]).then(function(u) {
        const requestclientid = [{
          url: url_base + url_api + '/' + u[0].email + '/purchase',
          method: 'PATCH',
          data: {transferto: u[1].pageslug} 
        }, {
          url: url_base + url_api + '/' + u[2].email + '/purchase',
          method: 'PATCH',
          data: {transferto: u[1].pageslug} 
        }];

        Promise.all(R.map(ajaxPromise, requestclientid)).then(function(x) {
          expect(readStatus(x[0])).toBe(200);
          expect(readStatus(x[1])).toBe(403);
          done();

        });
      });
    });

    it('BuildLink', function(done) {
      expect(api_accounts.BuildLink(23)).toBe('https://boardgamegeek.com/boardgame/23'); 
      expect(api_accounts.BuildLink(832)).toBe('https://boardgamegeek.com/boardgame/832'); 
      done();
    });

    it('BuildLinkObj', function(done) {
      expect(api_accounts.BuildLinkObj({id: 23}).get()).toEqual({bgglink: 'https://boardgamegeek.com/boardgame/23'}); 
      expect(api_accounts.BuildLinkObj({id: 832}).get()).toEqual({bgglink:'https://boardgamegeek.com/boardgame/832'}); 
      done();
    });

    it('MergeLink', function(done) {
      expect(api_accounts.MergeLink({})).toEqual({}); 
      expect(api_accounts.MergeLink({id:23})).toEqual({id:23, bgglink:'https://boardgamegeek.com/boardgame/23'}); 
      done();
    });

    it('SafeMatching', function(done) {
      expect(api_accounts.SafeMatching({}, 'own').isJust).toBe(false); 
      expect(api_accounts.SafeMatching({'matching': {}}, 'own').isJust).toBe(false); 
      expect(api_accounts.SafeMatching({'matching': {'own': true}}, 'own').get()).toBe(true); 
      expect(api_accounts.SafeMatching({'matching': {'own': false}}, 'own').get()).toBe(false); 
      done();
    });

    it('BuildFilter', function(done) {
      expect(api_accounts.BuildFilter({'query': {}}, {matching: {'own': true}})).toBe(false); 
      expect(api_accounts.BuildFilter({'query': {'listtype': 'x'}}, {matching: {'own': true}})).toBe(false); 
      expect(api_accounts.BuildFilter({'query': {'listtype': 'own'}}, {matching: {'own': true}})).toBe(true); 
      expect(api_accounts.BuildFilter({'query': {'listtype': 'x'}}, {matching: {'x': true}})).toBe(true); 
      expect(api_accounts.BuildFilter({'query': {'listtype': 'own'}}, {matching: {'own': false}})).toBe(false); 
      done();
    });

    it('FindProp', function(done) {
      expect(api_accounts.FindProp('id', [{}], 2)).toEqual({});
      expect(api_accounts.FindProp('id', [{id:2}], 2)).toEqual({id:2});
      expect(api_accounts.FindProp('d', [{id:2}], 2)).toEqual({});
      done();
    });

    it('UpdateObjList', function(done) {
      expect(api_accounts.UpdateObjList([{id:1, a:1}, {id:2, a:2}], 'a', {id: 1, b:1}).get()).toEqual({id:1, a:1, b:1});
      expect(api_accounts.UpdateObjList([{id:1, a:1}, {id:2, a:2}], 'a', {id: 2, b:1}).get()).toEqual({id:2, a:2, b:1});
      expect(api_accounts.UpdateObjList([{}], 'a', {id: 2, b:1}).isNothing).toEqual(true);
      done();
    });

    it('SafeGtTen', function(done) {
      expect(api_accounts.SafeGtTen(4).isNothing).toBe(true);
      expect(api_accounts.SafeGtTen(10).isNothing).toBe(true);
      expect(api_accounts.SafeGtTen(14).get()).toBe(14);
      done();
    });

    it('Percentasize', function(done) {
      expect(api_accounts.Percentasize('a')).toEqual({discount:'a% off'});
      expect(api_accounts.Percentasize('z')).toEqual({discount:'z% off'});
      done();
    });

    it('calcdiscount', function(done) {
      expect(api_accounts.Calcdiscount(Maybe.of(100), Maybe.of(50)).get()).toBe(50);
      expect(api_accounts.Calcdiscount(Maybe.of(200), Maybe.of(50)).get()).toBe(75);
      done();
    });

    it('UpdateAmazonObjList', function(done) {
      expect(api_accounts.UpdateAmazonObjList([{id:1, amazon:[{a:1, domain:'d'}]}, {id:2, amazon:[{a:2, domain:'d'}]}], 'd', 'a', {id: 1, b:1}).get()).toEqual({id:1, a:1, b:1});
      expect(api_accounts.UpdateAmazonObjList([{id:1, amazon:[{a:1, domain:'d'}]}, {id:2, amazon:[{a:2, domain:'d'}]}], 'd', 'a', {id: 2, b:1}).get()).toEqual({id:2, a:2, b:1});
      expect(api_accounts.UpdateAmazonObjList([{id:1, amazon:[{a:1, domain:'d'}]}, {id:2, amazon:[{a:2, domain:'d'}]}], 'e', 'a', {id: 2, b:1}).isNothing).toEqual(true);
      expect(api_accounts.UpdateAmazonObjList([{}], '', 'a', {id: 2, b:1}).isNothing).toEqual(true);
      done();
    });


    it('UpdateGame', function(done) {
      const baseGame = {
        id: 2,
        minplaytime:    20,
        maxplaytime:    40,
        minplayers:     120,
        maxplayers:     140,
        amazon: [{
          domain: 'webservices.amazon.com',
          currency: 'USD',
          rrp: 3999,
          saleprice: 1999, 
          formatrrp: '$39.99', 
          formatsaleprice: '$19.99', 
          pageUrl: 'url', 
          quantity: 4, 
          offerListingId: 'listing', 
        }]
      }

      const expectGame = { id : 2};

      expect(api_accounts.UpdateGame([baseGame], {id:1})).toEqual({id:1});
      expect(api_accounts.UpdateGame([baseGame], {id:2})).toEqual(expectGame);
      done();

    });

    it("get POST :email/notifications", function(done) {

        var text1 = 'text.abc'; 
        var text2 = 'text.def'; 

        var cacheGameObject = {
            numplays: 0,
            thumbnail: 'https://cf.geekdo-images.com/CEy5uVE6sa_LIJQbueqNpfAn5hM=/fit-in/246x300/pic2320134.jpg',
            yearpublished: 2017,
            name: {
                t: 'Dark Moon',
                sordindex: 1
            },
            subtype: 'boardgame',
            objectid: 170771,
            objecttype: 'thing'
        };
        var thumbnail = "thumbnail";
        var url = "url";

        const buildPostNotes = R.zipObj(['key', 'text', 'thumbnail', 'url']); 

        Promise.all([generateUser({}), generateUser({})]).then(function(u) {
            const buildPostNotifyRequest = (email, key, text, thumbnail, url) => zipObj(['url', 'method', 'data'], [url_base + url_api + '/' + email + '/notifications', 'POST', buildPostNotes([key, text, thumbnail, url])]);

            Promise.all([ajaxPromise(buildPostNotifyRequest(u[0].email, u[0].key, text1, thumbnail, url)), ajaxPromise(buildPostNotifyRequest(u[0].email, u[0].key, text2, thumbnail, url)), ajaxPromise(buildPostNotifyRequest(u[0].email, 'badkey', text1, thumbnail, url))]).then(function(x) {

                expect(x[0].statusCode).toBe(200);
                expect(x[1].statusCode).toBe(200);
                expect(x[2].statusCode).toBe(401);

                accounts.ReadAnAccountFromEmail(account, u[0].email).then(function(z) {
                    if (z.notifications[0].text == text1) {
                        expect(z.notifications[0].text).toBe(text1);
                        expect(z.notifications[1].text).toBe(text2);
                    } else {
                        expect(z.notifications[0].text).toBe(text2);
                        expect(z.notifications[1].text).toBe(text1);
                    }
                    expect(z.notifications[0].noticetype).toBe('api');
                    expect(z.notifications[1].noticetype).toBe('api');
                    done();

                });
            })
        });
    });


    it("GET & PATCH :email/notifications", function(done) {

        var text1 = 'd1'; 
        var text2 = 'd2'; 

        const length = R.compose(R.length, R.prop('body'))
        const hasProp = R.compose(R.has('_id'), R.head, R.prop('notices'), R.prop('body'));
        const buildGetNotes = R.zipObj(['key', 'type']); 
        const buildReadNote = R.zipObj(['key', 'status']); 

        const buildGetNotifyRequest = (email, key, status) => zipObj(['url', 'method', 'data'], [url_base + url_api + '/' + email + '/notifications', 'GET', buildGetNotes([key, status])]);

        const buildGetNotifyRequestNoStatus = (email, key) => zipObj(['url', 'method', 'data'], [url_base + url_api + '/' + email + '/notifications', 'GET', objOf('key', key)]);

        const buildNotify = (email, key, noteid, status) => zipObj(['url', 'method', 'data'], [url_base + url_api + '/' + email + '/notifications/' + noteid, 'PATCH', buildReadNote([key, status])])

        var thumbnail = "thumbx";
        var url = "urlx";
        var type = "typex";

        generateUser({}).then(function(u) {
            accounts.TaskNotifyUser(account, text1, u.email, thumbnail, url, type, false).fork(
                err => log.clos('err.getepatchnotification', err),
                datan1 => {
                    accounts.TaskNotifyUser(account, text2, u.email, thumbnail, url, type, false).fork(
                        err => log.clos('err.getpatchnotification2', err),
                        datan2 => {
                            Promise.all([ajaxPromise(buildGetNotifyRequest(u.email, u.key, 'new')), ajaxPromise(buildGetNotifyRequest(u.email, 'badkey', 'new'))]).then(function(x) {
                                expect(x[0].body.notices.length).toBe(2);
                                expect(hasProp(x[0])).toBe(false);
                                expect(readStatus(x[0])).toBe(200);
                                expect(readStatus(x[1])).toBe(401);

                                Promise.all([ajaxPromise(buildNotify(u.email, u.key, x[0].body.notices[0].noteid, 'read')), ajaxPromise(buildNotify(u.email, u.key, x[0].body.notices[0].noteid, 'badstatus')), ajaxPromise(buildNotify(u.email, 'badkey', x[0].body.notices[0].noteid, 'read'))]).then(function(x) {
                                    expect(x[0].statusCode).toBe(200);
                                    expect(x[1].statusCode).toBe(400);
                                    expect(x[2].statusCode).toBe(401);

                                    Promise.all([ajaxPromise(buildGetNotifyRequest(u.email, u.key, 'new'))]).then(function(x) {
                                        expect(x[0].body.notices.length).toBe(1);
                                        expect(x[0].body.faIconNotifyClass).toBe('fa fa-bell fa-fw notify-active');
                                        done();
                                    })
                                })
                            });
                        }
                    )
                }
            );
        });
    });

    it("READ ALL email/notifications", function(done) {

        var text1 = 'd1'; 
        var text2 = 'd2'; 

        const length = R.compose(R.length, R.prop('body'))
        const hasProp = R.compose(R.has('_id'), R.head, R.prop('notices'), R.prop('body'));
        const buildGetNotes = R.zipObj(['key', 'type']); 
        const buildReadNote = R.zipObj(['key', 'status']); 

        const buildGetNotifyRequest = (email, key, status) =>   zipObj(['url', 'method', 'data'], [url_base + url_api + '/' + email + '/notifications', 'GET', buildGetNotes([key, status])]);
        const buildReadNotify = (email, key, status) => zipObj(['url', 'method', 'data'], [url_base + url_api + '/' + email + '/notifications', 'PATCH', buildReadNote([key, status])])

        var thumbnail = "thumbx";
        var url = "urlx";
        var type = "typex";

        generateUser({}).then(function(u) {
            R.sequence(Task.of, [accounts.TaskNotifyUser(account, text1, u.email, thumbnail, url, type, false), accounts.TaskNotifyUser(account, text2, u.email, thumbnail, url, type, false)]).fork(
                err => log.clos('err.readall', err),
                datan1 => {
                    ajaxPromise(buildReadNotify(u.email, u.key, 'read')).then(function(x) {
                        expect(x.statusCode).toBe(200);
                        expect(x.body.update).toBe('ok');
                        done();
                    })
                }
            );
        });
    });

    it("POST :email/privatemails GET mailtype=outbox", function(done) {

      var subject1 = 'subject.abc'; 
      var subject2 = 'subject.def'; 

      var text1 = 'text.abc'; 
      var text2 = 'text.def'; 

      var cacheGameObject = {
        numplays: 0,
        thumbnail: 'https://cf.geekdo-images.com/CEy5uVE6sa_LIJQbueqNpfAn5hM=/fit-in/246x300/pic2320134.jpg',
        yearpublished: 2017,
        name: {
            t: 'Dark Moon',
            sordindex: 1
        },
        subtype: 'boardgame',
        objectid: 170771,
        objecttype: 'thing'
      };
      var thumbnail = "thumbnail";
      var url = "url";

      const buildPostMails = 
        R.zipObj(['key', 'pageslug', 'subject', 'text', 'mailtype']); 

      Promise
        .all([generateUser({}), generateUser({})])
        .then(function(u) {

        const buildPostMailRequest = (email, key, topageslug, subject, text) => 
          zipObj(
            ['url', 'method', 'data'], 
            [
              url_base + url_api + '/' + email + '/privatemails', 
              'POST', 
              buildPostMails([key, topageslug, subject, text, 'inbox'])
            ]
          );

        const buildGetSentMailRequest = (email, key) => 
          zipObj(
            ['url', 'method', 'data'], 
            [
              url_base + url_api + '/' + email + '/privatemails', 
              'GET', 
              {key: key, mailtype:'outbox'}
            ]
          );

        Promise.all([
          ajaxPromise(buildPostMailRequest(u[0].email, u[0].key, u[1].pageslug, subject1, text1)), 
          ajaxPromise(buildPostMailRequest(u[0].email, u[0].key, u[1].pageslug, subject2, text2)), 
          ajaxPromise(buildPostMailRequest(u[0].email, 'badkey', u[1].pageslug, subject1, text1))
        ]).then(function(x) {

          expect(x[0].statusCode).toBe(200);
          expect(x[1].statusCode).toBe(200);
          expect(x[2].statusCode).toBe(401);

          accounts.ReadAnAccountFromEmail(account, u[1].email).then(function(z) {
            if (z.mails[0].text == text1) {
              expect(z.mails[0].text).toBe(text1);
              expect(z.mails[0].subject).toBe(subject1);
              expect(z.mails[1].text).toBe(text2);
              expect(z.mails[1].subject).toBe(subject2);
            } else {
              expect(z.mails[0].text).toBe(text2);
              expect(z.mails[0].subject).toBe(subject2);
              expect(z.mails[1].text).toBe(text1);
              expect(z.mails[1].subject).toBe(subject1);
            }
            
            Promise.all([
              ajaxPromise(buildGetSentMailRequest(u[0].email, u[0].key)), 
              ajaxPromise(buildGetSentMailRequest(u[1].email, u[1].key))
            ]).then(x => {
              expect(x[0].body.privatemails.length).toBe(2);
              expect(x[1].body.privatemails.length).toBe(0);
              done();
            });
          });
        })
      });
    });

    it("GET & PATCH :email/privatemails", function(done) {

      var text1 = 'd1'; 
      var text2 = 'd2'; 

      const length = R.compose(R.length, R.prop('body'))
      const mailCount = R.compose(
        R.length, 
        R.path(['body', 'privatemails'])
      );
      const buildGetNotes = R.zipObj(['key', 'type']); 
      const buildReadNote = R.zipObj(['key', 'status']); 

      const buildGetPMRequest = (email, key, status) => 
        zipObj(
          ['url', 'method', 'data'], 
          [
            url_base + url_api + '/' + email + '/privatemails', 
            'GET', 
            buildGetNotes([key, status])
          ]
        );
      
      const buildGetPMRequestNoStatus = (email, key) => 
        zipObj(
          ['url', 'method', 'data'], 
          [
            url_base + url_api + '/' + email + '/privatemails', 
            'GET', 
            objOf('key', key)
          ]
        );

      const buildGetPMRequestOutbox = (email, key) => 
        zipObj(
          ['url', 'method', 'data'], 
          [
            url_base + url_api + '/' + email + '/privatemails', 
            'GET', 
            zipObj(['key', 'mailtype'], [key, 'outbox'])
          ]
        );

      const buildPM = (email, key, noteid, status) => 
        zipObj(
          ['url', 'method', 'data'], 
          [
            url_base + url_api + '/' + email + '/privatemails/' + noteid, 
            'PATCH', 
            buildReadNote([key, status])
          ]
        )

      var thumbnail = "thumbx";
      var url = "urlx";
      var type = "typex";

      Promise.all([generateUser({}), generateUser({})]).then(function(u) {
        accounts.TaskMailUser(account, u[0].email, u[0].profile.name, u[0].profile.avataricon, u[1].pageslug, 'subject', text1, 'inbox', 'new').fork(
          err => log.clos('err.getepatchnotification', err),
          datan1 => {
            accounts.TaskMailUser(account, u[0].email, u[0].profile.name, u[0].profile.avataricon, u[1].pageslug, 'subject', text2, 'inbox', 'new').fork(
              err => log.clos('err.getpatchnotification2', err),
              datan2 => {
                Promise.all([ajaxPromise(buildGetPMRequest(u[1].email, u[1].key, 'new')), ajaxPromise(buildGetPMRequest(u[1].email, 'badkey', 'new'))]).then(function(x) {

                  expect(x[0].body.privatemails.length).toBe(2);
                  expect(mailCount(x[0])).toBe(2);
                  expect(readStatus(x[0])).toBe(200);
                  expect(readStatus(x[1])).toBe(401);

                  Promise.all([ajaxPromise(buildPM(u[1].email, u[1].key, x[0].body.privatemails[0].mailid, 'read')), ajaxPromise(buildPM(u[1].email, u[1].key, x[0].body.privatemails[0].mailid, 'badstatus')), ajaxPromise(buildPM(u[1].email, 'badkey', x[0].body.privatemails[0].mailid, 'read'))]).then(function(x) {
                    expect(x[0].statusCode).toBe(200);
                    expect(x[1].statusCode).toBe(200);
                    expect(x[2].statusCode).toBe(401);

                    // buildGetPMRequestAll
                    Promise.all([ajaxPromise(buildGetPMRequest(u[1].email, u[1].key, 'new')), ajaxPromise(buildGetPMRequestNoStatus(u[1].email, u[1].key))]).then(function(x) {
                      expect(x[0].body.privatemails.length).toBe(1);
                      expect(x[0].body.faIconMailClass).toBe('fa fa-envelope fa-fw notify-active');
                      expect(x[1].body.privatemails.length).toBe(2);
                      done();

                    })
                  })
                });
              }
            )
          }
        );
      });
    });


    it("READ ALL email/privatemails", function(done) {

      var text1 = 'd1'; 
      var text2 = 'd2'; 

      const length = R.compose(R.length, R.prop('body'))
      const hasProp = R.compose(
        R.has('_id'), 
        R.head, 
        R.prop('privatemails'), 
        R.prop('body')
      );
      const buildGetNotes = R.zipObj(['key', 'type']); 
      const buildReadNote = R.zipObj(['key', 'status']); 

      const buildGetMailRequest = (email, key, status) =>
        zipObj(
          ['url', 'method', 'data'], 
          [
            url_base + url_api + '/' + email + '/privatemails', 
            'GET', 
            buildGetNotes([key, status])
          ]
        );

      const buildGetPMRequestAll = (email, key) => 
        zipObj(
          ['url', 'method', 'data'], 
          [
            url_base + url_api + '/' + email + '/privatemails', 
            'GET', 
            {key:key}
          ]
        );

      var thumbnail = "thumbx";
      var url = "urlx";
      var type = "typex";

      Promise.all([generateUser({}), generateUser({})]).then(function(u) {
        R.sequence(
          Task.of, 
          [
            accounts.TaskMailUser(
              account, 
              u[0].email, 
              u[0].profile.name, 
              u[0].profile.avataricon, 
              u[1].pageslug, 
              'subject', 
              text1, 
              'inbox',
              'new'
            ), 
            accounts.TaskMailUser(
              account, 
              u[0].email, 
              u[0].profile.name, 
              u[0].profile.avataricon, 
              u[1].pageslug, 
              'subject', 
              text2, 
              'inbox',
              'new'
            )
        ])
          .fork(
            err => log.clos('err.readall', err),
            datan1 => {
              ajaxPromise(buildGetPMRequestAll(u[1].email, u[1].key))
                .then(function(x) {
                  expect(x.statusCode).toBe(200);
                  expect(x.body.noticecount).toBe(2);
                  done();
              })
            }
          );
      });
    });


    it('get :/email/calendars/:icalkey/rollforgroup.ics', function(done) {
      const requestRegen = R.curry((email) => ({
        url: url_base + url_api + '/' + email + '/calendars',
        method: 'POST',
        data: {action: 'regenkey'}
      }));

      generateMatch('open', 2, 2).then(function(h) {
        account.findOneAndUpdate({email: h[0].email}, {'$set': {'premium.isactive': true}}).exec().then((u) => {
         browser.fetch(url_base + url_api + '/' + h[0].email + '/calendars/' + h[0].icalkey + '/rollforgroup.ics').then(function(response) {
           expect(response.status).toBe(200);
           ajaxPromise(requestRegen(h[0].email)).then(function(newurl) {
             browser.fetch(url_base + url_api + '/' + h[0].email + '/calendars/' + h[0].icalkey + '/rollforgroup.ics').then(function(response) {
               expect(response.status).toBe(404);
               browser.fetch(url_base + R.splitAt(21, newurl.body.newurl)[1]).then(function(response) {
                 expect(response.status).toBe(200);
                 done();

               })
             })
           });
          });
        });
      }).catch(log.clos('catch.err'));
    });


    it("test non bgg user", function(done) {
      var testEmail = 'apiuser@gmail.com';
      var readAccount = R.composeP(R.prop('key'), R.head, accounts.ReadAccountFromEmail(account));

      // test reading with key
      readAccount(testEmail).then(function(accountKey) { 
        var requestchange = [{
          url: url_base + url_api + '/' + testEmail,
          method: 'PATCH',
          data: {bggname: 'notexistaldsfskasdfds', key: accountKey}
        }];

        ajaxPromise(requestchange[0]).then(function(x) {
          expect(readStatus(x)).toBe(200);
          done();
        });
      });
    }, 20000);


    // TODO: make tests work, change the coordinates 
    it('/nearby/*', function(done) {
      var geofind = {
        'profile.localarea.loc': {
          $near: {
            $geometry: {
              type: "Point" ,
              coordinates: [
                129.39027799999997,
                -23.276389 
              ] 
            },
            $maxDistance: 1000,
            $minDistance: 0 
          }
        }
      };

      account.remove(geofind).exec().then(function(d) {
        Promise.all([
          generateUser({profile:{
            visibility:1,
            name: faker.Name.findName(),
            playxp: 100,
            localarea:{
              name:'Kintore NT, Australia',
              loc:{
                coordinates:[
                  129.39027799999997,
                  -23.276389 
                ],
                type:'Point'
              }
            }
          }}), 
          generateUser({profile:{
            visibility:1,
            name: faker.Name.findName(),
            playxp: 50,
            hostxp: 50,
            localarea:{
              name:'6 Miyarrpa Street, Kintore NT',
              loc:{
                coordinates:[
                  129.3830011,
                  -23.2734338 
                ],
                type:'Point'
              }
            }
          }}), 
          generateUser({profile:{
            visibility:1,
            name: faker.Name.findName(),
            localarea:{
              name:'Kintore Store',
              loc:{
                coordinates:[
                  129.38854400000002,
                  -23.275545
                ],
                type:'Point'
              }
            }
          }})
        ]).then((users) => {

          // query
          var requestchange = [{
            url: url_base + url_api + '/nearby/coordinates',
            method: 'GET',
            data: {
              lat: '-23.276389', 
              lng: '129.39027799999997', 
              xptype:'host',
              distance: '3000'
            }
          }, {
            url: url_base + url_api + '/nearby/coordinates',
            method: 'GET',
            data: {
              lat: '-23.276389', 
              lng: '129.39027799999997', 
              xptype:'play',
              distance: '500'
            }
          }, {
            url: url_base + url_api + '/nearby/coordinates',
            method: 'GET',
            data: {
              lat: '-23.276389', 
              lng: '129.39027799999997',
              distance: '3000'
            }
          }, {
            url: url_base + url_api + '/nearby/coordinates',
            method: 'GET',
            data: {
              lng: '129.39027799999997',
              distance: '3000'
            }
          },
          {
            url: url_base + url_api + '/nearby/email/' + users[0].email,
            method: 'GET',
            data: {
              xptype:'host',
              distance: '3000'
            }
          }, {
            url: url_base + url_api + '/nearby/email/' + users[0].email,
            method: 'GET',
            data: {
              distance: '3000'
            }
          }, 
          {
            url: url_base + url_api + '/nearby/coordinates',
            method: 'GET',
            data: {
              lat: '-23.276389', 
              lng: '129.39027799999997', 
              xptype:'host',
              distance: '3000',
              name: 'abc'
            }
          },
          {
            url: url_base + url_api + '/nearby/coordinates',
            method: 'GET',
            data: {
              lat: '-23.276389', 
              lng: '129.39027799999997', 
              xptype:'host',
              distance: '3000',
              name: users[1].profile.name 
            }
          }, {
            url: url_base + url_api + '/nearby/email/' + users[0].email,
            method: 'GET',
            data: {
              distance: '3000',
              name: users[1].profile.name 
            }
          }];
          
          Promise.all(
            requestchange.map(ajaxPromise)

          ).then(function(y) {

            expect(R.has('email', y[0].body[0])).toBeFalsy();
            expect(y[0].body[0].profile.localarea.name).toBe(users[1].profile.localarea.name);

            expect(y[1].body[0].profile.localarea.name).toBe(users[0].profile.localarea.name);
            expect(y[1].body[1].profile.localarea.name).toBe(users[2].profile.localarea.name);

            expect(y[2].body[0].profile.localarea.name).toBe(users[0].profile.localarea.name);

            expect(y[3].statusCode).toBe(500);

            expect(y[4].body.length).toBe(1);
            expect(y[4].body[0].profile.localarea.name).toBe(users[1].profile.localarea.name);

            expect(y[5].body.length).toBe(3);
            expect(y[5].body[0].profile.localarea.name).toBe(users[0].profile.localarea.name);

            expect(y[6].body.length).toBe(0);
            expect(y[7].body.length).toBe(1);
            expect(y[8].body.length).toBe(1);
            done();

          }).catch(function(err) {
            log.clos('err', err);
          });
        });
      });
    });

    it("test unsubscribe", function(done) {
      generateUser({}).then(function(x) {

        var requestData = [
          {
            url: url_base + url_api + '/' + x.email + '/unsubscribe',
            method: 'PATCH',
            data: {} 
          },
          {
            url: url_base + url_api + '/' + x.email + '/unsubscribe',
            method: 'PATCH',
            data: {
							key: 		'badkey'
						} 
          },
          {
            url: url_base + url_api + '/' + x.email + '/unsubscribe',
            method: 'PATCH',
            data: {
							key: 		x.key
						} 
          },
          {
            url: url_base + url_api + '/' + 'badslug' + '/unsubscribe',
            method: 'PATCH',
            data: {
							key: 		x.key
						} 
          }
        ];

        Promise.all(R.map(ajaxPromise, requestData)).then(function(ajx) {
					expect(readStatus(ajx[0])).toBe(401);
					expect(readStatus(ajx[1])).toBe(401);
					expect(readStatus(ajx[2])).toBe(200);
					expect(ajx[2].body.notifyupdates).toBeFalsy();
					expect(readStatus(ajx[3])).toBe(401);
          done();
        });

      });
    });


    it("close server", function(done) {
      server.close(function() {
        mongo.CloseDatabase();
        done();
      });
    });

  });
});
