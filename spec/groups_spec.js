var express = require("express"); 
var router = express.Router(); 
var log = require('../app/log.js'); 
var R = require('ramda'); 

var Promise = require('bluebird');
var faker = require('Faker');

var Task    = require('data.task');
var Maybe   = require('data.maybe');
var Either  = require('data.either');

var moment  = require('moment');

var genhelper = require('./genhelper.js');

var account = require('../app/models/account.js');

var group = require('../app/models/group.js');
var api_groups = require('../app/models/groups.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var mongo = require('../app/mongoconnect.js');
var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');

var httpPort = 3693;

var testapp = express();

const ajaxData = zipObj(['url', 'method', 'data']);
var ajax = require('./ajax.js');
var ajaxPromise = ajax.AjaxPromise;

var S = require('../app/lambda.js');

// ============== VARS ===============
var url_base = "http://localhost:" + httpPort;
var server;

// url to test
var url_api = '/api/v1/groups';
var generateUser = genhelper.GenerateUser(account);


// ============== RUN TESTS ================== 
describe("groups_spec.js api", function() {
  describe("read basic data", function() {

    it("setup server", function(done) {
      mongo.ConnectDatabase(dbUri);
      router = api_groups.SetupRouter(express.Router());
      testapp.use(url_api, router);
      server = testapp.listen(httpPort, function() {
        done();
      });
    });

    it('maybeGetQuery', function(done) {
      expect(api_groups.maybeGetQuery({}).isJust).toBeFalsy();
      expect(api_groups.maybeGetQuery({'params':{}}).isJust).toBeFalsy();
      expect(api_groups.maybeGetQuery({'params':{'groupurl':'abc'}}).get()).toEqual({url:'abc'});
      done();
    });


    it('updateLoc', function(done) {
      expect(api_groups.updateLoc({}).isJust).toBeFalsy();
      expect(api_groups.updateLoc({playarea: 'abc'}).isJust).toBeFalsy();
      expect(api_groups.updateLoc({playarea: 'abc', playarealng: 20}).isJust).toBeFalsy();
      expect(api_groups.updateLoc({playarea: 'abc', playarealat: 20}).isJust).toBeFalsy();
      expect(api_groups.updateLoc({playarea: 'abc', playarealng: 21, playarealat: 20}).isJust).toBeTruthy();
      expect(api_groups.updateLoc({playarea: 'abc', playarealng: 21, playarealat: 20}).get()).toEqual(
        { 
          locname : 'abc', 
          loc : { type : 'Point', coordinates : [ 21, 20 ] }, 
          playarea : 'abc', 
          playarealng : 21, 
          playarealat : 20 
        }
      );
      done();
    });


    it('cleanObj', function(done) {
      expect(api_groups.cleanObj({})).toEqual({});
      expect(api_groups.cleanObj({a: 'a'})).toEqual({});
      expect(api_groups.cleanObj({'name': 'a'})).toEqual({'name': 'a'});
      done();
    });

    it('maybeBuildUserData', function(done) {
      expect(api_groups.maybeBuildUserData('a', 'ps', 'n')).toEqual({
        members: [{
          pageslug: 'ps', avataricon: 'a', name: 'n', status: 'host'
        }]
      });
      done();
    });

    it('isHostSlug', function(done) {
      expect(api_groups.isHostSlug('a', [])).toBeFalsy;
      expect(api_groups.isHostSlug('a', [{pageslug:'a'}])).toBeFalsy;
      expect(api_groups.isHostSlug('a', [{pageslug:'a', status:'xyz'}])).toBeFalsy;
      expect(api_groups.isHostSlug('a', [{pageslug:'a', status:'host'}])).toBeTruthy;
      done();
    });

    it('buildObj', function(done) {
      expect(api_groups.buildObj({}, 'a')).toEqual({});
      expect(api_groups.buildObj({body:{}}, 'a')).toEqual({});
      expect(api_groups.buildObj({body:{b: 'c'}}, 'a')).toEqual({});
      expect(api_groups.buildObj({body:{a: 'c'}}, 'a')).toEqual({a: 'c'});
      done();
    });

  });



  describe("api_calls", function() {

    // apiGroupEndpoint :: s -> o -> s
    const apiGroupEndpoint = (urlbase, urlapi, groupurl) => urlbase + urlapi + '/' + groupurl;
    const apiEndpoint = (urlbase, urlapi) => urlbase + urlapi;

    // details for test group
    const groupDetails = (area, lng, lat, name, description, email, key) => ({
      name:   name,
      description: description,
      email:  email,
      key:    key,
      playarea: area,
      playarealng: lng,
      playarealat: lat
    });

    // POST data
    const postData = (name, description, email, key) => ({
      url: apiEndpoint(url_base, url_api),
      method: 'POST',
      data: groupDetails('Wollongong', 150.8930607, -34.4278121, name, description, email, key)
    });

    // buildGetData :: s -> o
    const buildGetData = (groupid) => ({
      url: apiGroupEndpoint(url_base, url_api, groupid),
      method: 'GET',
      data: {}
    });

    // buildGetAllData :: s -> o
    const buildGetAllData = (data) => ({
      url: apiEndpoint(url_base, url_api),
      method: 'GET',
      data: data 
    });

    var groupurl = '';
    var hostemail = '';
    var hostkey = '';

    var useremail = '';
    var userkey = '';

    it("POST api/v1/groups/:groupurl", function(done) {
      Promise.all([generateUser({}), generateUser({})]).then(function(u) {

        // POST data
        ajaxPromise(postData('abcdef', 'test description', u[0].email, u[0].key)).then(function(d) {

          expect(d.body.name).toBe('abcdef');
          expect(d.body.description).toBe('test description');

          expect(d.body.members[0].name).toBe(u[0].toObject().profile.name);
          expect(d.body.members[0].avataricon).toBe(u[0].toObject().profile.avataricon);
          expect(d.body.members[0].pageslug).toBe(u[0].toObject().pageslug);
          expect(d.body.members[0].status).toBe('host');

          // values for furhter tests
          groupurl = d.body.url;;
          hostemail = u[0].email;
          hostkey = u[0].key;

          useremail = u[1].email;
          userkey = u[1].key;

          done();
          
        });
      });
    });

    it("GET api/v1/groups", function(done) {

      // 150, -34

      // GET data
      Promise.all([
        {}, 
        {email:'abc'}, 
        {email:hostemail, key:hostkey}, 
        {email:useremail, key:userkey}, 
        {lng:150.8930607, lat:-34.4278121}
      ].map(buildGetAllData).map(ajaxPromise)).then(function(x) {

        expect(x[0].body.length).toBeGreaterThan(0);
        expect(x[0].statusCode).toBe(200);

        expect(x[1].statusCode).toBe(401);

        expect(x[2].body.length).toBe(1);
        expect(x[2].statusCode).toBe(200);

        expect(x[3].body.length).toBe(0);
        expect(x[3].statusCode).toBe(200);


        expect(R.find(R.propEq('name', 'abcdef'), x[4].body).name).toBe('abcdef');
        expect(R.find(R.propEq('description', 'test description'), x[4].body).description).toBe('test description');

        expect(x[4].statusCode).toBe(200);

        done();
      });

    });

    it("GET api/v1/groups/:groupurl", function(done) {

      // GET data
      Promise.all(R.map(ajaxPromise, [groupurl, 'a'].map(buildGetData))).then(function(x) {
        expect(x[0].statusCode).toBe(200);
        expect(x[0].body.name).toBe('abcdef');
        expect(x[0].body.description).toBe('test description');
        expect(x[1].statusCode).toBe(404);
        done();
      });

    });

    it("PATCH api/v1/groups/:groupurl", function(done) {

      // buildPatchData :: s -> o
      const buildPatchData = R.curry((groupid, hostemail, hostkey) => ({
        url: apiGroupEndpoint(url_base, url_api, groupid),
        method: 'PATCH',
        data: groupDetails('Wollongong', 150.8930607, -34.4278121, 'new_name', 'new_description', hostemail, hostkey)
      }));


      ajaxPromise(buildPatchData(groupurl, useremail, userkey)).then(function(z) {
        expect(z.statusCode).toBe(401);

        ajaxPromise(buildPatchData(groupurl, hostemail, 'badkey')).then(function(y) {
          expect(y.statusCode).toBe(401);

          ajaxPromise(buildPatchData(groupurl, hostemail, hostkey)).then(function(x) {
            expect(x.statusCode).toBe(200);
            expect(x.body.name).toBe('new_name');
            expect(x.body.description).toBe('new_description');
            done();
          });

        });
      });
    });

    it("Confirm PATCH -> GET api/v1/groups/:groupurl", function(done) {

     // GET data
      ajaxPromise(buildGetData(groupurl)).then(function(x) {
        expect(x.statusCode).toBe(200);
        expect(x.body.name).toBe('new_name');
        expect(x.body.description).toBe('new_description');
        done();
      });
    });


    it("DELETE api/v1/groups/:groupurl", function(done) {

      // buildPatchData :: s -> o
      const buildDeleteData = R.curry((groupid, hostemail, hostkey) => ({
        url: apiGroupEndpoint(url_base, url_api, groupid),
        method: 'DELETE',
        data: {
          email:  hostemail,
          key:    hostkey
        } 
      }));

      ajaxPromise(buildDeleteData(groupurl, useremail, userkey)).then(function(z) {
        expect(z.statusCode).toBe(401);

        ajaxPromise(buildDeleteData(groupurl, hostemail, 'badkey')).then(function(y) {
          expect(y.statusCode).toBe(401);

          ajaxPromise(buildDeleteData(groupurl, hostemail, hostkey)).then(function(x) {
            expect(x.statusCode).toBe(200);
            expect(x.body.status).toBe('ok');
            done();
          });

        });
      });
    });

    it("Confirm DELETE -> GET api/v1/groups/:groupurl", function(done) {

     // DELETE data
      ajaxPromise(buildGetData(groupurl)).then(function(x) {
        expect(x.statusCode).toBe(404);
        done();
      });
    });

  });



  it("close server", function(done) {
    server.close(function() {
      mongo.CloseDatabase();
      done();
    });
  });

});
