var Promise = require('bluebird');
var R = require('ramda');
const   compose = R.compose,
        curry   = R.curry,
        split   = R.split
        tail    = R.tail;

var FormData = require('form-data');
var fs = require('fs');

var util = require('util');
var log = require('../app/log.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var faker = require('Faker');
var passport = require('passport');
var express = require('express');

var Browser = require("zombie");
var browser = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var ajax = require('./ajax.js');
var ajaxPromise = ajax.AjaxPromise;

var uploadAjax = require('../app/upload_service.js');

var genhelper = require('./genhelper.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var dbUri = nconf.get('database:uri');
var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;

mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 6217;

const ajaxData = zipObj(['url', 'method', 'data']);

uploadAjax.RouteUploads(account, nconf, testapp);

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
    testapp.close(function(){
        console.log('matches_service_spec closed on port ' + httpPort);
    });
};


// ============== VARS ===============
var url_base = "http://localhost:" + httpPort;
var url_api = '/api/v1/upload';

var server;
const endpoint = (base, api) => base + api;
const generateUser = genhelper.GenerateUser(account);

const createPostData = curry(function(url_base, url_api, email, key, image) {
    var datify = zipObj(['email', 'key', 'avataricon']);
    return ajaxData([endpoint(url_base, url_api), 'POST', datify([email, key, image])]);
});

const createLocalPostData = createPostData(url_base, url_api);
const readStatus = compose(prop('statusCode'));


// ============== RUN TESTS ================== 
describe("upload_service_spec.js", function() {

    it("setup server", function(done) {

        server = testapp.listen(httpPort, function() {
            done();
        });

    });

    it('upload no file', function(done) {

        var postData = createLocalPostData('test.email', 'test.key', 'test.file');
        ajaxPromise(postData).then(function(x) {
            expect(readStatus(x)).toBe(400); 
            done();
        });

    });

    it('upload an avataricon file', function(done) {

		const readfile = compose(tail, split('/'));

        generateUser({}).then(function(u) {

            var form = new FormData();
            form.append('email', u.email);
            form.append('userkey', u.key);
            form.append('avataricon', fs.createReadStream('spec/benchmark/img/background.jpg'));

            form.submit(endpoint(url_base, url_api), function(err, res) {
                expect(readStatus(res)).toBe(200); 

                accounts.ReadAccountFromEmail(account, u.email).then(function(x) {
                    expect(x[0].profile.avataricontype).toBe('file');
					fs.unlink('./app/public/' + x[0].profile.avataricon, err => { 
						if (!err) {
							done();
						} else {
							log.clos('err', err);
						}
					});
				});
            });
        });
    });


    it('upload an avataricon file, fail on user cred', function(done) {

        var form = new FormData();
        form.append('email', 'test.email');
        form.append('userkey', 'test.key');
        form.append('avataricon', fs.createReadStream('spec/benchmark/img/background.jpg'));

        form.submit(endpoint(url_base, url_api), function(err, res) {
            expect(readStatus(res)).toBe(401); 
            done();

        });
    });


    it('upload an avataricon file, fail no userkey', function(done) {

        var form = new FormData();
        form.append('email', 'test.email');
        form.append('avataricon', fs.createReadStream('spec/benchmark/img/background.jpg'));

        form.submit(endpoint(url_base, url_api), function(err, res) {
            expect(readStatus(res)).toBe(400); 
            done();

        });
    });

    it('upload an avataricon file, fail no email', function(done) {

        var form = new FormData();
        form.append('userkey', 'test.key');
        form.append('avataricon', fs.createReadStream('spec/benchmark/img/background.jpg'));

        form.submit(endpoint(url_base, url_api), function(err, res) {
            expect(readStatus(res)).toBe(400); 
            done();

        });
    });

    it("close server", function(done) {
        server.close(function() {
            mongo.CloseDatabase();
            done();
        });
    });


});
