var express = require("express");
var router = express.Router();
var log = require('../app/log.js');
var R = require('ramda');
var Promise = require('bluebird');

var Browser = require("zombie");
var browser = new Browser();

var api_locations = require('../app/locations_service.js');
var request = require('ajax-request');

var bodyParser = require('body-parser')

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');

var mongo = require('../app/mongoconnect.js');
var mongoose = mongo.Mongoose;

var testapp = express();


// ============== AJAX ===============
var ajaxPromise = R.curry(function(options) {
  return new Promise(function (fulfill, reject) {
    request(options, function(err, res, body) {
      if (err) {
        reject(err)

      } else {
        fulfill(body)

      }
    });
  });
}); 

// ============== VARS ===============
var url_base = "http://localhost:3002";
var server;


// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  testapp.close(function(){
    console.log('locations_service_spec closed on port 3002');
  });
};



// ============== RUN TESTS ================== 
describe("locations_service_spec.js locations api", function() {
  describe("read basic data", function() {

    it("setup server", function(done) {
      mongo.ConnectDatabase(dbUri);

      var router = api_locations.SetupRouter(nconf.get('googlemapsclient:key'));
      testapp.use('/api/v1/locations', router);

      server = testapp.listen(3002, function() {
        done();
      });
    });

    it("lookupAddress.lookupCoords", function(done) {
      api_locations.lookupAddress('1st avenue new york').then((x)=> {
        expect(x.length).toBe(1);
        expect(x[0].place.address).toBe('1st Avenue, New York, NY, USA');
        expect(x[0].place.locality).toBe('New York');
        expect(x[0].place.country).toBe('United States');

        const lat = x[0].place.coords.lat;
        const lng = x[0].place.coords.lng;

        api_locations.lookupCoords(lat, lng).then((y)=> {
          expect(y.length).toBeGreaterThan(0);
          expect(y[0].place.locality).toBe('New York');
          expect(y[0].place.country).toBe('United States');
          done();
        });

      });
    });

    it('containsType', function(done) {
      expect(api_locations.containsType('a', {types:[]})).toBeFalsy();
      expect(api_locations.containsType('a', {types:['a']})).toBeTruthy();
      expect(api_locations.containsType('a', {types:['b']})).toBeFalsy();
      expect(api_locations.containsType('c', {types:['b', 'c']})).toBeTruthy();
      done();
    });

    it('containsType', function(done) {
      expect(api_locations.findTypeName('a', {address_components:{types:[]}})).toBeFalsy();
      expect(api_locations.findTypeName('a', {address_components:[{types:['a'], long_name:'x'}]})).toBe('x');
      expect(api_locations.findTypeName('a', {address_components:[{types:['b']}]})).toBe();
      expect(api_locations.findTypeName('c', {address_components:[
        {types:['b']},
        {types:['c'], long_name:'y'}
      ]})).toBeTruthy('y');
      done();
    });

    it('mapToResults', function(done) {
      const id = (a) => a;
      const appid = (a) => a + 'z'

      expect(api_locations.mapToResults(id, {json:{results:['a']}})).toEqual(['a']);
      expect(api_locations.mapToResults(id, {json:{results:['a', 'b']}})).toEqual(['a', 'b']);
      expect(api_locations.mapToResults(appid, {json:{results:['a']}})).toEqual(['az']);
      expect(api_locations.mapToResults(appid, {json:{results:['a', 'b']}})).toEqual(['az', 'bz']);
      done();
    });


    it('buildResponseObj', function(done) {
      expect(api_locations.buildResponseObj([])).toEqual([]);
      expect(api_locations.buildResponseObj({})).toEqual([]);
      expect(api_locations.buildResponseObj({json:{}})).toEqual([]);
      expect(api_locations.buildResponseObj({json:{results:{}}})).toEqual([]);
      expect(api_locations.buildResponseObj({
        json: {
          results: [
            {
              address_components: [
                {
                  long_name: 'Locality',
                  types: [ 'locality', 'political' ]
                },
                {
                  long_name: 'Country',
                  types: [ 'country', 'political' ]
                }
              ],
              formatted_address: 'fa',
              geometry: {
                location: { lat: 20, lng: 30 }
              },
              partial_match: true,
              place_id: 'ChIJvf1sKJgZE2sRU4iT2gwvrXY',
              types: [ 'premise' ]
            }
          ]
        }
      })).toEqual([ { place : { address : 'fa', coords : { lat : 20, lng : 30 }, locality : 'Locality', country : 'Country' } } ]);

      done();

    });


    // url to test
    var url_api = '/api/v1/locations';

    it("test bad path ", function(done) {
      var requestData = {
        url: url_base + url_api + '/blah/',
        method: 'GET',
        data: {} 
      };
      ajaxPromise(requestData).then(function(x) {
        expect(x).toContain('Cannot GET /api/v1/locations/blah/');
        done();
      });
    });

    it("test empty location search", function(done) {
      var requestData = {
        url: url_base + url_api + '?search=not@validdis090neyland',
        method: 'GET',
        data: {} 
      };
      ajaxPromise(requestData).then(function(x) {
        expect(x).toBe('[]');
        done();
      });
    });

    it("test one location search", function(done) {
      var requestDataA = {
        url: url_base + url_api + '?search=Ayres%20Rock',
        method: 'GET',
        data: {} 
      };

      var requestDataB = {
        url: url_base + url_api + '?search=New%20%Zealand',
        method: 'GET',
        data: {} 
      };

      var requestDataC = {
        url: url_base + url_api + '?search=New%20%Jersey',
        method: 'GET',
        data: {} 
      };

      Promise.all([requestDataA, requestDataB, requestDataC].map(ajaxPromise)).then(function(x) {
        expect(x[0]).toBe('[{"place":{"address":"Uluru, Petermann NT 0872, Australia","coords":{"lat":-25.3444277,"lng":131.0368822},"locality":"Petermann","country":"Australia"}}]');
        expect(x[1]).toBe('[]');
        // expect(x[2]).toBe('[{"place":{"address":"20 W State St # 4, Trenton, NJ 08608, USA","coords":{"lat":40.2206191,"lng":-74.7662295},"locality":"Trenton","country":"United States"}}]');
        expect(x[2]).toBe('[{"place":{"address":"NJ-20, New Jersey, USA","coords":{"lat":40.9150959,"lng":-74.13345989999999},"country":"United States"}}]');

        done();
      });

    });

    it("test multiple location search", function(done) {

      var requestData = {
        url: url_base + url_api,
        method: 'GET',
        data: {
          search: "20_staff_street_unanderra"
        } 
      };

      var requestReverseData = (lat,lng) => ({
        url: url_base + url_api,
        method: 'GET',
        data: {
          lat: lat,
          lng: lng,
        } 
      });


      ajaxPromise(requestData).then(function(x) {
        expect(JSON.parse(x).length).toBeGreaterThan(0);
        expect(JSON.parse(x)[0].place.address).toBe("20 Staff St, Wollongong NSW 2500, Australia");
        expect(JSON.parse(x)[0].place.coords.lat).toBe(-34.4266537);
        expect(JSON.parse(x)[0].place.coords.lng).toBe(150.8819492);
        expect(JSON.parse(x)[0].place.locality).toBe("Wollongong");
        expect(JSON.parse(x)[0].place.country).toBe("Australia");

        ajaxPromise(requestReverseData(-34.4266537, 150.8819492)).then(function(y) {
          expect(JSON.parse(y).length).toBeGreaterThan(0);
          expect(JSON.parse(y)[0].place.address).toBe("20 Staff St, Wollongong NSW 2500, Australia");
          expect(JSON.parse(y)[0].place.locality).toBe("Wollongong");
          expect(JSON.parse(y)[0].place.country).toBe("Australia");
          done();
        });

      });
    });

    it("close server", function(done) {
      server.close(function() {
        mongo.CloseDatabase();
        done();
      });
    });

  });
});
