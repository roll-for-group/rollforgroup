var Promise = require('bluebird');
var R = require('ramda');
var util = require('util')
var log = require('../app/log.js');

var passport = require('passport');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var buygems_page = require('../app/buygems_page.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var match = require('../app/models/match.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');
var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 33818;
var login = require('connect-ensure-login');

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

const Maybe = require('data.maybe')

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
    testapp.close(function(){
        console.log('matches_service_spec closed on port ' + httpPort);
    });
};


// ============== RUN TESTS ================== 
describe("buygems_page_spec.js", function() {

  var server;

  // loginUser :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
  var verifyregister = function(account, zombieb, email, pass, userobj) {
    return new Promise(function(fulfill, reject) {
      accounts.DeleteAccount(account, email).then(function(y) {
        var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

        register(email, pass).then(function(x) {
          account.update({email: email}, userobj).exec().then(function(z) {
            zombieb.visit(base_url + "login", function(err) {
              if (err) 
                reject(err);

              zombieb.fill('input[name="form-username"]', email)
              zombieb.fill('input[name="form-password"]', pass)
              zombieb.pressButton('login', function() {
                fulfill(x);
              });
            });
          });
        });
      });
    });
  };

  describe("web page tests", function() {

    var userEmail = faker.Internet.email().toLowerCase();
    var userPass = 'testpassword';
    var userValues = {};

    it("MergeTextInputValue", function(done) {

      var x = {one: {value: 'test'}};
      var y = {one: {value: 'value'}};
      
      var x2 = {
        textinput: {
          id: 'oldid',
          datapath: ['a', 'b'],
          name: 'newid'
        }
      };

      var valueObj = {a: {b: "othervalue" }};
      var z = {
        textinput: {
          id: 'newid',
          name: 'newid',
          datapath: ['a', 'b'],
          value: 'othervalue'
        }
      };

      done();

    });

    it("setup server", function(done) {

      testapp.get('/', function (req, res) {
        res.send('<html>Home Page</html>')
      });

      buygems_page.RouteGems(testapp, login);

      login_page.RouteLogins(testapp, passport);
      login_page.PostLogin(testapp, passport);
      login_page.RouteVerify(testapp, account);
      server = testapp.listen(httpPort, function() {
        done();
      });
    });

    it("non logged in user visit hostgame", function(done) {
      page = 'buygems';
      browser.visit(base_url + page, function(err) {
        expect(browser.success).toBe(true);
        expect(browser.html()).toContain('Roll for Group');
        done();
      });
    });

    it("user login", function(done) {
      verifyregister(account, browser, userEmail, userPass, {}).then(function(x) {
        done();
      });
    });

    var userValues = {};
    it("preload values into accout", function(done) {
      userValues.name = faker.Internet.userName();
      userValues.bggname = faker.Internet.userName();
      userValues.Monday = true;
      userValues.Tuesday = false;
      userValues.profession = faker.Company.companyName;
      userValues.biography = faker.Company.catchPhrase;
      userValues.mondaystarthour = 5;
      userValues.mondaystartminute = 30;
      userValues.mondayendhour = 15;
      userValues.mondayendminute = 45;

      account.findOneAndUpdate({'email': userEmail}, 
       {$set:
         {'profile':
            {
                'name': userValues.name, 
                'bggname': userValues.bggname, 
                'Monday': userValues.Monday, 
                'Tuesday': userValues.Tuesday,
                'profession': userValues.profession,
                'biography': userValues.biography,
                'mondaystarthour': userValues.mondaystarthour,
                'mondaystartminute': userValues.mondaystartminute,
                'mondayendhour': userValues.mondayendhour,
                'mondayendminute': userValues.mondayendminute,
            }
         }}, {new: true}).exec().then(function(x) {
         done();
      });
    });

    it("valid user visit /buygems", function(done) {
      browser.visit(base_url + 'buygems', function(err) {
        expect(browser.success).toBe(true);
        expect(browser.html('#div4plantype')).toContain('Giftable gems for friends');
        done();
      });
    });

    it('server close', function(done) {
      server.close(function() {
        done();
      });
    });

    it('db close', function(done) {
      mongo.CloseDatabase();
      done();
    });

  });
});
