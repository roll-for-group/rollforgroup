var log = require('../app/log.js');

describe('test logging', function() {
    it('test consoleLogObject', function(done) {
        var testObj = {test1:"hello", test2:["a", "b"]};
        expect(log.ConsoleLogObject(testObj)).toEqual(testObj); 
        done();
    });

    it('test consoleLogObjectSection', function(done) {
        var testObj = {test1:"hello", test2:["a", "b"]};
        expect(log.ConsoleLogObjectSection('spec/log.js', testObj)).toEqual(testObj); 
        done();
    });

    it('test consoleLogObjectSection', function(done) {
        var testObj = {test1:"hello", test2:["a", "b"]};
        expect(log.clos('spec/log.js-clos', testObj)).toEqual(testObj); 
        done();
    });



})
