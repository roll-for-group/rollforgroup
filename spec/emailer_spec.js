var R = require('ramda');
var util = require('util');
var log = require('../app/log.js');

var Promise = require('bluebird');
var faker = require('Faker');
var emailer = require('../app/emailer.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var nodemailer = require('nodemailer');
var stubTransport = require('nodemailer-stub-transport');
var transporter = nodemailer.createTransport(stubTransport());

var genhelper = require('./genhelper.js');

var mongo = require('../app/mongoconnect.js');
var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');

mongo.ConnectDatabase(dbUri);

var account = require('../app/models/account.js');
var boardgame = require('../app/models/boardgame.js');
var match = require('../app/models/match.js');

const generateMatch = genhelper.GenerateFakeHostedMatch(account, boardgame, match);
const generateUser = genhelper.GenerateUser(account);


describe("email", function() {
	describe("send mail functions", function() {

		it("buildJoinEmailUserData", function(done) {
			expect(emailer.buildJoinEmailUserData('he', 'hn', 'pn', 'pa', 'en', 'url')).toEqual( { email : 'he', mailplan : [ { template : 'request-joinmatch-001', hours : -1 } ], userdata : { email : 'he', avataricon : 'pa', name : 'hn', url : 'url', playername : 'pn', eventname : 'en' } } );

			expect(emailer.buildJoinEmailUserData('ae', 'an', 'pn2', 'pa2', 'enx', 'urlyz')).toEqual( { email : 'ae', mailplan : [ { template : 'request-joinmatch-001', hours : -1 } ], userdata : { email : 'ae', avataricon : 'pa2', name : 'an', url : 'urlyz', playername : 'pn2', eventname : 'enx' } } );

			done();
		});

		it("buildInterestedEmailUserData", function(done) {
			expect(emailer.buildInterestedEmailUserData('he', 'hn', 'pn', 'pa', 'en', 'url')).toEqual( { email : 'he', mailplan : [ { template : 'interested-match-001', hours : -1 } ], userdata : { email : 'he', avataricon : 'pa', name : 'hn', url : 'url', playername : 'pn', eventname : 'en' } } );

			expect(emailer.buildInterestedEmailUserData('ae', 'an', 'pn2', 'pa2', 'enx', 'urlyz')).toEqual( { email : 'ae', mailplan : [ { template : 'interested-match-001', hours : -1 } ], userdata : { email : 'ae', avataricon : 'pa2', name : 'an', url : 'urlyz', playername : 'pn2', eventname : 'enx' } } );

			done();
		});

		it("buildInterestedEmailReminderUserData", function(done) {
			expect(emailer.buildInterestedEmailReminderUserData('em', 'nm', 'uw', 'en', 'url')).toEqual( { email : 'em', mailplan : [ { template : 'interested-match-reminder-001', hours : -1 } ], userdata : { email : 'em', name : 'nm', untilwhen : 'uw', url : 'url', eventname : 'en' } } );

			done();
		});


		it("sendInviteGuestEmail", function(done) {
			generateUser({}).then(function(u) {
				generateMatch('open', 2, 2).then(function(m) {
					emailer.sendInviteGuestEmail(account, match, u.email, m[1].key).then(
						data => {
							expect(JSON.parse(data).email).toBe(u.email);
							expect(JSON.parse(data).campaign).toBe('event-invite');
							expect(JSON.parse(data).userdata.email).toBe(u.email);
							expect(JSON.parse(data).userdata.playername).toBe(u.profile.name);
							expect(JSON.parse(data).userdata.hostname).toBe(m[1].players[0].name);
							done();
						},
						err => {
							log.clos('sendInviteGuestEmail.err', err);
						}
					);
				});
			});
		});

		it("getMatchHost", function(done) {
			expect(emailer.getMatchHost('a', {}).isJust).toBe(false);
			expect(emailer.getMatchHost('a', [{players: 'abc'}]).isJust).toBe(false);
			expect(emailer.getMatchHost('a', [{players: []}]).isJust).toBe(false);
			expect(emailer.getMatchHost('a', [{players: [{'xyz':'hello'}]}]).isJust).toBe(false);
			expect(emailer.getMatchHost('xyz', [{players: [{'xyz':'hello'}]}]).get()).toBe('hello');
			done();
		});

		it("sendRequestNotifyEmails", function(done) {
			generateUser({}).then(function(u) {
				generateMatch('open', 2, 2).then(function(m) {
					emailer.SendRequestNotifyEmails(account, match, m[1].key, m[0].email).then(
						data => {
							expect(JSON.parse(data[1]).email).toBe(m[1].players[0].email);
							expect(JSON.parse(data[1]).campaign).toBe('event-request-join');
							expect(JSON.parse(data[1]).userdata.email).toBe(m[1].players[0].email);
							expect(JSON.parse(data[1]).userdata.playername).toBe(m[1].players[0].name);
							expect(JSON.parse(data[1]).userdata.hostname).toBe(u.name);
							done();
						},
						err => {
							log.clos('sendInviteGuestEmail.err', err);
						}
					);
				});
			});
		});

		it("sendInterestNotifyEmails", function(done) {
			generateUser({}).then(function(u) {
				generateMatch('open', 2, 2).then(function(m) {
					emailer.sendInterestNotifyEmails(account, match, m[1].key, m[0].email).then(
						data => {
							expect(JSON.parse(data[1]).email).toBe(m[1].players[0].email);
							expect(JSON.parse(data[1]).campaign).toBe('event-interested');
							expect(JSON.parse(data[1]).userdata.email).toBe(m[1].players[0].email);
							expect(JSON.parse(data[1]).userdata.playername).toBe(m[1].players[0].name);
							expect(JSON.parse(data[1]).userdata.hostname).toBe(u.name);
							done();
						},
						err => {
							log.clos('sendInviteGuestEmail.err', err);
						}
					);
				});
			});
		});


		it("getPluralNotification", function(done) {
			expect(emailer.getPluralNotification(0)).toBe('0 new notifications');
			expect(emailer.getPluralNotification(1)).toBe('a new notification');
			expect(emailer.getPluralNotification(2)).toBe('2 new notifications');
			done();
		});


		it("buildNotificationCountEmailUserData", function(done) {

			expect(emailer.buildNotificationCountEmailUserData('pe', 'pk', 'pn', 3, 'url')).toEqual({ email : 'pe', mailplan : [ { template : 'notifications-001', hours : -1 } ], userdata : { email : 'pe', unsubscribeurl : 'https://www.rollforgroup.com/unsubscribe?email=pe&key=pk', userkey : 'pk', name : 'pn', url : 'url', notificationcount : 3, notificationtext : '3 new notifications' } });

			expect(emailer.buildNotificationCountEmailUserData('pe2', 'pk2', 'pn2', 3, 'urlx')).toEqual({ email : 'pe2', mailplan : [ { template : 'notifications-001', hours : -1 } ], userdata : { email : 'pe2', unsubscribeurl : 'https://www.rollforgroup.com/unsubscribe?email=pe2&key=pk2', userkey : 'pk2', name : 'pn2', url : 'urlx', notificationcount : 3, notificationtext : '3 new notifications' } });
			done();

		});

		it("SMTPSettings", function(done) {
			expect(emailer.SMTPSettings(nconf).host).toBe(nconf.get('smtp:host')); 
			expect(emailer.SMTPSettings(nconf).port).toBe(nconf.get('smtp:port')); 
			expect(emailer.SMTPSettings(nconf).auth.user).toBe(nconf.get('smtp:user')); 
			expect(emailer.SMTPSettings(nconf).auth.pass).toBe(nconf.get('smtp:pass')); 
			done();
		})

		it("sendWithSMTP", function(done) {
			emailer.SendWithSMTP(transporter, {from: "from@mail.com", to: "to@mail.com", subject: "subject", html: "message"}).then(function (respond) {
				expect(respond.envelope.from).toBe('from@mail.com');
				expect(respond.envelope.to[0]).toBe('to@mail.com');
				done();
			});
		});

		it("sendSMTPmail", function(done) {
			emailer.SendSMTPmail(transporter, "from@mail.com", "to@mail.com", "subject", "message").then(function (respond) {
				expect(respond.envelope.from).toBe('from@mail.com');
				expect(respond.envelope.to[0]).toBe('to@mail.com');
				done();
			});
		});

    it("BuildGamelist", function(done) {
      expect(emailer.BuildGamelist([{name:'a'}])).toBe('a');
      expect(emailer.BuildGamelist([{name:'a'}, {name:'b'}])).toBe('a and b');
      expect(emailer.BuildGamelist([{name:'a'}, {name:'b'}, {name:'c'}])).toBe('a, b and c');
      done(); 
    });

		it("buildInviteEmailUserData", function(done) {
			expect(emailer.buildInviteEmailUserData('pe', 'hn', 'pn', 'gs', 'en', 'el', 'et', 'url')).toEqual( { email : 'pe', mailplan : [ { template : 'invite-event-001', hours : -1 } ], userdata : { email : 'pe', playername : 'pn', hostname : 'hn', games : 'gs', eventname : 'en', eventlocation : 'el', eventtime : 'et', url : 'url' } } );

			expect(emailer.buildJoinEmailUserData('ae', 'an', 'pn2', 'gs2', 'enx', 'elx', 'etx', 'urlb')).toEqual( { email : 'ae', mailplan : [ { template : 'request-joinmatch-001', hours : -1 } ], userdata : { email : 'ae', avataricon : 'gs2', name : 'an', url : 'elx', playername : 'pn2', eventname : 'enx' } } );

			done();
		});


		/*
		it("BuildInviteGuestEmail", function(done) {
      var guestemail = faker.Internet.email();

      generateMatch('open', 2, 2).then(function(h) {
        match.findOneAndUpdate({key: h[1].key}, {title: 'event_title'}).exec().then((x) => {

          const inviteObj = emailer.BuildInviteGuestEmail(account, match, nconf.get('smtp:user'));
          inviteObj(guestemail, h[0].email, h[1].key).then(function (response) {
            expect(response.from).toBe(nconf.get('smtp:user'));
            expect(response.to).toBe(guestemail);
            expect(response.subject).toBe(h[0].profile.name + ' invites you to event_title');
            done();

          });
        });
			});
		});

		it("SendMatchInviteEmail", function(done) {
      var guestemail = faker.Internet.email();
      generateMatch('open', 2, 2).then(function(h) {

        match.findOneAndUpdate({key: h[1].key}, {title: 'event_title'}).exec().then((x) => {
          const sendinvite = emailer.SendMatchInviteEmail(transporter, account, match, nconf.get('smtp:user'));
            
          sendinvite(guestemail, h[0].email, h[1].key).then(function (response) {
            expect(response.envelope.from).toBe(nconf.get('smtp:user'));
            expect(response.envelope.to[0]).toBe(guestemail);
       
            sendinvite(guestemail, 'random@host.com', h[1].key).then().catch(function (err) {
              expect(err.code).toBe(401);

              generateUser({}).then(function(u) {
                sendinvite(guestemail, u.email, h[1].key).then().catch(function (err) {
                  expect(err.code).toBe(403);
                  done();
                });
              });
            });
          });

        });
			});
		});
		*/

      it("from", function(done) {
          expect(emailer.From('a')).toBe('Roll for Group <a>');
          expect(emailer.From('z')).toBe('Roll for Group <z>');
          done();
      });

      it("seats", function(done) {
          expect(emailer.Seats(1)).toBe('is only 1 seat');
          expect(emailer.Seats(2)).toBe('are 2 seats');
          expect(emailer.Seats(5)).toBe('are 5 seats');
          done();
      });

        it("BuildHostAcceptEmail", function(done) {

            const playerA = R.zipObj(['email', 'name', 'type'], ['a@x.com', 'aname', 'host']);
            const playerB = R.zipObj(['email', 'name'], ['b@x.com', 'bname']);
            const testmatch = R.zipObj(['games', 'locname', 'date', 'seatavailable', 'players', 'title'], [[{name: 'gn'}], 'ln', '2017-04-26 10:19:26.954Z', 'sa', [playerA, playerB], 'matchtitle']);

            expect(emailer.BuildHostAcceptEmail(testmatch, 'f', 'b@x.com', 'a@x.com').from).toEqual('f');
            expect(emailer.BuildHostAcceptEmail(testmatch, 'f', 'b@x.com', 'a@x.com').to).toEqual('a@x.com');
            expect(emailer.BuildHostAcceptEmail(testmatch, 'f', 'b@x.com', 'a@x.com').subject).toEqual('Accepted - matchtitle');

            done();

        });


        it("DegreesToRadians", function(done) {
            expect(emailer.DegreesToRadians(90)).toEqual(1.5707963267948966);
            expect(emailer.DegreesToRadians(180)).toEqual(3.141592653589793);
            expect(emailer.DegreesToRadians(220)).toEqual(3.839724354387525);
            expect(emailer.DegreesToRadians(340)).toEqual(5.934119456780721);
            done();
        });

        it("DistanceInKmBetweenEarthCoordinates", function(done) {
            expect(emailer.DistanceInKmBetweenEarthCoordinates([15, 10], [15, 10.01])).toEqual(1.1119492664455637);
            expect(emailer.DistanceInKmBetweenEarthCoordinates([15, 10], [15, 10.1])).toEqual(11.119492664455834);
            expect(emailer.DistanceInKmBetweenEarthCoordinates([15, 10], [15, 11.1])).toEqual(122.31441930901458);
            done();
        });

        it("IsEmailEqual", function(done) {
            expect(emailer.IsEmailEqual('a', {email: 'a'})).toEqual(true);
            expect(emailer.IsEmailEqual('b', {email: 'a'})).toEqual(false);
            expect(emailer.IsEmailEqual('a', {email: 'b'})).toEqual(false);
            expect(emailer.IsEmailEqual('b', {email: 'b'})).toEqual(true);
            done();
        });

        const createProfile = (lat, lng, searchradius, notifyevents) => R.zipObj(['searchradius', 'localarea', 'notifyevents'], [searchradius, {loc: {coordinates: [lat, lng]} }, notifyevents]);


        it("IsInRange", function(done) {

            const playerA = R.zipObj(['email', 'name', 'profile'], ['a@x.com', 'aname', createProfile(15, 10.01, 20, true)]);
            const playerB = R.zipObj(['email', 'name', 'profile'], ['b@x.com', 'bname', createProfile(15, 10, 20, true)]);
            const playerC = R.zipObj(['email', 'name', 'profile'], ['c@x.com', 'cname', createProfile(15, 10.1, 20, true)]);
            const playerD = R.zipObj(['email', 'name', 'profile'], ['d@x.com', 'dname', createProfile(15, 11.1, 20, true)]);
            const testmatch = R.zipObj(['loc'], [{coordinates:[15, 10]} ] );

            expect(emailer.IsInRange(testmatch, playerA)).toBe(true);
            expect(emailer.IsInRange(testmatch, playerB)).toBe(true);
            expect(emailer.IsInRange(testmatch, playerC)).toBe(true);
            expect(emailer.IsInRange(testmatch, playerD)).toBe(false);
            expect(emailer.IsInRange(testmatch, {})).toBe(false);
            expect(emailer.IsInRange({}, playerA)).toBe(false);

            done();
        });


        it("FilterEventNotifyUsers", function(done) {

            const playerA = R.zipObj(['email', 'name', 'profile'], ['a@x.com', 'aname', createProfile(15, 10.01, 20, true)]);
            const playerB = R.zipObj(['email', 'name', 'profile'], ['b@x.com', 'bname', createProfile(15, 10, 20, true)]);
            const playerC = R.zipObj(['email', 'name', 'profile'], ['c@x.com', 'cname', createProfile(15, 10.1, 20, true)]);
            const playerD = R.zipObj(['email', 'name', 'profile'], ['d@x.com', 'dname', createProfile(15, 11.1, 20, true)]);
            const playerE = R.zipObj(['email', 'name', 'profile'], ['e@x.com', 'ename', createProfile(15, 10.01, 20, false)]);

            const testmatch = R.zipObj(['loc'], [{coordinates:[15, 10]} ] );

            expect(emailer.FilterEventNotifyUsers('b@x.com', testmatch, [playerA, playerB, playerC, playerD, playerE])).toEqual(['a@x.com', 'c@x.com']);
            done();
        });

        it("BuildMessage", function(done) {
					expect(emailer.BuildMessage('abc')).toBe('abc is hosting an open board game event nearby.') 
					expect(emailer.BuildMessage('def')).toBe('def is hosting an open board game event nearby.') 
					done();
        });

        it("SafeMessage", function(done) {
					expect(emailer.SafeMessage({}).isNothing).toBe(true);
					expect(emailer.SafeMessage({players:{}}).isNothing).toBe(true);
					expect(emailer.SafeMessage({players:[{name:'abc'}]}).get()).toBe('abc is hosting an open board game event nearby.');
					done();
        });

        it("BuildMatchUrl", function(done) {
					expect(emailer.BuildMatchUrl('abc')).toBe('/events/abc');
					expect(emailer.BuildMatchUrl('def')).toBe('/events/def');
					done();
        });

        it("SafeUrl", function(done) {
            expect(emailer.SafeUrl({}).isNothing).toBe(true);
            expect(emailer.SafeUrl({key: 'abc'}).get()).toBe('/events/abc');
            done();
        });

        it("SafeAvatarIcon", function(done) {
					expect(emailer.SafeAvatarIcon({}).isNothing).toBe(true);
					expect(emailer.SafeAvatarIcon({players:{}}).isNothing).toBe(true);
					expect(emailer.SafeAvatarIcon({players:[{avataricon:'abc'}]}).get()).toBe('abc');
					done();
        });

				it("SendInviteTemplateEmailData", function(done) {
					expect(emailer.sendInviteTemplateEmailData({})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/send-invitetemplate', method : 'POST', data : { email : '', mailplan : [ { template : 'invite-hosttemplate-001', hours : -1 } ], userdata : { email : '', hostname : 'host', eventlocation : '', eventtime : '', eventname : '', url : 'https://www.rollforgroup.com/events/' } } });
					expect(emailer.sendInviteTemplateEmailData({players:[{email:'e'}]})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/send-invitetemplate', method : 'POST', data : { email : 'e', mailplan : [ { template : 'invite-hosttemplate-001', hours : -1 } ], userdata : { email : 'e', hostname : 'host', eventlocation : '', eventtime : '', eventname : '', url : 'https://www.rollforgroup.com/events/' } } });
					expect(emailer.sendInviteTemplateEmailData({players:[{email:'e', name:'n'}]})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/send-invitetemplate', method : 'POST', data : { email : 'e', mailplan : [ { template : 'invite-hosttemplate-001', hours : -1 } ], userdata : { email : 'e', hostname : 'n', eventlocation : '', eventtime : '', eventname : '', url : 'https://www.rollforgroup.com/events/' } } });
					expect(emailer.sendInviteTemplateEmailData({players:[{email:'e', name:'n'}], locname:'ln'})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/send-invitetemplate', method : 'POST', data : { email : 'e', mailplan : [ { template : 'invite-hosttemplate-001', hours : -1 } ], userdata : { email : 'e', hostname : 'n', eventlocation : 'ln', eventtime : '', eventname : '', url : 'https://www.rollforgroup.com/events/' } } });
					expect(emailer.sendInviteTemplateEmailData({players:[{email:'e', name:'n'}], locname:'ln', title:'tt'})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/send-invitetemplate', method : 'POST', data : { email : 'e', mailplan : [ { template : 'invite-hosttemplate-001', hours : -1 } ], userdata : { email : 'e', hostname : 'n', eventlocation : 'ln', eventtime : '', eventname : 'tt', url : 'https://www.rollforgroup.com/events/' } } });
					expect(emailer.sendInviteTemplateEmailData({players:[{email:'e', name:'n'}], locname:'ln', title:'tt', key:'k1'})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/send-invitetemplate', method : 'POST', data : { email : 'e', mailplan : [ { template : 'invite-hosttemplate-001', hours : -1 } ], userdata : { email : 'e', hostname : 'n', eventlocation : 'ln', eventtime : '', eventname : 'tt', url : 'https://www.rollforgroup.com/events/k1' } } });
					done();
				});

				it("findPlayerNameByEmail", function(done) {
					expect(emailer.findPlayerNameByEmail('e', []).isJust).toBeFalsy();
					expect(emailer.findPlayerNameByEmail('e', [{}, {}]).isJust).toBeFalsy();
					expect(emailer.findPlayerNameByEmail('e', [{players:[{email:'e', name:'y'}, {email:'j', name:'x'}]}]).get()).toBe('y');
					expect(emailer.findPlayerNameByEmail('j', [{players:[{email:'e', name:'y'}, {email:'j', name:'x'}]}]).get()).toBe('x');
					
					done();
				});


				it("sendCreateErrUsernameExistsData", function(done) {
					expect(emailer.sendCreateErrUsernameExistsData({})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/createuser-err-emailexists', method : 'POST', data : { email : '', mailplan : [ { template : 'create-err-username-exists-001', hours : -1 } ], userdata : { email : '', name : '' } } });
					expect(emailer.sendCreateErrUsernameExistsData({profile:{}})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/createuser-err-emailexists', method : 'POST', data : { email : '', mailplan : [ { template : 'create-err-username-exists-001', hours : -1 } ], userdata : { email : '', name : '' } } });
					expect(emailer.sendCreateErrUsernameExistsData({email: 'abc', profile:{}})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/createuser-err-emailexists', method : 'POST', data : { email : 'abc', mailplan : [ { template : 'create-err-username-exists-001', hours : -1 } ], userdata : { email : 'abc', name : '' } } });
					expect(emailer.sendCreateErrUsernameExistsData({email: 'abc', profile:{name:'xyz'}})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/createuser-err-emailexists', method : 'POST', data : { email : 'abc', mailplan : [ { template : 'create-err-username-exists-001', hours : -1 } ], userdata : { email : 'abc', name : 'xyz' } } });
					done();
				});


				it("sendForgotEmailData", function(done) {
					expect(emailer.sendForgotEmailData({})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/createuser-err-emailexists', method : 'POST', data : { email : '', mailplan : [ { template : 'create-err-username-exists-001', hours : -1 } ], userdata : { email : '', name : '' } } });
					expect(emailer.sendForgotEmailData({profile:{}})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/createuser-err-emailexists', method : 'POST', data : { email : '', mailplan : [ { template : 'create-err-username-exists-001', hours : -1 } ], userdata : { email : '', name : '' } } });
					expect(emailer.sendForgotEmailData({email: 'abc', profile:{}})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/createuser-err-emailexists', method : 'POST', data : { email : 'abc', mailplan : [ { template : 'create-err-username-exists-001', hours : -1 } ], userdata : { email : 'abc', name : '' } } });
					expect(emailer.sendForgotEmailData({email: 'abc', profile:{name:'xyz'}})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/createuser-err-emailexists', method : 'POST', data : { email : 'abc', mailplan : [ { template : 'create-err-username-exists-001', hours : -1 } ], userdata : { email : 'abc', name : 'xyz' } } });
					done();
				});



				it("sendForgotPasswordData", function(done) {
					expect(emailer.sendForgotPasswordData('resetlink', {})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/reset-password-request', method : 'POST', data : { email : '', mailplan : [ { template : 'reset-password-request-001', hours : -1 } ], userdata : { email : '', name : '', resetlink: 'resetlink' } } });
					expect(emailer.sendForgotPasswordData('resetlink', {profile:{}})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/reset-password-request', method : 'POST', data : { email : '', mailplan : [ { template : 'reset-password-request-001', hours : -1 } ], userdata : { email : '', name : '', resetlink: 'resetlink' } } });
					expect(emailer.sendForgotPasswordData('resetlink', {email: 'abc', profile:{}})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/reset-password-request', method : 'POST', data : { email : 'abc', mailplan : [ { template : 'reset-password-request-001', hours : -1 } ], userdata : { email : 'abc', name : '', resetlink: 'resetlink' } } });
					expect(emailer.sendForgotPasswordData('resetlink', {email: 'abc', profile:{name:'xyz'}})).toEqual({ url : 'http://localhost:3016/api/v1/emailer/reset-password-request', method : 'POST', data : { email : 'abc', mailplan : [ { template : 'reset-password-request-001', hours : -1 } ], userdata : { email : 'abc', name : '', resetlink : 'resetlink' } } });
					done();
				});






        it("close server", function(done) {
            mongo.CloseDatabase();
            done();
        });
    });
});
