var Promise = require('bluebird');
var R = require('ramda');
var util = require('util')
var log = require('../app/log.js');

var passport = require('passport');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var stage_page = require('../app/stage_page.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var match = require('../app/models/match.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 3028;
var login = require('connect-ensure-login');

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);


// ============== END SERVER FOR TESTS =======
var closeServer = function() {
    testapp.close(function(){
        console.log('matches_service_spec closed on port ' + httpPort);
    });
};


// ============== RUN TESTS ================== 
describe("stage_page_spec.js", function() {

    var server;

    // loginUser :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
    var verifyregister = function(account, zombieb, email, pass, userobj) {
      return new Promise(function(fulfill, reject) {
        accounts.DeleteAccount(account, email).then(function(y) {
          var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

          register(email, pass).then(function(x) {
            account.update({email: email}, userobj).exec().then(function(z) {
              zombieb.visit(base_url + "login", function(err) {
                if (err) 
                  reject(err);

                zombieb.fill('input[name="form-username"]', email)
                zombieb.fill('input[name="form-password"]', pass)
                zombieb.pressButton('login', function() {
                  fulfill(x);
                });
              });
            });
          });
        });
      });
    };

    describe("web page tests", function() {

      var userEmail = faker.Internet.email();
      var userPass = 'testpassword';

      it("setup server", function(done) {

        testapp.get('/', function (req, res) {
          res.send('<html>Home Page</html>')
        });

        stage_page.RouteStage(testapp, login);

        login_page.RouteLogins(testapp, passport);
        login_page.PostLogin(testapp, passport);
        login_page.RouteVerify(testapp, account);
        server = testapp.listen(httpPort, function() {
          done();
        });
      });

      it("non logged in user visit hostgame", function(done) {
        page = 'stage1';
        browser.visit(base_url + page, function(err) {
          expect(browser.success).toBe(true);
          expect(browser.html()).toContain('Roll for Group');
          done();
        });
      });

      it("user login", function(done) {
        verifyregister(account, browser, userEmail, userPass, {} ).then(function(x) {
         done();
        });
      });

      // TODO: put zombie tests in here
      it("valid user visit /stage1", function(done) {
        browser.visit(base_url + 'stage1', function(err) {
          expect(browser.success).toBe(true);
          expect(browser.html('.bs-wizard')).toContain('Your Goals');
          expect(browser.html('.brandname')).toContain('Roll for Group');
          done();
        });
      });

      it('server close', function(done) {
        server.close(function() {
          done();
        });
      });

      it('db close', function(done) {
        mongo.CloseDatabase();
        done();
      });

    });
});
