var express = require("express"); 
var router = express.Router(); 
var log = require('../app/log.js'); 
var R = require('ramda'); 
var assoc   = R.assoc, 
    converge = R.converge,
    compose = R.compose,
    composeP = R.composeP,
    contains = R.contains,
    curry   = R.curry,
    equals  = R.equals,
    filter  = R.filter,
    flatten = R.flatten,
    has     = R.has,
    hasIn   = R.hasIn,
    head    = R.head,
    join    = R.join,
    last    = R.last,
    length  = R.length,
    map     = R.map,
    merge   = R.merge,
    not     = R.not,
    nth     = R.nth,
    or      = R.or,
    remove  = R.remove,
    path    = R.path,
    pipe    = R.pipe,
    prop    = R.prop,
    when    = R.when
    whereEq = R.whereEq,
    zipObj  = R.zipObj;


var S = require('./../app/lambda.js');
const shortid = require('shortid');

var Promise = require('bluebird');
var faker = require('Faker');

var Task = require('data.task');
var Maybe = require('data.maybe');
var Either = require('data.either');

var Browser = require("zombie");
var browser = new Browser();

var aguid = require('aguid');

var genhelper = require('./genhelper.js');

var moment = require('moment');
    // Date Formats
    // http://apiux.com/2013/03/20/5-laws-api-dates-and-times/
    // ISO-8601
    // YYYY-MM-DDTHH:MM:SS-ZZZZ
    // - ZZZZ = timezone

var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var testBoardgame = require('../app/models/boardgame.js');
var testBoardgames = require('../app/models/boardgames.js');

var api_matches = require('../app/matches_service.js');
var emailer = require('../app/emailer.js');
var request = require('ajax-request');
var bodyParser = require('body-parser')

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var mongo = require('../app/mongoconnect.js');
var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');

var httpPort = 3004;

var currentyear = new Date().getFullYear();
var cachegame = testBoardgames.CacheBoardgame(testBoardgame, currentyear);

var testapp = express();

var nodemailer = require('nodemailer');
var stubTransport = require('nodemailer-stub-transport');
var transporter = nodemailer.createTransport(stubTransport());

const generateMatch = genhelper.GenerateFakeHostedMatch(account, testBoardgame, match);

const ajaxData = zipObj(['url', 'method', 'data']);

const findPlayerBySlug = curry((req, slug) => compose(prop('playerkey'), find(pipe(prop('slug'), equals(slug))), prop('body'))(req));

const findPlayerkey = curry((req, email) => compose(prop('playerkey'), find(pipe(prop('email'), equals(email))), path(['body', 'match', 'players']))(req));
const findPlayerType = curry((req, email) => compose(prop('type'), find(pipe(prop('email'), equals(email))), path(['body', 'match', 'players']))(req));
const findPlayersType = curry((req, email) => compose(prop('status'), find(pipe(path('name'), equals(email))), prop('body'))(req));
const findPlayersTypeBySlug = curry((req, slug) => compose(prop('status'), find(pipe(prop('slug'), equals(slug))), prop('body'))(req));

var ajax = require('./ajax.js');
var ajaxPromise = ajax.AjaxPromise;


// apiEndPoint :: s -> s -> s -> s -> s
const apiEndpoint = (base, url_api, matchkey, end) => base + url_api + '/' + matchkey + '/' + end;

// ============== VARS ===============
var url_base = "http://localhost:" + httpPort;
var server;

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  testapp.close(function(){
    console.log('matches_service_spec closed on port ' + httpPort);
  });
};

// buildHost :: o -> o -> s -> s -> d -> n -> n -> s -> o -> s -> [n, n] -> o -> bool) -> o 
const buildHost = function (match, account, email, key, gamedate, maxplayers, minplayers, status, social, locname, coords, gameObj, isprivate, experience) {

  // timeslotUnlessPrivate :: b -> d -> arr
  const timeslotUnlessPrivate = R.curry((isprivate, gamedate) => 
    (isprivate)
      ? [{id: shortid.generate(), startdatetime: gamedate, enddatetime: moment(gamedate).add(60*60, 's').toDate()}]
      : []
  );

  // host :: a -> Promise o 
  const host = R.compose(
    matches.HostMatch(match, account, email, key),
		R.merge({autoaccept: false}),
    R.zipObj(['timeslots', 'maxplayers', 'minplayers', 'status', 'social', 'locname', 'coords', 'games', 'isprivate', 'experience'])
  );

  return host([
    timeslotUnlessPrivate(isprivate, gamedate),
    // [{startdatetime: gamedate, enddatetime: moment(gamedate).add(1, 'h')}], 
    maxplayers, 
    minplayers, 
    status, 
    social, 
    locname, 
    coords, 
    gameObj, 
    isprivate, 
    experience
  ]);

};


var cacheGame = curry(function(testBoardgame, currentyear, gameid, gamename, thumbnail, gameyear) { 
  return new Promise(function(fulfill, reject) {
    var cacheObject = {
      numplays: 0,
      thumbnail: thumbnail,
      yearpublished: gameyear,
      name: {
        t: gamename,
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: gameid,
      objecttype: 'thing'
    };
    fulfill(testBoardgames.CacheBoardgame(testBoardgame, currentyear, cacheObject));
  });
});
var cacheGame2017 = cacheGame(testBoardgame, 2017);

var generateGame = genhelper.GenerateFakeBoardgame(testBoardgame);


// ============== RUN TESTS ================== 
describe("matches_service_spec.js api", function() {

  it("buildReoccur", function(done) {
    expect(api_matches.buildReoccur(0, 'never')).toEqual({reoccur:{enable:false}});
    expect(api_matches.buildReoccur(1, 'never')).toEqual({reoccur:{enable:false}});
    expect(api_matches.buildReoccur(0, 'a')).toEqual({reoccur:{enable:false}});
    expect(api_matches.buildReoccur(-1, 'b')).toEqual({reoccur:{enable:false}});
    expect(api_matches.buildReoccur(1, 'a')).toEqual({reoccur:{enable:true, interval:'a', every:1}});
    expect(api_matches.buildReoccur(2, 'b')).toEqual({reoccur:{enable:true, interval:'b', every:2}});
    done();
  });

  it("findPlayerByEmail", function(done) {
    expect(api_matches.findPlayerByEmail([], '').isNothing).toBeTruthy();
    expect(api_matches.findPlayerByEmail([{email: 'abc'}], '').isNothing).toBeTruthy();
    expect(api_matches.findPlayerByEmail([{email: 'abc'}], 'abc').get()).toEqual({email:'abc'});
    done();
  });

  describe("read basic data", function() {

    // url to test
    var url_api = '/api/v1/matches';

    // Number -> String -> Date -> Number -> Number -> Number -> Boolean
    var matchTest = function(gameid, locname, players, minplayers, status, smoking, alcohol) {
      return whereEq({
        locname:    locname,
        seatmin:    minplayers,
        seatmax:    players,
        status:     status,
        smoking:    smoking,
        alcohol:    alcohol
      });
    }

    // createLocalAcountObj:: String -> Number -> Number -> Object
    var createLocalAccountObj = function(location, lat, lng) {
      return {
        profile: {
          ishost: 1,
          name: faker.Name.findName(),
          localarea: {
            name: location,
            loc: {
              coordinates: [lng, lat],
              type: 'Point'
            }
          }
        }
      };
    };

    // addUserGame :: s -> s -> s -> n -> s -> s -> {o}
    const addUserGame = (email, userkey, status, matchkey, gameid, gamename, thumbnail) => ajaxData([
      apiEndpoint(url_base, url_api, matchkey, 'games'), 
      'POST', {
        email:      email, 
        key:        userkey, 
        status:     status, 
        gameid:     gameid,
        name:       gamename,
        thumbnail:  thumbnail 
      }]);


      var generateUser = genhelper.GenerateUser(account);
      var generateHostUser = genhelper.GenerateHostUser(account);

      // createBggGameObject :: Number -> String -> String -> Number -> JSONObject
      var createBggGameObject = function(gameid, gamename, thumbnail, year) {
        return {
          numbplays: 0,
          thumbnail: thumbnail,
          yearpublished: year,
          name: {
            t: gamename,
            sortindex: 1
          },
          subtype: 'boardgame',
          objectid: gameid,
          objecttype: 'thing'
        };
      };

      // getify :: JSONObject -> JSONObject
      var getify = function(dataObj) {            
        return {
          url: url_base + url_api,
          method: 'GET',
          data: dataObj
        };
      };

      it("scrub", function(done) {
          const expectObj = {a: 'scrubbed'};
          expect(api_matches.Scrub(R.zipObj(['a'], ['scrubbed']))).toEqual(expectObj);
          expect(api_matches.Scrub(R.zipObj(['a', '_id', '__v'], ['scrubbed', 1, 2]))).toEqual(expectObj);
          done();
      });

      it("cleanPlayer", function(done) {
        expect(api_matches.cleanPlayer({players:[]})).toEqual([]);
        expect(api_matches.cleanPlayer({players:[{a:'x'}, {a:'y', _id:'s'}]})).toEqual([{a:'x'}, {a:'y'}]);
        done();
      });

      it("assocPlayers", function(done) {
        expect(api_matches.assocPlayers({players:[]})).toEqual({players:[]})
        expect(api_matches.assocPlayers({players:[{a:'x'}, {a:'y', _id:'s'}]})).toEqual({ players : [ { a : 'x' }, { a : 'y' } ] })
        done();
      });

      it("setup server", function(done) {
        mongo.ConnectDatabase(dbUri);
        var router = api_matches.SetupRouter(nconf.get('smtp:user'), transporter);
        testapp.use(url_api, router);

        server = testapp.listen(httpPort, function() {
          done();
        });
      });

      it('setup matches', function(done) {
        var removeGame = testBoardgames.DeleteBoardgame(testBoardgame);
        Promise.all([1200, 1210, 1220].map(removeGame)).then(function(x) {
          done()
        });
      });

      it('create games', function(done) {

        var cacheObject = [];

        cacheObject[0] = createBggGameObject(1200, 'Sagarian', '//cf.geekdo-images.com/oKU8lU5hDydoxIs3hsVhOovKeKM=/fit-in/246x300/pic419519.jpg', 2000); 
        cacheObject[1] = createBggGameObject(1210, 'Cults Across America', '//cf.geekdo-images.com/sEPA_wTLJqpUARDCW4SSCrULu2M=/fit-in/246x300/pic453845.jpg', 1988); 
        cacheObject[2] = createBggGameObject(1220, 'Looping', '//cf.geekdo-images.com/8816L2GQ3htN_PmTFMJc_oPLdWk=/fit-in/246x300/pic12828.jpg', 1988); 

        Promise.all(map(cachegame, cacheObject)).then(
          function(x) {
            done();
          }
        );

      });

      it("delete matches", function(done) {
        match.remove({}).exec().then(function(x) {
          done();
        });
      });

      it("test matches all public matches", function(done) {
        var requestData = getify({});
        ajaxPromise(requestData).then(function(x) {
          var readStatus = R.compose(R.prop('statusCode'));
          expect(readStatus(x)).toBe(200); 
          done();
        });
      });


      it("api/v1/matches", function(done) {

        var gameDate1 = new Date(2017, 07, 31, 20, 30, 0); 
        var gameDate2 = new Date(2017, 07, 31, 08, 30, 0); 
        var gameDate3 = new Date(2017, 09, 21, 20, 30, 0); 

        var gameEndDate1 = new Date(2017, 07, 31, 21, 30, 0); 
        var gameEndDate2 = new Date(2017, 07, 31, 09, 30, 0); 
        var gameEndDate3 = new Date(2017, 09, 21, 21, 30, 0); 

        var coords = [10, 10];

        var createMatches = [
          matches.CreateMatch(match, 'matchuser1@gmail.com', false, R.zipObj(['games', 'locname', 'coords', 'gamedate', 'timeend', 'maxplayers', 'minplayers', 'status', 'social'], [[{id:1200}], 'Place1', coords, gameDate1, gameEndDate1, 5, 1, 'open', {}])),
          matches.CreateMatch(match, 'matchuser1@gmail.com', false, R.zipObj(['games', 'locname', 'coords', 'gamedate', 'timeend', 'maxplayers', 'minplayers', 'status', 'social'], [[{id:1200}], 'Place2', coords, gameDate2, gameEndDate1, 4, 2, 'open', {}])),
          matches.CreateMatch(match, 'matchuser1@gmail.com', false, R.zipObj(['games', 'locname', 'coords', 'gamedate', 'timeend', 'maxplayers', 'minplayers', 'status', 'social'], [[{id:1200}], 'Place3', coords, gameDate3, gameEndDate3, 3, 3, 'open', {}]))
        ];

        Promise.all(createMatches).then(function(y) {
          done();
        });

      });

      it("api/v1/matches/findqueries", function(done) {

        var user1 = 'user1_findnearby@gmail.com';
        var user2 = 'user2_findnearby@gmail.com';
        var user3 = 'user3_findnearby@gmail.com';
        var users = [user1, user2, user3];

        var distance = 50000; // in meters

        var matchLocName1 = 'Port-aux-Français, French Southern and Antarctic Lands';
        var lat1 = -49.3496420;
        var lng1 = 70.2180040;

        var matchLocName2 = 'National Park "Russian Arctic", pr. Sovetskikh Kosmonavtov, 57, Arkhangel\'sk, Arkhangelskaya oblast\', Russia, 163000';
        var lat2 = 64.5385530;
        var lng2 = 40.5389430;

        var matchLocName3 = 'Port Couvreux, French Southern and Antarctic Lands';
        var lat3 = -49.27916;
        var lng3 = 69.690819;

        var thumbnail = '//cf.geekdo-images.com/q32KDF-KFiI2ZP8POk-fh5v2JOI=/fit-in/246x300/pic3184103.jpg';
        var gameyear = 2016;

        var gameDate = new Date(2020, 08, 03, 20, 30, 0); 
        var minPlayers = 2;
        var players = 4;
        var status = 'open';
        var smoking = 1;
        var alcohol = 1;
        
        var checkDistance = R.compose(
          R.prop('distance'), 
          R.head, 
          R.flatten
        ); 

        var createUser1 = accounts.RespawnAccountObject(account, user1, createLocalAccountObj(matchLocName1, lat1, lng1))
        var createUser2 = accounts.RespawnAccountObject(account, user2, createLocalAccountObj(matchLocName2, lat2, lng2))
        var createUser3 = accounts.RespawnAccountObject(account, user3, createLocalAccountObj(matchLocName3, lat3, lng3))

        Promise.all([createUser1, createUser2, createUser3]).then(function(u) {

          var requestData = [
            {distance: 5000,  latitude: lat1, longitude: lng1},
            {distance: 5000,  nearby: user2},
            {distance: 5000,  nearby: user2,  key: 'skfsljfsdf'},
            {distance: 5000,  latitude: lat3, longitude: lng3},
            {distance: 50000, latitude: lat3, longitude: lng3},
            {distance: 50000, nearby: user3,  key: u[2].key},
            {}
          ];

          const promiseGetify = R.pipe(getify, ajaxPromise);

          Promise.all([generateGame]).then(function(g) {

            var hostit = (user) => buildHost(match, account, user.email, user.key, gameDate, players, minPlayers, status, {}, user.profile.localarea.name, user.profile.localarea.loc.coordinates, [{id: g[0].id}], false);

            Promise.all([hostit(u[0]), hostit(u[1])]).then(function(m) {
              Promise.all(R.map(promiseGetify, requestData)).then(function(x) {

                var testResultLength = R.compose(length, path(['body', 'matches'])); 

                expect(testResultLength(x[0])).toBe(1);

                var match1Test = R.compose(
                  matchTest(g[0].id, matchLocName1, players, minPlayers, status, smoking, alcohol), 
                  head, 
                  path(['body', 'matches'])
                );

                expect(match1Test(x[0])).toBe(true); 

                var isScrubbed = R.compose(not, converge(or, [has('__v'), has('_id')]), head, path(['body', 'matches']));
                expect(isScrubbed(x[0])).toBe(true);

                var readStatus = compose(prop('statusCode'));
                expect(readStatus(x[1])).toBe(401); 

                var readStatus = compose(prop('statusCode'));
                expect(readStatus(x[2])).toBe(401); 
                expect(testResultLength(x[3])).toBe(0);
                expect(testResultLength(x[4])).toBe(1);
                expect(match1Test(x[4])).toBe(true); 
                expect(isScrubbed(x[4])).toBe(true);

                var readDistance = compose(prop('distance'), head, path(['body', 'matches']));
                expect(readDistance(x[4])).toBe(39054);
                expect(testResultLength(x[5])).toBe(1);

                var match2Test = compose(matchTest(g[0].id, matchLocName1, players, minPlayers, status, smoking, alcohol), head, path(['body', 'matches']));
                expect(match2Test(x[5])).toBe(true); 
                expect(isScrubbed(x[5])).toBe(true);

                expect(readStatus(x[6])).toBe(200); 
                // expect(testResultLength(x[6])).toBe(0);


                promiseGetify({distance: 5000,  nearby: user2,  key: u[1].key}).then(function(y) {
                  expect(y.body.matches[0].players[0].name).toBe(u[1].profile.name);
                  expect(y.body.matches[0].issignedup).toBeTruthy();
                  done();

                }).catch(err => log.clos('err', err));
              }); 
            })
          });
        });
      });




      it("api/v1/matches?event", function(done) {

        var gameid = 226562;
        var gamename = 'Immortals';
        var thumbnail = 'https://cf.geekdo-images.com/AMa2RndcDnnMV6q9DCUXLq0uMJ4=/fit-in/246x300/pic3544682.jpg';
        var gameyear = 2017;

        var matchx = {'games.id': gameid};
        var playyear = new Date().getFullYear() + 1;
        var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
        var gameEnd = new Date(playyear, 08, 03, 22, 30, 0); 
        var gameEnd24HrPlus = new Date(playyear, 08, 05, 22, 30, 0); 

        var matchLocName = 'University of Wollongong';
        var matchUser = {
          email: 'match1@gmail.com', 
          avataricon: 'http://localhost:3000/img/test-avatar1.jpg',
          name: 'Jeff Jones'
        };

        var minPlayers = 2;
        var matchPlayers = 5;
        var lat = -34.4054039;
        var lng = 150.8784299;
        var coords = [lng, lat];

        var readLat = compose(last, path(['loc', 'coordinates']));
        var readLng = compose(head, path(['loc', 'coordinates']));

        const readHostkey = (keyname, obj) => compose(prop(keyname), head, prop('players'))(obj);

        const status1 = 'open';
        const status2 = 'open';
        const smoking = 2;
        const alcohol = 1;
        const eventid = 'servicee3ent';

        const social = {
          smoking: smoking,
          alcohol: alcohol
        };

        const createMatchObj = R.compose(R.of, matches.Zipmatch, R.zipObj(['id', 'name', 'thumbnail', 'description', 'minplaytime', 'maxplaytime', 'minplayers', 'maxplayers']));

        var requestData = [];
        requestData[0] = {event: eventid};
        requestData[1] = {event: 'noid'};

        const makeEventMatch = R.compose(
          matches.CreateMatch(match, matchUser, false), 
          R.zipObj(['locname', 'coords', 'gamedate', 'timeend', 'maxplayers', 'minplayers', 'status', 'social', 'games', 'eventid', 'isprivate'])
        );

        const match24HrPlus = makeEventMatch([matchLocName, coords, gameDate, gameEnd24HrPlus, matchPlayers, minPlayers, status2, social, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 4]), eventid, true]);
        const match4 = makeEventMatch([matchLocName, coords, gameDate, gameEnd, matchPlayers, minPlayers, status2, social, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 4]), eventid, true]);


        match.remove(matchx).then(function(y) {
          cacheGame2017(gameid, gamename, thumbnail, gameyear).then(function(y) {
            match24HrPlus.then().catch(function(a) {
              expect(a).toBe('The event should be more than thirty minutes and less than 24hrs duration.');
              match4.then(function(y) {
                Promise.all(map(pipe(getify, ajaxPromise), requestData)).then(function(x) {

                  var readStatus = compose(prop('statusCode'));
                  expect(map(readStatus, x)).toEqual([200, 200]); 

                  var emptyResponse = compose(equals(0), length, path(['body', 'matches']));
                  expect(emptyResponse(x[0])).toBe(false);
                  expect(emptyResponse(x[1])).toBe(true);

                  var isScrubbed = compose(not, converge(or, [has('__v'), has('_id')]), head, path(['body', 'matches']));
                  expect(isScrubbed(x[0])).toBe(true);

                  expect(x[0].body.matches[0].eventid).toBe(eventid);

                  done();

                }); 
              });


            });

          }).catch(err => log.clos('caught', err));
        }); 

      });

      it("api/v1/matches?types", function(done) {

        var gameDate = new Date(2020, 08, 03, 20, 30, 0); 
        var players = 3;
        var minPlayers = 2;
        var status = 'open';
        var smoking = 1;
        var alcohol = 1;
        
        var requestData = [];
        Promise.all([generateHostUser({}), generateHostUser({}), generateGame]).then(function(u) {

          var matchtypes = ['host', 'invite', 'request', 'approve'];
          requestData[0] = {types: matchtypes, user: u[0].email, key: u[0].key};
          requestData[1] = {types: matchtypes, user: u[1].email, key: u[1].key};
          requestData[2] = {types: matchtypes, user: u[0].email, key: 'badkey'};

          var hostit = (user) => buildHost(match, account, user.email, user.key, gameDate, players, minPlayers, status, {}, user.profile.localarea.name, user.profile.localarea.loc.coordinates, [{id: u[2].id}], 1);

          Promise.all([hostit(u[0])]).then(function(m) {

            Promise.all(R.map(R.pipe(getify, ajaxPromise), requestData)).then(function(x) {

              var readStatus = R.compose(R.prop('statusCode'));
              expect(map(readStatus, x)).toEqual([200, 200, 401]); 

              var emptyResponse = R.compose(R.equals(0), length, path(['body', 'matches']));
              expect(emptyResponse(x[0])).toBe(false);
              expect(emptyResponse(x[1])).toBe(true);

              var testResponse = matchTest(u[2].id, u[0].profile.localarea.name, players, status, smoking, alcohol);
              expect(emptyResponse(x[0])).toBe(false);

              var isScrubbed = compose(not, converge(or, [has('__v'), has('_id')]), head, path(['body', 'matches']));
              expect(isScrubbed(x[0])).toBe(true);

              expect(x[0].body.matches[0].usertype).toBe('host');

              done();

            }); 
          });
        });
      });


      it("api/v1/matches?experience", function(done) {

        var gameDate = new Date(2020, 08, 03, 20, 30, 0); 
        var players = 3;
        var minPlayers = 2;
        var status = 'open';
        var smoking = 1;
        var alcohol = 1;
        
        var requestData = [];
        Promise.all([generateHostUser({}), generateHostUser({}), generateGame]).then(function(u) {

          var hostit = R.curry((user, experience) => buildHost(match, account, user.email, user.key, gameDate, players, minPlayers, status, {}, user.profile.localarea.name, user.profile.localarea.loc.coordinates, [{id: u[2].id}], false, experience));

          Promise.all([hostit(u[0], 1), hostit(u[0], 6), hostit(u[0], 11)]).then(function(m) {

            requestData[0] = {experience: [6, 11]};
            requestData[1] = {experience: 1};
            requestData[2] = {experience: [22]};
            requestData[3] = {experience: [1, 6, 11]};

            Promise.all(R.map(R.pipe(getify, ajaxPromise), requestData)).then(function(x) {

              // readStatus :: o -> s
              const readStatus = R.prop('statusCode');

              expect(R.map(readStatus, x)).toEqual([200, 200, 200, 200]); 

              // emptyResponse :: a -> bool
              const emptyResponse = R.compose(
                R.equals(0), 
                R.length, 
                R.path(['body', 'matches'])
              );

              //checkExperience :: a -> o -> bool
              const checkExperience = R.curry((vals, resp) => {

                // checkVal a -> n -> bool
                const checkVal = R.curry((a, b) => R.map(R.equals(b), a));

                // o -> bool
                return R.compose(
                  R.reduce(R.and, true),
                  R.map(R.reduce(R.or, false)),
                  R.map(checkVal(vals)),
                  R.map(R.prop('experience')),
                  R.path(['body', 'matches'])
                )(resp);

              });

              expect(checkExperience([6, 11], x[0])).toBeTruthy();
              expect(checkExperience([1], x[0])).toBeFalsy();
              expect(checkExperience([1], x[1])).toBeTruthy();
              expect(x[2].body.matches.length).toBe(0);
              expect(checkExperience([1, 6, 11], x[3])).toBeTruthy();

              // isScrubbed :: o -> bool
              const isScrubbed = R.compose(
                R.not, 
                R.converge(R.or, [R.has('__v'), R.has('_id')]), R.head, R.path(['body', 'matches'])
              );
              expect(isScrubbed(x[0])).toBe(true);
              done();

            }); 
          });
        });
      });


      it("api/v1/matches?history", function(done) {

        var gameyear = 2017;
        var gameDate = new Date(2017, 08, 03, 20, 30, 0); 
        var players = 3;
        var minPlayers = 2;
        var status1 = 'open';
        var status2 = 'played';
        var smoking = 1;
        var alcohol = 1;

        var requestData = [];
        Promise.all([generateHostUser({}), generateHostUser({}), generateHostUser({}), generateGame]).then(function(u) {

          var matchtypes = ['host', 'invite', 'request', 'approve'];
          requestData[0] = {history: true, types: matchtypes, user: u[0].email, key: u[0].key};
          requestData[1] = {history: true, types: matchtypes, user: u[1].email, key: u[1].key};
          requestData[2] = {history: true, types: matchtypes, user: u[0].email, key: 'badkey'};

          var hostit = curry((user, status) => buildHost(match, account, user.email, user.key, gameDate, players, minPlayers, status, {}, user.profile.localarea.name, user.profile.localarea.loc.coordinates, [{id: u[3].id}], 1));
          Promise.all([hostit(u[0], status1), hostit(u[0], status2)]).then(function(m) {
            Promise.all(map(pipe(getify, ajaxPromise), requestData)).then(function(x) {

              var readStatus = R.compose(prop('statusCode'));
              expect(map(readStatus, x)).toEqual([200, 200, 401]); 

              var emptyResponse = R.compose(equals(0), length, path(['body', 'matches']));

              expect(emptyResponse(x[0])).toBe(false);
              expect(emptyResponse(x[1])).toBe(true);

              expect(x[0].body.matches[0].status).toBe(status2);
              expect(emptyResponse(x[0])).toBe(false);

              var isScrubbed = compose(not, converge(or, [has('__v'), has('_id')]), head, path(['body', 'matches']));
              expect(isScrubbed(x[0])).toBe(true);
              expect(x[0].body.matches[0].usertype).toBe('host');

              done();

            }); 
          });
        });
      });

      it("findLastInvitedEmail", function(done) {
        expect(api_matches.findLastInvitedEmail({}).isJust).toBeFalsy();
        expect(api_matches.findLastInvitedEmail({players:[]}).isJust).toBeFalsy();
        expect(api_matches.findLastInvitedEmail({players:[{type:'accept'}, {type:'invite'}]}).isJust).toBeFalsy();
        expect(api_matches.findLastInvitedEmail({players:[{type:'invite', email:'bcd'}, {type:'accept', email:'abc'}]}).isJust).toBeFalsy();
        expect(api_matches.findLastInvitedEmail({players:[{type:'accept', email:'abc'}, {type:'invite', email:'bcd'}]}).get()).toEqual('bcd');
        done();
      });

      it("PrepareMatchForAPI", function(done) {

        var fakematch1 = {
          players: [
            {_id: 'id1', email: 'ab'},
            {_id: 'id2', email: 'cd'},
            {_id: 'id3', email: 'ef'},
          ]
        };
        fakematch1.distance = 100;

        var fakematch2 = {
          players: [
            {_id: 'id1', email: 'ab'},
            {_id: 'id2', email: 'cd'},
            {_id: 'id3', email: 'ef'},
          ]
        };

        var testfakematch = whereEq({
          players: [
            {email: 'ab'},
            {email: 'cd'},
            {email: 'ef'},
          ]
        });

        var testit = compose(testfakematch, api_matches.PrepareMatchForAPI);
        var hasdistance = compose(has('distance'), api_matches.PrepareMatchForAPI);
        
        expect(testit(fakematch1)).toBe(true);
        expect(hasdistance(fakematch1)).toBe(true);
        expect(hasdistance(fakematch2)).toBe(false);

        done();

      });

      it("GetName", function(done) {
        expect(api_matches.GetName({}, {profile: {name: 'abc'}})).toBe('abc');
        expect(api_matches.GetName({}, {})).toBe('No Name Set');
        expect(api_matches.GetName({type:'invite'}, {})).toBe('');
        done();
      });

      it("BuildProfileChatClass", function(done) {
        expect(api_matches.BuildProfileChatClass({})).toBe('img-circle imglistcircle-standard-thin user-chat-avatar vutips');
        expect(api_matches.BuildProfileChatClass({premium:{isactive:true}})).toBe('img-circle imglistcircle-premium-thin user-chat-avatar vutips');
        expect(api_matches.BuildProfileChatClass({premium:{isstaff:true}})).toBe('img-circle imglistcircle-staff-thin user-chat-avatar vutips');
        done();
      });


      it("BuildProfileClass", function(done) {
        expect(api_matches.BuildProfileClass({})).toBe('img-circle imglistcircle-standard-thin user-players-avatar vutips');
        expect(api_matches.BuildProfileClass({premium:{isactive:true}})).toBe('img-circle imglistcircle-premium-thin user-players-avatar vutips');
        expect(api_matches.BuildProfileClass({premium:{isstaff:true}})).toBe('img-circle imglistcircle-staff-thin user-players-avatar vutips');
        done();
      });


      it("BuildNamClass", function(done) {
        expect(api_matches.BuildNameClass({})).toBe('a-name-standard');
        expect(api_matches.BuildNameClass({premium:{isactive:true}})).toBe('a-name-premium');
        expect(api_matches.BuildNameClass({premium:{isstaff:true}})).toBe('a-name-staff');
        done();
      });


      it("BuildBadges", function(done) {
        expect(api_matches.BuildBadges({})).toEqual([]);
        expect(api_matches.BuildBadges({premium:{isactive:true}})).toEqual([ { showImg : true, imgsrc : '/img/u_roll_icon1-01.png', faicon : '', tooltip : 'Premium Player' } ]);
        expect(api_matches.BuildBadges({premium:{isstaff:true}})).toEqual([]);
        done();
      });

      it("BuildLink", function(done) {
        expect(api_matches.BuildLink({})).toBe('');
        expect(api_matches.BuildLink({pageslug:'ps'})).toBe('/users?id=ps');
        done();
      });

      it('allPlayersExcept', function(done) {
        expect(api_matches.allPlayersExcept('abc', {}).isNothing).toBeTruthy();
        expect(api_matches.allPlayersExcept('abc', {players:[]}).get()).toEqual([]);
        expect(api_matches.allPlayersExcept('abc', {players:[{email:'def'}]}).get()).toEqual([{email:'def'}]);
        expect(api_matches.allPlayersExcept('abc', {players:[{email:'abc'}, {email:'def'}]}).get()).toEqual([{email:'def'}]);
        done();
      });

      it('getUserEmail', function(done) {
        expect(api_matches.getUserEmail({})).toBe('');
        expect(api_matches.getUserEmail({user:{}})).toBe('');
        expect(api_matches.getUserEmail({user:{email:'frank'}})).toBe('frank');
        done();
      });

      it('buildMatchURL', function(done) {
        expect(api_matches.buildMatchURL('abc')).toBe('/events/abc');
        expect(api_matches.buildMatchURL('def')).toBe('/events/def');
        done();
      });


      it("api/v1/matches/match_key GET", function(done) {

        var gameid = 1200;
        var gameDate1 = new Date(2017, 07, 31, 20, 30, 0); 
        var gameEnd1 = new Date(2017, 07, 31, 22, 30, 0); 
        var coords = [10, 10];

        var price = 8.5
        var currency = "GBP"

        matches.CreateMatch(match, 'matchuser1@gmail.com', false, R.zipObj(['locname', 'coords', 'gamedate', 'timeend', 'maxplayers', 'minplayers', 'status', 'social', 'games', 'price', 'currency'], ['Place1', coords, gameDate1, gameEnd1, 5, 2, 'open', {smoking:1, alcohol:2}, wrapId(gameid), price, currency ])).then(function(m) {
          var requestData = {
            url: url_base + url_api + "/" + m.key,
            method: 'GET',
            data: {} 
          };

          ajaxPromise(requestData).then(function(x) {

            var readgameid =    R.compose(prop("gameid"), path(['body', 'matches']));
            var readgamename =  R.compose(prop("gamename"), path(['body', 'matches']));
            var readthumbnail = R.compose(prop("thumbnail"), path(['body', 'matches']));

            var testMatch =     R.compose(
              matchTest(1200, 'Place1', 5, 2, 'open', 1, 2), 
              prop('body')
            );

            expect(x.body.price).toBe(price);
            expect(x.body.currency).toBe(currency);
            expect(testMatch(x)).toBe(true);

            done();

          });
        });
      });


      it("api/v1/matches/match_key DELETE", function(done) {

        const datafy = zipObj(['email', 'key']);
        const findMatch = matches.FindMatchByKey(match);

        generateMatch('open', 2, 2).then(function(h) {
          var deleteData = ajaxData([url_base + url_api + "/" + h[1].key, 'DELETE', datafy(['invaliduser', 'invalidkey'])]);
          ajaxPromise(deleteData).then(function(x) {

            findMatch(h[1].key).then(function(m) {
              expect(m.length).toBe(1);
              expect(x.statusCode).toBe(403);

              deleteData = ajaxData([url_base + url_api + "/" + h[1].key, 'DELETE', datafy([h[0].email, h[0].key])]);

              ajaxPromise(deleteData).then(function(x) {
                expect(x.statusCode).toBe(200);
                findMatch(h[1].key).then(function(m) {
                  expect(m.length).toBe(0);
                  done();
                });
              });
            });
          });
        });
      });

      const endpoint = (base, api) => base + api;
      const userify = zipObj(['user', 'userkey', 'type']);
      const hostify = zipObj(['host', 'hostkey', 'playerkey', 'type']);

      var createJoinPostData = curry(function(url_base, url_api, email, userkey) {
        var datify = zipObj(['email', 'userkey', 'type']);
        return ajaxData([endpoint(url_base, url_api), 'POST', datify([email, userkey, 'request'])]);
      });

      var createInvitePostData = curry(function(url_base, url_api, host, hostkey, email) {
        var datify = zipObj(['host', 'hostkey', 'email', 'type', 'notsend']);
        return ajaxData([endpoint(url_base, url_api), 'POST', datify([host, hostkey, email, 'invite', true])]);
      });

      var createInvitePostDataByKey = curry(function(url_base, url_api, host, hostkey, userkey) {
        var datify = zipObj(['host', 'hostkey', 'userkey', 'type', 'notsend']);
        return ajaxData([endpoint(url_base, url_api), 'POST', datify([host, hostkey, userkey, 'invite', true])]);
      });

      var createStandbyPostDataByKey = R.curry(function(url_base, url_api, host, hostkey, userkey) {
        var datify = zipObj(['host', 'key', 'userkey', 'type']);
        return ajaxData([endpoint(url_base, url_api), 'POST', datify([host, hostkey, userkey, 'standby'])]);
      });


      const createPlayerAcceptPatchData = curry((url_base, url_api, user, userkey) => ajaxData([url_base + url_api, 'PATCH', userify([user, userkey, 'approve'])]));

      const createPlayerRejectPatchData = R.curry((url_base, url_api, user, userkey) => ajaxData([url_base + url_api, 'PATCH', userify([user, userkey, 'decline'])]));

      const createHostRejectPostData  = curry((url_base, url_api, host, hostkey, playerkey) => ajaxData([url_base + url_api, 'PATCH', hostify([host, hostkey, playerkey, 'reject'])]));

      const createApprovePostData     = curry((url_base, url_api, host, hostkey, playerkey) => ajaxData([url_base + url_api, 'PATCH', hostify([host, hostkey, playerkey, 'approve'])]));

      var createRemovePlayerByHost = curry(function(url_base, url_api, host, hostkey, playerkey) {
        var datify = zipObj(['host', 'key', 'playerkey']);
        return ajaxData([endpoint(url_base, url_api), 'DELETE', datify([host, hostkey, playerkey])]);
      });

      var createRemovePlayerByUser = curry(function(url_base, url_api, user, userkey) {
        var datify = zipObj(['user', 'key']);
        return ajaxData([endpoint(url_base, url_api), 'DELETE', datify([user, userkey])]);
      });

      const wrapId = R.compose(R.of, R.objOf('id'));

      var datify = zipObj(['games', 'title', 'description', 'date', 'timeend', 'maxplayers', 'minplayers', 'email', 'key', 'experience', 'status', 'smoking', 'alcohol', 'playarea', 'playarealng', 'playarealat', 'isprivate']);

      var datifyCurrency = zipObj(['games', 'title', 'description', 'date', 'timeend', 'maxplayers', 'minplayers', 'email', 'key', 'experience', 'status', 'smoking', 'alcohol', 'playarea', 'playarealng', 'playarealat', 'price', 'currency', 'isprivate']);

      var whenEventdatify = zipObj(['games', 'title', 'description', 'date', 'timeend', 'maxplayers', 'minplayers', 'email', 'key', 'experience', 'status', 'smoking', 'alcohol', 'playarea', 'playarealng', 'playarealat', 'eventid', 'price', 'currency', 'isprivate']);

      var createPostData = curry(function(url_base, url_api, data) {
        return ajaxData([endpoint(url_base, url_api), 'POST', data]);
      });

      var postData = R.compose(
        createPostData(url_base, url_api), 
        R.merge({
          'price':0, 
          'currency':'AUD', 
          'utcoffset':600, 
          'timezone':'Australia/Sydney', 
          'reoccurinterval':'never',
          'reoccurevery':'0',
					'autoaccept': 'false'
        }), 
        datify
      );

      var postDataCurrency = R.compose(
        createPostData(url_base, url_api), 
        R.merge({
          'utcoffset':600, 
          'timezone':'Australia/Sydney', 
          'reoccurinterval':'never',
          'reoccurevery':'0',
					'autoaccept': 'false'
        }), 
        datifyCurrency
      );

      var whenEventPostData = R.compose(
        createPostData(url_base, url_api), 
        R.merge({
          'utcoffset':600, 
          'timezone':'Australia/Sydney',
          'reoccurinterval':'never',
          'reoccurevery':'0',
					'autoaccept': 'false'
        }), 
        whenEventdatify
      );

      it("api/v1/matches POST fail unknown game", function(done) {

        var gameid = 20890; 
        var matchdate = '2017-10-20 20:30:00+1000';
        var maxplayers = 4;
        var minplayers = 2;
        var email = 'test_hostservice@gmail.com';
        var experiencelevel = 1;
        
        var locname = 'hosthouse';
        var lng = 11;
        var lat = 3; 

        var title = 'Game title';
        var description = 'How are you going description';
        var timeend = '2017-10-20T23:30:00+1000';

        testBoardgames.DeleteBoardgame(testBoardgame, gameid).then(function(y) {
          ajaxPromise(postData([wrapId(gameid), title, description, matchdate, timeend, maxplayers, minplayers, email, 'abcdef', 'open', experiencelevel, 1, 2, locname, lng, lat, false])).then(function(x) {
            expect(x).toEqual({statusCode:404});
            done();

          }).catch((err) => log.clos('err', err));
        });
      });

      it("api/v1/matches POST fail uknown user", function(done) {

        var gameid = 20890; 
        var gamename = 'Im Zeichen des Sechsecks: Klaus Teuber & Die Siedler von Catan';
        var thumbnail = '//cf.geekdo-images.com/images/pic106752_t.jpg';

        var locname = 'hosthouse';
        var lng = 11;
        var lat = 3; 
        var experiencelevel = 1;

        var title = 'Game title';
        var description = 'How are you going description';
        var timeend = '2017-10-20T23:30:00+1000';

        cacheObject = createBggGameObject(gameid, gamename, thumbnail, 2003); 
        cachegame(cacheObject).then(function(x) {

          var matchdate = '2017-10-20T20:30:00+1000';
          var maxplayers = 4;
          var minplayers = 2;
          var email = 'i_do_not_exist@gmail.com';
          
          ajaxPromise(postData([wrapId(gameid), title, description, matchdate, timeend, maxplayers, minplayers, email, 'abcdef', 'open', experiencelevel, 1, 2, locname, lng, lat, false])).then(function(x) {
            expect(x).toEqual({statusCode:404});
            // TODO: change to 401 - Unauthorised access
            done();

          });
        });
      });

      it("api/v1/matches POST fail based on user key", function(done) {

          var gameid = 20890; 
          var gamename = 'Im Zeichen des Sechsecks: Klaus Teuber & Die Siedler von Catan';
          var thumbnail = '//cf.geekdo-images.com/images/pic106752_t.jpg';

          var title = 'Game title';
          var description = 'How are you going description';
          var timeend = '2017-10-20T23:30:00+1000';
          var experiencelevel = 1;

          var locname = 'hosthouse';
          var lng = 11;
          var lat = 3; 

          cacheObject = createBggGameObject(gameid, gamename, thumbnail, 2003); 
          cachegame(cacheObject).then(function(x) {
              
              generateHostUser({}).then(function(u) {
             
                  var maxplayers = 4;
                  var minplayers = 2;
                  var matchdate = moment('2017-10-20T20:30:00+1000');

                  ajaxPromise(postData([wrapId(gameid), title, description, matchdate.toDate(), timeend, maxplayers, minplayers, u.email, 'abcdef', 'open', experiencelevel, 1, 2, locname, lng, lat, false])).then(function(x) {
                      expect(x).toEqual({statusCode:404});
                      done();
                  });

              });
          });
      });       

      it("api/v1/matches POST create match with host (success)", function(done) {

        var gameid = 20890; 
        var gamename = 'Im Zeichen des Sechsecks: Klaus Teuber & Die Siedler von Catan';
        var thumbnail = '//cf.geekdo-images.com/images/pic106752_t.jpg';

        cacheObject = createBggGameObject(gameid, gamename, thumbnail, 2003); 
        cachegame(cacheObject).then(function(x) {
            
          var title = 'Game title';
          var description = 'How are you going description';
          var timeend = '2017-10-20T23:30:00+1000';

          var testEmail = 'mapihost@gmail.com';
          var testBgg = 'mapihost';
          var locname = 'hosthouse';
          var coords = [10, 11];
          var experiencelevel = 4;

          var price = 10.50;
          var currency = 'AUD';

          var hostalcohol = 1;
          var hostsmoking = 2;

          // var mapiaccount = createHostAccountObj(locname, coords[0], coords[1]);
          var mapiaccount = createLocalAccountObj(locname, coords[0], coords[1]);

          generateHostUser({}).then(function(u) {
         
            var maxplayers = 4;
            var minplayers = 2;
            var matchdate = moment('2017-10-20T20:30:00+1000');

            ajaxPromise(postDataCurrency([[matches.defaultGame(gameid, gamename, thumbnail)], title, description, matchdate.toDate(), timeend, maxplayers, minplayers, u.email, u.key, experiencelevel, 'open', 1, 2, locname, coords[0], coords[1], price, currency, true])).then(function(x) {

              var status = 'open';
              var checkHttpStatus = compose(equals(200), prop('statusCode')); 
              expect(checkHttpStatus(x)).toBeTruthy;

              var readValue = (property) => path(['body', property]);

              expect(x.body.games[0].id).toBe(gameid);
              expect(x.body.games[0].name).toBe(gamename);
              expect(x.body.games[0].thumbnail).toBe(thumbnail);
              expect(x.body.games[0].email).toBe(u.email);

              expect(readValue('locname')(x)).toBe(locname);
              expect(readValue('seatmax')(x)).toBe(maxplayers);
              expect(readValue('seatmin')(x)).toBe(minplayers);
              expect(readValue('experience')(x)).toBe(experiencelevel);
              expect(readValue('status')(x)).toBe(status);
              expect(readValue('smoking')(x)).toBe(1);
              expect(readValue('alcohol')(x)).toBe(2);

              expect(readValue('price')(x)).toBe(price);
              expect(readValue('currency')(x)).toBe(currency);

              var validateHost = compose(prop('email'), head, path(['body', 'players']));
              expect(validateHost(x)).toBe(u.email);
              // expect(matchdate.diff(readValue('date')(x), 'minutes')).toBe(0);

              var validateCoords = compose(path(['body', 'loc', 'coordinates']));
              expect(validateCoords(x)[0]).toEqual(Number(coords[0]));
              expect(validateCoords(x)[1]).toEqual(Number(coords[1]));

              done();

            });
          });
        });
      });       

      it("api/v1/matches POST create match with multiple games and host (success)", function(done) {

        var gameid1 = 20890; 
        var gamename1 = 'Im Zeichen des Sechsecks: Klaus Teuber & Die Siedler von Catan';
        var thumbnail1 = '//cf.geekdo-images.com/images/pic106752_t.jpg';

        var gameid2 = 45315; 
        var gamename2 = 'Dungeon Lords';
        var thumbnail2 = 'https://cf.geekdo-images.com/boMLARyKxWH0rMkr2HKNSmoqKHA=/fit-in/246x300/pic569340.jpg';

        cacheObject1 = cacheGame2017(gameid1, gamename1, thumbnail1, 2003); 
        cacheObject2 = cacheGame2017(gameid2, gamename2, thumbnail2, 2009); 

        Promise.all([cacheObject1, cacheObject2]).then(function(x) {
            
          var title = 'Game title';
          var description = 'How are you going description';
          var timeend = '2017-10-20T23:30:00+1000';

          var testEmail = 'mapihost@gmail.com';
          var testBgg = 'mapihost';
          var locname = 'hosthouse';
          var coords = [10, 11];
          var experiencelevel = 4;

          var hostalcohol = 1;
          var hostsmoking = 2;

          // var mapiaccount = createHostAccountObj(locname, coords[0], coords[1]);
          var mapiaccount = createLocalAccountObj(locname, coords[0], coords[1]);

          generateHostUser({}).then(function(u) {
         
            var maxplayers = 4;
            var minplayers = 2;
            var matchdate = moment('2017-10-20T20:30:00+1000');

            ajaxPromise(postData([[matches.defaultGame(gameid1, gamename1, thumbnail1), matches.defaultGame(gameid2, gamename2, thumbnail2)], title, description, matchdate.toDate(), timeend, maxplayers, minplayers, u.email, u.key, experiencelevel, 'open', 1, 2, locname, coords[0], coords[1], false])).then(function(x) {

              var status = 'open';
              var checkHttpStatus = compose(equals(200), prop('statusCode')); 
              expect(checkHttpStatus(x)).toBeTruthy;

              var readValue = (property) => path(['body', property]);

              expect(x.body.games[0].id).toBe(gameid1);
              expect(x.body.games[0].name).toBe(gamename1);
              expect(x.body.games[0].thumbnail).toBe(thumbnail1);

              expect(x.body.games[1].id).toBe(gameid2);
              expect(x.body.games[1].name).toBe(gamename2);
              expect(x.body.games[1].thumbnail).toBe(thumbnail2);

              expect(readValue('locname')(x)).toBe(locname);
              expect(readValue('seatmax')(x)).toBe(maxplayers);
              expect(readValue('seatmin')(x)).toBe(minplayers);
              expect(readValue('experience')(x)).toBe(experiencelevel);
              expect(readValue('status')(x)).toBe(status);
              expect(readValue('smoking')(x)).toBe(1);
              expect(readValue('alcohol')(x)).toBe(2);

              var validateHost = compose(prop('email'), head, path(['body', 'players']));
              expect(validateHost(x)).toBe(u.email);
              // expect(matchdate.diff(readValue('date')(x), 'minutes')).toBe(0);

              var validateCoords = compose(path(['body', 'loc', 'coordinates']));
              expect(validateCoords(x)[0]).toEqual(Number(coords[0]));
              expect(validateCoords(x)[1]).toEqual(Number(coords[1]));

              done();

            });
          });
        }).catch(log.clos('err'));
      });       


      it("api/v1/matches PATCH update match", function(done) {

          // buildKey :: s1 => s2
          const buildKey = (matchkey) => url_base + url_api + '/' + matchkey;

          // buildPatchMatchRequest :: (k -> {d}) => Promise({a})
          const buildPatchMatchRequest = (key, data) => ajaxPromise(ajaxData([buildKey(key), 'PATCH', data]));

          // createAcceptUserObj :: {u} => {a}
          const createAcceptUserObj = (user) => zipObj(['email', 'type', 'avataricon', 'name', 'playerkey'], [user.email, 'approve', '', user.profile.name, aguid(user.email)]);
          
          // pushPlayer :: (k -> {u}) => Schema(m) 
          const pushPlayer = (matchkey, user) => match.findOneAndUpdate({key: matchkey}, {$push: {players: createAcceptUserObj(user)}}, {new:true}).exec(); 

          generateMatch('open', 4, 2).then(function(x) {

              const title = 'nt';
              const description = 'nd';

              const seatmin = 4;
              const seatmax = 8;

              const requestupdate = buildPatchMatchRequest(x[1].key, {email: x[0].email, key: x[0].key, title: title, description: description, seatmin: seatmin, seatmax: seatmax, autoaccept: true}); 

              const requestupdateokdate = buildPatchMatchRequest(x[1].key, {email: x[0].email, key: x[0].key, date: moment('2018-09-10 08:30'), timeend: moment('2018-09-10 12:30')}); 

              const requestupdatebaddate = buildPatchMatchRequest(x[1].key, {email: x[0].email, key: x[0].key, date: moment('2018-09-10 12:30'), timeend: moment('2018-09-10 08:30')}); 

              const requestupdatenokey = buildPatchMatchRequest('badkey', {email: x[0].email, key: x[0].key, title: title}); 

              const requestnouserkey = buildPatchMatchRequest(x[1].key, {title: title}); 
              const requestinvalidkey = buildPatchMatchRequest(x[1].key, {email: x[0].email, key: 'badkey', title: title}); 

              Promise.all([requestupdate, requestupdatenokey, requestnouserkey, requestinvalidkey, generateUser({}), generateUser({}), requestupdateokdate, requestupdatebaddate]).then(function(y) {

                  expect(y[0].statusCode).toBe(200);
                  expect(y[0].body.status).toBe('ok');

                  expect(y[1].statusCode).toBe(401);
                  expect(y[2].statusCode).toBe(401);
                  expect(y[3].statusCode).toBe(401);

                  expect(y[6].statusCode).toBe(200);
                  expect(y[7].statusCode).toBe(400);

                  matches.FindMatchByKey(match, x[1].key).then(function(m) { 
                      expect(m[0].title).toBe(title);
                      expect(m[0].description).toBe(description);
                      expect(m[0].seatmin).toBe(seatmin);
                      expect(m[0].seatmax).toBe(seatmax);
                      expect(m[0].autoaccept).toBe(true);

                      Promise.all([pushPlayer(x[1].key, y[4]), pushPlayer(x[1].key, y[5])]).then(function(z) {
                          buildPatchMatchRequest(x[1].key, {email: x[0].email, key: x[0].key, seatmax: 3}).then(function(res) {
                             expect(res.statusCode).toBe(400);
                             buildPatchMatchRequest(x[1].key, {email: x[0].email, key: x[0].key, seatmax: 2}).then(function(res) {
                                 expect(res.statusCode).toBe(400);
                                 buildPatchMatchRequest(x[1].key, {email: y[4].email, key: y[4].key, title: title}).then(function(res) {
                                   expect(res.statusCode).toBe(401);
                                   done();
                                 });; 
                             }); 
                          });
                      });
                  });
              });
          });
      }); 

      it("whenUploadLocaliseURL", function(done) {
        expect(api_matches.whenUploadLocaliseURL('abc')).toBe('abc');
        expect(api_matches.whenUploadLocaliseURL('upload/abc')).toBe('/upload/abc');
        done();
      });

      it("taskTransformUsers", function(done) {
        Promise.all([
          generateUser({}), 
          generateUser({premium: {isactive: true}}),
          generateUser({})
        ]).then(function(gu) {

          var uHost = {
            type: 'host',
            playerkey: 'hostkey',
            name: gu[0].profile.name,
            avataricon: gu[0].profile.avataricon,
            email: gu[0].email 
          };

          var anon = {
            type: 'host',
            playerkey: 'anonkey',
            email: gu[0].email 
          };

          var premUser = {
            type: 'host',
            playerkey: 'premkey',
            name: gu[1].profile.name,
            avataricon: gu[1].profile.avataricon,
            email: gu[1].email 
          }

          var uInvite = {
            name: gu[2].profile.name,
            playerkey: 'invitekey',
            avataricon: gu[2].profile.avataricon,
            type: 'invite',
            email: gu[2].email 
          };

          R.sequence(Task.of, [
            api_matches.TaskTransformUsers(uHost.email, uHost.email, uHost), 
            api_matches.TaskTransformUsers(uHost.email, '', anon), 
            api_matches.TaskTransformUsers(uHost.email, '', premUser), 
            api_matches.TaskTransformUsers('', '', uInvite),
            api_matches.TaskTransformUsers(uHost.email, gu[2].email, uInvite)
          ]).fork(
            err => log.clos('err', err),
            data => {
              expect(data[0].name).toBe(gu[0].profile.name);
              expect(data[0].status).toBe('host');
              expect(data[0].playerkey).toBe('hostkey');
              expect(data[0].statusfaicon).toBe('fa fa-home');
              expect(data[0].thumbnail).toBe(gu[0].profile.thumbnail);
              expect(data[0].playlevel).toBe(0);
              expect(data[0].link).toBe('/users?id=' + gu[0].pageslug);
              expect(data[0].imgtype).toBe('img');
              expect(data[0].slug).toBe(gu[0].pageslug);
              expect(data[0].badges).toEqual([]);
              expect(data[0].activeuser).toBeTruthy();

              expect(data[1].name).toBe(gu[0].profile.name);
              expect(data[1].status).toBe('host');
              expect(data[1].playerkey).toBe('anonkey');
              expect(data[1].statusfaicon).toBe('fa fa-home');
              expect(data[1].thumbnail).toBe(gu[0].profile.thumbnail);
              expect(data[1].playlevel).toBe(0);
              expect(data[1].link).toBe('/users?id=' + gu[0].pageslug);
              expect(data[1].imgtype).toBe('img');
              expect(data[1].slug).toBe(gu[0].pageslug);
              expect(data[1].badges).toEqual([]);
              expect(data[1].activeuser).toBeFalsy();

              expect(data[2].badges).toEqual([ { showImg : true, imgsrc : '/img/u_roll_icon1-01.png', faicon : '', tooltip : 'Premium Player' } ]);

              expect(data[3].name).toBe(uInvite.name);
              expect(data[3].playerkey).toBe(uInvite.playerkey);
              expect(data[3].slug).toBe(gu[2].pageslug);

              expect(data[3].buttons[0]).toEqual({
                btnclass:'btn btn-warning btn-sm',
                btnfaicon:'fa fa-ban',
                btntext:'Remove',
              });

              expect(data[4].buttons[0]).toEqual({
                btnclass:'btn btn-success btn-sm',
                btnfaicon:'fa fa-check-square-o',
                btntext:'Accept',
              });

              expect(data[4].buttons[1]).toEqual({
                btnclass:'btn btn-danger btn-sm',
                btnfaicon:'fa fa-sign-out',
                btntext:'Leave',
              });

              done();

            }
          )
        });
      });


      it("api/v1/matches/:match/users GET", function(done) {

        const getMatch = R.compose(R.last, R.head);
        const getHost = R.compose(R.head, R.head);
        const getInvite = R.compose(R.nth(1));
        const getRequest = R.compose(R.nth(2));
        const getApprove = R.compose(R.nth(3));
        const getReject = R.compose(R.nth(4));
        const getUser = R.compose(R.last);

        const viewGame = (u, email) => ajaxData([apiEndpoint(url_base, url_api, getMatch(u).key, 'users'), 'GET', {email: email , key: getHost(u).key}]);

        Promise.all([genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2), generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({})]).then(function(u) {
          matches.InviteMatch(account, match, getHost(u).email, getHost(u).key, getMatch(u).key, getInvite(u).email).then(function(y) {
            ajaxPromise(viewGame(u, getHost(u).email)).then(function(r) {
              expect(r.statusCode).toBe(200);
              expect(r.body[0].name).toBe(getHost(u).profile.name);
              expect(r.body[0].status).toBe('host');
              expect(r.body[0].statusfaicon).toBe('fa fa-home');
              expect(r.body[0].thumbnail).toBe(getHost(u).profile.thumbnail);
              expect(r.body[0].playlevel).toBe(0);
              expect(r.body[0].link).toBe('/users?id=' + getHost(u).pageslug);
              expect(r.body[0].imgtype).toBe('img');
              expect(r.body[0].badges).toEqual([]);

              expect(r.body[1].name).toBe(getInvite(u).profile.name);

              ajaxPromise(viewGame(u, getInvite(u).email)).then(function(r) {
                expect(r.statusCode).toBe(200);
                expect(r.body[0].name).toBe(getHost(u).profile.name);
                expect(r.body.length).toBe(2);

                ajaxPromise(viewGame(u, getUser(u).email)).then(function(r) {
                  expect(r.statusCode).toBe(200);
                  expect(r.body[0].name).toBe(getHost(u).profile.name);
                  expect(r.body[0].status).toBe('host');
                  expect(r.body[0].statusfaicon).toBe('fa fa-home');
                  expect(r.body[0].thumbnail).toBe(getHost(u).profile.thumbnail);
                  expect(r.body[0].playlevel).toBe(0);
                  expect(r.body[0].link).toBe('/users?id=' + getHost(u).pageslug);
                  expect(r.body[0].imgtype).toBe('img');
                  expect(r.body[0].badges).toEqual([]);

                  expect(r.body.length).toBe(2);
                  done();

                });
              });
            });
          });
        });
      }); 

      it("api/v1/matches/:match/users POST PATCH add user to match (success)", function(done) {

        var status = 'open';
        var smoking = 1;
        var alcohol = 2;

        var locname = 'hosthouse';
        var lng = 11;
        var lat = 3; 

        var title = 'Game title';
        var description = 'How are you going description';
        var timeend = '2017-10-20T23:30:00+1000';
        var experiencelevel = 4;

        Promise.all([
          generateHostUser({}), 
          generateUser({}), 
          generateUser({}), 
          generateUser({}), 
          generateUser({}), 
          generateUser({}), 
          generateGame, 
          generateUser({}), 
          generateUser({}), 
          generateUser({}),
          generateUser({}),
          generateUser({})
        ]).then(function(u) {

          var maxplayers = 5;
          var minplayers = 2;
          var matchdate = moment('2017-10-20T20:30:00+1000');

          ajaxPromise(postData([wrapId(u[6].id), title, description, matchdate.toDate(), timeend, maxplayers, minplayers, u[0].email, u[0].key, experiencelevel, 'open', smoking, alcohol, locname, lng, lat, false])).then(function(x) {

            var endpoint = url_api + '/' + x.body.key + '/users';
            const bringingUser = (u, matchkey) => ajaxData([apiEndpoint(url_base, url_api, matchkey, 'users'), 'PATCH', {email: u.email, key: u.key, extraplayers: 1}]);

            var matchkey = x.body.key;

            var badrequest = ajaxData([url_base + endpoint, 'POST', {email: u[2].email, userkey: u[2].key}]);

            var addUsers = [
              createJoinPostData(url_base, endpoint, u[1].email, u[1].key),
              createInvitePostData(url_base, endpoint, u[0].email, u[0].key, u[2].email),
              badrequest, 
              createJoinPostData(url_base, endpoint, u[3].email, u[3].key),
              createJoinPostData(url_base, endpoint, u[4].email, u[4].key),
              createInvitePostData(url_base, endpoint, u[0].email, u[0].key, u[5].email),
              createInvitePostData(url_base, endpoint, u[0].email, u[0].key, u[7].email),
              createJoinPostData(url_base, endpoint, u[8].email, u[8].key),
              createInvitePostData(url_base, endpoint, u[0].email, u[0].key, u[9].email),
              createInvitePostDataByKey(url_base, endpoint, u[0].email, u[0].key, u[10].pageslug),
              createStandbyPostDataByKey(url_base, endpoint, u[0].email, u[0].key, u[11].pageslug)
            ];

            Promise.all(map(ajaxPromise, addUsers)).then(function(r) {

              var insertHttpAjaxOK = R.compose(equals(200), prop('statusCode')); 
              var insertHttpAjaxMalformed = compose(equals(400), prop('statusCode')); 

              expect(R.map(insertHttpAjaxOK, r)).toEqual([true, true, false, true, true, true, true, true, true, true, true]);

              expect(insertHttpAjaxMalformed(r[2])).toBe(true);

              // because r[0] can be 3 or 4, depending on which is processed first by mongo
              expect(r[0].body.length).toBeGreaterThan(1);

              var isScrubbed = R.compose(
                R.converge(R.or, [has('__v'), has('_id')]), 
                R.prop('body')
              );
              expect(isScrubbed(r[0])).toBe(false);

              var testRequstUser = R.compose(
                R.equals(u[1].profile.name), 
                R.prop('name'), 
                R.head, 
                R.filter(R.pipe(R.prop('type'), R.equals('request'))), 
                R.path(['body', 'match', 'players'])
              );

              var testInviteUser = R.compose(
                R.contains(u[2].profile.name), 
                R.map(R.prop('name')), 
                R.filter(R.pipe(R.prop('status'), R.equals('invite'))), 
                R.prop('body')
              );

              var testStandbyUser = R.compose(
                R.contains(u[11].profile.name), 
                R.map(R.prop('name')), 
                R.filter(R.pipe(R.prop('status'), R.equals('standby'))), 
                R.prop('body')
              );

              expect(testInviteUser(r[1])).toBe(true);
              expect(testStandbyUser(r[10])).toBe(true);

              var getJoinUser = R.compose(
                ajaxPromise, 
                createJoinPostData(url_base, endpoint)
              );

              var getUnknownJoinMatch = R.compose(
                ajaxPromise, 
                createJoinPostData(url_base, url_api + '/abcd/users')
              );

              var getDoublePlayerInvite = R.compose(
                ajaxPromise, 
                createInvitePostData(url_base, endpoint, u[0].email, u[0].key)
              );

              var getUnknownInviteMatch = R.compose(
                ajaxPromise, 
                createInvitePostData(url_base, url_api + '/abcd/users', u[0].email, u[0].key)
              );

              var getInvalidInviteUser = R.compose(
                ajaxPromise, 
                createInvitePostData(url_base, endpoint, u[0].email, 'abcdef')
              );

              var rejectUser = R.compose(
                ajaxPromise, 
                createHostRejectPostData(url_base, endpoint, u[0].email, u[0].key), findPlayerBySlug(r[3])
              );

              var approveUser = R.compose(
                ajaxPromise, 
                createApprovePostData(url_base, endpoint, u[0].email, u[0].key), findPlayerBySlug(r[4])
              );

              var deleteUser = R.compose(
                ajaxPromise, 
                createRemovePlayerByHost(url_base, endpoint, u[0].email, u[0].key), 
                findPlayerBySlug(r[5])
              );

              var declineInvite = R.compose(
                ajaxPromise, 
                createPlayerRejectPatchData(url_base, endpoint)
              );

              var leaveMatch = R.compose(
                ajaxPromise, 
                createRemovePlayerByUser(url_base, endpoint)
              );

              var acceptInvite = R.compose(
                ajaxPromise, 
                createPlayerAcceptPatchData(url_base, endpoint)
              );

              Promise.all([
								getUnknownJoinMatch(u[1].email, u[1].key), 
								getUnknownInviteMatch(u[2].email), 
								getJoinUser(u[1].email, 'abcdef'), 
								getInvalidInviteUser(u[2].email), 
								getDoublePlayerInvite(u[2].email), 
								getJoinUser(u[1].email, u[1].key), 
								rejectUser(u[3].pageslug), 
								approveUser(u[4].pageslug), 
								deleteUser(u[5].pageslug), 
								declineInvite(u[7].email, u[7].key), 
								acceptInvite(u[7].email, u[7].key), 
								leaveMatch(u[8].email, u[8].key)

							]).then(function(sb) {
                expect(prop('statusCode', sb[0])).toBe(404);
                expect(prop('statusCode', sb[1])).toBe(403);
                expect(prop('statusCode', sb[2])).toBe(401);
                expect(prop('statusCode', sb[3])).toBe(401);

                expect(prop('statusCode', sb[4])).toBe(200);
                expect(prop('statusCode', sb[5])).toBe(409);
                expect(prop('statusCode', sb[6])).toBe(200);
                expect(findPlayersTypeBySlug(sb[6], u[3].pageslug)).toBe('reject'); 

                expect(prop('statusCode', sb[7])).toBe(200);
                expect(findPlayersTypeBySlug(sb[7], u[4].pageslug)).toBe('approve'); 

                var hasPlayerskey = curry((req, playerkey) => compose(
                  equals(1), 
                  length, 
                  filter(pipe(prop('playerkey'), equals(playerkey))), 
                  prop('body'))(req)
                );

                expect(prop('statusCode', sb[8])).toBe(200);
                expect(hasPlayerskey(sb[8], x)).toBe(false);

                expect(prop('statusCode', sb[9])).toBe(200);
                expect(findPlayersTypeBySlug(sb[9], u[7].pageslug)).toBe('decline'); 

                expect(prop('statusCode', sb[11])).toBe(200);
                expect(hasPlayerskey(sb[11], x)).toBe(false);

                acceptInvite(u[9].email, u[9].key).then(function(x) {

                  expect(prop('statusCode', x)).toBe(200);
                  expect(findPlayersTypeBySlug(x, u[9].pageslug)).toBe('approve'); 

                  ajaxPromise(bringingUser(u[9], matchkey)).then(function(p) {

                    expect(R.find(R.propEq('slug', u[9].pageslug), p.body).extraplayers).toBe(1);
                    expect(prop('statusCode', p)).toBe(200);
                    done();

                  });
                });
              });
            });
          });
        });
      }); 

      // add game
      it('api/v1/matches/:match/games GET', function(done) {

        const getMatch = R.compose(R.last, R.head);
        const getHost = R.compose(R.head, R.head);
        const getUser = R.compose(R.last);

        const viewGameHost = (u) => ajaxData([apiEndpoint(url_base, url_api, getMatch(u).key, 'games'), 'GET', {email: getHost(u).email, key: getHost(u).key}]);

        const viewGameUser = (u) => ajaxData([apiEndpoint(url_base, url_api, getMatch(u).key, 'games'), 'GET', {email: getUser(u).email, key: getUser(u).key}]);

        Promise.all([genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2), generateUser({})]).then(function(u) {
          ajaxPromise(viewGameHost(u)).then(function(r) {
            expect(r.statusCode).toBe(200);
            expect(R.last(r.body).id).toBe(getMatch(u).games[0].id);
            expect(R.last(r.body).name).toBe(getMatch(u).games[0].name);
            expect(R.last(r.body).thumbnail).toBe(getMatch(u).games[0].thumbnail);
            ajaxPromise(viewGameUser(u)).then(function(r) {
              expect(r.statusCode).toBe(200);
              expect(R.last(r.body).id).toBe(getMatch(u).games[0].id);
              expect(R.last(r.body).name).toBe(getMatch(u).games[0].name);
              expect(R.last(r.body).thumbnail).toBe(getMatch(u).games[0].thumbnail);
              done();
            })
          });
        });
      });


    // add game
    it('api/v1/matches/:match/games POST', function(done) {

      const getMatch = R.compose(R.last, R.head);
      const getHost = R.compose(R.head, R.head);

      const getGame = R.compose(R.nth(1));
      const getNextGame = R.compose(R.nth(4));
      const getMatchUser = R.compose(R.nth(2));
      const getUser = R.compose(R.nth(3));

      const badAddGameHost = (u) => ajaxData([
        apiEndpoint(url_base, url_api, getMatch(u).key, 'games'), 
        'POST', {
          email: getHost(u).email, 
          key: getHost(u).key
        }]);

      const bringGame = (newuser, matchkey, gameslug) => ajaxData([
        apiEndpoint(url_base, url_api, matchkey, 'games/' + gameslug),
        'PATCH', {
          email:   newuser.email, 
          key:     newuser.key, 
          status: 'select' 
        }]);

      Promise.all([genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2), generateGame, generateUser({}), generateUser({}), generateGame ]).then(function(u) {

        ajaxPromise(addUserGame(getHost(u).email, getHost(u).key, 'select', getMatch(u).key, getGame(u).id, getGame(u).name, getGame(u).thumbnail)).then(function(r) {

          expect(r.statusCode).toBe(200);
          expect(R.last(r.body).id).toBe(getGame(u).id);
          expect(R.last(r.body).name).toBe(getGame(u).name);
          expect(R.last(r.body).thumbnail).toBe(getGame(u).thumbnail);

          matches.JoinMatch(account, match, getMatch(u).key, getMatchUser(u).email, getMatchUser(u).key).then(function(m) {

            matches.ApproveUser(account, match, getMatch(u).key, getHost(u).email, getHost(u).key, m.players[1].playerkey).then(function(m2) {
              ajaxPromise(addUserGame(getMatchUser(u).email, getMatchUser(u).key, 'autosuggest', getMatch(u).key, getNextGame(u).id, getNextGame(u).name, getNextGame(u).thumbnail)).then(function(r) {

                expect(r.statusCode).toBe(200);
                expect(R.last(r.body).id).toBe(getNextGame(u).id);
                expect(R.last(r.body).name).toBe(getNextGame(u).name);
                expect(R.last(r.body).thumbnail).toBe(getNextGame(u).thumbnail);
                expect(R.last(r.body).status).toBe('autosuggest');

                ajaxPromise(bringGame(getUser(u), getMatch(u).key, R.last(r.body).pageslug)).then(function(r) {
                  expect(R.last(r.body).status).toBe('select');
                  expect(R.last(r.body).username).toBe(getUser(u).profile.name);

                  ajaxPromise(addUserGame(getUser(u).email, getUser(u).key, 'select', getMatch(u).key, getNextGame(u).id), getNextGame(u).name, getNextGame(u).thumbnail).then(function(r) {
                    expect(r.statusCode).toBe(403);

                    ajaxPromise(badAddGameHost(u)).then(function(r) {
                      expect(r.statusCode).toBe(500);
                      done();

                    })
                  })
                })
              })
            })
          });
        });
      });
    });


    // add expansion 
    it('api/v1/matches/:match/games/:gameslug/expansions', function(done) {

      const getMatch = R.compose(R.last, R.head);
      const getHost = R.compose(R.head, R.head);

      const getGame = R.compose(R.nth(1));
      const getNextGame = R.compose(R.nth(4));
      const getMatchUser = R.compose(R.nth(2));
      const getUser = R.compose(R.nth(3));

      /*
      const addUserGame = (email, userkey, matchkey, gameid, gamename, thumbnail) => ajaxData([
        apiEndpoint(url_base, url_api, matchkey, 'games'), 
        'POST', {
          email:      email, 
          key:        userkey, 
          gameid:     gameid,
          name:       gamename,
          thumbnail:  thumbnail 
        }]);
      */

      // getGameExpansions :: s -> s -> s -> s -> s -> s -> {o}
      const getGameExpansions = (matchkey, gameslug) => ajaxData([
        apiEndpoint(url_base, url_api, matchkey, 'games/' + gameslug + '/expansions/'), 
        'GET', 
        {}
      ]);

      // addGameExpansions :: s -> s -> s -> s -> n -> n -> b -> {o}
      const addGameExpansions = (email, userkey, userslug, matchkey, gameslug, expansiondata) => ajaxData([
        apiEndpoint(url_base, url_api, matchkey, 'games/' + gameslug + '/expansions/'), 
        'POST', 
        {
          email:  email, 
          key:    userkey, 
          userslug: userslug, 
          expansiondata: expansiondata
        }
      ]);

      // removeExpansionRequest :: s -> s -> s -> n -> n -> b -> {o}
      const removeExpansionRequest = (email, userkey, matchkey, gameslug, expansionid) => ajaxData([
        apiEndpoint(url_base, url_api, matchkey, 'games/' + gameslug + '/expansions/' + expansionid), 
        'DELETE', 
        {
          email:      email, 
          key:        userkey, 
        }
      ]);

      Promise.all([genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2), generateGame, generateUser({}), generateUser({}), generateGame ]).then(function(u) {
        ajaxPromise(addUserGame(getHost(u).email, getHost(u).key, 'select', getMatch(u).key, getGame(u).id, getGame(u).name, getGame(u).thumbnail, [getNextGame(u)])).then(function(g) {
          ajaxPromise(getGameExpansions(getMatch(u).key, g.body[0].pageslug)).then(function(getexpansionBefore) {
            expect(getexpansionBefore.body.length).toBe(0);
            ajaxPromise(addGameExpansions(getHost(u).email, getHost(u).key, getHost(u).pageslug, getMatch(u).key, g.body[0].pageslug, {
              id:           getNextGame(u).id,
              name:         getNextGame(u).name, 
              thumbnail:    getNextGame(u).thumbnail, 
              status:       getNextGame(u).status, 
              minplaytime:  getNextGame(u).minplaytime, 
              maxplaytime:  getNextGame(u).maxplaytime, 
              minplayers:   getNextGame(u).minplayers, 
              maxplayers:   getNextGame(u).maxplayers, 
              weight:       getNextGame(u).weight, 
              bggrank:      getNextGame(u).bggrank, 
              avgrating:    getNextGame(u).avgrating, 
              // playercounts: getNextGame(u).playercounts 
            })).then(function(postExpansion) {

              expect(postExpansion.body.length).toBe(1);
              expect(postExpansion.body[0].id).toBe(getNextGame(u).id);
              expect(postExpansion.body[0].name).toBe(getNextGame(u).name);
              expect(postExpansion.body[0].thumbnail).toBe(getNextGame(u).thumbnail);
              expect(postExpansion.body[0].userpageslug).toBe(getHost(u).pageslug);

              ajaxPromise(getGameExpansions(getMatch(u).key, g.body[0].pageslug)).then(function(getexpansionBefore) {

                expect(getexpansionBefore.body.length).toBe(1);
                expect(getexpansionBefore.body[0].id).toBe(getNextGame(u).id);
                expect(getexpansionBefore.body[0].name).toBe(getNextGame(u).name);
                expect(getexpansionBefore.body[0].thumbnail).toBe(getNextGame(u).thumbnail);
                expect(getexpansionBefore.body[0].userpageslug).toBe(getHost(u).pageslug);

                ajaxPromise(removeExpansionRequest(getHost(u).email, getHost(u).key, getMatch(u).key, g.body[0].pageslug, getNextGame(u).id)).then(function(deleteExpansion) {
                  expect(deleteExpansion.body.length).toBe(0);

                  ajaxPromise(getGameExpansions(getMatch(u).key, g.body[0].pageslug)).then(function(getexpansionAfter) {

                    expect(getexpansionAfter.body.length).toBe(0);
                    done();

                  });
                });
              });
            });
          });
        });
      });
    });


    // delete game
    it('api/v1/matches/:match/games DELETE', function(done) {

      const getMatch = R.compose(R.last, R.head);
      const getHost = R.compose(R.head, R.head);
      const getUser = R.compose(R.last);
      const getGameId = R.compose(R.prop('id'), R.nth(1));
      const getGameName = R.compose(R.prop('name'), R.nth(1));
      const getGameThumb = R.compose(R.prop('thumbnail'), R.nth(1));
      const getGame1 = R.compose(R.head, prop('games'));
      const getGame2 = R.compose(R.last, prop('games'));

      const deleteGameHost = (u, gameslug) => ajaxData([apiEndpoint(url_base, url_api, getMatch(u).key, 'games/' + gameslug), 'DELETE', {email: getHost(u).email, key: getHost(u).key}]);

      const badDeleteGameHost = (u, gameslug) => ajaxData([apiEndpoint(url_base, url_api, getMatch(u).key, 'games/' + "z" + gameslug), 'DELETE', {email: getHost(u).email, key: getHost(u).key}]);

      const deleteGameUser = (u, gameslug) => ajaxData([apiEndpoint(url_base, url_api, getMatch(u).key, 'games/' + gameslug), 'DELETE', {email: getUser(u).email, key: getUser(u).key}]);

      Promise.all([genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2), generateGame, generateUser({})]).then(function(u) {

        matches.TaskAddGameToMatch(account, match, getMatch(u).key, 'select', getGameId(u), getGameName(u), getGameThumb(u), getHost(u).email).fork(
          err => log.clos('err', err),
          data => {
            ajaxPromise(deleteGameHost(u, getGame1(data).pageslug)).then(function(r) {
              expect(r.statusCode).toBe(200);
              expect(r.body.length).toBe(1);
              expect(r.body[0].id).toBe(getGame2(data).id);
              expect(r.body[0].name).toBe(getGame2(data).name);
              expect(r.body[0].bgglink).toBe("http://www.boardgamegeek.com/boardgame/" + getGame2(data).id);

              matches.JoinMatch(account, match, getMatch(u).key, getUser(u).email, getUser(u).key).then(function(y) {
                matches.ApproveUser(account, match, getMatch(u).key, getHost(u).email, getHost(u).key, y.players[1].playerkey).then(function(z) {
                  matches.TaskAddGameToMatch(account, match, getMatch(u).key, 'select', getGameId(u), getGameName(u), getGameThumb(u), getUser(u).email).fork(
                    err => log.clos('err', err),
                    data => {
                      expect(data.games.length).toBe(2);
                      ajaxPromise(deleteGameUser(u, getGame1(data).pageslug)).then(function(r) {
                        expect(r.statusCode).toBe(200);
                        expect(r.body.length).toBe(1);
                        ajaxPromise(deleteGameUser(u, getGame2(data).pageslug)).then(function(r) {
                          expect(r.statusCode).toBe(200);
                          expect(r.body.length).toBe(0);
                          ajaxPromise(badDeleteGameHost(u, getGame1(data).pageslug)).then(function(r) {
                            expect(r.statusCode).toBe(500);
                            done();
                          })
                        })
                      });
                    }
                  )
                })
              })
            });
          }
        )
      });
    });


    it("match votes POST", function(done) {
      genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2)
        .then((x) => {

          const ajaxVote = curry((matchkey, gameid, data) => ajaxPromise({
            url: url_base + url_api + '/' + matchkey + '/games/' + gameid + '/votes',
            method: 'POST',
            data: data 
          }));

          const ajaxVoteMatch = ajaxVote(
            x[1].key, 
            x[1].games[0].id
          );

          Promise.all([
            ajaxVoteMatch({}),
            ajaxVoteMatch({email: x[0].email}),
            ajaxVoteMatch({email: x[0].email, key: x[0].key}),
            ajaxVoteMatch({email: x[0].email, key: 'badkey'})
          ]).then((z) => {
            expect(z[0].statusCode).toBe(500);
            expect(z[1].statusCode).toBe(500);
            expect(z[2].statusCode).toBe(200);
            expect(z[2].body.games.length).toBe(1);
            expect(z[2].body.games[0].vote[0].email).toBe(x[0].email);    // TODO email shouldn't appear when players vote
            expect(z[2].body.games[0].vote[0].userkey).toBe(x[0].pageslug);
            expect(z[2].body.games[0].vote[0].username).toBe(x[0].profile.name);
            expect(z[2].body.games[0].vote[0].avataricon).toBe(x[0].profile.avataricon);
            expect(z[2].body.games[0].vote[0].direction).toBe(1);
            expect(z[2].body.games[0].vote[0].votetype).toBe('wtpplay');
            expect(z[3].statusCode).toBe(401);
            ajaxVoteMatch({email: x[0].email, key: x[0].key}).then((y) => {
              expect(y.statusCode).toBe(409);
              done();

            });
          });
        });
    });


    it("match votes DELETE", function(done) {
      genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2)
        .then((x) => {

          const ajaxVote = curry((method, matchkey, gameid, data) => ajaxPromise({
            url: url_base + url_api + '/' + matchkey + '/games/' + gameid + '/votes',
            method: method,
            data: data 
          }));

          const ajaxVoteMatch = ajaxVote(
            'POST',
            x[1].key, 
            x[1].games[0].id
          );

          const ajaxUnvoteMatch = ajaxVote(
            'DELETE',
            x[1].key, 
            x[1].games[0].id
          );

          Promise.all([
            ajaxVoteMatch({}),
            ajaxVoteMatch({email: x[0].email}),
            ajaxVoteMatch({email: x[0].email, key: x[0].key}),
            ajaxVoteMatch({email: x[0].email, key: 'badkey'})
          ]).then((z) => {
            expect(z[0].statusCode).toBe(500);
            expect(z[1].statusCode).toBe(500);
            expect(z[2].statusCode).toBe(200);
            expect(z[2].body.games.length).toBe(1);
            expect(z[2].body.games[0].vote[0].email).toBe(x[0].email);
            expect(z[2].body.games[0].vote[0].direction).toBe(1);
            expect(z[2].body.games[0].vote[0].votetype).toBe('wtpplay');
            expect(z[3].statusCode).toBe(401);
            ajaxVoteMatch({email: x[0].email, key: x[0].key}).then((y) => {

              expect(y.statusCode).toBe(409);

              Promise.all([
                ajaxUnvoteMatch({}),
                ajaxUnvoteMatch({email: x[0].email}),
                ajaxUnvoteMatch({email: x[0].email, key: x[0].key}),
                ajaxUnvoteMatch({email: x[0].email, key: 'badkey'})
              ]).then((a) => {
                expect(a[0].statusCode).toBe(500);
                expect(a[1].statusCode).toBe(500);
                expect(a[2].statusCode).toBe(200);
                expect(a[2].body.games[0].vote.length).toBe(0);
                expect(a[3].statusCode).toBe(401);
                done();
              });
            });
          });
        });
    });


    it("match joins POST", function(done) {
      genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2)
        .then((x) => {

          // ajaxJoin :: s -> s -> a -> promise a
          const ajaxJoin = R.curry((matchkey, gamekey, data) => ajaxPromise({
            url: url_base + url_api + '/' + matchkey + '/games/' + gamekey + '/joins',
            method: 'POST',
            data: data 
          }));

          const ajaxJoinMatch = ajaxJoin(
            x[1].key, 
            x[1].games[0].gamekey
          );

          Promise.all([
            ajaxJoinMatch({}),
            ajaxJoinMatch({email: x[0].email}),
            ajaxJoinMatch({email: x[0].email, key: x[0].key}),
            ajaxJoinMatch({email: x[0].email, key: 'badkey'})
          ]).then((z) => {
            expect(z[0].statusCode).toBe(500);
            expect(z[1].statusCode).toBe(500);
            expect(z[2].statusCode).toBe(200);
            expect(z[2].body.games.length).toBe(1);
            expect(z[2].body.games[0].join[0].email).toBe(x[0].email);    // TODO email shouldn't appear when players vote
            expect(z[2].body.games[0].join[0].userkey).toBe(x[0].pageslug);
            expect(z[2].body.games[0].join[0].username).toBe(x[0].profile.name);
            expect(z[2].body.games[0].join[0].avataricon).toBe(x[0].profile.avataricon);
            expect(z[2].body.games[0].join[0].direction).toBe(1);
            expect(z[2].body.games[0].join[0].jointype).toBe('wtpplay');
            expect(z[3].statusCode).toBe(401);
            ajaxJoinMatch({email: x[0].email, key: x[0].key}).then((y) => {
              expect(y.statusCode).toBe(409);
              done();

            });
          });
        });
    });


    it("match joins DELETE", function(done) {
      genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'open', 2, 2)
        .then((x) => {

          const ajaxJoin = R.curry((method, matchkey, gamekey, data) => ajaxPromise({
            url: url_base + url_api + '/' + matchkey + '/games/' + gamekey + '/joins',
            method: method,
            data: data 
          }));

          const ajaxJoinMatch = ajaxJoin(
            'POST',
            x[1].key, 
            x[1].games[0].gamekey
          );

          const ajaxUnvoteMatch = ajaxJoin(
            'DELETE',
            x[1].key,
            x[1].games[0].gamekey
          );

          Promise.all([
            ajaxJoinMatch({}),
            ajaxJoinMatch({email: x[0].email}),
            ajaxJoinMatch({email: x[0].email, key: x[0].key}),
            ajaxJoinMatch({email: x[0].email, key: 'badkey'})
          ]).then((z) => {
            expect(z[0].statusCode).toBe(500);
            expect(z[1].statusCode).toBe(500);
            expect(z[2].statusCode).toBe(200);
            expect(z[2].body.games.length).toBe(1);
            expect(z[2].body.games[0].join[0].email).toBe(x[0].email);
            expect(z[2].body.games[0].join[0].direction).toBe(1);
            expect(z[2].body.games[0].join[0].jointype).toBe('wtpplay');
            expect(z[3].statusCode).toBe(401);
            ajaxJoinMatch({email: x[0].email, key: x[0].key}).then((y) => {

              expect(y.statusCode).toBe(409);

              Promise.all([
                ajaxUnvoteMatch({}),
                ajaxUnvoteMatch({email: x[0].email}),
                ajaxUnvoteMatch({email: x[0].email, key: x[0].key}),
                ajaxUnvoteMatch({email: x[0].email, key: 'badkey'})
              ]).then((a) => {
                expect(a[0].statusCode).toBe(500);
                expect(a[1].statusCode).toBe(500);
                expect(a[2].statusCode).toBe(200);
                expect(a[2].body.games[0].join.length).toBe(0);
                expect(a[3].statusCode).toBe(401);
                done();
              });
            });
          });
        });
    });



    it("match PATCH publish", function(done) {

      const ajaxPublishPrivate = R.curry((method, matchkey, email, key, isprivate, start, end) => ajaxPromise({
        url: url_base + url_api + '/' + matchkey + '/publish',
        method: method,
        data: {email: email, key: key, isprivate: isprivate, date: start, timeend: end} 
      }));

      const ajaxPublish = R.curry((method, matchkey, email, key, isprivate) => ajaxPromise({
        url: url_base + url_api + '/' + matchkey + '/publish',
        method: method,
        data: {email: email, key: key, isprivate: isprivate} 
      }));

      const startdate = moment().add(5, 'days').toDate();
      const enddate   = moment().add(5, 'days').add(3, 'hours').toDate();

      Promise.all([
        genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'draft', 2, 2),
        genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'draft', 2, 2)
      ])
        .then((fakeM) => {
          Promise.all([
            ajaxPublishPrivate('PATCH', fakeM[0][1].key, fakeM[0][0].email, fakeM[0][0].key, false, startdate, enddate),
            ajaxPublish('PATCH', fakeM[1][1].key, fakeM[1][0].email, fakeM[1][0].key, true),
          ]).then(function(aj) {
            expect(aj[0].body.status).toBe('open');
            expect(aj[0].body.isprivate).toBe(false);

            expect(new Date(aj[0].body.date).getUTCDate()).toBe(startdate.getUTCDate());
            expect(new Date(aj[0].body.timeend).getUTCDate()).toBe(enddate.getUTCDate());

            expect(aj[1].body.status).toBe('open');
            expect(aj[1].body.isprivate).toBe(true);
            done();
          });

        });
    });

    it("match POST sendhosttemplate", function(done) {

      const ajaxSendTemplate = R.curry((matchkey, email, key) => ajaxPromise({
        url: url_base + url_api + '/' + matchkey + '/sendhosttemplate',
        method: "POST",
        data: {email: email, key: key} 
      }));

      Promise.all([
        genhelper.GenerateFakeHostedMatch(account, testBoardgame, match, 'draft', 2, 2)
      ])
        .then((fakeM) => {
          Promise.all([
            ajaxSendTemplate(fakeM[0][1].key, fakeM[0][0].email, fakeM[0][0].key),
            ajaxSendTemplate('badmatch', fakeM[0][0].email, fakeM[0][0].key),
            ajaxSendTemplate(fakeM[0][1].key, 'not the email', fakeM[0][0].key),
            ajaxSendTemplate(fakeM[0][1].key, fakeM[0][0].email, 'badkey')
          ]).then(function(aj) {
            expect(aj[0].statusCode).toBe(200);
            expect(aj[0].body.status).toBe('ok');
            expect(aj[1].statusCode).toBe(401);
            expect(aj[2].statusCode).toBe(500);
            expect(aj[3].statusCode).toBe(500);
            done();

          });
        });

    });


    it("FindUserKey", function(done) {

      const initObj       = (key, value) => zipObj([key], [value]);
      const bodyObj       = (key, value) => initObj('body', initObj(key, value));
      const bodyUserObj   = (bodyObj, userObj) => zipObj(['body', 'user'], [bodyObj, userObj]);

      expect(api_matches.FindUserKey('userkey', bodyObj('userkey', 'req-key'))).toBe('req-key');
      expect(api_matches.FindUserKey('userkey', bodyUserObj(initObj('userkey', 'req-key'), initObj('key', 'user-key')))).toBe('req-key');
      expect(api_matches.FindUserKey('userkey', bodyUserObj({}, initObj('key', 'user-key')))).toBe('user-key');
      expect(api_matches.FindUserKey('hostkey', bodyUserObj({}, initObj('key', 'user-key')))).toBe('user-key');
      expect(api_matches.FindUserKey('hostkey', bodyObj('userkey', 'req-key'))).toBe(undefined);
      expect(api_matches.FindUserKey('hostkey', bodyObj('hostkey', 'host-key'))).toBe('host-key');
      expect(api_matches.FindUserKey('userkey', bodyUserObj({}, {}))).toBe(undefined);

      done();

    });


    it('maybeMatch', function(done) {
      expect(api_matches.MaybeMatch({}).isNothing).toBe(true); 
      expect(api_matches.MaybeMatch({params:{}}).isNothing).toBe(true); 
      expect(api_matches.MaybeMatch({params:{match:'abc'}}).get()).toBe('abc'); 
      done();
    });

    it('ReadUserType', function(done) {

      var fakeMatch = {
        players: [
          {email: 'abc@def.com', playerkey: 'keyA', type: 'typeA'},
          {email: 'abc@notdef.com', playerkey: 'keyB', type: 'typeB'},
        ]
      }

      expect(api_matches.ReadUserType('query', fakeMatch, {query: {user: 'abc@def.com'}})).toBe('typeA')
      expect(api_matches.ReadUserType('body', fakeMatch, {body: {user: 'abc@notdef.com'}})).toBe('typeB')
      done();
    });

    it("ValidUser", function(done) {

      var fakeMatch = {
        players: [
          {email: 'abc@def.com', playerkey: 'keyA', type: 'host'},
          {email: 'abc@defa.com', playerkey: 'keyB', type: 'approve'},
          {email: 'abc@defb.com', playerkey: 'keyC', type: 'invite'},
          {email: 'abc@defc.com', playerkey: 'keyD', type: 'request'},
        ]
      };
      expect(api_matches.ValidUser('body', fakeMatch, {body: {user: 'abc@def.com'}})).toBe(true); 
      expect(api_matches.ValidUser('query', fakeMatch, {query: {user: 'abc@def.com'}})).toBe(true); 
      expect(api_matches.ValidUser('body', fakeMatch, {body: {user: 'abc@defa.com'}})).toBe(true); 
      expect(api_matches.ValidUser('body', fakeMatch, {body: {user: 'abc@defb.com'}})).toBe(false); 
      expect(api_matches.ValidUser('body', fakeMatch, {body: {user: 'abc@defc.com'}})).toBe(false); 
      expect(api_matches.ValidUser('body', fakeMatch, {body: {user: 'abc@defd.com'}})).toBe(false); 
      done();
    });

		it("playerTypeObj", function(done) {
			expect(api_matches.playerTypeObj('host')).toEqual({ class : 'label label-primary', text : 'Hosting' });
			expect(api_matches.playerTypeObj('interested')).toEqual({ class : 'label label-info', text : 'Interested' });
			done();
		});

    it("ensurePlayerKeys", function(done) {
      expect(api_matches.ensurePlayerKeys({})).toEqual({ name : 'Player name not set', avataricon : '/img/default-avatar-sq.jpg', imgclass : 'img-circle', type: 'Unknown' });
      expect(api_matches.ensurePlayerKeys({name: 'abc'})).toEqual({ name : 'abc', avataricon : '/img/default-avatar-sq.jpg', imgclass : 'img-circle', type: 'Unknown' });
      expect(api_matches.ensurePlayerKeys({name: 'abc', avataricon: 'ai'})).toEqual({ name : 'abc', avataricon : 'ai', imgclass : 'img-circle', type: 'Unknown' });
      expect(api_matches.ensurePlayerKeys({name: 'abc', avataricon: 'ai', type:'tp'})).toEqual({ name : 'abc', avataricon : 'ai', imgclass : 'img-circle', type: 'tp' });
      done();
    });

    it("isPlayerAttending", function(done) {
      expect(api_matches.isPlayerAttending({})).toBe(false);
      expect(api_matches.isPlayerAttending({type: 'abc'})).toBe(false);
      expect(api_matches.isPlayerAttending({type: 'approve'})).toBe(true);
      expect(api_matches.isPlayerAttending({type: 'host'})).toBe(true);
      done();
    });

    it("headEventTitle", function(done) {
      expect(api_matches.headEventTitle([]).isJust).toBe(false);
      expect(api_matches.headEventTitle([{a:'hi'}]).isJust).toBe(false);
      expect(api_matches.headEventTitle([{a:'hi', title:'heads'}]).get()).toBe('heads');
      done();
    });


    it("notifyMatchUsers", function(done) {

      Promise.all([
        generateUser({}), 
        generateUser({}), 
        generateUser({}), 
        generateUser({})
      ]).then(function(y) {

        var matchA = [
          {
            extraplayers: 0,
            email: y[0].email,
            type: 'host',
            avataricon: 'pic1.jpg',
            name: y[0].profile.name,
            playerkey: '3562fd72-bc10-4288-8fde-ab1abe4e7616'
          },
          {
            extraplayers: 0,
            email: y[1].email,
            type: 'invite',
            avataricon: 'pic2.jpg',
            name: y[1].profile.name,
            playerkey: '3562fd72-bc10-4288-8fde-ab1abe4e7617'
          },
          {
            extraplayers: 0,
            email: y[2].email,
            type: 'approve',
            avataricon: 'pic3.jpg',
            name: y[2].profile.name,
            playerkey: '3562fd72-bc10-4288-8fde-ab1abe4e7618'
          },
          {
            extraplayers: 0,
            email: y[3].email,
            type: 'standby',
            avataricon: 'pic4.jpg',
            name: y[3].profile.name,
            playerkey: '3562fd72-bc10-4288-8fde-ab1abe4e7619'
          }
        ];

        // buildMessage :: s -> s 
        const buildMessage = R.curry((a, b) => a + " and " + b); 

        R.sequence(Task.of, [
          api_matches.notifyMatchUsers('txt', Maybe.of('ai'), Maybe.of('123'), 'typex', matchA)
        ]).fork(()=>{}, (val)=>{

          expect(val[0][0][0].status).toEqual('new');
          expect(val[0][0][0].text).toEqual('txt');
          expect(val[0][0][0].thumbnail).toEqual('ai');
          expect(val[0][0][0].url).toEqual('/events/123');
          expect(val[0][0][0].noticetype).toEqual('typex');
          expect(val[0][0][0].openNewTab).toEqual(false);

          expect(val[0][1][0].status).toEqual('new');
          expect(val[0][1][0].text).toEqual('txt');
          expect(val[0][1][0].thumbnail).toEqual('ai');
          expect(val[0][1][0].url).toEqual('/events/123');
          expect(val[0][1][0].noticetype).toEqual('typex');
          expect(val[0][1][0].openNewTab).toEqual(false);

          expect(val[0][2][0].status).toEqual('new');
          expect(val[0][2][0].text).toEqual('txt');
          expect(val[0][2][0].thumbnail).toEqual('ai');
          expect(val[0][2][0].url).toEqual('/events/123');
          expect(val[0][2][0].noticetype).toEqual('typex');
          expect(val[0][2][0].openNewTab).toEqual(false);

          expect(val[0].length).toBe(3);

          done();

        });

      });
    });

    it("filterPlayer", function(done) {
      expect(api_matches.filterPlayer("e", {})).toBe(false);
      expect(api_matches.filterPlayer("e", {email:"c"})).toBe(false);
      expect(api_matches.filterPlayer("e", {email:"e"})).toBe(true);
      expect(api_matches.filterPlayer("e", {type:"something"})).toBe(false);
      expect(api_matches.filterPlayer("e", {type:"host"})).toBe(true);
      expect(api_matches.filterPlayer("e", {type:"approve"})).toBe(true);
      expect(api_matches.filterPlayer("e", {type:"invite"})).toBe(true);
      expect(api_matches.filterPlayer("e", {type:"request"})).toBe(true);
      done();
    });

    it("filterPublicPlayers", function(done) {
      expect(api_matches.filterPublicPlayers("e", [{}])).toEqual([]);
      expect(api_matches.filterPublicPlayers("e", [{email:"c"}])).toEqual([]);
      expect(api_matches.filterPublicPlayers("e", [{email:"e"}])).toEqual([{email:"e"}]);
      expect(api_matches.filterPublicPlayers("e", [{type:"something"}])).toEqual([]);
      expect(api_matches.filterPublicPlayers("e", [{type:"host"}])).toEqual([{type:"host"}]);
      expect(api_matches.filterPublicPlayers("e", [{type:"approve"}])).toEqual([{type:"approve"}]);
      expect(api_matches.filterPublicPlayers("e", [{type:"invite"}])).toEqual([{type:"invite"}]);
      expect(api_matches.filterPublicPlayers("e", [{type:"request"}])).toEqual([{type:"request"}]);
      done();
    });

    it("isPlayerTypeHostInPlayers", function(done) {
      expect(api_matches.isPlayerTypeHostInPlayers("e", [{email:"c", type:"host"}, {email:"e", type:"invite"}]).isJust).toBe(false);
      expect(api_matches.isPlayerTypeHostInPlayers("c", [{email:"c", type:"host"}, {email:"e", type:"invite"}]).isJust).toBe(true);
      done();
    });

    it("safeBodyObj", function(done) {
      expect(api_matches.safeBodyObj({body:{'abc':3}}, 'abc').get()).toBe(3);
      expect(api_matches.safeBodyObj({body:{'abc':3}}, 'def').isJust).toBe(false);
      expect(api_matches.safeBodyObj({body:{'def':4}}, 'def').get()).toBe(4);
      done();
    });

    it("isLocationSearch", function(done) {
      expect(api_matches.isLocationSearch({})).toBeFalsy();

      expect(api_matches.isLocationSearch({longitude:'a'})).toBeFalsy();
      expect(api_matches.isLocationSearch({latitude:'a'})).toBeFalsy();
      expect(api_matches.isLocationSearch({distance:'a'})).toBeFalsy();

      expect(api_matches.isLocationSearch({longitude:'a', latitude:'b'})).toBeFalsy();
      expect(api_matches.isLocationSearch({latitude:'a', distance:'c'})).toBeFalsy();
      expect(api_matches.isLocationSearch({distance:'a', longitude:'x'})).toBeFalsy();

      expect(api_matches.isLocationSearch({distance:'a', latitude:'a', longitude:'x'})).toBeTruthy();

      done();
    });

    it("isNearbySearch", function(done) {
      expect(api_matches.isNearbySearch({})).toBeFalsy();

      expect(api_matches.isNearbySearch({nearby:'a'})).toBeFalsy();
      expect(api_matches.isNearbySearch({distance:'a'})).toBeFalsy();

      expect(api_matches.isNearbySearch({nearby:'y', distance:'a', latitude:'a', longitude:'x'})).toBeTruthy();

      done();
    });

    it("isTypeSearch", function(done) {
      expect(api_matches.isTypeSearch({})).toBeFalsy();
      expect(api_matches.isTypeSearch({types:'a'})).toBeFalsy();
      expect(api_matches.isTypeSearch({user:'a'})).toBeFalsy();

      expect(api_matches.isTypeSearch({types:'y', user:'a'})).toBeTruthy();

      done();
    });

    it("isHistorySearch", function(done) {
      expect(api_matches.isHistorySearch({})).toBeFalsy();

      expect(api_matches.isHistorySearch({history:'a'})).toBeFalsy();
      expect(api_matches.isHistorySearch({types:'a'})).toBeFalsy();
      expect(api_matches.isHistorySearch({user:'a'})).toBeFalsy();

      expect(api_matches.isHistorySearch({history:'a', user:'b'})).toBeFalsy();
      expect(api_matches.isHistorySearch({types:'a', history:'c'})).toBeFalsy();
      expect(api_matches.isHistorySearch({user:'a', types:'x'})).toBeFalsy();

      expect(api_matches.isHistorySearch({history:'a', types:'a', user:'x'})).toBeTruthy();

      done();
    });

    it("isEventSearch", function(done) {
      expect(api_matches.isEventSearch({})).toBeFalsy();
      expect(api_matches.isEventSearch({event: 'a'})).toBeTruthy();
      done();
    });

    it("isExperienceSearch", function(done) {
      expect(api_matches.isExperienceSearch({})).toBeFalsy();
      expect(api_matches.isExperienceSearch({dog: 3})).toBeFalsy();
      expect(api_matches.isExperienceSearch({experience: 3})).toBeTruthy();
      expect(api_matches.isExperienceSearch({experience: []})).toBeFalsy();
      expect(api_matches.isExperienceSearch({experience: [4]})).toBeTruthy();
      expect(api_matches.isExperienceSearch({experience: [4,5]})).toBeTruthy();
      done();
    });


    it("close server", function(done) {
      server.close(function() {
        mongo.CloseDatabase();
        done();
      });
    });

  });

});
