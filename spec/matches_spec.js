var Promise = require('bluebird');
var R = require('ramda');

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task');
const { Success, Failure, collect } = require('folktale/validation');

var converge = R.converge,
    compose = R.compose,
    composeP = R.composeP,
    curry   = R.curry,
    equals  = R.equals,
    filter  = R.filter,
    flatten = R.flatten,
    has     = R.has,
    head    = R.head,
    last    = R.last, 
    length  = R.length,
    map     = R.map,
    merge   = R.merge,
    nth     = R.nth,
    path    = R.path,
    pipe    = R.pipe,
    prop    = R.prop;


var util = require('util');
var log = require('../app/log.js');

var aguid = require('aguid');
const shortid = require('shortid');

var faker = require('Faker');
var moment = require('moment');

var testMatch = require('../app/models/match.js');
var testMatches = require('../app/models/matches.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var dbUri = nconf.get('database:uri');

var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;

var testAccount = require('../app/models/account.js');
var testAccounts = require('../app/models/accounts.js');

var testBoardgame = require('../app/models/boardgame.js');
var testBoardgames = require('../app/models/boardgames.js');

var genhelper = require('./genhelper.js');

var currentyear = new Date().getFullYear();

var nodemailer = require('nodemailer');
var stubTransport = require('nodemailer-stub-transport');
var transporter = nodemailer.createTransport(stubTransport());

mongo.ConnectDatabase(dbUri);

// scrub :: obj -> obj;
var scrub = R.compose(
  R.dissoc('_id'), 
  R.dissoc('__v'), 
  JSON.parse, 
  JSON.stringify
);

// mergeEvery o -> o
const mergeEveryWeek = R.curry((obj) => R.compose(
  R.merge(R.__, {reoccur:{enable:true, every: '1', interval:'week'}}), 
  scrub
)(obj));


describe('matches_spec.js', function() {

  // Number -> String -> Date -> Number -> Number -> Number -> Boolean
  var whereMatchIs = function(locname, gamedate, gameend, players, minplayers, status, smoking, alcohol) {
      // timeslots:  [{startdatetime: gamedate, enddatetime: gameend}],
    return R.whereEq({
      locname:    locname,
      seatmax:    players,
      seatmin:    minplayers,
      status:     status,
      smoking:    smoking,
      alcohol:    alcohol
    });
  }

  var whereGameIs = function(gameid, gamename, thumbnail) {
    return R.whereEq(testMatches.defaultGame(gameid, gamename, thumbnail));
  }

  const createMatchObj = R.compose(
    R.of, 
    testMatches.Zipmatch, 
    R.zipObj([
      'id', 'name', 'thumbnail', 'description', 
      'minplaytime', 'maxplaytime', 'minplayers', 
      'maxplayers'
    ])
  );

  // String -> Number -> Number -> Object
  var createHostAccountObj = function(location, lat, lng, avatar) {
    return {
      profile: {
        ishost: 2,
        avataricon: avatar,
        name: faker.Name.findName(),
      },
      hostareas: [
        {
          name: location,
          loc: {
            coordinates: [lng, lat],
            type: 'Point'
          }
        }
      ]
    };
  };
  
  // String -> Number -> Number -> Object
  var createLocalAccountObj = function(location, lat, lng, avatar) {
    return {
      profile: {
        ishost: 1,
        avataricon: avatar,
        name: faker.Name.findName(),
        localarea: {
          name: location,
          loc: {
            coordinates: [lng, lat],
            type: 'Point'
          }
        }
      }
    };
  };


  // BoardgameSchema -> Number -> Number -> String -> String -> Number -> Promise(Boardgame)
  var cacheGame = R.curry(function(testBoardgame, currentyear, gameid, gamename, thumbnail, gameyear) { 
    return new Promise(function(fulfill, reject) {
      var cacheObject = {
        numplays: 0,
        thumbnail: thumbnail,
        yearpublished: gameyear,
        name: {
          t: gamename,
          sortindex: 1
        },
        subtype: 'boardgame',
        objectid: gameid,
        objecttype: 'thing',
      };
      fulfill(testBoardgames.CacheBoardgame(testBoardgame, currentyear, cacheObject));
    });
  });
  var cacheGame2017 = cacheGame(testBoardgame, 2017);

  var generateUser = genhelper.GenerateUser(testAccount);
  var generateHostUser = genhelper.GenerateHostUser(testAccount);
  var generateGame = genhelper.GenerateFakeBoardgame(testBoardgame);
  var generateMatch = genhelper.GenerateFakeHostedMatch(testAccount, testBoardgame, testMatch);
  var generateMatchWDate = genhelper.GenerateFakeHostedMatchWDate(testAccount, testBoardgame, testMatch);

  // buildHost :: o -> o -> s -> s -> d -> n -> n -> s -> o -> s -> [n, n] -> o -> bool) -> o 
  const buildHost = function (match, account, email, key, gamedate, maxplayers, minplayers, status, social, locname, coords, gameObj, isprivate) {

		const objectify = (x) => {
			try {
				return x.toObject();
			} catch (err) {
				return x;
			}
		};

    const host = R.compose(
      testMatches.HostMatch(match, account, email, key),
			objectify,
      zipObj(['date', 'timeend', 'timeslots', 'maxplayers', 'minplayers', 'status', 'social', 'locname', 'coords', 'games', 'isprivate'])
    );

    // timeslotUnlessPrivate :: b -> d -> arr
    const timeslotUnlessPrivate = R.curry((isprivate, gamedate) => 
      (isprivate)
        ? [{id: shortid.generate(), startdatetime: gamedate, enddatetime: moment(gamedate).add(60*60, 's').toDate()}]
        : []
    );

    return host([
      gamedate,
      moment(gamedate).add(60*60, 's').toDate(),
      timeslotUnlessPrivate(isprivate, gamedate),
			maxplayers, 
			minplayers, 
			status, 
			social, 
			locname, 
			coords, 
			gameObj, 
			isprivate
		]);

  };

  it('findHostUser', function(done) {
    // make tests
    expect(testMatches.findHostUser({}).isJust).toBeFalsy();
    expect(testMatches.findHostUser({players:[]}).isJust).toBeFalsy();
    expect(testMatches.findHostUser({players:[{type:'host'}]}).isJust).toBeTruthy();
    expect(testMatches.findHostUser({players:[{type:'host', email:'host@memail' }]}).map(R.prop('email')).get()).toBe('host@memail');
    done();
  });


  it('testHostUser', function(done) {
    // make tests
    expect(testMatches.testHostUser('host', {})).toBeFalsy();
    expect(testMatches.testHostUser('host', {players:[]})).toBeFalsy();
    expect(testMatches.testHostUser('host', {players:[{type:'host'}]})).toBeFalsy();

    expect(testMatches.testHostUser('host@email', {players:[{type:'host', email:'bad@email' }]})).toBeFalsy();
    expect(testMatches.testHostUser('host@email', {players:[{type:'host', email:'host@email' }]})).toBeTruthy();
    done();
  });


  it('verifyDateOrder', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var gameEnd = new Date(playyear, 08, 03, 22, 30, 0); 

    var gameEnd24hrEarly = new Date(playyear, 08, 01, 22, 30, 0); 

    expect(testMatches.verifyDateOrder({gamedate: gameDate, timeend: gameEnd})).toBe(true);
    expect(testMatches.verifyDateOrder({gamedate: gameDate, timeend: gameEnd24hrEarly})).toBe(false);
    expect(testMatches.verifyDateOrder({gamedate: gameDate})).toBe(false);
    expect(testMatches.verifyDateOrder({timeend: gameEnd})).toBe(false);

    expect(testMatches.verifyDateOrder({timeslots: [{startdatetime: gameDate, enddatetime: gameEnd}]})).toBe(true);
    expect(testMatches.verifyDateOrder({timeslots: [{startdatetime: gameDate, enddatetime: gameEnd24hrEarly}]})).toBe(false);
    expect(testMatches.verifyDateOrder({timeslots: [{startdatetime: gameDate}]})).toBe(false);
    expect(testMatches.verifyDateOrder({timeslots: [{enddatetime: gameEnd}]})).toBe(false);

    expect(testMatches.verifyDateOrder({timeslots: [
      {startdatetime: gameDate, enddatetime: gameEnd},
      {startdatetime: gameDate, enddatetime: gameEnd24hrEarly}
    ]})).toBe(false);

    done();
  });


  it('verifyDateDuration', function(done) {

    var playyear =          new Date().getFullYear() + 1;
    var gameDate =          new Date(playyear, 08, 03, 20, 30, 0); 

    var gameEnd =           new Date(playyear, 08, 03, 22, 30, 0); 
    var gameEndShort =      new Date(playyear, 08, 03, 20, 45, 0); 
    var gameEnd24hrAfter =  new Date(playyear, 08, 05, 22, 30, 0); 

    expect(testMatches.verifyDateDuration({gamedate: gameDate, timeend: gameEnd})).toBe(true);
    expect(testMatches.verifyDateDuration({gamedate: gameDate, timeend: gameEnd24hrAfter})).toBe(false);
    expect(testMatches.verifyDateDuration({gamedate: gameDate, timeend: gameEndShort})).toBe(false);
    expect(testMatches.verifyDateDuration({gamedate: gameDate})).toBe(false);
    expect(testMatches.verifyDateDuration({timeend: gameEnd})).toBe(false);

    expect(testMatches.verifyDateDuration({timeslots: [{startdatetime: gameDate, enddatetime: gameEnd}]})).toBe(true);
    expect(testMatches.verifyDateDuration({timeslots: [{startdatetime: gameDate, enddatetime: gameEndShort}]})).toBe(false);
    expect(testMatches.verifyDateDuration({timeslots: [{startdatetime: gameDate, enddatetime: gameEnd24hrAfter}]})).toBe(false);
    expect(testMatches.verifyDateDuration({timeslots: [{startdatetime: gameDate}]})).toBe(false);
    expect(testMatches.verifyDateDuration({timeslots: [{enddatetime: gameEnd}]})).toBe(false);

    expect(testMatches.verifyDateDuration({timeslots: [
      {startdatetime: gameDate, enddatetime: gameEnd},
      {startdatetime: gameDate, enddatetime: gameEnd24hrAfter}
    ]})).toBe(false);

    done();
  });

  it('validReoccur', function(done) {

    expect(testMatches.validReoccur({}).value).toEqual([ 
      'Repeating campaign does not have reoccur enable set to true'
    ]);

    expect(testMatches.validReoccur({reoccur:{}}).value).toEqual([ 
      'Repeating campaign does not have reoccur enable set to true', 
    ]);

    expect(testMatches.validReoccur({reoccur:{enable:false}}).value).toEqual([ 
      'Repeating campaign does not have reoccur enable set to true', 
    ]);

    expect(testMatches.validReoccur({reoccur:{enable:true}}).hasInstance).toBeTruthy();

    done();

  });

  it('verifyGameExperience', function(done) {

    expect(testMatches.validGameExperience({}).hasInstance).toBeTruthy();

    expect(testMatches.validGameExperience({experience:'10'}).value).toEqual([ 
      'Repeating campaign does not have reoccur enable set to true', 
      'Event does not have a valid game' 
    ]);

    expect(testMatches.validGameExperience({experience:'10', reoccur:{}}).value).toEqual([ 
      'Repeating campaign does not have reoccur enable set to true', 
      'Event does not have a valid game' 
    ]);

    expect(testMatches.validGameExperience({experience:'10', reoccur:{enable:false}}).value).toEqual([ 
      'Repeating campaign does not have reoccur enable set to true', 
      'Event does not have a valid game' 
    ]);

    expect(testMatches.validGameExperience({experience:'10', reoccur:{enable:true}}).value).toEqual([ 
      'Event does not have a valid game' 
    ]);

    expect(testMatches.validGameExperience({experience:'10', reoccur:{enable:true}, games:[]}).value).toEqual([ 
      'Event does not have a valid game' 
    ]);

    expect(testMatches.validGameExperience({experience:'10', games:[]}).value).toEqual([ 
      'Repeating campaign does not have reoccur enable set to true', 
      'Event does not have a valid game' 
    ]);

    expect(testMatches.validGameExperience({experience:'10', games:[{a:'b'}]}).value).toEqual([ 
      'Repeating campaign does not have reoccur enable set to true'
    ]);

    expect(testMatches.validGameExperience({experience:'10', reoccur:{enable:true}, games:[{a:'b'}]}).hasInstance).toBeTruthy();

    done();

  });

  it('CreateMatch', function(done) {

    var gameid = 180263;
    var gamename = 'The 7th Continent';
    var thumbnail = 'https://cf.geekdo-images.com/pN4gQOkEryKdpUnOTcxwj5LKHqk=/fit-in/246x300/pic2648303.jpg';
    var gameyear = 2017;

    var matchx = {'games.id': gameid};
    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var gameEnd = new Date(playyear, 08, 03, 22, 30, 0); 

    var gameEnd24hrEarly = new Date(playyear, 08, 01, 22, 30, 0); 
    var gameEnd24hrAfter = new Date(playyear, 08, 05, 22, 30, 0); 

    var matchLocName = 'University of Wollongong';
    var matchUser = {
      email: 'match1@gmail.com', 
      avataricon: 'http://localhost:3000/img/test-avatar1.jpg',
      name: 'Jeff Jones'
    };

    var minPlayers = 2;
    var matchPlayers = 5;
    var lat = -34.4054039;
    var lng = 150.8784299;
    var coords = [lng, lat];

    var readLat = compose(last, path(['loc', 'coordinates']));
    var readLng = compose(head, path(['loc', 'coordinates']));

    const readHostkey = (keyname, obj) => compose(prop(keyname), head, prop('players'))(obj);

    const status1 = 'draft';
    const status2 = 'open';

    const smoking = 2;
    const alcohol = 1;
    const eventid = 'e3';

    const social = {
      smoking: smoking,
      alcohol: alcohol
    };

    testMatch.remove(matchx).then(function(y) {
      genhelper.GenerateFakeBoardgame(testBoardgame).then(function(y) {

        const makeMatch = R.compose(
          testMatches.CreateMatch(testMatch, matchUser, false), 
          zipObj(['locname', 'coords', 'timeslots', 'maxplayers', 'minplayers', 'status', 'social', 'games', 'isprivate'])
        );

        const makeEventMatch = R.compose(
          testMatches.CreateMatch(testMatch, matchUser, false), 
          zipObj(['locname', 'coords', 'timeslots', 'maxplayers', 'minplayers', 'status', 'social', 'games', 'eventid', 'isprivate'])
        );

        const makeMatchX = R.compose(
          testMatches.CreateMatch(testMatch, matchUser, false), 
          zipObj(['locname', 'coords', 'timeslots', 'maxplayers', 'minplayers', 'status', 'social', 'games', 'x', 'isprivate'])
        );

        const makeBadMatch = R.compose(
          testMatches.CreateMatch(testMatch, matchUser, false), 
          zipObj(['locname', 'coords', 'timeslots', 'maxplayers', 'minplayers', 'status', 'social'])
        );

        const match1 = makeMatchX([matchLocName, coords, [{startdatetime: gameDate, enddatetime: gameEnd}], matchPlayers, minPlayers, status1, social, createMatchObj([y.id, y.name, y.thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4]), 'n.ox', false]);

        const match2 = makeMatch([matchLocName, coords, [{startdatetime: gameDate, enddatetime: gameEnd}], matchPlayers, minPlayers, status1, social, createMatchObj([y.id, y.name, y.thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4]), true]);

        const match3 = makeMatch([matchLocName, coords, [], matchPlayers, minPlayers, status2, social, createMatchObj([y.id, y.name, y.thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4]), false]);

        const match4 = makeEventMatch([matchLocName, coords, [{startdatetime: gameDate, enddatetime: gameEnd}], matchPlayers, minPlayers, status1, social, createMatchObj([y.id, y.name, y.thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4]), eventid, true]);

        const match24Early = makeMatch([matchLocName, coords, [{startdatetime: gameDate, enddatetime: gameEnd24hrEarly}], matchPlayers, minPlayers, status1, social, createMatchObj([y.id, y.name, y.thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4]), true]);

        const match24EarlyTimeslot = makeMatch([matchLocName, coords, [ {startdatetime: gameDate, enddatetime: gameEnd}, {startdatetime: gameDate, enddatetime: gameEnd24hrEarly} ], matchPlayers, minPlayers, status1, social, createMatchObj([y.id, y.name, y.thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4]), true]);

        const match24After = makeMatch([matchLocName, coords, [{startdatetime: gameDate, enddatetime: gameEnd24hrAfter}], matchPlayers, minPlayers, status1, social, createMatchObj([y.id, y.name, y.thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4]), true]);

        const wherematcheq = whereMatchIs(matchLocName, gameDate, gameEnd, matchPlayers, minPlayers, status1, smoking, alcohol);
        const wherematcheq2= whereMatchIs(matchLocName, gameDate, gameEnd, matchPlayers, minPlayers, status2, smoking, alcohol);

        match24Early.then(()=>{}).catch(err => {
          expect(err).toBe("The start time must be after the end time.");

          match24EarlyTimeslot.then(()=>{}).catch(err => {
            expect(err).toBe("The start time must be after the end time.");

            match24After.then(()=>{}).catch(err => {
              expect(err).toBe("The event should be more than thirty minutes and less than 24hrs duration.");

              match1.then((x) => {

                expect(wherematcheq(x)).toBeTruthy();
                expect(x.games[0].email).toBe(matchUser.email);

                expect(readLat(x)).toBe(lat); 
                expect(readLng(x)).toBe(lng); 
                expect(prop('seatavailable', x)).toBe(matchPlayers - 1);
                expect(prop('seatmax', x)).toBe(matchPlayers);
                expect(prop('seatmin', x)).toBe(minPlayers);
                expect(readHostkey('avataricon', x)).toBe(matchUser.avataricon);
                expect(readHostkey('name', x)).toBe(matchUser.name);
                expect(prop('status', x)).toBe(status1);

                expect(prop('isprivate', x)).toBe(true);  // draft status forces events to be private

                match3.then((x) => {
                  expect(wherematcheq2(x)).toBeTruthy();
                  expect(x.games[0].email).toBe(matchUser.email);

                  expect(readLat(x)).toBe(lat); 
                  expect(readLng(x)).toBe(lng); 
                  expect(prop('seatavailable', x)).toBe(matchPlayers - 1);
                  expect(prop('seatmax', x)).toBe(matchPlayers);
                  expect(prop('seatmin', x)).toBe(minPlayers);
                  expect(readHostkey('avataricon', x)).toBe(matchUser.avataricon);
                  expect(readHostkey('name', x)).toBe(matchUser.name);
                  expect(prop('status', x)).toBe(status2);
                  expect(prop('isprivate', x)).toBe(false);  

                  match2.then((x) => {

                    expect(whereGameIs(y.id, y.name, y.thumbnail)(x.games[0])).toBe(true);
                    expect(readLat(x)).toBe(lat); 
                    expect(readLng(x)).toBe(lng); 
                    expect(prop('seatavailable', x)).toBe(matchPlayers - 1);
                    expect(prop('seatmax', x)).toBe(matchPlayers);
                    expect(prop('seatmin', x)).toBe(minPlayers);
                    expect(readHostkey('avataricon', x)).toBe(matchUser.avataricon);
                    expect(readHostkey('name', x)).toBe(matchUser.name);
                    expect(prop('status', x)).toBe(status1);
                    expect(prop('isprivate', x)).toBe(true);  // events with timeslots are private

                    match4.then(function(y) {
                      expect(prop('eventid', y)).toBe(eventid)
                      done()
                    });

                  });


                });
              });
            });
          });
        });
           
      }).catch(err => log.clos('caught', err));
    }); 
  });

  it('MapMergeEmail', function(done) {
    expect(testMatches.MapMergeEmail('e', 'u', 'a', [{}])).toEqual([{email:'e', username: 'u', avataricon: 'a'}]);
    expect(testMatches.MapMergeEmail('e', 'u', 'a', [{}, {}])).toEqual([{email:'e', username: 'u', avataricon: 'a'}, {email:'e', username: 'u', avataricon: 'a'}]);
    expect(testMatches.MapMergeEmail('e', 'u', 'a', [{a: 'hello'}, {b: 'goodbye'}])).toEqual([{a: 'hello', email:'e', avataricon: 'a', username: 'u'}, {b: 'goodbye', email:'e', avataricon: 'a', username: 'u'}]);
    done();
  });

  it('CheckDateOrder', function(done) {
    expect(testMatches.CheckDateOrder('2017-09-01', '2017-09-10')).toBe(true);
    expect(testMatches.CheckDateOrder('2017-09-10', '2017-09-01')).toBe(false);
    expect(testMatches.CheckDateOrder('2017-09-10 08:00', '2017-09-10 09:00')).toBe(true);
    expect(testMatches.CheckDateOrder('2017-09-10 09:00', '2017-09-10 08:00')).toBe(false);
    expect(testMatches.CheckDateOrder('2017-09-01', '2017-09-01')).toBe(false);
    done();
  });

  it('MergeWhenEvent', function(done) {
    expect(testMatches.MergeWhenEvent({})({})).toEqual({});
    expect(testMatches.MergeWhenEvent({})({a:'b'})).toEqual({a:'b'});
    expect(testMatches.MergeWhenEvent({eventid:'c'})({a:'b'})).toEqual({eventid:'c', a:'b'});
    done();
  });

  it('emptyWhenUndefined', function(done) {
    expect(testMatches.emptyWhenUndefined(['one'])).toEqual(['one']);
    expect(testMatches.emptyWhenUndefined(undefined)).toEqual([]);
    done();
  }); 

  it('mapPropId', function(done) {
    expect(testMatches.mapPropId([])).toEqual([]);
    expect(testMatches.mapPropId([{id:1}, {id:2}])).toEqual([1, 2]);
    done();
  }); 

  it('getGameIds', function(done) {
    expect(testMatches.getGameIds(undefined).isNothing).toBe(true);
    expect(testMatches.getGameIds({}).isNothing).toBe(true);
    expect(testMatches.getGameIds({games:[]}).get()).toEqual([]);
    expect(testMatches.getGameIds({games:[{id:1}, {id:2}]}).get()).toEqual([1, 2]);
    done();
  });

	it('statusOnTimeslot', function(done) {
		expect(testMatches.statusOnTimeslot('a', {})).toBe('a');
		expect(testMatches.statusOnTimeslot('draft', {})).toBe('open');
		expect(testMatches.statusOnTimeslot('draft', {timeslots:[]})).toBe('open');
		expect(testMatches.statusOnTimeslot('draft', {timeslots:[{}]})).toBe('draft');
		done();
	});

  it('whenPrivate', function(done) {
    expect(testMatches.whenPrivate(true, {isprivate:false})).toBeFalsy();
    expect(testMatches.whenPrivate(true, {isprivate:true})).toBeTruthy();
    expect(testMatches.whenPrivate(false, {isprivate:false})).toBeFalsy();
    expect(testMatches.whenPrivate(false, {isprivate:false, timeslots:[]})).toBeFalsy();
    expect(testMatches.whenPrivate(false, {isprivate:false, timeslots:[{a:'b'}]})).toBeTruthy();
    expect(testMatches.whenPrivate(false, {})).toBeTruthy();
    done();
  });

  it('assocWhenExist', function(done) {
    expect(testMatches.assocWhenExist('a', 3, {})).toEqual({a: 3})
    expect(testMatches.assocWhenExist('a', undefined, {})).toEqual({})
    done();
  });

  it('buildPoint', function(done) {
    expect(testMatches.buildPoint('a')).toEqual({type:'Point', coordinates:'a'});
    expect(testMatches.buildPoint(undefined)).toBeUndefined();
    done();
  });

	it('matchfields', function(done) {

		expect(testMatches.matchfields({}, {}, true, {})).toEqual({ games : {  }, title : undefined, description : undefined, experience : undefined, status : undefined, isprivate : undefined, seatmin : undefined, seatmax : undefined, seatavailable : 0, players : [ { type : 'host' } ], currency : undefined, price : undefined, utcoffset : undefined, timezone : undefined, reoccur : undefined, timeslots : undefined, date : undefined, timeend : undefined });

		expect(testMatches.matchfields({}, {}, false, {})).toEqual({ games : {  }, title : undefined, description : undefined, experience : undefined, status : undefined, isprivate : true, seatmin : undefined, seatmax : undefined, seatavailable : 0, players : [ { type : 'host' } ], currency : undefined, price : undefined, utcoffset : undefined, timezone : undefined, reoccur : undefined, timeslots : undefined, date : undefined, timeend : undefined });

		expect(testMatches.matchfields({}, {}, false, [{g:'hi'}])).toEqual({ games : [{g:'hi'}], title : undefined, description : undefined, experience : undefined, status : undefined, isprivate : true, seatmin : undefined, seatmax : undefined, seatavailable : 0, players : [ { type : 'host' } ], currency : undefined, price : undefined, utcoffset : undefined, timezone : undefined, reoccur : undefined, timeslots : undefined, date : undefined, timeend : undefined });

		expect(testMatches.matchfields('host', {title:'t', description:'d', locname: 'ln', coords:[{c:'a'}], experience:'xp', status:'s', minplayers:'1', maxplayers:'3', currency:'c', price: 'p', utcoffset: 'utc', timezone: 'tz', reoccur: {r:'r'}, timeslots:[], gamedate:'gds', timeend: 'gde'}, false, [{g:'hi'}])).toEqual({ games : [{g:'hi'}], title : 't', description : 'd', locname : 'ln', loc : { type : 'Point', coordinates : [{c:'a'}] }, experience : 'xp', status : 's', isprivate : true, seatmin : '1', seatmax : '3', seatavailable : 2, players : [ { email: 'host',type : 'host' } ], currency : 'c', price : 'p', utcoffset : 'utc', timezone : 'tz', reoccur : {r:'r'}, timeslots : [], date : 'gds', timeend : 'gde' });

		expect(testMatches.matchfields('host', {title:'t', description:'d', locname: 'ln', coords:[{c:'a'}], experience:'xp', status:'s', minplayers:'1', maxplayers:'3', currency:'c', price: 'p', utcoffset: 'utc', timezone: 'tz', reoccur: {r:'r'}, timeslots:[{a:''}], gamedate:'gds', timeend: 'gde', autoaccept: true}, false, [{g:'hi'}])).toEqual({ games : [ { g : 'hi' } ], title : 't', description : 'd', experience : 'xp', status : 's', isprivate : true, seatmin : '1', seatmax : '3', seatavailable : 2, players : [ { email : 'host', type : 'host' } ], currency : 'c', price : 'p', utcoffset : 'utc', timezone : 'tz', reoccur : { r : 'r' }, timeslots : [ { a : '' } ], date : 'gds', timeend : 'gde', autoaccept : true, loc : { type : 'Point', coordinates : [ { c : 'a' } ] }, locname : 'ln' });

		expect(testMatches.matchfields('host', {title:'t', description:'d', locname: 'ln', coords:[{c:'a'}], experience:'xp', status:'s', minplayers:'1', maxplayers:'3', currency:'c', price: 'p', utcoffset: 'utc', timezone: 'tz', reoccur: {r:'r'}, timeslots:[], gamedate:'gds', timeend: 'gde', autoaccept: true}, true, [{g:'hi'}])).toEqual({ games : [ { g : 'hi' } ], title : 't', description : 'd', locname : 'ln', loc : { type : 'Point', coordinates : [ { c : 'a' } ] }, experience : 'xp', status : 's', isprivate : undefined, seatmin : '1', seatmax : '3', seatavailable : 2, players : [ { email : 'host', type : 'host' } ], currency : 'c', price : 'p', utcoffset : 'utc', timezone : 'tz', reoccur : { r : 'r' }, timeslots : [  ], date : 'gds', timeend : 'gde', autoaccept : true } );

		done();
	});

	it('maybeHeadTimeslotProp', function(done) {
		expect(testMatches.maybeHeadTimeslotProp('a', 'b', {}).isJust).toBeFalsy();
		expect(testMatches.maybeHeadTimeslotProp('a', 'b', {timeslots:[]}).isJust).toBeFalsy();
		expect(testMatches.maybeHeadTimeslotProp('a', 'b', {timeslots:[{a:'h'}]}).get()).toEqual({b:'h'});
		done();
	});

	it('mergeWhenHeadTimeSLot', function(done) {
		expect({}, {}).toEqual({});
		expect(testMatches.mergeWhenHeadTimeSlot({}, {a:'hz'})).toEqual({a:'hz'});
		expect(testMatches.mergeWhenHeadTimeSlot({timeslots:[{startdatetime:'s'}]}, {a:'hz'})).toEqual({a:'hz', date:'s'});
		expect(testMatches.mergeWhenHeadTimeSlot({timeslots:[{startdatetime:'s', enddatetime:'edt'}]}, {a:'hz'})).toEqual({a:'hz', date:'s', timeend:'edt'});
		expect(testMatches.mergeWhenHeadTimeSlot({timeslots:[{startdatetime:'s', enddatetime:'edt', id:123}]}, {a:'hz'})).toEqual({a:'hz', date:'s', timeend:'edt', timeslotobj:123});
		done();
	});

  it('whichProfile', function(done) {
    expect(testMatches.whichProfile('a', {}).isNothing).toBe(true);
    expect(testMatches.whichProfile('a', {a: 'b'}).get()).toBe('b');
    expect(testMatches.whichProfile('a', {profile: {a: 'b'}}).get()).toBe('b');
    done();
  });

  it('CreateMatch Multiple Games', function(done) {

    var gameid1 = 5678;
    var gamename1 = 'Theives Guild';
    var thumbnail1 = '//cf.geekdo-images.com/T0EHgTBYxTAhJjDnvziP3S5RGno=/fit-in/246x300/pic25418.jpg';
    var gameyear1 = 2003;

    var gameid2 = 200954;
    var gamename2 = 'Fields of Green';
    var thumbnail2 = 'https://cf.geekdo-images.com/Ggpi5zPpW1XxnwCxkRezych2xSo=/fit-in/246x300/pic3504382.jpg';
    var gameyear2 = 2016;

    var matchx = {'games.id': gameid1};
    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var gameEnd = new Date(playyear, 08, 03, 22, 30, 0); 

    var matchLocName = 'University of Wollongong';
    var matchUser = {
      email: 'match1@gmail.com', 
      avataricon: 'http://localhost:3000/img/test-avatar1.jpg',
      name: 'Jeff Jones'
    };
    var minPlayers = 2;
    var matchPlayers = 5;
    var lat = -34.4054039;
    var lng = 150.8784299;
    var coords = [lng, lat];

    var readLat = compose(last, path(['loc', 'coordinates']));
    var readLng = compose(head, path(['loc', 'coordinates']));

    const readHostkey = (keyname, obj) => compose(prop(keyname), head, prop('players'))(obj);

    const status1 = 'open';
    const smoking = 2;
    const alcohol = 1;

    const social = {
      smoking: smoking,
      alcohol: alcohol
    };

    Promise.all([testMatch.remove(matchx), testBoardgames.DeleteBoardgame(testBoardgame, gameid1), testBoardgames.DeleteBoardgame(testBoardgame, gameid2)]).then(function(y) {

      Promise.all([cacheGame2017(gameid1, gamename1, thumbnail1, gameyear1), cacheGame2017(gameid2, gamename2, thumbnail2, gameyear2)]).then(function(z) {

        const makeMatch = R.compose(testMatches.CreateMatch(testMatch, matchUser, false), zipObj(['locname', 'coords', 'gamedate', 'timeend', 'maxplayers', 'minplayers', 'status', 'social', 'games']));

        const match1 = makeMatch([matchLocName, coords, gameDate, gameEnd, matchPlayers, minPlayers, status1, social, R.concat(createMatchObj([gameid1, gamename1, thumbnail1, 'Pending Bgg Lookup...', 30, 60, 2, 4]), createMatchObj([gameid2, gamename2, thumbnail2, 'Pending Bgg Lookup...', 30, 60, 2, 4]))]);

        const wherematcheq = whereMatchIs(matchLocName, gameDate, gameEnd, matchPlayers, minPlayers, status1, smoking, alcohol);

        // TODO: somehow this is rejecting the promise, but still going via then
        match1
          .then((x) => {
            expect(wherematcheq(x)).toBeTruthy();
            expect(whereGameIs(gameid1, gamename1, thumbnail1)(x.games[0])).toBe(true);
            expect(whereGameIs(gameid2, gamename2, thumbnail2)(x.games[1])).toBe(true);
            expect(readLat(x)).toBe(lat); 
            expect(readLng(x)).toBe(lng); 
            expect(prop('seatavailable', x)).toBe(matchPlayers - 1);
            expect(prop('seatmax', x)).toBe(matchPlayers);
            expect(prop('seatmin', x)).toBe(minPlayers);
            expect(readHostkey('avataricon', x)).toBe(matchUser.avataricon);
            expect(readHostkey('name', x)).toBe(matchUser.name);
            expect(prop('status', x)).toBe(status1);

            done();

          })
        .catch((err) => {
          log.clos('err', err);

        });
      });
    }); 
  });


  it('MaybeName', function(done) {
		user1 = [{profile: {name: 'n'}}];
		user2 = {};
		expect(testMatches.MaybeName(user1)).toBe('n');
		expect(testMatches.MaybeName(user2)).toBe('Unknown Name');
		done();
  });

  it('MaybeAvatar', function(done) {
		user1 = [{profile: {avataricon: 'a'}}];
		user2 = {};
		expect(testMatches.MaybeAvatar(user1)).toBe('a');
		expect(testMatches.MaybeAvatar(user2)).toBe('/img/default-avatar-sq.jpg');
		done();
  });

  it('MaybeFirst', function(done) {
    expect(testMatches.MaybeFirst(Maybe.of('a'), Maybe.of('b')).get()).toBe('a');
    expect(testMatches.MaybeFirst(Maybe.Nothing(), Maybe.of('b')).get()).toBe('b');
    expect(testMatches.MaybeFirst(Maybe.Nothing(), Maybe.Nothing()).isNothing).toBe(true);
    done();
  });

  it('MaybeData', function(done) {
    expect(testMatches.MaybeData({a: 'a'}, {a: 'b'}, 'a').get()).toBe('a');
    expect(testMatches.MaybeData({}, {a: 'b'}, 'a').get()).toBe('b');
    expect(testMatches.MaybeData({}, {}, 'a').isNothing).toBe(true);
    done();
  });

  it('playerAndFriendCount', function(done) {
    expect(testMatches.playerAndFriendCount({})).toBe(1);
    expect(testMatches.playerAndFriendCount({extraplayers:3})).toBe(4);
    done();
  });

  it('CountPlayers', function(done) {
    expect(testMatches.CountPlayers({players: ['a', 'b', 'c']}).get()).toBe(0);
    expect(testMatches.CountPlayers({players: [{type:'host'}, {type:'approve'}, {type:'approve'}]}).get()).toBe(3);
    expect(testMatches.CountPlayers({players: [{type:'host'}, {type:'approve'}]}).get()).toBe(2);
    expect(testMatches.CountPlayers({}).isNothing).toBe(true);
    done();
  });

  it('WhenFull', function(done) {
    expect(testMatches.WhenFull({isfull:false}, true)).toEqual({isfull:true});
    expect(testMatches.WhenFull({isfull:true}, false)).toEqual({isfull:false});
    expect(testMatches.WhenFull({}, false)).toEqual({isfull:false});
    done();
  });

  it('CheckFull', function(done) {
    expect(
      testMatches.CheckFull(
        {
          players:[
            {type:'host'}, 
            {type:'approve'},
            {type:'approve'}
          ], 
          seatmax:3
        }, 
        {
          seatmin: '2',
          seatmax: '3',
        } 
      )).toEqual({
        seatmin : '2', 
        seatmax : '3', 
        seatavailable : 0, 
        isfull : true 
      });

      expect(testMatches.CheckFull({players:[{type:'host'}, {type:'approve'}]}, {seatmax:3})).toEqual({seatmax:3, isfull:false, seatavailable:1});
      expect(testMatches.CheckFull({players:[{type:'host'}, {type:'approve'}, {type:'approve'}]}, {seatmax:3})).toEqual({seatmax:3, isfull:true, seatavailable:0});
      expect(testMatches.CheckFull({players:[{type:'host'}, {type:'approve'}, {type:'invite'}, {type:'approve'}]}, {seatmax:3})).toEqual({seatmax:3, isfull:true, seatavailable: 0});
      expect(testMatches.CheckFull({players:[{type:'host'}, {type:'approve'}, {type:'approve'}]}, {a:'a', b:'b'})).toEqual({a:'a', b:'b'});
      done();
  });

  it('TaskFindMatch', function(done) {
    generateMatch('open', 2, 2).then(function(a) {
      testMatches.TaskFindMatch(testMatch, a[1].key).fork(
        err => {},
        data => {
          expect(data.key).toBe(a[1].key);
          expect('open').toBe(a[1].status);
          expect(2).toBe(a[1].seatmin);
          testMatches.TaskFindMatch(testMatch, 'badkey').fork(
            err => {
              done();
            },
            data => {}
          )
        }
      )
    });
  });

  it('CreateHostUser', function(done) {
    expectObj = {email: 'e', avataricon: 'a', name: 'n'};
    expect(testMatches.CreateHostUser('a', 'n', 'e')).toEqual(expectObj);
    done();
  });

  it('checkSpamTitle', function(done) {
    expect(testMatches.checkSpamTitle({})).toBeFalsy();
    expect(testMatches.checkSpamTitle({title:'abc'})).toBeFalsy();
    expect(testMatches.checkSpamTitle({title:'abc钱xyz'})).toBeTruthy();
    done();
  });

  it('checkSpamJapaneseCharacters', function(done) {
    expect(testMatches.checkSpamJapaneseCharacters({})).toBeFalsy();
    expect(testMatches.checkSpamJapaneseCharacters({description:'abc'})).toBeFalsy();
    expect(testMatches.checkSpamJapaneseCharacters({description:'大戸屋www.spam.com'})).toBeTruthy();
    done();
  });

  it('checkSpamChineseCharacters', function(done) {
    expect(testMatches.checkSpamChineseCharacters({})).toBeFalsy();
    expect(testMatches.checkSpamChineseCharacters({description:'abc'})).toBeFalsy();
    expect(testMatches.checkSpamChineseCharacters({description:'餐馆www.xyz.com'})).toBeTruthy();
    done();
  });

  it('checkSpamGooglePlay', function(done) {
    expect(testMatches.checkSpamGooglePlay({})).toBeFalsy();
    expect(testMatches.checkSpamGooglePlay({description:'abc'})).toBeFalsy();
    expect(testMatches.checkSpamGooglePlay({description:'https://play.google.com/spam'})).toBeTruthy();
    done();
  });

  it('checkSpamAppleStore', function(done) {
    expect(testMatches.checkSpamAppleStore({})).toBeFalsy();
    expect(testMatches.checkSpamAppleStore({description:'abc'})).toBeFalsy();
    expect(testMatches.checkSpamAppleStore({description:'https://itunes.apple.com/spam'})).toBeTruthy();
    done();
  });

	it('hasTimeslots', function(done) {
		expect(testMatches.hasTimeslots({}).isJust).toBeFalsy();
		expect(testMatches.hasTimeslots({timeslots:[]}).isJust).toBeFalsy();
		expect(testMatches.hasTimeslots({timeslots:[{a:'hello'}]}).get()).toEqual({a:'hello'});
		done();
	});

  it('HostMatch', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var minPlayers = 2;
    var matchPlayers = 5;

    var status = ['open'];
    var social = [
      {smoking: 1, alcohol: 2}, 
      {smoking: 2, alcohol: 1}
    ];

    var readLat = R.compose(R.head, R.path(['loc', 'coordinates']));
    var readLng = R.compose(R.last, R.path(['loc', 'coordinates']));

    const readHostkey = (keyname, obj) => compose(prop(keyname), head, prop('players'))(obj);
    const readUser = composeP(head, testAccounts.ReadAccountFromEmail(testAccount));

    Promise.all([
      generateHostUser({tickets: 5}), 
      generateUser({}), 
      generateGame, 
      generateHostUser({tickets: 5}), 
      generateHostUser({spamuser: true}),
      generateHostUser({tickets: 5}), 
    ]).then(function(y) {

      var errorCode = R.prop('code');

      var matchSuccess = buildHost(testMatch, testAccount, y[0].email, y[0].key, gameDate, matchPlayers, minPlayers, status[0], social[0], y[0].hostareas[0].name, y[0].hostareas[0].loc.coordinates, createMatchObj([y[2].id, y[2].name, y[2].thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4], false));

      var matchPrivate = buildHost(testMatch, testAccount, y[3].email, y[3].key, gameDate, matchPlayers, minPlayers, status[0], social[0], y[3].hostareas[0].name, y[3].hostareas[0].loc.coordinates, createMatchObj([y[2].id, y[2].name, y[2].thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4], true));

      var matchSpam = buildHost(testMatch, testAccount, y[4].email, y[4].key, gameDate, matchPlayers, minPlayers, status[0], social[0], y[3].hostareas[0].name, y[3].hostareas[0].loc.coordinates, createMatchObj([y[2].id, y[2].name, y[2].thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4], true));

      var matchSpamTitle = testMatches.HostMatch(testMatch, testAccount, y[5].email, y[5].key, {title:'mtkHi! 2018最新商机用手机网上赚钱日入1000不是梦，有兴趣茄薇信wLmxqh123禾ム聊咨询！'});
      
      readUser(y[3].email).then(function(user) {
        matchPrivate.then(function(mp) {
          readUser(y[3].email).then(function(user) {
            matchSuccess.then(function(x) {
              readUser(y[0].email).then(function(user) {
                  
                var whereGame = whereGameIs(y[2].id, y[2].name, y[2].thumbnail);

                var whereMatch = whereMatchIs(y[0].hostareas[0].name, gameDate, x.timeend, matchPlayers, minPlayers, status[0], social[0].smoking, social[0].alcohol);

                expect(whereGame(x.games[0])).toBeTruthy();
                expect(whereMatch(x)).toBeTruthy();
                expect(readLat(x)).toBe(Number(y[0].hostareas[0].loc.coordinates[0])); 
                expect(readLng(x)).toBe(Number(y[0].hostareas[0].loc.coordinates[1])); 
                expect(prop('seatavailable', x)).toBe(matchPlayers - 1);
                expect(readHostkey('avataricon', x)).toBe(y[0].profile.avataricon);
                expect(readHostkey('name', x)).toBe(y[0].profile.name);

                var matchErrorInvalidUser = buildHost(testMatch, testAccount, 'invaliduser@gmail.com', y[0].key, y[2].id, gameDate, matchPlayers, minPlayers, status[0], social[0], y[0].hostareas[0].name, y[0].hostareas[0].loc.coordinates, false);

                matchErrorInvalidUser.then().catch(function(err) {

                  // error user does not exist/unauthorised
                  expect(errorCode(err)).toBe(401);

                  var matchErrorInvalidUserkey = buildHost(testMatch, testAccount, y[0].email, 'invalidkey', y[2].id, gameDate, matchPlayers, minPlayers, status[0], social[0], y[0].hostareas[0].name, y[0].hostareas[0].loc.coordinates, false );

                  // error user does not have valid key/unauthorised
                  matchErrorInvalidUserkey.then().catch(function(err) {
                    expect(errorCode(err)).toBe(401);

                    // error when match is created from a spam user
                    matchSpam.then().catch(
                      (err)=> {
                        expect(err).toBe(500);

                        // error when a match has suspected spam 
                        matchSpamTitle
                          .then()
                          .catch(
                            (err)=> {
                              expect(err).toBe(500);
                              done();

                            }
                          )
                      }

                    )
                  })
                })
              })
            })
          })
        })
      });
    });
  }); 


  it('hasPath', function(done) {
    expect(testMatches.hasPath(['a'], {})).toBeFalsy();
    expect(testMatches.hasPath(['a'], {a: 'a'})).toBeTruthy();

    expect(testMatches.hasPath(['d'], {})).toBeFalsy();
    expect(testMatches.hasPath(['d'], {d: 'a'})).toBeTruthy();
    done();
  });

  it('getCoord', function(done) {
    expect(testMatches.getCoord(0, {loc:{coordinates:['a', 'b', 'c']}})).toBe('a');
    expect(testMatches.getCoord(1, {loc:{coordinates:['a', 'b', 'c']}})).toBe('b');
    done();
  });

  it('CalculateMatchDistance', function(done) {

    var lat = -34.45822019999999;
    var lng = 150.83694730000002;

    var match = {
        loc: {
            coordinates: [150.84839120000004, -34.4662757]
        }
    };
    var matchB = {};
    
    var testdistance = R.compose(R.prop('distance'), testMatches.CalculateMatchDistance);

    expect(testdistance(lat, lng, match)).toBe(1381);
    expect(testdistance(lat, lng, {})).toBe(undefined);
    done();
    
  });


  it('DisbandMatch', function(done) {

    const disband = testMatches.Disband(testAccount, testMatch);
    const findMatch = testMatches.FindMatchByKey(testMatch);
    const joinMatch = testMatches.JoinMatch(testAccount, testMatch);
    const approveUser = testMatches.ApproveUser(testAccount, testMatch);

    const readMatchkey  = (x, num) => R.compose(prop('key'), nth((num * 2) + 1), flatten)(x);
    const readHostemail = (x, num) => R.compose(prop('email'), nth(num * 2), flatten)(x);
    const readHostkey   = (x, num) => R.compose(prop('key'), nth(num * 2), flatten)(x);

    // make matches
    generateMatch('open', 10, 2).then(function(a) {
      generateMatch('open', 10, 2).then(function(b) {
        generateMatch('open', 10, 2).then(function(c) {
          generateMatch('open', 10, 2).then(function(d) {

            var x = [a,b,c,d];
            disband(readMatchkey(x, 0), readHostemail(x, 0), readHostkey(x, 0)).then(function(y) {
              findMatch(readMatchkey(x, 0)).then(function(m) {
                expect(m.length).toBe(0);

                // make match, nonhost disband, fail
                disband(readMatchkey(x, 1), readHostemail(x, 2), readHostkey(x, 2)).then().catch(function(err) {
                  expect(err.code).toBe(403);
                  findMatch(readMatchkey(x, 1)).then(function(m) {
                    expect(m.length).toBe(1);

                    // request join to full, host disband success
                    generateUser({}).then(function(u) {
                      joinMatch(readMatchkey(x, 1), u.email, u.key).then(function(m) { 
                        disband(readMatchkey(x, 1), readHostemail(x, 1), readHostkey(x, 1)).then(function(d) {

                          // approve full, host disband fail 
                          generateUser({}).then(function(u) {
                            joinMatch(readMatchkey(x, 2), u.email, u.key).then(function(j) { 
                              approveUser(readMatchkey(x, 2), readHostemail(x, 2), readHostkey(x, 2), j.players[1].playerkey).then(function(m) { 
                                disband(readMatchkey(x, 2), readHostemail(x, 2), readHostkey(x, 2)).then((x) => {                                                    
                                  expect(x.players.length).toBe(2);
                                  done();
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                }); 
              }); 
            }); 
          });
        });
      });
    });
  });

	it('readHostKey', function(done) {
	  expect(testMatches.readFirstkey('a', [{profile:{a:'x'}}])).toBe('x');
	  expect(testMatches.readFirstkey('a', [{profile:{a:'y'}}])).toBe('y');
	  expect(testMatches.readFirstkey('b', [{profile:{b:'x'}}])).toBe('x');
		done();
	});

	it('createUserObj', function(done) {
		expect(R.omit(['playerkey'], testMatches.createUserObj('abc', [{email:'em', profile:{avataricon:'ai', name:'nm'}}]))).toEqual({ email : 'em', type : 'abc', avataricon : 'ai', name : 'nm' });
		expect(R.omit(['playerkey'], testMatches.createUserObj('key', [{email:'em2', profile:{avataricon:'ai2', name:'name'}}]))).toEqual({ email : 'em2', type : 'key', avataricon : 'ai2', name : 'name' });
		done();
	});

	it('unlessAutoAcceptRequest', function(done) {
		expect(testMatches.unlessAutoAcceptRequest(false)).toBe('request');
		expect(testMatches.unlessAutoAcceptRequest(true)).toBe('approve');
		done();
	});


  it('JoinMatch.autoaccept', function(done) {
    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 

    const seatAvailable = prop('seatavailable');
    const errorCode = prop('code');

    const checkPlayerCount = compose(length, prop('players'));

    const joinMatch = (matchkey) => testMatches.JoinMatch(testAccount, testMatch, matchkey);

    const userJoinMatch = R.curry((matchkey, user) => R.converge(joinMatch(matchkey), [prop('email'), prop('key')])(user));

    const interestedInMatch = (matchkey) => testMatches.interestedInMatch(testAccount, testMatch, matchkey);
    const userInterestedInMatch = R.curry((matchkey, user) => converge(interestedInMatch(matchkey), [prop('email'), prop('key')])(user));

    Promise.all([generateHostUser({}), generateUser({tickets:5}), generateUser({}), generateUser({})]).then(function(u) {

      var matchA = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 3, 2, 'draft', {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[3].id, u[3].name, u[3].thumbnail, 'description', 30, 60, 1, 2]), false);

      Promise.all([matchA]).then(function(m) {
				testMatch.findOneAndUpdate({key: m[0].key}, {autoaccept: true}, {new:true}).then(function(updatematch) {

					// nok: host tries to join his own game
					userJoinMatch(m[0].key, u[0]).then().catch(function(err) {
						expect(errorCode(err)).toBe(409);

						// nok:  user tries to join game with invalid key
						joinMatch(m[0].key)(u[1].email, 'invalidkey').then().catch(function(err) {
							expect(errorCode(err)).toBe(401);

							// ok:  user tries to interested game
							userInterestedInMatch(m[0].key, u[1]).then(function(game) {

								// ok:  interested user tries to join game
								userJoinMatch(m[0].key, u[1]).then(function(game) {

									// -> user count increases
									expect(checkPlayerCount(game)).toBe(2);
									expect(seatAvailable(game)).toBe(m[0].seatavailable - 1);       // available seats on decreases if people are accepted
									expect(game.players[1].type).toBe('approve');               // people who request to join enter the request state 

									// ok:  new user tries to join game
									userJoinMatch(m[0].key, u[2]).then(function(game) {
										expect(checkPlayerCount(game)).toBe(3);
										expect(seatAvailable(game)).toBe(m[0].seatavailable - 2);   // available seats on decreases if people are accepted
										expect(game.players[2].type).toBe('approve');               // people who request to join enter the request state 
										done();

									});
								});
							});
						});
					});
				});
      });
    });
  });


  it('JoinMatch', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var status = ['draft'];

    const seatAvailable = prop('seatavailable');
    const errorCode = prop('code');

    const readHostkeys = (keyname, obj) => compose(map(prop(keyname)), prop('players'))(obj);
    const checkPlayerCount = compose(length, prop('players'));

    const joinMatch = (matchkey) => testMatches.JoinMatch(testAccount, testMatch, matchkey);

    const userJoinMatch = R.curry((matchkey, user) => R.converge(joinMatch(matchkey), [prop('email'), prop('key')])(user));


    const interestedInMatch = (matchkey) => testMatches.interestedInMatch(testAccount, testMatch, matchkey);
    const userInterestedInMatch = R.curry((matchkey, user) => converge(interestedInMatch(matchkey), [prop('email'), prop('key')])(user));

    const findLast = compose(head, filter(pipe(prop('players'), length, equals(7))));
    const findPlayerField = curry((gameObject, field, email) => compose(prop(field), head, filter(pipe(prop('email'), equals(email))), prop('players'))(gameObject));
    const findLastPlayerProperty = curry((game, property, email) => findPlayerField(findLast(game), property, email));

    const hasPlayerKey = compose(has('playerkey'), JSON.parse, JSON.stringify);
    const readUser = composeP(head, testAccounts.ReadAccountFromEmail(testAccount));

    Promise.all([generateHostUser({}), generateUser({tickets:5}), generateUser({}), generateHostUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateGame]).then(function(u) {

      var matchA = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 3, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[8].id, u[8].name, u[8].thumbnail, 'description', 30, 60, 1, 2]), false);
      var matchB = buildHost(testMatch, testAccount, u[3].email, u[3].key, gameDate, 4, 2, status[0], {}, u[3].hostareas[0].name, u[3].hostareas[0].loc.coordinates, createMatchObj([u[8].id, u[8].name, u[8].thumbnail, 'description', 30, 60, 1, 2]), false);
      var matchC = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 4, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[8].id, u[8].name, u[8].thumbnail, 'description', 30, 60, 1, 2]), true);

      var matchD = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 3, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[8].id, u[8].name, u[8].thumbnail, 'description', 30, 60, 1, 2]), false);

      Promise.all([matchA, matchB, matchC, matchD]).then(function(m) {

        // nok: host tries to join his own game
        userJoinMatch(m[0].key, u[0]).then().catch(function(err) {
          expect(errorCode(err)).toBe(409);

          // nok:  user tries to join game with invalid key
          joinMatch(m[0].key)(u[1].email, 'invalidkey').then().catch(function(err) {
            expect(errorCode(err)).toBe(401);

            // nok: valid user tries to join a private game (when not invited) 
            userJoinMatch(m[2].key, u[1]).then(function(game) {
							expect(game.players.length).toBe(2); 
							expect(game.players[1].name).toBe(u[1].profile.name); 
							expect(game.players[1].avataricon).toBe(u[1].profile.avataricon); 

							// ok:  user tries to interested game
							userInterestedInMatch(m[0].key, u[1]).then(function(game) {
								// ok:  user tries to join game
								userJoinMatch(m[0].key, u[1]).then(function(game) {
									// -> user count increases
									expect(checkPlayerCount(game)).toBe(2);
									expect(seatAvailable(game)).toBe(m[0].seatavailable);       // available seats on decreases if people are accepted

									expect(game.players[1].type).toBe('request');               // people who request to join enter the request state 
									expect(readHostkeys('avataricon', game)[0]).toBe(u[0].profile.avataricon);
									expect(readHostkeys('avataricon', game)[1]).toBe(u[1].profile.avataricon);
									expect(readHostkeys('name', game)[0]).toBe(u[0].profile.name);
									expect(readHostkeys('name', game)[1]).toBe(u[1].profile.name);

									readUser(u[1].email).then(function(user) {

										// nok: user tries to join a second time 
										userJoinMatch(m[0].key, u[1]).then().catch(function(err) {
											expect(errorCode(err)).toBe(409);

											// nok: user tries to join a game that does not exist 
											userJoinMatch('badkey', u[2]).then().catch(function(err) {
												expect(errorCode(err)).toBe(404);

												// ok:  user tries to join game, fills up requests 
												Promise.all(R.map(userJoinMatch(m[0].key), [u[2], u[4], u[5], u[6], u[7]])).then(function(game) {
													expect(checkPlayerCount(findLast(game))).toBe(7);
													expect(seatAvailable(findLast(game))).toBe(2);

													expect(findLastPlayerProperty(game, 'avataricon', u[2].email)).toBe(u[2].profile.avataricon);
													expect(findLastPlayerProperty(game, 'type', u[7].email)).toBe('request');

													expect(hasPlayerKey(game[0].players[1])).toBe(true);
													expect(hasPlayerKey(game[0].players[2])).toBe(true);
													expect(game[0].players[1].playerkey).not.toBe(game[0].players[2].playerkey);

													readUser(u[7].email).then(function(user) {

														// (ok) reject user
														testMatches.RejectUser(testAccount, testMatch, m[0].key, u[0].email, u[0].key, findLastPlayerProperty(game, 'playerkey', u[7].email)).then(function(x) {

															expect(findPlayerField(x, 'type', u[7].email)).toBe('reject'); 
															expect(x.seatavailable).toBe(2);
															expect(x.status).toBe('open');

															readUser(u[7].email).then(function(user) {
																expect(prop('tickets', user)).toBe(5);
																expect(prop('ticketshold', user)).toBe(0);

																// (nok) join users but requesting user is not host 
																testMatches.ApproveUser(testAccount, testMatch, m[0].key, u[4].email, u[4].key, findLastPlayerProperty(game, 'playerkey', u[6].email)).then().catch(function(err) {
																	expect(err.code).toBe(403);

																	// (ok) join users to full
																	testMatches.ApproveUser(testAccount, testMatch, m[0].key, u[0].email, u[0].key, findLastPlayerProperty(game, 'playerkey', u[6].email)).then(function(x) {
																		expect(findPlayerField(x, 'type', u[6].email)).toBe('approve'); 
																		expect(x.seatavailable).toBe(1);
																		expect(x.status).toBe('open');

																		// (ok) join users to full
																		testMatches.ApproveUser(testAccount, testMatch, m[0].key, u[0].email, u[0].key, findLastPlayerProperty(game, 'playerkey', u[5].email)).then(function(x) {
																			expect(findPlayerField(x, 'type', u[5].email)).toBe('approve'); 
																			expect(x.seatavailable).toBe(0);
																			expect(x.isfull).toBe(true);

																			testMatches.ApproveUser(testAccount, testMatch, m[0].key, u[0].email, u[0].key, findLastPlayerProperty(game, 'playerkey', u[4].email)).then().catch(function(err) {
																				expect(errorCode(err)).toBe(422);
																				done();

																			});
																		});
																	});
																});
															});
														});
													});
												});
											});
										});
									});
								});
							});

            });
          });
        });
      });
    });
  });


  it('RejectUser', function(done) {

    var findPlayerType = R.curry((req, keyname, value) => R.compose(R.prop('type'), R.head, R.filter(R.pipe(R.prop(keyname), R.equals(value))), R.prop('players'))(req));
    var status = ['open', 'private'];

    Promise.all([generateMatch(status[0], 3, 2), generateUser({ticketshold:5}), generateUser({ticketshold:5})]).then(function(u) {

      var m = u[0][1];

      var errorCode = R.prop('code');
      var hostRejectUser = testMatches.RejectUser(testAccount, testMatch, m.key, u[0][0].email, u[0][0].key);

      var joinMatch1 = testMatches.JoinMatch(testAccount, testMatch, m.key);
      var userJoinMatch1 = R.converge(joinMatch1, [R.prop('email'), R.prop('key')]);
      Promise.all(R.map(userJoinMatch1, [u[1], u[2]])).then(function(games) {

        var updatePlayerMatch = testMatches.UpdateMatchPlayerType(testMatch, m.key);
        var setStatus = [
          updatePlayerMatch(u[1].email, 'request'),
          updatePlayerMatch(u[2].email, 'reject'),
        ]; 

        var readMatchTypes = R.composeP(R.map(R.prop('type')), R.prop('players'), R.head, testMatches.FindMatchByKey(testMatch));

        Promise.all(setStatus).then(function(n) {
          var rejectType = R.lift((type, matchkey) => R.composeP(hostRejectUser, R.prop('playerkey'), R.head, R.filter(R.pipe(R.prop('type'), R.equals(type))), R.prop('players'), R.head, testMatches.FindMatchByKey(testMatch))(matchkey));

          testMatches.RejectUser(testAccount, testMatch, n[0].key, u[1].email, u[1].key, n[0].players[1].playerkey).then().catch(function(err) {
            expect(err.code).toBe(403);

            Promise.all(rejectType(['request', 'reject'], [m.key])).then(function(s) {
              expect(findPlayerType(s[0], 'email', u[1].email)).toBe('reject');
              expect(findPlayerType(s[1], 'email', u[2].email)).toBe('reject');
              done();

            });
          });
        });
      });
    });
  });


  it('AcceptInvite', function(done) {

    var findPlayerType = testMatches.FindPlayerType;

    Promise.all([generateMatch('open', 3, 2), generateUser({}), generateUser({}), generateMatch('open', 3, 2)]).then(function(u) {

      var host = u[0][0];
      var m = u[0][1];

      var host2 = u[3][0];
      var m2 = u[3][1];

      var errorCode = R.prop('code');
      var userAcceptInvite = testMatches.AcceptInvite(testAccount, testMatch, m.key);
      var userAcceptInvite2 = testMatches.acceptInviteWithPlayerKey(testAccount, testMatch, m2.key, 'testdata_name');

      var inviteMatch1 = testMatches.InviteMatch(testAccount, testMatch, host.email, host.key, m.key);
      var inviteMatch2 = testMatches.InviteMatch(testAccount, testMatch, host2.email, host2.key, m2.key);

      Promise.all(R.map(inviteMatch1, R.map(R.prop('email'), [u[1], u[2]]))).then(function(games) {
        var updatePlayerMatch = testMatches.UpdateMatchPlayerType(testMatch, m.key);
        var setStatus = [
          updatePlayerMatch(u[1].email, 'invite'),
          updatePlayerMatch(u[2].email, 'approve'),
        ]; 

        Promise.all(setStatus).then(function(n) {
          testMatches.AcceptInvite(testAccount, testMatch, m.key, host.email, host.key).then().catch(function(err) {
            expect(err.code).toBe(403);
            var acceptInvUser = R.converge(userAcceptInvite, [R.prop('email'), R.prop('key')]);
            Promise.all(R.map(acceptInvUser, [u[1], u[2]])).then(function(s) {
              expect(findPlayerType(s[0], 'email', u[1].email)).toBe('approve');
              expect(findPlayerType(s[1], 'email', u[2].email)).toBe('approve');

              Promise.all(R.map(inviteMatch2, R.map(R.prop('email'), [u[1], u[2]]))).then(function(games2) {

                const getInvitedEmail = (x) => compose(R.prop('email'), R.nth(1), R.prop('players'), R.last);
                const getInvitedPlayer = (x) => compose(R.prop('playerkey'), R.nth(1), R.prop('players'), R.last);

                const acceptInvLoggedOutUser = R.converge(userAcceptInvite2, [getInvitedEmail(1), getInvitedPlayer(1)]);

                acceptInvLoggedOutUser(games2).then((x) => {

                  expect(x.players[1].type).toBe('approve');

                  testMatches.acceptInviteWithPlayerKey(testAccount, testMatch, x.key, 'name_x', x.players[2].email, 'badkey').then().catch((badkey) => {
                    expect(badkey).toBe('badkey');
                    done();
                  })
                }).catch((err) => {
                  log.clos('err', err);
                });

              });
            });
          });
        });
      });
    });
  });

  it('DeclineInvite', function(done) {

    var findPlayerType = testMatches.FindPlayerType;

    Promise.all([generateMatch('open', 3, 2), generateUser({}), generateUser({})]).then(function(u) {

      var host = u[0][0];
      var m = u[0][1];

      var errorCode = R.prop('code');
      var userDeclineInvite = testMatches.DeclineInvite(testAccount, testMatch, m.key);
      var userInterested = testMatches.interestedInMatch(testAccount, testMatch, m.key);
      var inviteMatch1 = testMatches.InviteMatch(testAccount, testMatch, host.email, host.key, m.key);

      Promise.all(R.map(inviteMatch1, R.map(R.prop('email'), [u[1], u[2]]))).then(function(games) {

        var updatePlayerMatch = testMatches.UpdateMatchPlayerType(testMatch, m.key);
        var setStatus = [
          updatePlayerMatch(u[1].email, 'invite'),
          updatePlayerMatch(u[2].email, 'decline'),
        ]; 

        Promise.all(setStatus).then(function(n) {
          testMatches.DeclineInvite(testAccount, testMatch, m.key, host.email, host.key).then().catch(function(err) {

            expect(err.code).toBe(403);
            var declineInvUser = R.converge(userDeclineInvite, [R.prop('email'), R.prop('key')]);
            var interestUser = R.converge(userInterested, [R.prop('email'), R.prop('key')]);

            Promise.all(R.map(declineInvUser, [u[1], u[2]])).then(function(s) {
              expect(findPlayerType(s[0], 'email', u[1].email)).toBe('decline');
              expect(findPlayerType(s[1], 'email', u[2].email)).toBe('decline');

              Promise.all(R.map(interestUser, [u[1], u[2]])).then(function(s) {
                expect(findPlayerType(s[0], 'email', u[1].email)).toBe('interested');
                expect(findPlayerType(s[1], 'email', u[2].email)).toBe('interested');
                done();
              });

            });
          });
        });
      });
    });
  });

  it('whenPlayerIsType', function(done) {
    expect(testMatches.whenPlayerIsType({}, 'key', 'type')).toBe(false);
    expect(testMatches.whenPlayerIsType({players:[]}, 'key', 'type')).toBe(false);
    expect(testMatches.whenPlayerIsType({players:[{playerkey:'key'}]}, 'key', 'type')).toBe(false);
    expect(testMatches.whenPlayerIsType({players:[{playerkey:'key', type:'type'}]}, 'key', 'type')).toBe(true);
    done();
  });


  it('PlayerChange', function(done) {

    var findPlayerType = testMatches.FindPlayerType;
    var findPlayerkey = testMatches.FindPlayerkey;

    Promise.all([generateMatch('open', 3, 2), generateUser({}), generateUser({}), generateUser({}), generateUser({})]).then(function(u) {

      var host = u[0][0];
      var m = u[0][1];

      var errorCode = R.prop('code');
      var inviteMatch1 = testMatches.InviteMatch(testAccount, testMatch, host.email, host.key, m.key);
      Promise.all(R.map(inviteMatch1, R.map(R.prop('email'), [u[1], u[2], u[3], u[4]]))).then(function(games) {

        var updatePlayerMatch = testMatches.UpdateMatchPlayerType(testMatch, m.key);
        var setStatus = [
          updatePlayerMatch(u[1].email, 'invite'),
          updatePlayerMatch(u[2].email, 'request'),
          updatePlayerMatch(u[3].email, 'invite'),
        ]; 

        Promise.all(setStatus).then(function(n) {

          testMatches.PlayerChange(testMatch, n[0], findPlayerkey(n[1], 'email', host.email), ['request'], 'accept').then().catch(function(err) {
            expect(err.code).toBe(422);

            testMatches.PlayerChange(testMatch, n[0], findPlayerkey(n[1], 'email', u[1].email), ['invite'], 'approve').then(function(m) {
              expect(findPlayerType(m, 'email', u[1].email)).toBe('approve');
              testMatches.PlayerChange(testMatch, m, findPlayerkey(n[1], 'email', u[3].email), ['invite'], 'reject').then(function(m) {
                expect(findPlayerType(m, 'email', u[3].email)).toBe('reject');
                testMatches.PlayerChange(testMatch, n[0], findPlayerkey(n[1], 'email', u[1].email), ['invite'], 'approve').then(function(m) {
                  expect(findPlayerType(m, 'email', u[1].email)).toBe('approve');
                  testMatches.PlayerChange(testMatch, n[1], findPlayerkey(n[1], 'email', u[1].email), ['complete'], 'reject').then().catch(function(err) {
                    expect(err.code).toBe(422);
                    testMatches.PlayerChange(testMatch, n[1], 'notakey', ['request'], 'accept').then().catch(function(err) {
                      expect(err.code).toBe(409);
                      done();
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });

  it('ApproveUser', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var status = ['open', 'private'];

    var seatAvailable = R.compose(R.prop('seatavailable'));

    Promise.all([generateMatch(status[0], 3, 2), generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({})]).then(function(u) {

      var m = u[0][1];

      var errorCode = R.prop('code');
      var checkPlayerCount = R.compose(R.length, R.prop('players'));
      var hostApproveUser = testMatches.ApproveUser(testAccount, testMatch, m.key, u[0][0].email, u[0][0].key);

      var joinMatch1 = testMatches.JoinMatch(testAccount, testMatch, m.key);
      var userJoinMatch1 = R.converge(joinMatch1, [R.prop('email'), R.prop('key')]);
      
      Promise.all(R.map(userJoinMatch1, [u[1], u[2], u[3], u[4], u[5], u[6], u[7]])).then(function(games) {

        var updatePlayerMatch = testMatches.UpdateMatchPlayerType(testMatch, m.key);
        var setStatus = [
          updatePlayerMatch(u[1].email, 'invite'),
          updatePlayerMatch(u[2].email, 'request'),
          updatePlayerMatch(u[3].email, 'approve'),
          updatePlayerMatch(u[4].email, 'reject'),
          updatePlayerMatch(u[5].email, 'complete'),
          updatePlayerMatch(u[6].email, 'absent'),
          updatePlayerMatch(u[7].email, 'review')
        ]; 

        var readMatchTypes = R.composeP(R.map(R.prop('type')), R.prop('players'), R.head, testMatches.FindMatchByKey(testMatch));

        Promise.all(setStatus).then(function(n) {

          var approveType = R.lift((type, matchkey) => R.composeP(
            hostApproveUser, 
            R.prop('playerkey'), 
            R.head, 
            R.filter(R.pipe(R.prop('type'), R.equals(type))), 
            R.prop('players'), 
            R.head, 
            testMatches.FindMatchByKey(testMatch)
          )(matchkey));

          Promise.all(approveType(['request', 'approve'], [m.key])).then(function(s) {
            expect(s.length).toBe(2);
            readMatchTypes(m.key).then(function(t) {
              expect(R.filter(R.equals('approve'), t).length).toBe(2);
              Promise.all(approveType(['invite', 'reject', 'complete', 'absent', 'review'], [m.key])).then().catch(function(err) {
                expect(errorCode(err)).toBe(422);
                done();
              });
            });
          });
        });
      });
    });
  });

	it('buildMatchUrl', function(done) {
		expect(testMatches.buildMatchUrl('a')).toBe('/events/a');
		expect(testMatches.buildMatchUrl('xyz')).toBe('/events/xyz');
		done();
	});

	it('userObj', function(done) {
		expect(R.omit(['playerkey'],testMatches.userObj('invite', 'A', Maybe.of({})))).toEqual({ email : 'a', type : 'invite', avataricon : '/img/default-avatar-sq.jpg', name : 'a' });
		expect(R.omit(['playerkey'],testMatches.userObj('standby', 'B', Maybe.of({})))).toEqual({ email : 'b', type : 'standby', avataricon : '/img/default-avatar-sq.jpg', name : 'b' });
		expect(R.omit(['playerkey'],testMatches.userObj('invite', 'A', Maybe.of({profile:{name:'xyz', avataricon:'ai'}})))).toEqual({ email : 'a', type : 'invite', avataricon : 'ai', name : 'xyz' });
		done();
	});

  it('StandbyMatch', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var status = ['open'];

    var seatAvailable = R.compose(R.prop('seatavailable'));
    var readAvatar = R.compose(R.map(R.prop('avataricon')), R.prop('players'));

    Promise.all([generateHostUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateGame]).then(function(u) {

      var openMatch = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 2, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[5].id, u[5].name, u[5].thumbnail, 'description', 30, 60, 1, 2]), false);

      var privateMatch = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 2, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[5].id, u[5].name, u[5].thumbnail, 'description', 30, 60, 1, 2]), true);

      var fullMatch = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 2, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[5].id, u[5].name, u[5].thumbnail, 'description', 30, 60, 1, 2]), true);

      Promise.all([openMatch, privateMatch, fullMatch]).then(function(m) {

        var errorCode = R.prop('code');
        var checkPlayerCount = R.compose(R.length, R.prop('players'));
        var standbyMatch = testMatches.addStandbyForMatch(testAccount, testMatch);

        // ok: user is invited game
        standbyMatch(m[0].key, u[1].email).then(function(game) {

          // readStandbyUsers :: [o] -> o
          const readStandbyUsers = R.compose(
            R.filter(R.pipe(R.prop('type'), R.equals('standby'))), 
            R.prop('players')
          );

          expect(readStandbyUsers(game).length).toBe(1);
          expect(readStandbyUsers(game)[0].email).toBe(u[1].email);
          expect(readStandbyUsers(game)[0].avataricon).toBe(u[1].profile.avataricon);
          expect(checkPlayerCount(game)).toBe(2);
          expect(seatAvailable(game)).toBe(1);
          expect(readAvatar(game)[0]).toBe(u[0].profile.avataricon);

          // ok: invited user invited a second time 
          standbyMatch(m[0].key, game.players[1].playerkey).then(function(newok) {

            // nok: user invited to join a game that does not exist 
            standbyMatch('abcdef', u[2].email).then().catch(function(err) {
              expect(errorCode(err)).toBe(403);

              // ok:  user invites exceed game player count
              var standbyUsers = [
                standbyMatch(m[0].key, u[2].email), 
                standbyMatch(m[0].key, u[3].email), 
                standbyMatch(m[0].key, u[4].email) 
              ];

              Promise.all(standbyUsers).then(function(games) {

                var findLast = R.compose(R.head, R.filter(R.pipe(R.prop('players'), R.length, R.equals(5))));
                expect(readStandbyUsers(findLast(games)).length).toBe(4);
                expect(checkPlayerCount(findLast(games))).toBe(5);
                expect(seatAvailable(findLast(games))).toBe(1);

                var hasPlayerKey = R.compose(R.has('playerkey'), JSON.parse, JSON.stringify);
                expect(hasPlayerKey(findLast(games).players[1])).toBe(true);
                expect(hasPlayerKey(findLast(games).players[2])).toBe(true);
                expect(findLast(games).players[1].playerkey).not.toBe(findLast(games).players[2].playerkey);

                // ok:  user can be invited to private match
                standbyMatch(m[1].key, u[1].email).then(function(game) {
                  // expect(readStandbyUsers(game).length).toBe(1);
                  // nok: user cannot be invited to a full match
                  // - need accept function to run this test
                  done()
                });

              });
            });
          });
        }); 

      });
    });
  });

  it('standbyMatchByKey', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var status = ['open'];

    var seatAvailable = R.compose(R.prop('seatavailable'));
    var readAvatar = R.compose(R.map(R.prop('avataricon')), R.prop('players'));

    var inviteMatch = testMatches.InviteMatch(testAccount, testMatch);

    Promise.all([generateHostUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateGame]).then(function(u) {

      var openMatch = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 2, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[5].id, u[5].name, u[5].thumbnail, 'description', 30, 60, 1, 2]), false);

      Promise.all([openMatch]).then(function(m) {

        var errorCode = R.prop('code');
        var checkPlayerCount = R.compose(R.length, R.prop('players'));
        var standbyMatchByKey = testMatches.standbyMatchByKey(testAccount, testMatch);

        // nok:  user tries to join game with invalid key
        standbyMatchByKey(u[0].email, 'invalidkey', m[0].key, u[1].pageslug).then().catch(function(err) {
          expect(errorCode(err)).toBe(401); 

          // nok:  non host user tries to invite a person to the match
          standbyMatchByKey(u[2].email, u[2].key, m[0].key, u[1].pageslug).then().catch(function(err) {
            expect(errorCode(err)).toBe(403); 

            // invite a user
            inviteMatch(u[0].email, u[0].key, m[0].key, u[1].email).then(function(ma) {

              // ok: inivted user is changed to standby for game
              standbyMatchByKey(u[0].email, u[0].key, m[0].key, ma.players[1].playerkey).then(function(game) {

                var readStandbyUsers = R.compose(R.filter(R.pipe(R.prop('type'), R.equals('standby'))), R.prop('players'));

                expect(readStandbyUsers(game).length).toBe(1);
                expect(readStandbyUsers(game)[0].email).toBe(u[1].email);
                expect(readStandbyUsers(game)[0].type).toBe('standby');
                expect(readStandbyUsers(game)[0].avataricon).toBe(u[1].profile.avataricon);
                expect(checkPlayerCount(game)).toBe(2);
                expect(seatAvailable(game)).toBe(1);
                expect(readAvatar(game)[0]).toBe(u[0].profile.avataricon);

                // nok: user invited to join a game that does not exist 
                standbyMatchByKey(u[0].email, u[0].key, 'abcdef', u[2].pageslug).then().catch(function(err) {
                  expect(errorCode(err)).toBe(403);

                  // ok:  user invites exceed game player count
                  var standbyUsers = [
                    standbyMatchByKey(u[0].email, u[0].key, m[0].key, u[2].pageslug), 
                    standbyMatchByKey(u[0].email, u[0].key, m[0].key, u[3].pageslug), 
                    standbyMatchByKey(u[0].email, u[0].key, m[0].key, u[4].pageslug) 
                  ];

                  Promise.all(standbyUsers).then(function(games) {

                    var findLast = R.compose(
                      R.head, 
                      R.filter(R.pipe(R.prop('players'), 
                      R.length, 
                      R.equals(5)))
                    );

                    expect(readStandbyUsers(findLast(games)).length).toBe(4);
                    expect(checkPlayerCount(findLast(games))).toBe(5);
                    expect(seatAvailable(findLast(games))).toBe(1);

                    var hasPlayerKey = R.compose(R.has('playerkey'), JSON.parse, JSON.stringify);
                    expect(hasPlayerKey(findLast(games).players[1])).toBe(true);
                    expect(hasPlayerKey(findLast(games).players[2])).toBe(true);
                    expect(findLast(games).players[1].playerkey).not.toBe(findLast(games).players[2].playerkey);
                    done()

                  });
                });
              });

            }); 
          }); 
        }); 
      });
    });
  });


  it('InviteMatch', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var status = ['open'];

    var seatAvailable = R.compose(R.prop('seatavailable'));
    var readAvatar = R.compose(R.map(R.prop('avataricon')), R.prop('players'));

    Promise.all([generateHostUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateGame]).then(function(u) {

      var openMatch = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 2, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[5].id, u[5].name, u[5].thumbnail, 'description', 30, 60, 1, 2]), false);

      var privateMatch = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 2, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[5].id, u[5].name, u[5].thumbnail, 'description', 30, 60, 1, 2]), true);

      Promise.all([openMatch, privateMatch]).then(function(m) {

        var errorCode = R.prop('code');
        var checkPlayerCount = R.compose(R.length, R.prop('players'));
        var inviteMatch = testMatches.InviteMatch(testAccount, testMatch);

        // nok:  user tries to join game with invalid key
        inviteMatch(u[0].email, 'invalidkey', m[0].key, u[1].email).then().catch(function(err) {
          expect(errorCode(err)).toBe(401); 

          // nok:  non host user tries to invite a person to the match
          inviteMatch(u[2].email, u[2].key, m[0].key, u[1].email).then().catch(function(err) {
            expect(errorCode(err)).toBe(403); 

            // ok: user is invited game
            inviteMatch(u[0].email, u[0].key, m[0].key, u[1].email).then(function(game) {
              var readInvitedUsers = R.compose(
                R.filter(R.pipe(R.prop('type'), R.equals('invite'))), 
                R.prop('players')
              );

              expect(readInvitedUsers(game).length).toBe(1);
              expect(readInvitedUsers(game)[0].email).toBe(u[1].email);
              expect(readInvitedUsers(game)[0].avataricon).toBe(u[1].profile.avataricon);
              expect(checkPlayerCount(game)).toBe(2);
              expect(seatAvailable(game)).toBe(1);
              expect(readAvatar(game)[0]).toBe(u[0].profile.avataricon);

              // nok: user invited to join a game that does not exist 
              inviteMatch(u[0].email, u[0].key, 'abcdef', u[2].email).then().catch(function(err) {
                expect(errorCode(err)).toBe(403);

                // ok:  user invites exceed game player count
                var inviteUsers = [
                  inviteMatch(u[0].email, u[0].key, m[0].key, u[2].email), 
                  inviteMatch(u[0].email, u[0].key, m[0].key, u[3].email), 
                  inviteMatch(u[0].email, u[0].key, m[0].key, u[4].email) 
                ];

                Promise.all(inviteUsers).then(function(games) {
                  var findLast = R.compose(R.head, R.filter(R.pipe(R.prop('players'), R.length, R.equals(5))));
                  expect(readInvitedUsers(findLast(games)).length).toBe(4);
                  expect(checkPlayerCount(findLast(games))).toBe(5);
                  expect(seatAvailable(findLast(games))).toBe(1);

                  var hasPlayerKey = R.compose(R.has('playerkey'), JSON.parse, JSON.stringify);
                  expect(hasPlayerKey(findLast(games).players[1])).toBe(true);
                  expect(hasPlayerKey(findLast(games).players[2])).toBe(true);
                  expect(findLast(games).players[1].playerkey).not.toBe(findLast(games).players[2].playerkey);

                  // ok:  user can be invited to private match
                  inviteMatch(u[0].email, u[0].key, m[1].key, u[1].email).then(function(game) {

                    expect(readInvitedUsers(game).length).toBe(1);
                    done()

                  });
                });
              });
            }); 
          }); 
        }); 
      });
    });
  });

  it('InviteMatchByKey', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var status = ['open'];

    var seatAvailable = R.compose(R.prop('seatavailable'));
    var readAvatar = R.compose(R.map(R.prop('avataricon')), R.prop('players'));

    Promise.all([generateHostUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateGame]).then(function(u) {

      var openMatch = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 2, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[5].id, u[5].name, u[5].thumbnail, 'description', 30, 60, 1, 2]), false);

      var privateMatch = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 2, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[5].id, u[5].name, u[5].thumbnail, 'description', 30, 60, 1, 2]), true);

      var fullMatch = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 2, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[5].id, u[5].name, u[5].thumbnail, 'description', 30, 60, 1, 2]), true);

      Promise.all([openMatch, privateMatch, fullMatch]).then(function(m) {

        var errorCode = R.prop('code');
        var checkPlayerCount = R.compose(R.length, R.prop('players'));
        var inviteMatchByKey = testMatches.InviteMatchByKey(testAccount, testMatch);

        // nok:  user tries to join game with invalid key
        inviteMatchByKey(u[0].email, 'invalidkey', m[0].key, u[1].pageslug).then().catch(function(err) {
          expect(errorCode(err)).toBe(401); 

          // nok:  non host user tries to invite a person to the match
          inviteMatchByKey(u[2].email, u[2].key, m[0].key, u[1].pageslug).then().catch(function(err) {
            expect(errorCode(err)).toBe(403); 

            // ok: user is invited game
            inviteMatchByKey(u[0].email, u[0].key, m[0].key, u[1].pageslug).then(function(game) {
              var readInvitedUsers = R.compose(R.filter(R.pipe(R.prop('type'), R.equals('invite'))), R.prop('players'));

              expect(readInvitedUsers(game).length).toBe(1);
              expect(readInvitedUsers(game)[0].email).toBe(u[1].email);
              expect(readInvitedUsers(game)[0].avataricon).toBe(u[1].profile.avataricon);
              expect(checkPlayerCount(game)).toBe(2);
              expect(seatAvailable(game)).toBe(1);
              expect(readAvatar(game)[0]).toBe(u[0].profile.avataricon);

              // nok: user invited to join a game that does not exist 
              inviteMatchByKey(u[0].email, u[0].key, 'abcdef', u[2].pageslug).then().catch(function(err) {
                expect(errorCode(err)).toBe(403);

                // ok:  user invites exceed game player count
                var inviteUsers = [
                  inviteMatchByKey(u[0].email, u[0].key, m[0].key, u[2].pageslug), 
                  inviteMatchByKey(u[0].email, u[0].key, m[0].key, u[3].pageslug), 
                  inviteMatchByKey(u[0].email, u[0].key, m[0].key, u[4].pageslug) 
                ];

                Promise.all(inviteUsers).then(function(games) {

                  var findLast = R.compose(R.head, R.filter(R.pipe(R.prop('players'), R.length, R.equals(5))));
                  expect(readInvitedUsers(findLast(games)).length).toBe(4);
                  expect(checkPlayerCount(findLast(games))).toBe(5);
                  expect(seatAvailable(findLast(games))).toBe(1);

                  var hasPlayerKey = R.compose(R.has('playerkey'), JSON.parse, JSON.stringify);
                  expect(hasPlayerKey(findLast(games).players[1])).toBe(true);
                  expect(hasPlayerKey(findLast(games).players[2])).toBe(true);
                  expect(findLast(games).players[1].playerkey).not.toBe(findLast(games).players[2].playerkey);

                  // ok:  user can be invited to private match
                  inviteMatchByKey(u[0].email, u[0].key, m[1].key, u[1].pageslug).then(function(game) {

                    expect(readInvitedUsers(game).length).toBe(1);
                    done();

                  });
                });
              });
            }); 
          }); 
        }); 
      });
    });
  });


  it('InviteMatchByPlayerKey', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var status = ['open'];

    var seatAvailable = R.compose(R.prop('seatavailable'));
    var readAvatar = R.compose(R.map(R.prop('avataricon')), R.prop('players'));

    Promise.all([generateHostUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateGame]).then(function(u) {

      var openMatch = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 2, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[5].id, u[5].name, u[5].thumbnail, 'description', 30, 60, 1, 2]), false);

      var openMatchB = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 2, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, createMatchObj([u[5].id, u[5].name, u[5].thumbnail, 'description', 30, 60, 1, 2]), false);

      Promise.all([openMatch, openMatchB]).then(function(m) {

        var errorCode = R.prop('code');
        var checkPlayerCount = R.compose(R.length, R.prop('players'));
        var standbyForMatch = testMatches.addStandbyForMatch(testAccount, testMatch);
        var interestedInMatch = testMatches.interestedInMatch(testAccount, testMatch);
        var inviteMatchByPlayerKey = testMatches.InviteMatchByPlayerKey(testAccount, testMatch);

				interestedInMatch(m[0].key, u[1].email, u[1].key).then((xz) => {

					// ok: user is invited game
					inviteMatchByPlayerKey(u[0].email, u[0].key, m[0].key, xz.players[1].playerkey).then(function(game) {
						var readInvitedUsers = R.compose(R.filter(R.pipe(R.prop('type'), R.equals('invite'))), R.prop('players'));

						expect(readInvitedUsers(game).length).toBe(1);
						expect(readInvitedUsers(game)[0].email).toBe(u[1].email);
						expect(readInvitedUsers(game)[0].avataricon).toBe(u[1].profile.avataricon);
						expect(checkPlayerCount(game)).toBe(2);
						expect(seatAvailable(game)).toBe(1);
						expect(readAvatar(game)[0]).toBe(u[0].profile.avataricon);

            standbyForMatch(m[1].key, u[1].email).then((xz) => {

              // ok: user is invited game
              inviteMatchByPlayerKey(u[0].email, u[0].key, m[1].key, xz.players[1].playerkey).then(function(game) {
                var readInvitedUsers = R.compose(R.filter(R.pipe(R.prop('type'), R.equals('invite'))), R.prop('players'));

                expect(readInvitedUsers(game).length).toBe(1);
                expect(readInvitedUsers(game)[0].email).toBe(u[1].email);
                expect(readInvitedUsers(game)[0].avataricon).toBe(u[1].profile.avataricon);
                expect(checkPlayerCount(game)).toBe(2);
                expect(seatAvailable(game)).toBe(1);
                expect(readAvatar(game)[0]).toBe(u[0].profile.avataricon);

                done();

              }); 
            });

					}); 
				});
      });
    });
  });

  it('buildStatusQuery', function(done) {

    expect(testMatches.buildStatusQuery('a', 'b')).toEqual({ status : 'b', players : { $elemMatch : { email : 'a', type : { $ne : 'standby' } } } });
    expect(testMatches.buildStatusQuery('c', 'd')).toEqual({ status : 'd', players : { $elemMatch : { email : 'c', type : { $ne : 'standby' } } } });
    done();

  });

  it('playerfind', function(done) {
    expect(testMatches.playerfind('abc')).toEqual({ $or : [ 
      { status : 'draft', players : { $elemMatch : { email : 'abc', type : { $ne : 'standby' } } } }, 
      { status : 'open', players : { $elemMatch : { email : 'abc', type : { $ne : 'standby' } } } }, 
      { status : 'full', players : { $elemMatch : { email : 'abc', type : { $ne : 'standby' } } } } 
    ] });
    expect(testMatches.playerfind('def')).toEqual({ $or : [ 
      { status : 'draft', players : { $elemMatch : { email : 'def', type : { $ne : 'standby' } } } }, 
      { status : 'open', players : { $elemMatch : { email : 'def', type : { $ne : 'standby' } } } }, 
      { status : 'full', players : { $elemMatch : { email : 'def', type : { $ne : 'standby' } } } } 
    ] });
    done();

  });


  it('geofind', function(done) {
    expect(testMatches.geofind(1,2,3)).toEqual({ $or : [ { status : 'open', isprivate : false }, { status : 'full', isprivate : false } ], experience:{$ne: 6, $ne: 11}, loc : { $near : { $geometry : { type : 'Point', coordinates : [ 2, 3 ] }, $maxDistance : 1, $minDistance : 0 } } });
    expect(testMatches.geofind(4,5,6)).toEqual({ $or : [ { status : 'open', isprivate : false }, { status : 'full', isprivate : false } ], experience : { $ne : 6, $ne: 11 }, loc : { $near : { $geometry : { type : 'Point', coordinates : [ 5, 6 ] }, $maxDistance : 4, $minDistance : 0 } } });
    done();

  });

  it('buildPublicExperienceQuery', function(done) {
    expect(testMatches.buildPublicExperienceQuery(10)).toEqual({ $and : [ { $or : [ { status : 'open', isprivate : false }, { status : 'full', isprivate : false } ] }, { experience : 10 } ] });
    expect(testMatches.buildPublicExperienceQuery(['a'])).toEqual({ $and : [ { $or : [ { status : 'open', isprivate : false }, { status : 'full', isprivate : false } ] }, { $or : [ { experience : 'a' } ] } ] });
    expect(testMatches.buildPublicExperienceQuery(['a', 'b', 'c'])).toEqual({ $and : [ { $or : [ { status : 'open', isprivate : false }, { status : 'full', isprivate : false } ] }, { $or : [ { experience : 'a' }, { experience : 'b' }, { experience : 'c' } ] } ] });
    done();
  });

  it('FindMatchesByDistances && findPublicMatches && findBoardgameMatchesOnline', function(done) {

    var users = [
      faker.Internet.email(),
      faker.Internet.email(),
      faker.Internet.email()
    ];

    var hostAvatar = 'http://localhost:3000/img/test-avatar2.jpg';
    var distance = 50000; // in meters

    var matchLocName1 = 'Zucchelli Station, Antarctica';
    var lat1 = -74.6953790;
    var lng1 = 164.0961780;

    var matchLocName2 = 'Norfolk Island International Airport, 2899';
    var lat2 = -29.0416730;
    var lng2 = 167.9393750;

    var matchLocName3 = 'Rooty Hill Rd, Kingston 2899, Norfolk Island';
    var lat3 = -29.0519720;
    var lng3 = 167.9655640;

    // TODO: Set Details for game below
    var gameid = 8190;
    var gamename = 'Made Up Game';
    var thumbnail = '//cf.geekdo-images.com/q32KDF-KFiI2ZP8POk-fh5v2JOI=/fit-in/246x300/pic3184103.jpg';
    var gameyear = 2016;
    var removeMatches = {'games.id': gameid};

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var gamePastDate = new Date(2014, 08, 03, 20, 30, 0); 

    var players = 4;
    var minPlayers = 2;
    var status = ['open', '', 'full'];
    
    var createUser1 = testAccounts.RespawnAccountObject(testAccount, users[0], createHostAccountObj(matchLocName1, lat1, lng1, hostAvatar));
    var createUser2 = testAccounts.RespawnAccountObject(testAccount, users[1], createHostAccountObj(matchLocName2, lat2, lng2, hostAvatar));
    var createUser3 = testAccounts.RespawnAccountObject(testAccount, users[2], createHostAccountObj(matchLocName3, lat3, lng3, hostAvatar));

    Promise.all([createUser1, createUser2, createUser3]).then(function(u) {
      Promise.all([cacheGame2017(gameid, gamename, thumbnail, gameyear), testMatch.remove(removeMatches)]).then(function(x) {

        var hostOpen = (user) => buildHost(testMatch, testAccount, user.email, user.key, gameDate, players, minPlayers, status[0], {}, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 2]), false);

        var hostPrivate = (user) => R.compose(
          testMatches.HostMatch(testMatch, testAccount, user.email, user.key), 
          zipObj(['gamedate', 'maxplayers', 'minplayers', 'status', 'social', 'locname', 'coords', 'games', 'timeend', 'isprivate'])
        )([gameDate, players, minPlayers, status[0], {}, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 2]), moment(gameDate).add(1, 'h'), true]);

        var hostOpenPast = (user) => buildHost(testMatch, testAccount, user.email, user.key, gamePastDate, players, minPlayers, status[0], {}, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 2]));

        var hostFull = (user) => buildHost(testMatch, testAccount, user.email, user.key, gameDate, players, minPlayers, status[2], {}, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 2]));

        // hostPrivateOnline :: o -> Promse o
        const hostPrivateOnline = (user) => R.compose(
          testMatches.HostMatch(testMatch, testAccount, user.email, user.key), 
          zipObj(['gamedate', 'maxplayers', 'minplayers', 'status', 'social', 'locname', 'coords', 'games', 'timeend', 'isprivate', 'experience'])
        )([gameDate, players, minPlayers, status[0], {}, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 2]), moment(gameDate).add(1, 'h'), true, 6]);

        // hostPublicOnline :: o -> Promse o
        const hostPublicOnline = (user) => R.compose(
          testMatches.HostMatch(testMatch, testAccount, user.email, user.key), 
          zipObj(['gamedate', 'maxplayers', 'minplayers', 'status', 'social', 'locname', 'coords', 'games', 'timeend', 'isprivate', 'experience', 'reoccur'])
        )([gameDate, players, minPlayers, status[0], {}, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 2]), moment(gameDate).add(1, 'h'), false, 11, {enable:true}]);

        var hostAll = R.flatten([
          R.map(hostOpen, u), 
          R.map(hostPrivate, u), 
          R.map(hostOpenPast, u), 
          R.map(hostFull, u),
          R.map(hostPrivateOnline, u),
          R.map(hostPublicOnline, u)
        ]);

        Promise.all(hostAll).then(function(m) {

          // search for games by distance
          var findByDistanceA = testMatches.FindBoardgameMatchesByDistance(testMatch, distance, u[0].email);
          var findByDistanceB = testMatches.FindBoardgameMatchesByDistance(testMatch, distance, u[1].email);

          Promise.all([
            findByDistanceA(lat1, lng1), 
            findByDistanceB(lat2, lng2), 
            findByDistanceB(lat3, lng3),
          ]).then(function(x) {
            expect(R.countBy(R.prop('status'), R.flatten(x))).toEqual({ open : 19, full : 5 });
            expect(R.countBy(R.prop('locname'), R.flatten(x))).toEqual({ "Zucchelli Station, Antarctica" : 6, "Norfolk Island International Airport, 2899" : 12, "Rooty Hill Rd, Kingston 2899, Norfolk Island" : 6});

            testMatches.findPublicMatches(u[0].email).then(function(y) {
              expect(R.countBy(R.prop('status'), R.flatten(y)).full).toBe(3);
              expect(R.countBy(R.prop('status'), R.flatten(y)).open).toBeGreaterThan(20);
              expect(y.length).toBeGreaterThan(20);

              testMatches.findPublicBoardgameByExperience([6, 11]).fork(
                err => {},
                z => {
                  expect(z[0].experience).toBe(11);
                  expect(z[1].experience).toBe(11);
                  expect(z[2].experience).toBe(11);
                  expect(z[0].isprivate).toBeFalsy();
                  expect(z[1].isprivate).toBeFalsy();
                  expect(z[2].isprivate).toBeFalsy();
                  expect(z.length).toBeGreaterThan(2);
                  done();
                }
              );

            });
          });
        }).catch(err => log.clos('err.FindMatchesByDistance', err));
      });
    });
  });

  it('Hostify', function(done) {
    const expectObj1 = {type:'host'};
    const expectObj2 = {type:'host', email: 'e', avataricon: 'a', name: 'n'};

    expect(testMatches.Hostify({a:1})).toEqual(expectObj1);
    expect(testMatches.Hostify({email:'e', avataricon: 'a', name: 'n'})).toEqual(expectObj2);
    expect(testMatches.Hostify({email:'e', avataricon: 'a', name: 'n', spam: 'test'})).toEqual(expectObj2);
    expect(testMatches.Hostify({email:'e', profile:{avataricon: 'a', name: 'n'}})).toEqual(expectObj2);
    done();
  });

  it('Seatavailable', function(done) {
    expect(testMatches.Seatavailable({x: 3})).toBe(0);
    expect(testMatches.Seatavailable({maxplayers: 3})).toBe(2);
    expect(testMatches.Seatavailable({maxplayers: 8})).toBe(7);
    done();
  });

  it('Userplayers', function(done) {
    const expectObj1 = [{type:'host', email: 'e'}];
    const expectObj2 = [{type:'host', email: 'e', avataricon: 'a', name: 'n'}];
    expect(testMatches.Userplayers('e')).toEqual(expectObj1);
    expect(testMatches.Userplayers({email:'e'})).toEqual(expectObj1);
    expect(testMatches.Userplayers({email:'e', avataricon: 'a', name: 'n'})).toEqual(expectObj2);
    expect(testMatches.Userplayers({email:'e', avataricon: 'a', name: 'n', spam: 'x'})).toEqual(expectObj2);
    done();
  });

  it('Zipmatch', function(done) {
      const expectObj1 = {};
      const expectObj2 = { thumbnail : 't', status : 'select', playorder : 1, description : 'description', minplaytime : 10, maxplaytime : 50, minplayers : 2, maxplayers : 4, playercounts : [  ], id : 1, name : 'n', bggrank : '', avgrating : '', weight : '', expansions:[] }; 
      expect(testMatches.Zipmatch({})).toEqual(expectObj1);
      expect(testMatches.Zipmatch({name: 'n', thumbnail: 't'})).toEqual(expectObj1);
      expect(testMatches.Zipmatch({id: 1, thumbnail: 't'})).toEqual(expectObj1);
      expect(testMatches.Zipmatch({id: 1, name: 'n'})).toEqual(expectObj1);
      expect(testMatches.Zipmatch({id: 1, name: 'n', thumbnail: 't', description: 'description', minplaytime: 10, maxplaytime: 50, minplayers: 2, maxplayers: 4})).toEqual(expectObj2);
      done();
  });


  it('FindBoardgameMatchesNearby', function(done) {

    var user1 = 'user1_findmatchesnearby@gmail.com';
    var user2 = 'user2_findmatchesnearby@gmail.com';
    var user3 = 'user3_findmatchesnearby@gmail.com';
    var users = [user1, user2, user3];
    var hostAvatar = 'http://localhost:3000/img/test-avatar2.jpg';

    var distance = 70000; // in meters

    var matchLocName1 = 'Northern Territory 0872, Australia';
    var lat1 = -23.5886632;
    var lng1 = 131.2808577;

    var matchLocName2 = 'Sakhalin Oblast, Russia, 694452';
    var lat2 = 51.9664320;
    var lng2 = 143.1319605;

    var matchLocName3 = 'Kunparrka NT 0872, Australia';
    var lat3 = -23.1989239;
    var lng3 = 131.8009317;

    var gameid = 155821;
    var gamename = 'Inis';
    var thumbnail = '//cf.geekdo-images.com/SPpzGVCnDxl2DgZTZMqGMqTcvwI=/fit-in/246x300/pic3112623.jpg';
    var gameyear = 2016;
    var removeMatches = {'games.id': gameid};

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var players = 4;
    var minPlayers = 2;
    var smoking = 1;
    var alcohol = 2;
    var social = {smoking: smoking, alcohol: alcohol};
    var status = ['open', 'private'];

    var createUser1 = testAccounts.RespawnAccountObject(testAccount, user1, createHostAccountObj(matchLocName1, lat1, lng1, hostAvatar))
    var createUser2 = testAccounts.RespawnAccountObject(testAccount, user2, createHostAccountObj(matchLocName2, lat2, lng2, hostAvatar))
    var createUser3 = testAccounts.RespawnAccountObject(testAccount, user3, createLocalAccountObj(matchLocName3, lat3, lng3, hostAvatar))

    Promise.all([createUser1, createUser2, createUser3]).then(function(u) {
      Promise.all([cacheGame2017(gameid, gamename, thumbnail, gameyear), testMatch.remove(removeMatches)]).then(function(x) {

        var hostOpen = (user) => buildHost(testMatch, testAccount, user.email, user.key, gameDate, players, minPlayers, status[0], social, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 2]));

        var hostPrivate = (user) => R.compose(
          testMatches.HostMatch(testMatch, testAccount, user.email, user.key), 
          zipObj(['gamedate', 'maxplayers', 'minplayers', 'status', 'social', 'locname', 'coords', 'games', 'timeend', 'isprivate'])
        )([gameDate, players, minPlayers, status[0], {}, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 2]), moment(gameDate).add(1, 'h'), true]);

        Promise.all([hostOpen(u[0]), hostPrivate(u[0]), hostOpen(u[1]), hostPrivate(u[1])]).then(function(h) {
            
          // search for games by distance
          testMatches.FindBoardgameMatchesNearby(testMatch, user3, distance).then(function(m) {
            expect(R.length(m)).toBe(1);

            var testNearby = whereMatchIs(matchLocName1, gameDate, m[0].timeend, players, minPlayers, status[0], smoking, alcohol);

            expect(testNearby(m[0])).toBe(true);
            expect(m[0].distance).toBe(68598);
            done();

          });
        }).catch(err => log.clos('err', err));
      });
    });
  });


  it('FindMatchByKey', function(done) {

    var minPlayers = 2;
    var players = 3;
    var status = 'open';
    var smoking = 1;
    var alcohol = 2;
    var social = {smoking, alcohol};

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 

    generateHostUser({}).then(function(u) {
      Promise.all([generateGame]).then(function(g) {

        buildHost(testMatch, testAccount, u.email, u.key, gameDate, players, minPlayers, status, social, u.hostareas[0].name, u.hostareas[0].loc.coordinates, createMatchObj([g[0].id, g[0].name, g[0].thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4])).then(function (m) {

          const matchATest = (gameEnd) => whereMatchIs(u.hostareas[0].name, gameDate, gameEnd, players, minPlayers, status, smoking, alcohol);
          const gameATest = whereGameIs(g[0].id, g[0].name, g[0].thumbnail);

          buildHost(testMatch, testAccount, u.email, u.key, gameDate, players, minPlayers, status, social, u.hostareas[0].name, u.hostareas[0].loc.coordinates, createMatchObj([g[0].id, g[0].name, g[0].thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4])).then(function(h) {

            testMatches.FindMatchByKey(testMatch, h.key).then(function(f) {
              expect(matchATest(f[0].timeend)(f[0])).toBe(true);
              expect(gameATest(f[0].games[0])).toBe(true);
              done();
            });

          });
        });
      });
    });
  });

  it('FindMatchByReoccurSeries', function(done) {
    generateMatch('open', 2, 2).then(function(h) {

      // find a single match by series id
      testMatches.FindLastMatchByReoccurSeries(testMatch, h[1].seriesid).then(function(f) {
        expect(f.date).toEqual(h[1].date);
        expect(f.timeend).toEqual(h[1].timeend);
        expect(f.seriesid).toBe(h[1].seriesid);

        // ensure a repawn match holds the series id 
        testMatches.taskRespawnMatch(testMatch, testAccount, mergeEveryWeek(h[1])).fork(
          ()=>{},
          (resp)=>{
            expect(h[1].date).toBeLessThan(resp.date);
            expect(h[1].timeend).toBeLessThan(resp.timeend);
            expect(h[1].seriesid).toBe(resp.seriesid);

            // ensure a finding last match returns the newest match 
            testMatches.FindLastMatchByReoccurSeries(testMatch, h[1].seriesid).then(function(fnew) {
              expect(fnew.date).toEqual(resp.date);
              expect(fnew.timeend).toEqual(resp.timeend);
              expect(fnew.seriesid).toBe(resp.seriesid);

              // check returning all, has the newest event first 
              testMatches.FindAllMatchByReoccurSeries(testMatch, h[1].seriesid).then(function(fall) {
                expect(R.last(fall).date).toEqual(f.date);
                expect(R.last(fall).timeend).toEqual(f.timeend);
                expect(R.last(fall).seriesid).toBe(f.seriesid);

                expect(R.head(fall).date).toEqual(fnew.date);
                expect(R.head(fall).timeend).toEqual(fnew.timeend);
                expect(R.head(fall).seriesid).toBe(fnew.seriesid);

                // searching for an unknown key should return an error 
                testMatches.FindLastMatchByReoccurSeries(testMatch, 'bad').catch(function(err) {
                  expect(err).toBe('bad series key');
                  done();
                });
              });

            })
          }
        );
      });
    });
  });


  it('FindHistoricMatchByPlayerType', function(done) {

      var playyear = new Date().getFullYear() + 1;
      var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
      var gameDatePast = new Date(2014, 08, 03, 20, 30, 0); 
      var players = 4;
      var minPlayers = 2;
      var status = ['open', 'played'];
      var smoking = 1;
      var alcohol = 2;
      var social = {smoking: smoking, alcohol, alcohol};

      var gameid = 123;

      Promise.all([generateHostUser({}), generateGame]).then(function(u) {

          var hostOpen = (user) => buildHost(testMatch, testAccount, user.email, user.key, gameDate, players, minPlayers, status[0], social, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([u[1].id, u[1].name, u[1].thumbnail, 'description', 30, 60, 1, 2]));

          var hostOpenPast = (user) => buildHost(testMatch, testAccount, user.email, user.key, gameDatePast, players, minPlayers, status[1], social, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([u[1].id, u[1].name, u[1].thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4]));

          Promise.all([hostOpen(u[0]), hostOpenPast(u[0])]).then(function(ho) {
              testMatches.FindHistoricMatchesByPlayerType(testMatch, u[0].email, '').then().catch(function(err) {
                  expect(err.code).toBe(400);  
                  testMatches.FindHistoricMatchesByPlayerType(testMatch, u[0].email, []).then().catch(function(err) {
                      expect(err.code).toBe(400);  
                      testMatches.FindHistoricMatchesByPlayerType(testMatch, u[0].email, ['host']).then(function(h) {
                          expect(h.length).toBe(1);

                          var testFindHost = (gameEnd) => R.compose(whereMatchIs(u[0].hostareas[0].name, gameDatePast, gameEnd, players, minPlayers, status[1], smoking, alcohol), R.head);
                          var testGame = whereGameIs(u[1].id, u[1].name, u[1].thumbnail);

                          expect(testFindHost(h[0].timeend)(h)).toBe(true);
                          expect(testGame(h[0].games[0])).toBe(true);

                          testMatches.UpdateMatchPlayerType(testMatch, h[0].key, u[0].email, 'invite').then(function(pm) {
                              var findify = testMatches.FindHistoricMatchesByPlayerType(testMatch, u[0].email);
                              var searches = [
                                  ['host'],
                                  ['invite'],
                                  ['host', 'approve', 'complete'],
                                  ['invite', 'request']
                              ];

                              Promise.all(R.map(findify, searches)).then(function(g) {
                                  expect(g[0].length).toBe(0);
                                  expect(g[1].length).toBe(1);
                                  expect(testFindHost(g[1][0].timeend)(g[1])).toBe(true);
                                  expect(g[2].length).toBe(0);
                                  expect(g[3].length).toBe(1);
                                  expect(testFindHost(g[3][0].timeend)(g[3])).toBe(true);
                                  done();

                              });
                          });
                      });
                  });
              });
          });
      });
  });

  it('FindMatchByPlayerType', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var gameDatePast = new Date(2014, 08, 03, 20, 30, 0); 
    var players = 4;
    var minPlayers = 2;
    var status = ['open', 'private'];
    var smoking = 1;
    var alcohol = 2;
    var social = {smoking: smoking, alcohol, alcohol};

    var gameid = 123;

    Promise.all([generateHostUser({}), generateGame]).then(function(u) {

      var hostOpen = (user) => buildHost(testMatch, testAccount, user.email, user.key, gameDate, players, minPlayers, status[0], social, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([u[1].id, u[1].name, u[1].thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4]));

      var hostOpenPast = (user) => buildHost(testMatch, testAccount, user.email, user.key, gameDatePast, players, minPlayers, status[0], social, user.hostareas[0].name, user.hostareas[0].loc.coordinates, createMatchObj([u[1].id, u[1].name, u[1].thumbnail, 'Pending Bgg Lookup...', 30, 60, 2, 4]));

      Promise.all([hostOpen(u[0]), hostOpenPast(u[0])]).then(function(ho) {

        testMatches.FindMatchesByPlayerType(testMatch, u[0].email, '').then().catch(function(err) {

          expect(err.code).toBe(400);  
          testMatches.FindMatchesByPlayerType(testMatch, u[0].email, []).then().catch(function(err) {
            expect(err.code).toBe(400);  
            testMatches.FindMatchesByPlayerType(testMatch, u[0].email, ['host']).then(function(h) {
              expect(h.length).toBe(1);

              var testFindHost = (gameEnd) => R.compose(whereMatchIs(u[0].hostareas[0].name, gameDate, gameEnd, players, minPlayers, status[0], smoking, alcohol), R.head);

              var testGame = whereGameIs(u[1].id, u[1].name, u[1].thumbnail);

              expect(testFindHost(h[0].timeend)(h)).toBe(true);
              expect(testGame(h[0].games[0])).toBe(true);

              testMatches.UpdateMatchPlayerType(testMatch, h[0].key, u[0].email, 'invite').then(function(pm) {
                var findify = testMatches.FindMatchesByPlayerType(testMatch, u[0].email);
                var searches = [
                  ['host'],
                  ['invite'],
                  ['host', 'approve', 'complete'],
                  ['invite', 'request']
                ];

                Promise.all(R.map(findify, searches)).then(function(g) {
                  expect(g[0].length).toBe(0);
                  expect(g[1].length).toBe(1);
                  expect(testFindHost(g[1][0].timeend)(g[1])).toBe(true);
                  expect(g[2].length).toBe(0);
                  expect(g[3].length).toBe(1);
                  expect(testFindHost(g[3][0].timeend)(g[3])).toBe(true);
                  done();

                });
              });
            });
          });
        });
      });
    });
  });


  it('RevokeMatch', function(done) {

    var findPlayerkey = testMatches.FindPlayerkey;
    var countPlayerByType = R.curry((req, type) => R.compose(R.length, R.filter(R.pipe(R.prop('type'), R.equals(type))), R.prop('players'))(req));
    var seatavailable = R.compose(R.prop('seatavailable'));

    Promise.all([generateMatch('open', 3, 2), generateUser({}), generateGame, generateUser({})]).then(function(u) {

      var host = u[0][0];
      var m = u[0][1];

      var invite = testMatches.InviteMatch(testAccount, testMatch, host.email, host.key);
      var revoke = testMatches.RevokeMatch(testAccount, testMatch);
      var join = testMatches.JoinMatch(testAccount, testMatch);
      var approve = testMatches.ApproveUser(testAccount, testMatch, m.key, host.email, host.key);
      var errorCode = R.prop('code');

      invite(m.key, u[1].email).then(function(game) {
        expect(game.players.length).toBe(2);
        expect(countPlayerByType(game, 'invite')).toBe(1);

        revoke(host.email, host.key, m.key, findPlayerkey(game, 'type', 'invite')).then(function(x) {

          expect(x.players.length).toBe(1);
          expect(countPlayerByType(x, 'invite')).toBe(0);

          var acceptAndRevoke = R.composeP(revoke(host.email, host.key, m.key), findPlayerkey(R.__, 'email', u[1].email), approve, findPlayerkey(R.__, 'email',  u[1].email), join(R.__, u[1].email, u[1].key));
          acceptAndRevoke(m.key).then(function(ar) { 

            expect(x.players.length).toBe(1);
            expect(seatavailable(ar)).toBe(2);

            var fillAndRevoke = R.composeP(revoke(host.email, host.key, m.key), findPlayerkey(R.__, 'email', u[3].email), approve, findPlayerkey(R.__, 'email', u[1].email), approve, findPlayerkey(R.__, 'email',  u[3].email), join(R.__, u[3].email, u[3].key), R.prop('key'), join(R.__, u[1].email, u[1].key));

            fillAndRevoke(m.key).then(m => {
              expect(m.players.length).toBe(2);

              revoke(host.email, 'badkey', m.key, findPlayerkey(game, 'type', 'invite')).then().catch(function(err) {

                expect(errorCode(err)).toBe(401);

                revoke(host.email, host.key, 'badmatch', findPlayerkey(game,'type', 'invite')).then().catch(function(err) {
                  expect(errorCode(err)).toBe(403) 
                  revoke(host.email, host.key, m.key, 'abcdef').then().catch(function(err) {

                    expect(errorCode(err)).toBe(409) 
                    done();

                  });
                });
              });
            });
          });
        });
      });
    });
  });


  it('RevokeRequest', function(done) {

      var findPlayerkey = testMatches.FindPlayerkey;
      var countPlayerByType = R.curry((req, type) => R.compose(R.length, R.filter(R.pipe(R.prop('type'), R.equals(type))), R.prop('players'))(req));
      var seatavailable = R.compose(R.prop('seatavailable'));

      Promise.all([generateMatch('open', 3, 2), generateUser({}), generateGame, generateUser({})]).then(function(u) {

          var host = u[0][0];
          var m = u[0][1];

          var invite = testMatches.InviteMatch(testAccount, testMatch, host.email, host.key);
          var leaveMatch = testMatches.LeaveMatch(testAccount, testMatch);
          var join = testMatches.JoinMatch(testAccount, testMatch);
          var approve = testMatches.ApproveUser(testAccount, testMatch, m.key, host.email, host.key);
          var errorCode = R.prop('code');

          invite(m.key, u[1].email).then(function(game) {

              expect(game.players.length).toBe(2);
              expect(countPlayerByType(game, 'invite')).toBe(1);
                  
              leaveMatch(u[1].email, u[1].key, m.key).then(function(x) {
                  expect(x.players.length).toBe(1);
                  expect(countPlayerByType(x, 'invite')).toBe(0);

                  var joinAndRevoke = R.composeP(leaveMatch(u[1].email, u[1].key), R.prop('key'), join(R.__, u[1].email, u[1].key));
                  joinAndRevoke(m.key).then(function(ar) { 
                      expect(x.players.length).toBe(1);
                      expect(countPlayerByType(x, 'request')).toBe(0);

                      var acceptAndRevoke = R.composeP(leaveMatch(u[1].email, u[1].key), R.prop('key'), approve, findPlayerkey(R.__, 'email',  u[1].email), join(R.__, u[1].email, u[1].key));
                      acceptAndRevoke(m.key).then(function(ar) { 
                          expect(x.players.length).toBe(1);
                          expect(seatavailable(ar)).toBe(2);

                          var fillAndRevoke = R.composeP(leaveMatch(u[3].email, u[3].key), R.prop('key'), approve, findPlayerkey(R.__, 'email', u[1].email), approve, findPlayerkey(R.__, 'email',  u[3].email), join(R.__, u[3].email, u[3].key), R.prop('key'), join(R.__, u[1].email, u[1].key));
                          fillAndRevoke(m.key).then(m => {
                              expect(m.players.length).toBe(2);
                              done();

                          });
                      });
                  });
              });
          });
      });
  });

  it('FindPlayerkey - FindPlayerType', function(done) {
    var fakeMatch = {
      players: [
        {email: 'abc@def.com', playerkey: 'keyA', type: 'typeA'},
        {email: 'abc@notdef.com', playerkey: 'keyB', type: 'typeB'},
      ]
    }

    expect(testMatches.FindPlayerKeys(fakeMatch, 'playerkey', 'email', 'abc@def.com')).toBe('keyA');
    expect(testMatches.FindPlayerKeys(fakeMatch, 'type', 'email', 'abc@notdef.com')).toBe('typeB');
    expect(testMatches.FindPlayerKeys(fakeMatch, 'playerkey', 'email', 'notfound@notdef.com')).toBe(false);

    expect(testMatches.FindPlayerkey(fakeMatch, 'email', 'abc@def.com')).toBe('keyA');
    expect(testMatches.FindPlayerkey(fakeMatch, 'email', 'abc@notdef.com')).toBe('keyB');
    expect(testMatches.FindPlayerType(fakeMatch, 'email', 'abc@def.com')).toBe('typeA');
    expect(testMatches.FindPlayerType(fakeMatch, 'email', 'abc@notdef.com')).toBe('typeB');
    done();

  });

  it('VerifyHost', function(done) {
      Promise.all([generateMatch('open', 3, 2), generateUser({}), generateUser({})]).then(function(u) {

          var host = u[0][0];
          var m = u[0][1];
          testMatches.JoinMatch(testAccount, testMatch, m.key, u[1].email, u[1].key).then(function(j) {
              var verify = testMatches.VerifyHostKey(testAccount, testMatch, m.key);
              verify(host.email, host.key).then(function(x) {
                  expect(x.key).toBe(m.key);

                  verify(u[1].email, u[1].key).then().catch(function(err) {
                      expect(err.code).toBe(403);
                      done();

                  });
              });
          });
      });
  });


  it('VerifyPlayer', function(done) {
    Promise.all([generateMatch('open', 3, 2), generateUser({}), generateUser({})]).then(function(u) {
      var host = u[0][0];
      var m = u[0][1];
      testMatches.JoinMatch(testAccount, testMatch, m.key, u[1].email, u[1].key).then(function(j) {
        var verify = testMatches.VerifyPlayerKey(testAccount, testMatch, m.key);
        verify(u[1].email, u[1].key).then(function(x) {
          expect(x.key).toBe(m.key);
          verify(host.email, host.key).then().catch(function(err) {
            expect(err.code).toBe(403);
            done();
          });
        });
      });
    });
  });


  it('VerifyKey', function(done) {
    Promise.all([generateMatch('open', 3, 2), generateUser({}), generateUser({})]).then(function(u) {

      var host = u[0][0];
      var m = u[0][1];
      
      var findPlayerType = R.curry((req, keyname, value) => R.compose(R.prop('type'), R.head, R.filter(R.pipe(R.prop(keyname), R.equals(value))), R.prop('players'))(req));
      testFunction = (match, email) => findPlayerType(match, 'email', email) == 'host';

      var verify = testMatches.VerifyKey(testAccount, testMatch, m.key, testFunction);

      testMatches.JoinMatch(testAccount, testMatch, m.key, u[1].email, u[1].key).then(function(j) {
        verify(host.email, host.key).then(function(x) {
          expect(x.key).toBe(m.key);
          verify(u[1].email, u[1].key).then().catch(function(err) {
            expect(err.code).toBe(403);
            verify(u[2].email, u[2].key).then().catch(function(err) {
              expect(err.code).toBe(403);
              verify(u[2].email, 'badkey').then().catch(function(err) {
                expect(err.code).toBe(401);
                testMatches.VerifyPlayerKey(testAccount, testMatch, 'nomatch', host.email, host.key).then().catch(function(err) {
                  expect(err.code).toBe(403);
                  done();

                });
              });
            });
          });
        });
      });
    });
  });


  it('updateMatchStatus - updateMatchPlayerkeyType - RemoveMatchPlayerkey', function(done) {

    var findPlayerkey = R.curry((req, keyname, value) => R.compose(R.prop('playerkey'), R.head, R.filter(R.pipe(R.prop(keyname), R.equals(value))), R.prop('players'))(req));
    var findPlayerType = R.curry((req, keyname, value) => R.compose(R.prop('type'), R.head, R.filter(R.pipe(R.prop(keyname), R.equals(value))), R.prop('players'))(req));

    Promise.all([generateMatch('open', 3, 2), generateUser({}), generateUser({})]).then(function(u) {

      var m = u[0][1];
      var joinMatch = testMatches.JoinMatch(testAccount, testMatch, m.key);
      Promise.all([joinMatch(u[1].email, u[1].key), joinMatch(u[2].email, u[2].key)]).then(function(j) {

        var playerstate = 'invite';

        testMatches.UpdateMatchPlayerType(testMatch, m.key, u[1].email, playerstate).then(function(pm) {
          expect(findPlayerType(pm, 'email', u[1].email)).toBe(playerstate);

          testMatches.RemoveMatchPlayerkey(testMatch, m.key, findPlayerkey(pm, 'email', u[1].email)).then(function(rp) {
            expect(rp.players.length).toBe(2);

            joinMatch(u[1].email, u[1].key).then(function(pm) {
              playerstate = "reject";
              testMatches.UpdateMatchPlayerkeyType(testMatch, m.key, findPlayerkey(pm, 'email', u[1].email), playerstate).then(function(pk) {
                expect(findPlayerType(pk, 'email', u[1].email)).toBe(playerstate);
                expect(pk.seatavailable).toBe(2);
                playerstate = "approve";

                testMatches.UpdateMatchPlayerkeyType(testMatch, m.key, findPlayerkey(pk, 'email', u[1].email), playerstate).then(function(ac) {
                  expect(findPlayerType(ac, 'email', u[1].email)).toBe(playerstate);
                  expect(ac.status).toBe('open');
                  expect(ac.seatavailable).toBe(1);

                  testMatches.UpdateMatchPlayerkeyType(testMatch, m.key, findPlayerkey(pk, 'email', u[2].email), playerstate).then(function(ac) {
                    expect(findPlayerType(ac, 'email', u[2].email)).toBe(playerstate);
                    expect(ac.isfull).toBe(true);
                    expect(ac.seatavailable).toBe(0);

                    var matchstate = 'played';
                    testMatches.UpdateMatchStatus(testMatch, m.key, matchstate).then(function(um) {
                      expect(um.status).toBe(matchstate);
                      expect(um.key).toBe(m.key);
                      done();

                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });

  it('updateRemovePlayerQry', function(done) {
    expect(testMatches.updateRemovePlayerQry('a', true)).toEqual({ $pull : { players : { email : 'a' } }, $inc : { seatavailable : 1 }, $set : { isfull : false } });
    expect(testMatches.updateRemovePlayerQry('b', true)).toEqual({ $pull : { players : { email : 'b' } }, $inc : { seatavailable : 1 }, $set : { isfull : false } });
    expect(testMatches.updateRemovePlayerQry('a', false)).toEqual({ $pull : { players : { email : 'a' } } });
    done();
  });

  it('buildMatchkey', function(done) {
    done();
  });

  it('RemovePlayerEmail', function(done) {
    Promise.all([
      generateMatch('open', 3, 2), 
      generateMatch('open', 3, 2), 
      generateUser({})
    ]).then(function(m) {
      Promise.all([
        testMatch.findOneAndUpdate(
          {key: m[0][1].key}, 
          {$push: {
            players: {
              avataricon: m[2].profile.avataricon,
              name:       m[2].profile.name,
              email:      m[2].email,
              type:       'invite',
            }
          }, seatmax: 2, seatavailable:1}, {new:true}),
        testMatch.findOneAndUpdate(
          {key: m[1][1].key}, 
          {$push: {
            players: {
              avataricon: m[2].profile.avataricon,
              name:       m[2].profile.name,
              email:      m[2].email,
              type:       'approve',
            }
          }, seatmax: 2, seatavailable:0, isfull:true}, {new:true})
      ]).then((x) => {
        testMatches.removePlayerEmail(testMatch, m[2].email).fork(
          (err) 		=> log.clos('err', err),
          (update) 	=> {

            expect(update[0].players.length).toBe(1);
            expect(update[0].seatavailable).toBe(1);
            expect(update[0].isfull).toBeFalsy();

            expect(update[1].players.length).toBe(1);
            expect(update[1].seatavailable).toBe(1);
            expect(update[1].isfull).toBeFalsy();

            done();

          }
        );
      });
    });
  });


  it('updateMatchPlayerkeyTypeName - RemoveMatchPlayerkey', function(done) {

    var findPlayerkey = R.curry((req, keyname, value) => R.compose(R.prop('playerkey'), R.head, R.filter(R.pipe(R.prop(keyname), R.equals(value))), R.prop('players'))(req));

    var findPlayerType = R.curry((req, keyname, value) => R.compose(R.prop('type'), R.head, R.filter(R.pipe(R.prop(keyname), R.equals(value))), R.prop('players'))(req));


    Promise.all([generateMatch('open', 3, 2), generateUser({}), generateUser({})]).then(function(u) {

      var m = u[0][1];
      var inviteMatch1 = testMatches.InviteMatch(testAccount, testMatch, u[0][0].email, u[0][0].key, m.key);

      Promise.all(R.map(inviteMatch1, R.map(R.prop('email'), [u[1], u[2]]))).then(function(games) {

        testMatches.approveAnonymousPlayer(testMatch, m.key, games[1].players[1].playerkey, 'alphadog').then(function(pm) {
          expect(pm.players[1].type).toBe('approve');
          expect(pm.players[1].name).toBe('alphadog');
          done();
        });

      });
    });
  });


  it('readPlayer', function(done) {
      expect(testMatches.ReadPlayer({email: 'abc', type: 'xyz'}).get()).toEqual(['abc', false]);
      expect(testMatches.ReadPlayer({email: 'abc', type: 'host'}).get()).toEqual(['abc', true]);
      expect(testMatches.ReadPlayer({email: 'xyz', type: 'xyz'}).get()).toEqual(['xyz', false]);
      expect(testMatches.ReadPlayer({email: 'xyz', type: 'host'}).get()).toEqual(['xyz', true]);
      done();
  });

  it('gamePlayers', function(done) {
    expect(testMatches.GamePlayers({players:[{email: 'abc', type: 'xyz'}, {email: 'xyz', type: 'host'}]}).get()).toEqual([ [ 'abc', false ], [ 'xyz', true ] ]);
    expect(testMatches.GamePlayers({players:[{email: 'xyz', type: 'host'}, {email: 'abc', type: 'sdf'}]}).get()).toEqual([ [ 'xyz', true ], [ 'abc', false ] ]);
    done();
  });

  it('gamePlayerXP', function(done) {
    expect(testMatches.GamePlayerXP({date: new Date(2017, 08, 20, 10, 30), timeend: new Date(2017, 08, 20, 12, 30), players:[{email: 'abc', type: 'xyz'}, {email: 'xyz', type: 'host'}]}).get()).toEqual([ ['abc', false, 120], ['xyz', true, 120] ]);
    expect(testMatches.GamePlayerXP({players:[{email: 'xyz', type: 'host'}]}).getOrElse([])).toEqual([]);
    expect(testMatches.GamePlayerXP({date: new Date(2017, 08, 20, 9, 30), timeend: new Date(2017, 08, 20, 12, 30), players:[{email: 'abc', type: 'xyz'}, {email: 'xyz', type: 'host'}]}).get()).toEqual([ [ 'abc', false, 180 ], [ 'xyz', true, 180 ] ]);
    expect(testMatches.GamePlayerXP({date: new Date(2017, 08, 20, 9, 15), timeend: new Date(2017, 08, 20, 12, 45), players:[{email: 'xyz', type: 'host'}, {email: 'abc', type: 'sdf'}]}).get()).toEqual([ [ 'xyz', true, 210 ], [ 'abc', false, 210 ] ]);
    done();
  });

  it('addWeek', function(done) {
    expect(testMatches.addWeek(Maybe.of(new Date('2017-07-31')), Maybe.of(1)).get()).toEqual(new Date('Mon Aug 07 2017 10:00:00 GMT+1000 (AEST)'));
    expect(testMatches.addWeek(Maybe.of(new Date('2017-07-31')), Maybe.of(2)).get()).toEqual(new Date('Mon Aug 14 2017 10:00:00 GMT+1000 (AEST)'));
    done();
  });

  it('addWeekOfMonth', function(done) {
    expect(testMatches.addWeekOfMonth(Maybe.of(new Date('2017-07-31')), Maybe.of(1)).get()).toEqual(new Date('Mon Aug 07 2017 10:00:00 GMT+1000 (AEST)'));
    expect(testMatches.addWeekOfMonth(Maybe.of(new Date('2017-07-31')), Maybe.of(2)).get()).toEqual(new Date('Mon Aug 14 2017 10:00:00 GMT+1000 (AEST)'));
    done();
  });

  it('recureDateObj', function(done) {

    expect(testMatches.recurDateObj('a', 'b', {a:20}).get()).toEqual({a:20});

    expect(testMatches.recurDateObj('a', 'b', {a:new Date('2017-09-01'), reoccur: {enable:false}}).get()).toEqual({ a: new Date('Fri Sep 01 2017 10:00:00 GMT+1000 (AEST)'), reoccur: Object({ enable: false }) });

    expect(testMatches.recurDateObj('a', 'b', {a:new Date('2017-09-01'), reoccur: {enable:true}}).get()).toEqual({ a : new Date('Fri Sep 01 2017 10:00:00 GMT+1000 (AEST)'), reoccur : { enable : true } } );

    expect(testMatches.recurDateObj('a', 'b', {a:new Date('2017-09-01'), status:'draft', reoccur: {enable:true, every: 2, interval:'week'}}).get()).toEqual({ a : new Date('Fri Sep 01 2017 10:00:00 GMT+1000 (AEST)'), status : 'draft', reoccur : { enable : true, every : 2, interval : 'week' } });

    expect(testMatches.recurDateObj('a', 'b', {a:new Date('2017-09-01'), reoccur: {enable:true, every: 2, interval:'week'}, status:'open'}).get()).toEqual({ a: new Date('Fri Sep 01 2017 10:00:00 GMT+1000 (AEST)'), reoccur: Object({ enable: true, every: 2, interval: 'week' }), b: new Date('Fri Sep 15 2017 10:00:00 GMT+1000 (AEST)'), status:'open'});

    expect(testMatches.recurDateObj('a', 'b', {a:new Date('2017-09-01'), reoccur: {enable:true, every: 2, interval:'weekOfMonth'}, status:'open'}).get()).toEqual({ a: new Date('Fri Sep 01 2017 10:00:00 GMT+1000 (AEST)'), reoccur: Object({ enable: true, every: 2, interval: 'weekOfMonth' }), b: new Date('Fri Sep 08 2017 10:00:00 GMT+1000 (AEST)'), status:'open' });

    expect(testMatches.recurDateObj('a', 'b', {a:new Date('2017-09-01'), reoccur: {enable:true, every: 3, interval:'weekOfMonth'}, status:'open'}).get()).toEqual({ a: new Date('Fri Sep 01 2017 10:00:00 GMT+1000 (AEST)'), reoccur: Object({ enable: true, every: 3, interval: 'weekOfMonth' }), b: new Date('Fri Sep 15 2017 10:00:00 GMT+1000 (AEST)'), status:'open' });
    
    done();
  });

  it('buildSocial', function(done) {
    expect(testMatches.buildSocial({})).toEqual({ social: { smoking: '1', alcohol: '1' } } );
    expect(testMatches.buildSocial({smoking:'2'})).toEqual({ social: { smoking: '2', alcohol: '1' } } );
    expect(testMatches.buildSocial({alcohol:'2'})).toEqual({ social: { smoking: '1', alcohol: '2' } } );
    expect(testMatches.buildSocial({smoking: '2', alcohol:'2'})).toEqual({ social: { smoking: '2', alcohol: '2' } } );
    done();
  });

  it('buildLoc', function(done) {
    expect(testMatches.buildLoc({})).toEqual({});
    expect(testMatches.buildLoc({loc:{coordinates:'a'}})).toEqual({coords:'a'});
    done();
  });

  it('renameProp', function(done) {
    expect(testMatches.renameProp('a', 'b', {a: 'c'})).toEqual({b: 'c'});
    expect(testMatches.renameProp('c', 'd', {a: 'c'})).toEqual({a: 'c'});
    done();
  });


  it('TaskReadByEvent', function(done) {

      var gameid = 151347;
      var gamename = 'Millennium Blades';
      var thumbnail = 'https://cf.geekdo-images.com/dcapJRTBU6Kexqy199AD7ZZgObw=/fit-in/246x300/pic2468179.jpg';
      var gameyear = 2016;

      var matchx = {'games.id': gameid};
      var playyear = new Date().getFullYear() + 1;
      var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
      var gameEnd = new Date(playyear, 08, 03, 22, 30, 0); 

      var matchLocName = 'University of Wollongong';
      var matchUser = {
          email: 'match1@gmail.com', 
          avataricon: 'http://localhost:3000/img/test-avatar1.jpg',
          name: 'Jeff Jones'
      };

      var minPlayers = 2;
      var matchPlayers = 5;
      var lat = -34.4054039;
      var lng = 150.8784299;
      var coords = [lng, lat];

      var readLat = compose(last, path(['loc', 'coordinates']));
      var readLng = compose(head, path(['loc', 'coordinates']));

      const readHostkey = (keyname, obj) => compose(prop(keyname), head, prop('players'))(obj);

      const status1 = 'open';
      const smoking = 2;
      const alcohol = 1;
      const eventid = 'e3ent';

      const social = {
        smoking: smoking,
        alcohol: alcohol
      };

      Promise.all([testMatch.remove(matchx), testBoardgames.DeleteBoardgame(testBoardgame, gameid)]).then(function(y) {
        cacheGame2017(gameid, gamename, thumbnail, gameyear).then(function(y) {

          const makeEventMatch = R.compose(
            testMatches.CreateMatch(testMatch, matchUser, false), 
            R.zipObj(['locname', 'coords', 'gamedate', 'timeend', 'maxplayers', 'minplayers', 'status', 'social', 'games', 'eventid', 'isprivate'])
          );

          const match4 = makeEventMatch([matchLocName, coords, gameDate, gameEnd, matchPlayers, minPlayers, status1, social, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 4]), eventid, true]);

          match4.then(function(y) {
            testMatches.TaskReadByEvent(testMatch, eventid).fork(
              err => log.clos('err', err),
              data => {
                expect(data[0].eventid).toEqual(eventid);
                done();
              }
            )
          });

        }).catch(err => log.clos('caught', err));
      }); 
  });

  it('transformDate', function(done) {

    var gameDate = new Date(2000, 08, 03, 20, 30, 0); 
    var gameEnd = new Date(2000, 08, 03, 22, 30, 0); 

    expect({}).toEqual({});
    expect(testMatches.transformDate({timeend: gameEnd, reoccur: {enable:false}, date:'abc'}).reoccur).toEqual({ enable : false });
    expect(testMatches.transformDate({timeend: gameEnd, reoccur: {enable:false}, date:'abc'}).date).toEqual(undefined);

    done();

  });


  it('taskRespawnMatch', function(done) {
    testMatch.remove({}).then(function(x) {

      var yesterday = new Date();
      yesterday.setDate(yesterday.getDate() - 1);

      // create test matches
      Promise.all([
        generateMatchWDate('open', 3, 2, yesterday - 60*60*1000, yesterday), 
        generateMatchWDate('postgame', 3, 2, yesterday - 60*60*1000, yesterday), 
        generateMatchWDate('open', 3, 2, yesterday - 60*60*1000, yesterday),
        generateUser({}), 
      ]).then(function(u) {

        testMatch
          .findOneAndUpdate({key: u[2][1].key}, {isprivate:false}, {new: true})
          .exec()
          .then(
            data => {

              testMatches.InviteMatch(testAccount, testMatch, u[0][0].email, u[0][0].key, u[0][1].key, u[3].email).then(invitedmatch => {
                testMatches.taskRespawnMatch(testMatch, testAccount, mergeEveryWeek(invitedmatch)).fork(
                  err => log.clos('RespawnMatch.err', err),
                  data => {
                    expect(data.timezone).toBe('Australia/Sydney');
                    expect(data.currency).toBe('AUD');
                    expect(data.status).toBe('open');
                    expect(data.viewcount).toBe(0);
                    expect(data.impressioncount).toBe(0);
                    expect(data.players[0].type).toBe('host');
                    expect(data.players.length).toBe(2);
                    expect(data.players[1].email).toBe(u[3].email);
                    expect(data.players[1].type).toBe('standby');

                    testMatches.taskRespawnMatch(testMatch, testAccount, mergeEveryWeek(u[1][1])).fork(
                      err => log.clos('RespawnMatch.err', err),
                      data => {
                        expect(R.has('date', scrub(data))).toBeTruthy();
                        expect(R.has('timeend', scrub(data))).toBeTruthy();
                        expect(R.has('isfull', scrub(data))).toBeTruthy();
                        expect(data.status).toBe('open');

                        testMatches.taskRespawnMatch(testMatch, testAccount, mergeEveryWeek(u[2][1])).fork(
                          err => log.clos('RespawnMatch.err', err),
                          data => {
                            expect(R.has('date', scrub(data))).toBeTruthy();
                            expect(R.has('timeend', scrub(data))).toBeTruthy();
                            expect(R.has('isfull', scrub(data))).toBeTruthy();
                            expect(data.isprivate).toBeTruthy();
                            expect(data.status).toBe('open');
                            done();
                          }
                        );

                      }
                    );
                  }
                );
              });

            },
            err => log.clos('taskRespawnMatch.err', err)
          )


      });
    });
  });


  it('respawnGames', function(done) {
    expect(testMatches.respawnGames({})).toEqual({games:[]});
    expect(testMatches.respawnGames({experience:4, games:[{a:'b'}]})).toEqual({experience:4, games:[]});
    expect(testMatches.respawnGames({experience:10, games:[{a:'b'}]})).toEqual({experience:10, games:[{a:'b'}]});
    done();
  });


  it('taskRespawnMatchRepeatingCampaign', function(done) {
    testMatch.remove({}).then(function(x) {

      var gameid = 151347;
      var gamename = 'Millennium Blades';
      var thumbnail = 'https://cf.geekdo-images.com/dcapJRTBU6Kexqy199AD7ZZgObw=/fit-in/246x300/pic2468179.jpg';
      var gameyear = 2016;

      var yesterday = new Date();
      yesterday.setDate(yesterday.getDate() - 1);

      const repeatingCampaign = 10;

      const repeatingMatch = R.compose(
        R.assoc('experience', 10),
        mergeEveryWeek
      );

      // create test matches
      Promise.all([
        generateMatchWDate('postgame', 3, 2, yesterday - 60*60*1000, yesterday), 
      ]).then(function(u) {

        testMatches.taskRespawnMatch(testMatch, testAccount, repeatingMatch(u[0][1])).fork(
          err => log.clos('RespawnMatch.err', err),
          data => {
            expect(data.timezone).toBe('Australia/Sydney');
            expect(data.currency).toBe('AUD');
            expect(data.status).toBe('open');
            expect(data.viewcount).toBe(0);
            expect(data.impressioncount).toBe(0);
            expect(data.players[0].type).toBe('host');
            expect(data.experience).toBe(repeatingCampaign);
            expect(data.games.length).toBe(1);
            done();

          }
        );

      });
    });
  });



  it('taskPlayMatch', function(done) {

    const joinmatch = testMatches.JoinMatch(testAccount, testMatch);
    const approveuser = R.curry((matchkey, email) => testMatches.UpdateMatchPlayerType(testMatch, matchkey, email, 'approve')); 

    testMatch.remove({}).then(function(x) {

      // rules are as follows:
      // -- open games 
      // when time expires
      // -- postgame
      // when time expires or host expires
      // -- played 

      var lastweek = new Date();
      lastweek.setDate(lastweek.getDate() - 8);

      var yesterday = new Date();
      yesterday.setDate(yesterday.getDate() - 1);

      var justthen = new Date();
      justthen.setDate(moment(justthen).subtract({minutes: 1}) - 1);

      var tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);

      // create test matches
      Promise.all([
        generateMatchWDate('open', 3, 2, lastweek - 60*60*1000, lastweek), 
        generateMatchWDate('open', 3, 2, yesterday - 60*60*1000, yesterday), 
        generateMatchWDate('open', 3, 2, yesterday - 60*60*1000, yesterday), 
        generateMatchWDate('open', 3, 2, tomorrow - 60*60*1000, tomorrow), 
        generateUser({}), 
        generateUser({})
      ]).then(function(u) {
        Promise.all([
          testBoardgame.update({id: u[0][1].games[0].id}, {$set:{minplaytime:20, maxplaytime: 40}}), 
          testBoardgame.update({id: u[1][1].games[0].id}, {$set:{minplaytime:30, maxplaytime: 60}}), 
          testBoardgame.update({id: u[2][1].games[0].id}, {$set:{minplaytime:40, maxplaytime: 80}}), 
          testBoardgame.update({id: u[3][1].games[0].id}, {$set:{minplaytime:60, maxplaytime:140}})
        ]).then(function(y) {
          Promise.all([
            joinmatch(u[0][1].key, u[4].email, u[4].key),
            joinmatch(u[1][1].key, u[4].email, u[4].key),
            joinmatch(u[1][1].key, u[5].email, u[5].key),
            joinmatch(u[3][1].key, u[4].email, u[4].key)
          ]).then(function(j) {
            Promise.all([
              approveuser(u[0][1].key, u[4].email),
              approveuser(u[1][1].key, u[4].email),
              approveuser(u[1][1].key, u[5].email),
              approveuser(u[3][1].key, u[4].email)
              ]).then(function(k) {
                // reset all other issues
                R.sequence(Task.of, [ 
                  testMatches.TaskPlayMatch(testMatch, testAccount, nconf.get('smtp:user'), transporter), 
                  testMatches.TaskPlayMatch(testMatch, testAccount, nconf.get('smtp:user'), transporter), 
                  testMatches.TaskPlayMatch(testMatch, testAccount, nconf.get('smtp:user'), transporter) 
                ]).fork(
                  err => log.clos('taskerror', err),
                  data => {
                    expect(data.length).toBe(3);

                    // find data :: o -> a -> s
                    const finddata = R.curry((data, arr) => R.find(R.propEq('key', u[arr][1].key))(data));
                    // checkstatus :: a -> a => s
                    const checkstatus = (data) => R.compose(R.prop('status'), finddata(data));

                    R.sequence(Task.of, [
                      testAccounts.TaskAccountFromEmail(testAccount, u[0][0].email),
                      testAccounts.TaskAccountFromEmail(testAccount, u[1][0].email),
                      testAccounts.TaskAccountFromEmail(testAccount, u[2][0].email)
                    ]).fork(
                      err => log.clos('taskerror', err),
                      accounts => {
                        expect(accounts[0].profile.playxp).toBe(0);
                        expect(accounts[1].profile.playxp).toBe(0);
                        expect(accounts[2].profile.playxp).toBe(0);

                        // the tomorrow match should not appear
                        expect(map(checkstatus(data), [0, 1, 2])).toEqual(['postgame', 'postgame', 'postgame']);
                        testMatches.TaskPlayMatch(testMatch, testAccount, nconf.get('smtp:user'), transporter).fork(
                          err => {
                            expect(err).toBe('no match');

                            // reset all other issues
                            R.sequence(Task.of, [ 
                              testMatches.taskFinaliseMatch(testMatch, testAccount), 
                              testMatches.taskFinaliseMatch(testMatch, testAccount), 
                              testMatches.taskFinaliseMatch(testMatch, testAccount) 
                            ]).fork(
                              err => done(),
                              data => {

                                // the tomorrow match should not appear
                                expect(R.map(checkstatus(data), [0, 1, 2])).toEqual(['played', 'played', 'played']);
                                  () => R.sequence(Task.of, [
                                    testAccounts.TaskAccountFromEmail(testAccount, u[0][0].email),
                                    testAccounts.TaskAccountFromEmail(testAccount, u[1][0].email),
                                    testAccounts.TaskAccountFromEmail(testAccount, u[2][0].email)
                                  ]).fork(
                                    err => log.clos('taskerror', err),
                                    accounts => {
                                      expect(accounts[0].profile.playxp).toBe(60);
                                      expect(accounts[1].profile.playxp).toBe(60);
                                      expect(accounts[2].profile.playxp).toBe(60);
                                      done();
                                    }
                                  )

                              }
                            )
                          }, done()
                        )
                      }
                    );
                  }
                ); 
              });
            });
          });
      });
    });
  });


  it('TaskIncrementStatWhenNotHost', function(done) {
      
      var userOther = faker.Internet.email();
      var tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);

      generateMatchWDate('open', 3, 2, tomorrow - 60*60*1000, tomorrow).then(function(m) {
          var userHost = R.compose(R.prop('email'), R.head);
          var matchkey = R.compose(R.prop('key'), R.last);

          testMatches.TaskIncrementStatWhenNotHost('impressioncount', testMatch, userHost(m), matchkey(m)).fork(
              err => {},
              data => {
                  expect(data.impressioncount).toBe(0);
                  testMatches.TaskIncrementStatWhenNotHost('impressioncount', testMatch, userOther, matchkey(m)).fork(
                      err => {},
                      data => {
                          expect(data.impressioncount).toBe(1);
                          testMatches.TaskIncrementStatWhenNotHost('viewcount', testMatch, userHost(m), matchkey(m)).fork(
                              err => {},
                              data => {
                                  expect(data.viewcount).toBe(0);
                                  testMatches.TaskIncrementStatWhenNotHost('viewcount', testMatch, userOther, matchkey(m)).fork(
                                      err => {},
                                      data => {
                                          expect(data.viewcount).toBe(1);
                                          testMatches.TaskIncrementStatWhenNotHost('viewcount', testMatch, '', matchkey(m)).fork(
                                              err => {},
                                              data => {
                                                  expect(data.viewcount).toBe(2);
                                                  done();
                                              }
                                          )
                                      }
                                  ) 
                              }
                          ) 
                      }
                  ) 
              }
          ) 
      }); 
  });


  it('checkLocname', function(done) {
    expect(testMatches.checkLocName({})).toEqual({});
    expect(testMatches.checkLocName({playarea:'a'})).toEqual({locname: 'a'});
    expect(testMatches.checkLocName({playarea:'a', b:'c'})).toEqual({locname: 'a', b:'c'});
    expect(testMatches.checkLocName({b:'c'})).toEqual({b:'c'});
    done();
  });

  it('coords', function(done) {
    expect(testMatches.coords(2, 3)).toEqual({ loc : { type : 'Point', coordinates : [ 2, 3 ] } });
    expect(testMatches.coords(4, 5)).toEqual({ loc : { type : 'Point', coordinates : [ 4, 5 ] } });
    done();
  });

  it('checkLocLatLng', function(done) {
    expect(testMatches.checkLocLatLng({})).toEqual({});
    expect(testMatches.checkLocLatLng({b:'c'})).toEqual({b:'c'});
    expect(testMatches.checkLocLatLng({'playarealng': 3})).toEqual({'playarealng': 3});
    expect(testMatches.checkLocLatLng({'playarealat': 4})).toEqual({'playarealat': 4});
    expect(testMatches.checkLocLatLng({'playarealng': 3, 'playarealat': 4})).toEqual({ loc : { type : 'Point', coordinates : [ 3, 4 ] } });
    expect(testMatches.checkLocLatLng({b:'c', 'playarealng': 3, 'playarealat': 4})).toEqual({ b:'c', loc : { type : 'Point', coordinates : [ 3, 4 ] } });
    done();
  });

  it('whenHasDateAssoc', function(done) {
    expect(testMatches.whenHasDateAssoc('a', 'b', {})).toEqual({});
    expect(testMatches.whenHasDateAssoc('b', 'c', {b:'10 Mar 2020'})).toEqual({b:'10 Mar 2020', c:new Date('10 Mar 2020')});
    done();
  });

  it('dateify', function(done) {
    expect(testMatches.dateify({})).toEqual({});
    expect(testMatches.dateify({date:'10 Mar 2020'})).toEqual({date:'10 Mar 2020', gamedate: new Date('10 Mar 2020')});
    expect(testMatches.dateify({date:'10 Mar 2020', timeend: '12 Mar 2020'})).toEqual({date:'10 Mar 2020', gamedate: new Date('10 Mar 2020'), timeend: new Date('12 Mar 2020')});
    done();
  });

  it('TaskUpdateMatch', function(done) {

    // createAcceptUserObj :: {u} => {a}
    const createAcceptUserObj = (user) => zipObj(['email', 'type', 'avataricon', 'name', 'playerkey'], [user.email, 'approve', '', user.profile.name, aguid(user.email)]);
    
    // addMatchUsers :: (k -> {u}) => Schema(m) 
    const addMatchUsers = (matchkey, user) => testMatch.findOneAndUpdate({key: matchkey}, {$push: {players: createAcceptUserObj(user)}}, {new:true}).exec(); 

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    const updateMatch = {title: 't', date: tomorrow, timeend: moment(tomorrow).add(2, 'h')};
    const updateMatchBadDate = {title: 't', date: moment(tomorrow).add(2, 'h'), timeend: tomorrow};

    generateMatch('open', 3, 2).then(function(m) {
      testMatches.TaskUpdateMatch(testMatch, m[1].key, updateMatchBadDate).fork(
        err => {
          testMatches.TaskUpdateMatch(testMatch, m[1].key, updateMatch).fork(
            err => log.clos('err', err),
            data => {
              expect(data.date - updateMatch.date).toEqual(0);
              expect(data.timeend - updateMatch.timeend).toEqual(0);
              expect(data.title).toEqual(updateMatch.title);
              Promise.all([generateUser({}), generateUser({})]).then(function(u) {
                Promise.all([addMatchUsers(m[1].key, u[0]), addMatchUsers(m[1].key, u[1])]).then(function(z) {

                  // update min to be less than max
                  const seatMaxLtMin = {seatmin: 3, seatmax: 2};
                  testMatches.TaskUpdateMatch(testMatch, m[1].key, seatMaxLtMin).fork(
                    err => {
                      // update max to be less than number of people 
                      const seatMaxLtPlayers = {seatmin: 1, seatmax: 2};
                      testMatches.TaskUpdateMatch(testMatch, m[1].key, seatMaxLtPlayers).fork(
                        err => {
                          // update max to be less than number of people 
                          const seatMaxAtPlayers = {seatmin: 1, seatmax: 3};
                          testMatches.TaskUpdateMatch(testMatch, m[1].key, seatMaxAtPlayers).fork(
                            err => {},
                            data => {
                              expect(data.isfull).toBe(true);
                              expect(data.status).toBe('open');
                              expect(data.seatavailable).toBe(0);

                              // update max to be less than number of people 
                              const seatMaxIncreaseAfterPlayers = {seatmin: 1, seatmax: 5};
                              testMatches.TaskUpdateMatch(testMatch, m[1].key, seatMaxIncreaseAfterPlayers).fork(
                                err => {},
                                data => {
                                  expect(data.status).toBe('open');
                                  expect(data.seatavailable).toBe(2);
                                  done();
                                }
                              )
                            }
                          )
                        },
                        data => {}
                      )
                    },
                    data => {}
                  )
                })
              });
            }
          )
      });
    });
  });

  it('addGameObj', function(done) {
    expect(testMatches.addGameObj({}, "e", 'select')({id: 20})).toEqual({ $push : { games : { username : 'No Name', avataricon : '/img/default-avatar-sq.jpg', vote:[ { email : 'e', userkey : 'No Name', username : 'No Name', avataricon : '/img/default-avatar-sq.jpg' } ], email : 'e', status : 'select', id : 20 } } });
    expect(testMatches.addGameObj({profile:{name: "n", avataricon: "a"}}, "f", 'somestatus')({id: 30})).toEqual({ $push : { games : { username : 'n', avataricon : 'a', email : 'f', vote : [ { email : 'f', userkey : 'No Name', username : 'n', avataricon : 'a' } ], status : 'somestatus', id : 30 } } });
    done();
  });

  it('TaskAddGameToMatch + TaskRemoveGameFromMatch', function(done) {
    const email = "test@website.com";

    generateUser({}).then(function(u) {
      generateUser({}).then(function(u2) {
        Promise.all([generateGame, generateMatch('open', 3, 2)]).then(function(m) {
          testMatches.TaskAddGameToMatch(testAccount, testMatch, m[1][1].key, 'autosuggest', m[0].id, m[0].name, m[0].thumbnail, u.email).fork(
            err => log.clos('err', err),
            data => {
              expect(data.key).toBe(m[1][1].key);
              expect(R.last(data.games).id).toBe(m[0].id);
              expect(R.last(data.games).name).toBe(m[0].name);
              expect(R.last(data.games).thumbnail).toBe(m[0].thumbnail);
              expect(R.last(data.games).email).toBe(u.email);
              expect(R.last(data.games).avataricon).toBe(u.profile.avataricon);
              expect(R.last(data.games).username).toBe(u.profile.name);
              expect(R.last(data.games).status).toBe('autosuggest');

              testMatches.taskBringSuggestedGameToMatch(testAccount, testMatch, m[1][1].key, R.last(data.games).pageslug, u2.email).fork( 
                err => log.clos('err', err),
                data => {

                  expect(R.last(data.games).status).toBe('select');
                  expect(R.last(data.games).email).toBe(u2.email);
                  expect(R.last(data.games).username).toBe(u2.profile.name);
                  expect(R.last(data.games).avataricon).toBe(u2.profile.avataricon);

                  testMatches.TaskRemoveGameFromMatch(testMatch, data.key, data.games[1].pageslug).fork(
                    err => log.clos('err', err),
                    data => {
                      expect(data.key).toBe(m[1][1].key);
                      expect(data.games.length).toBe(1);
                      done();
                    }
                  )
                }
              )
            }
          )
        })
      })
    })
  });

  it('taskIncrementExtraPlayer', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var status = ['open'];

    const joinMatch = (matchkey) => testMatches.JoinMatch(testAccount, testMatch, matchkey);
    const userJoinMatch = R.curry((matchkey, user) => converge(joinMatch(matchkey), [prop('email'), prop('key')])(user));

    Promise.all([generateHostUser({}), generateUser({})]).then(function(u) {

      var matchA = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 6, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, [], false);

      matchA.then(function(m) {
        userJoinMatch(m.key, u[1]).then(function(newmatch) {
          testMatches.ApproveUser(testAccount, testMatch, m.key, u[0].email, u[0].key, newmatch.players[1].playerkey).then((m) => {
            testMatches.taskIncrementExtraPlayers(testAccount, testMatch, m.key, u[1].email, 'whatkey', 1).fork(
              err => {
                testMatches.taskIncrementExtraPlayers(testAccount, testMatch, m.key, u[1].email, u[1].key, 1).fork(
                  err => {},
                  data => {
                    expect(data.players[1].extraplayers).toBe(1);
                    expect(data.seatavailable).toBe(3);
                    testMatches.taskIncrementExtraPlayers(testAccount, testMatch, m.key, u[0].email, u[0].key, 3).fork(
                      err => {},
                      data => {
                        expect(data.players[0].extraplayers).toBe(3);
                        expect(data.seatavailable).toBe(0);
                        expect(data.isfull).toBe(true);

                        testMatches.RemoveMatchPlayerkey(testMatch, data.key, data.players[1].playerkey).then((x) => {
                          expect(x.seatavailable).toBe(2);
                          expect(x.isfull).toBe(false);
                          done();

                        }).catch((err) => {
                          log.clos('err', err);
                        });
                      }
                    );
                  }
                );
              },
              data => {}
            );
          });
        })

      }).catch(function(err) {
        log.clos('err', err);
      });
    });
  });

  it('findDate', function(done) {
    expect(testMatches.findDate('b', [{id: 'abc', timeslot:'xyz'}], 'timeslot').isJust).toBeFalsy();
    expect(testMatches.findDate('abc', [{id: 'abc', timeslot:'xyz'}], 'timezzslot').isJust).toBeFalsy();
    expect(testMatches.findDate('abc', [{id: 'abc', timeslot:'xyz'}], 'timeslot').get()).toBe('xyz');
    expect(testMatches.findDate('abc', [{id: 'abc', time: 'abc', slot:'xyz'}], 'slot').get()).toBe('xyz');
    expect(testMatches.findDate('abc', [{id: 'abc', time: 'abc', slot:'xyz'}], 'time').get()).toBe('abc');
    done();
  });

  it('assignDates', function(done) {

    var playyear = new Date().getFullYear() + 1;
    var gameDate = new Date(playyear, 08, 03, 20, 30, 0); 
    var status = ['open'];

    Promise.all([generateHostUser({}), generateUser({})]).then(function(u) {

      var matchA = buildHost(testMatch, testAccount, u[0].email, u[0].key, gameDate, 6, 2, status[0], {}, u[0].hostareas[0].name, u[0].hostareas[0].loc.coordinates, [], true);

      matchA.then(function(m) {
        //expect(R.isNil(m.date)).toBeTruthy();
        //expect(R.isNil(m.timeend)).toBeTruthy();

        // test user who isn't in the match, should fail
        testMatches.assignDates(testAccount, testMatch, m.key, m.timeslots[0].id, u[1].email, u[1].key).fork(
          err => {

            // test by the host, it should pass
            testMatches.assignDates(testAccount, testMatch, m.key, m.timeslots[0].id, u[0].email, u[0].key).fork(
              err => {},
              data => {
                expect(data.date).toEqual(m.timeslots[0].startdatetime);
                expect(data.timeend).toEqual(m.timeslots[0].enddatetime);
                expect(data.activeTimeslotKey).toEqual(m.timeslots[0].id);
                done();
              }
            );
          }
        );

      }).catch(function(err) {
        log.clos('err', err);
      });
    });
  });

  describe('match discussion', function() {

    it('EntryFee', function(done) {
      expect(testMatches.EntryFee(3, 'AUD')).toBe('$3.00');
      expect(testMatches.EntryFee(3.5, 'GBP')).toBe('£3.50');
      expect(testMatches.EntryFee(20.5, 'USD')).toBe('$20.50');
      expect(testMatches.EntryFee(5, 'EUR')).toBe('5,00 €');
      done();
    });


    it('findPlayerInMatch', function(done) {
      expect(testMatches.findPlayerInMatch('a')({}).isJust).toBeFalsy();
      expect(testMatches.findPlayerInMatch('a')([]).isJust).toBeFalsy();
      expect(testMatches.findPlayerInMatch('a')([{players: []}]).isJust).toBeFalsy();
      expect(testMatches.findPlayerInMatch('a')([{players: [{email: 'b'}]}]).isJust).toBeFalsy();
      expect(testMatches.findPlayerInMatch('a')([{players: [{email: 'a'}]}]).isJust).toBeTruthy();
      done();
    });

    it('findHostInMatch', function(done) {
      expect(testMatches.findHostInMatch('a')({}).isJust).toBeFalsy();
      expect(testMatches.findHostInMatch('a')([]).isJust).toBeFalsy();
      expect(testMatches.findHostInMatch('a')([{players: []}]).isJust).toBeFalsy();
      expect(testMatches.findHostInMatch('a')([{players: [{email: 'b'}, {email: 'a'}]}]).isJust).toBeFalsy();
      expect(testMatches.findHostInMatch('a')([{players: [{email: 'a'}, {email: 'b'}]}]).isJust).toBeTruthy();
      done();
    });

    it('whenMatchHasVote', function(done) {
      expect(testMatches.whenMatchHasVote('e', 1)({}).isJust).toBeFalsy();
      expect(testMatches.whenMatchHasVote('e', 1)([]).isJust).toBeFalsy();
      expect(testMatches.whenMatchHasVote('e', 1)([{games:[]}]).isJust).toBeFalsy();
      expect(testMatches.whenMatchHasVote('e', 1)([{games:[{id: 1}]}]).isJust).toBeFalsy();
      expect(testMatches.whenMatchHasVote('e', 1)([{games:[{id: 1, vote:[]}]}]).isJust).toBeFalsy();
      expect(testMatches.whenMatchHasVote('e', 1)([{games:[{id: 1, vote:[{email:'e'}]}]}]).getOrElse(false)).toBeTruthy();
      done();
    });


    it('pushVote', function(done) {
      expect(testMatches.pushVote(1, 'a', 'k', 'n', 'a', [])).toEqual([{email: 'a', userkey:'k', username:'n', avataricon:'a', direction: 1}]);
      expect(testMatches.pushVote(2, 'b', 'uk', 'un', 'ai', [])).toEqual([{email: 'b', userkey:'uk', username:'un', avataricon:'ai', direction: 2}]);
      done();
    });

    it('addMatchVotes', function(done) {
      expect(testMatches.addMatchVotes('e', 'key', 'n', 'ai', 1, 20)([]).isJust).toBeFalsy();
      expect(testMatches.addMatchVotes('e', 'key', 'n', 'ai', 1, 20)([{games:[]}]).isJust).toBeFalsy();
      expect(testMatches.addMatchVotes('e', 'key', 'n', 'ai', 1, 20)([{games:[{id:20}]}]).isJust).toBeFalsy();

      expect(testMatches.addMatchVotes('e', 'key', 'n', 'ai', 1, 20)([{games:[{id:20, vote:[]}]}]).get()).toEqual({ '$set' : { 'games.$.vote' : [ { email : 'e', userkey:'key', username:'n', avataricon:'ai', direction : 1 } ] } });

      done();
    });

    it('removeMatchVotes', function(done) {
      expect(testMatches.removeMatchVotes('e', 20)([]).isJust).toBeFalsy();
      expect(testMatches.removeMatchVotes('e', 20)([{games:[]}]).isJust).toBeFalsy();
      expect(testMatches.removeMatchVotes('e', 20)([{games:[{id:20}]}]).isJust).toBeFalsy();
      expect(testMatches.removeMatchVotes('f', 20)([{games:[{id:20, vote:[{ email : 'e', direction : 1 } ]}]}]).get()).toEqual({ '$set' : { 'games.$.vote' : [{ email : 'e', direction : 1 }] } });
      expect(testMatches.removeMatchVotes('e', 20)([{games:[{id:20, vote:[{ email : 'e', direction : 1 } ]}]}]).get()).toEqual({ '$set' : { 'games.$.vote' : [ ] } });
      done();
    });


    it('voteForGame', function(done) {
      generateMatch('open', 3, 2).then(function(m) {
        testMatches.voteForGame(testAccount, testMatch, m[1].key, m[1].games[0].id, m[0].email, m[0].key).fork(
          err => {},
          data => {
            expect(data.games[0].vote.length).toBe(1);
            expect(data.games[0].vote[0].votetype).toBe('wtpplay');
            expect(data.games[0].vote[0].direction).toBe(1);
            expect(data.games[0].vote[0].email).toBe(m[0].email);

            testMatches.unvoteForGame(testAccount, testMatch, m[1].key, m[1].games[0].id, m[0].email, m[0].key).fork(
              err => {},
              data => {
                expect(data.games[0].vote.length).toBe(0);
                done();

              }
            )
          }
        )
      });
    });

    it('joinGame', function(done) {
      generateGame.then(function(g) {
        generateMatch('open', 3, 2).then(function(m) {
          testMatches.TaskAddGameToMatch(testAccount, testMatch, m[1].key, 'autosuggest', g.id, g.name, g.thumbnail, m[0].email).fork(

            err => log.clos('err.AddGame', err),
            eventWGame => {
              testMatches.joinGame(testAccount, testMatch, m[1].key, eventWGame.games[0].gamekey, m[0].email, m[0].key).fork(
                err => {},
                data => {
                  expect(data.games[0].join.length).toBe(1);
                  expect(data.games[0].join[0].jointype).toBe('wtpplay');
                  expect(data.games[0].join[0].direction).toBe(1);
                  expect(data.games[0].join[0].email).toBe(m[0].email);

                  testMatches.joinGame(testAccount, testMatch, m[1].key, eventWGame.games[1].gamekey, m[0].email, m[0].key).fork(
                    err => {
                      expect(err).toBe(409);
                      testMatches.leaveGame(testAccount, testMatch, m[1].key, eventWGame.games[0].gamekey, m[0].email, m[0].key).fork(
                        err => {},
                        data => {
                          expect(data.games[0].join.length).toBe(0);
                          done();

                        }
                      );

                    },
                    data => {
                      /*
                      expect(data.games[1].join.length).toBe(1);
                      expect(data.games[1].join[0].jointype).toBe('wtpplay');
                      expect(data.games[1].join[0].direction).toBe(1);
                      expect(data.games[1].join[0].email).toBe(m[0].email);
                      */

                    });
                }
              )
            });
        });
      });
    });


    it('publishMatch', function(done) {

      const startdate = moment().add(5, 'days').toDate();
      const enddate   = moment().add(5, 'days').add(3, 'hours').toDate();

      Promise.all([
        generateMatch('draft', 3, 2),
        generateMatch('draft', 3, 2)
      ]).then(function(m) {
        expect(m[0][1].status).toBe('draft');
        expect(m[1][1].status).toBe('draft');
        R.sequence(Task.of, [
          testMatches.publishMatch(testAccount, testMatch, m[0][1].key, m[0][0].email, m[0][0].key, {isprivate: true, date: startdate, timeend: enddate}), 
          testMatches.publishMatch(testAccount, testMatch, m[1][1].key, m[1][0].email, m[1][0].key, {isprivate: false}),
        ]).fork(
          err => {},
          data => {
            expect(data[0].status).toBe('open');
            expect(data[0].isprivate).toBe(true);

            expect(new Date(data[0].date).getUTCDate()).toBe(startdate.getUTCDate());
            expect(new Date(data[0].timeend).getUTCDate()).toBe(enddate.getUTCDate());

            expect(data[1].status).toBe('open');
            expect(data[1].isprivate).toBe(false);
            testMatches.publishMatch(testAccount, testMatch, m[0][1].key, false, m[1][0].email, m[1][0].key).fork(
              err => {
                done();
              },
              data => {}
            )
          }
        )
      });
    });

    it('FindMatchByUser', function(done) {
      Promise.all([generateMatch('open', 3, 2), generateUser({}), generateUser({})]).then(function(u) {
        testMatches.FindMatchByUser(testMatch, u[1].email).fork(
          err => log.clos('FindMatchByUser.err', err),
          data => {
            expect(data.length).toBe(0);
            testMatches.FindMatchByUser(testMatch, u[0][0].email).fork(
              err => log.clos('FindMatchByUser.err', err),
              data => {
                expect(data.length).toBe(1);
                done();
              }
            )
            done();
          }
        )
      })
    });

    it('findExpansions', function(done) {
      expect(testMatches.findExpansions('a', {})).toEqual([]);
      expect(testMatches.findExpansions('a', [])).toEqual([]);
      expect(testMatches.findExpansions('a', [{}])).toEqual([]);
      expect(testMatches.findExpansions('a', [{assignedexpansions:[]}], 'a')).toEqual([]);

      expect(testMatches.findExpansions('a', [{assignedexpansions:[{gamepageslug:'a'}]}])).toEqual([{gamepageslug:'a'}]);
      expect(testMatches.findExpansions('a', [{assignedexpansions:[{gamepageslug:'a'}, {gamepageslug:'b'}]}])).toEqual([{gamepageslug:'a'}]);
      expect(testMatches.findExpansions('b', [{assignedexpansions:[{gamepageslug:'a'}, {gamepageslug:'b'}]}])).toEqual([{gamepageslug:'b'}]);
      done();

    });

    it('taskAddExpansion', function(done) {
      Promise.all([generateGame, generateMatch('open', 3, 2), generateGame]).then(function(m) {

        var u = m[1][0]

        testMatches.TaskAddGameToMatch(testAccount, testMatch, m[1][1].key, 'select', m[0].id, m[0].name, m[0].thumbnail, u.email).fork(
          err => log.clos('err', err),
          data => {
            testMatches.taskReadExpansions(testMatch, m[1][1].key, data.games[0].pageslug).fork(
              err => log.clos('err', err),
              noexpansions => {
                expect(noexpansions.length).toBe(0);
                testMatches.taskAddExpansion(testAccount, testMatch, m[1][1].key, u.email, u.key, u.pageslug, data.games[0].pageslug, m[2].toObject()).fork(
                  err => log.clos('err', err),
                  newexpansion => {
                    expect(newexpansion[0].id).toBe(m[2].id);
                    expect(newexpansion[0].email).toBe(u.email);
                    expect(newexpansion[0].userpageslug).toBe(u.pageslug);
                    testMatches.taskReadExpansions(testMatch, m[1][1].key, data.games[0].pageslug).fork(
                      err => log.clos('err', err),
                      hasexpansion => {
                        expect(hasexpansion.length).toBe(1);
                        expect(hasexpansion[0].id).toBe(m[2].id);
                        expect(hasexpansion[0].userpageslug).toBe(u.pageslug);

                        testMatches.taskRemoveExpansion(testAccount, testMatch, m[1][1].key, u.email, u.key, data.games[0].pageslug, m[2].id).fork(
                          err => log.clos('err', err),
                          unexpansion => {
                            expect(unexpansion.length).toBe(0);
                            testMatches.taskReadExpansions(testMatch, m[1][1].key, data.games[0].pageslug).fork(
                              err => log.clos('err', err),
                              checkunexpansion => {
                                expect(checkunexpansion.length).toBe(0);
                                done();
                              }
                            );
                          }
                        );
                      }
                    )
                  }
                )
              }
            )
          }
        )
      })
    });

		it('match discussion', function(done) {
			expect(testMatches.findInterestedEmail({}).isJust).toBeFalsy;
			expect(testMatches.findInterestedEmail({players:[]}).isJust).toBeFalsy;
			expect(testMatches.findInterestedEmail({players:[{type: 'a'}, {type: 'interested'}]}).isJust).toBeFalsy;
			expect(testMatches.findInterestedEmail({players:[{type: 'a', email:'e'}, {type: 'interested', email:'b'}]}).get()[0]).toBe('b');
			done();
		});


    it('addJoinGame', function(done) {
      expect(testMatches.addJoinGame('e', 'key', 'n', 'ai', 1, 'k')([]).isJust).toBeFalsy();
      expect(testMatches.addJoinGame('e', 'key', 'n', 'ai', 1, 'k')([{games:[]}]).isJust).toBeFalsy();
      expect(testMatches.addJoinGame('e', 'key', 'n', 'ai', 1, 'k')([{games:[{gamekey:'k'}]}]).isJust).toBeFalsy();
      expect(testMatches.addJoinGame('e', 'key', 'n', 'ai', 1, 'k')([{games:[{gamekey:'k', join:[]}]}]).get()).toEqual({ '$set' : { 'games.$.join' : [ { email : 'e', userkey:'key', username:'n', avataricon:'ai', direction : 1 } ] } });
      done();
    });

    it('joinLength', function(done) {
      expect(isNaN(testMatches.joinLength({}))).toBe(true);
      expect(testMatches.joinLength({join:[]})).toBe(0);
      expect(testMatches.joinLength({join:['a', 'b']})).toBe(2);
      done();
    });

    it('calcBaseGamePlayers', function(done) {
      expect(testMatches.calcBaseGamePlayers('abc', {}).isJust).toBeFalsy();
      expect(testMatches.calcBaseGamePlayers('abc', {games:[]}).isJust).toBeFalsy();
      expect(testMatches.calcBaseGamePlayers('abc', {games:[{gamekey: 'abc', join:[1,2,3], maxplayers: 3}, {gamekey: 'xyz', maxplayers: 3, join:[4]}, {gamekey: 'def', maxplayers: 3}]}).get()).toBeTruthy();
      expect(testMatches.calcBaseGamePlayers('def', {games:[{gamekey: 'abc', join:[1,2,3], maxplayers: 3}, {gamekey: 'xyz', maxplayers: 3}, {gamekey: 'def', maxplayers: 3, join:[4]}]}).get()).toBeFalsy();
      expect(testMatches.calcBaseGamePlayers('xyz', {games:[{gamekey: 'abc', join:[1,2,3], maxplayers: 3}, {gamekey: 'xyz', maxplayers: 3}, {gamekey: 'def', maxplayers: 3, join:[4]}]}).get()).toBeFalsy();
      done();
    });

    it('findBaseGameJoinLength', function(done) {
      expect(testMatches.findBaseGameJoinLength('abc', {games:[]}).isJust).toBeFalsy();
      expect(testMatches.findBaseGameJoinLength('abc', {games:[{gamekey: 'abc', join:[1,2,3], maxplayers: 3}, {gamekey: 'xyz', maxplayers: 3, join:[4]}, {gamekey: 'def', maxplayers: 3}]}).get()).toBe(3);
      expect(testMatches.findBaseGameJoinLength('def', {games:[{gamekey: 'abc', join:[1,2,3], maxplayers: 3}, {gamekey: 'xyz', maxplayers: 3}, {gamekey: 'def', maxplayers: 3, join:[4]}]}).get()).toBe(1);
      expect(testMatches.findBaseGameJoinLength('xyz', {games:[{gamekey: 'abc', join:[1,2,3], maxplayers: 3}, {gamekey: 'xyz', maxplayers: 3}, {gamekey: 'def', maxplayers: 3, join:[4]}]}).get()).toBeFalsy();
     expect(testMatches.findBaseGameJoinLength('abc', {}).isJust).toBeFalsy();
      done();
    });

    it('findBaseGamePageSlug', function(done) {
      expect(testMatches.findBaseGamePageSlug('abc', {games:[]}).isJust).toBeFalsy();
      expect(testMatches.findBaseGamePageSlug('abc', {games:[{gamekey: 'abc', join:[1,2,3], maxplayers: 3, pageslug: 'this'}, {gamekey: 'xyz', maxplayers: 3, join:[4], pageslug: 'that'}, {gamekey: 'def', maxplayers: 3, pageslug:'theother'}]}).get()).toBe('this');
      expect(testMatches.findBaseGamePageSlug('def', {games:[{gamekey: 'abc', join:[1,2,3], maxplayers: 3, pageslug: 'this'}, {gamekey: 'xyz', maxplayers: 3, join:[4], pageslug: 'that'}, {gamekey: 'def', maxplayers: 3, pageslug:'theother'}]}).get()).toBe('theother');
      expect(testMatches.findBaseGamePageSlug('xyz', {games:[{gamekey: 'abc', join:[1,2,3], maxplayers: 3, pageslug: 'this'}, {gamekey: 'xyz', maxplayers: 3, join:[4]}, {gamekey: 'def', maxplayers: 3, pageslug:'theother'}]}).isJust).toBeFalsy();
      done();
    });

    it('findBaseGamePageSlug', function(done) {
      expect(testMatches.calcExpansionGamePlayers('abc', {assignedexpansions:[]}).isJust).toBeFalsy();
      expect(testMatches.calcExpansionGamePlayers('test', {assignedexpansions:[{gamepageslug:'test', maxplayers:2 }]}).get()).toBe(2);
      expect(testMatches.calcExpansionGamePlayers('test', {assignedexpansions:[{gamepageslug:'test', maxplayers:2 }, {gamepageslug:'test', maxplayers:8 }]}).get()).toBe(8);
      done();
    });

    it('isGameFull', function(done) {
      expect(testMatches.isGameFull('a', []).isJust).toBeFalsy();
      expect(testMatches.isGameFull('a', [{games:[]}]).isJust).toBeFalsy();
      expect(testMatches.isGameFull('a', [{games: [{gamekey:'b'}]}]).isJust).toBeFalsy();

       expect(testMatches.isGameFull('a', [{games: [{gamekey:'a', maxplayers:2, join:['a']}]}]).get()).toBeFalsy();
       expect(testMatches.isGameFull('a', [{games: [{gamekey:'a', maxplayers:2, join:['a', 'b']}]}]).get()).toBeTruthy();
       expect(testMatches.isGameFull('a', [{games: [{gamekey:'a', maxplayers:2, join:['a', 'b', 'c']}]}]).get()).toBeTruthy();

       expect(testMatches.isGameFull('a', [{games: [{pageslug: 'xyz', gamekey:'a', maxplayers:2, join:['a', 'b']}], assignedexpansions:[{gamepageslug: 'xyz', maxplayers: 2}, {gamepageslug: 'xyz', maxplayers: 3}]}]).get()).toBeFalsy();
       expect(testMatches.isGameFull('a', [{games: [{pageslug: 'xyz', gamekey:'a', maxplayers:2, join:['a', 'b']}], assignedexpansions:[{gamepageslug: 'abc', maxplayers: 2}, {gamepageslug: 'abc', maxplayers: 3}]}]).get()).toBeTruthy();
       expect(testMatches.isGameFull('a', [{games: [{pageslug: 'xyz', gamekey:'a', maxplayers:2, join:['a', 'b']}], assignedexpansions:[{gamepageslug: 'xyz', maxplayers: 2}, {gamepageslug: 'abc', maxplayers: 3}]}]).get()).toBeTruthy();
       expect(testMatches.isGameFull('a', [{games: [{pageslug: 'xyz', gamekey:'a', maxplayers:2, join:['a', 'b', 'c']}], assignedexpansions:[{gamepageslug: 'xyz', maxplayers: 2}, {gamepageslug: 'xyz', maxplayers: 3}]}]).get()).toBeTruthy();
       expect(testMatches.isGameFull('a', [{games: [{pageslug: 'xyz', gamekey:'a', maxplayers:2, join:['a', 'b', 'c']}], assignedexpansions:[{gamepageslug: 'abc', maxplayers: 2}, {gamepageslug: 'xyz', maxplayers: 3}]}]).get()).toBeTruthy();

      done()
    });

    it('whenHasJoinGame', function(done) {
      expect(testMatches.whenHasJoinGame('e', 'k')({}).isJust).toBeFalsy();
      expect(testMatches.whenHasJoinGame('e', 'k')([]).isJust).toBeFalsy();
      expect(testMatches.whenHasJoinGame('e', 'k')([{games:[]}]).isJust).toBeFalsy();
      expect(testMatches.whenHasJoinGame('e', 'k')([{games:[{gamekey: 'k'}]}]).isJust).toBeFalsy();
      expect(testMatches.whenHasJoinGame('e', 'k')([{games:[{gamekey: 'k', join:[]}]}]).isJust).toBeFalsy();
      expect(testMatches.whenHasJoinGame('e', 'k')([{games:[{gamekey: 'k', join:[{email:'e'}]}]}]).getOrElse(false)).toBeTruthy();
      expect(testMatches.whenHasJoinGame('e', 'l')([{games:[{gamekey: 'k', join:[{email:'e'}]}]}]).getOrElse(false)).toBeTruthy();
      done();
    });

    it('removeJoinGame', function(done) {
      expect(testMatches.removeJoinGame('e', 'k')([]).isJust).toBeFalsy();
      expect(testMatches.removeJoinGame('e', 'k')([{games:[]}]).isJust).toBeFalsy();
      expect(testMatches.removeJoinGame('e', 'k')([{games:[{gamekey:'k'}]}]).isJust).toBeFalsy();
      expect(testMatches.removeJoinGame('f', 'k')([{games:[{gamekey:'k', join:[{ email : 'e', direction : 1 } ]}]}]).get()).toEqual({ '$set' : { 'games.$.join' : [{ email : 'e', direction : 1 }] } });
      expect(testMatches.removeJoinGame('e', 'k')([{games:[{gamekey:'k', join:[{ email : 'e', direction : 1 } ]}]}]).get()).toEqual({ '$set' : { 'games.$.join' : [ ] } });
      done();
    });


  });
});

describe('close server', function() {
  it('connection close', function(done) {
    mongo.CloseDatabase();
    done();
  });
});
