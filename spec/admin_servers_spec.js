var express = require("express"); 
// var router = express.Router(); 
var R = require('ramda'); 
var log = require('../app/log.js'); 
var api_admin = require('../app/admin_service.js');
var mongo = require('../app/mongoconnect.js');
var nconf = require('nconf');
var ajax = require('./ajax.js');
var genhelper = require('./genhelper.js');
var account = require('../app/models/account.js');

// ============== VARS ===============
var dbUri = nconf.get('database:uri');
var httpPort = 3294;
var url_base = "http://localhost:" + httpPort;
var url_api = '/api/v1/admin';

var server;
var testapp = express();

// ============= LAMDA ===============

// ajaxData :: a -> o
const ajaxData = R.zipObj(['url', 'method', 'data']);

// apiEndPoint :: s -> s -> s -> s
const apiAdminEndpoint = (base, api, endpoint) => base + api + '/' + endpoint;

// ============== AJAX ===============
var ajaxPromise = ajax.AjaxPromise;

// ============== RUN TESTS ================== 
describe("admin api", function() {

  it("setup server", function(done) {
    mongo.ConnectDatabase(dbUri);
    var router = api_admin.SetupRouter();
    testapp.use(url_api, router);

    server = testapp.listen(httpPort, function() {
      done();
    });
  });


  describe("read basic data", function() {

    it("open admin page", function(done) {

      var generateUser = genhelper.GenerateUser(account);
      var generateHostUser = genhelper.GenerateHostUser(account);

      const endpoint = "/netpromoterscore";

      // ajaxAdmin :: s -> s -> s -> o
      const ajaxAdmin = (base, api, endpoint, email, key) => ajaxData([
        apiAdminEndpoint(base, api, endpoint),
        'GET',
        {
          email : email,
          key   : key
        }
      ]);

      Promise.all([generateHostUser({premium:{isstaff:true}}), generateUser({})]).then(function(users) {
        Promise.all(R.map(ajaxPromise, [
          ajaxAdmin(url_base, url_api, endpoint, users[0].email, users[0].key),
          ajaxAdmin(url_base, url_api, endpoint, users[1].email, users[1].key)
        ]))
          .then(function(data) {

            expect(data[0].statusCode).toBe(200);
            expect(R.has('body', data[0])).toBeTruthy();
            expect(R.has('userscount', data[0].body)).toBeTruthy();
            expect(R.has('promoters', data[0].body)).toBeTruthy();
            expect(R.has('detractors', data[0].body)).toBeTruthy();
            expect(R.has('netpromoterscore', data[0].body)).toBeTruthy();

            expect(R.has('whymanagemyevent', data[0].body)).toBeTruthy();
            expect(R.has('whyfindlocalgamers', data[0].body)).toBeTruthy();
            expect(R.has('whyfindlocalevents', data[0].body)).toBeTruthy();
            expect(R.has('whymanageboardgames', data[0].body)).toBeTruthy();

            expect(data[1].statusCode).toBe(401);

            done()

          });
      });
    });

    it("close server", function(done) {
      server.close(function() {
        mongo.CloseDatabase();
        done();
      });
    });


  })
});
