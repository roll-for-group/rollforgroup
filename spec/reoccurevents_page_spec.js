var Promise = require('bluebird');
var R = require('ramda');
var util = require('util');
var log = require('../app/log.js');

var passport = require('passport');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var reoccurevents_page = require('../app/reoccurevents_page.js');

var genhelper = require('./genhelper.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var browser2 = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');
var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');
var boardgame = require('../app/models/boardgame.js');
var boardgames = require('../app/models/boardgames.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');

var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 7931;
var login = require('connect-ensure-login');

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);


// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  testapp.close(function(){
    console.log('reoccurevents_page_spec closed on port ' + httpPort);
  });
};

describe('reoccurevents_page.js', function() {
  describe('UNIT Tests', function() {

		it('getHostButtons', function(done) {
			expect(reoccurevents_page.GetHostButtons('played')).toEqual({ buttons : [ { text : 'Delete History', faicon : 'ban', style : 'danger', id : 'disband', title : 'Delete this event' } ] });
			expect(reoccurevents_page.GetHostButtons('open')).toEqual({ buttons : [ { text : 'Disband', faicon : 'ban', style : 'danger', id : 'disband', title : 'Disband this event' } ] });
			expect(reoccurevents_page.GetHostButtons('draft')).toEqual({ buttons : [ { text : 'Cancel', faicon : 'ban', style : 'danger', id : 'disband', title : 'Cancel this event' } ] });
			done();

		});

		it('matchHasUser', function(done) {
			expect(reoccurevents_page.MatchHasUser('a')({})).toBe(false);
			expect(reoccurevents_page.MatchHasUser('a')({players:[]})).toBe(false);
			expect(reoccurevents_page.MatchHasUser('a')({players:[{email:'a'}]})).toBe(true);
			done();
		});

		it("MatchIsJoinable", function(done) {
			expect(reoccurevents_page.MatchIsJoinable({isfull:false, status:'postgame'})).toBe(false);
			expect(reoccurevents_page.MatchIsJoinable({isfull:false, status:''})).toBe(true);
			expect(reoccurevents_page.MatchIsJoinable({isfull:true, status:''})).toBe(false);
			done();
		});

		it("MergQuadrant", function(done) {
			expect(reoccurevents_page.MergeQuadrant({key:'a'})).toEqual({key:'a', gameQuad2: true})
			expect(reoccurevents_page.MergeQuadrant({key:'b'})).toEqual({key:'b', gameQuad3: true})
			expect(reoccurevents_page.MergeQuadrant({key:'c'})).toEqual({key:'c', gameQuad0: true})
			expect(reoccurevents_page.MergeQuadrant({key:'d'})).toEqual({key:'d', gameQuad1: true})
			done();
		});

		it('findReoccurEveryVal', function(done) {
			expect(reoccurevents_page.findReoccurEveryVal({})).toBe('-1');
			expect(reoccurevents_page.findReoccurEveryVal({reoccur:{enable:{}}})).toEqual('-1');
			expect(reoccurevents_page.findReoccurEveryVal({reoccur:{enable:true}})).toEqual('-1');
			expect(reoccurevents_page.findReoccurEveryVal({reoccur:{enable:true, every:3}})).toEqual(3);
			done();
		});

		it('findReoccurIntervalVal', function(done) {
			expect(reoccurevents_page.findReoccurIntervalVal({})).toBe('never');
			expect(reoccurevents_page.findReoccurIntervalVal({reoccur:{enable:{}}})).toEqual('never');
			expect(reoccurevents_page.findReoccurIntervalVal({reoccur:{enable:true}})).toEqual('never');
			expect(reoccurevents_page.findReoccurIntervalVal({reoccur:{enable:true, interval:'x'}})).toEqual('x');
			done();
		});

		it('assocMarkdown', function(done) {
			expect(reoccurevents_page.assocMarkdown({a:'x'})).toEqual({a: 'x'});
			expect(reoccurevents_page.assocMarkdown({description:'x'})).toEqual({description: 'x', markdown: 'x'});
			done();
		});

		it('isPremiumuser', function(done) {
			expect(reoccurevents_page.isPremiumUser({})).toBe(false);
			expect(reoccurevents_page.isPremiumUser({user:{}})).toBe(false);
			expect(reoccurevents_page.isPremiumUser({user:{premium:{}}})).toBe(false);
			expect(reoccurevents_page.isPremiumUser({user:{premium:{isstaff:true}}})).toBe(true);
			expect(reoccurevents_page.isPremiumUser({user:{premium:{isactive:true}}})).toBe(true);
			done();
		});

  });
});


// ============== RUN TESTS ================== 
describe("reoccurevents_page_spec.js api", function() {

    var server;

    // verifyregister :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
    var verifyregister = function(account, zombieb, email, pass, userobj) {
      return new Promise(function(fulfill, reject) {
        accounts.DeleteAccount(account, email).then(function(y) {
          var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));
          register(email, pass).then(function(x) {
            account.update({email: email}, userobj).exec().then(function(z) {
              zombieb.visit(base_url + "login", function(err) {
                if (err) 
                  reject(err);

                zombieb.fill('#form-username', email)
                zombieb.fill('#form-password', pass)
                zombieb.pressButton('#loginbutton', function() {
                  fulfill(x);
                });
              });
            });
          });
        });
      })
    };

    // String -> Number -> Number -> Object
    var createHostAccountObj = function(location, lat, lng, avatar) {
      return {
        profile: {
          ishost: 2,
          avataricon: avatar
        },
        hostareas: [
          {
            name: location,
            loc: {
              coordinates: [lat, lng]
            }
          }
        ]
      };
    };

    var generateUser = genhelper.GenerateUser(account);
    var generateHostUser = genhelper.GenerateHostUser(account);

    // BoardgameSchema -> Number -> Number -> String -> String -> Number -> Promise(Boardgame)
    var cacheGame = R.curry(function(testBoardgame, currentyear, gameid, gamename, thumbnail, gameyear) { 
      return new Promise(function(fulfill, reject) {
        var cacheObject = {
          numplays: 0,
          thumbnail: thumbnail,
          yearpublished: gameyear,
          name: {
            t: gamename,
            sortindex: 1
          },
          subtype: 'boardgame',
          objectid: gameid,
          objecttype: 'thing'
        };
        fulfill(boardgames.CacheBoardgame(testBoardgame, currentyear, cacheObject));
      });
    });
    var cacheGame2017 = cacheGame(boardgame, 2017);

    var generateRandomHost = accounts.RespawnAccountObject(account, faker.Internet.email().toLowerCase(), createHostAccountObj(faker.Address.streetAddress(), faker.Address.latitude(), faker.Address.longitude(), faker.Image.avatar()));

    describe("web page tests", function() {

      it("setup server", function(done) {
        testapp.get('/', function (req, res) {
          res.send('<html>Home Page</html>')
        });

        login_page.RouteLogins(testapp, passport);
        login_page.PostLogin(testapp, passport);
        login_page.RouteVerify(testapp, account);
        reoccurevents_page.Routegame(testapp, login, match, account);

        server = testapp.listen(httpPort, function() {
            done();
        });
      }, 10000);


      it("non logged in  user visit /events/series/1234", function(done) {
        page = 'events/series/1234';
        browser.visit(base_url + page, function(err) {
          expect(browser.success).toBe(false);
          done();
        });
      }, 10000);

      it("valid user visit invalid /events/series/1234", function(done) {
        var userEmail = 'event_web1@gmail.com';
        var userPass = 'testpassword';

        verifyregister(account, browser, userEmail, userPass, {} ).then(function(x) {
          page = 'events/series/1234';
          browser.visit(base_url + page, function(err) {
            expect(browser.success).toBe(false);
            expect(browser.html()).toContain('<pre>Cannot GET /events</pre>');
            done();
          });
        });
      }, 10000);

      it("valid user visit /events/series/", function(done) {

        var userEmail2 = faker.Internet.email().toLowerCase();
        var userPass = 'testpassword';

        var gameid = 43015;
        var gamename = 'Hansa Teutonica';
        var thumbnail = '//cf.geekdo-images.com/R6eYnXnFgniTevooXHPjbQIB09c=/fit-in/246x300/pic839090.jpg';
        var gameyear = 2016;

        var match1 = {gameid: gameid};
        var gameDate = new Date(2017, 06, 3, 20, 30, 0); 
        var gameEnd = new Date(2017, 06, 3, 22, 30, 0); 

        var hostAvatar = 'http://localhost:3000/img/test-avatar2.jpg';

        var minPlayers = 2;
        var matchPlayers = 5;

        var matchLocName = 'Ularu Australia';
        var lat = -25.344428;
        var lng = 131.036882;
        var coords = [lng, lat];

        var status = ['open'];
        var smoking = 2;
        var alcohol = 1;
        var social = {smoking: smoking, alcohol: alcohol};

        var useraccount = {
            profile: {ishost: 1},
        };

        Promise.all([generateRandomHost, verifyregister(account, browser, userEmail2, userPass, useraccount), match.remove(match1), boardgames.DeleteBoardgame(boardgame, gameid)]).then(function(y) {
          var userKey = y[0].key;
          var matchUser = {
            email: y[0].email, 
            avataricon: y[0].profile.avataricon 
          };

          const createMatchObj = R.compose(R.of, matches.Zipmatch, R.zipObj(['id', 'name', 'thumbnail', 'description', 'minplaytime', 'maxplaytime', 'minplayers', 'maxplayers']));

          const createMatch = R.compose(matches.CreateMatch(match, matchUser, false), R.zipObj(['locname', 'coords', 'gamedate', 'timeend', 'maxplayers', 'minplayers', 'status', 'social', 'games', 'isprivate']));

          cacheGame2017(gameid, gamename, thumbnail, gameyear).then(function(z) {
            var makematches = [
              createMatch([matchLocName, coords, gameDate, gameEnd, matchPlayers, minPlayers, status[0], social, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 4]), false]),
              createMatch([matchLocName, coords, gameDate, gameEnd, matchPlayers, minPlayers, status[0], social, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 4]), true])
            ];

            Promise.all(makematches).then(function(m) {

              // expect match to show in the browser when viewing key
              pageOpen = 'events/series/' + m[0].seriesid;
              pagePrivate = 'events/series/' + m[1].seriesid;

              browser.visit(base_url + pageOpen, function(err) {
                expect(browser.success).toBe(true);
                expect(browser.html()).toContain('Players');
                expect(browser.html()).toContain(matchLocName);
                expect(browser.html()).toContain(gamename);

                browser.visit(base_url + pagePrivate, function(err) {
                  expect(browser.success).toBe(true);
                  expect(browser.html()).toContain('Players');
                  matches.InviteMatch(account, match, y[0].email, userKey, m[1].key, userEmail2).then(function(i) { 
                    browser.visit(base_url + pageOpen, function(err) {
                      expect(browser.success).toBe(true);
                      expect(browser.html()).toContain('Players');
                      expect(browser.html()).toContain(matchLocName);
                      expect(browser.html()).toContain(gamename);
                      done();
                    });
                  });
                });
              });

              });
          });
        });
      }, 10000);

     

      it("test action buttons for  /event", function(done) {
          var users = [
            faker.Internet.email().toLowerCase(), 
            faker.Internet.email().toLowerCase(),
            faker.Internet.email().toLowerCase(), 
            faker.Internet.email().toLowerCase(), 
            faker.Internet.email().toLowerCase(), 
            faker.Internet.email().toLowerCase(), 
            faker.Internet.email().toLowerCase() 
          ];

          // user map
          // -- user[0] - host
          // -- user[1] request -> reject 
          // -- user[2] request -> approve
          // -- user[3] request -> leave 
          // -- user[4] invite  -> deny
          // -- user[5] invite  -> accept 
          // -- user[6] invite  -> revoke 

          var userPass = 'testpassword';

          var gameid = 43015;
          var gamename = 'Hansa Teutonica';
          var thumbnail = '//cf.geekdo-images.com/R6eYnXnFgniTevooXHPjbQIB09c=/fit-in/246x300/pic839090.jpg';
          var gameyear = 2016;

          var match1 = {gameid: gameid};
          var gameDate = new Date(2017, 06, 3, 20, 30, 0); 
          var gameEnd = new Date(2017, 06, 3, 22, 30, 0); 

          var hostAvatar = faker.Image.avatar();

          var matchUser = {
            email: users[0], 
            avataricon: hostAvatar
          };

          var minPlayers = 2;
          var matchPlayers = 5;

          var matchLocName = 'Ularu Australia';
          var lat = -25.344428;
          var lng = 131.036882;
          var coords = [lng, lat];

          var status = 'open';
          var smoking = 2;
          var alcohol = 1;
          var social = {smoking: smoking, alcohol: alcohol};

          var hostaccount = createHostAccountObj(matchLocName, lat, lng, hostAvatar);

          //const createMatchObj = R.compose(R.of, matches.Zipmatch, R.zipObj(['id', 'name', 'thumbnail']));
          const createMatchObj = R.compose(R.of, matches.Zipmatch, R.zipObj(['id', 'name', 'thumbnail', 'description', 'minplaytime', 'maxplaytime', 'minplayers', 'maxplayers']));

          const createMatch = R.compose(matches.CreateMatch(match, matchUser, false), R.zipObj(['locname', 'coords', 'gamedate', 'timeend', 'maxplayers', 'minplayers', 'status', 'social', 'games']));

          Promise.all([
            verifyregister(account, browser, users[0], userPass, hostaccount), 
            match.remove(match1), 
            boardgames.DeleteBoardgame(boardgame, gameid),
            verifyregister(account, browser2, users[1], userPass, hostaccount)
          ]).then(function(y) {
            cacheGame2017(gameid, gamename, thumbnail, gameyear).then(function(z) {
              createMatch([matchLocName, coords, gameDate, gameEnd, matchPlayers, minPlayers, status, social, createMatchObj([gameid, gamename, thumbnail, 'description', 30, 60, 1, 4]) ]).then(function(m) {

                // type: host
                page = 'events/series/' + m.seriesid;

                browser.visit(base_url + page, function(err) {
                  expect(browser.success).toBe(true);
                  expect(browser.html('#mappanel')).toContain(matchLocName);
                  expect(browser.query('#pendingpanel')).toBe(null);

                  // type: none || game open 
                  browser2.visit(base_url + page, function(err) {

                    expect(browser2.success).toBe(true);
                    expect(browser2.html('#mappanel')).toContain(matchLocName);

                    expect(browser2.query('#invitepanel')).toBe(null);
                    expect(browser2.query('#requestpanel')).toBe(null);
                    expect(browser2.query('#pendingpanel')).toBe(null);

                    expect(browser2.query('#lobbychatpanel')).toBe(null);
                    done();

                  });
                });
              });
            });
          });
		}, 10000);

    it('server close', function(done) {
      server.close(function() {
        done();
      });
    });

    it('db close', function(done) {
      mongo.CloseDatabase();
      done();
    });
  });

});

