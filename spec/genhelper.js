var Promise = require('bluebird');
var R = require('ramda');
var util = require('util');

var log = require('../app/log.js');
var faker = require('Faker');
var moment = require('moment');

const shortid = require('shortid');
var accounts = require('../app/models/accounts.js');
var matches = require('../app/models/matches.js');
var boardgames = require('../app/models/boardgames.js');

const toLower = (s) => s.toLowerCase();

// String -> Number -> String -> Object
var createAccountObj = function(email, host, avatar, name) {
  return {
    profile: {
      ishost: host,
      avataricon: avatar,
      name: name
    },
    facebook: {
      email: email,
      name: faker.Name.firstName() + ' ' + faker.Name.lastName(),
      photo: avatar
    }
  }; 
};


// String -> Number -> String -> Object
var createAccountObjWithHost = function(email, host, avatar, name, location, lat, lng) {
  return {
    profile: {
      ishost: host,
      avataricon: avatar,
      name: name,
      localarea: {
        name: location,
        loc: {
          type: "Point",
          coordinates: [Number(lng), Number(lat)]
        }
      }
    },
    facebook: {
      email: email,
      name: faker.Name.firstName() + ' ' + faker.Name.lastName(),
      photo: avatar
    }
  }; 
};


// String -> Number -> Number -> Object
var createHostAccountObj = function(email, location, lat, lng, avatar, name) {
  var hostareas = {
    hostareas: [
      {
        name: location,
        loc: {
          coordinates: [lng, lat]
        }
      }
    ]
  }

// createAccountObjWithHost
  // return R.merge(hostareas, createAccountObj(email, 2, avatar, name));
  return R.merge(hostareas, createAccountObjWithHost(email, 2, avatar, name, location, lat, lng));
};


var buildHost = (match, account, email, key, gamedate, gameend, maxplayers, minplayers, status, social, locname, coords, gameObj) => {

  const createMatchObj = R.compose(R.of, matches.Zipmatch, R.zipObj(['id', 'name', 'thumbnail', 'description', 'minplayers', 'maxplayers', 'minplaytime', 'maxplaytime']));

  const host = R.compose(
    matches.HostMatch(match, account, email, key),
    R.zipObj(['timeslots', 'gamedate', 'timeend', 'maxplayers', 'minplayers', 'status', 'social', 'locname', 'coords', 'timezone', 'games'])
  );

  return host([[{startdatetime:gamedate, enddatetime:gameend}], gamedate, gameend, maxplayers, minplayers, status, social, locname, coords, 'Australia/Sydney', createMatchObj([gameObj.id, gameObj.name, gameObj.thumbnail, gameObj.description, gameObj.minplayers, gameObj.maxplayers, gameObj.minplaytime, gameObj.maxplaytime])])

};


// generateUser :: Object -> Promise(Model(Account))
var generateUser = R.curry(function(account, userObj) {
  return new Promise(function(fulfill, reject) {
    var name = faker.Name.firstName() + ' ' + faker.Name.lastName();
    var email = faker.Internet.email();
    var avatar = faker.Image.avatar();
    var mergeUserAccount = R.merge(createAccountObj(toLower(email), 1, avatar, name));
    var addUser = R.compose(
      fulfill, 
      accounts.RespawnAccountObject(account, email.toLowerCase()), 
      mergeUserAccount
    );
    addUser(userObj);
  });
});
exports.GenerateUser = generateUser;


// generateHostUser :: Object -> Promise(Model(account))
var generateHostUser = R.curry(function(account, userObj) {
    return new Promise(function(fulfill, reject) {
        var name = faker.Name.firstName() + ' ' + faker.Name.lastName();
        var email = faker.Internet.email();
        var hostaccount = R.merge(createHostAccountObj(toLower(email), faker.Address.streetAddress(), faker.Address.latitude(), faker.Address.longitude(), faker.Image.avatar(), name));
        var addUser = R.compose(fulfill, accounts.RespawnAccountObject(account, toLower(email)), hostaccount);
        addUser(userObj);
    });
});
exports.GenerateHostUser = generateHostUser;


// generateFakeBoardgameObject :: {o} => {b} 
var generateFakeBoardgameObject = R.curry(function(obj) {

    var gameid = faker.Helpers.randomNumber(9999999);
    var gamename = R.join(' ', faker.Lorem.words()) + faker.Name.lastName();
    var thumbnail = faker.Image.sports();
    var currentyear = new Date().getFullYear();

    var cacheObject = {
        numplays: 0,
        thumbnail: thumbnail,
        yearpublished: currentyear,
        name: {
            t: gamename,
            sortindex: 1
        },
        subtype: 'boardgame',
        objectid: gameid,
        objecttype: 'thing'
    };

    return R.merge(obj, cacheObject)

});
exports.GenerateFakeBoardgameObject = generateFakeBoardgameObject;


// generateFakeBoardgame :: Schema(boardgame) => Promise(Model(boardgame))
var generateFakeBoardgame = R.curry(function(boardgame) {

    return new Promise(function(fulfill, reject) {
        var gameid = faker.Helpers.randomNumber(9999999);
        var gamename = R.join(' ', faker.Lorem.words()) + ' ' + faker.Name.lastName() + faker.Name.firstName();
        var thumbnail = faker.Image.sports();
        var currentyear = new Date().getFullYear();

        var cacheObject = {
            numplays: 0,
            thumbnail: thumbnail,
            yearpublished: currentyear,
            name: {
                t: gamename,
                sortindex: 1
            },
            subtype: 'boardgame',
            objectid: gameid,
            objecttype: 'thing',
         };

        fulfill(boardgames.CacheReturnBoardgame(boardgame, currentyear - 1, cacheObject));

    })
});
exports.GenerateFakeBoardgame = generateFakeBoardgame;


// generateFakeHostedMatch ::  Model(account) -> Model(boardgame) -> Model(match) -> s -> n -> m -> [Model(Account), Model(Match)] 
var generateFakeHostedMatch = R.curry(function(account, boardgame, match, status, seats, minseats) {
  return new Promise(function(fulfill, reject) {

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    var gameEnd = moment(tomorrow).add(60*60, 's');

    generateFakeBoardgame(boardgame).then(function(g) {
      generateHostUser(account, {ticketshold:5}).then(function(h) {

        buildHost(match, account, h.email, h.key, tomorrow, gameEnd, seats, minseats, status, {}, h.hostareas[0].name, h.hostareas[0].loc.coordinates, g).then(function(m) {
          fulfill([h, m]);

        });
      });
    });
  });
});
exports.GenerateFakeHostedMatch = generateFakeHostedMatch;


// generateFakeHostedMatchWDate ::  (Model(account) -> Model(boardgame) -> Model(match) -> String -> Number -> Number -> Date) => [Model(Account), Model(Match)] 
var generateFakeHostedMatchWDate = R.curry(function(account, boardgame, match, status, maxseats, minseats, gamedate, gameend) {
  return new Promise(function(fulfill, reject) {
    generateFakeBoardgame(boardgame).then(function(g) {
      generateHostUser(account, {ticketshold:5}).then(function(h) {

        buildHost(match, account, h.email, h.key, gamedate, gameend, maxseats, minseats, status, {}, h.hostareas[0].name, h.hostareas[0].loc.coordinates, g).then(function(m) {

          fulfill([h, m]);

        });
      });
    });
  });
});
exports.GenerateFakeHostedMatchWDate = generateFakeHostedMatchWDate;

