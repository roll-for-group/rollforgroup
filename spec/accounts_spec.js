var Promise = require('bluebird');

var faker = require('Faker');

var R = require('ramda');
var compose  = R.compose,
    composeP = R.composeP,
    isNil    = R.isNil,
    merge     = R.merge,
    nth = R.nth,
    prop     = R.prop,
    sequence = R.sequence,
    split    = R.split,
    tail     = R.tail;

var util = require('util');
var bgg = require('../app/bgg.js');
var log = require('../app/log.js');

var fs = require('fs');

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task');

var mathjs = require('mathjs');

var testAccount = require('../app/models/account.js');
var testAccounts = require('../app/models/accounts.js');
var testMatch = require('../app/models/match.js');
var testMatches = require('../app/models/matches.js');
var testBoardgame = require('../app/models/boardgame.js');
var testBoardgames = require('../app/models/boardgames.js');

var transaction = require('../app/models/transaction.js');
var transactions = require('../app/models/transactions.js');

var moment = require('moment');

const toLower = (s) => s.toLowerCase();

var genhelper = require('./genhelper.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
var dbUri = nconf.get('database:uri');

const shortid = require('shortid');

mongo.ConnectDatabase(dbUri);

var currentyear = 2017;
var cachegame = testBoardgames.CacheBoardgame(testBoardgame, currentyear);

var generateUser = genhelper.GenerateUser(testAccount);
var generateHostUser = genhelper.GenerateHostUser(testAccount);

describe('accounts_spec.js', function() {
    var userEmail = 'testdummy@gmail.com';
    var newUser;

    it('delete old bgg users', function(done) {
        testAccounts.DeleteAccount(testAccount, userEmail).then(
            function(x) {
                done()
            }
        );
    });

    it('creating new user object', function(done) {
        var gameobject = {
            objectid: 12345,
            minplaytime: 30,
            maxplaytime: 180,
            name: {
                t: 'Myname'
            },
            status: {
                own: true,
                wanttoplay: true
            }
        };
        var updatedobject = testAccounts.UpdateBoardGameName(gameobject);
        
        expect(updatedobject.id).toBe(12345);
        expect(updatedobject.name).toBe('Myname'); 
        expect(updatedobject.bggcache).toEqual(gameobject); 

        expect(updatedobject.minplaytime).toBe(30); 
        expect(updatedobject.maxplaytime).toBe(180); 

        expect(updatedobject.matching.own).toBeTruthy();
        expect(updatedobject.matching.wanttoplay).toBeTruthy();

        done();

    });

    it('unit test CreateAccountObject', function(done) {

        var name = 'CAO UserName';
        var bggname = 'CAObggname';

        var email = 'CAOemail@gmail.com';
        var skeletonaccount = {
            nickname:   name,
            profile:    {
              private: 1,
              name:       name,
              bggname:    bggname,
              Tuesday:    true
            }
        };

        testAccounts.DeleteAccount(testAccount, toLower(email)).then(function(d) {
            var newaccount = testAccounts.CreateAccountObject(testAccount, email, skeletonaccount).then(function(x) {

                expect(R.prop('nickname', x)).toBe(name);
                expect(R.prop('email', x)).toBe(email.toLowerCase());

                expect(R.path(['profile', 'name'], x)).toBe(name);
                expect(R.path(['profile', 'bggname'], x)).toBe(bggname);
                expect(R.path(['profile', 'Monday'], x)).toBeFalsy;
                expect(R.path(['profile', 'Tuesday'], x)).toBeTruthy;

                done();

               }
            );
        });
    });

    // deletes and cerates account object for testing
    it('unit test RespawnAccountObject', function(done) {

        var name = 'RSP UserName';
        var bggname = 'RSPbggname';

        var email = 'respawnemail@gmail.com';
        var skeletonaccount = {
            nickname:   name,
            profile:    {
              private: 1,
              name:       name,
              bggname:    bggname,
              Tuesday:    true
            }
        };

        var whereAccount = R.whereEq({
            nickname: name,
            email: email,
        });
        var whereProfile = R.whereEq({
            name: name,
            bggname: bggname,
            Monday: false,
            Tuesday: true
        });
        var respawnTest = R.composeP(R.converge(R.and, [R.pipe(R.prop('profile'), whereProfile), whereAccount]), testAccounts.RespawnAccountObject(testAccount, email));

        respawnTest(skeletonaccount).then(function(r1) {
            expect(r1).toBe(true);

            respawnTest(skeletonaccount).then(function(r2) {
                expect(r2).toBe(true);

                testAccounts.CreateAccountObject(testAccount, email, skeletonaccount).then().catch(function(f1) {
                    // mongo error code for duplicate
                    expect(f1.code).toBe(11000);
                    done();
                  
                });
            });
        });
    });


    it('SortedValues', function(done) {

        const gamelist = [ { numplays: 0,
			thumbnail: 'https://cf.geekdo-images.com/images/pic1083380_t.jpg',
			image: 'https://cf.geekdo-images.com/images/pic1083380.jpg',
			yearpublished: 2011,
			id: 96848,
			name: 'Mage Knight Board Game',
			bggcache: 
			 { numplays: 0,
			   status: 
				{ lastmodified: '2016-09-03 01:24:27',
				  preordered: 0,
				  wishlist: 0,
				  wanttobuy: 0,
				  wanttoplay: 0,
				  want: 0,
				  fortrade: 0,
				  prevowned: 0,
				  own: 1 },
			   thumbnail: 'https://cf.geekdo-images.com/images/pic1083380_t.jpg',
			   image: 'https://cf.geekdo-images.com/images/pic1083380.jpg',
			   yearpublished: 2011,
			   name: { t: 'Mage Knight Board Game', sortindex: 1 },
			   collid: 36660696,
			   subtype: 'boardgame',
			   objectid: 96848,
			   objecttype: 'thing' },
			_id: '5934ceef8d251a4ed2a3c65e',
			matching: 
			 { own: true,
			   wanttoplay: false,
			   avoid: false,
			   matchcount: 0,
			   personalrating: 0,
			   socialrating: 0,
			   skillrating: 0,
			   teach: false },
			active: false,
			activestatus: 'inactive',
			wanttoplay: 0 },
		  { numplays: 0,
			thumbnail: 'https://cf.geekdo-images.com/images/pic3165731_t.jpg',
			image: 'https://cf.geekdo-images.com/images/pic3165731.jpg',
			yearpublished: 2012,
			id: 121921,
			name: 'Robinson Crusoe: Adventures on the Cursed Island',
			bggcache: 
			 { numplays: 0,
			   status: 
				{ lastmodified: '2016-09-03 01:25:34',
				  preordered: 0,
				  wishlist: 0,
				  wanttobuy: 0,
				  wanttoplay: 0,
				  want: 0,
				  fortrade: 0,
				  prevowned: 0,
				  own: 0 },
			   thumbnail: 'https://cf.geekdo-images.com/images/pic3165731_t.jpg',
			   image: 'https://cf.geekdo-images.com/images/pic3165731.jpg',
			   yearpublished: 2012,
			   name: 
				{ t: 'Robinson Crusoe: Adventures on the Cursed Island',
				  sortindex: 1 },
			   collid: 36660702,
			   subtype: 'boardgame',
			   objectid: 121921,
			   objecttype: 'thing' },
			_id: '5934ceef8d251a4ed2a3c65d',
			matching: 
			 { own: true,
			   wanttoplay: false,
			   avoid: false,
			   matchcount: 0,
			   personalrating: 0,
			   socialrating: 0,
			   skillrating: 0,
			   teach: false },
			active: false,
			activestatus: 'active',
			wanttoplay: 0 } 
		]; 

		expect(testAccounts.SortedValues(gamelist).collection.active.length).toBe(1)
		expect(testAccounts.SortedValues(gamelist).collection.inactive.length).toBe(1)
		expect(testAccounts.SortedValues(gamelist).collection.active[0].name).toBe('Robinson Crusoe: Adventures on the Cursed Island');


		done();

    });

    var testEmail = "testdummy@gmail.com";
    var gameId = 96848;
    var gameName = "Mage Knight Board Game";
    var readAccount = testAccounts.ReadAccountFromEmail(testAccount);
    var getGames = R.prop('games');
    var getKeyMatching = R.prop('matching');

    it('test UpdateSchedule', function(done) {

        generateUser({profile:{Monday: false, Sunday: true, sundaystarthour:3, sundaystartminute:0, sundayendhour: 15, sundayendminute:45 }}).then(function(x) {

            expect(R.path(['profile', 'Monday'], x)).toBeFalsy();
            expect(R.path(['profile', 'Sunday'], x)).toBeTruthy();
            expect(R.path(['profile', 'sundaystarthour'], x)).toBe(3);
            expect(R.path(['profile', 'sundaystartminute'], x)).toBe(0);
            expect(R.path(['profile', 'sundayendhour'], x)).toBe(15);
            expect(R.path(['profile', 'sundayendminute'], x)).toBe(45);

            var updateSchedule = testAccounts.UpdateSchedule(testAccount, x.email);
            Promise.all([updateSchedule('Monday', true), updateSchedule('mondaystarthour', 6), updateSchedule('mondayendhour', 22)]).then(function(y) {

                expect(R.path(['profile', 'Monday'], y[0])).toBe(true);
                expect(R.path(['profile', 'mondaystarthour'], y[1])).toBe(6);
                expect(R.path(['profile', 'mondayendhour'], y[2])).toBe(22);

                readAccount(x.email).then(function(z) {

                    expect(R.path(['profile', 'Monday'], z[0])).toBeTruthy();
                    expect(R.path(['profile', 'mondaystarthour'], z[0])).toBe(6);
                    expect(R.path(['profile', 'mondayendhour'], z[0])).toBe(22);

                    expect(R.path(['profile', 'Sunday'], z[0])).toBeTruthy();
                    expect(R.path(['profile', 'sundaystarthour'], z[0])).toBe(3);
                    expect(R.path(['profile', 'sundayendhour'], z[0])).toBe(15);

                    done();

                });
            });
        });
    });

    it('test UpdateProfile', function(done) {
        generateUser({profile:{Monday: false, Sunday: true, sundaystarthour:3, sundaystartminute:0, sundayendhour: 15, sundayendminute:45 }}).then(function(x) {

            expect(R.path(['profile', 'Monday'], x)).toBeFalsy();
            expect(R.path(['profile', 'Sunday'], x)).toBeTruthy();
            expect(R.path(['profile', 'sundaystarthour'], x)).toBe(3);
            expect(R.path(['profile', 'sundaystartminute'], x)).toBe(0);
            expect(R.path(['profile', 'sundayendhour'], x)).toBe(15);
            expect(R.path(['profile', 'sundayendminute'], x)).toBe(45);

            testAccounts.UpdateProfile(testAccount, x.email, {Monday: true, mondaystarthour: 6, mondayendhour: 22}).then(function(y) {

                expect(R.path(['profile', 'Monday'], y)).toBe(true);
                expect(R.path(['profile', 'mondaystarthour'], y)).toBe(6);
                expect(R.path(['profile', 'mondayendhour'], y)).toBe(22);

                readAccount(x.email).then(function(z) {

                    expect(R.path(['profile', 'Monday'], z[0])).toBeTruthy();
                    expect(R.path(['profile', 'mondaystarthour'], z[0])).toBe(6);
                    expect(R.path(['profile', 'mondayendhour'], z[0])).toBe(22);

                    expect(R.path(['profile', 'Sunday'], z[0])).toBeTruthy();
                    expect(R.path(['profile', 'sundaystarthour'], z[0])).toBe(3);
                    expect(R.path(['profile', 'sundayendhour'], z[0])).toBe(15);

                    done();

                });
            });
        });
    });

    it('PurchaseTickets', function(done) {
			var ticketsstart = 5;
			generateUser({tickets: ticketsstart}).then(function(u) {

				expect(u.tickets).toBe(5);
				var ticketspurchased = 20;

				testAccounts.PurchaseTickets(testAccount, transaction, u.email, '20pack', 20, 'paypal', 'AUD', 100, 'trans-ABC', 'no promotion', 'no coupon').then(function(pt) {

					expect(pt[0].tickets).toBe(ticketsstart + ticketspurchased);
					expect(pt[1].tickets).toBe(ticketspurchased);
					expect(pt[0].email).toBe(pt[1].purchaser);
					expect(pt[0].email).toBe(pt[1].to);

					done();

				});
			});
    });

});


describe('accounts_spec', function() {

    it('BuildNavObject', function(done) {

        var testResponse = (email, name, avataricon, tickets, pageslug, response) => R.whereEq({
          user: {
            email: email,
            name: name,
            avataricon: avataricon,
            tickets: tickets,
            pageslug: pageslug
          }
        })(response);

        const test1 = {
          tickets: 20,
          profile: {ishost: 1},
          email: 'email',
          facebook: {
            name:  'name', 
            photo: 'icon',
          },
          pageslug: 'slug1'
        };

        const test2 = {
          tickets: 15,
          profile: {ishost: 2},
          email: 'email',
          google: {
            name:  'name', 
            photo: 'icon',
          },
          pageslug: 'slug2'
        };

        const test3 = {
          tickets: 10,
          email:  'email',
          profile: {
            ishost: 1,
            name:       'name', 
            avataricon: 'icon'
          },
          pageslug: 'slug3'
        };

        expect(testResponse('email', 'name', 'icon', 20, 'slug1', testAccounts.BuildNavObject(test1))).toBe(true);
        expect(R.has('headerbuttons', testAccounts.BuildNavObject(test1))).toBe(true);
        expect(testResponse('email', 'name', 'icon', 15, 'slug2', testAccounts.BuildNavObject(test2))).toBe(true);
        expect(R.has('headerbuttons', testAccounts.BuildNavObject(test2))).toBe(true);
        expect(R.has('headerbuttons', testAccounts.BuildNavObject({}))).toBe(true);

        expect(testResponse('email', 'name', 'icon', 10, 'slug3', testAccounts.BuildNavObject(test3))).toBe(true);

        done();

    });


    it('BuildNavbars', function(done) {

      var testResponse = (email, name, avataricon, tickets, pageslug, response) => R.whereEq({
        user: {
          email: email,
          name: name,
          avataricon: avataricon,
          tickets: tickets,
          pageslug: pageslug 
        }
      })(response);

      Promise.all([generateHostUser({tickets: 20}), generateUser({tickets: 30})]).then(function(u) {

        // email number 1
        testAccounts.BuildNavbars(testAccount, u[0].email).then(function(h) {
          expect(testResponse(u[0].email, u[0].profile.name, u[0].profile.avataricon, 20, u[0].pageslug, h)).toBe(true);
          // email number 2
          testAccounts.BuildNavbars(testAccount, u[1].email).then(function(n) {
            expect(testResponse(u[1].email, u[1].profile.name, u[1].profile.avataricon, 30, u[1].pageslug, n)).toBe(true);
            testAccounts.BuildNavbars(testAccount, '').then(function(n) {
              expect(n).toEqual({})
              done();
            });

          });
        });
      });
    });

    it('ReadAnAccountFromEmail', function(done) {
        var testUser = 'keytaskusername'; 
        var testEmail = 'testkeytask@gmail.com';
        var testBgg = 'bggname';

        testAccounts.DeleteAccount(testAccount, testEmail).then(function(d) {
            var newaccount = testAccounts.CreateDummyAccount(testAccount, testUser, testEmail, testBgg).then(function(a) {

                var readAccount = testAccounts.ReadAnAccountFromEmail(testAccount);
                    
                // test reading with key
                readAccount(testEmail).then(function(readuser) { 

                    expect(readuser.profile.name).toBe(testUser);
                    expect(readuser.profile.email).toBe(testEmail);
                    expect(readuser.profile.bggname).toBe(testBgg);

                    testAccounts.TaskAccountFromEmail(testAccount, testEmail).fork(
                        err => log.clos('err', err),
                        data => {
                            expect(data.profile.name).toBe(testUser);
                            expect(data.profile.email).toBe(testEmail);
                            expect(data.profile.bggname).toBe(testBgg);
                            done();
                        }
                    )
                })
            });
        });
    });


    it('ReadAnAccountFromPageSlug', function(done) {
        var testUser = 'keytaskusername'; 
        var testEmail = 'testkeytask@gmail.com';
        var testBgg = 'bggname';

        testAccounts.DeleteAccount(testAccount, testEmail).then(function(d) {
            var newaccount = testAccounts.CreateDummyAccount(testAccount, testUser, testEmail, testBgg).then(function(a) {

                var readAccount = testAccounts.ReadAnAccountFromPageSlug(testAccount);
                    
                // test reading with key
                readAccount(a.pageslug).then(function(readuser) { 
                    expect(readuser.profile.name).toBe(testUser);
                    expect(readuser.profile.email).toBe(testEmail);
                    expect(readuser.profile.bggname).toBe(testBgg);
                    expect(readuser.pageslug).toBe(a.pageslug);

                    testAccounts.TaskAccountFromSlug(testAccount, a.pageslug).fork(
                        err => log.clos('err', err),
                        data => {
                            expect(data.profile.name).toBe(testUser);
                            expect(data.profile.email).toBe(testEmail);
                            expect(data.profile.bggname).toBe(testBgg);
                            expect(data.pageslug).toBe(a.pageslug);
                            done();
                        }
                    )
                })
            });
        });
    });


    it('ReadVerifyAccountFromEmail', function(done) {
        var testUser = 'keyusername'; 
        var testEmail = 'testkey@gmail.com';
        var testBgg = 'bggname';

        testAccounts.DeleteAccount(testAccount, testEmail).then(function(d) {
            var newaccount = testAccounts.CreateDummyAccount(testAccount, testUser, testEmail, testBgg).then(function(a) {

                var readAccount = R.composeP(R.prop('key'), R.head, testAccounts.ReadAccountFromEmail(testAccount));
                    
                // test reading with key
                readAccount(testEmail).then(function(accountKey) { 
                    var verifyAccountTest1 = testAccounts.ReadVerifyAccountFromEmail(testAccount, testEmail, accountKey); 
                    var verifyAccountTest2 = testAccounts.ReadVerifyAccountFromEmail(testAccount, 'invalidemail', accountKey); 
                    var verifyAccountTest3 = testAccounts.ReadVerifyAccountFromEmail(testAccount, testEmail, 'invalidkey'); 

                    Promise.all([verifyAccountTest1, verifyAccountTest2, verifyAccountTest3]).then(function(x) {
                        expect(R.length(x[0])).toBe(1);
                        expect(R.length(x[1])).toBe(0);
                        expect(R.length(x[2])).toBe(0);
                        done();
                    });
                })
            });
        });
    });


    it('TaskVerifyAnAccountFromEmail', function(done) {

        var testUser = 'keyusername'; 
        var testEmail = 'testkey@gmail.com';
        var testBgg = 'bggname';

        testAccounts.DeleteAccount(testAccount, testEmail).then(function(d) {

            var newaccount = testAccounts.CreateDummyAccount(testAccount, testUser, testEmail, testBgg).then(function(a) {

                var readAccount = R.composeP(R.prop('key'), R.head, testAccounts.ReadAccountFromEmail(testAccount));
                    
                // test reading with key
                readAccount(testEmail).then(function(accountKey) { 

                    testAccounts.TaskVerifyAnAccountFromEmail(testAccount, testEmail, accountKey).fork(err => log.clos('err'), (x) => {
                        expect(x.profile.name).toBe(testUser);
                        expect(x.email).toBe(testEmail);
                        testAccounts.TaskVerifyAnAccountFromEmail(testAccount, 'invalidemail', accountKey).fork((err) => {
                        testAccounts.TaskVerifyAnAccountFromEmail(testAccount, testEmail, 'invalidkey').fork((err) => { 
                                done();
                            }); 
                        }); 
                    });
                })
            });
        });
    });

    it('CheckValidIcalkey premium', function(done) {
      const icalkey = 'abcdef';
      generateUser({icalkey:icalkey, premium:{isactive:true}}).then(function(u) {
        testAccounts.TaskCheckIcalKey(testAccount, u.email, 'badkey').fork(
          err => testAccounts.TaskCheckIcalKey(testAccount, u.email, icalkey).fork(
            err => log.clos('err.icalkey', err),
            data => {
              expect(data.email).toBe(u.email);
              expect(data.icalkey).toBe(u.icalkey);
              expect(data.icalviewcount).toBe(1);

              generateUser({icalkey:icalkey}).then(function(u) {
                testAccounts.TaskCheckIcalKey(testAccount, u.email, icalkey).fork(
                  err => {
                    expect(err).toBe('no user found');
                    done();
                  }
                )
              });
            }
          )
        )
      });
    });

    it('CheckValidIcalkey staff', function(done) {
      const icalkey = 'abcdef';
      generateUser({icalkey:icalkey, premium:{isstaff:true}}).then(function(u) {
        testAccounts.TaskCheckIcalKey(testAccount, u.email, 'badkey').fork(
          err => testAccounts.TaskCheckIcalKey(testAccount, u.email, icalkey).fork(
            err => log.clos('err.icalkey', err),
            data => {
              expect(data.email).toBe(u.email);
              expect(data.icalkey).toBe(u.icalkey);
              expect(data.icalviewcount).toBe(1);

              generateUser({icalkey:icalkey}).then(function(u) {
                testAccounts.TaskCheckIcalKey(testAccount, u.email, icalkey).fork(
                  err => {
                    expect(err).toBe('no user found');
                    done();
                  }
                )
              });
            }
          )
        )
      });
    });


    it('CheckValidEmail', function(done) {

        var testUser = 'keyusername'; 
        var testEmail = 'testkey@gmail.com';
        var testBgg = 'bggname';

        testAccounts.DeleteAccount(testAccount, testEmail).then(function(d) {
            var newaccount = testAccounts.CreateDummyAccount(testAccount, testUser, testEmail, testBgg).then(function(a) {

                var checkAccount = testAccounts.CheckValidEmail(testAccount);
                    
                // test reading with key
                checkAccount(a.email, a.key).then(function(r) { 
                    expect(r.profile.email).toBe(a.email);

                    checkAccount(a.email, 'badkey').then().catch(function(f) { 
                       expect(f.error.code).toBe(401);
                       done();

                    });
                });
            });
        });
    });


    it('FindPlayersNearby, FindBoardgamePlayersByDistanceCoords, FindBoardgamePlayersByDistance', function(done) {

      var geofind = {
        'profile.localarea.loc': {
          $near: {
            $geometry: {
              type: "Point" ,
              coordinates: [
                147.3271949,
                -42.8821377
              ] 
            },
            $maxDistance: 1000,
            $minDistance: 0 
          }
        }
      };

      testAccount.remove(geofind).exec().then(function(d) {
        Promise.all([
          generateUser({profile:{
            name: faker.Name.findName(),
            visibility: 1,
            localarea:{
              name:'Hobart TAS 7000, Australia',
              loc:{
                coordinates:[
                  147.3271949,
                  -42.8821377
                ],
                type:'Point'
              }
            }
          }}), 
          generateUser({profile:{
            name: faker.Name.findName(),
            visibility: 1,
            localarea:{
              name:'Dunn Pl, Hobart TAS 7000, Australia',
              loc:{
                coordinates:[
                  147.33204639999997,
                  -42.881823
                ],
                type:'Point'
              }
            }
          }}), 
          generateUser({profile:{
            name: faker.Name.findName(),
            visibility: 1,
            localarea:{
              name:'67-69 Barrack St, Hobart TAS 7000, Australia',
              loc:{
                coordinates:[
                  147.32025168836117,
                  -42.88398322495462
                ],
                type:'Point'
              }
            }
          }})
        ]).then((users) => {
          Promise.all([
            testAccounts.FindPlayersNearby(testAccount, users[0].email, 1),
            testAccounts.FindPlayersNearby(testAccount, users[0].email, 500),
            testAccounts.FindPlayersNearby(testAccount, users[0].email, 1000),
            testAccounts.FindBoardgamePlayersByDistanceCoords(testAccount, 1, [
              147.3271949,
              -42.8821377
            ]),
            testAccounts.FindBoardgamePlayersByDistanceCoords(testAccount, 500, [
              147.3271949,
              -42.8821377
            ]),
            testAccounts.FindBoardgamePlayersByDistanceCoords(testAccount, 1000, [
              147.3271949,
              -42.8821377
            ]),
            testAccounts.FindBoardgamePlayersByDistance(testAccount, 1, -42.8821377, 147.3271949),
            testAccounts.FindBoardgamePlayersByDistance(testAccount, 500, -42.8821377, 147.3271949),
            testAccounts.FindBoardgamePlayersByDistance(testAccount, 1000, -42.8821377, 147.3271949),
            testAccounts.FindPublicBoargamePlayersByCountCoords(testAccount, 1, [
              147.3271949,
              -42.8821377
            ]),
            testAccounts.FindPublicBoargamePlayersByCountCoords(testAccount, 2, [
              147.3271949,
              -42.8821377
            ]),
            testAccounts.FindPublicBoargamePlayersByCountCoords(testAccount, 3, [
              147.3271949,
              -42.8821377
            ]),
            testAccounts.FindPublicBoargamePlayersByNameCoords(testAccount, 3, users[0].profile.name, [
              147.3271949,
              -42.8821377
            ]),
            testAccounts.FindBoardgamePlayersByDistanceCoordsName(testAccount, 1000, users[0].profile.name, [
              147.3271949,
              -42.8821377
            ]),
            testAccounts.FindPlayersNearbyName(testAccount, users[0].email, users[1].profile.name, 1000),

          ]).then(function(y) {
            expect(y[0].length).toBe(1);
            expect(y[1].length).toBe(2);
            expect(y[2].length).toBe(3);

            expect(y[3].length).toBe(1);
            expect(y[4].length).toBe(2);
            expect(y[5].length).toBe(3);

            expect(y[6].length).toBe(1);
            expect(y[7].length).toBe(2);
            expect(y[8].length).toBe(3);

            expect(y[9].length).toBe(1);
            expect(y[10].length).toBe(1);
            expect(y[11].length).toBe(1);

            expect(y[12].length).toBe(1);
            expect(y[13].length).toBe(1);
            expect(y[14].length).toBe(1);

            done();

          });
        });
      });
    });


    it('calculatelevel', function(done) {
        // [50 -> 1], [105 -> 2], [166 -> 3], [232 -> 4], [305 -> 5]
        expect(testAccounts.CalculateLevel(0)).toBe(0);
        expect(testAccounts.CalculateLevel(10)).toBe(0);
        expect(testAccounts.CalculateLevel(60)).toBe(1);
        expect(testAccounts.CalculateLevel(150)).toBe(2);
        expect(testAccounts.CalculateLevel(220)).toBe(3);
        expect(testAccounts.CalculateLevel(320)).toBe(4);
        done();
    });

    it('TaskUpdateLevel', function(done) {
        Promise.all([generateUser({profile:{playlevel:300, hostlevel:600}}), generateUser({profile:{playlevel:200, hostlevel:300}})]).then((users) => {
            R.sequence(Task.of, [testAccounts.TaskUpdateLevel(testAccount, users[0].email, 3, 4), testAccounts.TaskUpdateLevel(testAccount, users[1].email, 8, 5)]).fork(
                e => log.clos('err', e),
                data => {
                    expect(data[0].profile.playlevel).toBe(3); 
                    expect(data[0].profile.hostlevel).toBe(4); 
                    expect(data[1].profile.playlevel).toBe(8); 
                    expect(data[1].profile.hostlevel).toBe(5); 
                    done();
                }
            )
        });
    });

    it('CheckLevelFreeGem', function(done) {
        expect(testAccounts.CheckLevelFreeGem(2,4)).toEqual([]);
        expect(testAccounts.CheckLevelFreeGem(2,5)).toEqual([5]);
        expect(testAccounts.CheckLevelFreeGem(2,6)).toEqual([5]);
        expect(testAccounts.CheckLevelFreeGem(2,12)).toEqual([5, 10]);
        done();
    });

    it('objXpToLevel', function(done) {
        expect(testAccounts.ObjXpToLevel('hostxp')({}).isNothing).toBe(true);
        expect(testAccounts.ObjXpToLevel('hostxp')({profile:{}}).isNothing).toBe(true);
        expect(testAccounts.ObjXpToLevel('hostxp')({profile:{playxp:300, hostxp:400}}).get()).toBe(5);
        //expect(testAccounts.ObjXpToLevel('hostxp')({profile:{playxp:420, hostxp:420}}).get()).toBe(3);
        expect(testAccounts.ObjXpToLevel('playxp')({profile:{playxp:300, hostxp:400}}).get()).toBe(4);
        //expect(testAccounts.ObjXpToLevel('playxp')({profile:{playxp:3150, hostxp:400}}).get()).toBe(4);
        done();
    });

    it('TaskAddGameExperience', function(done) {

        // details
        var email   = 'emailaddress';
        var gameid  = 1234;
        var currentyear = new Date().getFullYear();

        // create a game1
        var gameObj1 = genhelper.GenerateFakeBoardgameObject({minplaytime: 20, maxplaytime:80});
        var gameObj2 = genhelper.GenerateFakeBoardgameObject({minplaytime: 60, maxplaytime:140});

        const cachegame = testBoardgames.CacheReturnBoardgame(testBoardgame, currentyear -1); 
        const addXP = testAccounts.TaskAddGameExperience(testAccount);

        // setup objects
        Promise.all([cachegame(gameObj1), cachegame(gameObj2), generateUser({})]).then(function(x) {
            Promise.all([testBoardgame.update({id: gameObj1.objectid}, {$set:{minplaytime:20, maxplaytime: 80}}), testBoardgame.update({id: gameObj2.objectid}, {$set:{minplaytime:60, maxplaytime:140}})]).then(function(y) {

                addXP(x[2].email, false, gameObj1.maxplaytime).fork(
                    err => log.clos('err1', err),
                    data => {

                        // add experience1, see XP go up
                        expect(data.profile.playxp).toBe(gameObj1.maxplaytime);
                        expect(data.profile.hostxp).toBe(0);

                        // add experience1, see Level go up
                        expect(data.profile.playlevel).toBe(1);
                        expect(data.profile.hostlevel).toBe(0);
                        
                        addXP(x[2].email, true, gameObj2.maxplaytime).fork(
                            err => log.clos('err2', err),
                            data => {

                                // add experience2, see XP go up
                                expect(data.profile.playxp).toBe(gameObj1.maxplaytime + gameObj2.maxplaytime);
                                expect(data.profile.hostxp).toBe(gameObj2.maxplaytime);

                                // add experience1, see Level go up
                                expect(data.profile.playlevel).toBe(3);
                                expect(data.profile.hostlevel).toBe(2);

                                done();

                            }
                        );
                    }
                );
            });
        });
    });


    describe('Ticket Accounting', function() {

        it('AlterTicket', function(done) {

            const hasTicket = (user) => user.length > 0 && user[0].tickets > 0;
            const holdUser = composeP(testAccounts.AlterTicket(testAccount, R.__, 'paper', 'matchid', {$inc: {'tickets': -1, 'ticketshold': 1}}, hasTicket), prop('email'), generateUser);

            holdUser({tickets: 5, ticketshold: 0, ticketsconsume: 0}).then(function(nu) {

                expect(nu.tickets).toBe(4);
                expect(nu.ticketshold).toBe(1);
                expect(nu.ticketsconsume).toBe(0);

                holdUser({tickets: 0, ticketshold: 0, ticketsconsume: 0}).then().catch(function(err) {
                    expect(err.error.code).toBe(403);
                    done();

                });
            });
        });

        it('HoldTicket', function(done) {
            const holdUser = composeP(testAccounts.HoldTicket(testAccount, R.__, 'paper', 'matchid'), prop('email'), generateUser);

            holdUser({tickets: 5, ticketshold: 0, ticketsconsume: 0}).then(function(nu) {

                expect(nu.tickets).toBe(4);
                expect(nu.ticketshold).toBe(1);
                expect(nu.ticketsconsume).toBe(0);
                done();

            });
        });

        // release ticket
        it('ReleaseTicket', function(done) {
            // hold     -1
            // ticket   +1
            const releaseUser = composeP(testAccounts.ReleaseTicket(testAccount, R.__, 'paper', 'matchid'), prop('email'), generateUser);

            releaseUser({tickets: 5, ticketshold: 5, ticketsconsume: 0}).then(function(nu) {

                expect(nu.tickets).toBe(6);
                expect(nu.ticketshold).toBe(4);
                expect(nu.ticketsconsume).toBe(0);
                done();

            });
        });

        // release ticket
        it('ConsumeTicket', function(done) {

            // hold   -1
            const consumeUser = composeP(testAccounts.ConsumeTicket(testAccount, R.__, 'paper', 'matchid'), prop('email'), generateUser);

            consumeUser({tickets: 5, ticketshold: 5, ticketsconsume: 0}).then(function(nu) {

                expect(nu.tickets).toBe(5);
                expect(nu.ticketshold).toBe(4);
                expect(nu.ticketsconsume).toBe(1);
                done();

            });
        });

        it('unlink an avataricon file', function(done) {

            generateUser({}).then(function(u) {

                fs.createReadStream('app/public/upload/filename.jpg').pipe(fs.createWriteStream('app/public/upload/testfile.jpg')); 
                const initFile = (email) => new Task((reject, resolve) => testAccount.findOneAndUpdate({'email': email}, {$set: {'profile.avataricon': 'upload/testfile.jpg', 'profile.avataricontype':'file'}}, {new:true}).exec().then(resolve).catch(reject));

                initFile(u.email).fork(
                    err => log.clos('err', err),
                    updateUser => { 
                        testAccounts.UnlinkAvatar(nconf, testAccount, updateUser.email).then(function(x) {
                            expect(isNil(x.profile.avataricon)).toBe(true);
                            expect(isNil(x.profile.avataricontype)).toBe(true);
                            fs.unlink('./app/public/upload/testfile.jpg', err => { 
                                if (err) done();
                            });
                        }).catch(
                            err => log.clos('catch', err)
                        );
                    }
                );
            });
        });


        it('unlink non-existant avataricon file', function(done) {
            generateUser({}).then(function(u) {
                const initFile = (email) => new Task((reject, resolve) => testAccount.findOneAndUpdate({'email': email}, {$set: {'profile.avataricon': undefined}}, {new:true}).exec().then(resolve).catch(reject));

                initFile(u.email).fork(
                    err => log.clos('err', err),
                    updateUser => { 
                        testAccounts.UnlinkAvatar(nconf, testAccount, updateUser.email).then(
                            data => done()
                        );
                    }
                );
            });
        });

        it('getTestQuadrant', function(done) {
            expect(testAccounts.GetTestQuadrant('0ab30')).toBe(0);
            expect(testAccounts.GetTestQuadrant('1ab31')).toBe(1);
            expect(testAccounts.GetTestQuadrant('2ab32')).toBe(2);
            expect(testAccounts.GetTestQuadrant('3ab33')).toBe(3);
            expect(testAccounts.GetTestQuadrant('4ab34')).toBe(0);
            expect(testAccounts.GetTestQuadrant('5ab35')).toBe(1);
            expect(testAccounts.GetTestQuadrant('6ab36')).toBe(2);
            expect(testAccounts.GetTestQuadrant('7ab37')).toBe(3);
            expect(testAccounts.GetTestQuadrant('8ab38')).toBe(0);
            expect(testAccounts.GetTestQuadrant('9ab39')).toBe(1);
            expect(testAccounts.GetTestQuadrant('aab3a')).toBe(2);
            expect(testAccounts.GetTestQuadrant('bab3b')).toBe(3);
            expect(testAccounts.GetTestQuadrant('cab3c')).toBe(0);
            expect(testAccounts.GetTestQuadrant('dab3d')).toBe(1);
            expect(testAccounts.GetTestQuadrant('eab3e')).toBe(2);
            expect(testAccounts.GetTestQuadrant('fab3f')).toBe(3);
            done();
        });

        it('filterQuadKey', function(done) {
          expect(testAccounts.FilterQuadKey(1, {key:'023'})).toBe(false);
          expect(testAccounts.FilterQuadKey(1, {key:'123'})).toBe(true);
          expect(testAccounts.FilterQuadKey(0, {key:'023'})).toBe(true);
          expect(testAccounts.FilterQuadKey(0, {key:'123'})).toBe(false);
          done();
        });

        it('TaskReadPageSlug', function(done) {
          Promise.all([generateUser({}), generateUser({})]).then(function(u) {
            testAccounts.TaskReadPageSlug(testAccount, u[0].email).fork(
              err => log.clos('err', err),
              data => {
                expect(data).toBe(u[0].pageslug);
                testAccounts.TaskReadPageSlug(testAccount, 'bademail').fork(
                  err => testAccounts.TaskReadPageSlug(testAccount, u[1].email).fork(
                    err => log.clos('err', err),
                    dataslug => {
                      expect(dataslug).toBe(u[1].pageslug);
                      done();
                    }
                  )
                )
              }
            )
          })
        });

        it('TaskRecordLogin', function(done) {

            var prevDate = (days) => {
                var date = new Date();
                return(date.setDate(date.getDate() - days));
            }

            generateUser({}).then(function(u) {
                testAccount.findOneAndUpdate({email: u.email}, {$set: {loginlast: prevDate(10)}}, {new:true}).exec().then(function(old) {
                    testAccounts.TaskRecordLogin(testAccount, u.email).fork(
                        err => log.clos('err', err),
                        data => {
                            expect(new Date(data.loginlast).getDate()).toBe(new Date().getDate());
                            expect(data.logincount).toBe(1);
                            testAccounts.TaskRecordLogin(testAccount, u.email).fork(
                                err => log.clos('err', err),
                                data => {
                                    expect(new Date(data.loginlast).getDate()).toBe(new Date().getDate());
                                    expect(data.logincount).toBe(2);
                                    done();
                                }
                            )

                        }
                    )
                })
            });

        });

        it('TaskRecordActivity', function(done) {

            var prevDate = (days) => {
                var date = new Date();
                return(date.setDate(date.getDate() - days));
            }

            generateUser({}).then(function(u) {
                testAccount.findOneAndUpdate({email: u.email}, {$set: {activelast: prevDate(10)}}, {new:true}).exec().then(function(old) {
                    testAccounts.TaskRecordActivity(testAccount, u.email).fork(
                        err => log.clos('err', err),
                        data => {
                            expect(new Date(data.activelast).getDate()).toBe(new Date().getDate());
                            done();
                        }
                    )
                })
            });
        });

        it('BuildNote', function(done) {
            expect(testAccounts.BuildNote(['ta', 'tb', 'ur'])).toEqual({text:'ta', thumbnail:'tb', url: 'ur'});
            done();
        });

        it('BuildPush', function(done) {
            expect(testAccounts.BuildPush(['text', 'thumb', 'url'])['$push'].notifications.text).toBe('text');
            expect(testAccounts.BuildPush(['text', 'thumb', 'url'])['$push'].notifications.thumbnail).toBe('thumb');
            expect(testAccounts.BuildPush(['text', 'thumb', 'url'])['$push'].notifications.url).toBe('url');
            done();
        });
    });


    describe('Notifications and Mail', function() {
      it('TaskNotifyUser', function(done) {

          var text = 'd'; 

          var cacheGameObject = {
              numplays: 0,
              thumbnail: 'https://boardgamegeek.com/image/2673763/sword-sorcery',
              yearpublished: 2017,
              name: {
                  t: 'Sword & Sorcery',
                  sordindex: 1
              },
              subtype: 'boardgame',
              objectid: 170771,
              objecttype: 'thing'
          };
          var url = "myurl";
          var thumbnail = "mythumbnail";
          var noticetype = "type";

          Promise.all([generateUser({}), generateUser({}), cachegame(cacheGameObject)]).then(function(u) {
              testAccounts.TaskNotifyUser(testAccount, text, u[0].email, thumbnail, url, noticetype, false).fork(
                  err =>  log.clos('err', err),
                  data =>  {
                      expect(R.last(data).text).toBe(text);
                      expect(R.last(data).thumbnail).toBe(thumbnail);
                      expect(R.last(data).status).toBe('new');
                      expect(R.last(data).url).toBe(url);
                      expect(R.last(data).noticetype).toBe(noticetype);
                      expect(R.last(data).openNewTab).toBe(false);
                      done();
                  }
              );
          });
      });

      it('BuildMail', function(done) {
        expect(
          testAccounts.BuildMail(
            ['na', 'tn', 'ou', 'sb', 'tx', 'mt', 'new']
          )).toEqual({ 
            name : 'na', 
            thumbnail : 'tn', 
            pageslug : 'ou', 
            subject : 'sb', 
            text : 'tx',
            mailtype : 'mt',
            status: 'new'
          });
        done();
      });


      it('TaskGetMails', function(done) {

        var subject   = 's'; 
        var text      = 't'; 
        var mailtype  = 'inbox';

        Promise.all([generateUser({}), generateUser({})]).then(function(u) {

          testAccounts.TaskMailUser(
            testAccount, 
            u[0].pageslug, 
            u[0].profile.name, 
            u[0].profile.avataricon, 
            u[1].pageslug, 
            subject, 
            text, 
            mailtype, 
            'new'
          ).fork(
            err =>  log.clos('err', err),
            data =>  {
              expect(R.last(data).subject).toBe(subject);
              expect(R.last(data).text).toBe(text);
              expect(R.last(data).name).toBe(u[0].profile.name);
              expect(R.last(data).pageslug).toBe(u[0].pageslug);
              expect(R.last(data).thumbnail).toBe(u[0].profile.avataricon);
              expect(R.last(data).status).toBe('new');
               testAccounts.taskGetMail(testAccount, u[1].email, R.last(data).mailid).fork(
                  err =>  log.clos('err', err),
                  readmail =>  {
                    expect(readmail.mailid).toBe(R.last(data).mailid);
                    done();

                  }
                )
            }
          );
        });
      });

      it('elipsify', function(done) {
        expect(testAccounts.elipsify(['a', '']).get()).toBe('a');
        expect(testAccounts.elipsify(['a', 'b']).get()).toBe('a...');
        done();
      });


      it('elipsoidText', function(done) {
        expect(testAccounts.elipsoidText({sab:'hi'}).isNothing).toEqual(true);
        expect(testAccounts.elipsoidText({text:'hi'}).get()).toEqual({text:'hi'});
        expect(testAccounts.elipsoidText({
          a: 'hi',
          text:
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' 
          }
        ).get())
          .toEqual({
            a: 'hi',
            text:
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890...'
            }
          );
        done();
      });

      it('newMails', function(done) {
        expect(testAccounts.NewMails({}).isNothing).toBe(true);
        expect(testAccounts.NewMails({mails:[]}).get()).toEqual([]);
        expect(testAccounts.NewMails({mails:[
          {mailtype:'inbox', status:'new', test:'a'}, 
          {mailtype:'outbox', status:'new', test:'b'}, 
          {mailtype:'inbox', status:'read', test:'c'}, 
          {mailtype:'outbox', status:'read', test:'d'}, 
        ]}).get()).toEqual([{mailtype:'inbox', status:'new', test:'a'}]);
        done();
      });

      it('TaskMailUser - TaskReadMail - TaskGetNewMails - TaskGtSentMails', function(done) {

        var subject     = 's'; 
        var text        = 't'; 
        var mailtype      = "inbox";
        var mailsenttype  = "outbox";

        Promise.all([generateUser({}), generateUser({})]).then(function(u) {

          R.sequence(Task.of, [
            testAccounts.TaskMailUser(testAccount, u[0].pageslug, u[0].profile.name, u[0].profile.avataricon, u[1].pageslug, subject, text, mailtype, 'new'),
            testAccounts.TaskMailUser(testAccount, u[1].pageslug, u[0].profile.name, u[0].profile.avataricon, u[0].pageslug, subject, text, mailsenttype, 'new')
          ]).fork(
            err =>  log.clos('err', err),
            data =>  {
              expect(R.last(data[0]).subject).toBe(subject);
              expect(R.last(data[0]).text).toBe(text);
              expect(R.last(data[0]).name).toBe(u[0].profile.name);
              expect(R.last(data[0]).pageslug).toBe(u[0].pageslug);
              expect(R.last(data[0]).thumbnail).toBe(u[0].profile.avataricon);
              expect(R.last(data[0]).mailtype).toBe(mailtype);
              expect(R.last(data[0]).status).toBe('new');
              expect(R.last(data[1]).mailtype).toBe(mailsenttype);

              R.sequence(Task.of, [testAccounts.TaskGetNewMails(testAccount, u[1].email), testAccounts.TaskGetAllMails(testAccount, u[1].email), testAccounts.TaskGetNewMails(testAccount, u[0].email), testAccounts.TaskGetAllMails(testAccount, u[0].email), testAccounts.TaskGetSentMails(testAccount, u[0].email), testAccounts.TaskGetSentMails(testAccount, u[1].email)]).fork(
                err =>  log.clos('err', err),
                newmails =>  {
                  expect(newmails[0].length).toBe(1);
                  expect(newmails[1].length).toBe(1);
                  expect(newmails[2].length).toBe(0);
                  expect(newmails[3].length).toBe(0);
                  expect(newmails[4].length).toBe(1);
                  expect(newmails[5].length).toBe(0);
                  testAccounts.TaskReadMail(testAccount, u[1].email, R.last(data[0]).mailid).fork(
                    err =>  log.clos('err', err),
                    data =>  {
                      expect(data.n).toBe(1);
                      expect(data.nModified).toBe(1);
                      expect(data.ok).toBe(1);

                      R.sequence(Task.of, [testAccounts.TaskGetNewMails(testAccount, u[1].email), testAccounts.TaskGetAllMails(testAccount, u[1].email)]).fork(
                        err =>  log.clos('err', err),
                        data =>  {
                          expect(data[0].length).toBe(0);
                          expect(data[1].length).toBe(1);
                          done();
                        }
                      );
                    }
                  )
                }
              );
            }
          );
        });
      });


      it('buildURL', function(done) {
          expect(testAccounts.BuildURL('abc')).toBe('users?id=abc');
          expect(testAccounts.BuildURL('xyz')).toBe('users?id=xyz');
          done();
      });

      it('maybeDefaultImg', function(done) {
        expect(testAccounts.MaybeDefaultImg(Maybe.Nothing()).get()).toBe('/img/default-avatar-sq.jpg');
        expect(testAccounts.MaybeDefaultImg(Maybe.of('abc')).get()).toBe('abc');
        done();
      });

      it('findTopGames', function(done) {
        var games0 = [];
        var games1 = [{yearpublished: 10, matching: {wanttoplay:true}, name:'a'}];
        var games2 = [{yearpublished: 10, matching: {wanttoplay:false}, name:'a'}, {yearpublished: 10, matching: {wanttoplay:true}, name:'b'}, {yearpublished: 9, matching: {wanttoplay:true}, name:'c'}];
        var games3 = [{yearpublished: 10, matching: {wanttoplay:true}, name:'a'}, {yearpublished: 9, matching: {wanttoplay:true}, name:'b'}, {yearpublished: 4, matching: {wanttoplay:true}, name:'c'}];

        expect(testAccounts.FindTopGames(games0)).toEqual([])
        expect(testAccounts.FindTopGames(games1)).toEqual([ { yearpublished : 10, matching : { wanttoplay : true }, name: 'a' } ])
        expect(testAccounts.FindTopGames(games2)).toEqual([ { yearpublished : 10, matching : { wanttoplay : true }, name:'b' }, { yearpublished : 9, matching : { wanttoplay : true }, name:'c' } ])
        expect(testAccounts.FindTopGames(games3)).toEqual([ { yearpublished : 10, matching : { wanttoplay : true }, name:'a' }, { yearpublished : 9, matching : { wanttoplay : true }, name:'b' } ])
        done();
      });

      it('TaskNotifyNewNearbyUsers', function(done) {

        var locName = 'Chapultec Castle';
        var lat = 19.420440;
        var lng = -99.181935;

        var localarea = {
          $set: {
            profile: {
              private: 1,
              name: 'henry',
              avataricon: 'avatar.jpg',
              localarea: {
                loc: {
                  coordinates: [lng, lat],
                  type: 'Point'
                }, 
                name: locName
              }
            },
            games: [
              {name: 'abclkj', matching: {wanttoplay:true}, yearpublished: 2001},
              {name: 'abcdef', matching: {wanttoplay:true}, yearpublished: 2002},
              {name: 'abcdef', matching: {wanttoplay:false}, yearpublished: 2004},
              {name: 'dflsdfkj', matching: {wanttoplay:true}, yearpublished: 2005},
              {name: 'abccdflkj', matching: {wanttoplay:true}, yearpublished: 2003},
              {name: 'acepoi', matching: {wanttoplay:false}, yearpublished: 2000}
            ]
          }
        }
        
        testAccount.remove({'profile.localarea.name': locName}).exec().then((a) => Promise.all([generateUser({}), generateUser({}), generateUser({})]).then(function(u) {

          Promise.all([testAccount.findOneAndUpdate({email: u[0].email}, merge({key:2334}, localarea), {new:true}), testAccount.findOneAndUpdate({email: u[1].email}, merge({key:123}, localarea), {new:true}), testAccount.findOneAndUpdate({email: u[2].email}, merge({key:234}, localarea), {new:true})]).then((y) => {
            testAccounts.TaskNotifyNewNearbyUsers(testAccount, u[0].email).fork(
              err => log.clos('err', err),
              data => {
                expect(data[0].length).toBe(1);
                expect(data[1].length).toBe(1);
                done();
              }
            )
          }).catch();
        }));
      });

      it('TaskReadNotification', function(done) {

        var text1 = 'd1'; 
        var text2 = 'd2'; 

        var cacheGameObject = {
          numplays: 0,
          thumbnail: 'https://boardgamegeek.com/image/2673763/sword-sorcery',
          yearpublished: 2017,
          name: {
            t: 'Sword & Sorcery',
            sordindex: 1
          },
          subtype: 'boardgame',
          objectid: 170771,
          objecttype: 'thing'
        };
        var url = "myurl";
        var thumbnail = "mythumbnail";
        var type = "typeX";

        Promise.all([generateUser({}), generateUser({}), cachegame(cacheGameObject)]).then(function(u) {
          testAccounts.TaskNotifyUser(testAccount, text1, u[0].email, thumbnail, url, type, false).fork(
            err => log.clos('err', err),
            datan1 => {
              testAccounts.TaskNotifyUser(testAccount, text2, u[0].email, thumbnail, url, type, false).fork(
                err => log.clos('err', err),
                datan2 => {
                  testAccounts.TaskReadNotification(testAccount, u[0].email, datan1[0].noteid).fork(
                    err => log.clos('err', err),
                    data => {
                      expect(data.n).toBe(1);
                      expect(data.nModified).toBe(1);
                      expect(data.ok).toBe(1);
                      testAccount.findOne({'email': u[0].email}).exec().then(readdata => {
                        const notify2 = R.compose(R.find(R.propEq('noteid', datan2[1].noteid)), R.prop('notifications'));
                        expect(notify2(readdata).status).toBe('new');
                        testAccounts.TaskGetNewNotifications(testAccount, u[0].email).fork(                                             
                          err => log.clos('err', err),
                          data => {
                            expect(data.length).toBe(1);
                            expect(data[0].status).toBe('new');
                            done();
                          }
                        ); 
                      });
                    }
                  )
                }
              )
            }
          );
        });
      });

      it('taskClearMatchNotifications', function(done) {

        var text1 = 'd1'; 
        var text2 = 'd2'; 

        var cacheGameObject = {
          numplays: 0,
          thumbnail: 'https://boardgamegeek.com/image/2673763/sword-sorcery',
          yearpublished: 2017,
          name: {
            t: 'Sword & Sorcery',
            sordindex: 1
          },
          subtype: 'boardgame',
          objectid: 170771,
          objecttype: 'thing'
        };

        var matchid = "abcdef";
        var url = "match?matchkey=";
        var thumbnail = "mythumbnail";
        var type = "typeX";

        Promise.all([generateUser({}), generateUser({}), cachegame(cacheGameObject)]).then(function(u) {
          testAccounts.TaskNotifyUser(testAccount, text1, u[0].email, thumbnail, url + matchid, type, false).fork(
            err => log.clos('err', err),
            datan1 => {
              testAccounts.TaskNotifyUser(testAccount, text2, u[0].email, thumbnail, url + matchid, type, false).fork(
                err => log.clos('err', err),
                datan2 => {
                  testAccounts.taskClearMatchNotifications(testAccount, u[0].email, url + matchid).fork(
                    err => log.clos('err', err),
                    data => {
                      expect(data.n).toBe(1);
                      expect(data.nModified).toBe(1);
                      expect(data.ok).toBe(1);
                      testAccount.findOne({'email': u[0].email}).exec().then(readdata => {
                        expect(readdata.notifications.length).toBe(0);
                        done();
                      });
                    }
                  )
                }
              )
            }
          );
        });
      });


      it('TaskNotifyUser', function(done) {

          var text = 'd'; 

          var cacheGameObject = {
              numplays: 0,
              thumbnail: 'https://boardgamegeek.com/image/2673763/sword-sorcery',
              yearpublished: 2017,
              name: {
                  t: 'Sword & Sorcery',
                  sordindex: 1
              },
              subtype: 'boardgame',
              objectid: 170771,
              objecttype: 'thing'
          };
          var url = "myurl";
          var thumbnail = "mythumbnail";
          var noticetype = "type";

          Promise.all([generateUser({}), generateUser({}), cachegame(cacheGameObject)]).then(function(u) {
              testAccounts.TaskNotifyUser(testAccount, text, u[0].email, thumbnail, url, noticetype, false).fork(
                  err =>  log.clos('err', err),
                  data =>  {
                      expect(R.last(data).text).toBe(text);
                      expect(R.last(data).thumbnail).toBe(thumbnail);
                      expect(R.last(data).status).toBe('new');
                      expect(R.last(data).url).toBe(url);
                      expect(R.last(data).noticetype).toBe(noticetype);
                      expect(R.last(data).openNewTab).toBe(false);
                      done();
                  }
              );
          });
      });

      it('BuildMail', function(done) {
        expect(
          testAccounts.BuildMail(
            ['na', 'tn', 'ou', 'sb', 'tx', 'mt', 'new']
          )).toEqual({ 
            name : 'na', 
            thumbnail : 'tn', 
            pageslug : 'ou', 
            subject : 'sb', 
            text : 'tx',
            mailtype : 'mt',
            status: 'new'
          });
        done();
      });


      it('TaskGetMails', function(done) {

        var subject   = 's'; 
        var text      = 't'; 
        var mailtype  = 'inbox';

        Promise.all([generateUser({}), generateUser({})]).then(function(u) {

          testAccounts.TaskMailUser(
            testAccount, 
            u[0].pageslug, 
            u[0].profile.name, 
            u[0].profile.avataricon, 
            u[1].pageslug, 
            subject, 
            text, 
            mailtype, 
            'new'
          ).fork(
            err =>  log.clos('err', err),
            data =>  {
              expect(R.last(data).subject).toBe(subject);
              expect(R.last(data).text).toBe(text);
              expect(R.last(data).name).toBe(u[0].profile.name);
              expect(R.last(data).pageslug).toBe(u[0].pageslug);
              expect(R.last(data).thumbnail).toBe(u[0].profile.avataricon);
              expect(R.last(data).status).toBe('new');
               testAccounts.taskGetMail(testAccount, u[1].email, R.last(data).mailid).fork(
                  err =>  log.clos('err', err),
                  readmail =>  {
                    expect(readmail.mailid).toBe(R.last(data).mailid);
                    done();

                  }
                )
            }
          );
        });
      });

      it('elipsify', function(done) {
        expect(testAccounts.elipsify(['a', '']).get()).toBe('a');
        expect(testAccounts.elipsify(['a', 'b']).get()).toBe('a...');
        done();
      });


      it('elipsoidText', function(done) {
        expect(testAccounts.elipsoidText({sab:'hi'}).isNothing).toEqual(true);
        expect(testAccounts.elipsoidText({text:'hi'}).get()).toEqual({text:'hi'});
        expect(testAccounts.elipsoidText({
          a: 'hi',
          text:
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' + 
            '12345678901234567890' 
          }
        ).get())
          .toEqual({
            a: 'hi',
            text:
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890' + 
              '12345678901234567890...'
            }
          );
        done();
      });

      it('newMails', function(done) {
        expect(testAccounts.NewMails({}).isNothing).toBe(true);
        expect(testAccounts.NewMails({mails:[]}).get()).toEqual([]);
        expect(testAccounts.NewMails({mails:[
          {mailtype:'inbox', status:'new', test:'a'}, 
          {mailtype:'outbox', status:'new', test:'b'}, 
          {mailtype:'inbox', status:'read', test:'c'}, 
          {mailtype:'outbox', status:'read', test:'d'}, 
        ]}).get()).toEqual([{mailtype:'inbox', status:'new', test:'a'}]);
        done();
      });

      it('TaskMailUser - TaskReadMail - TaskGetNewMails - TaskGtSentMails', function(done) {

        var subject     = 's'; 
        var text        = 't'; 
        var mailtype      = "inbox";
        var mailsenttype  = "outbox";

        Promise.all([generateUser({}), generateUser({})]).then(function(u) {

          R.sequence(Task.of, [
            testAccounts.TaskMailUser(testAccount, u[0].pageslug, u[0].profile.name, u[0].profile.avataricon, u[1].pageslug, subject, text, mailtype, 'new'),
            testAccounts.TaskMailUser(testAccount, u[1].pageslug, u[0].profile.name, u[0].profile.avataricon, u[0].pageslug, subject, text, mailsenttype, 'new')
          ]).fork(
            err =>  log.clos('err', err),
            data =>  {
              expect(R.last(data[0]).subject).toBe(subject);
              expect(R.last(data[0]).text).toBe(text);
              expect(R.last(data[0]).name).toBe(u[0].profile.name);
              expect(R.last(data[0]).pageslug).toBe(u[0].pageslug);
              expect(R.last(data[0]).thumbnail).toBe(u[0].profile.avataricon);
              expect(R.last(data[0]).mailtype).toBe(mailtype);
              expect(R.last(data[0]).status).toBe('new');
              expect(R.last(data[1]).mailtype).toBe(mailsenttype);

              R.sequence(Task.of, [testAccounts.TaskGetNewMails(testAccount, u[1].email), testAccounts.TaskGetAllMails(testAccount, u[1].email), testAccounts.TaskGetNewMails(testAccount, u[0].email), testAccounts.TaskGetAllMails(testAccount, u[0].email), testAccounts.TaskGetSentMails(testAccount, u[0].email), testAccounts.TaskGetSentMails(testAccount, u[1].email)]).fork(
                err =>  log.clos('err', err),
                newmails =>  {
                  expect(newmails[0].length).toBe(1);
                  expect(newmails[1].length).toBe(1);
                  expect(newmails[2].length).toBe(0);
                  expect(newmails[3].length).toBe(0);
                  expect(newmails[4].length).toBe(1);
                  expect(newmails[5].length).toBe(0);
                  testAccounts.TaskReadMail(testAccount, u[1].email, R.last(data[0]).mailid).fork(
                    err =>  log.clos('err', err),
                    data =>  {
                      expect(data.n).toBe(1);
                      expect(data.nModified).toBe(1);
                      expect(data.ok).toBe(1);

                      R.sequence(Task.of, [testAccounts.TaskGetNewMails(testAccount, u[1].email), testAccounts.TaskGetAllMails(testAccount, u[1].email)]).fork(
                        err =>  log.clos('err', err),
                        data =>  {
                          expect(data[0].length).toBe(0);
                          expect(data[1].length).toBe(1);
                          done();
                        }
                      );
                    }
                  )
                }
              );
            }
          );
        });
      });


      it('buildURL', function(done) {
          expect(testAccounts.BuildURL('abc')).toBe('users?id=abc');
          expect(testAccounts.BuildURL('xyz')).toBe('users?id=xyz');
          done();
      });

      it('maybeDefaultImg', function(done) {
        expect(testAccounts.MaybeDefaultImg(Maybe.Nothing()).get()).toBe('/img/default-avatar-sq.jpg');
        expect(testAccounts.MaybeDefaultImg(Maybe.of('abc')).get()).toBe('abc');
        done();
      });

      it('findTopGames', function(done) {
        var games0 = [];
        var games1 = [{yearpublished: 10, matching: {wanttoplay:true}, name:'a'}];
        var games2 = [{yearpublished: 10, matching: {wanttoplay:false}, name:'a'}, {yearpublished: 10, matching: {wanttoplay:true}, name:'b'}, {yearpublished: 9, matching: {wanttoplay:true}, name:'c'}];
        var games3 = [{yearpublished: 10, matching: {wanttoplay:true}, name:'a'}, {yearpublished: 9, matching: {wanttoplay:true}, name:'b'}, {yearpublished: 4, matching: {wanttoplay:true}, name:'c'}];

        expect(testAccounts.FindTopGames(games0)).toEqual([])
        expect(testAccounts.FindTopGames(games1)).toEqual([ { yearpublished : 10, matching : { wanttoplay : true }, name: 'a' } ])
        expect(testAccounts.FindTopGames(games2)).toEqual([ { yearpublished : 10, matching : { wanttoplay : true }, name:'b' }, { yearpublished : 9, matching : { wanttoplay : true }, name:'c' } ])
        expect(testAccounts.FindTopGames(games3)).toEqual([ { yearpublished : 10, matching : { wanttoplay : true }, name:'a' }, { yearpublished : 9, matching : { wanttoplay : true }, name:'b' } ])
        done();
      });

      it('TaskNotifyNewNearbyUsers', function(done) {

        var locName = 'Chapultec Castle';
        var lat = 19.420440;
        var lng = -99.181935;

        var localarea = {
          $set: {
            profile: {
              private: 1,
              name: 'henry',
              avataricon: 'avatar.jpg',
              localarea: {
                loc: {
                  coordinates: [lng, lat],
                  type: 'Point'
                }, 
                name: locName
              }
            },
            games: [
              {name: 'abclkj', matching: {wanttoplay:true}, yearpublished: 2001},
              {name: 'abcdef', matching: {wanttoplay:true}, yearpublished: 2002},
              {name: 'abcdef', matching: {wanttoplay:false}, yearpublished: 2004},
              {name: 'dflsdfkj', matching: {wanttoplay:true}, yearpublished: 2005},
              {name: 'abccdflkj', matching: {wanttoplay:true}, yearpublished: 2003},
              {name: 'acepoi', matching: {wanttoplay:false}, yearpublished: 2000}
            ]
          }
        }
        
        testAccount.remove({'profile.localarea.name': locName}).exec().then((a) => Promise.all([generateUser({}), generateUser({}), generateUser({})]).then(function(u) {
          Promise.all([testAccount.findOneAndUpdate({email: u[0].email}, merge({key:2334}, localarea), {new:true}), testAccount.findOneAndUpdate({email: u[1].email}, merge({key:123}, localarea), {new:true}), testAccount.findOneAndUpdate({email: u[2].email}, merge({key:234}, localarea), {new:true})]).then((y) => {
            testAccounts.TaskNotifyNewNearbyUsers(testAccount, u[0].email).fork(
              err => log.clos('err', err),
              data => {
                expect(data[0].length).toBe(1);
                expect(data[1].length).toBe(1);
                done();
              }
            )
          }).catch();
        }));
      });

    });


    it('logPurchase', function(done) {
        var date = new Date();
        var ordercode = shortid.generate();

        generateUser({}).then(function(u) {
            testAccounts.LogPurchase(testAccount, u.email, ordercode, [{itemcode:'a', price: 20}], 20, 'AUD', date).fork(
                err => log.clos('err', err),
                data => {
                    expect(data.ordercode).toBe(ordercode);
                    expect(data.total).toBe(20);
                    expect(data.currency).toBe('AUD');
                    expect(data.date).toEqual(date);
                    done();
                }
            )
        });
    });

    it("WhenTrue", function(done) {
        expect(testAccounts.WhenTrue('a', 'b', true)).toBe('a');
        expect(testAccounts.WhenTrue('a', 'b', false)).toBe('b');
        done();
    });

    it("SafeWhenPremium", function(done) {
        expect(testAccounts.SafeWhenPremium('a', 'b', {})).toBe('b');
        expect(testAccounts.SafeWhenPremium('a', 'b', {premium:true})).toBe('b');
        expect(testAccounts.SafeWhenPremium('a', 'b', {premium:{isactive:true}})).toBe('a');
        expect(testAccounts.SafeWhenPremium('a', 'b', {premium:{isactive:false}})).toBe('b');
        done();
    });

    it("SafeWhenStaff", function(done) {
        expect(testAccounts.SafeWhenStaff('a', 'b', {})).toBe('b');
        expect(testAccounts.SafeWhenStaff('a', 'b', {premium:true})).toBe('b');
        expect(testAccounts.SafeWhenStaff('a', 'b', {premium:{isstaff:true}})).toBe('a');
        expect(testAccounts.SafeWhenStaff('a', 'b', {premium:{isstaff:false}})).toBe('b');
        done();
    });

    it("SafeWhenType", function(done) {
        expect(testAccounts.SafeWhenType('stand', 'staff', 'prem', {})).toBe('stand');
        expect(testAccounts.SafeWhenType('stand', 'staff', 'prem', {premium:{}})).toBe('stand');
        expect(testAccounts.SafeWhenType('stand', 'staff', 'prem', {premium:{isstaff:true}})).toBe('staff');
        expect(testAccounts.SafeWhenType('stand', 'staff', 'prem', {premium:{isactive:true}})).toBe('prem');
        done();
    });


    it('ongoingSubscription', function(done) {

        const itemRG1 = 'rfggold-once';
        const itemRG1tp = 'rfggold-once-12pack';

        const itemRGC = 'rfggold-credit';
        const itemRGCtp = 'rfggold-credit-12pack';

        const itemRGO = 'rfggold-ongoing';
        const itemRGOtp = 'rfggold-ongoing-12pack';

        const quant3 = 3;

        Promise.all([generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({}), generateUser({})]).then(function(u) {
            R.sequence(Task.of, [
                testAccounts.PurchaseItem(testAccount, u[0].email, itemRG1, quant3),
                testAccounts.PurchaseItem(testAccount, u[1].email, itemRG1tp, 1),
                testAccounts.PurchaseItem(testAccount, u[2].email, itemRGC, quant3),
                testAccounts.PurchaseItem(testAccount, u[3].email, itemRGCtp, quant3),
                testAccounts.PurchaseItem(testAccount, u[4].email, itemRGO, quant3),
                testAccounts.PurchaseItem(testAccount, u[5].email, itemRGOtp, quant3),
            ]).fork(
                err => log.clos('err', err),
                purchase => {

                    expect(purchase[0].isactive).toBe(true);
                    expect(purchase[1].isactive).toBe(true);
                    expect(moment(purchase[0].activetodate).diff(new Date(), 'days')).toBeGreaterThan(87);
                    expect(moment(purchase[0].activetodate).diff(new Date(), 'days')).toBeLessThan(93);
                    expect(moment(purchase[1].activetodate).diff(new Date(), 'days')).toBeGreaterThan(362);
                    expect(moment(purchase[1].activetodate).diff(new Date(), 'days')).toBeLessThan(385);

                    expect(purchase[2].creditsshare).toBe(3);
                    expect(purchase[3].creditsshare).toBe(36);
                    expect(purchase[4].isongoing).toBe(true);
                    expect(purchase[4].isongoingsize).toBe(3);
                    expect(purchase[5].isongoing).toBe(true);
                    expect(purchase[5].isongoingsize).toBe(36);

                    testAccounts.PurchaseItem(testAccount, u[0].email, itemRG1, 1).fork(
                        (err) => log.clos('err', err),
                        (x) => {
                            expect(x.isactive).toBe(true);
                            expect(moment(x.activetodate).diff(new Date(), 'days')).toBeGreaterThan(116);
                            expect(moment(x.activetodate).diff(new Date(), 'days')).toBeLessThan(123);
                            done();
                        }
                    );

                }
            )
        });
    });

    it('datePlusMonths', function(done) {
        expect(moment(testAccounts.DatePlusMonths(new Date(), 1)).diff(new Date(), 'days')).toBeGreaterThan(26);
        expect(moment(new Date()).diff(testAccounts.DatePlusMonths(new Date(), 1), 'days')).toBeLessThan(32);
        expect(moment(testAccounts.DatePlusMonths(new Date(), 3)).diff(new Date(), 'days')).toBeGreaterThan(87);
        expect(moment(new Date()).diff(testAccounts.DatePlusMonths(new Date(), 3), 'days')).toBeLessThan(93);
        done();
    });

    it('getExpireDate', function(done) {
        var date1 = new Date;
        var date2 = testAccounts.DatePlusMonths(new Date(), 1);
        expect(testAccounts.MaxDate(date1, date2)).toBe(date2);
        done();
    });


    it('getExpireDate', function(done) {
        const expireDate = testAccounts.DatePlusMonths(new Date(), 1);
        const expirePrevDate = testAccounts.DatePlusMonths(new Date(), -1);

        expect(moment(testAccounts.GetExpireDate({})).diff(new Date())).toBeLessThan(10);
        expect(moment(testAccounts.GetExpireDate({})).diff(new Date())).toBeGreaterThan(-1);
        expect(moment(testAccounts.GetExpireDate({premium:{}})).diff(new Date())).toBeLessThan(10);
        expect(moment(testAccounts.GetExpireDate({premium:{}})).diff(new Date())).toBeGreaterThan(-10);
        expect(testAccounts.GetExpireDate({premium:{activetodate: expireDate}})).toBe(expireDate);
        expect(moment(testAccounts.GetExpireDate({premium:{activetodate: expirePrevDate}})).diff(new Date())).toBeLessThan(10);
        expect(moment(testAccounts.GetExpireDate({premium:{activetodate: expirePrevDate}})).diff(new Date())).toBeGreaterThan(-10);

        done();
    });

    it('getUserName', function(done) {
        expect(testAccounts.GetUserName({})).toBe('An anonymous player');
        expect(testAccounts.GetUserName({profile:{}})).toBe('An anonymous player');
        expect(testAccounts.GetUserName({profile:{name:'jack'}})).toBe('jack');
        done();
    });

    it('hasGems', function(done) {
        expect(testAccounts.HasGems({})).toBe(false);
        expect(testAccounts.HasGems({premium:{}})).toBe(false);
        expect(testAccounts.HasGems({premium:{creditsshare:1}})).toBe(true);
        done();
    });

    it('getAvatarIcon', function(done) {
      expect(testAccounts.GetAvatarIcon({})).toBe('/img/u_roll_icon1-01.png');
      expect(testAccounts.GetAvatarIcon({profile:{}})).toBe('/img/u_roll_icon1-01.png');
      expect(testAccounts.GetAvatarIcon({profile:{avataricon:'abc'}})).toBe('abc');
      done();
    });

    it('resolvePremium', function(done) {
        testAccounts.ResolvePremium((a) => {
            expect(a).toBe('abc');
            testAccounts.ResolvePremium((a) => {
                expect(a).toBe(undefined);
                done();
            })({});
        })({premium:'abc'});
    });

    it('receiveMembership', function(done) {
        generateUser({}).then((u) => testAccounts.ReceiveMembership(testAccount, 'from', u.email, 'desc', 'ref', 1, new Date('1979-07-31')).fork(
            err => log.clos('err', err),
            data => {
                expect(data.isactive).toBe(true);
                expect(data.creditsconsumed).toBe(0);
                expect(data.creditsshare).toBe(0);
                expect(data.creditsself).toBe(0);
                expect(data.activetodate).toBe('1979-08-31T00:00:00.000Z');

                expect(data.creditlog[0].activeto).toBe('1979-08-31T00:00:00.000Z');
                expect(data.creditlog[0].reference).toBe('ref');
                expect(data.creditlog[0].to).toBe(u.email);
                expect(data.creditlog[0].from).toBe('from');
                expect(data.creditlog[0].description).toBe('desc');

                expect(moment(data.creditlog[0].date).diff(new Date())).toBeLessThan(1);

                testAccounts.ReceiveMembership(testAccount, 'from', u.email, 'desc', 'ref', 1, new Date('1979-08-31')).fork(
                    err => log.clos('err', err),
                    data => {
                        expect(data.activetodate).toBe('1979-09-30T00:00:00.000Z');
                        expect(data.creditlog[1].activeto).toBe('1979-09-30T00:00:00.000Z');
                        done();
                    }
                )
            }
        ));
    });

    it('extendCreditExpiry', function(done) {
        generateUser({}).then((u) => testAccounts.ExtendCreditExpiry(testAccount, 'from', u, 'desc', 'ref', 1).fork(
            err => log.clos('err', err),
            data => {
                expect(data.isactive).toBe(true);
                expect(data.creditsconsumed).toBe(0);
                expect(data.creditsshare).toBe(0);
                expect(data.creditsself).toBe(0);

                expect(moment(data.activetodate).diff(new Date(), 'days')).toBeLessThan(32);
                expect(moment(data.activetodate).diff(new Date(), 'days')).toBeGreaterThan(26);
                expect(moment(data.creditlog[0].activeto).diff(new Date(), 'days')).toBeLessThan(32);
                expect(moment(data.creditlog[0].activeto).diff(new Date(), 'days')).toBeGreaterThan(26);

                expect(data.creditlog[0].reference).toBe('ref');
                expect(data.creditlog[0].to).toBe(u.email);
                expect(data.creditlog[0].from).toBe('from');
                expect(data.creditlog[0].description).toBe('desc');

                testAccounts.ExtendCreditExpiry(testAccount, 'from', R.assoc('premium', data, u), 'desc', 'ref', 1).fork(
                    err => log.clos('err', err),
                    data => {
                        expect(moment(data.activetodate).diff(new Date(), 'days')).toBeLessThan(62);
                        expect(moment(data.activetodate).diff(new Date(), 'days')).toBeGreaterThan(55);
                        expect(moment(data.creditlog[1].activeto).diff(new Date(), 'days')).toBeLessThan(62);
                        expect(moment(data.creditlog[1].activeto).diff(new Date(), 'days')).toBeGreaterThan(55);
                        done();
                    }
                )
            }
        ));
    });


    it('getEmail', function(done) {
        expect(testAccounts.GetEmail({})).toBe('');
        expect(testAccounts.GetEmail({email:'abc'})).toBe('abc');
        done();
    });


    it('giftGem', function(done) {

      var refX = shortid.generate();
      var refY = shortid.generate();

      Promise.all([generateUser({premium:{creditsshare: 2}}), generateUser({}), generateUser({})]).then(function(u) {
        testAccounts.GiftGem(testAccount, refX, u[2].email, u[1].pageslug).fork(
          err => testAccounts.GiftGem(testAccount, refX, u[0].email, u[1].pageslug).fork(
            err => log.clos('err.gifting1', err),
            userfrom => {
              expect(userfrom.creditsshare).toBe(1);
              expect(userfrom.creditlog[0].from).toBe(u[0].email);
              expect(userfrom.creditlog[0].to).toBe(u[1].email);
              expect(moment(userfrom.creditlog[0].date).diff(new Date())).toBeLessThan(1);
              expect(userfrom.creditlog[0].description).toBe('gift-send');
              expect(userfrom.creditlog[0].reference).toBe(refX);

              testAccounts.ReadAnAccountFromEmail(testAccount, u[1].email).then((userto) => {
                expect(userto.email).toBe(u[1].email);
                expect(userto.premium.isactive).toBe(true);
                expect(moment( moment(new Date()).add(1, 'months') ).diff(userto.premium.activetodate )).toBeLessThan(20);
                expect(userto.premium.creditlog[0].from).toBe(u[0].email);
                expect(userto.premium.creditlog[0].to).toBe(u[1].email);
                expect(moment(userto.premium.creditlog[0].date).diff(new Date())).toBeLessThan(1);
                expect(userto.premium.creditlog[0].description).toBe('gift-receive');
                expect(userto.premium.creditlog[0].reference).toBe(refX);
                expect(moment( moment(new Date()).add(1, 'months') ).diff(userto.premium.creditlog[0].activeto )).toBeLessThan(20);
                testAccounts.GiftGem(testAccount, refY, u[0].email, u[1].pageslug).fork(
                  err => log.clos('err.gifting2', err),
                  userfrom => {
                    expect(userfrom.creditsshare).toBe(0);
                    testAccounts.ReadAnAccountFromEmail(testAccount, u[1].email).then((userto) => {
                      expect(userto.email).toBe(u[1].email);
                      expect(userto.premium.isactive).toBe(true);
                      expect(moment(new Date()).add(2, 'months').diff(userto.premium.activetodate, 'days')).toBeLessThan(30);
                      expect(userto.premium.creditlog[1].from).toBe(u[0].email);
                      expect(userto.premium.creditlog[1].to).toBe(u[1].email);
                      expect(moment(userto.premium.creditlog[1].date).diff(new Date())).toBeLessThan(1);
                      expect(userto.premium.creditlog[1].description).toBe('gift-receive');
                      expect(userto.premium.creditlog[1].reference).toBe(refY);
                      expect(moment(new Date()).add(2, 'months').diff(userto.premium.creditlog[1].activeto, 'days')).toBeLessThan(30);

                      // fail because we're out of gems
                      testAccounts.GiftGem(testAccount, refY, u[0].email, u[1].pageslug).fork(
                        err => {
                          expect(err).toBe('user has no gems');
                          done()
                        }
                      )
                    }
                  )
                }
              )
            })
          }),
          data => log.clos('should fail due to no gems', data)
        );
      });
    });

    it('taskGenerateLevelUpGem', function(done) {
      Promise.all([generateUser({premium:{creditsshare: 2}}), generateUser({})]).then(function(u) {
        testAccounts.TaskGenerateLevelUpGem(testAccount, u[0].email, 5).fork(
          err => log.clos('err.gifting1', err),
          userfrom => {
            expect(userfrom.email).toBe(u[0].email);
            expect(userfrom.premium.creditsshare).toBe(3);
            expect(userfrom.premium.creditlog[0].from).toBe('rollforgroup');
            expect(userfrom.premium.creditlog[0].to).toBe(u[0].email);
            expect(moment(userfrom.premium.creditlog[0].date).diff(new Date())).toBeLessThan(1);
            expect(userfrom.premium.creditlog[0].description).toBe('levelup-5');

            testAccounts.TaskGenerateLevelUpGem(testAccount, u[1].email, 10).fork(
              err => log.clos('err.gifting2', err),
              userfrom => {
               expect(userfrom.email).toBe(u[1].email);
               expect(userfrom.premium.creditsshare).toBe(1);
               done();
              }
            )
          }
        );
      });
    });

  it('regenerateIcalendarId', function(done) {
    generateUser({icalkey:'123abc'}).then(function(u) {
      expect(u.icalkey).toBe('123abc');
      testAccounts.RegenerateCalendarId(testAccount, u.email).fork(
        err =>  log.clos('err', err),
        data => {
          expect(data.icalkey).not.toBe('123abc');
          done();
        }
      );
    });
  });

  const yesterday = new Date(Date.now() - 86400000);
  const tomorrow = new Date(Date.now() + 86400000);

  it('unPremiumMemberships', function(done) {
    testAccounts.unPremiumMemberships(testAccount).fork(
      err => log.clos('unPremiumMembership', err),
      data => {
        generateUser({premium:{isactive:true, activetodate: yesterday}}).then(function(u) {
          testAccounts.unPremiumMemberships(testAccount).fork(
            err => log.clos('unPremiumMembership', err),
            data => {
              expect(data.n).toBe(1);
              expect(data.nModified).toBe(1);
              expect(data.ok).toBe(1);
              generateUser({premium:{isactive:true, activetodate: tomorrow}}).then(function(u) {
                testAccounts.unPremiumMemberships(testAccount).fork(
                  err => log.clos('unPremiumMembership', err),
                  data => {
                    expect(data.n).toBe(0);
                    expect(data.nModified).toBe(0);
                    expect(data.ok).toBe(1);
                    done();
                  }
                );
              });
            }
          );
        });
      }
    );
  });

  it('unPremiumMemberships', function(done) {
    generateUser({}).then(function(u) {
      testAccounts.spamUser(testAccount, u.email).fork(
        err => log.clos('spamuser', err),
        data => {
          expect(data.n).toBe(1);
          expect(data.nModified).toBe(1);
          expect(data.ok).toBe(1);
          done();
        }
      );
    });
  });

});


describe('close server', function() {
  it('connection close', function(done) {
    mongo.CloseDatabase();
    done();
  });
});
