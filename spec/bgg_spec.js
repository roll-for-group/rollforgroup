var bgg = require('../app/bgg.js');
var getrequest = require('request');
var log = require('../app/log.js');

describe('bgg.js Board Game Geek Wrapper (bgg)', function() {

    describe('fix xml character encoding in strings', function() {
        it('RemoveXMLCharacterCodes', function(done) {
            expect(bgg.RemoveXMLCharacterCodes("tom&amp;jerry")).toBe("tom&jerry");
            expect(bgg.RemoveXMLCharacterCodes("ben&amp;jerry")).toBe("ben&jerry");
            expect(bgg.RemoveXMLCharacterCodes("ben&amp;jerry&amp;tom")).toBe("ben&jerry&tom");
            expect(bgg.RemoveXMLCharacterCodes("less&lt;than")).toBe("less<than");
            expect(bgg.RemoveXMLCharacterCodes("greater&gt;than")).toBe("greater>than");
            expect(bgg.RemoveXMLCharacterCodes("&quot;hello&quot;")).toBe('\\\"hello\\\"');
            expect(bgg.RemoveXMLCharacterCodes("don&apos;t")).toBe("don't");
            done();
        });
    });
    
    describe('getting bgg boardgame object', function() {

        it('retreive a valid game', function(done) {
            bgg.GetBGGBoardGameById(500).then(function(x){
                expect(x.type).toBe('boardgame');
                expect(x.id).toBe(500);
                done();
            }, function(err) {
                log.ConsoleLogObject(err);
            });
        });

    });

    describe('bgg.GetBGGThingById', function() {

        it('get boardgame', function(done) {
            bgg.GetBGGThingById(219502).then(function(x){
                expect(x.type).toBe('boardgame');
                expect(x.id).toBe(219502);
                done();
            }, function(err) {
                log.ConsoleLogObject(err);
            });
        });

        it('get boardgame expansion', function(done) {
            bgg.GetBGGThingById(202976).then(function(x){
                expect(x.type).toBe('boardgameexpansion');
                expect(x.id).toBe(202976);
                done();
            }, function(err) {
                log.ConsoleLogObject(err);
            });
        });

    });




    describe('bgg.GetBGGBoardGameById', function() {
        it('retreive an invalid game', function(done) {
            bgg.GetBGGBoardGameById('badtext').then(
                done(),
                function(x) {
                    done();
                }
            );
        });

        it('retreive expansion fail', function(done) {
            bgg.GetBGGBoardGameById(202876).then(
                done(),
                function(x) {
                    done();
                }
            );
        });



        it('retrieve a user', function(done) {
            var username = 'thots';
            try {

            bgg.GetBGGUser(username).then(
                function(x) {
                    expect(x.user.name).toBe(username);
                    done();
                }
            );

            } catch (err) {
                log.ConsoleLogObjectSection('error:', err);
            }
        });

        it('retrieve an invalid user', function(done) {
            bgg.GetBGGUser('invalid-jaied').then(
                function(x) {
                    expect(x.div.class).toBe('messagebox error');
                    done();
                },
                function(err) {
                    done();
                }
            );
        });

        it('retreive a users boardgame collection', function(done) {
            var username = 'thots';
            //var username = 'meng';
            bgg.GetBGGUserCollection(username, 10).then(
                function(x) {
                    expect(x.items.hasOwnProperty("totalitems")).toBeTruthy();
                    expect(x.items.hasOwnProperty("termsofuse")).toBeTruthy();
                    expect(x.items.hasOwnProperty("pubdate")).toBeTruthy();
                    done();
                }
            ).catch(err => {
                log.clos('error', err);
            });
        }, 20000);

        it('retreive an invalid users boardgame collection', function(done) {
            var username = 'abcdoesnotexistlfg';
            bgg.GetBGGUserCollection(username).then(x => {
                expect(x.errors.error.message).toBe('Invalid username specified');
                done();
            })
        }, 20000);


        it('retreive a collection after failed retreieve', function(done) {
            var username = 'thots';
            bgg.CheckForValidCollection(username, {message:'failed attempt'}).then(
                function(x) {
                    expect(x.items.hasOwnProperty("totalitems")).toBeTruthy();
                    expect(x.items.hasOwnProperty("termsofuse")).toBeTruthy();
                    expect(x.items.hasOwnProperty("pubdate")).toBeTruthy();
                    done();
                }
            );
        });

        /*
        it('search for users', function(done) {
            bgg.SearchBGGBoardGameByName("Entdecker").then(
                function(x) {
                    expect(x.items.hasOwnProperty("total")).toBeTruthy();
                    expect(x.items.hasOwnProperty("termsofuse")).toBeTruthy();
                    expect(x.items.total).toBe(x.items.item.length);
                    done();
                }
            )
        });
        */

	});
});
