var Promise = require('bluebird');
var R = require('ramda');
var util = require('util');
var log = require('../app/log.js');
var path = require('path');

var passport = require('passport');
var genhelper = require('./genhelper.js');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var lounge_page = require('../app/lounge_page.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var browser2 = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');
var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');
var mongo = require('../app/mongoconnect');
var mongoose = mongo.Mongoose;
mongo.ConnectDatabase(dbUri);

var testapp = express();
var httpPort = 4391;
var login = require('connect-ensure-login');

// String -> (Promise -> String)
var promisifyZombie = zombieHelper.PromisifyZombie();
var zombieGetBody = zombieHelper.PromiseZombieVisit(browser, "body");

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);
lounge_page.RoutePlayerList(testapp, login, account);

var generateUser = genhelper.GenerateUser(account);
var generateHostUser = genhelper.GenerateHostUser(account);

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
    testapp.close(function(){
        console.log('matches_service_spec closed on port ' + httpPort);
    });
};

// ============== RUN TESTS ================== 
describe("lounge_page_spec.js api", function() {

    var server;

    // loginUser :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
    var verifyregister = function(account, zombieb, email, pass, userobj) {
        return new Promise(function(fulfill, reject) {

            accounts.DeleteAccount(account, email).then(function(y) {
                var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

                register(email, pass).then(function(x) {

                    account.update({email: email}, userobj).exec().then(function(z) {
                        zombieb.visit(base_url + "login", function(err) {
                            if (err) 
                                reject(err);

                            zombieb.fill('input[name="form-username"]', email)
                            zombieb.fill('input[name="form-password"]', pass)
                            zombieb.pressButton('login', function() {
                                fulfill(x);
                            });
                        });
                    });
                });
            });

        })
    };

    describe("unit tests", function() {
        it("maybeDistance", function(done) {
            const testObj = {
                user: {
                    profile: {
                        searchradius: 90
                    }
                }
            };
            expect(lounge_page.MaybeDistance(testObj)).toEqual(90000);
            expect(lounge_page.MaybeDistance({})).toEqual(50000);
            done();
        });

        it("CheckVisibility", function(done) {
            expect(lounge_page.CheckVisibility({a:'b'}, 1)).toEqual({a:'b'});
            expect(lounge_page.CheckVisibility({a:'b'}, 2)).toEqual({a:'b', novisibility:true});
            done();
        });

        it("ShowWarning", function(done) {
            expect(lounge_page.ShowWarning({user:{profile: {visibility: 1}}}, {a:'b'})).toEqual({a:'b'});
            expect(lounge_page.ShowWarning({user:{profile: {visibility: 2}}}, {a:'b'})).toEqual({a:'b', novisibility:true});
            done();
        });

        it("MergePage", function(done) {
            expect(lounge_page.MergePage({user: {profile: {localarea: {name: 'abc'}}}})('s')({a:'b'})).toEqual( { heading : { name : 'Nearby Players', i : { class : 'fa fa-s fa-fw' }, h4 : { faicon : 'users', text : 'abc' } }, a : 'b' } )
            expect(lounge_page.MergePage({user: {profile: {localarea: {name: 'text'}}}})('d')({c:'d'})).toEqual( { heading : { name : 'Nearby Players', i : { class : 'fa fa-d fa-fw' }, h4 : { faicon : 'users', text : 'text' } }, c : 'd' } )
            done();
        })

        it("buildFaIcon", function(done) {
            const expectObj = { class : 'fa fa-u fa-fw' } ;
            expect(lounge_page.BuildFaIcon('u')).toEqual(expectObj);
            done();
        });

        it("BuildImage", function(done) {
            const expectObj = { class : 'u', src : 's' } ;
            expect(lounge_page.BuildImage('u', 's')).toEqual(expectObj);
            done();
        });

        it("BuildHeading", function(done) {
            const expectObj = { name : 'u', s : 'v', h4 : { faicon : 'a', text : 'f' } } ;
            expect(lounge_page.BuildHeading('u', 's', 'a', 'f', 'v')).toEqual(expectObj);
            done();
        });

        it("BuildPage", function(done) {
            const expectObj = { url : 'u', handlebars : { heading : { name : 's', i : 'v', h4 : { faicon : 'a', text : 'f' } } } } ;
            expect(lounge_page.BuildPage('u', 's', 'i', 'a', 'f', 'v')).toEqual(expectObj);
            done();
        });

        it("OneIfTrue", function(done) {
            expect(lounge_page.OneIftrue(false)).toBe(0);
            expect(lounge_page.OneIftrue(true)).toBe(1);
            done();
        });

        it('CalculateMatchDistance', function(done) {

            var lat = -34.45822019999999;
            var lng = 150.83694730000002;
            var user = {
                profile: {
                    localarea: {
                        loc: {
                            coordinates: [150.84839120000004, -34.4662757]
                        }
                    }
                }
            };
            
            var testdistance = R.compose(R.prop('distance'), lounge_page.CalculatePlayerDistance);
            expect(testdistance(lat, lng, user)).toBe(1381);
            done();
            
        });

        it("WhenInDistance", function(done) {
            expect(lounge_page.WhenInDistance({profile: {searchradius: 20}, distance:10000})).toBe(true);
            expect(lounge_page.WhenInDistance({profile: {searchradius: 20}, distance:30000})).toBe(false);
            done();
        });

        it("WhenNotPrivate", function(done) {
            expect(lounge_page.WhenNotPrivate({profile: {visibility: 1}})).toBe(true);
            expect(lounge_page.WhenNotPrivate({profile: {visibility: 2}})).toBe(false);
            done();
        });

        it("ReadMotive", function(done) {
            expect(lounge_page.ReadMotive({profile: {abc:true}}, 'abc')).toBe('abc')
            expect(lounge_page.ReadMotive({profile: {abc:false}}, 'abc')).toBe('')
            expect(lounge_page.ReadMotive({}, 'abc')).toBe('')
            done();
        });

        it("CalcMotive", function(done) {
            expect(lounge_page.CalcMotive({abc:'x'}, {profile:{abc: 'x'}}, 'abc')).toBe(12.5);
            expect(lounge_page.CalcMotive({abc:'x'}, {profile:{abd: 'x'}}, 'abd')).toBe(0);
            expect(lounge_page.CalcMotive({abd:'x'}, {profile:{abc: 'x'}}, 'abd')).toBe(0);
            done();
        });


        it("buildPlayerList", function(done) {
            var lat = -34.45822019999999;
            var lng = 150.83694730000002;
            var user1 = {
                email: 'abc@xyz.com',
                profile: {
                    searchradius: 1500,
                    localarea: {
                        loc: {
                            coordinates: [150.84839120000004, -34.4662757]
                        }
                    }
                }
            };
            var user2 = {
                email: 'abc@xyz.com',
                profile: {
                    searchradius: 15,
                    localarea: {
                        loc: {
                            coordinates: [150.84839120000004, -34.4662757]
                        }
                    }
                }
            };
            var user3 = {
                email: 'abc@xyz.com',
                profile: {
                    searchradius: 1500,
                    localarea: {
                        loc: {
                            coordinates: [153.84839120000004, -34.4662757]
                        }
                    }
                }
            };
            var user4 = {
                email: 'abc@xyz.com',
                profile: {
                    visibility: 2,
                    searchradius: 1500,
                    localarea: {
                        loc: {
                            coordinates: [150.84839120000004, -34.4662757]
                        }
                    }
                }
            };

 
            //var expectObj = { playerlist : [ { email : 'abc@xyz.com', profile : { searchradius : 1500, localarea : { loc : { coordinates : [ 150.84839120000004, -34.4662757 ] } } }, compatibility : 37.5, motives : [  ], distance : 1381 }, { email : 'abc@xyz.com', profile : { searchradius : 15, localarea : { loc : { coordinates : [ 150.84839120000004, -34.4662757 ] } } }, compatibility : 37.5, motives : [  ], distance : 1381 }, { email : 'abc@xyz.com', profile : { searchradius : 1500, localarea : { loc : { coordinates : [ 153.84839120000004, -34.4662757 ] } } }, compatibility : 37.5, motives : [  ], distance : 276390 } ] } 
            var expectObj = { playerlist : [ {profile : { searchradius : 1500, localarea : { loc : { coordinates : [ 150.84839120000004, -34.4662757 ] } } }, compatibility : 37.5, motives : [  ], distance : 1381 }, {profile : { searchradius : 15, localarea : { loc : { coordinates : [ 150.84839120000004, -34.4662757 ] } } }, compatibility : 37.5, motives : [  ], distance : 1381 }, {profile : { searchradius : 1500, localarea : { loc : { coordinates : [ 153.84839120000004, -34.4662757 ] } } }, compatibility : 37.5, motives : [  ], distance : 276390 } ] } 

            expect(lounge_page.BuildPlayerList(lng, lat, [user1, user2, user3, user4], { motivecompetition: true, motivecommunity: true, motivestrategy: true, motivecooperation: true, motivesocialmanipulation: true })).toEqual(expectObj);
            done();
        });


		it("faiconWhenTwo", function(done) {
            var obj0 =  {x: 0};
            var obj1 =  {x: 1};
            var obj2 =  {x: 2};
            var expectObj = { fa : { faicon : 'fa', faclass : 'fatooltip', tooltip : 'at' } };
            expect(lounge_page.FaiconWhenTwo('fa', 'at', 0)).toEqual({});
            expect(lounge_page.FaiconWhenTwo('fa', 'at', 1)).toEqual({});
            expect(lounge_page.FaiconWhenTwo('fa', 'at', 2)).toEqual(expectObj);
            done();
        });

        it("buildMotive", function(done) {
            expect(lounge_page.BuildMotive(['motivecompetition', 'b'], 'motivecompetition')).toEqual({ fa : { faicon : 'fa-bullseye', tooltip : 'Conflict. Such as stealing resources, blocking others or directly attacking other players.', id : 'motivecompetition', faclass : 'fatooltip' } });
            expect(lounge_page.BuildMotive(['a', 'motivecompetition'], 'motivecompetition')).toEqual({ fa : { faicon : 'fa-bullseye', tooltip : 'Conflict. Such as stealing resources, blocking others or directly attacking other players.', id : 'motivecompetition', faclass : 'fatooltip' } });
            expect(lounge_page.BuildMotive(['a', 'b'], 'motivecompetition')).toEqual({});
            done();
        });


        it("buildPlayLevel", function(done) {
            expect(lounge_page.BuildPlayLevel(0)).toBe('');
            expect(lounge_page.BuildPlayLevel(1)).toBe('Play Level: 1');
            expect(lounge_page.BuildPlayLevel(99)).toBe('Play Level: 99');
            done();
        });


        it("ShowCompatibility", function(done) {
            expect(lounge_page.ShowCompatibility(0)).toEqual({});
            expect(lounge_page.ShowCompatibility(50)).toEqual({});
            expect(lounge_page.ShowCompatibility(80)).toEqual({ fa : { faicon : 'fa-thumbs-o-up', tooltip : 'Player compatibility 80%', faclass : 'fatooltip' } });
            expect(lounge_page.ShowCompatibility(100)).toEqual({ fa : { faicon : 'fa-thumbs-up', tooltip : 'High player compatibility 100%', faclass : 'fatooltip' } });
            done();
        });


        it("BuildToolTip", function(done) {
            expect(lounge_page.BuildTooltip('n', 'y', false, false)).toBe('n (y) ');
            expect(lounge_page.BuildTooltip('n', 'y', true, false)).toBe('n (y) The player owns this game. ');
            expect(lounge_page.BuildTooltip('n', 'y', false, true)).toBe('n (y) You own this game.');
            expect(lounge_page.BuildTooltip('n', 'y', true, true)).toBe('n (y) The player owns this game. You own this game.');
            done();
        });


        it("BuildClass", function(done) {
            expect(lounge_page.BuildClass(false, false)).toBe("imglistsquare fatooltip wtpimages")
            expect(lounge_page.BuildClass(true, false)).toBe("imglistsquareUserOwn fatooltip wtpimages")
            expect(lounge_page.BuildClass(false, true)).toBe("imglistsquareViewerOwn wow wobble fatooltip wtpimages")
            expect(lounge_page.BuildClass(true, true)).toBe("imglistsquareBothOwn fatooltip wtpimages")
            done();
        });


        it("MakeImg", function(done) {
            expect(lounge_page.MakeImg({name: 'n', yearpublished: 'yp', userown: true, viewerown: true, src: 's', thumbnail:'t'})).toEqual({ img : { src : 't', class : 'imglistsquareBothOwn fatooltip wtpimages', tooltip : 'n (yp) The player owns this game. You own this game.' } });
            expect(lounge_page.MakeImg({name: 'n', yearpublished: 'yp', userown: false, viewerown: false, src: 's', thumbnail:'t'})).toEqual({ img : { src : 't', class : 'imglistsquare fatooltip wtpimages', tooltip : 'n (yp) ' } });

            done();
        });

        it('userPage', function(done) {
            expect(lounge_page.UserPage('c', 'a', 'b')).toEqual({a: {text: 'a', href: 'users?id=b', class: 'c'}});
            done();
        });

        it('buildPageslug', function(done) {
            expect(lounge_page.BuildPageslug({})).toEqual({});
            expect(lounge_page.BuildPageslug({pageslug: 'a'})).toEqual({});
            expect(lounge_page.BuildPageslug({name: 'b'})).toEqual({});
            expect(lounge_page.BuildPageslug({pageslug: 'a', profile: {name:'b'}}))
              .toEqual({a: {text: 'b', href: 'users?id=a', class: ''}});
            expect(lounge_page.BuildPageslug({showstaff: true, pageslug: 'a', profile: {name:'b'}}))
              .toEqual({a: {text: 'b', href: 'users?id=a', class: 'a-name-staff'}});
            expect(lounge_page.BuildPageslug({showpremium: true, pageslug: 'a', profile: {name:'b'}}))
              .toEqual({a: {text: 'b', href: 'users?id=a', class: 'a-name-premium'}});
            done();
        });

        it('compatibility', function(done) {
            expect(lounge_page.Compatibility({})).toEqual({});
            expect(lounge_page.Compatibility({compatibility:70})).toEqual({ fa : { faicon : 'fa-thumbs-o-up', tooltip : 'Player compatibility 70%', faclass : 'fatooltip' } });
            expect(lounge_page.Compatibility({compatibility:90})).toEqual({ fa : { faicon : 'fa-thumbs-up', tooltip : 'High player compatibility 90%', faclass : 'fatooltip' } });
            done();
        });

        it('avataricon', function(done) {
            expect(lounge_page.Avataricon({})).toEqual('/img/default-avatar-sq.jpg');
            expect(lounge_page.Avataricon({profile:{avataricon:'abc'}})).toEqual('abc');
            done();
        });

        it('getLevel', function(done) {
            expect(lounge_page.GetLevel('a', {})).toBe(0);
            expect(lounge_page.GetLevel('a', {profile:{}})).toBe(0);
            expect(lounge_page.GetLevel('a', {profile:{a:3}})).toBe(3);
            done();
        });

        it('hostlevel', function(done) {
            expect(lounge_page.HostLevel({})).toEqual({ fa : { faicon : 'fa-home', tooltip : 'Hosting Level', faclass : 'fatooltip' }, text : 0 });
            expect(lounge_page.HostLevel({profile:{}})).toEqual({ fa : { faicon : 'fa-home', tooltip : 'Hosting Level', faclass : 'fatooltip' }, text : 0 });
            expect(lounge_page.HostLevel({profile:{hostlevel:3}})).toEqual({ fa : { faicon : 'fa-home', tooltip : 'Hosting Level', faclass : 'fatooltip' }, text : 3 });
            done();
        });

        it('playlevel', function(done) {
            expect(lounge_page.PlayLevel({})).toBe('');
            expect(lounge_page.PlayLevel({profile:{}})).toBe('');
            expect(lounge_page.PlayLevel({profile:{playlevel:3}})).toBe('Play Level: 3');
            done();
        });

        it('safeArr', function(done) {
            expect(lounge_page.SafeArr('a', {})).toEqual([]);
            expect(lounge_page.SafeArr('a', {a: ['b', 'c']})).toEqual(['b', 'c']);
            done();
        });

        it('safeTeacher', function(done) {
            expect(lounge_page.SafeTeacher({})).toEqual({});
            expect(lounge_page.SafeTeacher({profile:{}})).toEqual({});
            expect(lounge_page.SafeTeacher({profile:{isteacher:1}})).toEqual({});
            expect(lounge_page.SafeTeacher({profile:{isteacher:2}})).toEqual({ fa : { faicon : 'fa-graduation-cap', faclass : 'fatooltip', tooltip : 'Likes Teaching Games to New Players' } });
            done();
        });

        it('SafePropOrFalse', function(done) {
            expect(lounge_page.SafePropOrFalse('b', {b:true})).toBe(true);
            expect(lounge_page.SafePropOrFalse('a', {b:true})).toBe(false);
            expect(lounge_page.SafePropOrFalse('a', {})).toBe(false);
            done();
        });


        it("SortGames", function(done) {
            expect(lounge_page.SortGames({ id:100 })).toBe(100) 
            expect(lounge_page.SortGames({ id:100, userown:true })).toBe(201) 
            expect(lounge_page.SortGames({ id:100, viewerown:true })).toBe(201) 
            expect(lounge_page.SortGames({ id:100, userown:true, viewerown:true })).toBe(302) 
            done();
        });

    });


    describe("web page tests", function() {

        it("setup server", function(done) {

			testapp.get('/', function (req, res) {
				res.send('<html>Home Page</html>')
			});

            login_page.RouteLogins(testapp, passport);
            login_page.PostLogin(testapp, passport);
            login_page.RouteVerify(testapp, account);
            server = testapp.listen(httpPort, function() {
                done();
            });

        });

		it("non logged in user visit player", function(done) {
            const page = 'lounge';
            browser.visit(base_url + page, function(err) {
                expect(browser.success).toBe(true);
                expect(browser.html()).toContain('Roll for Group');
                done();
            });
		});

		it("invalid user visit /lounge", function(done) {

            var userEmail = faker.Internet.email();
            var userPass = 'testpassword';

            var userobj = {};

            verifyregister(account, browser, userEmail, userPass, userobj).then(function(x) {
                const page = 'lounge';
                browser.visit(base_url + page, function(err) {
                    expect(browser.success).toBe(true);
                    expect(browser.html('#listhead')).toContain('Location Not Set');
                    //expect(browser.html('#warninglocation')).toContain('Your Profile Needs a Location.');
                    done();
                });

            });
        });

		it("valid user visit /lougne", function(done) {

            var userEmail = faker.Internet.email();
            var userPass = 'testpassword';

            var userobj = {profile: {
                searchradius: 10,
                localarea: {
                    name: 'area',
                    loc: {
                        coordinates: [20, 20],
                        type: 'Point'
                    }
                }
            }};

            verifyregister(account, browser, userEmail, userPass, userobj).then(function(x) {
                const page = 'lounge';
                browser.visit(base_url + page, function(err) {
                    expect(browser.success).toBe(true);
                    done();
                });

            });
        });

        it('server close', function(done) {
            server.close(function() {
                done();
            });
        });

        it('db close', function(done) {
            mongo.CloseDatabase();
            done();
        });
    });

});



