var express = require("express");
var router = express.Router();
var log = require('../app/log.js');

var R = require('ramda');
var assoc   = R.assoc,
    append  = R.append,
    converge = R.converge,
    compose = R.compose,
    composeP = R.composeP,
    contains = R.contains,
    curry   = R.curry,
    equals  = R.equals,
    filter  = R.filter,
    flatten = R.flatten,
    has     = R.has,
    head    = R.head,
    join    = R.join,
    length  = R.length,
    map     = R.map,
    merge   = R.merge,
    not     = R.not,
    nth     = R.nth,
    or      = R.or,
    remove  = R.remove,
    path    = R.path,
    pipe    = R.pipe,
    prop    = R.prop,
    when    = R.when
    whereEq = R.whereEq,
    zipObj  = R.zipObj;

var Promise = require('bluebird');
var faker = require('Faker');

var api_feedback = require('../app/feedback_service.js');
var request = require('ajax-request');
var bodyParser = require('body-parser')

var genhelper = require('./genhelper.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');

var mongo = require('../app/mongoconnect.js');
var mongoose = mongo.Mongoose;

mongo.ConnectDatabase(dbUri);

var httpPort = 3239;

var testapp = express();
var account = require('../app/models/account.js');

var generateUser = genhelper.GenerateUser(account);
var ajax = require('./ajax.js');

var ajaxPromise = ajax.AjaxPromise;


// ============== VARS ===============
var url_base = "http://localhost:" + httpPort;
var server;


// ============== END SERVER FOR TESTS =======
var closeServer = function() {
    testapp.close(function(){
        console.log('matches_service_spec closed on port ' + httpPort);
    });
};


// ============== RUN TESTS ================== 
describe("feedback_service_spec.js dbcalls", function() {
    describe("database calls", function() {
        it("createFeedback", function(done) {

            generateUser({}).then(function(u) {
                api_feedback.CreateFeedback(u.email, u.key, 'likes', 'dislikes', 'suggestion').then(function(a) {
                    expect(prop(a, 'useremail')).toBe(u.useremail);
                    expect(prop(a, 'likes')).toBe(u.likes);
                    expect(prop(a, 'dislikes')).toBe(u.dislikes);
                    expect(prop(a, 'suggestion')).toBe(u.suggestion);

                    api_feedback.CreateFeedback(u.email, 'userkey', 'anotherlike', 'anotherdisklike', 'another suggestion').then().catch(function(errb) {
                        expect(errb).toBe(401);

                        api_feedback.CreateFeedback('invaliduser', 'invalidkey', 'anotherlike', 'anotherdisklike', 'another suggestion').then().catch(function(errc) {
                            expect(errc).toBe(401);
                            done();

                        });
                    });
                });
            });

        });
    });

    describe("api calls", function() {

        // url to test
        var url_api = '/api/v1/feedbacks';

        it("setup server", function(done) {
            var router = api_feedback.SetupRouter();

            testapp.use(url_api, router);
            server = testapp.listen(httpPort, function() {
                done();

            });
        });

        it("request data", function(done) {

            var postify = (ajaxdata) => zipObj(['url', 'method', 'data'], [url_base + url_api, 'POST', ajaxdata]);
            var feedbackdata = zipObj(['email', 'userkey', 'likes', 'dislikes', 'suggestion']);
            var ajaxdata = compose(postify, feedbackdata);
            var statusCode = prop('statusCode');

            generateUser({}).then(function(u) {
                ajaxPromise(ajaxdata([u.email, u.key, 'likes', 'dislikes', 'suggestion'])).then(function(aj) {
                    expect(statusCode(aj)).toBe(200);
                    expect(path(['body', 'useremail'], aj)).toBe(u.email);
                    expect(path(['body', 'likes'], aj)).toBe('likes');
                    expect(path(['body', 'dislikes'], aj)).toBe('dislikes');
                    expect(path(['body', 'suggestion'], aj)).toBe('suggestion');

                    ajaxPromise(ajaxdata([u.email, 'invalidkey', 'likes', 'dislikes', 'suggestion'])).then(function(aj) {
                        expect(statusCode(aj)).toBe(401);
                        ajaxPromise(ajaxdata(['invaliduser', 'invalidkey', 'likes', 'dislikes', 'suggestion'])).then(function(aj) {
                            expect(statusCode(aj)).toBe(401);
                            done();

                        });
                    });
                });
            });
        });

        it("close server", function(done) {
            server.close(function() {
                mongo.CloseDatabase();
                done();
            });
        });
    });
});
