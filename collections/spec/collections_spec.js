var Promise = require('bluebird');
var R = require('ramda');
var S = require('../app/lambda.js');

var boardgame = require('../app/boardgame.js');

var account = require('../app/account.js');

var collection = require('../app/collection.js');
var collections = require('../app/collections.js');
var shortid = require('shortid');
var log = require('../app/log.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var genhelper = require('./genhelper.js');

const Task      = require('data.task');

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

var mongo = require('../app/mongoconnect');
var mongoose = require('mongoose');
var dbUri = nconf.get('database:uri');

const Maybe     = require('data.maybe');

var Browser = require("zombie");
var browser = new Browser();
var zombieHelper = require('./zombiehelper.js');

var ajax = require('./ajax.js');
var ajaxPromise = ajax.AjaxPromise;

var httpPort = 40001;
var url_base = "http://localhost:" + httpPort;
var url_api = '/api/v1/collections';

var express = require("express");
var router = express.Router();
var testapp = express();

var server;

mongoose.Promise = global.Promise;
mongo.ConnectDatabase(mongoose, dbUri);

var testCollectionKey = 'collectionWithKnownUser';

var testAnonCollectionKey = 'anonCollectionKey';
var testAnonCollectionAdminKey = '';

var testUserEmail = '';
var testUserKey = '';
var testUserAccessKey = '';
var testUserCollectionKey = '';


describe('collections_spec.js', function() {

  it('objOfKey', function(done) {
    expect(collections.objOfKey('a')).toEqual({key:'a'});
    expect(collections.objOfKey('za')).toEqual({key:'za'});
    done();
  });

  it('findById', function(done) {
    expect(collections.findById([], 10).isJust).toBeFalsy();
    expect(collections.findById([{id:10}], 12).isJust).toBeFalsy();
    expect(collections.findById([{id:12}], 12).isJust).toBeTruthy();
    expect(collections.findById([{id:12}], 12).get()).toEqual({id:12});
    done();
  });

  it('validateText', function(done) {
    expect(collections.validateText('').isJust).toBeFalsy();
    expect(collections.validateText('hello').isJust).toBeTruthy();
    expect(collections.validateText('hello').get()).toBe('hello');
    done();
  });

  it('mergeObjKeyEmail', function(done) {
    expect(collections.mergeObjKeyEmail('a', 'email', 'b')).toEqual({key:'a', email:'b'});
    expect(collections.mergeObjKeyEmail('z', 'xyz', 'a')).toEqual({key:'z', xyz:'a'});
    done();
  });

  it('maybeQuery', function(done) {
    expect(collections.maybeQuery('', {}).isJust).toBeFalsy;
    expect(collections.maybeQuery('', {query:{}}).isJust).toBeFalsy;
    expect(collections.maybeQuery('b', {query:{a: 'b'}}).isJust).toBeFalsy;
    expect(collections.maybeQuery('a', {query:{a: 'b'}}).get()).toBe('b');
    done();
  });

  it('maybeUserId', function(done) {
    expect(collections.maybeUserId({}).isJust).toBeFalsy;
    expect(collections.maybeUserId({body:{}}).isJust).toBeFalsy;
    expect(collections.maybeUserId({body:{userkey:'a'}}).get()).toBe('a');
    done();
  });

  it('getParamColl', function(done) {
    expect(collections.getParamColl({}).isJust).toBeFalsy;
    expect(collections.getParamColl({params:{}}).isJust).toBeFalsy;
    expect(collections.getParamColl({params:{collectionid:'a'}}).get()).toBe('a');
    done();
  });

  it('maybeAdminKey', function(done) {
    expect(collections.maybeAdminKey({}).isJust).toBeFalsy;
    expect(collections.maybeAdminKey({body:{}}).isJust).toBeFalsy;
    expect(collections.maybeAdminKey({body:{adminkey:'a'}}).get()).toBe('a');
    done();
  });

  it("mergeGameValue", function(done) {
    expect(collections.mergeGameValue([{id:1, a:2}], 'a')({id:1})).toEqual({id:1, a:2});  
    expect(collections.mergeGameValue([{id:2, a:2}], 'a')({id:1, b:3})).toEqual({id: 1, b:3});  
    expect(collections.mergeGameValue([{id:1, b:2}], 'a')({id:1})).toEqual({id: 1});  
    done();
  });

  it("updateGame", function(done) {
    expect(collections.updateExtendedGameData([{id:1, a:2}], {id:1, a:2})).toEqual({id:1, a:2});  
    expect(collections.updateExtendedGameData([{id:2, a:2}], {id:1, b:3})).toEqual({id: 1, b:3});  
    expect(collections.updateExtendedGameData([{id:1, b:2}], {id:1})).toEqual({id: 1});  
    expect(collections.updateExtendedGameData([{id:1, maxplayers:3, minplayers: 1}], {id:1, a:2})).toEqual({id:1, a:2, maxplayers:3, minplayers: 1});  
    expect(collections.updateExtendedGameData([{id:1, maxplayers:3, minplayers: 1, maxplaytime: 20, minplaytime: 20}], {id:1, a:2})).toEqual({id:1, a:2, maxplayers:3, minplayers: 1, maxplaytime: 20, minplaytime: 20});  
    done();
  });

  it('splitQuote', function(done) {
    expect(collections.splitQuote('').isJust).toBeFalsy();
    expect(collections.splitQuote('hello there people').isJust).toBeFalsy();
    expect(collections.splitQuote('hello "there" people').get()).toBe('there');
    done();
  });

  it('regexGameName', function(done) {
    expect(collections.regexGameName({}).isJust).toBeFalsy();
    expect(collections.regexGameName({codeName: 'DuplicateKey'}).isJust).toBeFalsy();
    expect(collections.regexGameName({codeName: 'DuplicateKey', errmsg:''}).isJust).toBeFalsy();
    expect(collections.regexGameName({codeName: 'DuplicateKey', errmsg:'hells "kitchen"'}).get()).toBe('kitchen');
    expect(collections.regexGameName({errmsg:'hells "kitchen"'}).isJust).toBeFalsy();
    done();
  });


  it('taskCreateAnonymousCollection - taskCreateCollection no bgg', function(done) {

    collections.taskCreateCollection(
      collection, 
      '',
      'anon name x',
      'anon description',
      'anon thumbnail',
      '',
      'anon type',
      1 
    ).fork(
      err => {},
      data => {
        testAnonCollectionKey = data.key; 
        testAnonCollectionAdminKey = data.adminkey
        expect(data.name).toBe('anon name x');
        expect(data.description).toBe('anon description');
        expect(data.thumbnail).toBe('anon thumbnail');
        expect(data.bggname).toBe('');
        expect(data.bggtype).toBe('anon type');

        collections
          .taskReadCollection(collection, testAnonCollectionKey)
          .fork(
            err => {},
            dataread => {
              expect(dataread.userkey).toBe('');
              expect(dataread.adminkey).toBe(testAnonCollectionAdminKey);
              expect(dataread.description).toBe('anon description');
              expect(dataread.thumbnail).toBe('anon thumbnail');
              expect(dataread.isbgg).toBeFalsy();
              expect(dataread.bggname).toBe('');
              expect(dataread.bggtype).toBe('anon type');
              expect(dataread.viewcount).toBe(0);

              collections
                .taskReadCollectionIncrement(testAnonCollectionKey)
                .fork(
                  err => {},
                  dataread => {
                    expect(dataread.userkey).toBe('');
                    expect(dataread.adminkey).toBe(testAnonCollectionAdminKey);
                    expect(dataread.description).toBe('anon description');
                    expect(dataread.thumbnail).toBe('anon thumbnail');
                    expect(dataread.isbgg).toBeFalsy();
                    expect(dataread.bggname).toBe('');
                    expect(dataread.bggtype).toBe('anon type');
                    expect(dataread.viewcount).toBe(1);
                    done();

                  }
                )
            }
          )
      }
    )

  });


  it('taskListCollectionsByUser', function(done) {
    genhelper.GenerateUser(account, {}).then(
      user => {
        var findUserKey = user.pageslug;
        var findUserAccessKey = user.key;

        collections.taskCreateCollection(
          collection, 
          findUserKey,
          'name x a',
          'description',
          'thumbnail',
          '',
          'type',
          1 
        ).fork(
          err => {},
          data => {
            testCollectionKey = data.key; 
            expect(data.userkey).toBe(findUserKey);
            expect(data.name).toBe('name x a');
            expect(data.description).toBe('description');
            expect(data.thumbnail).toBe('thumbnail');
            expect(data.bggname).toBe('');
            expect(data.bggtype).toBe('type');

            collections
              .taskListCollectionsByUser(collection, findUserKey)
              .fork(
                err => {},
                dataread => {
                  expect(dataread[0].userkey).toBe(findUserKey);
                  expect(dataread[0].description).toBe('description');
                  expect(dataread[0].thumbnail).toBe('thumbnail');
                  expect(dataread[0].isbgg).toBeFalsy();
                  expect(dataread[0].bggname).toBe('');
                  expect(dataread[0].bggtype).toBe('type');
                  done();

                }
              )
          }
        )
      }
    );
  });


  it('taskCreateCollection - taskCreateCollection no bgg', function(done) {
    genhelper.GenerateUser(account, {}).then(
      user => {
        testUserEmail = user.email;
        testUserKey = user.pageslug;
        testUserAccessKey = user.key;

        collections.taskCreateCollection(
          collection, 
          testUserKey,
          'name x b',
          'description',
          'thumbnail',
          '',
          'type',
          1 
        ).fork(
          err => {},
          data => {
            testCollectionKey = data.key; 
            expect(data.userkey).toBe(testUserKey);
            expect(data.name).toBe('name x b');
            expect(data.description).toBe('description');
            expect(data.thumbnail).toBe('thumbnail');
            expect(data.bggname).toBe('');
            expect(data.bggtype).toBe('type');

            collections
              .taskReadCollection(collection, testCollectionKey)
              .fork(
                err => {},
                dataread => {
                  expect(dataread.userkey).toBe(testUserKey);
                  expect(dataread.description).toBe('description');
                  expect(dataread.thumbnail).toBe('thumbnail');
                  expect(dataread.isbgg).toBeFalsy();
                  expect(dataread.bggname).toBe('');
                  expect(dataread.bggtype).toBe('type');
                  done();

                }
              )
          }
        )
      }
    );
  });


  it('taskCreateCollection - with bgg', function(done) {

    // test
    collections.taskCreateCollection(
      collection, 
      testUserKey,
      'name',
      'description',
      'thumbnail',
      'jaied',
      '0',
      1
    ).fork(
      err => {},
      data => {
        expect(data.isbgg).toBe(true);
        expect(data.bgghassearched).toBe(false);
        expect(data.bggname).toBe('jaied');
        expect(data.bggtype).toBe('0');
        expect(data.viewcount).toBe(0);
        expect(data.bgglistnumber).toBe(1);

        collections.updateBggCollection(boardgame, collection, data.key)
          .fork(
          err => {}, 
          update => {
            expect(update.thumbnail).toBe('thumbnail');
            expect(update.isbgg).toBe(true);
            expect(update.bgghassearched).toBe(true);
            expect(update.bggname).toBe('jaied');
            expect(update.bggtype).toBe('0');
            expect(update.viewcount).toBe(0);
            expect(update.bgglistnumber).toBe(1);
            done();
          }
        )
      }
    )

  }, 10000);

  it('taskCreateCollection - taskCreateCollection w/bggname', function(done) {

    collections.taskCreateCollection(
      collection, 
      testUserKey,
      'name',
      'description',
      'thumbnail',
      'bggname',
      'type',
      1
    ).fork(
      err => {},
      data => {
        expect(data.userkey).toBe(testUserKey);
        expect(data.name).toBe('name');
        expect(data.description).toBe('description');
        expect(data.thumbnail).toBe('thumbnail');
        expect(data.bggname).toBe('bggname');
        expect(data.bggtype).toBe('type');

        collections
          .taskReadCollection(collection, data.key)
          .fork(
            err => {},
            dataread => {
              expect(dataread.userkey).toBe(testUserKey);
              expect(dataread.description).toBe('description');
              expect(dataread.thumbnail).toBe('thumbnail');
              expect(dataread.isbgg).toBeTruthy();
              expect(dataread.bggname).toBe('bggname');
              expect(dataread.bggtype).toBe('type');
              done();

            }
          )

      }
    )
  });

  it('taskUpdateCollection', function(done) {
    collections
      .taskUpdateCollection(collection, testCollectionKey, {
        userkey: 'abc',
        name: 'np',
        description:'des',
        thumbnail: 'tn',
        type: 'tp'
      })
      .fork(
        err => {},
        dataupdate => {
          expect(dataupdate.userkey).toBe(testUserKey);
          expect(dataupdate.name).toBe('np');
          expect(dataupdate.description).toBe('des');
          expect(dataupdate.thumbnail).toBe('tn');
          expect(dataupdate.bggtype).toBe('type');
          done();

        }
      )
  });

  it('taskDeleteCollection', function(done) {
    collections
      // .taskDeleteCollection(collection, testCollectionKey, 'userkey', testUserKey)
      .taskDeleteCollection(collection, testCollectionKey)
      .fork(
        err => {log.clos('a', err)},
        datadelete => {
          expect(datadelete.n).toBe(1);
          expect(datadelete.ok).toBe(1);
          collections
            .taskReadCollection(collection, testCollectionKey)
            .fork(
              err => {log.clos('b', err)},
              datareaddelete => {
                expect(datareaddelete).toBe(null);
                done();

              }
            )

        }
      );
  });


  it("taskCollectionGame", function(done) {

    var userkey = 'key';

    collections.taskCreateCollection(
      collection, 
      userkey,
      'name',
      'description',
      'thumbnail',
      'bggname',
      'type',
      1
    ).fork(
      err => {},
      data => {
        genhelper
          .GenerateFakeBoardgame(boardgame)
          .then(function(game) {
            collections.taskCollectionAddGame(collection, boardgame, data.key, game.id).fork(
              err => log.clos('taskCollectionAddGame.err', err),
              data => {
                expect(data.games.length).toBe(1);
                expect(data.games[0].id).toBe(game.id);
                expect(data.games[0].subtype).toBe('boardgame');

                collections.taskCollectionRemoveGame(collection, data.key, game.id).fork(
                  err => log.clos('taskCollectionRemoveGame.err', err),
                  deletedata => {
                    expect(deletedata.games.length).toBe(0);
                    done();
                  }
                )

              }
            )
        });
      }
    )
  });


  it("propGames", function(done) {
    expect(collections.propGames({}).isJust).toBeFalsy();
    expect(collections.propGames({games:[]}).isJust).toBeTruthy();
    expect(collections.propGames({games:[{a:'b', __v:'x', _id:'a'}]}).get()).toEqual([{a:'b'}]);
    done();
  });

  it("removeBggItem", function(done) {
    expect(collections.removeBggItem([], {})).toEqual({});
    expect(collections.removeBggItem([], {id:1})).toEqual({});
    expect(collections.removeBggItem([{id:1}], {})).toEqual({});
    expect(collections.removeBggItem([{id:1}], {id:1})).toEqual({id:1});
    done();
  });

  it("removeBggItems", function(done) {
    expect(collections.removeBggItems([{id:1}], []))
      .toEqual([]);

    expect(collections.removeBggItems([{id:1}], [{id:1}]))
      .toEqual([{id:1}]);

    expect(collections.removeBggItems([{id:1}, {id:2}], [{id:1}]))
      .toEqual([{id:1}]);

    expect(collections.removeBggItems([{id:1}, {id:2}, {id:3}], [{id:1}, {id:3}]))
      .toEqual([{id:1}, {id:3}]);

    done();

  });

  it("prepareGameName", function(done) {
    expect(collections.prepareGameName({
      items: {
        item: [{
          objectid: 123,
          name: {'$t':'abc'}, 
          status: {
            own: 1,
            prevowned: 0,
            fortrade: 0,
            want: 0,
            wanttoplay: 0,
            wanttobuy: 0,
            wishlist: 0,
            preordered: 0
          },
          stats: {
            minplayers: 3,
            maxplayers: 4,
            minplaytime: 10,
            maxplaytime: 20
          },
          numplays: 10
        }]
      }
    })).toEqual( [ { 
      id : 123, 
      name : 'abc', 
      minplayers: 3,
      maxplayers: 4,
      minplaytime: 10,
      maxplaytime: 20,
      numplays: 10,
      bggcache : { 
        objectid : 123, 
        name : { 
          t : 'abc' 
        }, 
        status : { 
          own : 1, 
          prevowned : 0, 
          fortrade : 0, 
          want : 0, 
          wanttoplay : 0, 
          wanttobuy : 0, 
          wishlist : 0, 
          preordered : 0 
        }, 
        stats: {
          minplayers: 3,
          maxplayers: 4,
          minplaytime: 10,
          maxplaytime: 20
        },
        numplays: 10
      } 
    } ] );
    done();
  });


  it("updateBggItem", function(done) {
    expect(collections.updateBggItem([], {})).toEqual({});
    expect(collections.updateBggItem([], {id:305})).toEqual({id:305});
    expect(collections.updateBggItem([{id:1}], {})).toEqual({});
    expect(collections.updateBggItem([{id:1, a:'bc'}], {id:1})).toEqual({id:1, a:'bc'});
    done();
  });

  it("updateBggItems", function(done) {
    expect(collections.updateBggItems([], []))
      .toEqual([]);

    expect(collections.updateBggItems([], [{id:305}]))
      .toEqual([{id:305}]);

    expect(collections.updateBggItems([{id:1}], []))
      .toEqual([]);

    expect(collections.updateBggItems([{id:1, a:'bc'}], [{id:1}]))
      .toEqual([{id:1, a:'bc'}]);

    done();

  });

  it("addBggItem", function(done) {

    expect(collections.addBggItem([], {})).toEqual({});

    expect(collections.addBggItem([], {id:305}).id).toEqual(305);
    expect(collections.addBggItem([], {id:305}).bggsync).toBeTruthy();

    expect(collections.addBggItem([{id:1}], {})).toEqual({});

    expect(collections.addBggItem([{id:1, a:'bc'}], {id:1})).toEqual({});

    done();

  });

  it("addBggItems", function(done) {

    expect(collections.addBggItems([{id:1}, {id:2}], [{id:1}, {id:2, abc:true}])).toEqual([]);

    var result = collections.addBggItems([{id:1}, {id:2}, {id:3, xyz: 'abc'}], [{id:1}, {id:2, abc:true}]);

    expect(result.length).toEqual(1);
    expect(result[0].id).toEqual(3);
    expect(result[0].xyz).toEqual('abc');
    expect(result[0].bggsync).toEqual(true);

    done();
  });

  it("filterListtype", function(done) {
    expect(collections.filterListtype('own', 1, [
      {id:1, bggcache:{status:{own:1}}},
      {id:2, bggcache:{status:{wishlist:3}}}
    ]).get()).toEqual([
      {id:1, bggcache:{status:{own:1}}}
    ])

    expect(collections.filterListtype('wishlist', 3, [
      {id:1, bggcache:{status:{own:1}}},
      {id:2, bggcache:{status:{wishlist:3}}}
    ]).get()).toEqual([
      {id:2, bggcache:{status:{wishlist:3}}}
    ])
    done();

  });

  it("bggFilterList", function(done) {
    expect(collections.bggFilterList(
      {'bggtype': 'own', 'bgglistnumber': 1}, 
      [
        {id:1, bggcache:{status:{own:1}}},
        {id:2, bggcache:{status:{wishlist:3}}}
      ]
    )).toEqual([
      {id:1, bggcache:{status:{own:1}}}
    ])

    expect(collections.bggFilterList(
      {'bggtype': 'wishlist', 'bgglistnumber': 3}, 
      [
        {id:1, bggcache:{status:{own:1}}},
        {id:2, bggcache:{status:{wishlist:3}}}
      ]
    )).toEqual([
      {id:2, bggcache:{status:{wishlist:3}}}
    ])
    done();

  });

  it("mergeIfJust", function(done) {
    expect(collections.mergeIfJust({x:'a'}, Maybe.of({y:'b'}))).toEqual({x:'a', y:'b'});
    expect(collections.mergeIfJust({x:'a'}, Maybe.of({x:'b'}))).toEqual({x:'b'});
    done();
  });

  it("headOrObj", function(done) {
    expect(collections.headOrObj(['a'])).toBe('a');
    expect(collections.headOrObj('b')).toBe('b');
    done();
  });

  it("mergeUserRank", function(done) {
    expect(collections.mergeUserRank({}).isJust).toBeFalsy();
    expect(collections.mergeUserRank({
      stats:{
        rating:{
          value: 20
        }
      }
    }).get()).toEqual({
      stats:{
        rating:{
          value: 20
        }
      },
      userrank: 20
    });

    done();
  });


  it("mergeNumPlay", function(done) {

    // dataPlays
    dataPlays = [
      {objectid: 1, numplays: 20},
      {objectid: 3}, 
      {objectid: 20},
      {objectid: 99, numplays: 5}
    ];

    // dataGames
    dataGames1 = {id: 1};
    dataGames2 = {id: 3};
    dataGames3 = {id: 99, numplays: 1};

    expect(collections.mergeNumPlay(dataPlays, dataGames1)).toEqual({id: 1, numplays:20});
    expect(collections.mergeNumPlay(dataPlays, dataGames2)).toEqual({id: 3});
    expect(collections.mergeNumPlay(dataPlays, dataGames3)).toEqual({id: 99, numplays:5});
    done();

  });


  it("mergeNumPlays", function(done) {

    // dataPlays
    dataPlays = {
      items:{
        item: [
          {objectid: 1, numplays: 20},
          {objectid: 3}, 
          {objectid: 20},
          {objectid: 99, numplays: 5}
        ]
      }
    };

    // dataGames
    dataGames = [
      {id: 1},
      {id: 3}, 
      {id: 20},
      {id: 99}
    ];

    expect(collections.mergeNumPlays(dataPlays, dataGames)).toEqual([
      {id: 1, numplays:20},
      {id: 3}, 
      {id: 20},
      {id: 99, numplays: 5}
    ]);

    done();

  });


  it("updateBggCollection", function(done) {

    // isPlayed
    const isPlayed = (arrayGames) => {
     return S.find(R.propEq('id', Number(180263)), arrayGames)
       .chain(S.prop('id'))
       .getOrElse(0)

    };

    collections.taskCreateCollection(
      collection, 
      'key',
      'name',
      'description',
      'thumbnail',
      'jaied',
      'wanttobuy',
      1
    ).fork(
      err => {},
      data => {
        testCollectionKey = data.key; 
        collections
          .updateBggCollection(boardgame, collection, testCollectionKey)
          .fork(
            err => {
              log.clos('err', err);
              //done();
            },
            dataread => {
              expect(dataread.userkey).toBe('key');
              expect(dataread.name).toBe('name');
              expect(dataread.description).toBe('description');
              expect(dataread.thumbnail).toBe('thumbnail');
              expect(dataread.bggname).toBe('jaied');
              expect(isPlayed(dataread.games)).toBe(180263);
              done();
            }
          )

      }
    )
  });


  it("createBggUserCollections", function(done) {

    var fakeId = shortid.generate();

    collections
      .createBggUserCollections(collection, fakeId, '')
      .fork(
        err => {
          log.clos('err', err);
          //done();
        },
        dataCollections => {
          expect(dataCollections.length).toBe(3);
          collections
            .createBggUserCollections(collection, fakeId, 'bgguser')
            .fork(
              err => {
                log.clos('err', err);
              },
              dataCollections => {
                expect(dataCollections[0].userkey).toBe(fakeId);
                expect(dataCollections[0].isbgg).toBeTruthy();
                expect(dataCollections[0].bggname).toBe('bgguser');
                expect(dataCollections[0].name).toBe('Games Owned');
                expect(dataCollections[0].description).toBe('These are the games I own');
                expect(dataCollections[0].bggtype).toBe('own');
                expect(dataCollections[0].bgglistnumber).toBe(1);
                expect(dataCollections[0].bgghassearched).toBeFalsy();

                expect(dataCollections[1].userkey).toBe(fakeId);
                expect(dataCollections[1].isbgg).toBeTruthy();
                expect(dataCollections[1].bggname).toBe('bgguser');
                expect(dataCollections[1].name).toBe('Want to Play');
                expect(dataCollections[1].description).toBe('The games I want to play');
                expect(dataCollections[1].bggtype).toBe('wanttoplay');
                expect(dataCollections[1].bgglistnumber).toBe(1);
                expect(dataCollections[1].bgghassearched).toBeFalsy();

                expect(dataCollections[2].userkey).toBe(fakeId);
                expect(dataCollections[2].isbgg).toBeTruthy();
                expect(dataCollections[2].bggname).toBe('bgguser');
                expect(dataCollections[2].name).toBe('Want to Buy');
                expect(dataCollections[2].description).toBe('The games I want to buy');
                expect(dataCollections[2].bggtype).toBe('wanttobuy');
                expect(dataCollections[2].bgglistnumber).toBe(1);
                expect(dataCollections[2].bgghassearched).toBeFalsy();

                done();

              }
            );
        }
      );
  });


  it("verifyTask", function(done) {

    // verifyTestWithUser :: s -> s -> s -> {o}
    var verifyTestWithUser = (listid, userid, userkey) => ({
      params: {
        collectionid: listid
      },
      body: {
        userkey: userid,
        key: userkey 
      }
    });

    // verifyTestWithAdminKey :: s -> s -> {o}
    var verifyTestWithAdminKey = (listid, adminkey) => ({
      params: {
        collectionid: listid
      },
      body: {
        adminkey: adminkey,
      }
    });

    genhelper.GenerateUser(account, {}).then(
      user => {
        collections.taskCreateCollection(
          collection, 
          user.pageslug,
          'name x c',
          'description',
          'thumbnail',
          '',
          'type',
          1 
        ).fork(
          err => {},
          list => {
            collections.verifyTask(collection, {}).getOrElse(taskFail()).fork(
              err => {
                expect(err).toBe('bad params');
                collections.verifyTask(collection, verifyTestWithUser(list.key, user.pageslug, user.key)).getOrElse(taskFail()).fork(
                  err => {},
                  data1 => {
                    expect(data1).toBeTruthy();

                    collections.verifyTask(collection, verifyTestWithAdminKey(list.key, list.adminkey)).getOrElse(taskFail()).fork(
                      err => {},
                      data2 => {
                        expect(data2).toBeTruthy();
                        done();

                      }
                    );
                  }
                );
              }
            );
          }
        )
      }
    )
  });


  it('verifyUserWithSlug', function(done) {
    genhelper.GenerateUser(account, {}).then(
      user => {
        R.sequence(Task.of, [
          collections.verifyUserWithSlug(account, user.key, user.pageslug),
          collections.verifyUserWithSlug(account, user.key, ''),
          collections.verifyUserWithSlug(account, '', user.pageslug),
        ]).fork(
          err => {},
          verify => {
            expect(verify[0]).toBeTruthy();
            expect(verify[1]).toBeFalsy();
            expect(verify[2]).toBeFalsy();
            done();
          }
        )
      }
    )
  });


  it("verifyAdminKey", function (done) {
    collections.taskCreateCollection(
      collection, 
      'key',
      'name',
      'description',
      'thumbnail',
      '',
      'wanttobuy',
      1
    ).fork(
      err => {},
      collect => {

        var colId = collect.key;
        var colAdminKey = collect.adminkey;

        R.sequence(Task.of, [
          collections.verifyAdminKey(collection, colId, colAdminKey),
          collections.verifyAdminKey(collection, colId, 'badkey'),
          collections.verifyAdminKey(collection, 'unknownid', colAdminKey)
        ]).fork(
          err => {},
          data => {
            expect(data[0]).toBeTruthy();
            expect(data[1]).toBeFalsy();
            expect(data[2]).toBeFalsy();
            done();
          }
        );

      }
    );
  })

  it("verifyUserAccount", function (done) {
    genhelper.GenerateUser(account, {}).then(
      user => {
        var testUserSlug = user.pageslug;
        var testUserKey = user.key;

        collections.taskCreateCollection(
          collection, 
          testUserSlug,
          'name x d',
          'description',
          'thumbnail',
          '',
          'type',
          1 
        ).fork(
          err => {},
          data => {
            var gameId = data.key;

            R.sequence(Task.of, [
              collections.verifyUserAccount(collection, gameId, testUserSlug, testUserKey),
              collections.verifyUserAccount(collection, 'thisisbadid', testUserSlug, testUserKey),
              collections.verifyUserAccount(collection, gameId, 'nottheuser', testUserKey),
              collections.verifyUserAccount(collection, gameId, testUserSlug, 'badkey'),
            ]).fork(
              err => {},
              data => {
                expect(data[0]).toBeTruthy();
                expect(data[1]).toBeFalsy();
                expect(data[2]).toBeFalsy();
                expect(data[3]).toBeFalsy();
                done();
              }
            );

            done()
          }
        )
      }
    );
  })

});

describe('collections api', function() {

  it("setup server", function(done) {
    var router = collections.setupRouter(boardgame, collection);
    testapp.use(url_api, router);
    server = testapp.listen(httpPort, function() {
      done();
    });
  });

  it('post /collection', function(done) {

    var postInitialiseCollection = (userkey) => ({
      url: url_base + url_api,
      method: 'POST',
      data: {
        userkey:      userkey,
        bggname:      'jaied',
        initialise:   'true'
      } 
    });


    collection.remove({userkey: testUserKey}).then((c) => {

      ajax.ajaxPromise(postInitialiseCollection(testUserKey)).then(init => {
        expect(init.statusCode).toBe(200);
        expect(init.body.collections.length).toBe(3);
        expect(init.body.collections[0].userkey).toBe(testUserKey);
        expect(init.body.collections[0].bggname).toBe('jaied');
        expect(init.body.collections[0].bggtype).toBe('own');
        expect(init.body.collections[1].bggtype).toBe('wanttoplay');
        expect(init.body.collections[2].bggtype).toBe('wanttobuy');

        ajax.ajaxPromise(postInitialiseCollection(testUserKey)).then(init => {
          expect(init.statusCode).toBe(200);
          expect(init.body.collections.length).toBe(3);

          collection.find({userkey: testUserKey}).then((c) => {
            expect(c.length).toBe(3);
            done();
          });

        });
      });
    });


  });

  it('post read /collection', function(done) {

    var postCollection = (userkey) => ({
      url: url_base + url_api,
      method: 'POST',
      data: {
        userkey:      userkey,
        name:         'name',
        description:  'description',
        thumbnail:    'thumbnail',
        bggname:      '',
        bggtype:      'type',
        bgglistnumber: 1 
      } 
    });

    var postCollectionNoEmail = {
      url: url_base + url_api,
      method: 'POST',
      data: {
        description:'description',
        thumbnail:'thumbnail',
        bggname:  '',
        bggtype:  'type',
        bgglistnumber: 1 
      } 
    };

    // readCollection :: n -> {o}
    const readCollection = (id) => ({
      url: url_base + url_api + '/' + id,
      method: 'GET',
      data: {} 
    });

    ajax.ajaxPromise(postCollectionNoEmail).then(data => {
      expect(data.statusCode).toBe(500);

      ajax.ajaxPromise(postCollection(testUserKey)).then(data => {
        expect(data.statusCode).toBe(200);
        expect(data.body.userkey).toBe(testUserKey);
        expect(data.body.description).toBe('description');
        expect(data.body.thumbnail).toBe('thumbnail');
        expect(data.body.bggtype).toBe('type');
        testCollectionKey = data.body.key;
        
        ajax.ajaxPromise(readCollection(data.body.key)).then(dataread => {
          expect(dataread.statusCode).toBe(200);
          expect(dataread.body.key).toBe(data.body.key);
          expect(dataread.body.thumbnail).toBe('thumbnail');
          expect(dataread.body.bggtype).toBe('type');
          expect(dataread.body.isbgg).toBe(false);
          expect(dataread.body.viewcount).toBe(1);
          done();

        })
      });
    });
  });


  it('read /collection', function(done) {

    // readUserCollection :: n -> {o}
    const readUserCollection = (id) => ({
      url: url_base + url_api,
      method: 'GET',
      data: {userid: id} 
    });

    // readUserCollectionFromUser
    const readUserCollectionFromUser = (id, userid, userkey) => ({
      url: url_base + url_api,
      method: 'GET',
      data: {
        userid: id,
        userkey: userid,
        key: userkey,
      } 
    });

    ajax.ajaxPromise(readUserCollection('badkey')).then(datafail => {
      expect(datafail.statusCode).toBe(200);
      expect(datafail.body).toEqual([]);

      ajax.ajaxPromise(readUserCollection(testUserKey)).then(data => {
        expect(data.statusCode).toBe(200);
        expect(data.body[0].userkey).toBe(testUserKey);
        expect(data.body[1].userkey).toBe(testUserKey);

        // should have admin
        ajax.ajaxPromise(readUserCollectionFromUser(testUserKey, testUserKey, testUserAccessKey)).then(data => {
          expect(data.statusCode).toBe(200);
          expect(data.body[0].userkey).toBe(testUserKey);
          expect(R.has('key', data.body[0])).toBeTruthy();
          expect(R.has('adminkey', data.body[0])).toBeTruthy();

          expect(data.body[1].userkey).toBe(testUserKey);
          expect(R.has('key', data.body[1])).toBeTruthy();
          expect(R.has('adminkey', data.body[1])).toBeTruthy();

          // no key no admin 
          ajax.ajaxPromise(readUserCollectionFromUser(testUserKey, testUserKey, '' )).then(data => {
            expect(data.statusCode).toBe(200);
            expect(data.body[0].userkey).toBe(testUserKey);
            expect(R.has('key', data.body[0])).toBeTruthy();
            expect(R.has('adminkey', data.body[0])).toBeFalsy();

            expect(data.body[1].userkey).toBe(testUserKey);
            expect(R.has('key', data.body[1])).toBeTruthy();
            expect(R.has('adminkey', data.body[1])).toBeFalsy();

            // wrong key no admin 
            ajax.ajaxPromise(readUserCollectionFromUser(testUserKey, testAnonCollectionKey, testAnonCollectionAdminKey)).then(data => {
              expect(data.statusCode).toBe(200);
              expect(data.body[0].userkey).toBe(testUserKey);
              expect(R.has('key', data.body[0])).toBeTruthy();
              expect(R.has('adminkey', data.body[0])).toBeFalsy();

              expect(data.body[1].userkey).toBe(testUserKey);
              expect(R.has('key', data.body[1])).toBeTruthy();
              expect(R.has('adminkey', data.body[1])).toBeFalsy();
              done();

            });
          });
        });
      });
    });

  });


  it('patch /collection', function(done) {

    // updateCollection :: s -> s -> s -> {o}
    const updateCollection = (id, userid, userkey) => ({
      url: url_base + url_api + '/' + id,
      method: 'PATCH',
      data: {
        userkey: userid,
        key: userkey,
        name: 'np',
        description:'des',
        thumbnail: 'tn',
        bggtype: 'tp'
      } 
    });

    // updateCollection :: s -> s -> {o}
    const updateCollectionNoKey = (id, userkey) => ({
      url: url_base + url_api + '/' + id,
      method: 'PATCH',
      data: {
        userkey: userkey,
        name: 'np',
        description:'des',
        thumbnail: 'tn',
        bggtype: 'tp'
      } 
    });

    // updateCollection :: s -> s -> {o}
    const updateCollectionAdminKey = (id, adminkey) => ({
      url: url_base + url_api + '/' + id,
      method: 'PATCH',
      data: {
        adminkey: adminkey,
        name: 'admin.np',
        description:'admin.des',
        thumbnail: 'admin.tn'
      } 
    });

    ajax.ajaxPromise(updateCollection(testCollectionKey, testUserKey, testUserAccessKey)).then(dataupdate => {
      expect(dataupdate.statusCode).toBe(200);
      expect(dataupdate.body.userkey).toBe(testUserKey);
      expect(dataupdate.body.name).toBe('np');
      expect(dataupdate.body.description).toBe('des');
      expect(dataupdate.body.thumbnail).toBe('tn');
      expect(dataupdate.body.bggtype).toBe('type');

      ajax.ajaxPromise(updateCollection(testCollectionKey, testUserKey)).then(dataupdate => {
        expect(dataupdate.statusCode).toBe(401);

        ajax.ajaxPromise(updateCollectionAdminKey(testAnonCollectionKey, testAnonCollectionAdminKey)).then(dataAnonUpdate => {
          expect(dataAnonUpdate.statusCode).toBe(200);
          expect(dataAnonUpdate.body.name).toBe('admin.np');
          expect(dataAnonUpdate.body.description).toBe('admin.des');
          expect(dataAnonUpdate.body.thumbnail).toBe('admin.tn');
          done();

        });

      });
    });
  });


  it('delete /collection', function(done) {

    // readCollection :: n -> {o}
    const readCollection = (id) => ({
      url: url_base + url_api + '/' + id,
      method: 'GET',
      data: {} 
    });

    // deleteCollection :: s -> s -> {o}
    const deleteCollection = (id, userid, userkey) => ({
      url: url_base + url_api + '/' + id,
      method: 'DELETE',
      data: {
        userkey: userid,
        key: userkey,
      } 
    });

    // deleteCollection :: s -> s -> {o}
    const deleteCollectionNoKey = (id, userid) => ({
      url: url_base + url_api + '/' + id,
      method: 'DELETE',
      data: {
        userkey: userid 
      } 
    });

    // deleteCollectionAdminKey :: s -> s -> {o}
    const deleteCollectionAdminKey = (id, adminkey) => ({
      url: url_base + url_api + '/' + id,
      method: 'DELETE',
      data: {
        adminkey: adminkey
      } 
    });


    ajax
      .ajaxPromise(deleteCollectionNoKey(testCollectionKey, testUserKey))
      .then(datadelete => {
        expect(datadelete.statusCode).toBe(401);

        ajax
          .ajaxPromise(deleteCollection(testCollectionKey, testUserKey, testUserAccessKey))
          .then(datadelete => {
            expect(datadelete.statusCode).toBe(200);

            ajax.ajaxPromise(readCollection(testCollectionKey)).then(dataread => {
              expect(dataread.statusCode).toBe(404);

              ajax.ajaxPromise(deleteCollectionAdminKey(testAnonCollectionKey, testAnonCollectionAdminKey)).then(dataAnonUpdate => {
                expect(datadelete.statusCode).toBe(200);

                ajax.ajaxPromise(readCollection(testAnonCollectionKey)).then(dataread => {

                  expect(dataread.statusCode).toBe(404);
                  done();

                });
              });
            });
          });

      })
  });

  it('taskFindUserGames', function(done) {
    genhelper.GenerateUser(account, {}).then(
      user => {
        testUserKey = user.pageslug;
        testUserAccessKey = user.key;

        collections.taskCreateCollection(
          collection, 
          testUserKey,
          'name x e',
          'description',
          'thumbnail',
          '',
          'wanttoplay',
          1 
        ).fork(
          err => {},
          data => {
            testUserCollectionKey = data.key;
            genhelper
              .GenerateFakeBoardgame(boardgame)
              .then(function(game) {
                collections.taskCollectionAddGame(collection, boardgame, data.key, game.id).fork(
                  err => log.clos('taskFinduserGames.err', err),
                  data => {
                    collections.taskFindUserGames(collection, user.pageslug, 'wanttoplay', 5).fork(
                      err => log.clos('taskFinduserGames.err', err),
                      wanttoplay => {
                        expect(wanttoplay.listkey).toBe(data.key);
                        expect(wanttoplay.games[0].name).toBe(game.name);
                        expect(wanttoplay.games[0].id).toBe(game.id);
                        done();
                      }
                    )
                  }
                )
              })
          }
        )
      }
    );
  });

  it('taskDiscoverRandomCollection', function(done) {

    genhelper.GenerateUser(account, {}).then(
      user => {
        testUserKey = user.pageslug;
        testUserAccessKey = user.key;

        collections.taskCreateCollection(
          collection, 
          testUserKey,
          'name x e',
          'description',
          'thumbnail',
          '',
          'wanttoplay',
          1 
        ).fork(
          err => {},
          data => {
            testUserCollectionKey = data.key;
            Promise.all([
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),

              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),

              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame)

            ])
              .then(function(game) {
                collections.taskCollectionAddGame(collection, boardgame, data.key, game[0].id).fork(
                  err => log.clos('taskFinduserGames.err', err),
                  data => {
                    R.sequence(Task.of, [
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[1].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[2].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[3].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[4].id),

                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[5].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[6].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[7].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[8].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[9].id),

                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[10].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[11].id)

                    ]).fork(
                      err => log.clos('taskFinduserGames.err', err),
                      data => {
                        collections.taskDiscoverRandomCollection(collection, 'notme').fork(
                          err => log.clos('taskFinduserGames.err', err),
                          wanttoplay => {
                            expect(wanttoplay.listkey).toBe(data.listkey);
                            expect(wanttoplay.name).toBe('name x e');
                            expect(wanttoplay.games[0].name).toBe(game[0].name);
                            expect(wanttoplay.games[0].id).toBe(game[0].id);
                            done();
                          }
                        )
                      }
                    )
                  }
                );
              })
          }
        )
      }
    );
  }, 10000);

  it('collections&discover', function(done) {

    // getDiscoverCollection :: s -> {o}
    const getDiscoverCollection = (bggid) => ({
      url: url_base + url_api,
      method: 'GET',
      data: {
        discover: 1,
        bggid:   bggid
      } 
    });

    genhelper.GenerateUser(account, {}).then(
      user => {
        testUserKey = user.pageslug;
        testUserAccessKey = user.key;

        collections.taskCreateCollection(
          collection, 
          testUserKey,
          'somename',
          'description',
          'thumbnail',
          '',
          'wanttoplay',
          1 
        ).fork(
          err => {},
          data => {
            testUserCollectionKey = data.key;
            Promise.all([
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),

              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame),

              genhelper.GenerateFakeBoardgame(boardgame),
              genhelper.GenerateFakeBoardgame(boardgame)

            ])
              .then(function(game) {

                collections.taskCollectionAddGame(collection, boardgame, data.key, game[0].id).fork(
                  err => log.clos('taskFinduserGames.err', err),
                  data => {
                    R.sequence(Task.of, [
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[1].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[2].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[3].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[4].id),

                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[5].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[6].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[7].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[8].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[9].id),

                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[10].id),
                      collections.taskCollectionAddGame(collection, boardgame, data.key, game[11].id)

                    ]).fork(
                      err => log.clos('taskFinduserGames.err', err),
                      data => {
                        ajax.ajaxPromise(getDiscoverCollection('abc'))
                          .then(function(wanttoplay) {
                            expect(wanttoplay.body.key).toBe(data[0].key);
                            expect(wanttoplay.body.name).toBe('somename');
                            done();

                          });
                      }
                    )
                  }
                );
              })
          }
        )
      }
    );

  }, 10000);
});


describe('collections/id/games api', function() {

  var patchEventId = '';
  var patchEventAnonId = '';

  var patchUserSlug = '';
  var patchUserKey = '';
  var patchAdminKey = '';

  var gameId;

  it('prepPostGames', function(done) {

    // postCollection :: s -> {o}
    const postCollection = (userkey) => ({
      url: url_base + url_api,
      method: 'POST',
      data: {
        userkey: userkey,
        name:   'name',
        description:'description',
        thumbnail:'thumbnail',
        bggname:   '',
        bggtype:   'type',
        bgglistnumber: 1,
      } 
    });

    const postCollectionNoKey = ({
      url: url_base + url_api,
      method: 'POST',
      data: {
        name:   'name',
        description:'description',
        thumbnail:'thumbnail',
        bggname:   '',
        bggtype:   'type',
        bgglistnumber: 1,
      } 
    });

    genhelper.GenerateUser(account, {}).then(
      user => {
        patchUserSlug = user.pageslug;
        patchUserKey = user.key;

        genhelper.GenerateFakeBoardgame(boardgame)
          .then(function(game) {
            gameId = game; 

            Promise.all([
              ajax.ajaxPromise(postCollection(patchUserSlug)),
              ajax.ajaxPromise(postCollectionNoKey)
            ]).then(data => {
              expect(data[0].statusCode).toBe(200);
              expect(data[1].statusCode).toBe(200);

              patchEventId = data[0].body.key;
              patchEventAnonId = data[1].body.key;
              patchAdminKey = data[1].body.adminkey;

              done();

            });
        });
      }
    )
  });

  it('add games', function(done) {

    // collectionAddGameNoKey :: s -> n -> {o}
    var collectionAddGameNoKey = R.curry((listid, userkey, gameid) => ({
      url: url_base + url_api + '/' + listid + '/games/',
      method: 'POST',
      data: {
        gameid: gameid,
        userkey: userkey 
      } 
    }));

    // collectionAddGame :: s -> n -> {o}
    var collectionAddGame = R.curry((listid, userkey, key, gameid) => ({
      url: url_base + url_api + '/' + listid + '/games/',
      method: 'POST',
      data: {
        gameid: gameid,
        userkey: userkey,
        key: key 
      } 
    }));

    // collectionAddGameAdmin :: s -> n -> {o}
    var collectionAddGameAdmin = R.curry((listid, adminkey, gameid) => ({
      url: url_base + url_api + '/' + listid + '/games/',
      method: 'POST',
      data: {
        gameid: gameid,
        adminkey: adminkey 
      } 
    }));

    Promise.all([
      ajax.ajaxPromise(collectionAddGameNoKey(patchEventId, patchUserSlug, gameId.id)),
      ajax.ajaxPromise(collectionAddGame(patchEventId, patchUserSlug, patchUserKey, gameId.id)),
      ajax.ajaxPromise(collectionAddGameAdmin(patchEventAnonId, patchAdminKey, gameId.id))
    ]).then(adddata => {

      expect(adddata[0].statusCode).toBe(401);

      expect(adddata[1].statusCode).toBe(200);
      expect(adddata[1].body.length).toBe(1);

      expect(adddata[2].statusCode).toBe(200);
      expect(adddata[2].body.length).toBe(1);

      done();
    });
  });


  it('get games', function(done) {

    // collectionGetGames :: s -> n -> {o}
    const collectionGetGames = (key) => ({
      url: url_base + url_api + '/' + key + '/games',
      method: 'GET',
      data: {} 
    });

    // collectionGetGames :: s -> n -> {o}
    const collectionGetListGames = (user) => ({
      url: url_base + url_api + '/listtype/wanttoplay',
      method: 'GET',
      data: {
        userid: user,
        gamelimit: 5
      } 
    });

    Promise.all([
      ajax.ajaxPromise(collectionGetGames(patchEventId)),
      ajax.ajaxPromise(collectionGetListGames(testUserKey)),
    ]).then(getdata => {
      expect(getdata[0].statusCode).toBe(200);
      expect(getdata[0].body.length).toBe(1);
      expect(getdata[1].body.listkey).toBe(testUserCollectionKey);
      expect(getdata[1].body.games.length).toBe(5);
      done();
    });

  });


  it('delete games', function(done) {

    // collectionDeleteGame :: s -> n -> {o}
    const collectionDeleteGameNoKey = (listid, userid, gameid) => ({
      url: url_base + url_api + '/' + listid + '/games/' + gameid,
      method: 'DELETE',
      data: {
        userkey: userid 
      } 
    });

    // collectionDeleteGame :: s -> n -> {o}
    const collectionDeleteGame = (listid, userid, key, gameid) => ({
      url: url_base + url_api + '/' + listid + '/games/' + gameid,
      method: 'DELETE',
      data: {
        userkey: userid,
        key: key
      } 
    });

    // collectionDeleteGame :: s -> n -> {o}
    const collectionDeleteGameAdmin = (listid, adminkey, gameid) => ({
      url: url_base + url_api + '/' + listid + '/games/' + gameid,
      method: 'DELETE',
      data: {
        adminkey: adminkey 
      } 
    });

    Promise.all([
      ajax.ajaxPromise(collectionDeleteGameNoKey(patchEventId, patchUserSlug, gameId.id)),
      ajax.ajaxPromise(collectionDeleteGame(patchEventId, patchUserSlug, patchUserKey, gameId.id)),
      ajax.ajaxPromise(collectionDeleteGameAdmin(patchEventAnonId, patchAdminKey, gameId.id))
    ]).then(deletedata => {
      expect(deletedata[0].statusCode).toBe(401);

      expect(deletedata[1].statusCode).toBe(200);
      expect(deletedata[1].body.length).toBe(0);

      expect(deletedata[2].statusCode).toBe(200);
      expect(deletedata[2].body.length).toBe(0);

      done();
    });

  });
});

describe('close server', function() {
  it('connection close', function(done) {
    server.close(function(){
      mongo.CloseDatabase(mongoose);
      done();
    });
  });
});
