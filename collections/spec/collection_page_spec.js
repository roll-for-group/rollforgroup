var Promise = require('bluebird');
var R = require('ramda');
var util = require('util');
var log = require('../app/log.js');

var passport = require('passport');

var express = require('express');
var expressHelper = require('../app/expresshelper');

var login_page = require('../app/login_page.js');
var collection_page = require('../app/collection_page.js');

var genhelper = require('./genhelper.js');

var faker = require('Faker');

var Browser = require("zombie");
var browser = new Browser();
var browser2 = new Browser();
var zombieHelper = require('../spec/zombiehelper.js');

var account = require('../app/account.js');
var accounts = require('../app/accounts.js');
// var match = require('../app/match.js');
// var matches = require('../app/matches.js');
var boardgame = require('../app/boardgame.js');
var boardgames = require('../app/boardgames.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');

var mongo = require('../app/mongoconnect');
//var mongoose = mongo.Mongoose;
var mongoose = require('mongoose');
mongo.ConnectDatabase(mongoose, dbUri);

var testapp = express();
var httpPort = 9437;
var login = require('connect-ensure-login');

// String -> (Promise -> String)
var zombieGetBody = zombieHelper.PromiseZombieVisit(browser, "body");

// ============== VARS ===============
var base_url = "http://localhost:" + httpPort + "/";

login_page.SetupPassportStrategies(passport, account);
expressHelper.SetupWebpages(testapp, passport);


// ============== END SERVER FOR TESTS =======
var closeServer = function() {
    testapp.close(function(){
        console.log('collection_service_spec closed on port ' + httpPort);
    });
};

// ============== RUN TESTS ================== 
describe("collection_page_spec.js api", function() {

  var server;

  // String -> Number -> Number -> Object
  var createHostAccountObj = function(location, lat, lng, avatar) {
    return {
      profile: {
        ishost: 2,
        avataricon: avatar
      },
      hostareas: [
        {
          name: location,
          loc: {
            coordinates: [lat, lng]
          }
        }
      ]
    };
  };

  var generateUser = genhelper.GenerateUser(account);
  var generateHostUser = genhelper.GenerateHostUser(account);

  // verifyregister :: UserSchema -> Zombie -> String -> String -> Object -> UserSchema 
  var verifyregister = function(account, zombieb, email, pass, userobj) {
    return new Promise(function(fulfill, reject) {
      accounts.DeleteAccount(account, email).then(function(y) {
        var register = R.composeP(accounts.verifyAccount(account), R.prop('authToken'), accounts.registerAccount(account));

        register(email, pass).then(function(x) {
          account.update({email: email}, userobj).exec().then(function(z) {
            zombieb.visit(base_url + "login", function(err) {
              if (err) 
                reject(err);

              zombieb.fill('input[name="form-username"]', email)
              zombieb.fill('input[name="form-password"]', pass)
              zombieb.pressButton('login', function() {
                fulfill(x);
              });
            });
          });
        });
      });
    })
  };


  // BoardgameSchema -> Number -> Number -> String -> String -> Number -> Promise(Boardgame)
  var cacheGame = R.curry(function(testBoardgame, currentyear, gameid, gamename, thumbnail, gameyear) { 
      return new Promise(function(fulfill, reject) {
          var cacheObject = {
              numplays: 0,
              thumbnail: thumbnail,
              yearpublished: gameyear,
              name: {
                  t: gamename,
                  sortindex: 1
              },
              subtype: 'boardgame',
              objectid: gameid,
              objecttype: 'thing'
          };
          fulfill(boardgames.CacheBoardgame(testBoardgame, currentyear, cacheObject));
      });
  });
  var cacheGame2017 = cacheGame(boardgame, 2017);

  var generateRandomHost = accounts.RespawnAccountObject(account, faker.Internet.email(), createHostAccountObj(faker.Address.streetAddress(), faker.Address.latitude(), faker.Address.longitude(), faker.Image.avatar()));


  describe("render data", function() {
    it("maybeQuery", function(done) {
      expect(collection_page.maybeQuery({}).isNothing).toEqual(true);
      expect(collection_page.maybeQuery({query:{}}).isNothing).toEqual(true);
      expect(collection_page.maybeQuery({query:{id:'a'}}).get()).toEqual({collectionid:'a'});
      expect(collection_page.maybeQuery({query:{id:'hello'}}).get()).toEqual({collectionid:'hello'});
      done();
    });

    it("maybeAdminKey", function(done) {
      expect(collection_page.maybeAdminKey({}).isNothing).toEqual(true);
      expect(collection_page.maybeAdminKey({query:{}}).isNothing).toEqual(true);
      expect(collection_page.maybeAdminKey({query:{adminkey:'a'}}).get()).toEqual({adminkey:'a'});
      expect(collection_page.maybeAdminKey({query:{adminkey:'hello'}}).get()).toEqual({adminkey:'hello'});
      done();
    });

    it("maybeBggname", function(done) {
      expect(collection_page.maybeBggname({}).isNothing).toEqual(true);
      expect(collection_page.maybeBggname({user:{}}).isNothing).toEqual(true);
      expect(collection_page.maybeBggname({user:{profile:{bggname:'a'}}}).get()).toEqual({bgguser:'a'});
      expect(collection_page.maybeBggname({user:{profile:{bggname:'echo'}}}).get()).toEqual({bgguser:'echo'});
      done();
    });


    it("maybeBuildNavs", function(done) {
      expect(collection_page.maybeBuildNavs({}).isNothing).toBeTruthy();
      expect(collection_page.maybeBuildNavs({user:{}}).isNothing).toBeTruthy(true);
      expect(collection_page.maybeBuildNavs({user:{email:'a'}}).isJust).toBeTruthy();
      done();
    });

  });


  describe("web page tests", function() {
    it("setup server", function(done) {

      testapp.get('/', function (req, res) {
        res.send('<html>Home Page</html>')
      });

      // ['collection'].map(collection_page.RouteGetCollection(testapp, login, accounts, account));

      collection_page.routeGetCollection(testapp, 'collection');
      login_page.RouteLogins(testapp, passport);
      login_page.PostLogin(testapp, passport);
      login_page.RouteVerify(testapp, account);
      server = testapp.listen(httpPort, function() {
        done();
      });
    }, 10000);

    it("non logged in  user visit collection", function(done) {
      page = 'collection';
      browser.visit(base_url + page, function(err) {
        expect(browser.success).toBe(true);
        expect(browser.html()).toContain('Create a New Board Game List');
        done();
      });
    }, 10000);

    var userEmail = 'collection_webmatchhost@gmail.com';
    it("user login", function(done) {
      var userPass = 'testpassword';
      verifyregister(account, browser, userEmail, userPass, {} ).then(function(x) {
       done();
      });
    }, 10000);

    it("valid user visit /collection", function(done) {
      genhelper.GenerateFakeBoardgame(boardgame).then(function(bg) {

        account.findOneAndUpdate({email:userEmail}, {$set: {games:[{numplays: 10, thumbnail: bg.thumbnail, image: bg.thumbnail, yearpublished: bg.yearpublished, id: bg.id, name:bg.name, matching: {own: true, wanttoplay: true, avoid: false, matchcount: 10}}]}}).exec().then(function(x) {
          browser.visit(base_url + 'collection', function(err) {
            expect(browser.success).toBe(true);
            expect(browser.html()).toContain('maincontent');
            done();
          });
        });
      });
    });

    it('server close', function(done) {
      server.close(function() {
        done();
      });
    });

    it('db close', function(done) {
      mongo.CloseDatabase(mongoose);
      done();
    });

  });

});
