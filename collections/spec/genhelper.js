var Promise = require('bluebird');
var R = require('ramda');
var util = require('util');

var log = require('../app/log.js');
var faker = require('Faker');
var moment = require('moment');

var accounts = require('../app/accounts.js');
var boardgames = require('../app/boardgames.js');

const toLower = (s) => s.toLowerCase();

// String -> Number -> String -> Object
var createAccountObj = function(email, host, avatar, name) {
  return {
    profile: {
      ishost: host,
      avataricon: avatar,
      name: name
    },
    facebook: {
      email: email,
      name: faker.Name.firstName() + ' ' + faker.Name.lastName(),
      photo: avatar
    }
  }; 
};


// generateHostUser :: Object -> Promise(Model(account))
var generateHostUser = R.curry(function(account, userObj) {
  return new Promise(function(fulfill, reject) {
    var name = faker.Name.firstName() + ' ' + faker.Name.lastName();
    var email = faker.Internet.email();
    var hostaccount = R.merge(createHostAccountObj(toLower(email), faker.Address.streetAddress(), faker.Address.latitude(), faker.Address.longitude(), faker.Image.avatar(), name));
    var addUser = R.compose(fulfill, accounts.RespawnAccountObject(account, toLower(email)), hostaccount);
    addUser(userObj);
  });
});
exports.GenerateHostUser = generateHostUser;


// generateUser :: Object -> Promise(Model(Account))
var generateUser = R.curry(function(account, userObj) {
  return new Promise(function(fulfill, reject) {
    var name = faker.Name.firstName() + ' ' + faker.Name.lastName();
    var email = faker.Internet.email();
    var avatar = faker.Image.avatar();
    var mergeUserAccount = R.merge(createAccountObj(toLower(email), 1, avatar, name));
    var addUser = R.compose(
      fulfill, 
      accounts.RespawnAccountObject(account, email.toLowerCase()), 
      mergeUserAccount
    );
    addUser(userObj);
  });
});
exports.GenerateUser = generateUser;

// generateFakeBoardgameObject :: {o} => {b} 
var generateFakeBoardgameObject = R.curry(function(obj) {

  var gameid = faker.Helpers.randomNumber(9999999);
  var gamename = R.join(' ', faker.Lorem.words());
  var thumbnail = faker.Image.sports();
  var currentyear = new Date().getFullYear();

  var cacheObject = {
    numplays: 0,
    thumbnail: thumbnail,
    yearpublished: currentyear,
    name: {
      t: gamename,
      sortindex: 1
    },
    subtype: 'boardgame',
    objectid: gameid,
    objecttype: 'thing'
  };

  return R.merge(obj, cacheObject)

});
exports.GenerateFakeBoardgameObject = generateFakeBoardgameObject;


// generateFakeBoardgame :: Schema(boardgame) => Promise(Model(boardgame))
var generateFakeBoardgame = R.curry(function(boardgame) {
  return new Promise(function(fulfill, reject) {
    var gameid = faker.Helpers.randomNumber(9999999);
    var gamename = R.join(' ', faker.Lorem.words());
    var thumbnail = faker.Image.sports();
    var currentyear = new Date().getFullYear();

    var cacheObject = {
      numplays: 0,
      thumbnail: thumbnail,
      yearpublished: currentyear,
      name: {
        t: gamename,
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: gameid,
      objecttype: 'thing',
     };

    fulfill(boardgames.CacheReturnBoardgame(boardgame, currentyear - 1, cacheObject));

  })
});
exports.GenerateFakeBoardgame = generateFakeBoardgame;
