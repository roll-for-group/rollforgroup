var express = require("express");
var Browser = require("zombie");
var browser = new Browser();
var fs = require('fs');

// setup webpages
var app = express();
var expressHelper = require('../app/expresshelper');
var Promise = require('promise');

var test_folder = "spec/benchmark/";
var zombieHelper = require('./zombiehelper');
// String -> (Promise -> String)
var zombieGetBody = zombieHelper.PromiseZombieVisit(browser, "body");

// ============== START SERVER ===============
var base_url = "http://localhost:3098/"
var server = app.listen(3098, function(){
	  console.log('Testing on Port: 3098');
});

// ============== END SERVER FOR TESTS =======
var closeServer = function() {
  server.close(function(){
    console.log('Closing Server on Port 3098');
  });
};


const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

expressHelper.StaticRouteFolders(app, ["fail.MD", "css"]);

describe("Static route tests", function() {
	describe("function StaticRoutFolders", function() {
    it("test folder css/", function(done) {
      zombieGetBody(base_url + "css/creative.css").then(function(html) {
        expect(html).toContain('font-family: "Raleway"');
        done();
        closeServer();
      })
		});

  });
});

describe("Express helper", function() {
    describe("addReqUserToData", function() {

        it("LookupUsername", function(done) {
            var testEmail = {
                email: 'testemail',
                profile: {name: 'testname', avataricon: 'testicon'}
            };
            var testFacebook = {
                facebook: {
                    email:  'facebookemail',
                    name:   'facebookname',
                    photo:  'facebookphoto'
                }
            };
            
            var expectEmail = expressHelper.LookupUsername(testEmail);
            var expectFacebook = expressHelper.LookupUsername(testFacebook);

            expect(expectEmail.name).toBe('testname');
            expect(expectEmail.email).toBe('testemail');
            expect(expectEmail.avataricon).toBe('testicon');
            expect(expectFacebook.name).toBe('facebookname');
            expect(expectFacebook.email).toBe('facebookemail');
            expect(expectFacebook.avataricon).toBe('facebookphoto');

            done();
        });


        it("add user info to data for handlebars", function(done) {
            
            var facebookreq = {
                user: {
                    email: "testuser@email.com",
                    facebook: {
                        email: "facebook@email.com",
                        name: "John Citizen"
                    },
                    profile: {
                        bggname: "fredjones",
                        name: "emailname"
                    }
                }
            };

            var emailreq = {
                user: {
                    email: "testuser@email.com",
                    profile: {
                        name: "emailname",
                        bggname: "fredjones"
                    }
                }
            };

            var nullreq = {};

            var webdata = {
                mytest: "data"
            };
            
            var newfbdata = expressHelper.AddReqUserToData(facebookreq, webdata);
            expect(newfbdata.user.name).toBe("John Citizen");
            expect(newfbdata.user.email).toBe("facebook@email.com");
            expect(newfbdata.user.bggname).toBe("fredjones");
            expect(newfbdata.mytest).toBe("data");

            var newemaildata = expressHelper.AddReqUserToData(emailreq, webdata);
            expect(newemaildata.user.name).toBe("emailname");
            expect(newemaildata.user.email).toBe("testuser@email.com");
            expect(newemaildata.user.bggname).toBe("fredjones");
            expect(newemaildata.mytest).toBe("data");

            var newdata = expressHelper.AddReqUserToData(nullreq, webdata);
            expect(newdata).toEqual(webdata);

            done();
        });

        it("buildNavbar", function(done) {
            const buildNav = expressHelper.BuildNavbar;

            expect(buildNav('events', 'b').li[0].a.link).toBe('events');
            expect(buildNav('events', 'b').li[0].activeclass).toBe(true);
            expect(buildNav('events', 'b').li[3].a.link).toBe('users?email=b');

            expect(buildNav('lounge', 'b').li[0].a.link).toBe('events');
            expect(buildNav('lounge', '0').li[1].activeclass).toBe(true);
            expect(buildNav('lounge', 'b').li[3].a.link).toBe('users?email=b');

            expect(buildNav('events', 'd').li[0].a.link).toBe('events');
            expect(buildNav('events', 'd').li[0].activeclass).toBe(true);
            expect(buildNav('events', 'd').li[3].a.link).toBe('users?email=d');

            done();
        });


        it("buildNavbarHead", function(done) {
            const buildNav = expressHelper.BuildNavbarHead;
            expect(buildNav('f'))
              .toEqual({ 
                navhead : { 
                  dropdown : { 
                    text : 'f', 
                    menu : [ { 
                      text : 'Local Players', 
                      href : 'events' 
                    } ] 
                  } 
                } 
              });

            expect(buildNav('a'))
              .toEqual({ 
                navhead : { 
                  dropdown : { 
                    text : 'a', 
                    menu : [ { 
                      text : 'Local Players', 
                      href : 'events' } ] 
                  } 
                } 
              });

            done();
        });

    }); 
});
