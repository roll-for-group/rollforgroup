var express = require("express");
var router = express.Router();
var log = require('../app/log.js');
var R = require('ramda');
var Promise = require('bluebird');

var Browser = require("zombie");
var browser = new Browser();

var api_boardgames = require('../app/boardgames_service.js');
var request = require('ajax-request');

var bodyParser = require('body-parser')

var boardgame =     require('../app/boardgame.js');
var boardgames =    require('../app/boardgames.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});
var dbUri = nconf.get('database:uri');

var mongo = require('../app/mongoconnect.js');
//var mongoose = mongo.Mongoose;
var mongoose = require('mongoose');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var testapp = express();


// ============== AJAX ===============
var ajaxPromise = R.curry(function(options) {
  return new Promise(function (fulfill, reject) {
    request(options, function(err, res, body) {
      if (err) {
        reject(err)

      } else {
        fulfill(body)

      }
    });
  });
}); 

// ============== VARS ===============
var url_base = "http://localhost:3992";
var server;


// ============== END SERVER FOR TESTS =======
var closeServer = function() {
    testapp.close(function(){
        console.log('boardgames_service_spec closed on port 3922');
    });
};


// ============== RUN TESTS ================== 
describe("boardgames_service_spec.js boardgames api", function() {
    describe("read basic data", function() {

        var hasError = R.compose(R.has('error'), JSON.parse);
        var hasProfile = R.compose(R.has('profile'), JSON.parse);
        var readError = R.compose(R.path(['error', 'code']), JSON.parse);
        var readEmail = R.compose(R.path(['profile', 'email']), JSON.parse);

        it("setup server", function(done) {

          mongo.ConnectDatabase(mongoose, dbUri);

          var router = api_boardgames.SetupRouter();
          testapp.use('/api/v1/boardgames', router);

          server = testapp.listen(3992, function() {
            done();
          });

        });

       
        // url to test
        var url_api = '/api/v1/boardgames';

        it("test empty boardgames search", function(done) {
          var requestData = {
            url: url_base + url_api + '?name=notvaliddisneyland',
            method: 'GET',
            data: {} 
          };
          ajaxPromise(requestData).then(function(x) {
            expect(x).toBe('[]');
            done();
          });
        });

        it("test empty invalid search key", function(done) {
          var requestData = {
            url: url_base + url_api + '?search=notvaliddisneyland',
            method: 'GET',
            data: {} 
          };
          ajaxPromise(requestData).then(function(x) {
            expect(x).toBe('Bad Request');
            done();
          });
        });

        it("test one boardgames search", function(done) {

          var gametemplate = {
            objectid: 226501,
            yearpublished: 2017,
            thumbnail: '//cf.geekdo-images.com/wuxhVvdoxNm4ed-fCOvZNpqkdZA=/fit-in/246x300/pic3534544.png', 
            name: {
              t: 'Dragonfire'
            },
            expansions:[{
              name: 'Dragonfire: Adventures – A Corruption in Calimshan',
              id: 246441
            }, {
              name: 'Dragonfire: Adventures – Chaos in the Trollclaws',
              id: 233142 
            }],
            videos: [{
              "id" : 126164,
              "title" : "Tutorial juego de mesa Survive Space Attack!",
              "category" : "instructional",
              "language" : "Spanish",
              "link" : "http://www.youtube.com/watch?v=TLJ323En11U",
              "username" : "ZxWx",
              "userid" : 1087658
            }]

          };

          var gametemplate2 = {
            objectid: 193840,
            yearpublished: 2017,
            thumbnail: 'https://cf.geekdo-images.com/thumb/img/yElwe7dMjsl_84zm0kkOHvjZ7IA=/fit-in/200x150/pic2964484.jpg', 
            name: {
              t: 'The Dragon & Flagon'
            }
          };

          var gametemplate3 = {
            objectid: 246441,
            yearpublished: 2018,
            thumbnail: 'https://cf.geekdo-images.com/itemrep/img/XtGYMOjJHtB5tR-CeLK0FjURRhY=/fit-in/246x300/pic4030959.jpg', 
            name: {
              t: 'Dragonfire: Adventures – A Corruption in Calimshan'
            }
          };

          var requestData1 = {
            url: url_base + url_api + '?name=Dragonfire',
            method: 'GET',
            data: {} 
          };

          var requestData2 = {
            url: url_base + url_api + '/226501',
            method: 'GET',
            data: {} 
          };

          var requestData3 = {
            url: url_base + url_api + '/226501/expansions',
            method: 'GET',
            data: {} 
          };

          var requestData4 = {
            url: url_base + url_api + '/226501/videos',
            method: 'GET',
            data: {} 
          };

          Promise.all([
            boardgames.CacheBoardgame(boardgame, 2017, gametemplate),
            boardgames.CacheBoardgame(boardgame, 2017, gametemplate2),
            boardgames.CacheBoardgame(boardgame, 2018, gametemplate3)
          ]).then(
            function(x) {
              ajaxPromise(requestData1).then(function(x) {
                var x = JSON.parse(x);
                expect(x[0].id).toBe(226501);
                expect(x[0].name).toBe("Dragonfire");
                expect(x[0].thumbnail).toBe("//cf.geekdo-images.com/wuxhVvdoxNm4ed-fCOvZNpqkdZA=/fit-in/246x300/pic3534544.png");

                if (x[0].description == 'Pending Bgg Lookup...') {
                  expect(x[0].minplaytime).toBe(30);
                  expect(x[0].maxplaytime).toBe(60);
                  expect(x[0].minplayers).toBe(2);
                  expect(x[0].maxplayers).toBe(4);
                } else {
                  expect(x[0].minplaytime).toBe(60);
                  expect(x[0].maxplaytime).toBe(90);
                  expect(x[0].minplayers).toBe(2);
                  expect(x[0].maxplayers).toBe(6);
                }
                expect(x[0].yearpublished).toBe(2017);

                ajaxPromise(requestData2).then(function(y) {
                  var y = JSON.parse(y);
                  expect(y.id).toBe(226501);
                  expect(y.name).toBe("Dragonfire");
                  expect(y.thumbnail).toBe("//cf.geekdo-images.com/wuxhVvdoxNm4ed-fCOvZNpqkdZA=/fit-in/246x300/pic3534544.png");

                  expect(y.expansions).toBe(undefined);
                  ajaxPromise(requestData3).then(function(y) {

                    var y = JSON.parse(y);
                    expect(y.id).toBe(226501);
                    expect(y.name).toBe("Dragonfire");
                    expect(y.thumbnail).toBe("//cf.geekdo-images.com/wuxhVvdoxNm4ed-fCOvZNpqkdZA=/fit-in/246x300/pic3534544.png");

                    expect(y.expansions[0].id).toBe(246441);
                    expect(y.expansions[0].name).toBe('Dragonfire: Adventures – A Corruption in Calimshan');

                    ajaxPromise(requestData4).then(function(y) {

                      var y = JSON.parse(y);
                      expect(y.id).toBe(226501);
                      expect(y.name).toBe("Dragonfire");
                      expect(y.thumbnail).toBe("//cf.geekdo-images.com/wuxhVvdoxNm4ed-fCOvZNpqkdZA=/fit-in/246x300/pic3534544.png");
                      expect(y.videos[0].id).toBe(126164);
                      expect(y.videos[0].link).toBe('http://www.youtube.com/watch?v=TLJ323En11U');
                      done();

                    });
                  });
                });
              });
            }
          );

        });

        it("test lookupGamename", function(done) {

          var gametemplate1 = {
            objectid: 193840,
            yearpublished: 2017,
            thumbnail: 'https://cf.geekdo-images.com/thumb/img/yElwe7dMjsl_84zm0kkOHvjZ7IA=/fit-in/200x150/pic2964484.jpg', 
            name: {
              t: 'The Dragon & Flagon'
            },
            type: 'boardgame'
          };

          var gametemplate2 = {
            objectid: 212692,
            yearpublished: 2017,
            thumbnail: 'https://cf.geekdo-images.com/itemrep/img/-yp8mWAjKUOv6XtbWO2Ybmyj08s=/fit-in/246x300/pic3631964.jpg',
            name: {
							t: 'The Dragon & Flagon: Secret Room!'
            },
            type: 'boardgameexpansion'
          };

					Promise.all([
						boardgames.CacheBoardgame(boardgame, 2017, gametemplate1),
						boardgames.CacheBoardgame(boardgame, 2017, gametemplate2)
					]).then(function(y) {
            boardgame.findOneAndUpdate({id:212692}, {$set: {type: 'boardgameexpansion'}}).exec().then(
              (a) => {
                R.sequence(Task.of, [
                  api_boardgames.LookupGamename(false, 'Dragon Flagon Secret Room'),
                  api_boardgames.LookupGamename(true, 'Dragon Flagon Secret Room')
                ]).fork(
                  (err) => {
                    log.clos('err', err);
                    done('fail');
                  },
                  (x) => {

                    expect(R.find(R.propEq('name', 'The Dragon & Flagon: Secret Room!'), x[0])).toBeUndefined();


                    expect(R.find(R.propEq('name', 'The Dragon & Flagon: Secret Room!'), x[1]).id).toBe(212692);
                    expect(R.find(R.propEq('name', 'The Dragon & Flagon: Secret Room!'), x[1]).name).toBe('The Dragon & Flagon: Secret Room!');
                    expect(R.find(R.propEq('name', 'The Dragon & Flagon: Secret Room!'), x[1]).thumbnail).toBe('https://cf.geekdo-images.com/itemrep/img/-yp8mWAjKUOv6XtbWO2Ybmyj08s=/fit-in/246x300/pic3631964.jpg');
                    expect(R.find(R.propEq('name', 'The Dragon & Flagon: Secret Room!'), x[1]).minplaytime).toBe(30);
                    expect(R.find(R.propEq('name', 'The Dragon & Flagon: Secret Room!'), x[1]).maxplaytime).toBe(60);
                    expect(R.find(R.propEq('name', 'The Dragon & Flagon: Secret Room!'), x[1]).yearpublished).toBe(2017);

                    done();
                  }
                );

              }
            );

          });

        });

        it("test lookupGameId", function(done) {
          api_boardgames.LookupGameId(193840).fork(
            (err) => done('fail'),
            (x) => {
              expect(x[0].id).toBe(193840);
              expect(x[0].name).toBe('The Dragon & Flagon');
              expect(x[0].thumbnail).toBe('https://cf.geekdo-images.com/thumb/img/yElwe7dMjsl_84zm0kkOHvjZ7IA=/fit-in/200x150/pic2964484.jpg');
              expect(x[0].minplaytime).toBe(60);
              expect(x[0].maxplaytime).toBe(60);
              expect(x[0].minplayers).toBe(2);
              expect(x[0].maxplayers).toBe(8);
              expect(x[0].yearpublished).toBe(2017);
              done();
            }
          );
        });

        it("test lowercase boardgames search", function(done) {

          var requestData = {
            url: url_base + url_api + '?name=dragonfire',
            method: 'GET',
            data: {} 
          };

          ajaxPromise(requestData).then(function(x) {
            var x = JSON.parse(x);
            expect(x[0].id).toBe(226501);
            expect(x[0].name).toBe("Dragonfire");
            expect(x[0].thumbnail).toBe("//cf.geekdo-images.com/wuxhVvdoxNm4ed-fCOvZNpqkdZA=/fit-in/246x300/pic3534544.png");
            if (x[0].description == 'Pending Bgg Lookup...') {
              expect(x[0].minplaytime).toBe(30);
              expect(x[0].maxplaytime).toBe(60);
              expect(x[0].minplayers).toBe(2);
              expect(x[0].maxplayers).toBe(4);
            } else {
              expect(x[0].minplaytime).toBe(60);
              expect(x[0].maxplaytime).toBe(90);
              expect(x[0].minplayers).toBe(2);
              expect(x[0].maxplayers).toBe(6);
            }
            expect(x[0].yearpublished).toBe(2017);
            done();
          });
        });


        it("test multiple boardgames search", function(done) {

          var gametemplate = {
            objectid: 1149,
            yearpublished: 1993,
            thumbnail: '//cf.geekdo-images.com/R6NeM7ddQAg_hP-JeWYr542pAZo=/fit-in/246x300/pic897658.jpg', 
            name: {
              t: 'Dragon Strike'
            }
          };

          var requestData = {
            url: url_base + url_api + '?name=Dragon',
            method: 'GET',
            data: {} 
          };

          boardgames.CacheBoardgame(boardgame, 1993, gametemplate).then(function(x) {
           ajaxPromise(requestData).then(function(x) {
              mapobj = JSON.parse(x);
              expect(mapobj.length).toBeGreaterThan(1);
              done();
            });
          });

        });

        it("close server", function(done) {
          server.close(function() {
            mongo.CloseDatabase(mongoose);
            done();
          });
        });

    });
});
