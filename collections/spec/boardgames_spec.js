var R = require('ramda');
var util = require('util');
var testBoardgame = require('../app/boardgame.js');
var testBoardgames = require('../app/boardgames.js');
var bgg = require('../app/bgg.js');
var log = require('../app/log.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'spec/config/config.json'});

var genhelper = require('./genhelper.js');

var mongo = require('../app/mongoconnect');
//var mongoose = mongo.Mongoose;
var mongoose = require('mongoose');

var dbUri = nconf.get('database:uri');

mongoose.Promise = global.Promise;
mongo.ConnectDatabase(mongoose, dbUri);

var currentyear = new Date().getFullYear();
var cachegame = testBoardgames.CacheBoardgame(testBoardgame, currentyear);

describe('boardgames_spec.js', function() {

  it('setup', function(done) {
    var removeGame = testBoardgames.DeleteBoardgame(testBoardgame);
    Promise.all([459, 1242, 3000, 3001, 2651, 197376, 219502, 202976].map(removeGame)).then(function(x) {
      done()
    });
  });

  it('delete game bgg 1241', function(done) {
    var promiseDelete = [];
    promiseDelete[0] = testBoardgames.DeleteBoardgame(testBoardgame, 1241);
    promiseDelete[1] = testBoardgames.DeleteBoardgame(testBoardgame, 12345);
    promiseDelete[2] = testBoardgames.DeleteBoardgame(testBoardgame, 2891);
    promiseDelete[4] = testBoardgames.DeleteBoardgame(testBoardgame, 459);

    Promise.all(promiseDelete).then(
      function(x) {
        testBoardgames.ReadBoardgame(testBoardgame, 1241).then(
          function(x) {
            expect(x.length).toBe(0);
            done();
          }
        );
      }
    );
  });


  it('delete game name', function(done) {
    var promiseDelete = [];
    promiseDelete[0] = testBoardgames.deleteBoardgameByName(testBoardgame, "Cranium");

    Promise.all(promiseDelete).then(
      function(x) {
        testBoardgames.ReadBoardgame(testBoardgame, 891).then(
          function(x) {
            expect(x.length).toBe(0);
            done();
          }
        );
      }
    );
  });


  it('TaskFindBoardgame', function(done) {
    genhelper.GenerateFakeBoardgame(testBoardgame).then(function(x) {
      testBoardgames.TaskFindBoardgame(testBoardgame, x.id).fork(
        err => {},
        data => {
          expect(data.id).toBe(x.id);
          expect(data.name).toBe(x.name);
          testBoardgames.TaskFindBoardgame(testBoardgame, 9999999).fork(
            err => {
              done();
            }
          )
        }
      );
    });
  });

  it('TaskFindCountBoardgame', function(done) {
    genhelper.GenerateFakeBoardgame(testBoardgame).then(function(x) {
      testBoardgames.TaskFindCountBoardgame(testBoardgame, x.id).fork(
        err => {},
        data => {
          expect(data.id).toBe(x.id);
          expect(data.updatecounter).toBe(1);
          expect(data.name).toBe(x.name);
          testBoardgames.TaskFindCountBoardgame(testBoardgame, 9999999).fork(
            err => {
              done();
            }
          )
        }
      );
    });
  });


  it('UpdateNewGames', function(done) {
    var gametemplate = {
      objectid: 197376,
      yearpublished: 2017,
      name: {
        t: "Charterstone"
      }
    };

    testBoardgames.CacheBoardgame(testBoardgame, 2017, gametemplate).then(function(x) {
      testBoardgames.ReadBoardgame(testBoardgame, gametemplate.objectid).then(function(z) {
        expect(x.type).toBe(undefined);
        expect(z[0].yearpublished).toBe(2017);
        testBoardgames.UpdateNewGames(testBoardgame, 1).then(function(x) {

          expect(x[0].type).not.toBe(undefined);
          expect(x[0].description).not.toBe(undefined);
          expect(x[0].minplaytime).not.toBe(undefined);
          expect(x[0].maxplaytime).not.toBe(undefined);
          expect(x[0].minplayers).not.toBe(undefined);
          expect(x[0].maxplayers).not.toBe(undefined);

          done();

        }).catch((err) => log.clos('err', err));
      });
    });
  });

  it('UpdateFailLookupGames', function(done) {

    var gametemplate = {
      objectid: 2651,
      yearpublished: 2004,
      description: "Pending Bgg Lookup...",
      type: "boardgame",
      name: {
        t: "Power Grid"
      }
    };

    testBoardgames.CacheBoardgame(testBoardgame, 2017, gametemplate).then(function(x) {
      testBoardgame.findOneAndUpdate({id:2651}, {type:'boardgame'}).exec().then(function(y) {
        testBoardgames.ReadBoardgame(testBoardgame, gametemplate.objectid).then(function(z) {
          testBoardgames.UpdateFailLookupGames(testBoardgame, 1).then(function(x) {
            expect(x[0].type).toBe('boardgame');
            expect(x[0].description).toBe('Power Grid is the updated release of the Friedemann Friese crayon game Funkenschlag. It removes the crayon aspect from network building in the original edition, while retaining the fluctuating commodities market like Crude: The Oil Game and an auction round intensity reminiscent of The Princes of Florence.&amp;&amp;#35;10;&amp;&amp;#35;10;The objective of Power Grid is to supply the most cities with power when someone&apos;s network gains a predetermined size.  In this new edition, players mark pre-existing routes between cities for connection, and then bid against each other to purchase the power plants that they use to power their cities.&amp;&amp;#35;10;&amp;&amp;#35;10;However, as plants are purchased, newer, more efficient plants become available, so by merely purchasing, you&apos;re potentially allowing others access to superior equipment.&amp;&amp;#35;10;&amp;&amp;#35;10;Additionally, players must acquire the raw materials &amp;&amp;#35;40;coal, oil, garbage, and uranium&amp;&amp;#35;41; needed to power said plants &amp;&amp;#35;40;except for the &apos;renewable&apos; windfarm/ solar plants, which require no fuel&amp;&amp;#35;41;, making it a constant struggle to upgrade your plants for maximum efficiency while still retaining enough wealth to quickly expand your network to get the cheapest routes.&amp;&amp;#35;10;&amp;&amp;#35;10;Power Grid FAQ - Please read this before posting a rules question!  Many questions are asked over and over in the forums... If you have a question about a specific expansion, please check the rules forum or FAQ for that particular expansion.&amp;&amp;#35;10;&amp;&amp;#35;10;');
            expect(x[0].minplaytime).toBe(120);
            expect(x[0].maxplaytime).toBe(120);
            expect(x[0].minplayers).toBe(2);
            expect(x[0].maxplayers).toBe(6);

            done();

          }).catch((err) => log.clos('err', err));
        }).catch((err) => log.clos('ReadBoardgame.err', err));
      }).catch((err) => log.clos('findOneAndUpdate.err', err));
    }).catch((err) => log.clos('CacheBoardgame.err', err));
  });


  it('create game 459', function(done) {
    testBoardgames.CreateBoardgame(testBoardgame, 459, 'Ausgebremst', {}).then(
      function(x) {
        expect(x.id).toBe(459);
        expect(x.name).toBe('Ausgebremst');
        done();
      }
    );
  });

  it('create game 891', function(done) {
    testBoardgames.CreateBoardgame(testBoardgame, 891, 'Cranium', {}).then(
      function(x) {
        expect(x.id).toBe(891);
        expect(x.name).toBe('Cranium');
        done();
      }
    );
  });

  it('create game 185343', function(done) {
    var gametemplate = {
      objectid: 185343,
      yearpublished: 2017,
      name: {
        t: 'Anachrony'
      }
    };
    testBoardgames.CacheBoardgame(testBoardgame, 2017, gametemplate).then(
      function(x) {
        testBoardgames.ReadBoardgame(testBoardgame, 185343).then(function(z) {
          expect(z[0].yearpublished).toBe(2017);
          done();
        })
      }
    );
  });

  it('read game 459', function(done) {
    testBoardgames.ReadBoardgame(testBoardgame, 459).then(
      function(x) {
        var first = R.head(x);
        expect(first.id).toBe(459);
        expect(first.name).toBe('Ausgebremst');
        done();
      }
    );
  });

  it('precisionRound', function(done) {
    expect(testBoardgames.precisionRound(2, 12.4598)).toBe(12.46);
    expect(testBoardgames.precisionRound(3, 12.4598)).toBe(12.46);
    expect(testBoardgames.precisionRound(3, 12.4578)).toBe(12.458);
    expect(testBoardgames.precisionRound(1, 1.98)).toBe(2.0);
    done();
  });

  it('maybeRating', function(done) {
    expect(testBoardgames.maybeRating('average', {}).isNothing).toBe(true);

    expect(testBoardgames.maybeRating('average', {
      statistics: {
        ratings: {
          average: {
            value: 12.4598
          }
        }
      }
    }).get()).toBe(12.46);

    expect(testBoardgames.maybeRating('averageweight', {
      statistics: {
        ratings: {
          averageweight: {
            value: 1.4568
          }
        }
      }
    }).get()).toBe(1.46);

    done();
  });

  it('maybeRank', function(done) {
    expect(testBoardgames.maybeRank({}).isNothing).toBeTruthy();

    expect(testBoardgames.maybeRank({
      statistics: {
        ratings: {
          ranks: {
            rank: [{
              name: 'natboardgame',
              value: 30 
            }]
          }
        }
      }
    }).isNothing).toBeTruthy();

    expect(testBoardgames.maybeRank({
      statistics: {
        ratings: {
          ranks: {
            rank: [{
              name: 'boardgame',
              value: 30 
            }]
          }
        }
      }
    }).get()).toBe(30);

    expect(testBoardgames.maybeRank({
      statistics: {
        ratings: {
          ranks: {
            rank: [{
              name: 'boardgame',
              value: 'abc' 
            }]
          }
        }
      }
    }).get()).toBe('');

    done();

  });

  it('getVotes', function(done) {
    expect(testBoardgames.getVotes('a')({}).isNothing).toBeTruthy();
    expect(testBoardgames.getVotes('a')([{}]).isNothing).toBeTruthy();
    expect(testBoardgames.getVotes('a')([{value: 'a'}])).toBeTruthy();
    expect(testBoardgames.getVotes('a')([{value: 'a', numvotes:1}]).get()).toBe(1);
    expect(testBoardgames.getVotes('b')([
      {value: 'a', numvotes:1}, 
      {value: 'b', numvotes:2}]
    ).get()).toBe(2);
    done();
  });

  it('convertObj', function(done) {
    expect(testBoardgames.convertObj({})).toBe('Recommended');
    expect(testBoardgames.convertObj([
      {value:'Best', numvotes:0},
      {value:'Recommended', numvotes:0},
    ])).toBe('Recommended');
    expect(testBoardgames.convertObj([
      {value:'Best', numvotes:5},
      {value:'Recommended', numvotes:0},
    ])).toBe('Best');
    expect(testBoardgames.convertObj([
      {value:'Not Recommended', numvotes:5},
      {value:'Recommended', numvotes:0},
    ])).toBe('Not Recommended');
    done();
  });

  it('maybeValueVotes', function(done) {
    expect(testBoardgames.maybeValueVotes({}).get()).toEqual([]);
    expect(testBoardgames.maybeValueVotes({
      poll:[{
        name:'suggested_numplayers',
        results: [{
          numplayers: 1,
          result: [
            {value:'Best', numvotes:5},
            {value:'Recommended', numvotes:0},
            {value:'Not Recommended', numvotes:0}
          ]
        }]
      }]
    }).get()).toEqual(['Best']);

    expect(testBoardgames.maybeValueVotes({
      poll:[{
        name:'suggested_numplayers',
        results: [{
          numplayers: 1,
          result: [
            {value:'Best', numvotes:5},
            {value:'Recommended', numvotes:0},
            {value:'Not Recommended', numvotes:15}
          ]
        }]
      }]
    }).get()).toEqual(['Not Recommended']);

    expect(testBoardgames.maybeValueVotes({
      poll:[{
        name:'suggested_numplayers',
        results: [{
          numplayers: 1,
          result: [
            {value:'Best', numvotes:5},
            {value:'Recommended', numvotes:20},
            {value:'Not Recommended', numvotes:15}
          ]
        }]
      }]
    }).get()).toEqual(['Recommended']);

    done();
  });


  it('maybePlayerVotes', function(done) {

    expect(testBoardgames.maybePlayerVotes({}).get()).toEqual([]);

    expect(testBoardgames.maybePlayerVotes({
      poll:[{
        name:'suggested_numplayers',
        results: [
          {numplayers: 1},
          {numplayers: 2},
          {numplayers: 3},
          {numplayers: '3+'}
        ]
      }]
    }).get()).toEqual(['1', '2', '3', '3+']);

    expect(testBoardgames.maybePlayerVotes({
      poll:[{
        name:'suggested_numplayers',
        results: [
          {numplayers: 1},
          {numplayers: 2},
          {numplayers: '2+'}
        ]
      }]
    }).get()).toEqual(['1', '2', '2+']);

    done();

  });

  it('maybePlayerCounts', function(done) {

    expect(testBoardgames.maybePlayerCounts({}).get()).toEqual([]);
    expect(testBoardgames.maybePlayerCounts({
      poll:[{
        name:'suggested_numplayers',
        results: [{
          numplayers: 1,
          result: [
            {value:'Best', numvotes:0},
            {value:'Recommended', numvotes:5},
            {value:'Not Recommended', numvotes:10}
          ]
        },{
          numplayers: 2,
          result: [
            {value:'Best', numvotes:5},
            {value:'Recommended', numvotes:20},
            {value:'Not Recommended', numvotes:0}
          ]
        },{
          numplayers: 3,
          result: [
            {value:'Best', numvotes:20},
            {value:'Recommended', numvotes:15},
            {value:'Not Recommended', numvotes:0}
          ]
        }
        ]
      }]
    }).get()).toEqual([ { playercount : '1', status : 'Not Recommended' }, { playercount : '2', status : 'Recommended' }, { playercount : '3', status : 'Best' } ]);

    done();

  });

  it('createUpdateGameMongoSet', function(done) {
    expect(testBoardgames.createUpdateGameMongoSet([
      'a', 'b', 'c', 'd', 'e', 'f', 'v', 'g', 'h', 'i', 'j'
    ])).toEqual({
      $set: {
        'type'        : 'a',
        'description' : 'b', 
        'minplaytime' : 'c', 
        'maxplaytime' : 'd', 
        'minplayers'  : 'e', 
        'maxplayers'  : 'f', 
        'videos'      : 'v',
        'avgrating'   : 'g', 
        'weight'      : 'h', 
        'bggrank'     : 'i', 
        'playercounts' : 'j',
        'updatecache' : false
      }
    });
    done();
  });

  it('getLinkValues', function(done) {
    expect(testBoardgames.getLinkValues('a', {}).get()).toEqual([]);
    expect(testBoardgames.getLinkValues('a', {link: [
      {type: 'a', value: 'b' }
    ]}).get()).toEqual(['b']);
    expect(testBoardgames.getLinkValues('a', {link: [
      {type: 'a', value: 'b' },
      {type: 'a', value: 'd' },
      {type: 'a', value: 'f' },
    ]}).get()).toEqual(['b', 'd', 'f']);
    expect(testBoardgames.getLinkValues('a', {link: [
      {type: 'a', value: 'b' },
      {type: 'c', value: 'd' },
      {type: 'e', value: 'f' },
    ]}).get()).toEqual(['b']);

    done();
  });

  it('getLinkObjects', function(done) {
    expect(testBoardgames.getLinkObjects('a', {}).isNothing).toBeTruthy();
    expect(testBoardgames.getLinkObjects('a', {link: [
      {type: 'a', value: 'b', id: 3 }
    ]}).get()).toEqual([{value: 'b', id: 3}]);
    expect(testBoardgames.getLinkObjects('a', {link: [
      {type: 'a', value: 'b', id: 1 },
      {type: 'a', value: 'd', id: 2 },
      {type: 'a', value: 'f', id: 3 },
    ]}).get()).toEqual([
      {value: 'b', id: 1 },
      {value: 'd', id: 2 },
      {value: 'f', id: 3 }]);
    expect(testBoardgames.getLinkObjects('a', {link: [
      {type: 'a', value: 'b', id: 5 },
      {type: 'c', value: 'd', id: 6 },
      {type: 'e', value: 'f', id: 7 },
    ]}).get()).toEqual([{ value:'b', id: 5}]);

    done();
  });


  it('updateGameInfo -> boardgame type', function(done) {

      var gametemplate = {
        objectid: 219502,
        yearpublished: 2017,
        name: {
          t: "Templars' Journey"
        }
      };

      testBoardgames.CacheBoardgame(testBoardgame, 2017, gametemplate).then(function(x) {
        testBoardgames.ReadBoardgame(testBoardgame, 219502).then(function(z) {
          expect(x.type).toBe(undefined);
          expect(z[0].yearpublished).toBe(2017);
          expect(z[0].maxplaytime).toBe(60);
          testBoardgames.UpdateGameInfo(testBoardgame, 219502).then(function(x) {
            expect(x.type).toBe('boardgame');
            expect(x.yearpublished).toBe(2017);
            expect(x.minplaytime).toBe(60);
            expect(x.maxplaytime).toBe(60);
            expect(x.minplayers).toBe(3);
            expect(x.maxplayers).toBe(5);
            done();
          });
        })
      });
  });

  it('updateGameInfo -> boardgameexpansion type', function(done) {

    var gametemplate = {
      objectid: 202976,
      yearpublished: 2017,
      name: {
        t: "7 Wonders Duel: Pantheon"
      }
    };
    testBoardgames.CacheBoardgame(testBoardgame, 2017, gametemplate).then(function(x) {
      testBoardgames.ReadBoardgame(testBoardgame, 202976).then(function(z) {
        expect(x.type).toBe(undefined);
        expect(z[0].yearpublished).toBe(2017);
        testBoardgames.UpdateGameInfo(testBoardgame, 202976).then(function(x) {
          expect(x.type).toBe('boardgameexpansion');
          done();
        });
      })
    });
  });

  it('search game name Anachrony active', function(done) {
    testBoardgames.SearchBoardgameName(testBoardgame, 'anachrony', false).then(
      function(x) {
        var first = R.head(x);
        expect(first.id).toBe(185343);
        expect(first.name).toBe('Anachrony');
        done();
      },
      function(err) {
        log.ConsoleLogObject(err);
      }
    );
  });

  it('search game name Ausgebremst inactive', function(done) {
    testBoardgames.SearchBoardgameName(testBoardgame, 'ausgebremst', false).then(
      function(x) {
        var first = R.head(x);
        expect(first.id).toBe(459);
        expect(first.name).toBe('Ausgebremst');
        done();
      },
      function(err) {
        log.ConsoleLogObject(err);
      }
    );
  });

  it("test getting board game name", function(done) {

    var testname1 = {name: 'testname'};
    var testname2 = {name: 'testanothername'};

    var arrayname1 = {name: [
      {value: 'Gamename1', sortindex: 1, type: 'primary'},
      {value: 'Gamename2', sortindex: 2, type: 'alternate'},
      {value: 'Gamename3', sortindex: 3, type: 'alternate'}
    ]};

    var arrayname2 = {name: [
      {value: 'Gamename1', sortindex: 2, type: 'alternate'},
      {value: 'Gamename2', sortindex: 1, type: 'primary'},
      {value: 'Gamename3', sortindex: 3, type: 'alternate'}
    ]};

    expect(testBoardgames.GetBoardgameName(testname1)).toBe('testname');
    expect(testBoardgames.GetBoardgameName(testname2)).toBe('testanothername');
    expect(testBoardgames.GetBoardgameName(arrayname1)).toBe('Gamename1');
    expect(testBoardgames.GetBoardgameName(arrayname2)).toBe('Gamename2');

    done();

  });

  it('read game 891', function(done) {
    testBoardgames.ReadBoardgame(testBoardgame, 891).then(
      function(x) {
        var first = R.head(x);
        expect(first.id).toBe(891);
        expect(first.name).toBe('Cranium');
        done();
      }
    );
  });

  it('preread game 12345', function(done) {
    testBoardgames.ReadBoardgame(testBoardgame, 12345).then(
      function(x) {
        expect(x.length).toBe(0);
        done();
      }
    );
  });

  it('cache game 12345', function(done) {
    var cacheObject = {
      numplays: 0,
      thumbnail: '//cf.geekdo-images.com/images/pic12345.jpg',
      yearpublished: 2011,
      name: {
        t: 'Test name',
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: 12345,
      objecttype: 'thing'
    };

    // expect return
    var shadowObject = {
      id: cacheObject.objectid,
      name: cacheObject.name.t,
      yearpublished: cacheObject.yearpublished,
      thumbnail: cacheObject.thumbnail,
      subtype: cacheObject.subtype,
      active: false,
      activestatus: 'inactive'
    };

    testBoardgames.CacheReturnBoardgame(testBoardgame, 2017, cacheObject).then(function(x) {
      var test = R.whereEq(shadowObject)
      expect(test(x)).toBe(true); 
      done();
    });
  });

  it('post read game 12345', function(done) {
    testBoardgames.ReadBoardgame(testBoardgame, 12345).then(
      function(x) {
        expect(x.length).toBe(1);
        var first = R.head(x);
        expect(first.id).toBe(12345);
        expect(first.name).toBe('Test name');
        expect(first.yearpublished).toBe(2011);
        expect(first.thumbnail).toBe('//cf.geekdo-images.com/images/pic12345.jpg');
        expect(R.hasIn('description', x)).toBeFalsy();
        done();
      }
    );
  });

  it('test repeat cache doesnt duplicate 12345', function(done) {
    var cacheObject = {
      numplays: 0,
      thumbnail: '//cf.geekdo-images.com/images/pic12345.jpg',
      yearpublished: 2011,
      name: {
        t: 'Test name',
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: 12345,
      objecttype: 'thing'
    };

    cachegame(cacheObject).then(function(a) {
      testBoardgames.ReadBoardgame(testBoardgame, 12345).then(
        function(x) {
          expect(x.length).toBe(1);
          done();
        }
      )
    });
  });

  it('increment game 12345', function(done) {
    var readWantPlay = R.prop('wanttoplay');
    var firstRead = R.composeP(R.head, testBoardgames.ReadBoardgame(testBoardgame));
    firstRead(12345).then(function(x) {
      expect(readWantPlay(x)).toBe(0);

      var gamename = R.prop('name', x);
      testBoardgames.IncrementWantToPlay(testBoardgame, gamename).then(function(y) {
        expect(y.wanttoplay).toBe(1);
        done();
      }, function(err) {
        console.log(err);
      });
    });
  });

  it('deincrement game 12345', function(done) {
    var readWantPlay = R.prop('wanttoplay');
    var firstRead = R.composeP(R.head, testBoardgames.ReadBoardgame(testBoardgame));
    firstRead(12345).then(function(x) {
      expect(readWantPlay(x)).toBe(1);

      var gamename = R.prop('name', x);
      testBoardgames.DeincrementWantToPlay(testBoardgame, gamename).then(
        function(y) {
          expect(y.wanttoplay).toBe(0);
          done();
        },
        function(err) {
          console.log(err);
        }
      );
    });
  });

  it('cache game 2891', function(done) {
    var gameName = 'Cranium-xx';
    var cacheObject = {
      numplays: 0,
      thumbnail: '//cf.geekdo-images.com/images/pic188821_t.jpg',
      yearpublished: 2010,
      name: {
        t: gameName,
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: 2891,
      objecttype: 'thing'
    };

    cachegame(cacheObject).then(function(x){
      expect(x.ok).toBe(1);
      expect(x.n).toBe(1);
      expect(x.nModified).toBe(0);

      testBoardgames.ReadBoardgame(testBoardgame, 2891).then(function(y) {
        var result = R.head(y);
        expect(result.id).toBe(2891);
        expect(result.name).toBe(gameName);
        expect(result.thumbnail).toBe('//cf.geekdo-images.com/images/pic188821_t.jpg');
        expect(result.yearpublished).toBe(2010);
        done();
      });
    }, function(err) {
      log.ConsoleLogObject(err);
    });
  });

  it('changed cache for game 2891', function(done) {
    var cacheObject = {
      numplays: 0,
      thumbnail: '//cf.geekdo-images.com/images/abc188821_t.jpg',
      yearpublished: 2015,
      name: {
        t: 'Cranium-Changed',
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: 2891,
      objecttype: 'thing'
    };

    cachegame(cacheObject).then(function(x){
      expect(x.id).toBe(2891);
      expect(x.name).toBe('Cranium-Changed');
      expect(x.thumbnail).toBe('//cf.geekdo-images.com/images/abc188821_t.jpg');
      expect(x.yearpublished).toBe(2015);
      testBoardgames.ReadBoardgame(testBoardgame, 2891).then(function(y) {
        var result = R.head(y);

        expect(result.id).toBe(2891);
        expect(result.name).toBe('Cranium-Changed');
        expect(result.thumbnail).toBe('//cf.geekdo-images.com/images/abc188821_t.jpg');
        expect(result.yearpublished).toBe(2015);
        done();

      });
    });
  });


  it('check publish activation by date', function(done) {

    var previousyear = 2015;
    var currentyear = 2016;

    var cacheObjectOld = {
      numplays: 0,
      thumbnail: '//cf.geekdo-images.com/images/abc188821_t.jpg',
      yearpublished: previousyear,
      name: {
        t: 'Test not active',
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: 3000,
      objecttype: 'thing'
    };

    var cacheObjectNew = {
      numplays: 0,
      thumbnail: '//cf.geekdo-images.com/images/abc188821_t.jpg',
      yearpublished: currentyear,
      name: {
        t: 'Test Active',
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: 3001,
      objecttype: 'thing'
    };
    var cacheObjects = [cacheObjectOld, cacheObjectNew];

    cacheinCurrentyear = testBoardgames.CacheBoardgame(testBoardgame, currentyear + 1);
    Promise.all(R.map(cacheinCurrentyear, cacheObjects)).then(function(x) {

      var createdGameIds = [3000, 3001];
      var readgames = R.composeP(R.head, testBoardgames.ReadBoardgame(testBoardgame));
      Promise.all(R.map(readgames, createdGameIds)).then(function(y) {
        expect(y[0].active).toBeFalsy(); 
        expect(y[0].activestatus).toBe('inactive'); 
        expect(R.prop('active', y[1])).toBeTruthy(); 
        expect(R.prop('activestatus', y[1])).toBe('active'); 
        done();
      });

    });
  });

  it('read known boardgames', function(done) {
    var optionList = testBoardgames.GetBoardgameOptionList(testBoardgame).then(
      function(x) {
        expect(R.contains({id:459}, x)).toBeTruthy;
        expect(R.contains({option:'Ausgebremst'}, x)).toBeTruthy;
        done();
      }
    );
  });


  it('taskCheckExpansionData', function(done) {

    var expansion = {
      value: 'Neta-Tanka',
      id: 245931
    }

    var cacheObjectNew = {
      numplays: 0,
      thumbnail: '//cf.geekdo-images.com/images/abc188821_t.jpg',
      yearpublished: 2017,
      name: {
        t: 'Neta-Tanka',
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: 245931,
      objecttype: 'thing'
    };

    cacheinCurrentyear = testBoardgames.CacheBoardgame(testBoardgame, currentyear + 1, cacheObjectNew).then(function(x) {
      testBoardgames.taskCheckExpansionData(testBoardgame, expansion).fork(
        err => {
          log.clos('err', err);
        },
        data => {
          expect(data.id).toBe(245931);
          expect(data.thumbnail).toBe('//cf.geekdo-images.com/images/abc188821_t.jpg');
          done();
        }
      )
    });

  });


  it('getExpansions', function(done) {

    var cacheObjectOld = {
      numplays: 0,
      thumbnail: '//cf.geekdo-images.com/images/abc188821_t.jpg',
      yearpublished: 2004,
      name: {
        t: 'Ticket to Ride',
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: 9209,
      objecttype: 'thing'
    };

    var expansions = {
      expansions: [{
        value: 'Ticket to Ride Map Collection: Volume 6 - France & Old West',
        thumb: '//cf.geekdo-images.com/images/abc188821_t.jpg',
        id: 233891
      }]
    };

    var cacheObjectNew = {
      numplays: 0,
      thumbnail: '//cf.geekdo-images.com/images/abc188821_t.jpg',
      yearpublished: 2017,
      name: {
        t: 'Ticket to Ride Map Collection: Volume 6 - France & Old West',
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: 233891,
      objecttype: 'thing'
    };

    var cacheObjects = [cacheObjectOld, cacheObjectNew];

    cacheinCurrentyear = testBoardgames.CacheBoardgame(testBoardgame, currentyear + 1);
    Promise.all(R.map(cacheinCurrentyear, cacheObjects)).then(function(x) {
      testBoardgame.findOneAndUpdate({id:9209}, expansions, {new:true}).then(function(g) {
        expect(R.has('thumbnail', g.expansions[0])).toBeFalsy();
        testBoardgames.getExpansions(testBoardgame, 9209).fork(
          err => {},
          data => {
            expect(data.expansions.length).toBe(1);
            expect(data.expansions[0].id).toBe(233891);
            expect(data.expansions[0].thumbnail).toBe('//cf.geekdo-images.com/images/abc188821_t.jpg');
            done();

          }
        )
      });
    });
  });


  it('getVideos', function(done) {

    var cacheObjectOld = {
      numplays: 0,
      thumbnail: 'https://cf.geekdo-images.com/thumb/img/tXEvWoebov1J8Pjq0SLGo236vQg=/fit-in/200x150/pic2506676.jpg',
      yearpublished: 2015,
      name: {
        t: 'Survive: Space Attack!',
        sortindex: 1
      },
      subtype: 'boardgame',
      objectid: 177147,
      objecttype: 'thing'
    };

    var videos = {
      "videos" : [ 
        {
          "id" : 126164,
          "title" : "Tutorial juego de mesa Survive Space Attack!",
          "category" : "instructional",
          "language" : "Spanish",
          "link" : "http://www.youtube.com/watch?v=TLJ323En11U",
          "username" : "ZxWx",
          "userid" : 1087658,
        }, 
        {
          "id" : 82471,
          "title" : "Survive in space! ~ what's in the box? with biffta",
          "category" : "other",
          "language" : "English",
          "link" : "http://www.youtube.com/watch?v=2aw7TsbQUZc",
          "username" : "biffta",
          "userid" : 412918,
        }, 
        {
          "id" : 81211,
          "title" : "Ice Dice BGS: Survive: Space Attack Review",
          "category" : "review",
          "language" : "English",
          "link" : "http://www.youtube.com/watch?v=GWUWjzQxmgA",
          "username" : "IceDiceBGS",
          "userid" : 1053038,
        }, 
        {
          "id" : 77146,
          "title" : "Overview of Survive: Space Attack! at Gen Con 2015",
          "category" : "interview",
          "language" : "English",
          "link" : "http://www.youtube.com/watch?v=nY755wmUrTc",
          "username" : "W Eric Martin",
          "userid" : 15734,
        }, 
        {
          "id" : 77003,
          "title" : "Hunter & Cron: Boardgame Review - Survive: Space Attack!",
          "category" : "review",
          "language" : "German",
          "link" : "http://www.youtube.com/watch?v=FABQ6JhtB-E",
          "username" : "Norc",
          "userid" : 405769,
        }, 
        {
          "id" : 76595,
          "title" : "Dice Tower Reviews: Survive: Space Attack!",
          "category" : "review",
          "language" : "English",
          "link" : "http://www.youtube.com/watch?v=LjvWFhyRbGA",
          "username" : "TomVasel",
          "userid" : 722,
        }, 
        {
          "id" : 75075,
          "title" : "Survive! Space Attack Overview – GenCon 2015",
          "category" : "interview",
          "language" : "English",
          "link" : "http://www.youtube.com/watch?v=GHxuDH-mJD8",
          "username" : "teamcovenant",
          "userid" : 675333,
        }, 
        {
          "id" : 74941,
          "title" : "Gen Con 2015 - Survive Space Attack ",
          "category" : "review",
          "language" : "English",
          "link" : "http://www.youtube.com/watch?v=DZ5KcmGclX0",
          "username" : "eekamouse",
          "userid" : 75811,
        }
      ]
    };

    cacheinCurrentyear = testBoardgames.CacheBoardgame(testBoardgame, currentyear + 1);

    cacheinCurrentyear(cacheObjectOld).then(function(x) {
      testBoardgame.findOneAndUpdate({id:177147}, videos, {new:true}).then(function(g) {

        testBoardgames.getVideos(testBoardgame, 177147).fork(
          err => {},
          data => {
            expect(data.videos.length).toBe(8);

            expect(data.videos[1].id).toBe(82471);
            expect(data.videos[1].username).toBe('biffta');
            expect(data.videos[1].link).toBe('http://www.youtube.com/watch?v=2aw7TsbQUZc');
            done();

          }
        )
      });
    });
  });



});

describe('close server', function() {
  it('connection close', function(done) {
    mongo.CloseDatabase(mongoose);
    done();
  });
});
