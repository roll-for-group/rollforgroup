var log = require('../app/log.js');

var R = require('ramda');
var assoc   = R.assoc,
    compose = R.compose,
    curry   = R.curry,
    equals  = R.equals,
    merge   = R.merge,
    prop    = R.prop,
    when    = R.when;


var request = require('ajax-request');
var Promise = require('bluebird');
const Task      = require('data.task');


// ============== AJAX ===============
var ajaxPromise = curry(function(options) {
  return new Promise(function (fulfill, reject) {
    try {
      request(options, function(err, res, body) {
        if (err) {
          reject(err)
        } else {
          statusCode = prop('statusCode');
          var datify = compose(assoc('body', R.__, {}), JSON.parse);
          var isokcode = compose(equals(200), prop('statusCode'));

          if (prop('statusCode', res) == 200) {
            fulfillify = compose(fulfill, when(isokcode, merge(datify(body))), assoc('statusCode', R.__, {}), prop('statusCode'));
          } else {
            fulfillify = compose(fulfill, assoc('statusCode', R.__, {}), prop('statusCode'));
          };
          fulfillify(res);
        }
      });
    } catch(err) {
      log.ConsoleLogObjectSection('ajaxPromise:Error', err);
    };
  });
}); 
exports.ajaxPromise = ajaxPromise;

var ajaxTask = function(options) {
  return new Task((reject, resolve) => 
    ajaxPromise(options)
      .then(resolve)
      .catch(reject)
  );
};
exports.ajaxTask = ajaxTask;
