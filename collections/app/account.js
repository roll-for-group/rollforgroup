var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongooseEmail = require('passport-local-mongoose-email');

const uuid = require('uuid');
const shortid = require('shortid');

var Account = new Schema({
  nickname    : String,
  email       : {type: String, index: {unique: true}},
	birthdate   : Date,
  pageslug    : {type: String, default: shortid.generate, index: {unique: true}},
	age         : Number,
  key         : {type: String, default: uuid.v4, index: {unique: true}},
  icalkey     : {type: String, default: uuid.v4},
  tickets     : {type: Number, default: 5},
  ticketshold : {type: Number, default: 0},
  ticketsconsume  : {type: Number, default: 0},
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  locale      : String,
	facebook		: {
		id			: String,
		token		: String,
		email		: String,
		name		: String,
		photo		: String
	},
  google  :{
		id			: String,
		token		: String,
		email		: String,
		name		: String,
		photo		: String,
		gender		: String
	},
	relationship  : [{
		email       : String,
		rating		  : Number,
		gameplays	  : Number,
		complaints	: Number,
		tags		    : [String]
	}],
  gamesCollectionCreated: {
    type: Boolean,
    default: false
  },
  games         : [{
    id          : Number,
    numplays    : Number,
    name        : String,
    minplaytime : Number,
    maxplaytime : Number,
    minplayers  : Number,
    maxplayers  : Number,
    matching    : {
      wanttoplay  :       Boolean,
      own         :       Boolean,
      wanttobuy   :       Boolean,
      wishlist    :       Boolean,
      fortrade    :       {type: Boolean, default: false},
      want        :       {type: Boolean, default: false},
      teach       :       {type: Boolean, default: false},
      skillrating :       {type: Number,  default: 0},
      socialrating :      {type: Number,  default: 0},
      personalrating :    {type: Number,  default: 0},
      matchcount  :       {type: Number,  default: 0},
      avoid       :       {type: Boolean, default: false}
    },
    thumbnail   : String,
    image       : String,
    yearpublished   : Number,
    bggcache: Schema.Types.Mixed
  }],
  hostareas : [{
    name    : String,
    loc: {
      type: {
        String,
        required: true,
        enum: ['Point', 'LineString', 'Polygon'],
        default: 'Point'
      },
      coordinates: [
        {
          type: [Number],
          default: [0,0]
        }
      ] 
    }
  }],
  profile			          : {
    email               : String,
    age                 : Number,
		avataricon          : String,
    avataricontype      : String,
    biography           : String,
    job                 : String,
		stage		            : String,
		queue		            : String,
		bggname		          : String,
		party1		          : String,
		party2		          : String,
		party3		          : String,
		party4		          : String,
		name		            : String,
		Monday		          : {type: Boolean, default: false},
		Tuesday		          : {type: Boolean, default: false},
		Wednesday	          : {type: Boolean, default: false},
		Thursday	          : {type: Boolean, default: false},
		Friday		          : {type: Boolean, default: false},
		Saturday	          : {type: Boolean, default: false},
		Sunday		          : {type: Boolean, default: false},
		mondaystarthour		  : {type: Number, default: 18},
		mondaystartminute	  : {type: Number, default: 0},
		mondayendhour		    : {type: Number, default: 22},
		mondayendminute		  : {type: Number, default: 30},
		tuesdaystarthour	  : {type: Number, default: 18},
		tuesdaystartminute	: {type: Number, default: 0},
		tuesdayendhour	    : {type: Number, default: 22},
		tuesdayendminute	  : {type: Number, default: 30},
		wednesdaystarthour  : {type: Number, default: 18},
		wednesdaystartminute: {type: Number, defaut: 0},
    wednesdayendhour	  : {type: Number, default: 22},
		wednesdayendminute	: {type: Number, default: 30},
		thursdaystarthour	  : {type: Number, default: 18},
		thursdaystartminute	: {type: Number, default: 0},
		thursdayendhour	    : {type: Number, default: 22},
		thursdayendminute	  : {type: Number, default: 30},
		fridaystarthour		  : {type: Number, default: 18},
		fridaystartminute	  : {type: Number, default: 0},
		fridayendhour	      : {type: Number, default: 22},
		fridayendminute	    : {type: Number, default: 30},
		saturdaystarthour	  : {type: Number, default: 12},
		saturdaystartminute	: {type: Number, default: 0},
		saturdayendhour		  : {type: Number, default: 22},
		saturdayendminute   : {type: Number, default: 30},
		sundaystarthour		  : {type: Number, default: 12},
		sundaystartminute   : {type: Number, default: 0},
		sundayendhour		    : {type: Number, default: 22},
		sundayendminute		  : {type: Number, default: 30},
    isteacher           : {type: Number, default: 1},
    ishost              : {type: Number, default: 1},
		playeralcohol       : Number,
		playersmoking       : Number,
		venueprivate        : Number,
		venuenogamegroup    : {type: Boolean, default: false},
		venuenocommercial   : {type: Boolean, default: false},
    venuenoconvention   : {type: Boolean, default: false},
		venuenomeetup       : {type: Boolean, default: false},
    searchradius        : {type: Number,  default: 50},
    nodriving           : {type: Boolean, default: false},
    carpool             : {type: Boolean, default: false},
    pubtransport        : {type: Number},
		refer1				      : String,
		refer2				      : String,
		refer3				      : String,
		refer4				      : String,
		Icanhostgames	      : Boolean,
		Icanbringboardgames	: Boolean,
		Icanbringgamerfriendlysnacks : Boolean,
		Icanbringbeverages	: Boolean,
		Icandrivepeople	    : Boolean,
		hostmax				      : Number,
		hostalcohol		      : Number,
		hostsmoking		      : Number,
    localarea           : { 
      name              : String,
      loc: {
        type: {
          String,
          required: true,
          enum: ['Point', 'LineString', 'Polygon'],
          default: 'Point'
        },
        coordinates: [
          {type: [Number]}
        ] 
      }
    },
    usebggcollection  : {type: Boolean, default: true},
    notifyevents      : {type: Boolean, default: true},
    notifyplayers     : Boolean,
    notifyupdates     : {type: Boolean, default: true},
    notifymatch       : Boolean,
    visibility        : {type: Number, index: true},
    matching          : Number,
    socialfacebook    : String,
    socialtwitter     : String,
    socialpinterest   : String,
    socialtwitch      : String,
    socialyoutube     : String,
    socialwebsite     : String,
    socialgoogleplus  : String,
    commemailvisible  : Boolean,
    commsteam         : String,
    commphoneno       : String,
    commskype         : String,
    commreddit        : String,
    commbattlenet     : String,
    commxbox          : String,
    commpsn           : String,
    commwii           : String,
    motivecompetition : Boolean,
    motivesocialmanipulation : Boolean,
    motivecommunity   : Boolean,
    motivecooperation : Boolean,
    motivestrategy    : Boolean,
    motivediscovery   : Boolean,
    motivedesign      : Boolean,
    motivestory       : Boolean,
    playlevel         : {type: Number, default: 0},
    playxp            : {type: Number, default: 0},
    hostlevel         : {type: Number, default: 0},
    hostxp            : {type: Number, default: 0},
    teachinglevel     : {type: Number, default: 0},
    teachingxp        : {type: Number, default: 0},
	},
	bgg: {
    cache           : {
      user: Schema.Types.Mixed,
      collection: Schema.Types.Mixed
    },
    lastupdated		: Date
	},
  logincount: {type: Number, default: 0},
  loginlast: Date,
  icalviewcount: {type: Number, default: 0},
  activelast: {type: Date, index: true},
  premium: {
    isstaff         : Boolean,
    isactive        : Boolean,
    activetodate    : Date,
    isongoing       : Boolean,
    isongoingsize   : Number,
    creditsself     : {type: Number, default: 0},
    creditsshare    : {type: Number, default: 0},
    creditsconsumed : {type: Number, default: 0},
    creditsexpire   : Date,
    creditlog       : [{
      from          : String,
      to            : String,
      date          : Date,
      activeto      : Date,
      description   : String, 
      runningtotal  : Number,
      reference     : String
    }]
  },
  purchases: [{
    ordercode   : String,
    items       : [{
      code      : String,
      quant     : Number,
      price     : Number 
    }],
    total       : Number,
    currency    : String,
    date        : Date,
  }],
  notifications: [{
    noteid      : {type: String, default: shortid.generate},
    date        : {type: Date}, 
    text        : String,
    emailText	  : String,
    emailHtml	  : String,
    status      : {
      type      : String, 
      enum      : ['new', 'emailed', 'read'], 
      default   : 'new'
    },
    thumbnail   : String,
    url         : String,
    noticetype  : String,
    openNewTab  : Boolean
  }],
  mails: [{
    mailid      : {type: String, default: shortid.generate},
    name        : String,
    thumbnail   : String,
    pageslug    : String,
    date        : {type: Date}, 
    subject     : String,
    text        : String,
    status      : {
      type      : String, 
      enum      : ['new', 'emailed', 'read'], 
      default   : 'new'
    },
    mailtype    : String,
  }],
  senttoemaillog: [{
    template: String,
    datesent: Date
  }]
},
{
  timestamps: true
});

Account.index({'profile.localarea.loc': '2dsphere'});
Account.index({'hostareas[loc]': '2dsphere'});

Account.plugin(passportLocalMongooseEmail, {
	usernameField: 'email',
  usernameLowerCase: true
});

module.exports = mongoose.model('Account', Account);
