// var mongoose = require('mongoose');
// var mongoose = require('mongoose').set('debug',true);
var connectionCount = 0;
var Promise = require('bluebird');
var R = require('ramda');

// module.exports = mongoose;
// exports.Mongoose = mongoose;

// connectDatabase :: MongooseObject -> String -> MongooseObject
var connectDatabase = R.curry(function(mongoose, dbUri) {
  mongoose.Promise = global.Promise;
  if (connectionCount == 0) {
    mongoose.connect(dbUri);
    console.log('Mongoose Opened')
  }
  connectionCount = incrementState(connectionCount);
  return mongoose;

});
exports.ConnectDatabase = connectDatabase;


// closeDatabase :: MongooseObject -> String -> MongooseObject
var closeDatabase = function(mongoose) {
  connectionCount = decreaseState(connectionCount);
  if (connectionCount == 0) {
    mongoose.connection.close(function() {
      console.log('Mongoose Closed')
    });
  };
  return mongoose;
};
exports.CloseDatabase = closeDatabase;

var incrementState = function(count) {
  count = count + 1;
  console.log('Mongoose increase: ' + count);
  return count;
};
exports.IncrementState = incrementState;

var decreaseState = function(count) {
  count = count - 1;
  console.log('Mongoose decrease: ' + count);
  return count;
};
