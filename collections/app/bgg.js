var exports = module.exports = {};
var R = require('ramda');
var util = require('util');
var log = require('./log.js');
var request = require('ajax-request');
var getrequest = require('request');
var parseString = require('xml2js').parseString;

// all options are optional
var options = {
  timeout: 10000, // timeout of 10s (5s is the default)
  // see https://github.com/cujojs/rest/blob/master/docs/interceptors.md#module-rest/interceptor/retry
  retry: {
    initial: 100,
    multiplier: 2,
    max: 15e3
  }
}
var bgg = require('bgg')(options);

// ============== AJAX ===============
var ajaxPromise = R.curry(function(options) {
    return new Promise(function (fulfill, reject) {
        request(options, function(err, res, body) {
            if (err) {
                reject(err)

            } else {
                fulfill(body)

            }
        });
    });
}); 

// requestPromise :: String -> Promise(String)
var requestPromise = R.curry(function(url) {
    return new Promise(function(fulfill, reject) {
        getrequest(url, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                fulfill(body);
            } else {
                reject(response.statusCode);
            }
        });
    })
});
exports.RequestPromise = requestPromise;


// removeXMLCharacterCodes :: String -> String
var removeXMLCharacterCodes = function(x) {

  try {

    var replaceAmpersand = R.replace(/&amp;/g, '&'); 
    var replaceLessThan = R.replace(/&lt;/g, '<'); 
    var replaceGreaterThan = R.replace(/&gt;/g, '>'); 
    var replaceQuotes = R.replace(/&quot;/g, "\\\"");
    var replaceApostrophe = R.replace(/&apos;/g, "'");
    var replaceLinefeed = R.replace(/&&#35;10;/g, "\\n");
    var replaceLeftBracket = R.replace(/&&#35;40;/g, "(");
    var replaceRightBracket = R.replace(/&&#35;41;/g, ")");
    var replaceXMLCharacterCodesFromString = R.compose(replaceLeftBracket, replaceRightBracket, replaceLinefeed, replaceApostrophe, replaceQuotes, replaceLessThan, replaceGreaterThan, replaceAmpersand);

    return replaceXMLCharacterCodesFromString(x);

  } catch(err) {
    return x;
  }
}
exports.RemoveXMLCharacterCodes = removeXMLCharacterCodes;


// replaceTDollar :: JSONObject -> JSONObject
var replaceTDollar = function(data) {
  var replacet = R.curry(R.replace(/\$t/g, 't'));
  var removefieldt = R.compose(JSON.parse, removeXMLCharacterCodes, replacet, JSON.stringify);
  return removefieldt(data);
}
exports.ReplaceTDollar = replaceTDollar;


// extractJSONItem :: JSONObject -> JSONObject
var extractJSONItem = function(BGGObject) {
  return R.path(['items','item'], BGGObject);
}


// checkForValidCollection :: JSONObject -> Promise(bggusername, JSONObject);
var checkForValidCollection = R.curry(function(bggusername, collection) {
  return new Promise(function(fulfill, reject) {
    if (collection.hasOwnProperty('message')) {
      setTimeout(fulfill(getBGGUserCollection(bggusername)), 300);
    } else if (R.isEmpty(collection)) {
      reject();
    } else {
      fulfill(collection);
    }
  })
});
exports.CheckForValidCollection = checkForValidCollection;

// getBGGUser :: String -> Promise(JSONObject);
var getBGGUser = function(bggusername) {
  return new Promise(function(fulfill, reject) {
    bgg('user', {
      name: bggusername, guilds: 1, buddies: 1, hot: 1, top: 1, domain: 'boardgame'}).then(function(x) {
      if (R.isEmpty(x)) {
        reject(x);
      } else {
        fulfill(x);
      };
    });
  });
};
exports.GetBGGUser = getBGGUser;


// getUserBGGUserCollection :: String -> String -> Promise(JSONObject)
var getBGGUserCollection = function(bggusername) {
  return new Promise(function(fulfill, reject) {
    var checkUserCollection = checkForValidCollection(bggusername);
    bgg('collection', {username: bggusername, stats: 1}).then(checkUserCollection).then(fulfill).catch(
      (err) => {
        if (err.error == "precanceled") {
          setTimeout(() => fulfill(getBGGUserCollection(err.request.params.username)), 20000);
        } else {
          reject(err);
        }
      }
    );
  });
};
exports.GetBGGUserCollection = getBGGUserCollection;


// getBGGThingById :: Int -> Promise(JSONObject);
var getBGGThingById = function(id) {
  return new Promise(function(fulfill, reject) {
    bgg('thing', {id: id, stats: 1, videos: 1}).then(extractJSONItem).then(function(x) {
      if (R.isNil(x)) {
        reject(x);
      } else {
        fulfill(x);
      };
    });
  });
};
exports.GetBGGThingById = getBGGThingById;


// getBGGBoardGameById :: Int -> Promise(JSONObject);
var getBGGBoardGameById = function(id) {
  return new Promise(function(fulfill, reject) {
    bgg('thing', {type: 'boardgame', id: id}).then(extractJSONItem).then(function(x) {
      if (R.isNil(x)) {
        reject(x);
      } else {
        fulfill(x);
      };
    });
  });
};
exports.GetBGGBoardGameById = getBGGBoardGameById;

// unarraify :: Object.Array[x] -> Object.x;
var unarraify = R.curry(function(property, object) {
    try {
        xlens = R.lensProp(property);
        return R.over(xlens, R.head, object);
    } catch (err) {
        log.ConsoleLogObjectSection('error:unarrayify', err);
    }
});

// searchBggBoardGameByName :: String -> Promise(JSONObject);
var searchBGGBoardGameByName = function(name) {
    return bgg('search', {query: name, type: 'boardgame', exact: 0});
};
exports.SearchBGGBoardGameByName = searchBGGBoardGameByName;
