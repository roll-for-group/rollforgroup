var Promise = require('bluebird');
var R = require('ramda');
    chain   = R.chain,
    compose = R.compose,
    curry   = R.curry,
    dissoc = R.dissoc,
    isEmpty = R.isEmpty,
    isNil   = R.isNil,
    lift    = R.lift,
    filter  = R.filter,
    find    = R.find,
    map     = R.map,
    merge   = R.merge,
    objOf   = R.objOf,
    path    = R.path,
    prop    = R.prop
    propEq  = R.propEq;
var bgg = require('./bgg.js');
var log = require('./log.js');
var S = require('./lambda.js');

const Maybe     = require('data.maybe');
const Either    = require('data.either');
const Task      = require('data.task');

var scrub = compose(dissoc('_id'), dissoc('__v'), JSON.parse, JSON.stringify);

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// taskNothing => Task.of({})
const taskNothing = () => new Task((reject, resolve) => resolve({}));


// taskFindBoardgame :: Model boardgame -> Integer -> task Model boardgame
var taskFindBoardgame = R.curry(function(schemaBoardgame, id) {
  return new Task((reject, resolve) => 
    schemaBoardgame.findOne({id:id}).exec().then((m) => 
      isNil(m) 
        ? reject(404) 
        : resolve(m)).catch(reject)
  )
});
exports.TaskFindBoardgame = taskFindBoardgame;


// taskFindCountBoardgame :: (Model(boardgame) -> Integer) => Promise(Model(boardgame))
var taskFindCountBoardgame = R.curry(function(schemaBoardgame, id) {
  return new Task((reject, resolve) => schemaBoardgame.findOneAndUpdate({id:id}, {$inc:{updatecounter:1}}, {new:true}).exec().then((m) => isNil(m) 
    ? reject(404) 
    : resolve(m)).catch(reject))
});
exports.TaskFindCountBoardgame = taskFindCountBoardgame;


// readBoardgame :: (Model(boardgame) -> Integer) => Promise(Model(boardgame))
var readBoardgame = R.curry(function(schemaBoardgame, id) {
  return schemaBoardgame.find({id:id}).limit(15).exec();
});
exports.ReadBoardgame = readBoardgame;


// searchBoardgameName :: Schema(boardgame) -> String -> Promise(Model(boardgame))
var searchBoardgameName = R.curry(function(schemaBoardgame, gamename, withExpansions) {

  // findName :: s -> regex
  const findName = (gamename) => new RegExp('^' + gamename, "i");

	// queryNameString :: bool -> o
	const queryNameString = (searchExpansions) => searchExpansions
		? {"$or":[ {"type":{"$exists":false}}, {"type": "boardgame"}, {"type":"boardgameexpansion"} ]}
		: {"$or":[ {"type":{"$exists":false}}, {"type": "boardgame"} ]}

	return schemaBoardgame
    .find({'$and':[ {'$text':{$search: gamename, $diacriticSensitive: false }}, queryNameString(withExpansions) ]}, { score : { $meta: 'textScore' } })
    .sort({ score: {$meta: "textScore" }})
    .limit(10)
    .exec();

});
exports.SearchBoardgameName = searchBoardgameName;


// createBoardgame :: Schema(boardgame) -> Integer -> String -> JSONObject -> Promise(Model(boardgame))
var createBoardgame = R.curry(function(schemaBoardgame, id, name, cache) {
  var newBoardgame = new schemaBoardgame({
    id: id,
    name: name,
    bggcache: cache
  });
  return newBoardgame.save();
});
exports.CreateBoardgame = createBoardgame;


// deleteBoardgame :: Schema(boardgame) -> Integer -> Promise(Model(boardgame))
var deleteBoardgame = R.curry(function(schemaBoardgame, id) {
  return schemaBoardgame.remove({id:id});
});
exports.DeleteBoardgame = deleteBoardgame;


// deleteBoardgameByName :: Schema(boardgame) -> Integer -> Promise(Model(boardgame))
var deleteBoardgameByName = R.curry(function(schemaBoardgame, name) {
  return schemaBoardgame.remove({name:name});
});
exports.deleteBoardgameByName = deleteBoardgameByName;


// getBoardgameName :: JSONObject -> String
var getBoardgameName = function(nameobject) {

  var getName;
  var readName = R.curry(R.prop('name'));
  var readValue = R.curry(R.prop('value'));
  var checkNameForArray = R.compose(Array.isArray, readName);

  if (checkNameForArray(nameobject)) {
    var isPrimary = R.pathEq(['type'], 'primary');
    var filterPrimary = R.curry(R.filter(isPrimary));
    getName = R.compose(R.head, R.map(readValue), R.filter(isPrimary), readName);
  } else {
    getName = readName;
  };
  return getName(nameobject);

};
exports.GetBoardgameName = getBoardgameName;


// cacheReturnBoardgame :: (Schema(boardgame) -> n -> {o}) -> Promise(Model(boardgame));
var cacheReturnBoardgame = R.curry(function(schemaBoardgame, yearpublished, cacheObject) {

  return new Promise(function(fulfill, reject) {    
    var id = R.prop('objectid', cacheObject);
    readBoardgame(schemaBoardgame, id).then(function(x) {

      var gameId = R.prop('objectid');
      var buildGame = R.pick(['thumbnail', 'yearpublished', 'subtype', 'objecttype'], cacheObject);

      buildGame.id = gameId(cacheObject);
      buildGame.name = R.path(['name', 't'], cacheObject);
      buildGame.bggcache = cacheObject;
      buildGame.description = 'Pending Bgg Lookup...';
      buildGame.minplaytime = 30;
      buildGame.maxplaytime = 60;
      buildGame.minplayers = 2;
      buildGame.maxplayers = 4;

      if (x.length == 0) {

        var actify = R.compose(R.gte(1), R.subtract(yearpublished));
        buildGame.active = actify(buildGame.yearpublished);

        if (buildGame.active) {
          buildGame.activestatus = "active";
        } else {
          buildGame.activestatus = "inactive";
        }  
        var newBoardgame = new schemaBoardgame(buildGame);
        fulfill(newBoardgame.save());

      } else {

        var findUpdate = {id: gameId(cacheObject)};
        var updateGame = {$set: buildGame};
        fulfill(schemaBoardgame.update(findUpdate, updateGame, {upsert:true, new:true}).exec());

      };

    }, function(err){
      log.ConsoleLogObjectSection('error', x);

    });
  });
});
exports.CacheReturnBoardgame = cacheReturnBoardgame;


// cacheBoardgame :: (Schema(boardgame) -> n -> {o}) -> Promise(Model(boardgame));
var cacheBoardgame = R.curry(function(schemaBoardgame, yearpublished, cacheObject) {
  return new Promise(function(fulfill, reject) {

    var id = R.prop('objectid', cacheObject);
    readBoardgame(schemaBoardgame, id).then(function(x) {

      var gameId = R.prop('objectid');
      var buildGame = R.pick(['thumbnail', 'yearpublished', 'subtype', 'objecttype'], cacheObject);

      buildGame.id = gameId(cacheObject);
      buildGame.name = R.path(['name', 't'], cacheObject);
      buildGame.bggcache = cacheObject;
      buildGame.thumbnail = (typeof buildGame.thumbnail !== 'string')
        ? 'img/rfg-logo-whiteglow-64.png'
        : buildGame.thumbnail;

      buildGame.expansions =  S.prop('expansions', cacheObject).getOrElse([]);
      buildGame.videos =      S.prop('videos', cacheObject).getOrElse([]);

      if (x.length == 0) {

        buildGame.updatecache = true;
        buildGame.description = 'Pending Bgg Lookup...';
        buildGame.minplaytime = 30;
        buildGame.maxplaytime = 60;
        buildGame.minplayers = 2;
        buildGame.maxplayers = 4;

        var actify = R.compose(R.gte(1), R.subtract(yearpublished));
        buildGame.active = actify(buildGame.yearpublished);

        if (buildGame.active) {
          buildGame.activestatus = "active";
        } else {
          buildGame.activestatus = "inactive";
        }  

        var newBoardgame = new schemaBoardgame(buildGame);
        var respond = {ok:1, n:1, nModified:0};

        newBoardgame.save().then(function(y) {
          fulfill(respond);
        }).catch(function(x) {
          fulfill(respond);
        });

      } else {

        buildGame.updatecache = true;
        var findUpdate = {id: gameId(cacheObject)};
        var updateGame = {$set: buildGame};

        fulfill(schemaBoardgame.findOneAndUpdate(findUpdate, updateGame, {upsert:true, new:true}).exec());

      };

    }, function(err){
      log.ConsoleLogObjectSection('error', x);

    });
  });
});
exports.CacheBoardgame = cacheBoardgame;


// deincremet game matching details
var deincrementWantToPlay = R.curry(function(schemaBoardgame, gameName) {
  var conditions = {name: gameName}; 
  var update = {$inc: {'wanttoplay':-1}};
  return schemaBoardgame.findOneAndUpdate(conditions, update, {new:true}).exec();
});
exports.DeincrementWantToPlay = deincrementWantToPlay;


// incremet game matching details
var incrementWantToPlay = R.curry(function(schemaBoardgame, gameName) {
  var conditions = {name: gameName}; 
  var update = {$inc: {'wanttoplay':1}};
  return schemaBoardgame.findOneAndUpdate(conditions, update, {new:true}).exec();
});
exports.IncrementWantToPlay = incrementWantToPlay;


// getBoardgameOptionList :: Array[String]
var getBoardgameOptionList = function(schemaBoardgame) {
  return new Promise(function(fulfill, reject) {
    schemaBoardgame.find().lean().sort('name').exec().then(function(x){
      var pullIdAndNameFields = R.props(['id', 'name']);
      var buildObjectWithKeyValues = R.zipObj(['id', 'option']);
      var extractGames = R.compose(fulfill, R.map(buildObjectWithKeyValues), R.map(pullIdAndNameFields));
      var results = extractGames(x);
    });
  })
};
exports.GetBoardgameOptionList = getBoardgameOptionList;


// precisionRound :: n -> n -> n
const precisionRound = R.curry((precision, number) => {
  var factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
});
exports.precisionRound = precisionRound;


// maybeRating :: s -> o -> Maybe n
const maybeRating = R.curry((key, obj) => compose(
  map(precisionRound(2)),
  S.path(['statistics', 'ratings', key, 'value'])
)(obj));
exports.maybeRating = maybeRating;

// whenNanZero :: s -> s
const whenNanZero = (x) => (isNaN(x)) 
  ? '' 
  : x;

// maybeBoardGameItem :: a -> maybe o
const maybeBoardGameItem = (x) => Array.isArray(x)
  ? S.find(R.propEq('name', 'boardgame'), x)
  : Maybe.of(x)
;
exports.maybeBoardGameItem = maybeBoardGameItem;


// maybeRank :: o -> Maybe n
const maybeRank = (x) => S.path(['statistics', 'ratings', 'ranks', 'rank'], x)
  .chain(maybeBoardGameItem)
  .chain(S.prop('value'))
  .map(whenNanZero)
exports.maybeRank = maybeRank;

// getVotes :: [o] -> f(o) => maybe n
const getVotes = (key) => R.compose(
  chain(S.prop('numvotes')),
  S.find(R.propEq('value', key))
);
exports.getVotes = getVotes;

// convertObj :: o -> maybe o
const convertObj = (obj) => {

  // liftGt :: maybe n -> maybe n -> maybe b 
  const liftGt = R.lift(R.gt);

  return liftGt(getVotes('Best')(obj), getVotes('Recommended')(obj)).getOrElse('')
    ? liftGt(getVotes('Not Recommended')(obj), getVotes('Best')(obj)).getOrElse('')
      ? 'Not Recommended'
      : 'Best'
    : liftGt(getVotes('Not Recommended')(obj), getVotes('Recommended')(obj)).getOrElse('')
      ? 'Not Recommended'
      : 'Recommended';
};
exports.convertObj = convertObj;

// maybeValueVotes :: o -> n
const maybeValueVotes = compose(
  map(map(convertObj)),
  R.sequence(Maybe.of),
  chain(chain(S.prop('result'))),
  chain(S.prop('results')),
  chain(S.find(R.propEq('name', 'suggested_numplayers'))),
  S.prop('poll')
);
exports.maybeValueVotes = maybeValueVotes;

// maybePlayerVotes :: o -> n
const maybePlayerVotes = (x) => {

  // toString :: a -> s
  const toString = (x) => x.toString();

  return compose(
    map(map(toString)),
    R.sequence(Maybe.of),
    chain(chain(S.prop('numplayers'))),
    chain(S.prop('results')),
    chain(S.find(R.propEq('name', 'suggested_numplayers'))),
    S.prop('poll'))(x)
};
exports.maybePlayerVotes = maybePlayerVotes;

// maybePlayerCounts :: o -> Maybe [{o}]
const maybePlayerCounts = (g) => 
  Maybe.of(
    R.map(
      R.zipObj(['playercount', 'status']), 
        R.transpose([
          maybePlayerVotes(g).get(),
          maybeValueVotes(g).get()
        ]
      )
    )
  );
exports.maybePlayerCounts = maybePlayerCounts;

// createUpdateGameMongoSet :: [a] -> o
const createUpdateGameMongoSet = R.compose(
  objOf('$set'), 
  R.merge({updatecache: false}),
  R.zipObj([
    'type', 
    'description', 
    'minplaytime', 
    'maxplaytime', 
    'minplayers', 
    'maxplayers', 
    'videos',
    'avgrating', 
    'weight', 
    'bggrank', 
    'playercounts',
    'family',
    'categories',
    'mechanics',
    'expansions',
    'artists',
    'publishers',
    'designers',
    'implementation'
  ])
);
exports.createUpdateGameMongoSet = createUpdateGameMongoSet;

// getLinkValues :: s -> o -> [a]
const getLinkValues = (key, obj) => R.compose(
  R.sequence(Maybe.of),
  chain(map(S.prop('value'))),
  chain(S.filter(R.propEq('type', key))),
  S.prop('link')
)(obj);
exports.getLinkValues = getLinkValues;

// getLinkObjects :: s -> o -> [o]
const getLinkObjects = (key, obj) => R.compose(
  map(map(R.pick(['value', 'id']))),
  chain(S.filter(R.propEq('type', key))),
  S.prop('link')
)(obj);
exports.getLinkObjects = getLinkObjects;


// updateGameInfo :: Schema(boardgame) -> id -> Model(boardgame)
const updateGameInfo = curry(function(boardgame, id) {

  // idOf :: {o} -> {o}
  const idOf = objOf('id'); 

  // blankIfEmpty :: a -> s
  const blankIfEmpty = (a) => (R.isEmpty(a)) 
    ? ''
    : a;

  return new Promise(function(fulfill, reject) {
    bgg.GetBGGThingById(id).then(function(g) {

      // safeSet :: o -> o
      const safeSet = (g) => createUpdateGameMongoSet([
        S.prop('type',        g).getOrElse('undefined'), 
        S.prop('description', g).map(blankIfEmpty).getOrElse('Pending Bgg Lookup...'), 
        S.path(['minplaytime',  'value'], g).getOrElse(30), 
        S.path(['maxplaytime',  'value'], g).getOrElse(60), 
        S.path(['minplayers',   'value'], g).getOrElse(1), 
        S.path(['maxplayers',   'value'], g).getOrElse(4),
        S.path(['videos',       'video'], g).getOrElse([]),
        maybeRating('average', g).getOrElse(0),
        maybeRating('averageweight', g).getOrElse(2.5),
        maybeRank(g).getOrElse(''),
        maybePlayerCounts(g).getOrElse([]),
        getLinkValues('boardgamefamily', g).getOrElse([]),
        getLinkValues('boardgamecategory', g).getOrElse([]),
        getLinkValues('boardgamemechanic', g).getOrElse([]),
        getLinkObjects('boardgameexpansion', g).getOrElse([]),
        getLinkValues('boardgameartist', g).getOrElse([]),
        getLinkValues('boardgamepublisher', g).getOrElse([]),
        getLinkValues('boardgamedesigner', g).getOrElse([]),
        getLinkObjects('boardgameimplementation', g).getOrElse([]),
      ]);

      // buildSet :: o -> o
      const buildSet = (g) => (g.type === "boardgame" || g.type === "boardgameexpansion")
        ? safeSet(g)
        : {'$set':{'type': g.type, updatecache: false}};

      (S.prop('type', g).isJust) 
        ? fulfill(boardgame.findOneAndUpdate(idOf(id), buildSet(g), {new:true}).exec())
        : reject("error retreiving game" + id);

    }).catch((err) => fulfill(boardgame.findOneAndUpdate(idOf(id), {'$set':{'type': 'undefined', 'updatecache': false}}, {new:true}).exec()))
  });
});
exports.UpdateGameInfo = updateGameInfo


// updateNewGames :: (Schema(boardgame) -> limit) => Promise([Model(games)]) 
const updateNewGames = function(schemaBoardgame, limit) {
  return new Promise(function(fulfill, reject) {
    schemaBoardgame
      .find({updatecache: true})
      .limit(limit)
      .exec()
      .then(function(g) {
        Promise.all(g.map(prop('id')).map(updateGameInfo(schemaBoardgame))).then(fulfill).catch(reject);
      });
  });
}
exports.UpdateNewGames = updateNewGames;


// updateFailLookupGames :: (Schema(boardgame) -> limit) => Promise([Model(games)]) 
const updateFailLookupGames = function(schemaBoardgame, limit) {
  return new Promise(function(fulfill, reject) {
    schemaBoardgame.find({description: "Pending Bgg Lookup...", type: "boardgame"}).limit(limit).exec().then(function(g) {
      Promise.all(g.map(prop('id')).map(updateGameInfo(schemaBoardgame))).then(fulfill).catch(reject);
    });
  });
}
exports.UpdateFailLookupGames = updateFailLookupGames;


// pickGameFields :: pickGameFields => {o} -> {o}
const pickGameFields = R.pick([
  'thumbnail',
  'yearpublished',
  'id',
  'name',
  'description',
  'minplaytime',
  'maxplaytime',
  'minplayers',
  'maxplayers',
  'bggrank',
  'weight',
  'avgrating',
  'playercounts'
]);
exports.pickGameFields = pickGameFields;


// taskCheckExpansionData :: {o} -> Task {o}
const taskCheckExpansionData = R.curry(function(boardgame, expObject) { 
  return new Task((reject, resolve) => {
    S.prop('id', expObject).map(taskFindBoardgame(boardgame))
      .getOrElse(taskNothing())
      .fork(
        err => {
          resolve({});
        },
        data => {
          resolve(pickGameFields(data));
        }
      )

  });
});
exports.taskCheckExpansionData = taskCheckExpansionData;


// getExpansions :: Schema boardgame -> int -> [obj]
const getExpansions = R.curry(function(boardgame, id) {

  // safeExpansions :: {o} -> [{o}] 
  const safeExpansions = (gameObj) => S
    .prop('expansions', gameObj)
    .getOrElse([]);

  // filterValidData :: [a] => [a]
  const filterValidData = (a) => R.filter(R.has('id'), a);
  
  return new Task ((reject, resolve) => {
    taskFindBoardgame(boardgame, id).fork(
      err => reject(err),
      data => {
        R.sequence(Task.of, safeExpansions(data)
          .map(taskCheckExpansionData(boardgame))
        )
          .fork(
            err => reject(err),
            dataUpdated => {
              resolve(R.merge(pickGameFields(data), {expansions: filterValidData(dataUpdated)}));
            }
          )
      }
    );
  });
});
exports.getExpansions = getExpansions;


// getVideos :: Schema boardgame -> int -> [obj]
const getVideos = R.curry(function(boardgame, id) {

  // safeVideos :: {o} -> [{o}] 
  const safeVideos = (gameObj) => S
    .prop('videos', gameObj)
    .getOrElse([]);

  // filterValidData :: [a] => [a]
  const filterValidData = (a) => R.filter(R.has('id'), a);
  
  return new Task ((reject, resolve) => {
    taskFindBoardgame(boardgame, id).fork(
      err => reject(err),
      data => {
        resolve(R.merge(pickGameFields(data), {videos: safeVideos(data)}));

      }
    );
  });
});
exports.getVideos = getVideos;
