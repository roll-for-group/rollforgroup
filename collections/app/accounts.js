var Promise = require('bluebird');
var R = require('ramda');

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task');

var bgg     = require('./bgg.js');
var util    = require('util');
var log     = require('./log.js');
var S = require('./lambda.js');

const shortid = require('shortid');
const uuid = require('node-uuid');

const append    = R.append, 
    chain       = R.chain,
    concat      = R.concat,
    compose     = R.compose,
    composeP    = R.composeP,
    curry       = R.curry,
    filter      = R.filter,
    find        = R.find,
    fromPairs   = R.fromPairs,
    groupBy     = R.groupBy,
    head        = R.head,
    isEmpty     = R.isEmpty,
    isNil       = R.isNil,
    join        = R.join,
    last        = R.last,
    lift        = R.lift,
    map         = R.map,
    merge       = R.merge,
    not         = R.not,
    nth         = R.nth,
    objOf       = R.objOf,
    of          = R.of,
    path        = R.path,
    pick        = R.pick,
    prop        = R.prop,
    propEq      = R.propEq,
    reject      = R.reject,
    reverse     = R.reverse,
    sortBy      = R.sortBy,
    split       = R.split,
    take        = R.take,
    toPairs     = R.toPairs,
    values      = R.values,
    zipObj      = R.zipObj;

var schemaBoardgame = require('./boardgame.js');
var boardgames = require('./boardgames.js');

var expresshelper = require('./expresshelper.js');

var moment = require('moment');

var fs = require('fs');
const S3FS = require('s3fs');

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

const notEmpty = compose(not, isNil);
const wipeSchema = R.compose(JSON.parse, JSON.stringify);

// safeProp :: (a -> b) => Maybe(b[a])
const safeProp = curry((x, o) => (isEmpty(o) || isNil(o) || isNil(o[x])) ? Maybe.Nothing() : Maybe.of(o[x]));

const safePath = curry((arraypath, a) => ((path(arraypath, a) == undefined)) ? Maybe.Nothing() : Maybe.of(path(arraypath, a))); 

const eitherProp = curry((propname, a) => ((prop(propname, a) == undefined)) ? Either.Left(405) : Either.Right(prop(propname, a))); 

const eitherPath = curry((arraypath, a) => ((path(arraypath, a) == undefined)) ? Either.Left(405) : Either.Right(path(arraypath, a))); 

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

const safeHead   = (x) => Maybe.of(x[0]);
const eitherHead = (x) => (x.length == 0) ? Either.Left(401) : Either.Right(x[0]);

    // Notes - add tests to account for the following:
    // HostGame = function()
    // - when public game
    // -    confirm purchasing user has a ticket 
    // -    hold ticket 
    // -    confirm tickets are held 

    // JoinGame = function()
    // - when join game
    // -    confirm purchasing user has a ticket 
    // -    hold ticket 
    // -    confirm tickets are held 
    
    // ApproveGame = function()
    // - when approve game
    // -    remove hold ticket 
    // -    create purchase 
    // -    create ledger

    // - Disband or LeaveGame
    // - unhold ticket
    // -    confirm ticket is no longer held
    // -    confirm ticket is now available


// sortedValues :: [a] => {o}
const sortedValues = compose(objOf('collection'), groupBy(prop('activestatus')), reverse, sortBy(prop('yearpublished')), filter(path(['matching', 'own'])), filter(notEmpty));
exports.SortedValues = sortedValues;

// checkValidEmail :: model(account, String) -> Promise(String)
var checkValidEmail = curry(function(account, email, objectkey) {
  return new Promise(function(fulfill, reject) {
    readVerifyAccountFromEmail(account, email, objectkey).then(function(a) {

      const profilise = objOf('profile')
      const eitherAccount = (x) => (x.length == 0)
          ? Either.Left({error: {code: 401, description: 'invalid user profile'}}) 
          : Either.Right(x[0]);

      eitherAccount(a).map(pick(['email'])).map(profilise).map(fulfill).leftMap(reject); 

    }).catch(function(err) {
      reject (err);

    });
  });
});
exports.CheckValidEmail = checkValidEmail;


// updateMatchingOwn :: Schema(Account) -> String -> Integer -> Boolean -> Promise(Update)
var updateMatchingOwn = R.curry(function(userSchema, email, gameId, value) {
    return userSchema.findOneAndUpdate({'email': email, 'games.id': gameId}, {$set: {'games.$.matching.own': value}}, {new:true}).exec();
});
exports.UpdateMatchingOwn = updateMatchingOwn;


// updateMatchingTeach :: Schema(Account) -> String -> Integer -> Boolean -> Promise(Update)
var updateMatchingTeach = R.curry(function(userSchema, email, gameId, value) {
    return userSchema.findOneAndUpdate({'email': email, 'games.id': gameId}, {$set: {'games.$.matching.teach': value}}, {new:true}).exec();
});
exports.UpdateMatchingTeach = updateMatchingTeach;


// associateGame :: Schema(Account) -> String -> Integer -> Promise(Update)
var associateGame = R.curry(function(userSchema, email, gameName, matchingObj) {
  return new Promise(function(fulfill, reject) {
    var pickGame = R.pick(['id', 'thumbnail', 'yearpublished', 'subtype', 'objecttype', 'name', 'bggcache']);
    var findgame = (boardgame, game) => boardgame.find({name: game}).exec();
    var ripGame = R.composeP(pickGame, R.head, findgame);
    ripGame(schemaBoardgame, gameName).then(function(userGame) {
      userGame.matching = matchingObj;
      fulfill(userSchema.findOneAndUpdate({'email': email}, {$push: {'games': userGame}}, {new:true}).exec());
    });
  });
});
exports.AssociateGame = associateGame;


// updateMatchingAvoid :: Schema(Account) -> String -> String -> Boolean -> Promise(Update)
var updateMatchingAvoid = R.curry(function(userSchema, email, gameName, value) {

  var updateQuery = objOf('games.$.matching.avoid', value);
  var newObj = {avoid: value, own: false, wanttoplay: false};
  return updateMatching(userSchema, email, gameName, updateQuery, newObj);

});
exports.UpdateMatchingAvoid = updateMatchingAvoid;


// updateMatchingWantToPlay :: Schema(Account) -> String -> String -> Integer -> Promise(Update)
var updateMatchingWantToPlay = R.curry(function(userSchema, email, gameName, value) {

  var updateQuery = objOf('games.$.matching.wanttoplay', value);
  var newObj = {avoid: false, own: false, wanttoplay: value};
  return updateMatching(userSchema, email, gameName, updateQuery, newObj);

});
exports.UpdateMatchingWantToPlay = updateMatchingWantToPlay;

// updateMatchingOwn :: Schema(Account) -> String -> String -> Integer -> Promise(Update)
var updateMatchingOwnName = R.curry(function(userSchema, email, gameName, value) {

  var updateQuery = objOf('games.$.matching.own', value);
  var newObj = {avoid: false, own: value, wanttoplay: false};
  return updateMatching(userSchema, email, gameName, updateQuery, newObj);

});
exports.UpdateMatchingOwnName = updateMatchingOwnName;


// updateProfile :: Schema(Account) -> String -> Object -> Promise(Update)
var updateProfile = R.curry(function(userSchema, email, obj) {

  var createSet = (a) => ["profile." + a[0], a[1]];
  var createQueryObj = compose(fromPairs, map(createSet), toPairs);
  var updateQuery = (obj) => zipObj(['$set'], [createQueryObj(obj)]);
  return userSchema.findOneAndUpdate({email: email}, updateQuery(obj), {upsert:true, new:true}).exec(); 

});
exports.UpdateProfile = updateProfile;


// updateSchedule :: Schema(Account) -> String -> String -> String -> Promise(Update)
var updateSchedule = R.curry(function(userSchema, email, schedulekey, value) {

  return updateProfile(userSchema, email, zipObj([schedulekey], [value]));

});
exports.UpdateSchedule = updateSchedule;


// updateMatching :: Schema(Account) -> String -> String -> String -> JSONObject -> Promise(Update)
var updateMatching = R.curry(function(userSchema, email, gameName, updateQuery, addObject) {
  return new Promise(function(fulfill, reject) {

    var modifiedCount = R.prop('nModified');
    userSchema.findOneAndUpdate({'email': email, 'games.name': gameName}, {$set: updateQuery}).exec().then(function(x) {
      if (isEmpty(x)) {
        fullfill({nModified:1, nMatched:1})
      } else {
        fulfill(associateGame(userSchema, email, gameName, addObject));
      }
    });
  });
});


// respawnAccountObject :: Schema(Account) -> String -> Model(Account)
var respawnAccountObject = R.curry(function(userSchema, userEmail, userObject) {
  return new Promise (function(fulfill, reject) {
    deleteAccount(userSchema, userEmail).then(function(d) {
      fulfill(createAccountObject(userSchema, userEmail, userObject));
    });
  });
});
exports.RespawnAccountObject = respawnAccountObject;


// createAccountObject :: Schema(Account) -> String -> Model(Account)
var createAccountObject = R.curry(function(userSchema, userEmail, userObject) {

  var defaultAccount = {
    email: userEmail,
    profile: {
      email:      userEmail,
      Monday:     false,
      Tuesday:    false,
      Wednesday:  false,
      Thursday:   false,
      Friday:     false,
      Saturday:   false,
      Sunday:     false
    }
  };

  var mergedAccount = R.merge(defaultAccount, userObject);
  var newAccount = new userSchema(mergedAccount);
  return newAccount.save();

});
exports.CreateAccountObject = createAccountObject;


// createDummyAccount :: Schema(Account) -> String -> String -> String -> Model(Account)
var createDummyAccount = R.curry(function(userSchema, userName, userEmail, userBgg) {

    var userObject = {
        nickname: userName,
        profile: {
            name:       userName,
            email:      userEmail,
            bggname:    userBgg,
            Monday:     false,
            Tuesday:    false,
            Wednesday:  false,
            Thursday:   false,
            Friday:     false,
            Saturday:   false,
            Sunday:     true,
            biography:  'This is text about me',
            job:        'Test job',
            sundaystarthour:    3,
            sundaystartminute:  0,
            sundayendhour:      15,
            sundayendminute:    45,
            hostalcohol:        1,
            hostsmoking:        2,
            notifyplayers:      true,
            notifyevents:       true,
            notifyupdates:      true
        }
    };
    return createAccountObject(userSchema, userEmail, userObject);

});
exports.CreateDummyAccount = createDummyAccount;


// deleteAccount :: Schema(Account) -> String -> Schema(Account)
var deleteAccount = R.curry(function(userSchema, userEmail) {
    return userSchema.remove({email: userEmail});
});
exports.DeleteAccount = deleteAccount;


// readCollection :: Model(Account) -> Promise(JSONObject)
var readCollection = function(userSchema) {
  return new Promise(function(fulfill, reject) {

    fulfill(sortedValues([]));

    /*
    Promise.all(map(mergeGameMatchingProps, []))
      .then(data => fulfill(sortedValues(data)))
      .catch(reject);
    */

  });
};
exports.ReadCollection = readCollection;


// mergeGameMatchingProps :: Model(schemaBoardGame) -> Promise(schemaBoardgame)
var mergeGameMatchingProps = function(schemaGame) {
  return new Promise(function(fulfill, reject) {
    try {
      const id = prop('id');
      const jsonGame = wipeSchema(schemaGame);
      const keys = ['id', 'active', 'activestatus', 'wanttoplay'];
      const mergeMainGame = composeP(head, map(merge(jsonGame)), map(pick(keys)), boardgames.ReadBoardgame(schemaBoardgame));
      mergeMainGame(id(schemaGame)).then(fulfill).catch(reject);
    } catch (err) {
      reject(err);
    };
  })
};
exports.MergeGameMatchingProps = mergeGameMatchingProps;


// registerAccount :: Account -> String -> String -> (Promise -> Schema);
var registerAccount = R.curry(function(UserSchema, user_email, user_password) {
	return new Promise(function (fulfill, reject) {
		UserSchema.register(new UserSchema({email: user_email}), user_password, function (err, useraccount) {
			if (err) {
				reject(err);
			}
			fulfill(useraccount);
		});
	})
});
exports.registerAccount = registerAccount;


// verifyAccount :: Account -> String -> (Promise -> String)
var verifyAccount = R.curry(function(UserSchema, authToken) {
	return new Promise(function (fulfill, reject) {
		UserSchema.verifyEmail(authToken, function(err, existingAuthToken) {
			if (err)
				reject(err);

			if (existingAuthToken.isAuthenticated) {
				fulfill(existingAuthToken)
			} else {
				reject(existingAuthToken);
			}
		});
	});
});
exports.verifyAccount = verifyAccount;

// readAccountFromEmail :: String -> Model(Account) -> Promise(Account)
var readAccountFromEmail = R.curry(function(account, email) {
  return account.find({'email':email}).exec();
});
exports.ReadAccountFromEmail = readAccountFromEmail;

// readAnAccountFromEmail :: String -> Model(Account) -> Promise(Account)
var readAnAccountFromEmail = R.curry(function(account, email) {
  return account.findOne({'email':email}).exec();
});
exports.ReadAnAccountFromEmail = readAnAccountFromEmail;

// readAnAccountFromPageSlug :: String -> Model(Account) -> Promise(Account)
var readAnAccountFromPageSlug = R.curry(function(account, pageslug) {
  return account.findOne({'pageslug':pageslug}).exec();
});
exports.ReadAnAccountFromPageSlug = readAnAccountFromPageSlug;

// TaskCheckIcalKey :: Model(Account) -> s -> s -> Task(Account)
var taskCheckIcalKey = R.curry((account, email, icalkey) => new Task((reject, resolve) => account.findOneAndUpdate({'email':email, 'icalkey':icalkey, 'premium.isactive': true}, {$inc: {icalviewcount: 1}}, {new:true}).exec().then((x) => {
        if (isNil(x)) {
            reject('no user found');
        } else {
            resolve(x);
        }
    }
).catch(reject)));
exports.TaskCheckIcalKey = taskCheckIcalKey;

// readVerifyAccountFromEmail :: String -> (Model(Account) -> e -> u) => Promise(Account)
var readVerifyAccountFromEmail = R.curry(function(account, email, key) {
    return account.find({'email':email, 'key':key}).exec();
});
exports.ReadVerifyAccountFromEmail = readVerifyAccountFromEmail;

// taskVerifyAnAccountFromEmailMaybe :: model account -> s -> s -> task maybe account
const taskVerifyAnAccountFromEmail = R.curry(function(account, email, key) {
  return new Task((reject, resolve) => account.findOne({'email':email, 'key':key}).exec().then((x) => 
    isNil(x) 
      ? reject(404) 
      : resolve(x)).catch(reject)
  );
});
exports.TaskVerifyAnAccountFromEmail = taskVerifyAnAccountFromEmail;


// CreateSaveRelationships :: Model(account) -> String -> String -> [String] -> String)
var createSaveRelationships = function(account, emailfrom, emailto, tags) {
	return new Promise(function (fulfill, reject) {
		// save the form data
        var assignRelationshipToUser = createRelationship(emailto, tags);
        var createSaveRelationshipFromUser = R.composeP(fulfill, R.head, R.map(assignRelationshipToUser), readAccountFromEmail);
        createSaveRelationshipFromUser(account, emailfrom);
	})
};
exports.CreateSaveRelationships = createSaveRelationships;


// createRelationship :: String -> [String] -> Model(account) -> Model(account);
var createRelationship = R.curry(function(relationshipEmailTo, relationshipTags, user) {
    user.relationship = createRelationships(relationshipEmailTo, relationshipTags);

    return user.save();
});


// createRelationships :: [String] -> [String] -> [JSONObject]
var createRelationships = function(emailto, tags) {
	var createRelationshipToEmails = createRelationshipObject(tags);

	return R.map(createRelationshipToEmails, emailto);
};
exports.CreateRelationships = createRelationships;


// createRelationshipObject :: [String] -> String -> JSONObject
var createRelationshipObject = R.curry(function(tags, emailto) {
	var newRelationship = {
		"email": emailto,
		"rating": 1,
		"gameplays": 0,
		"complaints": 0,
		"tags" : [tags]
	};

	return newRelationship;
});


// cacheBggUserInfo :: Model(User) -> JSONObject -> Model(User)
var cacheBggUserInfo = R.curry(function(account, bggUserData) {
  var updatedDate = new Date();
  if (R.has('user', bggUserData)) {
    account.bgg.cache.user = bggUserData.user;
    account.profile.bggname = bggUserData.user.name;
    account.bgg.lastupdated = updatedDate;
  }
  return account.save();
});
exports.CacheBggUserInfo = cacheBggUserInfo;


// updateBoardGameName :: Model(boardgames) -> JSONObject -> JSONObject
var updateBoardGameName = R.curry(function(object) {

  var newObject = R.pick(['numplays', 'thumbnail', 'image', 'yearpublished', 'minplaytime', 'maxplaytime'], object);

  newObject.id = object.objectid;
  newObject.name = object.name.t;
  newObject.bggcache = object;

  var matchingobj = {
    own: object.status.own,
    wanttoplay: object.status.wanttoplay,
    wishlist:   object.status.wishlist,
    wanttobuy:  object.status.wanttobuy,
    fortrade:   object.status.fortrade,
    want:       object.status.want
  };
  newObject.matching = matchingobj;

  return newObject;

});
exports.UpdateBoardGameName = updateBoardGameName;


// cacheBggCollection :: Model(User) -> JSONObject -> Model(User)
var cacheBggCollection = R.curry(function(account, bggUserCollection) {

	return new Promise(function(fulfill, reject) {

    if (isEmpty(bggUserCollection)) {
      reject('no collection');

    } else if(bggUserCollection.hasOwnProperty('errors')) {
      reject(bggUserCollection.errors);

    } else if(R.path(['items', 'totalitems'], bggUserCollection) == 0) {
      reject('no collection');

    } else {

      var pathToItem = R.path(['items', 'item']);

      var prepareItems = R.compose(R.map(updateBoardGameName), bgg.ReplaceTDollar, pathToItem);

      var boardGameList = prepareItems(bggUserCollection);

      // findById :: [{g}] -> n => {g}
      const findById = curry((list, id) => R.find(R.propEq('id', id), list));

      // mergeGameValue :: p -> {g} => {o}
      const mergeGameValue = curry((lookupgames, property, game) => merge(game, buildProp(lookupgames, property)(game).getOrElse({})));

      // buildProp :: s => (n => Maybe({g}))
      const buildProp = curry((lookupgames, property) => compose(map(objOf(property)), chain(safeProp(property)), map(findById(lookupgames)), safeProp('id')));

      // updateExtendedGameData :: [{e}] -> {g} => {o}
      const updateExtendedGameData = curry((lookupgames, game) => {

        // mergeValue
        const mergeValue = mergeGameValue(lookupgames)

        // updateGame :: {g} => {o}
        const updateGame = compose(mergeValue('maxplayers'), mergeValue('minplayers'), mergeValue('maxplaytime'), mergeValue('minplaytime'));

        return updateGame(game);

      });

      // removeDuplicateGames :: [{g}] => [{o}]
      const removeDuplicateGames = (lookupgames) => compose(map(updateExtendedGameData(lookupgames)), map(head), values, groupBy(R.prop('id')));

      var getId = R.curry(R.prop('id'));
      var getBggCache = R.prop('bggcache');
      var currentyear = new Date().getFullYear();
      var cacheGames = boardgames.CacheBoardgame(schemaBoardgame, currentyear);

      var writeBoardGameList = R.compose(Promise.all, R.map(cacheGames), R.map(getBggCache));

      var updatedDate = new Date();

      writeBoardGameList(boardGameList).then((x) => {
        account.games = removeDuplicateGames(x)(boardGameList); 
        account.bgg.lastupdated = updatedDate;
        fulfill(account.save());
      });

    }
	})
});
exports.CacheBggCollection = cacheBggCollection;


// importBggUserToAccount :: Model(User) -> String -> Model(User)
var importBggUserToAccount = R.curry(function(account, bggUserName) {
  return new Promise(function(fulfill, reject) {

    var addBggUserData = cacheBggUserInfo(account);
    var addBggCollection = cacheBggCollection(account);
    var lookupUserData = R.composeP(addBggUserData, bgg.GetBGGUser);
    var lookupCollection = R.composeP(addBggCollection, bgg.GetBGGUserCollection);

    var parallelLookup = [ 
      lookupUserData(bggUserName), 
      lookupCollection(bggUserName) 
    ];

    if (isEmpty(bggUserName)) {
      reject({error: {code:506, description:'bgg user not provided'}});
    } else {
      Promise.all(parallelLookup).then(
        function(x) {
            fulfill(account);
        },
        function(err) {
            reject(err);
        }
      );
    }
  })
});
exports.ImportBggUserToAccount = importBggUserToAccount;


// buildNavbars :: ModelAccount -> String -> JSONObject
var buildNavbars = R.curry(function(account, email) {
    return new Promise(function(fulfill, reject) {

        // readAnAcount
        readAnAccountFromEmail(account, email).then(function(x) {
            fulfill ((isNil(x) || isEmpty(x)) ? {} : buildNavObject(x));
        });

    }); 
});
exports.BuildNavbars = buildNavbars;


// buildNavObject :: [ModelAccount] => JSONObject
var buildNavObject = R.curry(function(accountObject) {
    
  var buildImg = (iclass, src) => R.zipObj(['class', 'src'], [iclass, src]);

  var navbars = {
    user: {},
    alerts: [],
    messages: [],
    headerbuttons: [{   
      button: {
        type: "button",
        class: "btn btn-default btn-lg card-2",
        id: "hostgame",
        text: " Host Event",
        img: buildImg("brandlogo16image", "img/rfg-logo-whiteglow-16.png") 
      }   
    }]
  };

  navbars.user = R.merge(expresshelper.LookupUsername(accountObject), {tickets: accountObject.tickets});

  return navbars;

});
exports.BuildNavObject = buildNavObject;

// unlinkAvatar :: nconf -> Schema(account) -> String
const unlinkAvatar = curry(function(nconf, account, email) {

    return new Promise(function(fulfill, reject) {
        readAccountFromEmail(account, email).then(function(x) {

            if (x[0].profile.avataricontype == 'facebook') {
                fulfill(x[0]);

            } else if (nconf.get('upload:type') == 'file') {

                const unlinkit  = (file) => fs.unlink(file, err => Maybe.Nothing()); 
                const unlinkfile = compose(map(unlinkit), map(join('')), map(append(R.__, './app/public/')), chain(safePath(['profile', 'avataricon'])), map(head));

                unlinkfile(Maybe.of(x));
                account.findOneAndUpdate({'email': email}, {$set: {'profile.avataricon': undefined, 'profile.avataricontype':undefined}}, {new:true}).exec().then(fulfill).catch(reject);

            } else if (nconf.get('upload:type') == 's3') {

                const bucketPath = nconf.get('upload:bucketPath');
                const s3Options = nconf.get('upload:s3Options');
                var fsImpl = new S3FS(bucketPath, s3Options);

                const unlinkS3  = curry((s3fsImp, file) => { 
                    s3fsImp.unlink(file, err => Maybe.Nothing()); 
                });

                const unlinkS3file = compose(map(unlinkS3(fsImpl)), chain(safePath(['profile', 'avataricon'])), map(head));
                unlinkS3file(Maybe.of(x));

                account.findOneAndUpdate({'email': email}, {$set: {'profile.avataricon': undefined, 'profile.avataricontype':undefined}}, {new:true}).exec().then(fulfill).catch(reject);

            };
        }).catch(reject);
    });

});
exports.UnlinkAvatar = unlinkAvatar;


// UpdateFacebookPage :: (nconf -> Schema(a) -> e) => i
const updateFacebookPage = curry(function(nconf, account, email) {
  return new Promise(function(fulfill, reject) {

    const getFbPicUrl = (id) => join('', ['https://graph.facebook.com/v2.8/', id ,'/picture?type=large']);

    const findURL = compose(map(getFbPicUrl), eitherPath(['facebook', 'id']));

    unlinkAvatar(nconf, account, email).then(a => {
      findURL(a).map(
        icon => {
          account.findOneAndUpdate({'email': email}, {$set: {'profile.avataricon': icon, 'proifle.avataricontype': 'facebook'}}, {new:true}).exec().then(fulfill).catch(500); 
        }
      ).leftMap(reject);
    }).catch(err => reject);

  });
});
exports.UpdateFacebookPage = updateFacebookPage

// findBoargamePlayersByDistance :: schemaAccount -> Number -> Number -> Number -> [Model(Account)]
var findBoardgamePlayersByDistance = curry(function(matchAccount, distance, lat, lng) {
  return findBoardgamePlayersByDistanceCoords(matchAccount, distance, [lng, lat]);
});
exports.FindBoardgamePlayersByDistance = findBoardgamePlayersByDistance;

// findBoargamePlayersByDistanceCoords :: schemaAccount -> Number -> [Number, Number] -> [Model(Account)]
var findBoardgamePlayersByDistanceCoords = R.curry((matchAccount, distance, coords) => 
  new Promise (function(fulfill, reject) {
    var geofind = {
      'profile.localarea.loc': {
        $near: {
          $geometry: {
            type: "Point" ,
            coordinates: coords 
          },
          $maxDistance: distance,
          $minDistance: 0 
        }
      }
    };

    const findPlayers = (searchquery) => 
      matchAccount
        .find(searchquery)
        .sort({activelast:-1})
        .limit(300)
        .exec();

    const measureDistance = R.composeP(fulfill, findPlayers);
    measureDistance(geofind);
  })
);
exports.FindBoardgamePlayersByDistanceCoords = findBoardgamePlayersByDistanceCoords;


// findPlayersNearby :: (schemaAccount -> String -> d) => [Model(Account)]
var findPlayersNearby = curry(function(accountSchema, userEmail, distance) {
  return new Promise (function(fulfill, reject) {

    var findbydistance = findBoardgamePlayersByDistance(accountSchema, distance, R.__, R.__);
    var findgamesnearby = R.composeP(fulfill, R.converge(findbydistance, [nth(1), nth(0)]), R.path(['profile', 'localarea', 'loc', 'coordinates']), head, readAccountFromEmail(accountSchema));
    findgamesnearby(userEmail);

  });
});
exports.FindPlayersNearby = findPlayersNearby;

// calculateLevel :: n => m
const calculateLevel = function(xp) {

  var checklevel = 0;
  var nextxpbreak = 0;
  var xpinterval = 50;

  while (xp >= nextxpbreak) {
    checklevel ++;
    xpinterval = xpinterval * 1.1;
    nextxpbreak = nextxpbreak + xpinterval;
  };

  return checklevel -1;

};
exports.CalculateLevel = calculateLevel

// calclevel :: s -> ({o} => Maybe(n))
const objXpToLevel = (propLevel) => compose(map(calculateLevel), safePath(['profile', propLevel]));
exports.ObjXpToLevel = objXpToLevel;

// taskUpdateLevel :: e -> n -> m => Task([Schema(account)])
const taskUpdateLevel = curry((account, email, plevel, hlevel) => new Task((rejectb, resolve) => account.findOneAndUpdate({email:email}, {$set: {"profile.playlevel": plevel, "profile.hostlevel":hlevel}}, {new:true}).exec().then(resolve).catch(rejectb)));
exports.TaskUpdateLevel = taskUpdateLevel;

// checkLevelFreeGem :: n1 -> n2 => c
const checkLevelFreeGem = curry((levelfrom, levelto) => {
    var gemcount = [];
    for (i=levelfrom; i<=levelto; i++) {
        if (i != levelfrom) 
            if (R.modulo(i, 5) == 0)
                gemcount.push(i);
    } 
    return gemcount;
});
exports.CheckLevelFreeGem = checkLevelFreeGem;

// taskGenerateLevelUpGem :: Model(account) -> e -> n -> Task({n})
const taskGenerateLevelUpGem = curry((account, email, level) => new Task((reject, resolve) => account.findOneAndUpdate({email: email}, {$inc: {'premium.creditsshare': 1}, $push: {'premium.creditlog': {from: 'rollforgroup', to: email, date: new Date(), description:'levelup-' + level}}}, {new:true}).exec().then(resolve).catch(reject)));
exports.TaskGenerateLevelUpGem = taskGenerateLevelUpGem;

// taskAddGameExperience :: (Schema(account) -> s -> bool -> n) => Model(account)
const taskAddGameExperience = curry(function(account, email, ishost, xppoints) { 

  // createUpdateQuery :: b -> n => {u}
  const createUpdateQuery = curry((host, xp) => host 
    ? {$inc: {"profile.playxp": xp, "profile.hostxp": xp}} 
    : {$inc: {"profile.playxp": xp}} );

  // readHostLevel :: {o} => n
  const readHostLevel = (x) => safePath(['profile', 'hostlevel'], x).getOrElse(0);

  // readHostXp :: {o} => n
  const readHostXp = (x) => safePath(['profile', 'hostxp'], x).getOrElse(0);

  // nextLevel :: {o} => n
  const nextLevel = compose(calculateLevel, readHostXp);

  // udpateGems :: Model(account) -> e -> n -> Task([{n}]);
  const updateGems = curry((account, email, level) => R.sequence(Task.of, [
    taskGenerateLevelUpGem(account, email, level),
    taskNotifyUser(account, "Congratulations. You have earned a gem for reaching hosting level " + level + ".", email, 'img/rfg-logo-whiteglow-64.png', 'docs/premiumgems.html', 'levelupgemreward', true)
  ]));

  return new Task((rejectc, resolvec) => account.findOneAndUpdate({email:email}, createUpdateQuery(ishost, xppoints), {new:true}).exec().then((x) => {
    if (ishost) {
      try { R.sequence(Task.of, checkLevelFreeGem(readHostLevel(x), nextLevel(x)).map(updateGems(account, email))).fork() } catch(err) {}
    }

    taskUpdateLevel(account, email, objXpToLevel('playxp')(x).getOrElse(xppoints), objXpToLevel('hostxp')(x).getOrElse(xppoints)).fork(rejectc, resolvec);

  }).catch(reject)); 

})
exports.TaskAddGameExperience = taskAddGameExperience;


// taskReadPageSlug :: o -> e -> Task o 
const taskReadPageSlug = curry(function(account, email) { 
  const findSlug = safeProp('pageslug');
  return new Task((reject, resolve) => {
    account.findOne({email: email}, {pageslug: 1}).exec().then(data => {
      if (findSlug(data).map(resolve).isNothing)
        reject('email not found');

    }).catch(reject);
  })
});
exports.TaskReadPageSlug = taskReadPageSlug; 


// taskRecordLogin :: (Schema(account) -> e) => Task(p)
const taskRecordLogin = curry(function(account, email) { 
  return new Task((reject, resolve) => {
    account.findOneAndUpdate({email: email}, {$currentDate: {loginlast: true}, $inc:{logincount: 1}}, {new:true}).exec().then(resolve).catch(reject);
  })
});
exports.TaskRecordLogin = taskRecordLogin; 


// taskRecordActivity :: (Schema(account) -> e) => Task(p)
const taskRecordActivity = curry(function(account, email) { 
  return new Task((reject, resolve) => {
    account.findOneAndUpdate({email: email}, {$currentDate: {activelast: true}}, {new:true}).exec().then(resolve).catch(reject);
  })
});
exports.TaskRecordActivity = taskRecordActivity; 


// buildNote :: [s, t, u] => {o}
const buildNote = zipObj(['text', 'thumbnail', 'url', 'noticetype', 'openNewTab']);
exports.BuildNote = buildNote;

// buildURL :: s => u
const buildURL = (slug) => "users?id=" + slug;
exports.BuildURL = buildURL;

// maybeDefaultImg :: Maybe(x) => Maybe(s)
const maybeDefaultImg = (x) => (x.isNothing ? Maybe.of('img/default-avatar-sq.jpg') : x);
exports.MaybeDefaultImg = maybeDefaultImg;

// findTopGames :: [{a}] => [b]
const findTopGames = (a) => compose(take(2), filter(path(['matching', 'wanttoplay'])), reverse, sortBy(prop('yearpublished')))(a); 
exports.FindTopGames = findTopGames;


// taskNotifyNewNearbyUsers :: (Schema(account) -> e) => Task.of([{n}])
const taskNotifyNewNearbyUsers = curry(function(account, email) {

    // taskFindNearby ::  e => Task([{u}])
    const taskFindNearby = (email) => new Task((reject, resolve) => findPlayersNearby(account, email, 50000).then(resolve).catch(reject));

    // nearBySentence :: [{g}] => s
    const nearBySentence = (games) => (findTopGames(games).length == 0) 
        ? "." : (findTopGames(games).length == 1) 
        ? " and is looking to play " + findTopGames(games)[0].name + "." : " interested in playing " + findTopGames(games)[0].name + " or " + findTopGames(games)[1].name;

    // liftBuildText :: (Maybe(n) -> Maybe([{g}])) => Maybe(s)
    const liftBuildText = lift((name, games) =>  "A new player, " + name + ", has joined the area" + nearBySentence(games));

    // liftNotify :: Maybe(Model({a}) -> Maybe(s) -> Maybe([{g}]) -> Maybe(e) -> Maybe(t) -> Maybe(g)) => Task([{u}]) 
    const liftNotify = lift(taskNotifyUser)    

    // buidNotifyUser :: ({u} -> {t}) => Task([{n}])
    const buildNotifyUser = curry((aboutUser, toUser) => {
        return liftNotify(Maybe.of(account), liftBuildText(safePath(['profile', 'name'], aboutUser), safeProp('games', aboutUser)), safeProp('email', toUser), maybeDefaultImg(safePath(['profile', 'avataricon'], aboutUser)), safeProp('pageslug', aboutUser).map(buildURL), Maybe.of('newlocaluser'), Maybe.of(false)).getOrElse(taskFail);
    });

    // buidNotifyActiveUser :: u => Task([{n}])
    const buildNotifyActiveUser = user => taskNotifyUser(account, 'Set up your Roll for Group profile. Let other gamers know more about you.', email, 'img/rfg-logo-whiteglow-64.png', 'profile', 'signupprofile', false);

    // activeUser :: [{u}] => {e} 
    const activeUser = find(propEq('email', email));

    return new Task((reject, resolve) => taskFindNearby(email).fork(reject, data => {

      // taskActiveNotify :: {u} => Task.of({n})
      const taskActiveNotify = compose(R.sequence(Task.of), map(buildNotifyActiveUser), R.filter(R.propEq('email', email)));

      // taskNotify ::  {u} => Task.of([{n}])
      const taskNotify = compose(R.sequence(Task.of), R.append(taskActiveNotify(data)), map(buildNotifyUser(activeUser(data))), R.reject(R.propEq('email', email)));

      taskNotify(data).fork(reject, resolve)

    }));

});
exports.TaskNotifyNewNearbyUsers = taskNotifyNewNearbyUsers;


// BuildPush :: [t, n, u] => {p} 
const buildPush = compose(objOf('$push'), objOf('notifications'), merge({date: new Date()}), buildNote);
exports.BuildPush = buildPush;


// TaskNotifyUser :: (Schema(a) -> p -> e -> n -> u) => Task(Notification)
const taskNotifyUser = curry((account, text, email, thumbnail, url, noticetype, openNewTab) => 
  new Task((reject, resolve) => 
    account.findOneAndUpdate({email: email}, buildPush([text, thumbnail, url, noticetype, openNewTab]), {new:true}).exec().then((user) => {
      resolve (safeProp('notifications', user).getOrElse([]));
    }
  ).catch(reject))
);
exports.TaskNotifyUser = taskNotifyUser;

// buildMail :: [s, s, s, s, s, s, s] => {o}
const buildMail = zipObj([
  'name', 
  'thumbnail', 
  'pageslug', 
  'subject', 
  'text', 
  'mailtype', 
  'status'
]);
exports.BuildMail = buildMail;

// buildPush :: [] => {m}
const buildPushMail = compose(objOf('$push'), objOf('mails'), merge({date: new Date()}), buildMail);
exports.BuildPushMail = buildPushMail;

// taskMailUser :: ( Schema(a) -> f -> s -> t -> to -> tn -> tt -> type ) => Task(Notification)
const taskMailUser = curry(function(account, frompageslug, fromname, fromthumbnail, topageslug, subject, text, mailtype, status) {
  return new Task((reject, resolve) => 
    account.findOneAndUpdate(
      {pageslug: topageslug}, 
      buildPushMail([fromname, fromthumbnail, frompageslug, subject, text, mailtype, status]), 
      {new:true}
    )
      .exec()
      .then((user) => {
          resolve (safeProp('mails', user).getOrElse([]));
        })
      .catch(reject));
});
exports.TaskMailUser = taskMailUser;

// TaskGetMail :: o -> s -> s -> Task o
const taskGetMail = curry(function(account, email, mailid) {
  return new Task((reject, resolve) => 
    account.findOne({email: email, mails:{$elemMatch:{mailid:mailid}}})
      .exec()
      .then((user) => {
        resolve(S.prop('mails', user)
          .chain(S.find(R.propEq('mailid', mailid)))
          .getOrElse({})
        );
      })
      .catch(reject));
});
exports.taskGetMail = taskGetMail;


// TaskReadMail :: o -> s -> s -> Task o
const taskReadMail = curry(function(account, email, mailid) {
  return new Task((reject, resolve) => 
    account.update(
      {email: email, mails:{$elemMatch:{mailid:mailid}}}, 
      {$set: {'mails.$.status': 'read'}}, 
      {new:true}
    )
      .exec()
      .then(resolve)
      .catch(reject));
});
exports.TaskReadMail = taskReadMail;


// tructText :: [s] -> Maybe s
const elipsify = (a) => (a[1] === '') 
  ? S.head(a)
  : S.head(a).map((x) => x + '...')
exports.elipsify = elipsify;


// newMails :: o -> Maybe o
const elipsoidText = (msg) => {

  const getElipsify = (a) => elipsify(a).getOrElse(a);

  // tructText :: o -> o -> Maybe o
  const tructTextMsg = (txtmsg) => compose(
    merge(wipeSchema(msg)), 
    objOf('text'), 
    getElipsify, 
    R.splitAt(160)
  )(txtmsg);

  // tructText :: o -> Maybe o
  const tructText = compose(
    map(tructTextMsg),
    S.prop('text'),
    wipeSchema
  );

  return tructText(msg);

};
exports.elipsoidText = elipsoidText;


// newMails :: o -> Maybe [o]
const newMails = (mail) => {

  // chainElipse :: Maybe o -> o
  const chainElipse = (a) => elipsoidText(a).getOrElse(a);

  // elipseMail :: o -> Maybe o
  const elipseMail = compose(
    map(map(chainElipse)),
    map(R.filter(R.propEq('status', 'new'))), 
    map(R.filter(R.propEq('mailtype', 'inbox'))), 
    S.prop('mails')
  );

  return elipseMail(mail);

};
exports.NewMails = newMails;


// taskGetNewMails :: Model(Account) -> e -> Task(Model(Account))
const taskGetNewMails = curry(function(account, email) {
  return new Task(
    (reject, resolve) => account.findOne({email: email, 'mails.status':'new', 'mails.mailtype':'inbox'})
      .exec()
      .then((user) => resolve(newMails(user).getOrElse([])))
      .catch(reject)
  );
});
exports.TaskGetNewMails = taskGetNewMails;


// taskGetSentMails :: Model(Account) -> e -> Task(Model(Account))
const taskGetSentMails = curry(function(account, email) {
  return new Task((reject, resolve) => account.findOne({email: email, 'mails.mailtype':'outbox'}).exec().then((user) => {
    const newMails = compose(map(R.filter(R.propEq('mailtype', 'outbox'))), safeProp('mails'));
    resolve(newMails(user).getOrElse([]));
  }).catch(reject));
});
exports.TaskGetSentMails = taskGetSentMails;


// taskGetAllMails :: Model(Account) -> e -> Task(Model(Account))
const taskGetAllMails = curry(function(account, email) {
  return new Task((reject, resolve) => account.findOne({email: email, 'mails.mailtype':'inbox'}).exec().then((user) => {
    const newMails = compose(map(R.filter(R.propEq('mailtype', 'inbox'))), safeProp('mails'));
    resolve(newMails(user).getOrElse([]));
  }).catch(reject));
});
exports.TaskGetAllMails = taskGetAllMails;

// TaskReadNotification :: Model(Account) -> e -> n -> Task(Model(Account))
const taskReadNotification = curry(function(account, email, noteid) {
  return new Task((reject, resolve) => account.update({email: email}, { $pull: {notifications : {noteid: noteid }}}).exec().then(resolve).catch(reject));
});
exports.TaskReadNotification = taskReadNotification;

// taskClearMatchNotifications :: Model(Account) -> e -> s -> Task(Model(Account))
const taskClearMatchNotifications = curry((account, email, url) => 
  new Task((reject, resolve) => 
    account
      .update(
        {email: email}, 
        {$pull: {notifications : {url: url }}},
        {multi: true}
      )
      .exec()
      .then(resolve)
      .catch(reject))
);
exports.taskClearMatchNotifications = taskClearMatchNotifications;


// TaskReadNotification :: Model(Account) -> e -> Task(Model(Account))
const taskReadNotifications = curry(function(account, email) {

    // taskFind :: e => Task([{n}])
    const taskFind = (email) => new Task((reject, resolve) => account.findOne({email: email}, {notifications:1}).exec().then(resolve).catch(reject));

    // updateNotifications {n} :: 
    const updateNotifications = compose(R.sequence(Task.of), map(taskReadNotification(account, email)), map(prop('noteid')), prop('notifications'));
    
    return new Task((reject, resolve) => {
        taskFind(email).fork(err => reject(err),
            data => updateNotifications(data).fork(
                err => reject(err),
                datan => resolve(datan)
            )
        )
    });

});
exports.TaskReadNotifications = taskReadNotifications;


// TaskGetNewNotifications :: ()
const taskGetNewNotifications = curry(function(account, email) {
  return new Task((reject, resolve) => {

    // resolveNew :: {o} => f([{n}])
    const resolveNew = compose(resolve, filter(propEq('status', 'new')), prop('notifications'));
    account.findOne({email: email, 'notifications.status': 'new'}).limit(99).exec().then((rn) => {
      isNil(rn) ? resolve([]) : resolveNew(rn)
    }).catch(reject);
  });
});
exports.TaskGetNewNotifications = taskGetNewNotifications;


// GetTestQuadrant :: k => n
const getTestQuadrant = function(key) {
    const combine = (x) => (x > 96) ?  x - 39 : x 
    const charCode = (x) => x.charCodeAt(0);
    const calcQuadrant = compose(R.modulo(R.__, 4), combine, charCode);
    return calcQuadrant(key);
}
exports.GetTestQuadrant = getTestQuadrant;


// filterQuadKey :: (n -> {o}) => Bool
const filterQuadKey = curry((quad, user) => (safeProp('key', user).map(getTestQuadrant).getOrElse('') == quad));
exports.FilterQuadKey = filterQuadKey


// logPurchase :: (Modl(a) -> e -> c -> [{i}] -> t -> s -> d) => Task({a});
const logPurchase = curry((account, email, ordercode, items, total, currency, date) => {

    const createPurchaseItem = zipObj(['ordercode', 'items', 'total', 'currency', 'date']);

    return new Task((reject, resolve) => {
        const resolvePurchases = compose(resolve, last, prop('purchases'));
        account.findOneAndUpdate({'email': email}, {$push: {'purchases': createPurchaseItem([ordercode, items, total, currency, date])}}, {new:true}).exec().then(resolvePurchases).catch(reject);
    });
});
exports.LogPurchase = logPurchase;

// taskAccountFromEmail
const taskAccountFromEmail = curry((account, email) => new Task((reject, resolve) => readAnAccountFromEmail(account, email).then(resolve).catch(reject)));
exports.TaskAccountFromEmail = taskAccountFromEmail;

// taskAccountFromPageSlug :: e => Task({o}) 
const taskAccountFromSlug = curry((account, pageslug) => 
  new Task((reject, resolve) => 
    readAnAccountFromPageSlug(account, pageslug)
      .then(resolve)
      .catch(reject)
  )
);
exports.TaskAccountFromSlug = taskAccountFromSlug;

// whenTrue :: (a -> b -> bool) => a||b 
const whenTrue = curry((trueVal, falseVal, bool) => (bool) ? trueVal : falseVal);
exports.WhenTrue = whenTrue;

// safeWhenPremium :: ({u} -> s1 -> s2) => s
const safeWhenPremium = curry((whenActive, whenInactive, userdata) => safePath(['premium', 'isactive'], userdata).map(whenTrue(whenActive, whenInactive)).getOrElse(whenInactive));
exports.SafeWhenPremium = safeWhenPremium;

// safeWhenStaff :: ({u} -> s1 -> s2) => s
const safeWhenStaff = curry((whenActive, whenInactive, userdata) => safePath(['premium', 'isstaff'], userdata).map(whenTrue(whenActive, whenInactive)).getOrElse(whenInactive));
exports.SafeWhenStaff = safeWhenStaff;

// safeWhenType :: (s1 -> s2 -> s3 -> {o}) => s
const safeWhenType = curry((standardValue, staffValue, premiumValue, obj) => safeWhenStaff(staffValue, safeWhenPremium(premiumValue, standardValue, obj), obj));
exports.SafeWhenType = safeWhenType;

// datePlusMonth :: d1 => d2
const datePlusMonths = curry((date, numb) => moment(date).add(numb, 'months'));
exports.DatePlusMonths = datePlusMonths;

// maxDate :: d1 -> d2 => d1||d2
const maxDate = curry((date1, date2) => (moment(date1).diff(date2)) > 0 ? date1 : date2);
exports.MaxDate = maxDate;

// getExpireDate :: {u} -> s
//const getExpireDate = (user) => safePath(['premium', 'activetodate'], user).getOrElse(new Date());
const getExpireDate = (user) => safePath(['premium', 'activetodate'], user).map(maxDate(new Date())).getOrElse(new Date());
exports.GetExpireDate = getExpireDate;

// getUserName :: {u} -> s
const getUserName = (user) => safePath(['profile', 'name'], user).getOrElse("An anonymous player");
exports.GetUserName = getUserName;

// hasGems :: {u} -> s
const hasGems = (user) => safePath(['premium', 'creditsshare'], user).getOrElse(0) > 0;
exports.HasGems = hasGems;

// getAvatarIcon :: {u} -> s
const getAvatarIcon = (user) => safePath(['profile', 'avataricon'], user).getOrElse('img/u_roll_icon1-01.png');
exports.GetAvatarIcon = getAvatarIcon;

// resolvePremium :: f() => Task({p})
const resolvePremium = (cb) => compose(cb, prop('premium'), JSON.parse, JSON.stringify);
exports.ResolvePremium = resolvePremium;

// getEmail :: {u} -> s
const getEmail = (user) => safeProp('email', user).getOrElse("");
exports.GetEmail = getEmail;

// receiveMembership :: s => Task({u});
const receiveMembership = curry((account, fromemail, toemail, description, reference, months, date) => new Task((reject, resolve) => account.findOneAndUpdate({email: toemail}, {$set: {'premium.isactive':true, 'premium.activetodate': datePlusMonths(date, months)}, $push: {'premium.creditlog': {from: fromemail, to: toemail, date: new Date(), description: description, reference: reference, activeto: datePlusMonths(date, months)}}}, {new:true}).exec().then(resolvePremium(resolve)).catch(reject)));
exports.ReceiveMembership = receiveMembership;

// extendCreditExpiry :: {u1} -> Task({u2})
const extendCreditExpiry = (account, fromemail, touser, description, reference, quant) => compose(receiveMembership(account, fromemail, getEmail(touser), description, reference, quant), getExpireDate)(touser);
exports.ExtendCreditExpiry = extendCreditExpiry;


// giftGem :: e1 -> e2 => Task({u})
const giftGem = curry((account, reference, fromemail, topageslug) => new Task((reject, resolve) => {

  // giftCredit :: s => Task({u});
  const giftCredit = curry((fromemail, reference, toemail) => new Task((reject, resolve) => account.findOneAndUpdate({email: fromemail}, {$inc: {'premium.creditsshare': -1}, $push: {'premium.creditlog': {from: fromemail, to: toemail, date: new Date(), description:'gift-send', reference: reference}}}, {new:true}).exec().then(resolve).catch(reject)));

  Promise.all([readAnAccountFromEmail(account, fromemail), readAnAccountFromPageSlug(account, topageslug)]).then(users => {
    hasGems(users[0]) 
      ? giftCredit(fromemail, reference, users[1].email).fork(
        err => reject(err),
        dataFrom => extendCreditExpiry(account, fromemail, users[1], 'gift-receive', reference, 1).fork(
          err => reject(err),
          dataTo => {
            taskNotifyUser(account, getUserName(users[0]) + ' has gifted you a gem. Unlocking a month of premium access.', users[1].email, getAvatarIcon(dataFrom), 'users?id=' + users[1].pageslug, 'giftgem', false).fork();
            resolvePremium(resolve)(dataFrom);
          }
        )
      )
      : reject('user has no gems')
  }).catch(reject)

}));
exports.GiftGem = giftGem;


// purchaseItem :: 
const purchaseItem = curry((account, email, itemcode, quantity) => {

  // activate :: {o} => {p}
  const activate = merge(R.__, {isactive:true});

  // lookupUserAndExtend :: Model(account) -> s -> t -> d -> r -> n => Task({o})
  const lookupUserAndExtend = (account, fromemail, touser, description, reference, quant) => new Task(
    (reject, resolve) => readAnAccountFromEmail(account, touser).then(
      (u) => extendCreditExpiry(account, fromemail, u, description, reference, quant).fork(
        err => reject(err),
        data => resolve(data)
      )
    ).catch(taskFail())
  ); 

  // udpateItem :: s => {u}
  const updateItem = (itemcode, quant) =>         
    (itemcode) == "rfggold-credit" 
      ? {$inc:{'premium.creditsshare': quant}} 
      : (itemcode) == "rfggold-credit-12pack" 
        ? {$inc:{'premium.creditsshare': quant * 12}} 
        : (itemcode) == "rfggold-ongoing" 
          ? {$set:{'premium.isongoing': true, 'premium.isongoingsize': quant}} 
          : (itemcode) == "rfggold-ongoing-12pack" 
            ? {$set:{'premium.isongoing': true, 'premium.isongoingsize': quant * 12}} 
            : {}


  // purchaseGemTask :: e -> i -> n => Task({o})
  const purchaseGemTask = (email, itemcode, quantity) => new Task((reject, resolve) => account.findOneAndUpdate({'email': email}, updateItem(itemcode, quantity), {new:true}).exec().then(resolvePremium(resolve)).catch(reject));

  const findUpdateTask = (email, itemcode, quant) => 
      (itemcode) == "rfggold-once" ? lookupUserAndExtend(account, 'rollforgroup', email, itemcode, 'ref', quant) : 
      (itemcode) == "rfggold-once-12pack" ? lookupUserAndExtend(account, 'rollforgroup', email, itemcode, 'ref', quant * 12) : 
      (itemcode) == "rfggold-credit" ? purchaseGemTask(email, itemcode, quant) : 
      (itemcode) == "rfggold-credit-12pack" ? purchaseGemTask(email, itemcode, quant) :
      (itemcode) == "rfggold-ongoing" ? purchaseGemTask(email, itemcode, quant) :
      (itemcode) == "rfggold-ongoing-12pack" ? purchaseGemTask(email, itemcode, quant) : {}

  return findUpdateTask(email, itemcode, quantity);

});
exports.PurchaseItem = purchaseItem;


// ongoingSubscription :: 
const ongoingSubscription = curry((account, email, itemcode, quantity) => {

  // activate :: {o} => {p}
  const activate = merge(R.__, {isactive:true});

  // lookupUserAndExtend :: Model(account) -> s -> t -> d -> r -> n => Task({o})
  const lookupUserAndExtend = (account, fromemail, touser, description, reference, quant) => new Task(
    (reject, resolve) => readAnAccountFromEmail(account, touser).then(
      (u) => extendCreditExpiry(account, fromemail, u, description, reference, quant).fork(
        err => reject(err),
        data => resolve(data)
      )
    ).catch(taskFail())
  ); 

  // udpateItem :: s => {u}
  const updateItem = (itemcode, quant) =>         
    (itemcode) == "rfggold-credit" 
      ? {$inc:{'premium.creditsshare': quant}} 
      : (itemcode) == "rfggold-credit-12pack" 
        ? {$inc:{'premium.creditsshare': quant * 12}} 
        : (itemcode) == "rfggold-ongoing" 
          ? {$set:{'premium.isongoing': true, 'premium.isongoingsize': quant}} 
          : (itemcode) == "rfggold-ongoing-12pack" 
            ? {$set:{'premium.isongoing': true, 'premium.isongoingsize': quant * 12}} 
            : {}

  // purchaseGemTask :: e -> i -> n => Task({o})
  const purchaseGemTask = (email, itemcode, quantity) => 
    new Task((reject, resolve) => 
      account.findOneAndUpdate(
        {'email': email}, 
        updateItem(itemcode, quantity), 
        {new:true}
      )
      .exec()
      .then(resolvePremium(resolve))
      .catch(reject)
    );

  const findUpdateTask = (email, itemcode, quant) => 
    (itemcode) == "rfggold-once" 
      ? lookupUserAndExtend(account, 'rollforgroup', email, itemcode, 'ref', quant) 
      : (itemcode) == "rfggold-once-12pack" 
        ? lookupUserAndExtend(account, 'rollforgroup', email, itemcode, 'ref', quant * 12) 
        : (itemcode) == "rfggold-credit" 
          ? purchaseGemTask(email, itemcode, quant) 
          : (itemcode) == "rfggold-credit-12pack"
            ? purchaseGemTask(email, itemcode, quant) 
            : (itemcode) == "rfggold-ongoing" 
              ? purchaseGemTask(email, itemcode, quant) 
              : (itemcode) == "rfggold-ongoing-12pack" 
                ? purchaseGemTask(email, itemcode, quant) 
                : {}

  return findUpdateTask(email, itemcode, quantity);

});
exports.ongoingSubscription = ongoingSubscription;


// regenerateCalendarId :: Model(account) -> s => Task(account)
const regenerateCalendarId = curry((account, email) => 
  new Task((reject, resolve) => 
    account
      .findOneAndUpdate({email:email}, {icalkey: uuid.v4()}, {new:true})
      .exec()
      .then(resolve)
      .catch(reject)
  )
);
exports.RegenerateCalendarId = regenerateCalendarId;

// unPremiumMemberships :: account -> Task o 
const unPremiumMemberships = (account) => new Task((reject, resolve) => {
  account
    .update(
      {'premium.isactive':true, 'premium.activetodate':{$lt:new Date()}},
      {$set:{'premium.isactive':false}},
      {multi:true}
    )
    .limit(100)
    .exec()
    .then(resolve)
    .catch(reject);
});
exports.unPremiumMemberships = unPremiumMemberships;
