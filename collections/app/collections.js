var Promise = require('bluebird');
var express = require('express');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var R = require('ramda');

var bgg 				= require('./bgg.js');
var log 				= require('./log.js'); 
var S 					= require('./lambda.js');

var account 	= require('./account.js');
var accounts 	= require('./accounts.js');

var boardgame 	= require('./boardgame.js');
var boardgames 	= require('./boardgames.js');
var collection 	= require('./collection.js');

const Maybe     = require('data.maybe');
const Task      = require('data.task');

// noEquals :: o -> Bool 
const notEquals = R.compose(R.not, R.equals({}));

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// scrub :: mongoObj -> obj;
const scrub = compose(R.dissoc('_id'), R.dissoc('__v'), JSON.parse, JSON.stringify);

// objOfKey :: s -> {o}
const objOfKey = R.objOf('key');
exports.objOfKey = objOfKey;

// findById :: [{g}] -> n => maybe {g}
const findById = curry((list, id) => S.find(R.propEq('id', Number(id)), list));
exports.findById = findById;

// validateText :: s -> maybe s
const validateText = (x) => (x !== '')
  ? Maybe.of(x)
  : Maybe.Nothing();
exports.validateText = validateText;

// mergeObjKeyEmail :: s -> s -> {o}
const mergeObjKeyEmail = R.curry((matchkey, field, value) => 
  R.merge( 
    R.objOf('key', matchkey),
    R.objOf(field, value)
  )
);
exports.mergeObjKeyEmail = mergeObjKeyEmail;

// maybeUserId :: {o} -> maybe s
const maybeUserId = S.path(['body', 'userkey']);
exports.maybeUserId = maybeUserId;

// maybeQuery :: s -> {o} -> maybe n 
const maybeQuery = R.curry((key, req) => S.path(['query', key], req));
exports.maybeQuery = maybeQuery;

// getParamColl :: {o} -> maybe s
const getParamColl = (req) => S.path(['params', 'collectionid'], req);
exports.getParamColl = getParamColl;

// maybeAdminKey :: {o} -> maybe s
const maybeAdminKey = S.path(['body', 'adminkey']);
exports.maybeAdminKey = maybeAdminKey;

// maybeKey :: obj => Maybe(key);
const maybeKey = (req, pathkey) => (S.path([pathkey, 'key'], req).isJust) 
  ? S.path([pathkey, 'key'], req) 
  : S.path(['user', 'key'], req); 
exports.MaybeKey = maybeKey;

// mergeGameValue :: [{o}] -> s -> {o} -> {o}
const mergeGameValue = curry((lookupgames, property, game) =>
  R.compose(
    R.map(R.merge(game)),
    R.map(R.objOf(property)), 
    R.chain(S.prop(property)), 
    R.chain(findById(lookupgames)), 
    S.prop('id')
  )(game).getOrElse(game)
);
exports.mergeGameValue = mergeGameValue;


// updateExtendedGameData :: [{e}] -> {g} => {o}
const updateExtendedGameData = curry((lookupgames, game) => {

  // mergeValue :: -> s -> n -> {o}
  const mergeValue = mergeGameValue(lookupgames)

  // updateGame :: {o} -> {o}
  const updateGame = compose(
    mergeValue('maxplayers'), 
    mergeValue('minplayers'), 
    mergeValue('maxplaytime'), 
    mergeValue('minplaytime')
  );

  return updateGame(game);

});
exports.updateExtendedGameData = updateExtendedGameData;

// taskCreateCollection :: schema -> s -> s -> s -> s -> s -> s -> s -> task schema
const taskCreateCollection = R.curry((collection, userkey, name, description, thumbnail, bggname, bggtype, bgglistnumber) => 
  new Task((reject, resolve) => {
		var coll = new collection({
      userkey: userkey,
			name:  name,
			description: description,
			thumbnail: thumbnail,
			isbgg:    bggname !== '',
			bggname:  bggname,
			bggtype:  bggtype,
      bgglistnumber: bgglistnumber
		});

		coll
			.save()
			.then(resolve)
      .catch(reject)

  })
);
exports.taskCreateCollection = taskCreateCollection;


// taskReadCollection :: schema -> s -> task schema
const taskReadCollection = R.curry((collection, key) => {
  return new Task((reject, resolve) => {
    collection
      .findOne(objOfKey(key))
      .exec()
      .then(resolve)
      .catch(reject);

  })
});
exports.taskReadCollection = taskReadCollection;


// taskReadCollectionIncrement :: s -> task schema
const taskReadCollectionIncrement = (key) => {
  return new Task((reject, resolve) => {
    collection
      .findOneAndUpdate(objOfKey(key), {$inc:{viewcount:1}}, {new:true})
      .exec()
      .then(resolve)
      .catch(reject);

  })
};
exports.taskReadCollectionIncrement = taskReadCollectionIncrement;


// taskListCollectionsByUser :: schema -> s -> task schema
const taskListCollectionsByUser = R.curry((collection, pageslug) => {
  return new Task((reject, resolve) => {
    collection
      .find({userkey: pageslug})
      .exec()
      .then(resolve)
      .catch(reject);

  })
});
exports.taskListCollectionsByUser = taskListCollectionsByUser;


// taskUpdateCollection :: schema -> s -> {o} -> task schema
const taskUpdateCollection = R.curry((collection, collkey, updateObj) => {

  // pickFields :: {o} -> {o}
  const pickFields = R.pick(['description', 'thumbnail', 'name']);
  return new Task((reject, resolve) => {
    collection
      .findOneAndUpdate(
        {key: collkey},
        pickFields(updateObj),
        {new:true}
      )
      .exec()
      .then(resolve)
      .catch(reject);
  })
});
exports.taskUpdateCollection = taskUpdateCollection;

// taskDeleteCollection :: schema -> s -> task schema
const taskDeleteCollection = R.curry((collection, collkey) => {
  return new Task((reject, resolve) => {
    collection
      .remove({key: collkey})
      .exec()
      .then(resolve)
      .catch(reject);

  })
});
exports.taskDeleteCollection = taskDeleteCollection;


// taskCollectionAddGame :: schema -> s -> n -> task schema
const taskCollectionAddGame = R.curry((collection, boardgame, collkey, boardgameid) => 
  new Task((reject, resolve) => {

    // look up the boardgame
    boardgames.TaskFindBoardgame(boardgame, boardgameid).fork(
      err => reject('game not found'),
      data => {
        collection.findOneAndUpdate(
          {key: collkey},
          {'$push': { 
            games: data
          }},
          {new:true}
        )
          .lean()
          .exec()
          .then(resolve)
          .catch(reject);

      }
    );

  })
);
exports.taskCollectionAddGame = taskCollectionAddGame;


// taskCollectionRemoveGame :: schema -> s -> task schema
const taskCollectionRemoveGame = R.curry((collection, collkey, boardgameid) => 
  new Task((reject, resolve) => {
    collection.findOneAndUpdate(
      {key: collkey},
      {$pull: { 
        games: {id: Number(boardgameid)} 
      }},
      {new:true}
    )
      .lean()
      .exec()
      .then(resolve)
      .catch(reject);

  })
);
exports.taskCollectionRemoveGame = taskCollectionRemoveGame;


// propGames :: {o} -> maybe [{o}]
const propGames = R.compose(
  map(map(scrub)),
  S.prop('games')
);
exports.propGames = propGames;

// updateBoardGameName :: Model(boardgames) -> JSONObject -> JSONObject
var updateBoardGameName = R.curry(function(object) {

  var newObject = R.pick(['thumbnail', 'image', 'yearpublished', 'minplaytime', 'maxplaytime', 'numplays'], object);

  newObject.id = object.objectid;
  newObject.name = object.name.t;
  newObject.bggcache = object;

  newObject.minplayers = object.stats.minplayers;
  newObject.maxplayers = object.stats.maxplayers;
  newObject.minplaytime = object.stats.minplaytime;
  newObject.maxplaytime = object.stats.maxplaytime;

  return newObject;

});
exports.UpdateBoardGameName = updateBoardGameName;

// removeBggItem :: [a] -> o -> o
const removeBggItem = R.curry((bgglist, item) => (
  S.find(R.propEq('id', Number(item.id)), bgglist).isNothing)
    ? {}
    : item
);
exports.removeBggItem = removeBggItem;

// removeBggItems :: [a] -> [b] -> [c]
const removeBggItems = R.curry((col, bgglist) => R.filter(notEquals, col.map(removeBggItem(bgglist)))); 
exports.removeBggItems = removeBggItems;


// updateBggItem :: [a] -> o -> o
const updateBggItem = R.curry((list, item) => {

  const findOrElse = R.curry((bgglist, id) => S
    .find(R.propEq('id', Number(id)), bgglist)
    .getOrElse({}))

  const updateItem = R.compose(
    map(merge(item)),
    map(findOrElse(list)),
    S.prop('id')
  )
  
  return updateItem(item).getOrElse(item);

});
exports.updateBggItem = updateBggItem;

// updateBggItems :: [a] -> [b] -> [c]
const updateBggItems = R.curry((bgglist, col) => {
  return col.map(updateBggItem(bgglist))
});
exports.updateBggItems = updateBggItems;


// addBggItem :: [a] -> o -> o
const addBggItem = R.curry((col, item) => {

  // findIdWhenExists :: n => Maybe n 
  const findIdWhenExists = R.curry((col, id) => S.find(R.propEq('id', Number(id)), col).isJust
    ? Maybe.Nothing()
    : Maybe.of(id)
  );

  // propfindById :: {o} -> Maybe bool
  const propFindById = R.compose(
    R.chain(findIdWhenExists(col)),
    S.prop('id')
  );

  return propFindById(item).isNothing
    ? {}
    : R.merge(scrub(item), {bggsync: true, bggadded: new Date()})

})
exports.addBggItem = addBggItem;

// arrayatise :: a||[a] => [a]
const arrayatise = (obj) =>
  Array.isArray(obj)
    ? obj
    : R.of(obj)
exports.arrayatise = arrayatise;


// addBggItems :: [a] -> [b] -> [c]
const addBggItems = R.curry((bgglist, col) => {
  return R.filter(notEquals, bgglist.map(addBggItem(col)))
});
exports.addBggItems = addBggItems;


// prepareGameName :: {o} -> {o}
const prepareGameName = R.compose(
  R.map(updateBoardGameName), 
  arrayatise,
  bgg.ReplaceTDollar, 
  R.path(['items', 'item'])
);
exports.prepareGameName = prepareGameName;


const filterListtype = R.curry((listtype, value, list) => 
  S.filter(R.pathEq(['bggcache', 'status', listtype], value), list));
exports.filterListtype = filterListtype;


// bggFilterList
const bggFilterList = R.curry((data, list) => {
  return S.filter(
    R.pathEq(
      ['bggcache', 'status', S.prop('bggtype', data).getOrElse('own')], 
      S.prop('bgglistnumber', data).getOrElse(1)
    ), list
  ).getOrElse([]);
})
exports.bggFilterList = bggFilterList;


// verifyAdminKey :: o -> Task Bool
const verifyAdminKey = R.curry((collection, collectionId, adminKey) =>
  new Task((reject, resolve) => {
    taskReadCollection(collection, collectionId).fork(
      err => resolve(false),
      data => resolve(S.whenPropEq('adminkey', adminKey, data).isJust)
    )
  })
);
exports.verifyAdminKey = verifyAdminKey;

// verifyUserAccount :: o -> Task Bool
const verifyUserAccount = R.curry((collection, collectionId, userslug, useraccountkey) =>
  new Task((reject, resolve) => {
    taskReadCollection(collection, collectionId).fork(
      err => resolve(false),
      data => {
        if (S.whenPropEq('userkey', userslug, data).isJust) {
          accounts.TaskAccountFromSlug(account, userslug).fork(
            err => resolve(false), 
            data => resolve(S.whenPropEq('key', useraccountkey, data).isJust)
          )
        } else {
          resolve(false);
        }
      }
    )
  })
)
exports.verifyUserAccount = verifyUserAccount;

// mergeIfJust :: o -> Maybe o -> o
const mergeIfJust = R.curry((o, b) => (b.isJust)
  ? R.merge(o, b.get())
  : o
);
exports.mergeIfJust = mergeIfJust;

// headOrObj :: [a]||o ->  o
const headOrObj = (obj) => (Array.isArray(obj))
  ? obj[0]
  : obj;
exports.headOrObj = headOrObj;

// mergeUserRank :: o -> o
const mergeUserRank = (obj) => R.compose( 
  R.map(R.merge(obj)),
  R.map(R.objOf('userrank')),
  S.path(['stats', 'rating', 'value'])
)(obj);
exports.mergeUserRank = mergeUserRank;


// mergeNumPlay :: [{o}] -> {o} -> [{o}] 
const mergeNumPlay = R.curry((bggPlays, game) => {
  return S.find(R.propEq('objectid', game.id), bggPlays)
    .chain(S.prop('numplays'))
    .map(R.objOf('numplays'))
    .map(R.merge(game))
    .getOrElse(game);

}); 
exports.mergeNumPlay = mergeNumPlay;


// mergeNumPlays :: [{o}] -> [{o}] -> [{o}] 
const mergeNumPlays = R.curry((bggPlays, gameList) => {

  // TODO Make this function integrate to add the users rank
  // mergeUserRank

  // getList :: {o} -> [{o}]
  const getList = (plays) => S.path(['items', 'item'], plays)
    .getOrElse([]);

  return R.map(mergeNumPlay(getList(bggPlays)), gameList);

}); 
exports.mergeNumPlays = mergeNumPlays;


// findAndUpdateABggCollection :: model {o} -> task {o}
const findAndUpdateABggCollection = (boardgame, collection) => 
  new Task((reject, resolve) => {
    collection
      .findOne({isbgg:true, bgghassearched:false})
      .exec()
      .then((val) => {

        // taskLiftUpdateBggCollection :: maybe s -> maybe task schema
        const taskLiftUpdateBggCollection = R.lift(updateBggCollection(boardgame, collection)); 

        if (!R.isEmpty(val)) {
          taskLiftUpdateBggCollection(S.prop('key', val))
            .getOrElse(taskFail())
            .fork(
              err => reject(err),
              data => resolve(data)
            )

        }
      })
      .catch(err => reject(err))
  });
exports.findAndUpdateABggCollection = findAndUpdateABggCollection;


// splitQuote s -> maybe s 
const splitQuote = (x) => x.split('"').length == 3
  ? Maybe.of(x.split('"')[1])
  : Maybe.Nothing();
exports.splitQuote = splitQuote;


// regexGameName :: o -> maybe s
const regexGameName = R.compose(
  R.chain(splitQuote),
  R.chain(S.prop('errmsg')),
  S.whenPropEq('codeName', 'DuplicateKey')
);
exports.regexGameName = regexGameName;


// updateBggCollection :: model {o} -> model {o} -> s -> task {o}
const updateBggCollection = R.curry((boardgame, collection, key) =>
  new Task((reject, resolve) => {

    // updateCollectionSearched :: model {o} -> s -> Promise {o}
    const updateCollectionSearched = R.curry((collection, key) => 
      new Promise((fulfill, reject) => 
        collection
          .findOneAndUpdate({key: key}, {bgghassearched: true}, {new:true})
          .exec()
          .then((yz) => resolve(scrub(yz)))
          .catch(reject)
      )
    );

    taskReadCollection(collection, key).fork(
      err => reject('bgg user not read'),
      data => {
        if (S.whenPropEq('isbgg', true, data).isJust) {
          bgg.GetBGGUserCollection(S.prop('bggname', data).map(R.trim).get()).then(function(bggUserCollection) {

            if (isEmpty(bggUserCollection)) {
              reject('no collection');

            } else if(bggUserCollection.hasOwnProperty('errors')) {
              if ((S.pathEq(['errors', 'error', 'message'], 'Invalid username specified', bggUserCollection)).isJust) {
                updateCollectionSearched(collection, key)
                  .then(resolve)
                  .catch(reject);

              } else {
                reject(bggUserCollection.errors);
              };

            } else if(R.path(['items', 'totalitems'], bggUserCollection) == 0) {
              updateCollectionSearched(collection, key)
                .then(resolve)
                .catch(reject);

            } else {

              var getBggCache = R.prop('bggcache');
              var currentyear = new Date().getFullYear();
              var cacheGames = boardgames.CacheBoardgame(boardgame, currentyear);

              // notPropBggsync :: {o} -> bool 
              const propBggsync = R.compose(R.not, R.propEq('bggsync', true));

              // colsync :: [{o}] -> [{o}] 
              const colsync = (col) =>    R.filter(propEq('bggsync', true), col);

              // colsync :: [{o}] -> [{o}] 
              const colnotsync = (col) => R.filter(propBggsync, col);

              var writeBoardGameList = R.compose(
                Promise.all, 
                R.map(cacheGames), 
                R.map(getBggCache)
              );

              writeBoardGameList(prepareGameName(bggUserCollection)).then((x) => {

                // buildGames :: [{o}] -> [{o}] -> [{o}] 
                const buildGames = R.curry((col, bgglist) => {
                  return R.compose(
                    R.concat(colnotsync(col)),
                    mergeNumPlays(bggUserCollection),
                    addBggItems(bggFilterList(data, bgglist)),
                    updateBggItems(bggFilterList(data, bgglist)),
                    removeBggItems(bggFilterList(data, bgglist)),
                    colsync
                  )(col)
                });

                if (validateText(data.bggname).isJust) {
                  collection
                    .findOneAndUpdate(
                      {key: data.key},
                      {
                        $set: {
                          games: buildGames(data.games, x),
                          bgghassearched: true
                        }
                      },
                      {new:true}
                    )
                    .exec()
                    .then((yz) => {
                      resolve(scrub(yz));
                    })
                    .catch(reject);
                } else {
                  resolve(x);
                }

              })
              .catch(err => {
                regexGameName(err).cata({
                  Just:     (name) => boardgames.deleteBoardgameByName(boardgame, name).then(), 
                  Nothing:  () => {} 
                });

                log.clos('duplicate.key', err);

                reject(err);

              });

            }
          }).catch(err => {
            log.clos('err', err);
          })

        } else {
          resolve(data);
        }

      }
    )
  })
);
exports.updateBggCollection = updateBggCollection;


// createBggUserCollections :: collection -> s -> s -> task {o}
const createBggUserCollections = R.curry((collection, userId, bggUser) =>
  new Task((reject, resolve) => {

    // maybeUserKey :: s -> {o} -> s -> {o} => {o}
    const maybeUserKey = R.curry((key, obj, ofKey, mergeObj) => 
      S.path(['user', key, 'value'], obj)
        .map(R.objOf(ofKey))
        .map(R.merge(mergeObj))
    );

    if (userId === '') {
      resolve([]);
    } else {

      account
        .findOne({pageslug: userId, gamesCollectionCreated:true})
        .then((data) =>{
          if (R.isNil(data)) {
            R.sequence(Task.of, [
              taskCreateCollection(collection, userId, 'Games Owned', 'These are the games I own', '', bggUser, 'own', 1),
              taskCreateCollection(collection, userId, 'Want to Play', 'The games I want to play', '', bggUser, 'wanttoplay', 1),
              taskCreateCollection(collection, userId, 'Want to Buy', 'The games I want to buy', '', bggUser, 'wanttobuy', 1)
            ]).fork(
              err => {},
              data => {

                // if bggUser -> lookup and update user info
                account
                  .findOneAndUpdate({pageslug: userId}, {gamesCollectionCreated:true}, {new:true})
                  .exec()
                  .then((a) => {

                    bgg.GetBGGUser(bggUser)
                      .then((user) => {

                        // buildProfile :: {o} -> {o} -> {o}
                        const buildProfile = R.curry((bggUser) => maybeUserKey('webaddress', bggUser, 'profile.socialwebsite', {})
                          .chain(maybeUserKey('xboxaccount', bggUser, 'profile.commxbox'))
                          .chain(maybeUserKey('wiiaccount', bggUser, 'profile.commwii'))
                          .chain(maybeUserKey('psnaccount', bggUser, 'profile.commpsn'))
                          .chain(maybeUserKey('battlenetaccount', bggUser, 'profile.commbattlenet'))
                          .chain(maybeUserKey('steamaccount', bggUser, 'profile.commsteam'))
                          .map(R.objOf('$set'))
                        );

                        if (buildProfile(user).isJust) {
                          account
                            .findOneAndUpdate({pageslug: userId}, buildProfile(user).get({}), {new:true})
                            .exec()
                            .then((updateduser) => {
                              resolve(data);
                            })
                            .catch();
                        } else {
                          resolve(data);
                        }
                      })
                      .catch();
                  });
              }
            );

          } else {
            collection.find({userkey: userId}).then((data) => {
              resolve(data); 
            });

          }
        });


    }

  })
);
exports.createBggUserCollections = createBggUserCollections;


// verifyTask :: {o} -> {o} -> maybe task {o}
const verifyTask = R.curry((collection, req) => {

  const maybeVerifyUser = R.lift(verifyUserAccount(collection));
  const maybeVerifyAdminKey = R.lift(verifyAdminKey(collection));

  return (maybeUserId(req).isJust)
    ? maybeVerifyUser(getParamColl(req), maybeUserId(req), maybeKey(req, 'body'))
    : maybeVerifyAdminKey(getParamColl(req), maybeAdminKey(req))

});
exports.verifyTask = verifyTask;


// verifyUserWithSlug :: {o} -> s -> s -> s -> task boolean 
const verifyUserWithSlug = R.curry((account, key, slug) => 
  new Task((reject, resolve) =>
    accounts.TaskAccountFromSlug(account, slug)
      .fork(
        err => resolve(false),
        user => {
          resolve(S.whenPropEq('key', key, user).isJust) 
        }
      ) 
  )
);
exports.verifyUserWithSlug = verifyUserWithSlug;

// liftTaskVerifyUserWithSlug :: maybe s -> maybe s -> maybe s -> maybe task boolean 
const liftTaskVerifyUserWithSlug = R.lift(verifyUserWithSlug(account));

// pickNoAdminKey {o} -> {o}
const pickNoAdminKey = R.pick([
  'userkey', 
  'name', 
  'description', 
  'thumbnail', 
  'bggname', 
  'bggtype', 
  'bgglistnumber', 
  'key',
  'isbgg',
  'viewcount'
]);


// pickNoAdminKey {o} -> {o}
const pickWithAdminKey = R.pick([
  'userkey', 
  'name', 
  'description', 
  'thumbnail', 
  'bggname', 
  'bggtype', 
  'bgglistnumber', 
  'adminkey',
  'key',
  'isbgg',
  'viewcount'
]);

// taskFindUserGames :: s -> s -> n -> task [{o}]
const taskFindUserGames = R.curry((collection, userslug, bggtype, gamelimit) => 
  new Task ((reject, resolve) => { 

    // getGames :: {o} -> [{o}]
    const getGames = R.curry((listkey, data) => R.compose(
      R.map(R.merge({listkey: listkey})),
      R.map(R.objOf('games')),
      R.map(R.head),
      R.map(R.splitAt(gamelimit)),
      S.prop('games')
    )(data));

    collection
      .findOne({userkey:userslug, bggtype:bggtype})
      .exec()
      .then((data) => {

       R.isEmpty(data)
         ? reject(404)
         : resolve(getGames(data.key, data).getOrElse([]))

      })
      .catch(reject);

  })
);
exports.taskFindUserGames = taskFindUserGames;


// taskDiscoverRandomCollection :: s -> s -> n -> task [{o}]
const taskDiscoverRandomCollection = R.curry((collection, userslug) => 
  new Task ((reject, resolve) => { 
    collection
      .findOne({bggtype:"wanttoplay", $where: "this.games.length > 10", bggname: {$ne:userslug}})
      .sort({updatedAt:-1})
      .limit(1)
      .exec()
      .then((data) => {
        R.isEmpty(data)
          ? reject(404)
          : resolve(data)

      })
      .catch((err) => {
        reject(err);
      });

  })
);
exports.taskDiscoverRandomCollection = taskDiscoverRandomCollection;


// setupRouter :: express 
var setupRouter = function(boardgame, collection) {

  var router = express.Router();

  router.route('/')
    .get(function(req, res) {

      // taskLiftUserId :: maybe s -> maybe task schema
      const taskLiftUserId = R.lift(taskListCollectionsByUser(collection)); 

      if (maybeQuery('discover', req).isJust) {
        taskDiscoverRandomCollection(collection, maybeQuery('bggid', req).getOrElse(''))
          .fork(
            err =>  res.sendStatus(404),
            data => res.json(pickNoAdminKey(data))
          );

      } else {

        // const getUserList()
        taskLiftUserId(maybeQuery('userid', req))
          .getOrElse(taskFail())
          .fork(
            err => res.sendStatus(500),
            data => {
              liftTaskVerifyUserWithSlug(maybeKey(req, 'query'), maybeQuery('userid', req))
              .cata({
                Just: (t) => t.fork(
                  err   => res.json(R.map(pickNoAdminKey, data)),
                  user  => {
                    (user) 
                      ? res.json(R.map(pickWithAdminKey, data))
                      : res.json(R.map(pickNoAdminKey, data))
                }),
                Nothing:  () => res.json(R.map(pickNoAdminKey, data))
              });

            }
          );
      }
    })
    .post(jsonParser, function(req, res) {

      // taskLiftCreate :: maybe name -> maybe description -> maybe thumbnail -> maybe bggname -> maybe bggtype -> maybe bgglistnumber -> maybe task 
      const taskLiftCreate = R.lift(taskCreateCollection(collection, S.path(['body', 'userkey'], req).getOrElse('')));

      // liftTaskCreateBggUserCollections :: maybe s -> maybe s -> maybe task {o}
      const liftTaskCreateBggUserCollections = R.lift(createBggUserCollections(collection));
      
      // composeTaskUpdate :: a => maybe [task]
      const composeTaskUpdate = R.compose(
        R.sequence(Maybe.of),
        R.map(R.map(updateBggCollection(boardgame, collection))),
        R.map(S.prop('key'))
      );

      if (S.pathEq(['body', 'initialise'], 'true', req).isJust) {
        liftTaskCreateBggUserCollections(
          S.path(['body', 'userkey'], req),
          S.path(['body', 'bggname'], req)
        ).cata({
          Just: (t) => t.fork(
            err =>  res.sendStatus(500),
            data => {
              composeTaskUpdate(data).cata({
                Just:   (t) => R.sequence(Task.of, t).fork(
                  err => {},
                  bg  => {} 
                ),
                Nothing: () => {}
              });
              res.json({collections:data})

            }
          ),
          Nothing: () => res.sendStatus(500)
        });

      } else {
        taskLiftCreate(
          S.path(['body', 'name'], req),
          S.path(['body', 'description'], req),
          S.path(['body', 'thumbnail'], req),
          S.path(['body', 'bggname'], req),
          S.path(['body', 'bggtype'], req),
          S.path(['body', 'bgglistnumber'], req)
        ).cata({
          Just  : (t) => t.fork( 
            err =>  res.sendStatus(500),
            data => res.json(pickWithAdminKey(data))
          ),
          Nothing : b => res.sendStatus(500)
        });

      }
    })

  router.route('/listtype/:listtype')
    .get(function(req, res) {

      // taskLiftFindUsers :: maybe s -> maybe s -> maybe s -> maybe task s
      const taskLiftFindUsers = R.lift(taskFindUserGames(collection));
      const getListType = (req) => S.path(['params', 'listtype'], req);

      taskLiftFindUsers( maybeQuery('userid', req),
        getListType(req),
        maybeQuery('gamelimit', req)
      )
        .getOrElse(taskFail())
        .fork(
          err => res.sendStatus(404),
          data => {
            res.json(data)
          }
        );

    })

  router.route('/:collectionid')
    .get(function(req, res) {

      const taskLiftRead = R.lift(taskReadCollectionIncrement); 

      taskLiftRead(
        getParamColl(req)
      ).getOrElse(taskFail()).fork(
        err =>  res.sendStatus(404),
        data => res.json(pickNoAdminKey(data))
      );

    })
    .patch(jsonParser, function(req, res) {

      const taskLiftUpdate = R.lift(taskUpdateCollection(collection)); 

      verifyTask(collection, req).getOrElse(taskFail()).fork(
        err => res.sendStatus(401),
        verify =>  {
          if (verify) {
            taskLiftUpdate(
              getParamColl(req),
              S.prop('body', req) 
            ).getOrElse(taskFail()).fork(
              err =>  res.sendStatus(404),
              data => res.json(pickNoAdminKey(data))
            );
          } else {
            res.sendStatus(404)
          }
        }
      )

    })
    .delete(jsonParser, function(req, res) {

      const taskLiftDelete = R.lift(taskDeleteCollection(collection)); 

      verifyTask(collection, req).getOrElse(taskFail()).fork(
        err => res.sendStatus(401),
        verify =>  {
          if (verify) {
            taskLiftDelete(getParamColl(req)).getOrElse(taskFail()).fork(
              err =>  res.sendStatus(404),
              data => res.json({status:'ok'})
            )
          } else {
            res.sendStatus(404)
          }
        }
      )

    })

  router.route('/:collectionid/games')
    .get(jsonParser, function(req, res) {
      
      const taskLiftRead = R.lift(taskReadCollection(collection)); 

      taskLiftRead(
        getParamColl(req)
      ).getOrElse(taskFail()).fork(
        err =>  res.sendStatus(404),
        data => res.json(propGames(data).getOrElse([]))
      );

    })
    .post(jsonParser, function(req, res) {

      // getDataGame :: {o} -> maybe s 
      const getDataGame = (req) => S.path(['body', 'gameid'], req);
      const taskLiftAddGame = R.lift(taskCollectionAddGame(collection, boardgame));

      verifyTask(collection, req).getOrElse(taskFail()).fork(
        err => res.sendStatus(401),
        verify =>  {
          if (verify) {
            taskLiftAddGame(
              getParamColl(req),
              getDataGame(req)
            ).getOrElse(taskFail()).fork(
              err =>  res.sendStatus(404),
              data => res.json(S.prop('games', data).getOrElse([]))
            );
          }
        }
      );
    })
    .patch(jsonParser, function(req, res) {

      const taskUpdateBgg = R.lift(updateBggCollection(boardgame, collection));
      taskUpdateBgg(
        getParamColl(req)
      ).getOrElse(taskFail()).fork(
        err => res.sendStatus(404),
        data => {
          res.json(S.prop('games', data).getOrElse([]));
        }
      );

    });

  router.route('/:collectionid/games/:gameid')
    .delete(jsonParser, function(req, res) {

      // getParamGame :: {o} -> maybe s 
      const getParamGame = (req) => S.path(['params', 'gameid'], req);
      const taskLiftRemoveGame = R.lift(taskCollectionRemoveGame(collection));

      verifyTask(collection, req).getOrElse(taskFail()).fork(
        err => res.sendStatus(401),
        verify =>  {
          if (verify) {
            taskLiftRemoveGame(
              getParamColl(req),
              getParamGame(req)
            ).getOrElse(taskFail()).fork(
              err => res.sendStatus(404),
              data => res.json(S.prop('games', data).getOrElse([]))
            );
          }
        }
      );
    })

  return router;

}; 
exports.setupRouter = setupRouter;
