var R = require('ramda');
var compose = R.compose,
    curry   = R.curry,
    head    = R.head,
    isNil   = R.isNil,
    lift    = R.lift,
    path    = R.path,
    prop    = R.prop,
    map     = R.map;

var util = require('util');
var log = require('./log.js');
var expressHelper = require('./expresshelper');
var crypto = require('crypto');

var LocalStrategy = require('passport-local');

var accounts = require('./accounts.js');

var Maybe = require('data.maybe');
var Task = require('data.task');

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});



// maybeNotNull :: x -> Maybe(x)
const maybeNotNull = (x) => isNil(x) ? Maybe.Nothing() : Maybe.of(x);

// safeHead :: [a] -> Maybe(a)
const safeHead = function(xs) {
  return (xs.length == 0) ? Maybe.Nothing() : Maybe.of(xs[0]);
};

// safeProp :: a -> b -> Maybe(a.b)
const safeProp = curry(function(x, o) {
  return Maybe.of(o[x]);
});

// safePath :: a -> [l, m] -> Maybe(a.l.m)
const safePath = curry(function(x, o) {
  if (path(x, o) == undefined) {
    return Maybe.Nothing()
  } else {
    return Maybe.of(o).map(path(x));
  }
});

// ============== URL ROUTES =================

// routeLogins :: Express -> Passport -> Redirect
var routeLogins = function(expressApp, passport) {

    // routeGetsExt :: (s -> t -> l -> d) => render
    var routeGetsExt = expressHelper.routeGetRequestExtended(expressApp);

    routeGetsExt('/login', 'login', 'Log in for Roll for Group board gamer meet up site.', 'Board gamers can log in to Roll for Group to play board games, find local board game events and find board game players.');

    routeGetsExt('/signup', 'signup', 'Create an account for the Roll for Group board gamer meet up site.', 'Sign up for an account to find other local board gamers, join board game events, and play board games.');

    routeGetsExt('/loginfailure', 'loginfailure', 'Try again to log in to the Roll for Group site and play board games.', 'Try again to sign in and find board gamers to play board games and meet up with.');

    expressApp.get('/login/facebook', passport.authenticate('facebook', { scope: [ 'email' ] }));
    expressApp.get('/login/loginfailure', passport.authenticate('facebook', { scope: [ 'email' ] }));

    expressApp.get('/login/facebook/return', passport.authenticate('facebook', { failureRedirect: '/loginfailure' }), function(req, res) {
      res.redirect('/collection');
    });

	expressApp.get('/auth/google',
	  passport.authenticate('google', { scope: 
		[ 'https://www.googleapis.com/auth/plus.login',
		, 'https://www.googleapis.com/auth/plus.profile.emails.read' ] }
	));
	 
	expressApp.get( '/auth/google/callback', 
		passport.authenticate( 'google', { 
			successRedirect: '/',
			failureRedirect: '/loginfailure'
	}));

};
exports.RouteLogins = routeLogins;


// postLogin :: Express -> Passport -> Redirect
var postLogin = function(expressApp, passport) {
    expressApp.post('/login', passport.authenticate('local', {
        successReturnToOrRedirect: '/collection',
        failureRedirect: '/loginfailure'
      })
    )
};
exports.PostLogin = postLogin;


// cryptotoken :: number => Task(string) 
const cryptotoken = (size) => new Task((reject, resolve) => {
    crypto.randomBytes(size, function(err, buff) {
        if (err) reject(err)
        resolve(buff.toString('hex'));
    });
}); 
exports.CryptoToken = cryptotoken;

// postForgot :: Express -> Passport -> Redirect
var postForgot = function(expressApp, account, transporter, emailFrom, siteUrl) {
    expressApp.post('/forgot', function(req, res, next) {

        email = safePath(['body', 'email'], req); 
        sendEmail = lift(sendEmailTransport(transporter, account, emailFrom, siteUrl, email));

        sendEmail(cryptotoken(60)).fork(
            err => {
                res.redirect('/forgot?error=invalid')
            },
            emailtask => {
                emailtask.fork(
                    err => res.redirect('/forgot?error=invalid'),
                    emailsent => res.render('reset', {info: 'An email has been sent to ' + emailsent + ' with further instructions'})
                )
            }
        ); 

    });
};
exports.PostForgot = postForgot;


// routeVerify :: Express -> Passport -> Redirect
var routeVerify = function(expressApp, account) {

  // not sure why this goes to the failure, when it passes
  expressApp.get('/verify', function(req, res) {
    accounts.verifyAccount(account, req.query.authToken);
    res.redirect('login');
  });

};
exports.RouteVerify = routeVerify;


// setupPassportStrategies :: Passport -> Account -> Passport
var setupPassportStrategies = function(passport, account) {

  passport.serializeUser(function(user, cb) {
    cb(null, user);
  });

  passport.deserializeUser(function(obj, cb) {
    cb(null, obj);
  });

  // setup passport
  passport.use(account.createStrategy());

  // use static authenticate method of model in LocalStrategy
  passport.use(new LocalStrategy({
    usernameField: 'form-username',
    passwordField: 'form-password',
    session: true
  },
    account.authenticate()
  ));

  passport.serializeUser(account.serializeUser());
  passport.deserializeUser(account.deserializeUser());

  return passport;

};
exports.SetupPassportStrategies = setupPassportStrategies;
