var schedule = require('node-schedule'); 
var path = require('path');
var R = require('ramda');
var moment = require('moment');

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

var dbUri = nconf.get('database:uri');
// var dbCollectionUri = nconf.get('collections:database:uri');

var Promise = require('bluebird');
var S       = require('./lambda.js');
var log = require('./log.js');

// setup webpages
var app = require('express')();
var http = require('http').Server(app);

var expressHelper = require('./expresshelper');

var mongoose = require('mongoose');
var mongo = require('./mongoconnect.js');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var Account = require('./account.js');
var Accounts = require('./accounts.js');

var Boardgame = require('./boardgame.js');
var Boardgames = require('./boardgames.js');

var Collection = require('./collection.js');
var Collections = require('./collections.js');

var collection_page = require('./collection_page.js');
var boardgamesAjax = require('./boardgames_service.js');

var passport = require('passport');
var session = require('express-session');

const mongoStore = require('connect-mongo')(session);
const redisStore = require('connect-redis')(session);

mongoose.Promise = global.Promise;
mongo.ConnectDatabase(mongoose, dbUri);

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// ============== PASSPORT STRATEGIES ===============

// updatePlayers
const updatePlayers = function() {

  // createCollection :: {o} -> task [{o}]
  const createCollection = (user) => {
    const liftCreate = R.lift(Collections.createBggUserCollections(Collection, R.__, S.path(['profile', 'bggname'], user).getOrElse('')));
    return liftCreate(S.prop('pageslug', user))
      .getOrElse(taskFail())
  };

  Account.find({
    "gamesCollectionCreated": false,
    "createdAt": { $lt: moment().add(-1, 'h').utc().valueOf() } 
  })
    .limit(100)
    .exec()
    .then((x) => {
      R.sequence(Task.of, x.map(createCollection)).fork(
        err =>  {},
        data => {} 
      );
    })

};

// schedule
schedule.scheduleJob('0 * * * * *', function(){
  Boardgames.UpdateNewGames(Boardgame, 5); 
  Boardgames.UpdateFailLookupGames(Boardgame, 15); 
  updatePlayers();
  Collections
    .findAndUpdateABggCollection(Boardgame, Collection)
    .fork(
      err => {},
      data => {}
    );
});

expressHelper.SetupWebpages( //395
  app, 
  passport, 
  (nconf.get('session:type') == "redis")
    ? new redisStore({url: nconf.get('session:uri')})
    : new mongoStore({url: nconf.get('session:uri')})
);

//expressHelper.SetupWebpages(app, passport);
expressHelper.StaticRouteFolders(app, ["img", "css", "js"]);
collection_page.routeGetCollection(app, 'collection');

var boardgamesrouter = boardgamesAjax.SetupRouter();
app.use('/api/v1/boardgames', boardgamesrouter);

var collectionrouter = Collections.setupRouter(Boardgame, Collection);
app.use('/api/v1/collections', collectionrouter);

var login = require('connect-ensure-login');

var login_page = require('./login_page.js');
login_page.SetupPassportStrategies(passport, Account); // 140

login_page.RouteLogins(app, passport); //287
login_page.RouteVerify(app, Account);  //288
login_page.PostLogin(app, passport);   //337

var httpPort = nconf.get('service:collections:port');

// ============== START SERVER ===============
http.listen(httpPort, function(){
  console.log('listening on *:' + httpPort);
});

// ============== END SERVER FOR TESTS =======
closeServer = function() {
  console.log("stopping webserver...");
  http.close(function() {

    console.log("stopping mongoose now...");
    mongo.CloseDatabase(mongoose);

  });
};
exports.closeServer = closeServer;

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', closeServer);
