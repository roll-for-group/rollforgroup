const Task = require('data.task');
const Maybe = require('data.maybe');
const Either = require('data.either');

var Promise = require('bluebird');
var R = require('ramda');

var collection = require('./collection.js');
var collections = require('./collections.js');

var log = require('./log.js');
var S = require('./lambda.js');
var expressHelper = require('./expresshelper');

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// slashFile :: String -> String
var slashString = (file) => "/" + file;

// renderData :: Render -> String -> Object -> Render 
const renderData = R.curry((res, template, data) => {
  res.render(template, data);
})

// maybeQuery :: {o} -> maybe {s}
const maybeQuery = R.compose(
  R.map(R.objOf('collectionid')),
  S.path(['query', 'id'])
);
exports.maybeQuery = maybeQuery;

// maybeAdminKey :: {o} -> maybe s
const maybeAdminKey = R.compose(
  R.map(R.objOf('adminkey')),
  S.path(['query', 'adminkey'])
);
exports.maybeAdminKey = maybeAdminKey;

// maybeBggname :: {o} -> maybe {m} 
const maybeBggname = (req) => {
  return S.path(['user', 'profile', 'bggname'], req)
    .map(R.objOf('bgguser'));
}
exports.maybeBggname = maybeBggname;

// maybeBuildNavs :: {o} -> maybe {m} 
const maybeBuildNavs = (req) => {
  return S.path(['user', 'email'], req)
    .map(expressHelper.BuildNavbar('collection'))
    .map(R.objOf('navbarul'));
}
exports.maybeBuildNavs = maybeBuildNavs;


// routeGetCollection :: Express -> s -> 0
var routeGetCollection = R.curry(function (expressApp, url) {

  // whenSafePath :: {o} -> {m}
  const whenSafePath = (obj) => {

    var name = S.path(['user', 'profile', 'name'], obj)
      .map(R.objOf('name'));

    var pageslug = S.path(['user', 'pageslug'], obj)
      .map(R.objOf('pageslug'));

    return name
      .map(R.merge)
      .ap(pageslug)
      .map(R.objOf('user'))
      .getOrElse({navigation: {nouser:true}});
  }

  // mergeName :: {o} -> {o}
  const mergeName = R.curry((reqObj, mergeObj) => R.compose(
    R.merge(mergeObj), 
    whenSafePath
  )(reqObj));

  expressApp.get(slashString(url), function(req, res) {
    var renderit = R.compose(
      renderData(res, 'collection'),
      R.merge({boardgamesURL: nconf.get('service:boardgames:weburl')}),
      R.merge(maybeBggname(req).getOrElse({})),
      R.merge(maybeBuildNavs(req).getOrElse({})),
      R.merge(expressHelper.BuildNavbarHead('Profile')),
      mergeName(req),
      R.merge(maybeQuery(req).getOrElse({}))
    );

    // detirmine if logged in user has a key, what is it?
    const liftVerifyUserAccount = R.lift(collections.verifyUserAccount(collection));

    liftVerifyUserAccount(
      S.path(['query', 'id'], req),
      S.path(['user', 'pageslug'], req),
      S.path(['user', 'key'], req)
    ).getOrElse(taskFail()).fork(
      err => renderit(maybeAdminKey(req).getOrElse({})),
      data => {
        if (data) {
          R.lift(collections.taskReadCollection(collection))
            (S.path(['query', 'id'], req))
            .getOrElse(taskFail()).fork(
              err => renderit(maybeAdminKey(req).getOrElse({})),
              data => {
                renderit({adminkey: data.adminkey});
              }
            )
        } else {
          renderit(maybeBuildNavs(req).getOrElse({}));
        }
      }
    );

    return 0; // success

  });
});
exports.routeGetCollection = routeGetCollection;
