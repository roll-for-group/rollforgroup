var mongoose = require('mongoose');

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

var dbCollectionUri = nconf.get('service:boardgames:database:uri');
var conn = mongoose.createConnection(dbCollectionUri);

var Schema = mongoose.Schema;

/*
var mongo = require('./mongoconnect.js'),
  mongoose = mongo.Mongoose,
  Schema = mongoose.Schema;
*/

var Boardgame = new Schema({
  id          : Number, 
  name        : String,
  type        : String,
  description : String,
  thumbnail   : String,
  yearpublished: Number,
  subtype     : String,
  objecttype  : String,
  active      : Boolean,
  activestate : String,
  minplaytime : Number,
  maxplaytime : Number,
  minplayers  : Number,
  maxplayers  : Number,
  playercounts : [{
    playercount:  String,
    status:       String
  }],
  bggrank     : String,
  avgrating   : Number,
  weight      : Number,
  family      : [String],
  categories  : [String],
  mechanics   : [String],
  expansions  : [{value: String, id: Number, thumbnail: String}],
  artists     : [String],
  publishers  : [String],
  designers   : [String],
  implementation : [{value: String, id: Number}],
  wanttoplay  : {type: Number, default: 0},
  own         : {type: Number, default: 0},
  teach       : {type: Number, default: 0},
  matchcount  : {type: Number, default: 0},
  avoid       : {type: Number, default: 0},
  active      : Boolean,
  activestatus : String,
  updatecache : {type: Boolean, default: false},
  bggcache    : Schema.Types.Mixed,
  videos      : [{
    id: Number,
    title: String,
    category: String,
    language: String,
    link: String,
    username: String,
    userid: Number,
    postdate: Date 
  }],
  link        : Schema.Types.Mixed,
  asin        : String,
  confirmasin : {type: Boolean, default: false},
  amazon: [{
    domain  : String,
    lastupdated: Date,
    rrp:        Number,
    saleprice:  Number,
    formatrrp:  String,
    formatsaleprice: String,
    currency:   String,
    title   :   String,
    pageUrl :   String,
    offerListingId: String,
    quantity:   Number,
    asin    :   String 
  }],
  updatecounter:  Number
},
{
  timestamps: true
});

Boardgame.index({id: 1}, {unique: true});
Boardgame.index({type: 1});
Boardgame.index({name: 1});

Boardgame.index({
  name        : "text", 
  description : "text",
  family      : "text",
  publishers  : "text",
  mechanics   : "text",
  artists     : "text",
  designers   : "text",
  //"implementation.value" : "text",
  //"expansions.value" : "text"
}, {
  weights: {
    name        : 10, 
    description : 1,
    family      : 3,
    publishers  : 2,
    mechanics   : 1,
    artists     : 2,
    designers   : 5,
    //"implementation.value" : 7,
    //"expansions.value" : 4 
  }
});
Boardgame.index({name: 1, yearpublished: 1}, {unique: true});

module.exports = conn.model('Boardgame', Boardgame);
