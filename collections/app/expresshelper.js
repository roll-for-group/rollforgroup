// expresshelper.js
var favicon = require('serve-favicon');
var express = require('express');
var exphbs = require('express-handlebars');

// functions that help setting up static webpages
var R = require('ramda');
var S = require('./lambda.js');

var assoc = R.assoc,
    curry = R.curry,
    lift = R.lift, 
    map = R.map,
    pathEq = R.pathEq,
    objOf = R.objOf,
    zipObj = R.zipObj;

var path = require('path');
var log = require('./log.js');

var exports = module.exports = {};

// =============== STATIC ROUTES ================
//	pass an express object and array of files and folders to StaticRouteFolders()
//		these files and folders from the array will be accessed via the website url
//		and will be served from the ./public/* directory
//
//	StaticRouteFolders(express, ["myfile"])
//	i.e. http://myurl/myfile ---served_from---> __dirname/public/myfile
//
// ==============================================

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task');


const safePath = curry((arraypath, a) => ((R.path(arraypath, a) == undefined)) ? Maybe.Nothing() : Maybe.of(R.path(arraypath, a))); 

// slashFile :: String -> String
var slashString = (file) => "/" + file;
exports.slashString = slashString();

// publicFolder :: String -> String -> String
var publicFolder = R.curry(function (localpath, file) {
	return path.join(localpath, "/public/", file);
});

// localFilePath :: String -> String
var localFilePath = publicFolder(__dirname);

// routeAppRequests :: Express -> String -> a
var routeUseRequests = R.curry (function (expressApp, asset) {
	expressApp.use(slashString(asset), express.static(localFilePath(asset)));
	return 0;  // success
});

// routeFolderes :: Express -> Array -> Boolean
exports.StaticRouteFolders = function(expressApp, foldersArray) {

  // routeUse :: String -> a
  var routeUse = routeUseRequests(expressApp);

  // maps a set of folders from an array so that they are accessible
  foldersArray.map(routeUse);
  return true;

}

// routeGetRequestExtended :: Express -> String -> String -> a
exports.routeGetRequestExtended = R.curry(function (expressApp, url, handlebar_template, title, description) {
	expressApp.get(url, function(req, res) {
		res.render(handlebar_template, {webtitle: title, metadescription: description})
	});
	return 0; // success
});

// routeGetRequests :: Express -> String -> String -> a
exports.routeGetRequests = R.curry(function (expressApp, url, handlebar_template) {
	expressApp.get(url, function(req, res) {
		res.render(handlebar_template)
	});
	return 0; // success
});

// addReqUserToData :: JSONObject -> ReqObject -> JSONObject
var addReqUserToData = R.curry(function(req, data) {

  var newdata = data;
  try {
    if (typeof (req.user) != "undefined") {
      if (typeof (req.user.facebook) === "undefined") {
        // build a non-facebook user
        data.user = {
          email: req.user.email,
          name: req.user.profile.name
        };
      } else {
        // build a facebook user
        data.user = {
          email: req.user.facebook.email,
          name: req.user.facebook.name
        };
      };
      data.user.bggname = req.user.profile.bggname;
    };

  } catch (err) {
      console.log(err);
  }

  return newdata;

});
exports.AddReqUserToData = addReqUserToData;

// lookupUsername :: Object -> Object
var lookupUsername = function(userObject) {

  // whenFacebook :: {u} => Bool
  var whenFacebook = R.compose(R.has('facebook'), JSON.parse, JSON.stringify);

  // builduser :: (e, n, a) => {u}
  const builduser = lift((email, name, avatar) => zipObj(['name', 'email', 'avataricon'], [name, email, avatar]));

  const safeAvatar = curry((arraypath, a) => ((R.path(arraypath, a) == undefined)) 
    ? Maybe.of('/img/defaultavatar.jpg') 
    : Maybe.of(R.path(arraypath, a))); 

  const safeName = curry((arraypath, a) => ((R.path(arraypath, a) == undefined)) 
    ? Maybe.of('') 
    : Maybe.of(R.path(arraypath, a))); 

  if (whenFacebook(userObject)) {
    // build a facebook user
    return builduser(safePath(['facebook', 'email'], userObject), safePath(['facebook', 'name'], userObject), safeAvatar(['facebook', 'photo'], userObject)).getOrElse({});

  } else {
    // build a non-facebook user
    return builduser(safePath(['email'], userObject), safeName(['profile', 'name'], userObject), safeAvatar(['profile', 'avataricon'], userObject)).getOrElse({});

  };

};
exports.LookupUsername = lookupUsername


// setupWebpages :: Express -> Express
var setupWebpages = function(expressApp, passport, store) {

	expressApp.engine('handlebars', exphbs({defaultLayout: 'main'}));
	expressApp.set('view engine', 'handlebars');
	expressApp.use(require('morgan')('combined'));
	expressApp.use(require('cookie-parser')('just rolling for a cookie'));
	expressApp.use(require('body-parser').urlencoded({ 
    extended: true }
  ));
	expressApp.use(require('express-session')({ 
    secret: 'rolling for group session key', 
    store: store,
    resave: true, 
    saveUninitialized: true 
  }));

	expressApp.use(passport.initialize());
	expressApp.use(passport.session());
	expressApp.use(favicon(path.join(__dirname, '..', 'app', 'public','img','favicon.ico')));

    return expressApp;

}
exports.SetupWebpages = setupWebpages


// buildNavbarHead :: (s -> u) => {o}
const buildNavbarHead = curry(function (headtext) {
  return {
    navhead: {
      dropdown: {
        text: headtext,
        menu: [{
          text: 'Local Players',
          href: 'events'
        }]
      }
    }
  }
});
exports.BuildNavbarHead = buildNavbarHead;



// buildNavbar :: s -> u => {o}
const buildNavbar = curry(function (link, email) {

  const buildEmailLink = (email) => 'users?email=' + email;

  // const highlightSelected = (pageslug) => 'users?id=' + pageslug;
  const highlightSelected = curry((link, obj) => {

    // hasSublink :: s -> {o} -> bool
    const hasSublink = curry((link, obj) => S.path(['a', 'sublinks'], obj)
      .map(R.contains(link))
      .getOrElse(false)
    );

    return (pathEq(['a', 'link'], link, obj)) 
      ? assoc('activeclass', true, obj) 
      : (hasSublink(link, obj)) 
        ? assoc('activeclass', true, obj)
        : obj;
  });

  const links = [ 
    {a: {link: 'events', text: 'Events', sublinks:['events', 'schedule', 'history', 'hostgame']}},
    {a: {link: 'players', text: 'Players', sublinks:['players', 'lounge', 'mail', 'mail?mailtype=outbox']}},
    {a: {link: 'games', text: 'Board Games'}},
    {a: {link: buildEmailLink(email), text: 'Achievements'}},
    {a: {link: 'http://blog.rollforgroup.com', text: 'Blog'}}
  ];

  return objOf('li', map(highlightSelected(link), links));

});
exports.BuildNavbar = buildNavbar;
