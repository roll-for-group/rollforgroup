var mongoose = require('mongoose');

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

var dbCollectionUri = nconf.get('service:collections:database:uri');
var conn = mongoose.createConnection(dbCollectionUri);

var Schema = mongoose.Schema;
var uuid = require('node-uuid');
var shortid = require('shortid');

var Collection = new Schema({
  key         : {
    type    : String, 
    default : shortid.generate, 
    index   : 1
  },
  adminkey    : {
    type    : String, 
    default : uuid.v4,
    index   : 1
  },
  userkey     : {
    type    : String,
    index   : 1
  },
  name        : String,
  description : String,
  thumbnail   : String,
  games       : [{
    id          : Number, 
    name        : String,
    description : String,
    thumbnail   : String,
    yearpublished: Number,
    subtype     : String,
    objecttype  : String,
    minplaytime : Number,
    maxplaytime : Number,
    minplayers  : Number,
    maxplayers  : Number,
    playercounts : [{
      playercount:  String,
      status:       String
    }],
    bggrank     : Number,
    avgrating   : Number,
    weight      : Number,
    family      : [String],
    categories  : [String],
    mechanics   : [String],
    expansions  : [{value: String, id: Number}],
    artists     : [String],
    publishers  : [String],
    designers   : [String],
    implementation : [{value: String, id: Number}],
    bggsync     : Boolean,
    bggadded    : Date,
    numplays    : Number,
    userrank    : Number,
    tags        : [String],
    comments    : String
  }],
  isbgg         : Boolean,
  bggname       : String,
  bggtype       : String,
  bgglistnumber : Number,
  bgghassearched  : {
    type      : Boolean,
    default   : false
  },
  viewcount: {type: Number, default: 0}
},
{
  timestamps: true
});

Collection.index({userkey: 1});
Collection.index({key: 1}, {unique: true});

//module.exports = mongoose.model('Collection', Collection);
module.exports = conn.model('Collection', Collection);
