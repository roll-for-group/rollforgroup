/**************************************************
**
** an ajax service for all things to do with boardgames 
**
** todo: seperate into microservice
** documentation as api
**
*************************************************/

var express = require('express');
var bodyParser = require('body-parser')

var util = require('util');
var R = require('ramda');
var compose = R.compose,
    curry   = R.curry,
    isEmpty = R.isEmpty,
    isNil   = R.isNil,
    has     = R.has,
    head    = R.head,
    lift    = R.lift,
    map     = R.map,
    path    = R.path,
    prop    = R.prop,
    zipObj  = R.zipObj;

var Promise = require('bluebird');
var log = require('./log.js');
var exports = module.exports = {};

var boardgame   = require('./boardgame.js');
var boardgames  = require('./boardgames.js');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var S           = require('./lambda.js');

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// createBoardgameJson :: {o} -> {o}
const createBoardgameJson = R.pick(['id', 'name', 'thumbnail', 'minplaytime', 'maxplaytime', 'minplayers', 'maxplayers', 'yearpublished', 'bggrank', 'avgrating', 'weight', 'description', 'playercounts', 'expansions']);
exports.CreateBoardgameJson = createBoardgameJson;

// lookupGamename :: String => [String]
var lookupGamename = R.curry(function(includeExpansions, name) {
	return new Task(function (reject, resolve) {
    const resolvegame = R.compose(
      resolve, 
      R.map(createBoardgameJson)
    );

    boardgames
      .SearchBoardgameName(boardgame, name, includeExpansions)
      .then((x) => {
        resolvegame(x);
      }).catch(reject);  

  });
});
exports.LookupGamename = lookupGamename;


// lookupGameId :: String => [String]
var lookupGameId = function(id) {
	return new Task(function (reject, resolve) {

    boardgames.TaskFindBoardgame(boardgame, id).fork(
      err => reject(err),
      x => {
        var resolvegame = compose(
          resolve, 
          map(createBoardgameJson),
          R.of
        )
        resolvegame(x);
      }
    );  

  });
};
exports.LookupGameId = lookupGameId;

/*
// lookupGamesBySlugListtypeLimit :: String => [String]
var lookupGamesBySlugListtypeLimit = function(pageslug, listtype, limit) {
	return new Task(function (reject, resolve) {

    boardgames.TaskFindBoardgame(boardgame, id).fork(
      err => reject(err),
      x => {
        var resolvegame = compose(
         resolve, 
          map(createBoardgameJson),
          R.of
        )
        resolvegame(x);
      }
    );  

  });
};
exports.LookupGamesBySlugListtypeLimit = lookupGamesBySlugListtypeLimit;
*/


// setupRotuer 
var setupRouter = function() {

  var router = express.Router();
  var jsonParser = bodyParser.json();

  // maybeGameId :: o => maybe a;
  const maybeGameId = S.path(['params', 'gameid']);

  // filterValidData :: [a] => [a]
  const filterValidData = (a) => R.filter(R.has('id'), a);

  router.route('/:gameid')
    .get(function(req, res) {
      maybeGameId(req)
        .map(boardgames.TaskFindBoardgame(boardgame))
        .getOrElse(taskFail())
        .fork(
          err => {
            res.json({data:'error'});
          },
          data => {
            res.json(boardgames.pickGameFields(data));
          }
        );

    });

  router.route('/:gameid/expansions')
    .get(function(req, res) {
      maybeGameId(req)
        .map(boardgames.getExpansions(boardgame))
        .getOrElse(taskFail())
        .fork(
          err => {
            res.json({data:'error'});
          },
          data => {
            res.json(data);
          }
        );

    });

  router.route('/:gameid/videos')
    .get(function(req, res) {
      maybeGameId(req)
        .map(boardgames.getVideos(boardgame))
        .getOrElse(taskFail())
        .fork(
          err => {
            res.json({data:'error'});
          },
          data => {
            res.json(data);
          }
        );

    });


  router.route('/')
    .get(function(req, res) {

			// isExpansions :: o -> bool
			const isExpansions = (req) => 
				S.path(['query', 'expansions'], req).isJust;

      var namesearch = (req) => S
				.path(['query', 'name'], req)
        .chain(lookupGamename(isExpansions(req)));  // TODO test this section

      var nameid = (req) => S.path(['query', 'id'], req)
        .chain(lookupGameId);

      if (has('fork', namesearch(req))) {
        namesearch(req).fork(
          (err) => res.sendStatus(500),
          (data) => res.send(data)
        )

      } else if (has('fork', nameid(req))) {
        nameid(req).fork(
          (err) => res.sendStatus(500),
          (data) => res.send(data)
        )

      } else {
        res.sendStatus(400)
      }
    })

  return router;

}; 
exports.SetupRouter = setupRouter;
