// var exports = module.exports = {};
var R = require('ramda');

var isEmpty = R.isEmpty,
  isNil = R.isNil, 
  objOf = R.objOf,
  prop = R.prop,
  path = R.path;

var S = require('./lambda.js');
var account = require('./account.js');
var accounts = require('./accounts.js');
var log = require('./log.js');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

// safeHead :: [a] -> Maybe(a)
const safeHead = xs => Maybe.of(xs[0]);

// safeProp :: (a -> b) => Maybe(b[a])
const safeProp = curry((x, o) => (isEmpty(o) || isNil(o) || isNil(o[x])) ? Maybe.Nothing() : Maybe.of(o[x]));

// safePath :: ([a] -> b) => Maybe(b[a])
const safePath = curry((x, o) => (isEmpty(o) || isNil(o) || isNil(path(x, o))) ? Maybe.Nothing() : Maybe.of(path(x, o)));

// eitherPath :: ([a] -> b) => Maybe(b[a])
const eitherPath = curry((x, o) => (isEmpty(o) || isNil(o) || isNil(path(x, o))) ? Either.Right('') : Either.Left(path(x, o)));


// updateFacebookPicture :: Model(Account) -> Model(Account)
var updateFacebookPicture = function(user) {

	var updatedUser = user;
	if (safePath(['facebook', 'photo'], user).isJust) {
		updatedUser.profile.avataricon = user.facebook.photo;
	}
	return updatedUser;

};
exports.updateFacebookPicture = updateFacebookPicture;


// updateGooglePicture :: Model(Account) -> Model(Account)
var updateGooglePicture = function(user) {

	var updatedUser = user;
	//if (!R.isNil(user.google.photo)) {
	if (safePath(['google', 'photo'], user).isJust) {
		updatedUser.profile.avataricon = user.google.photo;
	}
	return updatedUser;

};
exports.updateGooglePicture = updateGooglePicture;


// addFacebookUser :: Model -> String -> Schema -> Account
var addFacebookUser = function(newAccount, Token, ProfileSchema) {

	return new Promise(function(fulfill, reject) {

		newAccount.facebook.id = ProfileSchema.id;
		newAccount.facebook.token = Token;
		newAccount.facebook.name = ProfileSchema.name.givenName + ' ' + ProfileSchema.name.familyName;
		newAccount.facebook.email = ProfileSchema.emails[0].value;
		newAccount.facebook.photo = ProfileSchema.photos[0].value;
		newAccount.email = ProfileSchema.emails[0].value;

		newAccount.profile.name = ProfileSchema.name.givenName + ' ' + ProfileSchema.name.familyName;
		newAccount.profile.avataricon = ProfileSchema.photos[0].value;
		newAccount.profile.avataricontype = 'facebook';

		newAccount.save(function(err) {

			if (err)
				reject(err);

      accounts.sendWelcomeMail(ProfileSchema.emails[0].value, ProfileSchema.name.givenName + ' ' + ProfileSchema.name.familyName).fork(
        err   => log.clos('err', err),
        data  => {} 
      );

			fulfill(newAccount);

		})
	})
}
exports.addFacebookUser = addFacebookUser;

// addGoogleUser :: Model -> String -> Schema -> Account
var addGoogleUser = function(newAccount, Token, ProfileSchema) {

	return new Promise(function(fulfill, reject) {

		newAccount.google.id = ProfileSchema.id;
		newAccount.google.token = Token;
		newAccount.google.name = ProfileSchema.name.givenName + ' ' + ProfileSchema.name.familyName;
		newAccount.google.email = ProfileSchema.emails[0].value;
		newAccount.google.photo = ProfileSchema.photos[0].value;
		newAccount.google.gender = ProfileSchema.gender;

		newAccount.email = ProfileSchema.emails[0].value;

		newAccount.profile.name = ProfileSchema.name.givenName + ' ' + ProfileSchema.name.familyName;
		newAccount.profile.avataricon = ProfileSchema.photos[0].value;
		newAccount.profile.avataricontype = 'google';

		newAccount.save(function(err) {
			if (err)
				reject(err);
      
      accounts.sendWelcomeMail(ProfileSchema.emails[0].value, ProfileSchema.name.givenName + ' ' + ProfileSchema.name.familyName).fork(
        err   => log.clos('err', err),
        data  => {} 
      );
			fulfill(newAccount);

		})
	})
}
exports.addGoogleUser = addGoogleUser;


// facebookLogin :: Model -> String -> Schema -> (Promise -> Account)
var facebookLogin = function(Users, accessToken, profile) {
	return new Promise(function (fulfill, reject) {

    const getMail = R.compose(
      R.chain(S.prop('value')), 
      R.chain(S.head), 
      S.prop('emails')
    );

    Users.findOne({ 'facebook.id' : profile.id }, function(err, user) {

      if (err) {
        reject(err);

      } else {
        if (user) {

          // found existing user
          var updatedUser = updateFacebookPicture(user)
          fulfill(updatedUser);

        } else {

          if (getMail(profile).isJust) {


            // create a new user
            Users.findOne({ 'email': profile.emails[0].value }, function(err,user) {
              if (err) {
                reject(err);
              } else {
                user = (user) 
                  ? user 
                  : new account();
                addFacebookUser(user, accessToken, profile).then(fulfill, reject);
              }
            })

          } else {
            reject('Permission to retrieve users email not granted. Roll for Group identifies each user by their email address, you will need to allow Roll for Group permission to your email to log in.');
          }

        }
      }
    });
	})
};
exports.facebookLogin = facebookLogin;


// googleLogin :: Model -> String -> Schema -> (Promise -> Account)
var googleLogin = function(Users, accessToken, profile) {
	return new Promise(function (fulfill, reject) {
    Users.findOne({ 'google.id' : profile.id }, function(err,user) {

      if (err) {
        reject(err);
      }

      if (user) {
        // found existing user
        var updatedUser = updateGooglePicture(user)
        fulfill(updatedUser);

      } else {

        // create a new user
        Users.findOne({ 'email': profile.emails[0].value }, function(err,user) {
          if (err) {
            reject(err);

          } else {
            user = (user) 
              ? user 
              : new account();
            addGoogleUser(user, accessToken, profile).then(fulfill, reject);

          }
        })
      }
    });
	})
};
exports.googleLogin = googleLogin;
