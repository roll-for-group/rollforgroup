#!/bin/bash

mkdir app/public/docs

FILES=*.md
#for f in $FILES
for f in docs-api/$FILES
do
  # extension="${f##*.}"
  filename="${f%.*}"
  echo "Converting $f to $filename.html"
  `pandoc -f markdown $f -t html -o $filename.html --css ../css/support.css`
  cp -a docs-site/*.html app/public/docs
  # uncomment this line to delete the source file.
  # rm $f
done
