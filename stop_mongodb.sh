#!/bin/bash

docker stop RFG-mongo
docker rm RFG-mongo

docker stop RFG-mongo-emails
docker rm RFG-mongo-emails

docker stop RFG-collections
docker rm RFG-collections
