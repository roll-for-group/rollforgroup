% Premium Gems

## What Are Premium Gems?  
Premium Gems are like gift cards, and can be used to gift premium access to other players. Each gem gives the receiving player receives one month of premium membership. Premium membership will give the player a premium badge, and various perks while using the website.

## Acquiring Premium Gems
There are two ways to receive gems:

* A premium gem is gained every five hosting levels a player earns.
* Players may additionally purchase additional gems.

## How Do I Gift a Gem?
The gifting player must have at least one gem to gift to another player. When the gifting player has gems, on viewing another players profile page a "Gift Gems" button will be displayed along with the total number of gems owned.

![Gems Owned](../img/docs/gems-giftbutton.png)

The receiving player will receive a notification with the sender's name, detailing who tipped them the membership.

![Player Notification](../img/docs/gems-notification.png)

The player now has the premium membership for one month and will receive all associated premium perks. When gifting a player who already has premium membership, the players existing membership will be extended by one month.

![Premium Player](../img/docs/gems-premiumember.png)

## Gem Expiry  
A gem can only be held for 12 months, after which it will expire if it is not gifted to another player. This restriction is placed on Roll for Group by the underwriting payment processing company.
