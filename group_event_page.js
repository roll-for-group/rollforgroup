var account = require("../app/models/account.js");
var accounts = require("../app/models/accounts.js");
var match = require("../app/models/match.js");
var matches = require("../app/models/matches.js");

var Promise = require("bluebird");
var R = require("ramda");

var chain   = R.chain,
    compose = R.compose, 
    converge = R.converge,
    curry   = R.curry, 
    dissoc  = R.dissoc,
    empty   = R.empty,
    equals  = R.equals, 
    evolve  = R.evolve,
    has     = R.has,
    hasIn   = R.hasIn,
    head    = R.head,
    isEmpty = R.isEmpty,
    isNil   = R.isNil,
    findIndex = R.findIndex,
    last    = R.last,
    map     = R.map,
    merge   = R.merge,
    objOf   = R.objOf,
    or      = R.or,
    not     = R.not,
    path    = R.path,
    pipe    = R.pipe,
    prop    = R.prop,
    propEq  = R.propEq,
    unless  = R.unless,
    zipObj  = R.zipObj;
var util = require('util');
var log = require('./log.js');
var expressHelper = require('./expresshelper');
var S = require('./lambda');

var md = require('markdown-it')({linkify:true});

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

var moment = require('moment');

// renderData :: Render -> String -> Object -> Render 
var renderData = R.curry((res, template, data) => res.render(template, data));


// maybebuildNavs :: {o} => {m} 
const maybeBuildNavs = (req) => S.path(['user', 'email'], req).map(expressHelper.BuildNavbar('')).map(objOf('navbarul'));

// getHostButtons :: s => {b}
const getHostButtons = (status) => (status === 'played') 
  ? {
    buttons: [{
      text:'Delete History', 
      faicon: 'ban', 
      style:'danger', 
      id: 'disband',
      title: 'Delete this event'
    }]
  } 
  : (status === 'open') 
    ? {
      buttons: [{
        text:'Disband', 
        faicon: 'ban', 
        style:'danger', 
        id: 'disband',
        title: 'Disband this event'
      }]
    }
    : (status === 'draft') 
      ? {
        buttons: [
          {
            text:'Cancel', 
            faicon: 'ban', 
            style:'danger', 
            id: 'disband',
            title: 'Cancel this event'
          },
        ]
      }
      : {};
exports.GetHostButtons = getHostButtons;


// matchHasUser :: s -> o -> b
const matchHasUser = R.curry((email, obj) => {

	// lookupUser :: [p] -> bool
	const lookupUser = R.compose(
		R.chain(S.find(R.propEq('email', email))),
		R.chain(S.prop('players')), 
		S.head
	);

	return lookupUser(obj).isJust

});
exports.MatchHasUser = matchHasUser;

// matchIsOpen :: [{m}] -> maybe b
const matchIsOpen = (ms) => {
  const checkOpen = compose( 
    map(not),
    map(R.equals('draft')),
    chain(S.prop('status')),
    S.head
  );
  return checkOpen(ms).getOrElse(false);
}
exports.MatchIsOpen = matchIsOpen;


// matchIsJoinable :: [{m}] => b
const matchIsJoinable = R.compose(
  R.converge(R.and, [R.pipe(R.prop('isfull'), R.not), R.pipe(R.prop('status'), R.equals('postgame'), R.not)]),
  R.head
);
exports.MatchIsJoinable = matchIsJoinable;

// mergeQuadrant :: {o} => {p} 
const mergeQuadrant = (data) => {
  const objWhenOne = (x) => {
    return (x == 0) 
      ? objOf('gameQuad0', true) 
      : (x == 1) 
        ? objOf('gameQuad1', true) 
        : (x == 2) 
          ? objOf('gameQuad2', true) 
          : (x == 3) 
            ? objOf('gameQuad3', true) 
            : {};
  }
  const addQuad = compose(
    map(objWhenOne), 
    map(accounts.GetTestQuadrant), 
    S.prop('key')
  );
  return merge(addQuad(data).getOrElse({}), data);
};
exports.MergeQuadrant = mergeQuadrant;

// findReoccurEveryVal :: o -> n 
const findReoccurEveryVal = (obj) => S.path(['reoccur', 'enable'], obj)
  .getOrElse(false)
    ? S.path(['reoccur', 'every'], obj).getOrElse('-1')
    : '-1';
exports.findReoccurEveryVal = findReoccurEveryVal;

// findReoccurIntervalVal :: o -> n 
const findReoccurIntervalVal = (obj) => S.path(['reoccur', 'enable'], obj)
  .getOrElse(false)
    ? S.path(['reoccur', 'interval'], obj).getOrElse('never')
    : 'never';
exports.findReoccurIntervalVal = findReoccurIntervalVal;

// assocMarkdown :: o -> o 
const assocMarkdown = (x) => S.prop('description', x)
  .map(R.assoc('markdown', R.__, x))
  .getOrElse(x);
exports.assocMarkdown = assocMarkdown;

// isPremiumUser :: o -> bool
const isPremiumUser = (req) => (
	S.pathEq(['user', 'premium', 'isstaff'], true, req).isJust ||
	S.pathEq(['user', 'premium', 'isactive'], true, req).isJust);
exports.isPremiumUser = isPremiumUser;

// routegame :: Express -> Object -> Model -> Schema -> Schema -> 0
var routegame = function (expressApp, CheckLogin, match, account) {

  log.clos('route', 'game');

	var url = "groupevent";
	var urlFail = "login";
  var useremail = '';

	var data = {};
  var slashString = (file) => '/' + file;

	expressApp.get(slashString(url), function(req, res) {

    // assocReoccurEvery :: o -> o 
    const assocReoccurEvery = (x) => 
      R.assoc('reoccurevery', findReoccurEveryVal(x), x);

    // assocReoccurInterval :: o -> o 
    const assocReoccurInterval = (x) => 
      R.assoc('reoccurinterval', findReoccurIntervalVal(x), x);

    // mdrend :: a => <b />
    const mdrend = (x) => md.render(x);
    const transDescription = objOf('description', mdrend);

    // ifImage :: s -> o
    const ifImage = (imgurl) =>
      (R.isEmpty(imgurl))
        ? R.objOf('image', imgurl)
        : {};

    // objectify :: o -> o
    const objectify = (x) => 
      x.toObject();

    // findThumbnail :: a -> maybe a
    const findThumbnail = R.compose(
      R.map(R.objOf('image')),
      R.chain(S.prop('thumbnail')),
      R.chain(S.head),
      R.map(objectify),
      R.chain(S.prop('games')),
      S.head
    );

    // sendCleanedPage :: o -> s -> o -> b -> o -> f => o -> o
    var sendCleanedPage = R.curry((res, url, hostdata, isPremium, pagedata, fprepare, navbars) => R.compose(
      renderData(res, 'match'), 
      merge(findThumbnail(pagedata).getOrElse({})),
      merge({boardgameserviceurl: nconf.get('service:boardgames:weburl')}),
      merge(maybeBuildNavs(req).getOrElse({})), 
      merge(navbars), 
      merge(hostdata), 
      fprepare, 
      evolve(transDescription), 
      assocReoccurInterval, 
      assocReoccurEvery, 
      assocMarkdown, 
			R.assoc('ispremium', isPremium),
      mergeQuadrant, 
      dissoc('__v'), 
      dissoc('_id'), 
      JSON.parse, 
      JSON.stringify, 
      head
    )(pagedata));

    if (R.hasIn('eventkey', req.query)) {
      var email = R.path(['user', 'email'], req);

      // lookup match
      matches.FindMatchByKey(match, req.query.eventkey).then(function(x) {

        if (x.length == 1) {

					// playersByType :: [o] -> [o]
					var playersByType = R.compose(
						R.groupBy(R.prop('type')), 
						R.map(R.dissoc('__v')), 
						R.map(R.dissoc('_id')), 
						R.prop('players'), 
						JSON.parse, 
						JSON.stringify, 
						R.head
					);

					var hostify = R.compose(
						R.assoc('host', R.__, {}), 
						R.head, 
						R.prop('host'), 
						R.map(R.dissoc('type')), 
						playersByType
					);

					var typify = R.curry((type, match) => R.compose(
						R.ifElse(R.hasIn(type), R.pipe(R.prop(type), R.map(R.dissoc('type')), R.assoc(type, R.__, {})), R.empty), 
						playersByType
					)(match));  

					var approves = {approves: typify('approve', x)};

					const div = (thread) => zipObj(['class', 'text'], ['initials', S.prop('username', thread).getOrElse('N')]);

					const divify = (thread) => zipObj(['div'], [div(thread)]);

					// liftEntryFee :: {o} => Maybe(s)
					const liftEntryFee = (x) => R.lift(matches.EntryFee)(S.prop('price', x), S.prop('currency', x));

					// safeGtZero :: a => Maybe(a);
					const safeGtZero = (a) => R.gt(a, 0)
						? Maybe.of(a) 
						: Maybe.Nothing;

					// entryFee :: {o} => {p}
					var entryFee = (S.prop('price', x[0]).chain(safeGtZero)) 
						? objOf('entryFee', liftEntryFee(x[0]).getOrElse(0)) 
						: objOf('entryFee', 0);

					const heading = (thread) => zipObj(['text', 'class', 'span'], [S.prop('username', thread).getOrElse('No Name Set'), 'media-heading', prop('useremail', thread)]);

					const paragraph = (thread) => zipObj(['text', 'class'], [prop('comment', thread)]);

					const image = (thread) => zipObj(['src', 'class', 'style'], [prop('avataricon', thread), 'media-object img-rounded', 'max-width:60px']);

					const getLastHash = compose(prop('hash'), last);

					const unlessEmptyGetLastHash = (thread) => thread.length == 0 
						? '' 
						: getLastHash(thread);

					const imgify = (thread) => zipObj(['img'], [image(thread)]);
					const h4ify  = (thread) => zipObj(['h4'],  [heading(thread)]);
					const parafy = (thread) => zipObj(['p'],   [paragraph(thread)]);

					const whenHasImage = (thread) => (prop('avataricon', thread) != undefined) 
						? imgify(thread) 
						: divify(thread);

					const chatParts = (thread) => zipObj(['media-left', 'media-body'], [whenHasImage(thread), merge(parafy(thread), h4ify(thread))]);
					const threadify = (thread) => zipObj(['forumthread'], [chatParts(prop('thread', thread))]);
					const whenMemberAddLobby = (thread) => zipObj(['chat', 'buttons'], [map(threadify, thread), {textarea: '', button: ''}]);

					var matchpointlist = [
						{id: 'status', text: x[0].status, faicon: 'fa-bell'}
					];

					var whenSocial = R.curry((key, text, faicon, data) => (R.prop(key, x[0]) == 2) 
						? R.append({text: text, faicon: faicon}, data) 
						: data
					); 

					var socialise = R.compose(
						whenSocial('alcohol', 'Drinking', 'fa-beer'), 
						whenSocial('smoking', 'Smoking', 'fa-fire')
					);

					var matchpoints = {points: socialise(matchpointlist)};
					var prepareData = R.identity;

          var buttons = matchIsJoinable(x) 
            ? { buttons: [
                {text:'Interested', faicon:'bell', style:'primary', id:'interested'},
                {text:'Join Event', faicon:'hand-paper-o', style:'success', id:'join'}
              ]}
            : {};


					if (!matchHasUser(email)(x)) {

						// checkLogin :: ({r} -> {o}) => {p}
						const checkLogin = curry((req, obj) => has('user', req) 
							? obj 
							: merge(obj, {navigation: {nouser: true}}));

						prepareData = R.compose(
							R.merge(entryFee), 
							R.merge({showJoin:matchIsJoinable(x)}), 
							R.merge(approves), 
							checkLogin(req), 
							R.merge(matchpoints), 
							R.merge(buttons)
						);

						matches.TaskIncrementStatWhenNotHost('viewcount', match, '', x[0].key).fork();

					} else { 

						var buttons = {};

						var matchPlayerType = R.compose(
							R.prop('type'), 
							R.head, 
							R.filter(R.propEq('email', email)), 
							R.prop('players'), 
							R.head
						);

						const mergeRequests = (x) => (R.propEq("status", "played", x[0]))
							? merge({}) 
							: compose(merge, objOf('requests'), typify('request'))(x); 

						switch (matchPlayerType(x)) {
							case 'host':
								var buttons = getHostButtons(x[0].status);
								var showReach = {reach: true, enableEdit: true};
								prepareData = R.compose(
									R.merge(entryFee), 
									merge(showReach), 
									merge(matchpoints), 
									merge(approves), 
									unless(pipe(prop('seatavailable'), equals(0)), pipe(mergeRequests(x))), 
									merge(buttons)
								);
								break;

							case 'invite':
								buttons = {buttons: [
                  {text:'Accept', faicon:'hand-paper-o', style:'success', id:'accept'},
                  {text:'Leave', faicon: 'sign-out', style: 'warning', id:'leave'}
                ]};
								prepareData = R.compose(
									R.merge(entryFee), 
									R.merge(matchpoints), 
									R.merge(buttons), 
									R.merge(approves)
								);
								matches.TaskIncrementStatWhenNotHost('viewcount', match, '', x[0].key).fork();
								break;

							case 'request':
								buttons = {buttons: []};
								var pending = {pending:{
									avataricon: req.user.profile.avataricon,
									name: req.user.profile.name,
									email: req.user.email}
								};
								prepareData = R.compose(
									R.merge(entryFee), 
									R.merge(pending), 
									R.merge(matchpoints), 
									R.merge(buttons), 
									R.merge(approves)
								);
								matches.TaskIncrementStatWhenNotHost('viewcount', match, '', x[0].key).fork();
								break;

							case 'approve':
								buttons = {buttons: [{text:'Leave', faicon: 'sign-out', style: 'danger', id:'leave'}]};
								approves = {approves: typify('approve', x)};
								prepareData = R.compose(
									R.merge(entryFee), 
									R.merge(matchpoints), 
									R.merge(approves), 
									R.merge(buttons)
								);
								break;

							case 'interested':
                buttons = { buttons: [
                  {text:'Join Event', faicon:'hand-paper-o', style:'success', id:'join'},
                  {text:'Leave', faicon: 'sign-out', style: 'warning', id:'leave'},
                ]};

                prepareData = R.compose(
                  R.merge(entryFee), 
                  R.merge({showJoin:matchIsJoinable(x)}), 
                  R.merge(approves), 
                  R.merge(matchpoints), 
                  R.merge(buttons)
                );
								break;

							}
						};

						accounts
							.taskClearMatchNotifications(account, email, 'event?eventkey=' + req.query.eventkey)
							.fork(data=>{}, err=>{});

            accounts
              .BuildNavbars(account, email)
              .then(sendCleanedPage(res, url, hostify(x), isPremiumUser(req), x, prepareData));

        } else {
          res.redirect('findmatch?err=matchnotfound');
        };

      }).catch(function(err) {
        res.redirect('findmatch?err=matchnotfound');
      });

    } else {
      res.redirect('findmatch?err=matchnotfound');

    };

	});


};
exports.Routegame = routegame;
