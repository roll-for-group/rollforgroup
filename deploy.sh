#!/bin/bash
./build_docs.sh
rsync -r app ubuntu@rollforgroup.com:/home/ubuntu/rollforgroup
rsync -r views ubuntu@rollforgroup.com:/home/ubuntu/rollforgroup
rsync -r package.json ubuntu@rollforgroup.com:/home/ubuntu/rollforgroup
rsync -r emailer/app ubuntu@rollforgroup.com:/home/ubuntu/rollforgroup/emailer
rsync -r collections/app ubuntu@54.183.71.123:/home/ubuntu/rollforgroup/collection
rsync -r views ubuntu@54.183.71.123:/home/ubuntu/rollforgroup
scp views/partials/analytics.handlebars.prod ubuntu@rollforgroup.com:~/rollforgroup/views/partials/analytics.handlebars
scp views/partials/analytics.handlebars.prod ubuntu@54.183.71.123:~/rollforgroup/views/partials/analytics.handlebars
