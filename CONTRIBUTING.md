# Contributing to Roll For Group

Thanks for wanting to help us out!

Roll for Group's mission is to create friendships by providing the best board game experiences. If you love board gaming, then you're at the right place.

If you've found something you think is a bug, check out how to [report bugs](CONTRIBUTING.md#reporting-bugs).

If you have a great idea for a feature or modification, head down to [suggesting features](CONTRIBUTING.md#suggesting-features).

If you are a developer who wants to get involved, please see our [developer guidelines](CONTRIBUTING.md#developer-guidelines).

If you are multilingual and would like to help us out with a translation, we will soon be looking for submissions.  We don't have the capability for translations yet, but it is coming in the future.

Regardless of how you would like to contribute, please keep our [code of conduct](CONTRIBUTING.md#code-of-conduct) in mind.



# Code of Conduct



 **Everyone who is honestly attempting to better this project should feel welcome here.**



The world is a diverse place, with different ideas, cultures, geographical locations, levels of knowledge, and ways of thinking. The project has some public-facing elements, and many of our actions within those elements are open for anyone to see. So please strive for **all** communication between the team, it's members, and the public to be kept civil.

We don't want political, cultural, religious, or other agendas to be thrown around - please leave them at the door when you step into the space of this project.

If someone does say something that you don't like, please don't jump to the conclusion that they are trying to attack or offend you - they may not realise. Politely tell them that you are not comfortable with their words/actions, and they will likely stop. If they do continue to misbehave, let someone else know so we can try to work through the situation.

If you find there is someone who you just can't handle interacting with for no real fault of your or their own, just leave it be and let someone else handle it.



Most importantly:

> **Be excellent to each other**
>
> ​	*Bill - Bill & Ted's Excellent Adventure*



# Reporting Bugs

Bugs are tracked right here in [the issue tracker](https://gitlab.com/jaied/rollforgroup/issues).

Please search before you add a bug - someone might have already reported it. If you don't find a bug similar to what you want to report, then you are welcome to add a new issue.

When submitting a bug please include as much information as you can on how to recreate it. A screenshot or two can be extremely useful too. Please make sure that you include which operating system you are using and your browser version.

Yes, developers, this includes you! Please write your bugs as if someone else will be handling it, even if you plan to fix the issue yourself.



### Labelling your Bugs

**Project Developers:** Please add all labels you think are necessary. You are welcome to estimate the priority yourself if you think you know where it should be. Always add `Bug`, and unless you are already fixing it, always add `Awaiting Review`.

**Everyone else:** No need to add labels, but if you really want to, you can add the labels  `Bug` & `Awaiting Review`. But please don't add any other labels to your report. Thank you!



# Suggesting Features

Feature requests are also handled here in [the issue tracker](https://gitlab.com/jaied/rollforgroup/issues).

Please search before you request a feature. If there is an existing request for what you want, please just give it a thumbs up. If there is a very similar request but not quite the same, give it a thumbs up and leave a comment with your own ideas too.

If you can't find anything similar you can then create a new request.

Please submit as much detail about what you are requesting as you can. The more information we have to go on, the easier it is for us to figure out if the feature you are requesting aligns with our vision for the project.



### Labelling your Feature Requests

**Project Developers:** Go ahead and add the labels you think belong there. Priority estimations are welcome. Please always add `Feature Request` & `Awaiting Review`.

**Everyone else:** You don't need to add any labels, someone on the team will take care of it soon enough. But it is helpful if you add the  `Feature Request` & `Awaiting Review` labels. Please don't add any other labels. Thank you!



# Developer Guidelines

## Complete Issue Lifecycle

1. Issue created *(anyone)*
2. Check details *(developers)*
   1. *For bugs:* Ensure reproducibility | *For feature requests:* Ensure adequate detail
   2. Request clarifications if needed, until details are sufficient
      1. *For bugs:* Close if bug is unreproducible or determined to be an error on the reporter's part
   3. Ensure adequate labels are present: `Awaiting Review` + `Bug|Feature Request|Improvement` at a minimum
3. Review *(team)*
   1. *For feature requests:* Determine validity of request
      1. Close if feature does not align with project vision
   2. Determine priority and any other applicable tags
   3. Remove `Awaiting Review` label, add `Reviewed` label
4. Developers claim issues *(developer)*
   1. Issues may be offered out as they need to be done (especially the more critical issues) but if you are looking for something to do there should always be unassigned tasks to grab
   2. Ensure the issue is assigned to you so others know not to grab it
5. **Coding magic happens** :fireworks: :ghost: :gem: *(developer/wizard)*
6. Create a merge request and assign it to someone else *(developer)*
   1. If necessary add anybody else who should have a say in the MR as an Approver
   2. If there are any "gotchas" in your changes that are important to know, but it was not meaningful to comment about it in the code itself, leave comments in the MR so your reviewer(s) know are informed
   3. Keep an eye on your open MRs for comments from your reviewer(s) in case any problems are found
7. Check the merge request *(reviewer)*
   1. Ensure code style is consistent
   2. Read through code for obvious errors
   3. Manually check the feature/bug fix to ensure it meets the definition of "Done"
   4. If there are problems, comment on/around the associated code so the original developer knows where the problems are
   5. Try to be specific about what the problems are - don't just leave a comment that says "inconsistent code style" - (politely) tell them what is wrong with their code style
   6. If there are no problems, or once the problems have been fixed, make sure the "Remove source branch" checkbox is checked, and merge
   7. If you have merge requests you don't think you can check in a timely manner, try to find someone who can





## Branching, Committing & Merging Strategy

Follow the [Gitlab flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html) model.



The abridged version, plus some additional best practices we recommend:

- Create feature branches from latest master
  - Name branches starting with the associated issue tracker number (where applicable)
  - Create meaningful branch names
- Commit often
  - Avoid commits with huge lists of changes in the message where you can
  - Never use generic "all the things" type commit messages - changes should be somewhat traceable in the commit history
  - Commit messages should make sense when they are added to "When this commit is merged it will...."
    - Some bad examples: "Installation docs updated for new dependencies", "Profile page layout improved on mobile", "Fixed inability to change password"
    - And their better counterparts: "Update installation docs for new dependencies", "Improve mobile layout of profile page", "Fix inability to change password" - note that last one - the difference is very subtle!
- Push occasionally
  - If a feature/bug fix takes more than a couple of days, push your work back so it exists somewhere other than just your computer
- Test your work
  - Manual testing of features and bug fixes is mandatory (unless you have a *very* good reason why you can't)
  - Write and/or update unit tests for your work
  - Run unit tests to make sure you haven't broken anything
- Create merge requests back to master
  - Merge requests containing unfinished work, or work you want interim reviewed, must be marked as a work in progress (start the title with `WIP:`)
  - Always select "Remove source branch"
  - Merge requests must always be reviewed by at least one developer who isn't you



## CSS

Use basic CSS.

You are welcome to use [flexbox](https://caniuse.com/#feat=flexbox) and [CSS grid](https://caniuse.com/#feat=css-grid) (and probably any other newish and funky styling standards that come out), but only if you ensure that the layout degrades semi-gracefully on unsupported browsers and that consistency of elements across the site is maintained.



## Client-side JavaScript

### Supported Standards

All client-side JavaScript code should adhere to ES5 standards, with no shims, polyfills, or other workarounds for pre-ES5 issues and oddities.

ES6+ features should be avoided. **If** there is an ES6+ feature that is deemed essential for a client-side feature to work in an optimal manner (after discussion with the team), it should be introduced with a minimal polyfill included only in the places it is needed.



## Including Libraries & Polyfills

One of the following methods is the preferred way for including polyfills & client-side libraries in the project:

1. Link to a minimal library/polyfill from a CDN
2. Include the source of a minimal library/polyfill from [Microjs](http://microjs.com) or a similar resource in the code base
3. Copy/paste a library/polyfill from [MDN](https://developer.mozilla.org/bm/docs/Web/JavaScript) or a similar resource (please remember to provide a link and/or attribution in a comment above it)

Under no circumstance should a larger library be included for one or two small features!



## Server-side JavaScript

### Base Style Guide

Follow [Felixge node style guide](https://github.com/felixge/node-style-guide#use-uppercase-for-constants).

Use functional programming, not object orientated.



### Type Signatures

Give functions a Type Signature, use the [Ramda Type Signature Guide](https://github.com/ramda/ramda/wiki/Type-Signatures).


### Terse functions

Functions declared within functions should be Terse for Node.js
Declare terse functions as constants, but follow camelCase naming
For example `exampleTerseFunction = (firstName, lastName) => firstName + ' ' + lastName;`



### Variable Assignments

All server-side JavaScript code should use `const` to declare variables.

Functional programming style should restrict/deter the need for variable reassignments, so there should be little to no use for `let`.

Never use `var`. Yes, the code currently uses `var`, but all future merge requests will not be accepted if `var` is used for new variables.



## Running Tests

To run tests, use `jasmine-node spec`.

To run tests on services use `jasmine-node collections/spec` and `jasmine-node boardgames/spec`.

