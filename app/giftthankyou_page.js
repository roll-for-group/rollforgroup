var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');
var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var Promise = require('bluebird');
var R = require('ramda');
var compose = R.compose, 
    curry   = R.curry, 
    isEmpty = R.isEmpty,
    isNil   = R.isNil,
    merge   = R.merge,
    path    = R.path;
var log = require('./log.js');
var expressHelper = require('./expresshelper');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

// safePath :: ([a] -> b) => Maybe(b[a])
const safePath = curry((x, o) => (isEmpty(o) || isNil(o) || isNil(path(x, o))) ? Maybe.Nothing() : Maybe.of(path(x, o)));

// renderData :: Render -> String -> Object -> Render 
var renderData = R.curry((res, template, data) => res.render(template, data));

// maybebuildNavs :: {o} => {m} 
const maybeBuildNavs = (req) => safePath(['user', 'email'], req).map(expressHelper.BuildNavbar('')).map(R.objOf('navbarul'));


// routeGiftThankyou :: Express -> Object -> Model -> Schema -> Schema -> 0
var routeGiftThankyou = function (expressApp, CheckLogin, match, account) {

	var url = "giftthankyou";
	var urlFail = "login";

  var slashString = (file) => '/' + file;

	expressApp.get(slashString(url), CheckLogin.ensureLoggedIn(slashString(urlFail)), function(req, res) {

   var sendCleanedPage = curry((res, url, navbars) => compose(renderData(res, url), merge(maybeBuildNavs(req).getOrElse({})))(navbars));
   var email = R.path(['user', 'email'], req);
   accounts.BuildNavbars(account, email).then(sendCleanedPage(res, url));

	});
};

exports.RouteGiftThankyou = routeGiftThankyou;
