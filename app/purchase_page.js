var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var Promise = require('bluebird');
var R = require('ramda'),
    append  = R.append,
    compose = R.compose,
    dissoc  = R.dissoc,
    hasIn   = R.hasIn,
    head    = R.head,
    merge   = R.merge,
    prop    = R.prop;

var util = require('util');
var log = require('./log.js');
var expressHelper = require('./expresshelper');

// renderData :: Render -> String -> Object -> Render 
var renderData = R.curry((res, template, data) => res.render(template, data));

// slashString :: String -> String
var slashString = (endpoint) => "/" + endpoint;

var purchase_page = function(req) {
    return {
        pageURL: "purchase",
        sections: [
            {
                title: "Puchase Tickets",
                label: "Tickets allow you to join or host public games. You have " + req.user.tickets + " tickets.",
                icon: "fa fa-ticket",
                canvas: "img/mageknight_small.jpg",
                nosubmit: true,
                questions: [
                    {
                        select: {
                            label: "Select your ticket package below",
                            instructable: "During our launch stage, tickets are free. Enjoy some games on us!",
                            id: "itempack",
                            options: [
                                {option:"10 ticket pack", value: 10, default: true},
                            ]
                        }
                    },
                    {
                        textarea: {
                            label: "Free Tickets are Provided for Feedback",
                            instructable: "What do you like most about the Roll for Group service?",
                            id: "like",
                        }
                    },
                    {
                        textarea: {
                            instructable: "What do you dislike about the Roll for Group service?",
                            id: "dislike",
                        }
                    },
                    {
                        textarea: {
                            instructable: "What suggestion do you have for the Roll for Group service?",
                            id: "suggestion",
                        }
                    }
                ]
            }
        ]
    }
};


// routeStage :: Express -> Object -> Model -> Schema
var routePurchase = function (expressApp, CheckLogin) {

    //TODO when user has too many tickets, change the option (or msgbox modal) 
    //TODO show number of tickets somewhere

    expressApp.get(slashString('purchase'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res) {

        res.render('purchase', merge(purchase_page(req), {'user': expressHelper.LookupUsername(req.user)}));
        
    });
};
exports.RoutePurchase = routePurchase;

