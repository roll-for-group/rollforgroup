var mongoose = require('mongoose');
// var mongoose = require('mongoose').set('debug',true);
var connectionCount = 0;
var Promise = require('bluebird');

mongoose.Promise = global.Promise;

// module.exports = mongoose;
exports.Mongoose = mongoose;

// connectDatabase :: MongooseObject -> String -> MongooseObject
var connectDatabase = function(dbUri) {
  if (connectionCount == 0) {
    mongoose.connect(dbUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    console.log('Mongoose Opened')
  }
  connectionCount = incrementState(connectionCount);
  return mongoose;
};
exports.ConnectDatabase = connectDatabase;


// closeDatabase :: MongooseObject -> String -> MongooseObject
var closeDatabase = function() {
  connectionCount = decreaseState(connectionCount);
  if (connectionCount == 0) {
    mongoose.connection.close(function() {
      console.log('Mongoose Closed')
      return mongoose;
    });
  } else {
    return mongoose;
  }
};
exports.CloseDatabase = closeDatabase;

var incrementState = function(count) {
  count = count + 1;
  console.log('Mongoose increase: ' + count);
  return count;
};
exports.IncrementState = incrementState;

var decreaseState = function(count) {
  count = count - 1;
  console.log('Mongoose decrease: ' + count);
  return count;
};
