var accounts = require('../app/models/accounts.js');
var account = require('../app/models/account.js');

var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var Promise = require('bluebird');
var R = require('ramda');

const S       = require('../app/lambda.js');

var log = require('./log.js');
var expressHelper = require('./expresshelper');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')


const fullmotivelist = [ 
  'motivecompetition', 
  'motivecommunity', 
  'motivestrategy', 
  'motivestory', 
  'motivedesign', 
  'motivediscovery', 
  'motivecooperation', 
  'motivesocialmanipulation' 
];


// slashString :: String -> String
var slashString = (endpoint) => "/" + endpoint;

// renderData :: Render -> String -> Object -> Render 
var renderData = R.curry((res, template, data) => res.render(template, data));


// RouteAdminList :: (Express -> Object -> Model) => Promise() 
var routeAdminList = R.curry(function (expressApp, CheckLogin, account) {
  return new Promise(function(fulfill, reject) {

    expressApp.get(slashString('userstats'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res){

      // maybebuildNavs :: {o} => {m} 
      const maybeBuildNavs = (req) => S.path(['user', 'email'], req)
        .map(expressHelper.BuildNavbar('userstats'))
        .map(R.objOf('navbarul'));

      // composeUserData :: ({u}) => express.render
      const composeUserData = R.curry((data) => R.composeP(
        renderData(res, 'admin'), 
        R.merge(maybeBuildNavs(req).getOrElse({})), 
        accounts.BuildNavbars(account)
      ));

      // note user activity
      S.path(['user', 'email'], req).chain(accounts.TaskRecordActivity(account));
      S.path(['user', 'email'], req).map(composeUserData({}));

    });
  });
});
exports.RouteAdminList = routeAdminList;
