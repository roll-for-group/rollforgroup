var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var R = require('ramda');
var chain   = R.chain,
    compose = R.compose, 
    composeP = R.composeP,
    map     = R.map,
    merge   = R.merge;

var log = require('./log.js');
var S = require('./lambda.js');

var expressHelper = require('./expresshelper');

const Maybe = require('data.maybe')

// maybeDefined :: a => Maybe(a) || Maybe(Nothing)
const maybeDefined = (value) => ((value == undefined) || (value == {})) 
  ? Maybe.Nothing() 
  : Maybe.of(value);

// renderData :: Render -> String -> Object -> Render 
const renderData = R.curry((res, template, data) => res.render(template, data));

// slashString :: String -> String
const slashString = (endpoint) => "/" + endpoint;

// unlinkObj :: ObjA => ObjB
const unlinkObj = compose(JSON.parse, JSON.stringify);

var routeProfile = function (expressApp, CheckLogin) {

	const unsubscribe_page = {
		pageURL: "unsubscribe",
		sections: [{
			title: "What's Your Gamer Style?",
			image: "/img/1_2_34.png",
			label: "Your gamer style will help define what games you will be matched to play.",
			icon: "fa fa-photo",
			canvas: "/img/carcassonne.jpg",
			questions: [
					{
						checkbox: {
							label: "Email Notifications",
							instructable: "Update me by email",
							id: "notifyupdates",
							options: [
								{option: 'Site Notifications', id: 'notifyevents'},
							]
						}
					}
				]
			}
		]
	};

	const readAccount = accounts.ReadAccountFromEmail(account);

	// userProfile :: email => Maybe(jsonObj) 
	const userProfile = composeP(
		map(unlinkObj), 
		chain(S.prop('profile')), 
		S.head, 
		readAccount
	);

	expressApp.get(slashString('unsubscribe'), function(req, res) {

		// note user activity
		S.path(['query', 'email'], req).chain(accounts.TaskRecordActivity(account));

		const resolveProfile = R.curry((data, x) => {
			x.then(function(interior) {

				// maybebuildNavs :: {o} => {m} 
				const maybeBuildNavs = (req) => S.path(['user', 'email'], req)
					.map(expressHelper.BuildNavbar('unsubscribe'))
					.map(R.objOf('navbarul'));

				const render = composeP(
					renderData(res, 'unsubscribe'), 
					merge(expressHelper.BuildNavbarHead('Unsubscribe')), 
					merge(maybeBuildNavs(req).getOrElse({})), 
					accounts.BuildNavbars(account)
				);

				render(req.query.email);

			}).catch(function(err) {
				res.status(401);
			});
		});

		maybeDefined(req.query.email).map(userProfile).map(resolveProfile(unsubscribe_page));
			
	});

};
exports.RouteProfile = routeProfile;
