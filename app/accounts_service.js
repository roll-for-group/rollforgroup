/**************************************************
**
** an ajax service for all things to do with the account
**
** todo: seperate into microservice
**
*************************************************/

var express     = require('express');
var bodyParser  = require('body-parser');
var jsonParser  = bodyParser.json();
var braintree   = require('braintree');
var ajax        = require('./ajax.js');

var R         = require('ramda');
var assoc     = R.assoc;
var chain     = R.chain;
var compose   = R.compose;
var curry     = R.curry;
var dissoc    = R.dissoc;
var has       = R.has;
var head      = R.head;
var filter    = R.filter;
var lift      = R.lift;
var map       = R.map;
var merge     = R.merge;
var path      = R.path;
var prop      = R.prop;
var reverse   = R.reverse;
var sortBy    = R.sortBy;
var zipObj    = R.zipObj;

var Promise = require('bluebird');
var log = require('./log.js');
var exports = module.exports = {};

var account = require('./models/account.js');
var accounts = require('./models/accounts.js');

var match   = require('./models/match.js');
var matches = require('./models/matches.js');

var S = require('./lambda.js');

var moment = require('moment');
var icalToolkit = require('ical-toolkit');

// ============== AJAX ===============
var ajaxTask = ajax.AjaxTask;


const shortid = require('shortid');

const Maybe     = require('data.maybe');
const Either    = require('data.either');
const Task      = require('data.task');

var geolib = require('geolib');

// scrub :: o -> o;
const scrub = compose(dissoc('_id'), dissoc('__v'), JSON.parse, JSON.stringify);

// safeGeocodify :: o -> maybe o
const safeGeocodify = function(data) {

  // maybeNumberKey :: (k, o) -> maybe n 
  const maybeNumberKey = (key, data) => 
    R.compose(
      R.map(parseFloat), 
      S.prop(key)
    )(data);

  // geocodify :: maybe s -> maybe n -> maybe n -> maybe o 
  const geocodify = R.lift((name, lng, lat) => 
    zipObj(
      ['name', 'loc'], 
      [name, {type: 'Point', coordinates: [lng, lat]}]
    )); 

  return geocodify(
    S.prop('playcity', data), 
    maybeNumberKey('playcitylng', data), 
    maybeNumberKey('playcitylat', data)
  );

};
exports.SafeGeocodify = safeGeocodify;

// eitherGeocode :: o -> maybe o -> either o
const eitherGeocode = curry((validdata, maybeGeocode) => 
  (maybeGeocode.isJust) ? 
    Either.Right(assoc('localarea', maybeGeocode.get(), validdata)) : 
    Either.Left(validdata));
exports.EitherGeocode = eitherGeocode;

// maybeUserProp :: o -> s -> s -> maybe s;
const maybeUserProp = (req, propkey, pathkey) => 
  (S.path([pathkey, propkey], req).isJust) 
    ? S.path([pathkey, propkey], req) 
    : S.path(['user', propkey], req); 
exports.MaybeUserProp = maybeUserProp;

// maybeKey :: o -> s -> maybe s;
const maybeKey = (req, pathkey) => 
  (S.path([pathkey, 'key'], req).isJust) 
    ? S.path([pathkey, 'key'], req) 
    : S.path(['user', 'key'], req); 
exports.MaybeKey = maybeKey;

// safeMail :: o -> maybe s;
const safeMail = S.path(['params', 'email']);

// taskFail :: Task s
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// taskCheck :: o -> s -> s -> Task o 
const taskCheck = R.lift((account, email, apikey) => 
  new Task((reject, resolve) => 
    accounts.CheckValidEmail(account, email, apikey)
      .then(resolve)
      .catch(reject)
  )
);

const pickValidData = R.pick([
  'stage', 'avataricon', 'biography', 'job', 'bggname', 'name', 'Monday', 
  'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 
  'mondaystarthour', 'mondaystartminute', 'mondayendhour', 'mondayendminute', 
  'tuesdaystarthour', 'tuesdaystartminute', 'tuesdayendhour', 
  'tuesdayendminute', 'wednesdaystarthour', 'wednesdaystartminute', 
  'wednesdayendhour', 'wednesdayendminute', 'thursdaystarthour', 
  'thursdaystartminute', 'thursdayendhour', 'thursdayendminute', 
  'fridaystarthour', 'fridaystartminute', 'fridayendhour', 'fridayendminute', 
  'saturdaystarthour', 'saturdaystartminute', 'saturdayendhour', 
  'saturdayendminute', 'sundaystarthour', 'sundaystartminute', 'sundayendhour',
  'sundayendminute', 'ishost', 'isteacher', 'playeralcohol', 'playersmoking', 
  'hostalcohol', 'hostsmoking', 'usebggcollection', 'notifyevents', 
  'notifyplayers', 'notifyupdates', 'notifymatch', 'venueprivate', 
  'venuenogamegroup', 'venuenocommercial', 'venuenoconvention', 'venuenomeetup',
  'searchradius', 'nodriving', 'carpool', 'pubtransport', 'hostmax', 'matching',
  'visibility', 'avataricon', 'socialfacebook', 'socialtwitter', 'socialtwitch',
  'socialyoutube', 'socialpinterest', 'socialtumblr', 'socialwebsite', 
  'socialgoogleplus', 'socialdiscord', 'commemailvisible', 'commsteam', 
  'commphoneno', 'commskype', 'commreddit', 'commbattlenet', 'commxbox', 
  'commpsn', 'localarea', 'motivecommunity', 'motivecompetition', 
  'motivediscovery', 'motivestrategy', 'motivestory', 'motivedesign', 
  'motivesocialmanipulation', 'motivecooperation', 'whymanagemyevent', 
  'whyfindlocalgamers', 'whyfindlocalevents', 'whymanageboardgames', 'whyother'
]);

// buildData :: o -> o
const buildData = R.converge(eitherGeocode, [pickValidData, safeGeocodify]);

// buildIcalEvent :: s -> a -> s -> s -> s -> s -> dt -> dt -> o 
const buildIcalEvent = curry(
  (url, alarms, title, key, location, description, datestart, dateend) => 
    zipObj([
      'start', 'end', 'transp', 'summary', 'alarms', 'uuid', 'location', 
      'description', 'url'
    ], [ 
      new Date(datestart), new Date(dateend), 'OPAQUE', title, alarms, key, 
      location, description, url  + '/events/' + key 
    ])
);
exports.BuildIcalEvent = buildIcalEvent;

// valQueryToNumber :: o -> maybe n
const valQueryToNumber = R.curry((keyname, req) => S
  .path(['query', keyname], req)
  .map(Number));
exports.valQueryToNumber = valQueryToNumber;

// hasAccumulatedHostXP :: o -> Bool
const hasAccumulatedHostXP = (o) => compose(
  chain(S.gt(R.__, 0)), 
  S.path(['profile', 'hostxp'])
)(o).getOrElse(false);
exports.hasAccumulatedHostXP = hasAccumulatedHostXP;

// mergeImgClass :: {o} -> {o}
const mergeImgClass = (player) => {
  const buildImgClass = (player) => S.path(['premium', 'isstaff'], player).getOrElse(false) 
    ? 'imglistcircle img-circle fatooltip imglistcircle-staff'
    : S.path(['premium', 'isactive'], player).getOrElse(false)
      ? 'imglistcircle img-circle fatooltip imglistcircle-premium'
      : 'imglistcircle img-circle fatooltip imglistcircle-standard';
  return R.assoc('imgclass', buildImgClass(player), scrub(player)); 

};
exports.mergeImgClass = mergeImgClass;

// mergeAClass :: {o} -> {o}
const mergeAClass = (player) => {
  const buildAClass = (player) => S.path(['premium', 'isstaff'], player).getOrElse(false) 
    ? 'a-name-staff'
    : S.path(['premium', 'isactive'], player).getOrElse(false)
      ? 'a-name-premium'
      : 'a-name-standard';
  return R.assoc('aclass', buildAClass(player), scrub(player)); 

};
exports.mergeAClass = mergeAClass;

//whenUploadLocaliseURL 
const whenUploadLocaliseURL = (url) =>
  (R.isNil(url))
    ? url
    : (R.startsWith('upload', url))
      ? '/' + url
      : url;

exports.whenUploadLocaliseURL = whenUploadLocaliseURL;

const transformUpload = {
  profile: {
    avataricon: whenUploadLocaliseURL
  }
};

// sortByPlayXP :: [{o}] -> [{o}]
const sortByPlayXP = R.compose(
  R.map(R.evolve(transformUpload)),
  R.map(R.pick(['aclass', 'imgclass', 'updatedAt', 'pageslug', 'profile'])), 
  R.map(mergeAClass),
  R.map(mergeImgClass),
  R.reverse, 
  sortBy(path(['profile', 'playxp'])),
  R.filter(R.pathEq(['profile', 'visibility'], 1))
);
exports.sortByPlayXP = sortByPlayXP;

// sortByHostXP :: [{o}] -> [{o}]
const sortByHostXP = R.compose(
  R.map(R.evolve(transformUpload)),
  R.map(R.pick(['aclass', 'imgclass', 'updatedAt', 'pageslug', 'profile'])), 
  R.map(mergeAClass),
  R.map(mergeImgClass),
  R.reverse, 
  sortBy(path(['profile', 'hostxp'])),
  R.filter(R.pathEq(['profile', 'visibility'], 1)),
  R.filter(hasAccumulatedHostXP)
);
exports.sortByHostXP = sortByHostXP;


// updateBggName :: s -> s -> Task o
const updateBggName = R.curry(function(nconf, pageslug, bggname) {

  var requestData = {
    url: nconf.get('service:collections:weburl'),
    method: 'POST',
    data: {
      initialise: 'true',
      userkey: pageslug,
      bggname: bggname 
    } 
  };
  return ajaxTask(requestData);

});


// userToNetworkNode :: o -> o
const userToNetworkNode = (obj) => ({
  id: S.prop('pageslug', obj).getOrElse(''),
  shape: "circularImage",
  image: S.path(["profile", "avataricon"], obj).getOrElse("/img/default-avatar.jpg"),
  brokenImage: "/img/default-avatar.jpg",
  label: S.path(["profile", "name"], obj).getOrElse("??"),
  color: accounts.SafeWhenType("#cccccc", "#99b3ff", "#ffc266", obj)
});
exports.userToNetworkNode = userToNetworkNode;

/*
// findCoplayers :: s -> o
const findCoplayers = R.curry((useremail, playedmatches) => R.compose(
  R.sequence(Task.of),
  R.map(accounts.TaskAccountFromEmail(account)),
  R.uniq,
  R.without([useremail]),
  R.map(R.prop('email')),
  R.flatten,
  R.map(R.prop('players'))
)(playedmatches));
exports.findCoplayers = findCoplayers;
*/


// linkPlayer :: s -> o -> Bool
const linkPlayer = R.curry((playeremail, obj) => ((R.propEq('type', 'host', obj) || R.propEq('email', playeremail, obj))));

const linkPlayers = R.curry((useremail, eventPlayers) => {

  // findUser :: [o] -> o
  const findUser = R.find(R.propEq('email', useremail));

  // isHost :: [o] -> Bool
  const isHost = R.compose(
    // R.propEq('type', 'host'),
    S.whenPropEq('type', 'host'),
    findUser
  );

  return (isHost(eventPlayers).isJust)
    ? eventPlayers
    : R.filter(linkPlayer(useremail), eventPlayers);

});

// findCoplayers :: s -> task [o]
const findCoplayers = R.curry((useremail, playedmatches) => R.compose(
  R.sequence(Task.of),
  R.map(accounts.TaskAccountFromEmail(account)),
  R.uniq,
  R.without([useremail]),
  R.map(R.prop('email')),
  R.flatten,
  // R.map(R.filter(linkPlayer(useremail))),   // if the player is a host, then this should allow all players 
  R.map(linkPlayers(useremail)),
  R.map(R.prop('players'))
)(playedmatches));
exports.findCoplayers = findCoplayers;


// createEdige :: s -> o -> o
const createEdge = R.curry ((fromslug, toobj) => ({
  from: fromslug,
  to: S.prop('pageslug', toobj).getOrElse('')
}));
exports.createEdge = createEdge;

// taskUserNode :: s -> task [o]
const taskUserNode = (pageslug) => new Task((reject, resolve) => {
  accounts.ReadAnAccountFromPageSlug(account, pageslug)
    .then(data => resolve(userToNetworkNode(data)))
    .catch(() => reject('could not find user with pageslug ' + pageslug))
});
exports.taskUserNode = taskUserNode;

// findFriends :: s -> task [o]
const taskFindFriends = (pageslug) => new Task((reject, resolve) => {

  // liftHistoric :: maybe string -> maybe [o]
  const liftHistoric = R.lift(matches.FindHistoricMatchesByPlayerType(match, R.__, ['host', 'approve']))

  accounts.ReadAnAccountFromPageSlug(account, pageslug).then(
    data => {
      liftHistoric(S.prop('email', data)).cata({
        Just: (promiseMatches) => {
          promiseMatches.then(playedmatches => {
            findCoplayers(data.email, playedmatches).fork(
              () => resolve({nodes: [userToNetworkNode(data)], edges: [] }),
              (closefriends) => resolve({ 
                nodes: R.flatten([R.map(userToNetworkNode, closefriends)]),
                edges: R.flatten([R.map(createEdge(data.pageslug), closefriends)]),
              })
            );

          }).catch((err)=>{
            reject(pageslug + 'matches search failed');
          })
        },
        Nothing: () => resolve({nodes:[], edges:[]}) 
      })
    }).catch(() => resolve({nodes:[], edges:[]}))

});
exports.taskFindFriends = taskFindFriends;

// taskFriendsFromNodes :: o -> maybe [s]
const taskFriendsFromNodes = R.compose( 
  R.sequence(Task.of),
  R.chain(R.map(taskFindFriends)),
  R.sequence(Maybe.of),
  R.chain(R.map(S.prop('id'))),
  S.prop('nodes')
);
exports.taskFriendsFromNodes = taskFriendsFromNodes;

// unionWithUniqueId :: o -> o -> [o]
const unionWithUniqueId = R.curry((friends, user) => R.compose(
  R.unionWith(R.eqProps('id'), R.prop('nodes', friends)),
  R.of
)(user));
exports.unionWithUniqueId = unionWithUniqueId;

// flatEdges :: [[o]] -> [o]
const flatEdges = R.compose(
  R.flatten,
  R.map(R.prop('edges'))
);
exports.flatEdges = flatEdges;

// compareFromTo :: o -> o -> Bool
const compareFromTo = (objA, objB) => (
  (R.eqProps('from', objA, objB) && R.eqProps('to', objA, objB)) ||
  ((objA.to === objB.from) && (objA.from === objB.to))
);
exports.compareFromTo = compareFromTo;



// setupRouter :: express 
var setupRouter = function(nconf, account, transaction) {

  var router = express.Router();
	var braintreeGateway;

	if (nconf.get('braintree:environment') === undefined) {
		braintreeGateway = {};

	} else {

		var braintreeGateway = braintree.connect({
			environment: (nconf.get('braintree:environment') === 'production') 
				? braintree.Environment.Production 
				: braintree.Environment.Sandbox,
			merchantId: nconf.get('braintree:merchantid'),
			publicKey: nconf.get('braintree:publickey'),
			privateKey: nconf.get('braintree:privatekey') 
		});

	};

  router.route('/:email')
    .get(function(req, res) {

      // isValidEmail :: o -> bool
      const isValidEmail = (req) => S.path(['params', 'email'], req)
        .map(R.test(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))
        .getOrElse(false);

      // resResponse :: o -> o -> s -> s -> Promise o
      const resResponse = R.curry((res, account, email, apikey) =>
        accounts
          .CheckValidEmail(account, email, apikey)
          .then((x) => 
            (has('error', x))
              ? res.sendStatus(401)
              : res.json(x))
          .catch((err) => res.sendStatus(401))
      );

      // scrubProfile :: o -> maybe o
      const scrubProfile = R.compose(R.map(scrub), S.prop('profile'));

      if (isValidEmail(req)) {
        if (
          R.lift(resResponse(res, account))(
            S.path(['params', 'email'], req), 
            maybeKey(req, 'query')).isNothing) {
              res.sendStatus(401);
        }
      } else {
        S.path(['params', 'email'], req)
          .chain(accounts.TaskAccountFromSlug(account))
          .fork(
            err => res.sendStatus(401),
            data => (data === null) 
              ? res.sendStatus(401)
              : res.json(scrubProfile(data).getOrElse({}))
          );
      };
    })
    .patch(jsonParser, function(req, res) {

      // resResponse :: o -> o -> o -> s -> s -> Promise
      const resResponse = R.curry((res, account, body, email, apikey) =>
        accounts.CheckValidEmail(account, email, apikey)
          .then(function(updatedUser) {
            accounts.UpdateProfile(account, email, buildData(body).merge())
              .then(function(x) {

                // notifyNewUser :: maybe s -> maybe n -> maybe bool -> Task
                const notifyNewUser = R.lift(R.curry((email, visibility, isNew) => {
                  if (visibility === 1 && isNew) {
                    accounts.TaskNotifyNewNearbyUsers(account, email).fork();
                  }
                }));

                // liftUpdateBggName :: maybe s -> maybe s -> maybe Promise o
                const liftUpdateBggName = R.lift(updateBggName(nconf));

                liftUpdateBggName(
                  S.path(['pageslug'], x),
                  S.path(['body', 'bggname'], req)
                ).getOrElse(taskFail()).fork(()=>{}, ()=>{});

                notifyNewUser(
                  S.prop('email', x), 
                  S.path(['user', 'profile', 'visibility'], req), 
                  S.path(['body', 'newplayernotify'], req)
                );

                // add the bgg username of the person 
                req.login(x, function(err) {
                  (err)
                    ? res.sendStatus(401)
                    : res.json({result: 'success'});
                });

              }).catch(function(err) {
                res.sendStatus(401);
              });  

          }).catch(function(err) {
            res.sendStatus(401);
          })
      );

      if (R.lift(resResponse(res, account))(
        S.prop('body', req), 
        S.path(['params', 'email'], req), 
        maybeKey(req, 'body')).isNothing) {
          res.sendStatus(401);
      }

    })
    .delete(jsonParser, function(req, res) {

      // resResponse :: o -> o -> o -> s -> s -> Promise
      const resResponse = curry((res, account, email, apikey) => 
        accounts.CheckValidEmail(account, email, apikey)
          .then(function(updatedUser) {
            accounts.DeleteAccount(account, email)
              .then(function(x) {
                res.json({result: 'success'});
              });
          })
        );

      if (lift(resResponse(res, account))(
        S.path(['params', 'email'], req), 
        maybeKey(req, 'body')).isNothing) {
          res.sendStatus(401);
        } 

    });

  // fromNow :: (dt) -> s
  const fromNow = (time) => moment(time).fromNow();

  // nullWhenZero :: n -> o 
  const nullWhenZero = (a) => zipObj(
    ['noticecount', 'noticeclass'], (a === 0) 
      ? ['', ''] 
      : [a, 'button__badge wow tada']
    );

  // mergeCount :: [o] -> f(o) -> o
  const mergeCount = compose(merge, nullWhenZero, R.length);

  // faObj :: (s, s, o) -> s
  const faObj = (objName, faicon, notices) => 
    R.objOf(objName, (notices.length === 0) 
      ? 'fa ' + faicon + ' fa-fw' 
      : 'fa ' + faicon + ' fa-fw notify-active'
    );

  // mergeFromNow :: o -> o 
  const mergeFromNow = (data) => 
    compose(merge(data), R.objOf('fromnow'), fromNow, prop('date'))(data);

  // sortByDate :: [o] -> [o]
  const sortByDate = sortBy(prop('date'));

  // buildEvent :: o -> builder -> o ->
  const buildEvent = curry((nconf, builder, matchdata) => {

    //  liftIcal :: maybe t -> maybe k -> maybe l -> maybe l -> maybe d -> 
    //    maybe dt -> maybe dt
    const liftIcal = lift(buildIcalEvent(nconf.get('site:url'), [120, 30, 5]));

    // pushEvent :: builder -> o -> o 
    const pushEvent = curry((builder, event) => {
        builder.events.push(event);
        return event;
    });

    liftIcal(
      S.prop('title', matchdata), 
      S.prop('key', matchdata), 
      S.prop('locname', matchdata), 
      S.prop('description', matchdata), 
      S.prop('date', matchdata), 
      S.prop('timeend', matchdata)
    ).map(pushEvent(builder));

  });

  router.route('/:email/calendars')
    .post(jsonParser, function(req, res) {

      // liftRegen :: Maybe s -> Maybe s
      const liftRegen = lift(accounts.RegenerateCalendarId(account));

      (S.path(['body', 'action'], req).getOrElse() === 'regenkey') 
        ? liftRegen(safeMail(req)).getOrElse(taskFail()).fork(
          err => res.SendStatus(404),
          data => {
            res.json({
              newurl: nconf.get('site:url') + '/api/v1/accounts/' + data.email 
                + '/calendars/' + data.icalkey + '/rollforgroup.ics'
            });
          }) 
        : res.sendStatus(500);

    });

    // findUserType :: s -> f({players:[{email:s, type:s}]}) => s
    const findUserType = (email) => 
      compose(
        prop('type'), 
        head, 
        filter(R.propEq('email', email)), 
        prop('players')
      );
    exports.findUserType = findUserType;

    router.route('/:email/calendars/:icalkey/rollforgroup.ics')
      .get(function(req, res) {

        // safeParam :: s -> f(o) => maybe s
        const safeParam = (key) => S.path(['params' , key]);

        // iCalCheck :: maybe s -> maybe t -> Task o
        const iCalCheck = lift(accounts.TaskCheckIcalKey(account));

        // lookup games where this player is in them
        const builder = icalToolkit.createIcsFileBuilder(); 

        // isMailOfType :: s -> o -> b 
        const isMailOfType = curry((type, data) => 
          R.equals(type, findUserType(safeMail(req).getOrElse(''))(data))
        ); 

        iCalCheck(safeMail(req), safeParam('icalkey')(req))
          .getOrElse(taskFail())
          .fork(
            err => res.sendStatus(404),
            data => {

              // create vcal and send back
              builder.calname = 'Roll for Group';
              builder.method = 'REQUEST';
              
              // findMatches :: maybe o => task o 
              const findMatches = lift(matches.FindMatchByUser(match)); 

              findMatches(safeMail(req)).getOrElse(taskFail()).fork(
                err   => res.sendStatus(404),
                data  => {

                  // filterUnapproved :: [o] => [o] 
                  const filterUnapproved = compose(
                    R.reject(isMailOfType('invite')), 
                    R.reject(isMailOfType('request')), 
                    R.reject(isMailOfType('reject'))
                  );

                  filterUnapproved(data).map(buildEvent(nconf, builder));
                  res.send(builder.toString());

                }
              );
            }
          );
      });

    // buildLink :: n -> s 
    const buildLink = (id) => 'https://boardgamegeek.com/boardgame/' + id;
    exports.BuildLink = buildLink;

    // buildBggLink :: o -> maybe o
    const buildLinkObj = compose(
      map(R.objOf('bgglink')), 
      map(buildLink), 
      S.prop('id')
    );
    exports.BuildLinkObj = buildLinkObj;

    // meregeLink :: o -> o
    const mergeLink = (gameObj) => 
      merge(buildLinkObj(gameObj).getOrElse({}), gameObj);
    exports.MergeLink = mergeLink;

    // safeMatch :: o -> s -> maybe s 
    const safeMatching = curry((gameObj, list) => 
      S.path(['matching', list], gameObj));
    exports.SafeMatching = safeMatching;

    // buildFilter :: o -> o => b
    const buildFilter = curry((req, gameObj) => 
      chain(safeMatching(gameObj), S.path(['query', 'listtype'], req))
        .getOrElse(false));
    exports.BuildFilter = buildFilter;

    // findPropId :: s -> [o] -> n -> o  
    const findProp = curry((propname, list, id) => 
      S.find(R.propEq(propname, id), list)
      .getOrElse({}));
    exports.FindProp = findProp;

    // updateObjList :: [o] -> s -> o -> maybe o
    const updateObjList = curry((extendedGames, propname, obj) => 
      compose(
        map(merge(obj)), 
        map(R.objOf(propname)), 
        chain(S.prop(propname)), 
        map(findProp('id', extendedGames)), 
        S.prop('id'))(obj)
    );
    exports.UpdateObjList = updateObjList;

    // updateAmazonObjList :: [o] -> s -> s -> o -> maybe o
    const updateAmazonObjList = curry((extendedGames, domain, propname, obj) => 
      compose(
        map(merge(obj)), 
        map(R.objOf(propname)), 
        chain(S.prop(propname)), 
        map(findProp('domain', R.__, domain)), 
        chain(S.prop('amazon')), 
        map(findProp('id', extendedGames)), 
        S.prop('id')
      )(obj)
    );
    exports.UpdateAmazonObjList = updateAmazonObjList;

    // safeGtTen :: n => maybe n
    const safeGtTen = (a) => R.gt(a, 10) 
      ? Maybe.of(a) 
      : Maybe.Nothing();
    exports.SafeGtTen = safeGtTen;

    // percentasize :: s -> b
    const percentasize = (a) => ({discount: a + '% off'});
    exports.Percentasize = percentasize;

    // calcdiscount :: maybe n -> maybe n -> maybe n
    const calcdiscount = lift((retailprice, saleprice) => 
      compose(
        Math.round, 
        R.multiply(100), 
        R.divide(R.__, retailprice), 
        R.subtract(retailprice)
      )(saleprice));
    exports.Calcdiscount = calcdiscount;

    // updateAmazonPriceObjList :: [o] -> s -> o -> maybe o
    const updateAmazonPriceObjList = curry((extendedGames, domain, obj) => {

      // [o] -> s -> {id: o} -> maybe a 
      const getProp = curry((extendedGames, propname, obj) => 
        compose(
          chain(S.prop(propname)), 
          map(findProp('domain', R.__, domain)), 
          chain(S.prop('amazon')), 
          map(findProp('id', extendedGames)), 
          S.prop('id')
        )(obj)
      );

      return Maybe.of(
        calcdiscount(
          getProp(extendedGames, 'rrp', obj), 
          getProp(extendedGames, 'saleprice', obj)
        )
          .chain(safeGtTen)
          .map(percentasize)
          .map(merge(obj))
          .getOrElse(obj)
      );

    });
    exports.UpdateAmazonPriceObjList = updateAmazonPriceObjList;

    // updateGame :: [{e}] -> {g} => {o}
    const updateGame = curry((extendedGames, userGame) => {

      // updateObj :: s -> {g} => Maybe({o})
      const updateObj = updateObjList(extendedGames);

      const chainUpdates = compose( 
        chain(updateObj('minplaytime')), 
        chain(updateObj('maxplaytime')), 
        chain(updateObj('minplayers')), 
        chain(updateObj('avgrating')), 
        chain(updateObj('bggrank')), 
        chain(updateObj('weight')), 
        chain(updateObj('playercounts')), 
        updateObj('maxplayers')
      );

      return chainUpdates(userGame).getOrElse(userGame);

    });
    exports.UpdateGame = updateGame;

    // taskWhenNewOrType :: maybe s -> maybe s -> Task f(s) => [o]
    const taskWhenNewOrType = lift((isnew, mailtype) => 
      (mailtype === 'outbox') 
        ? accounts.TaskGetSentMails(account)
        : (isnew === 'new') 
          ? accounts.TaskGetNewMails(account) 
          : accounts.TaskGetAllMails(account)
    );

    // privateMailsPrepData :: o -> o
    const privateMailsPrepData = (data) => 
      compose(
        mergeCount(data), 
        merge(faObj('faIconMailClass', 'fa-envelope', data)), 
        R.objOf('privatemails'), 
        reverse, 
        sortByDate, 
        map(mergeFromNow),
        map(scrub)
      )(data);

    router.route('/:email/privatemails')
      .get(jsonParser, function(req, res) {
        taskCheck(Maybe.of(account), safeMail(req), maybeKey(req, 'query'))
          .getOrElse(taskFail())
          .fork(
            err => res.sendStatus(401),
            data => safeMail(req)
              .chain(
                taskWhenNewOrType(
                  S.pathDefaultTo('', ['query', 'type'], req), 
                  S.pathDefaultTo('', ['query', 'mailtype'], req)
                )
                  .getOrElse(accounts.TaskGetAllMails(account))
              )
              .fork(
                err =>  res.sendStatus(500),
                data => res.json(privateMailsPrepData(data))
              )
          )

      })
      .post(jsonParser, function(req, res) {
        taskCheck(Maybe.of(account), safeMail(req), maybeKey(req, 'body'))
          .getOrElse(taskFail()).fork(
            err => res.sendStatus(401),
            data => 
            {
              Promise.all([accounts.ReadAnAccountFromEmail(account, data.profile.email), accounts.ReadAnAccountFromPageSlug(account, req.body.pageslug)])
                .then((users) => {
                  R.sequence(Task.of, [
                    lift(accounts.TaskMailUser(account))(
                      S.prop('pageslug', users[0]), 
                      S.path(['profile', 'name'], users[0]), 
                      S.pathDefaultTo('/img/default-avatar-sq.jpg', ['profile',  'avataricon'], users[0]),
                      S.path(['body',    'pageslug'], req), 
                      S.path(['body',    'subject' ], req), 
                      S.path(['body',    'text'    ], req), 
                      Maybe.of('inbox'),
                      Maybe.of('new')
                    ).getOrElse(taskFail()),
                    lift(accounts.TaskMailUser(account))(
                      S.prop('pageslug', users[1]), 
                      S.path(['profile', 'name'], users[1]), 
                      S.pathDefaultTo('/img/default-avatar-sq.jpg', ['profile', 'avataricon'], users[1]), 
                      S.prop('pageslug', users[0]), 
                      S.path(['body',    'subject' ], req), 
                      S.path(['body',    'text'    ], req), 
                      Maybe.of('outbox'),
                      Maybe.of('read')
                    ).getOrElse(taskFail())
                  ]).fork(
                    err   => res.sendStatus(500),
                    data  => res.json({status:'ok'})
                  )
              })
            }
          )
      })

    router.route('/:email/privatemails/:mailid')
      .patch(jsonParser, function(req, res) {

        // safeMailId :: {params: {mailid: s}} -> maybe s
        const safeMailId = S.path(['params', 'mailid']);

        taskCheck(Maybe.of(account), safeMail(req), maybeKey(req, 'body'))
          .getOrElse(taskFail())
          .fork(
            err 	=> res.sendStatus(401),
						data 	=> lift(accounts.TaskReadMail(account))(safeMail(req), safeMailId(req))
              .getOrElse(taskFail())
              .fork(
                err   => res.sendStatus(500),
                data  => lift(accounts.taskGetMail(account))
                  (safeMail(req), safeMailId(req))
                    .getOrElse(taskFail())
                    .fork(
                      err => res.sendStatus(500),
                      readMail => res.json(readMail)
                    )
              ) 
          )
      })

    router.route('/:email/notifications')
        .get(jsonParser, function(req, res) {

          // liftRecord :: Maybe(e) => Maybe(Task())
          const liftRecord = lift(accounts.TaskRecordActivity);
          taskCheck(Maybe.of(account), safeMail(req), maybeKey(req, 'query')).getOrElse(taskFail()).fork(
            err => res.sendStatus(401),
            data => {
              safeMail(req).chain(accounts.TaskGetNewNotifications(account)).fork(
                err =>  res.sendStatus(500),
                data => {
                  const prepData = compose(mergeCount(data), merge(faObj('faIconNotifyClass', 'fa-bell', data)), R.objOf('notices'), reverse, sortByDate, map(mergeFromNow), map(scrub));
                  liftRecord(Maybe.of(account), safeMail(req)).getOrElse(taskFail()).fork(err => reject(err), dataL => {
                    res.json(prepData(data));
                  });
                }
              );
            }
          )
        })
        .post(jsonParser, function(req, res) {

          const safeText = S.path(['body', 'text']);
          const safeThumbnail = S.path(['body', 'thumbnail']);
          const safeUrl = S.path(['body', 'url']);

          taskCheck(Maybe.of(account), safeMail(req), maybeKey(req, 'body')).getOrElse(taskFail()).fork(
            err => res.sendStatus(401),
            data => {
              lift(accounts.TaskNotifyUser)(Maybe.of(account), safeText(req), safeMail(req), safeThumbnail(req), safeUrl(req), Maybe.of('api'), Maybe.of(false)).getOrElse(taskFail()).fork(
                err => res.sendStatus(500),
                data => {
                  res.json({status:'ok'})
                }
              )
            }
          )
        })

        .patch(jsonParser, function(req, res) {

          const safeStatus = S.path(['body', 'status']);
          const isread = (status) => status == 'read';

          taskCheck(Maybe.of(account), safeMail(req), maybeKey(req, 'body')).getOrElse(taskFail()).fork(
            err => {
              res.sendStatus(401)
            },
            checkdata => {
              (safeStatus(req).map(isread).getOrElse(false))
                ? lift(accounts.TaskReadNotifications)(Maybe.of(account), safeMail(req)).getOrElse(taskFail()).fork(
                  err => {
                    res.sendStatus(500);
                  },
                  data => {
                    res.json({update: 'ok'})
                  }
                ) 
                : res.sendStatus(400) 
            }
          )
        });

  router.route('/:email/notifications/:notifyid')
    .patch(jsonParser, function(req, res) {

        const safeNoteId = S.path(['params', 'notifyid']);
        const safeStatus = S.path(['body', 'status']);
        const isread = (status) => status == 'read';

        taskCheck(Maybe.of(account), safeMail(req), maybeKey(req, 'body'))
          .getOrElse(taskFail()).fork(
            err => {
              res.sendStatus(401)
            },
            checkdata => {
              (safeStatus(req)
                .map(isread)
                .getOrElse(false))
                  ? lift(accounts.TaskReadNotification)(
                      Maybe.of(account), 
                      safeMail(req), 
                      safeNoteId(req))
                    .getOrElse(taskFail())
                    .fork(
                      err => res.sendStatus(500),
                      data => {
                        res.json({update: 'ok'})
                      }
                    ) 
                  : res.sendStatus(400) 
            }
          )
        })

    router.route('/:email/purchase')
      .get(jsonParser, function(req, res) {
        if (S.pathEq(['query', 'request'], 'clienttoken', req).isJust) {
          braintreeGateway.clientToken.generate({}, function(err, response) {
            res.json({'clienttoken': response.clientToken});
          });
        } else {
          res.sendStatus(400);
        }
      })
      .patch(jsonParser, function(req, res) {
        var ordercode = shortid.generate();
        const safeBody = (prop) => S.path(['body', prop], req);
        const safeGiftGem = lift(accounts.GiftGem(account));

        safeGiftGem(Maybe.of(ordercode), safeMail(req), safeBody('transferto')).getOrElse(taskFail()).fork(
          err => res.sendStatus(403),
          data => {
            res.json({status: 'ok'})
          }
        )

      })
      .post(jsonParser, function(req, res) {

			// resResponse ::
			const processPurchase = curry((res, account, email, apikey, items, paymentnonce) => {

				// lookupPrice :: s => n
				const lookupPrice = (item) => 
					(item == 'rfggold-once') ? 5 : 
					(item == 'rfggold-ongoing') ? 5 : 
					(item == 'rfggold-credit') ? 5 : 
					(item == 'rfggold-once-12pack') ? 50 : 
					(item == 'rfggold-ongoing-12pack') ? 50 : 
					(item == 'rfggold-credit-12pack') ? 50 : 0

        // getTotal :: Maybe(n) -> Maybe(s)		
				const getTotal = lift((quant, code) => quant * code);

				// safePrice :: {o} => Maybe(p) 
				const safePrice = compose(map(lookupPrice), S.prop('code'))

				// calcTotal :: {o} => Maybe(s)
				const calcTotal = (item) => getTotal(safePrice(item), S.prop('quant', item));

				// calcTotals :: [{o}] => Maybe(n)
				const calcTotals = compose(map(R.sum), R.sequence(Maybe.of), map(calcTotal));

				// mergePrice :: {o} => {p}
				const mergePrice = (a) => merge({price: calcTotal(a).getOrElse(0)}, a);

        accounts.CheckValidEmail(account, email, apikey).then(function(x) {
					braintreeGateway.transaction.sale({
						amount: calcTotals(items).getOrElse(0),
						paymentMethodNonce: paymentnonce,
						options: {
							submitForSettlement: true
						}
					}, function (err, result) {
						if (result.success) {
							accounts.LogPurchase(account, email, result.transaction.id, items.map(mergePrice), calcTotals(items).getOrElse(0), result.transaction.currencyIsoCode, result.transaction.createdAt).fork(
								err => log.clos('err1', err),
								data => {
                  const buyItem = (item) => lift(accounts.PurchaseItem(account, email))(S.prop('code', item), S.prop('quant', item)).getOrElse(taskFail());
                  R.sequence(Task.of, items.map(buyItem)).fork(
                    err => log.clos('err2', err),
                    data => res.json({status:'ok'})
                  )
                }
							);
						} else { 
							res.sendStatus(400);
						}
					});
				});

			});

			// liftPurchase :: (Maybe(e) -> Maybe(a) -> Maybe(i) -> Maybe(p)) => Maybe()
			const liftPurchase = lift(processPurchase(res, account));

			liftPurchase(S.path(['params', 'email'], req), maybeKey(req, 'body'), S.path(['body', 'items'], req), S.path(['body', 'nonce'], req) );

    });

    // no tests for this section, as I don't know how to run tests on the facebook API
    router.route('/:email/updatefacebook')
      .patch(jsonParser, function(req, res) {

        // resResponse :: ({res} -> Schema -> email -> obj -> key) => Promise
        const resResponse = curry((res, account, email, apikey) =>
          accounts.CheckValidEmail(account, email, apikey).then(function(x) {
            accounts.UpdateFacebookPage(nconf, account, x.profile.email)
              .then(x => res.json({icon: x.profile.avataricon}))
              .catch(err => res.sendStatus(err));
          }).catch(err => res.sendStatus(401))
        );

        if (lift(resResponse(res, account))(S.path(['params', 'email'], req), maybeKey(req, 'body')).isNothing)
          res.sendStatus(401);

      });


    router.route('/nearby/coordinates')
      .get(function(req, res) {

        // taskFindPlayers :: Schema a -> n -> n -> n => Task [{a}];
        const taskFindPlayers = R.curry((account, lat, lng, distance) => 
          new Task((reject, fulfill) => 
            (S.path(['query', 'name'],req).isJust)
              ? accounts
                .FindPublicBoargamePlayersByNameCoords(account, 150, req.query.name, [lng, lat])
                .then(fulfill)
                .catch(reject)
              : accounts
                .FindPublicBoargamePlayersByCountCoords(account, distance, [lng, lat])
                .then(fulfill)
                .catch(reject)
          )
        );

        // liftFindPlayers :: Maybe n -> Maybe [n, n] => Maybe Task [{a}]
        const liftFindPlayers = R.lift(taskFindPlayers(account));

        // mergeDistance :: Number -> Number -> Nubmer -> Number -> {o} -> {o}
        var mergeDistance = R.curry((lat1, lng1, obj) => {

          try {
            return R.merge(obj, {distance: 
              parseInt(geolib.getDistanceSimple(
                {
                  latitude: Number(lat1), 
                  longitude: Number(lng1)
                },
                {
                  latitude: Number(obj.profile.localarea.loc.coordinates[1]), 
                  longitude: Number(obj.profile.localarea.loc.coordinates[0])
                }
              ) / 1000) // convert meters to km
            })
          } catch (err) {
            return R.merge(obj, {distance: ''});
          }

        });

        // mergeDistanceObj :: {o} -> {o} 
        const mergeDistanceObj = mergeDistance(
          valQueryToNumber('lat', req).get(), 
          valQueryToNumber('lng', req).get()
        );

        liftFindPlayers(
          valQueryToNumber('lat', req),
          valQueryToNumber('lng', req),
          valQueryToNumber('distance', req)
        ).getOrElse(taskFail()).fork(
          err => res.sendStatus(500),
          data => {
            res.json(
              R.pathEq(['query', 'xptype'], 'host', req)
                ? sortByHostXP(data)
                  .map(mergeDistanceObj)
                : sortByPlayXP(data)
                  .map(mergeDistanceObj)
            );
          }
        );

      });


    router.route('/nearby/email/:email')
      .get(function(req, res) {

        // taskFindPlayersByEmail :: Schema a -> n -> s => Task [{a}];
        const taskFindPlayersByEmail = R.curry((account, email, distance) => 
          new Task((reject, fulfill) => 
            (S.path(['query', 'name'], req).isJust)
              ? accounts
                .FindPlayersNearbyName(account, email, req.query.name, distance)
                .then((a) => {
                  fulfill(a);
                })
                .catch(reject)
              : accounts
                .FindPlayersNearby(account, email, distance)
                .then(fulfill)
                .catch(reject)
          )
        );

        // liftFindPlayersByEmail :: Maybe n -> Maybe s => Maybe Task [{a}]
        const liftFindPlayersByEmail = R.lift(taskFindPlayersByEmail(account));

        const mergeDistance = (obj) => R.merge(obj, {distance:''});

        liftFindPlayersByEmail(
          S.path(['params', 'email'], req),
          valQueryToNumber('distance', req) 
        ).getOrElse(taskFail()).fork(
          err => res.sendStatus(500),
          data => {
            res.json(
              R.pathEq(['query', 'xptype'], 'host', req)
                ? sortByHostXP(data).map(mergeDistance)
                : sortByPlayXP(data).map(mergeDistance)
            );
          }
        );

      });

    router.route('/:email/unsubscribe')
			.patch(jsonParser, function(req, res) {

        // verifyUser :: maybe s -> maybe s -> maybe task
        const verifyUser = R.lift(accounts.TaskVerifyAnAccountFromEmail(account));

				verifyUser(
          S.path(['params', 'email'], req),
          S.path(['body', 'key'], req)
				).getOrElse(taskFail()).fork(
					err => 	res.sendStatus(401),
					data => {
						account.findOneAndUpdate({
							email: S.path(['params', 'email'], req).getOrElse('')
						}, {
							$set: {
								'profile.notifyupdates': false,
								'profile.notifyevents': false
							}
						}, {
							new: true
						}).exec().then(
							updateddata => res.json({notifyupdates: updateddata.toObject().profile.notifyupdates}),
							err => { res.sendStatus(500) }
						);

					}
				);

			});

    router.route('/:pageslug/networkmap')
      .get(function(req, res) {
        S.path(['params', 'pageslug'], req).cata({
          Just: slug => 
            taskUserNode(slug).fork(
              () => res.sendStatus(500),
              (user) => {
                taskFindFriends(slug).fork(
                  () => res.sendStatus(500),
                  (friends) => {
                    taskFriendsFromNodes(friends).fork(
                      (err) => log.clos('err', err),
                      (friendsoffriends) => {

                        res.send({
                          nodes: R.unionWith(R.eqProps('id'), unionWithUniqueId(friends, user), R.flatten(R.map(R.prop('nodes'), friendsoffriends))),
                          edges: R.unionWith(compareFromTo, R.prop('edges', friends), flatEdges(friendsoffriends))
                          

                        })
                      }
                    )
                  }
                )
              }
            ),
          Nothing: () => res.sendStatus(500)
        });



      });

    return router;

}; 
exports.SetupRouter = setupRouter;
