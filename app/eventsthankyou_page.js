var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');
var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var Promise = require('bluebird');
var R = require('ramda');

var util = require('util');
var log = require('./log.js');
var S = require('./lambda.js');
var expressHelper = require('./expresshelper');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

// renderData :: Render -> String -> Object -> Render 
var renderData = R.curry((res, template, data) => res.render(template, data));

// maybebuildNavs :: {o} => {m} 
const maybeBuildNavs = (req) => S.path(['user', 'email'], req).map(expressHelper.BuildNavbar('')).map(R.objOf('navbarul'));


// routegame :: Express -> Object -> Model -> Schema -> Schema -> 0
var routeEventsThankyou = function (expressApp, CheckLogin, match, account) {

	var url = "eventsthankyou";
	var urlFail = "login";

  var slashString = (file) => '/' + file;

	expressApp.get(slashString(url), CheckLogin.ensureLoggedIn(slashString(urlFail)), function(req, res) {

    // sendCleanedPage :: o -> s -> o -> o -> o
    var sendCleanedPage = R.curry((res, url, pagedata, navbars) => R.compose(
      renderData(res, url), 
      R.merge(maybeBuildNavs(req).getOrElse({})), 
      R.merge(navbars), 
      R.dissoc('__v'), 
      R.dissoc('_id'), 
      JSON.parse, 
      JSON.stringify, 
      R.head
    )(pagedata));

    if (R.hasIn('matchkey', req.query)) {
      var email = R.path(['user', 'email'], req);

      // lookup match
      matches.FindMatchByKey(match, req.query.matchkey).then(function(x) {
        if (x.length == 1) {
          accounts.BuildNavbars(account, email).then(sendCleanedPage(res, url, x));
        } else {
          res.redirect('events');
        }

      }).catch(function(err) {
        res.redirect('events');
      });

    } else {
      res.redirect('events');
    }
	});
};
exports.RouteEventsThankyou = routeEventsThankyou;
