// function for the purpose of managing groups
var Promise = require('bluebird');
var R = require('ramda');
var S = require('./lambda.js');

var account   = require('../app/models/account.js');
var accounts  = require('../app/models/accounts.js');

var group   = require('../app/models/group.js');
var groups  = require('../app/models/groups.js');

var express = require('express');
var bodyParser = require('body-parser')
var util = require('util');
var expressHelper = require('./expresshelper');

var Task = require('data.task');
var Maybe = require('data.maybe');
var Either = require('data.either');

var log = require('./log.js');
var exports = module.exports = {};


// renderData :: Response -> String -> Object -> Render 
var renderData = R.curry((res, template, data) => res.render(template, data));

// maybeBuildNavs :: o -> o
const maybeBuildNavs = (req) => S
  .path(['user', 'email'], req)
  .map(expressHelper.BuildNavbar(''))
  .map(R.objOf('navbarul'));


// routeNewGroupPage :: Express -> Object -> Model -> Promise() 
var routeNewGroupPage = R.curry(function (expressApp, CheckLogin) {

  expressApp.get('/groups', function(req, res) {

    // composeUser :: ({u} -> e) => express.render
    const composeUser = R.curry((email) => R.composeP(
      renderData(res, 'groups'), 
      R.merge({css:[{csshref: '/css/groups.css'}]}),
      R.merge(maybeBuildNavs(req).getOrElse({})), 
      accounts.BuildNavbars(account)
    )(email));

    S.path(['user', 'email'], req)
      .map(composeUser);

  });
});
exports.routeNewGroupPage = routeNewGroupPage;
