var R = require('ramda');

var smap = require('express-sitemap');

var match   = require('./models/match.js');
var matches = require('./models/matches.js');
var S       = require('./lambda.js');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var moment = require('moment');
var log = require('./log.js');

// buildUrl :: s -> s
const buildURL = (key) => "/events/" + key;
exports.buildURL = buildURL;

// buildSeriesUrl :: s -> s
const buildSeriesURL = (key) => "/events/series/" + key;
exports.buildSeriesURL = buildSeriesURL;

// maybeBuildEitherURL :: o -> maybe s
const maybeBuildEitherURL = (obj) => S 
  .pathEq(['reoccur', 'enable'], true, obj)
  .cata({
    Just:     () => S.prop('seriesid', obj).map(buildSeriesURL),
    Nothing:  () => S.prop('key', obj).map(buildURL)
  });
exports.maybeBuildEitherURL = maybeBuildEitherURL;

// buildSiteGet :: s -> [s, o]
const buildSiteGet = (keyValue) => R.objOf( keyValue, ['get'] );
exports.buildSiteGet = buildSiteGet;

// buildGetKey :: o -> maybe s
const buildGetKey = R.compose(
  R.map(buildSiteGet), 
  maybeBuildEitherURL
);
exports.buildGetKey = buildGetKey;

// buildSiteRoute :: s -> [s, o]
const buildSiteRoute = R.curry((date, status, keyValue) => R.objOf(keyValue, {
  lastmod: moment(date).format('YYYY-MM-DD'),
  changefreq: (status === 'open')
    ? 'hourly'
    : (status === 'full') 
      ? 'daily'
      : 'monthly',
  priority: (status === 'open') 
    ? 1.0
    : (status === 'full') 
      ? 0.8
      : 0.4
}));
exports.buildSiteRoute = buildSiteRoute;

// buildKeyUrls :: o -> ([o] => o)
const buildKeyUrls = (staticUrls) => R.compose(
  R.map(R.objOf('map')),
  R.map(R.merge(staticUrls)), 
  R.map(R.mergeAll), 
  R.sequence(Maybe.of),
  R.map(buildGetKey)
);
exports.buildKeyUrls = buildKeyUrls;

// buildSite :: s -> s -> s -> o
const buildSite = R.curry((oDate, oStatus, oKey) => R.compose(
  buildSiteRoute(oDate, oStatus), 
  buildURL
)(oKey));
exports.buildSite = buildSite;

// buildSiteSeries :: s -> s -> s -> o
const buildSiteSeries = R.curry((oDate, oStatus, oKey) => R.compose(
  buildSiteRoute(oDate, oStatus), 
  buildSeriesURL
)(oKey));
exports.buildSiteSeries = buildSiteSeries;

// buildRouteKey :: o -> maybe s
const buildRouteKey = (obj) => {
  return S
    .pathEq(['reoccur', 'enable'], true, obj)
    .cata({
      Just: () => {
        return S.prop('updatedAt', obj)
          .map(buildSiteSeries)
          .ap(S.prop('status', obj))
          .ap(S.prop('seriesid', obj))
      },
      Nothing: () => {
        return S.prop('updatedAt', obj)
          .map(buildSite)
          .ap(S.prop('status', obj))
          .ap(S.prop('key', obj));
      }
    });
}
exports.buildRouteKey = buildRouteKey;

// buildRouteUrls :: o -> [0]
const buildRouteUrls = R.compose(
  R.map(R.objOf('route')),
  R.map(R.mergeAll), 
  R.sequence(Maybe.of),
  R.map(buildRouteKey)
);
exports.buildRouteUrls = buildRouteUrls;

var routeSiteMap = function(app) {
  app.get('/sitemap.xml', function(req, res) {
    match.find({
      isprivate:false, 
      $or:[
        {status:'open'}, 
        {status:'full'}
      ]
    }, {
      key: 1, 
      updatedAt: 1, 
      status: 1,
      seriesid: 1,
      reoccur: 1
    }).exec().then((ms) => {
      var sitemap = smap(
        R.mergeAll([
          {url:'www.rollforgroup.com'}, 
          {http:'https'}, 
          buildRouteUrls(ms).getOrElse(), 
          buildKeyUrls({
            '/home' : [ 'get' ],
            '/docs/hostevent.html': [ 'get' ],
            '/docs/inviteevent.html': [ 'get' ],
            '/docs/joinevent.html': [ 'get' ],
            '/docs/useraccount.html': [ 'get' ],
            '/docs/userforgotpassword.html': [ 'get' ],
            '/docs/userlinkbgg.html': [ 'get' ],
            '/docs/membership.html': [ 'get' ],
            '/docs/premiumgems.html': [ 'get' ],
            '/docs/linkcalendar.html': [ 'get' ],
            '/sitemap.xml': [ 'get' ]
          })(ms)
          .getOrElse({})])
      );
      sitemap.XMLtoWeb(res);

    });

  });
};
exports.RouteSiteMap = routeSiteMap;

