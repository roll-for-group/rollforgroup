var accounts = require('../app/models/accounts.js');
var account = require('../app/models/account.js');

var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var Promise = require('bluebird');
var R = require('ramda');

const compose = R.compose,
    curry = R.curry,
    gt = R.gt,
    head = R.head,
    isEmpty = R.isEmpty,
    isNil = R.isNil,
    map = R.map,
    mapAccum = R.mapAccum,
    path = R.path,
    prop = R.prop,
    props = R.props;

var util = require('util');
var log = require('./log.js');
var expressHelper = require('./expresshelper');
var S = require('./lambda');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

// slashString :: String -> String
var slashString = (endpoint) => "/" + endpoint;

// renderData :: Render -> String -> Object -> Render 
var renderData = curry((res, template, data) => res.render(template, data));


// untilUpdatedAssocSchedules :: {r} => f()
const untilUpdatedAssocSchedules = (req) => {

  const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

  // whenTrueOne :: b => n
  const whenTrueOne = (a) => a ? 1 : 0

  // appender (a -> b) => [c, c]
  var appender = (a, b) => [a + b, a + b];

  // hasMotive :: [{o}] => {p}
  const hasMotive = compose(gt(R.__, 0), head, mapAccum(appender, 0), map(whenTrueOne), props(days));

  const sections = [{
    questions: [{
      checkbox: {
        instructable: "What times and days of the week are you available?",
        id: "availability",
        options: [
          {option: 'Monday',      id: 'Monday'},
          {option: 'Tuesday',     id: 'Tuesday'},
          {option: 'Wednesday',   id: 'Wednesday'},
          {option: 'Thursday',    id: 'Thursday'},
          {option: 'Friday',      id: 'Friday'},
          {option: 'Saturday',    id: 'Saturday'},
          {option: 'Sunday',      id: 'Sunday'},
        ]
      }
    },
    {
      slider: {
        instructable: "Monday Availability",
        id: {
          id: "mondayslider", 
          values: {
            starthour:  "mondaystarthour",
            startminute: "mondaystartminute",
            endhour:    "mondayendhour",
            endminute:  "mondayendminute"
          }
        }
      }
    },
    {
      slider: {
        instructable: "Tuesday Availability",
        id: {
          id: "tuesdayslider", 
          values: {
            starthour:  "tuesdaystarthour",
            startminute: "tuesdaystartminute",
            endhour:    "tuesdayendhour",
            endminute:  "tuesdayendminute"
          }
        },
        datastarthour: 18,
        datastartminute: 30,
        dataendhour: 22,
        dataendminute: 0
      }
    },
    {
      slider: {
        instructable: "Wednesday Availability",
        id: {
          id: "wednesdayslider", 
          values: {
            starthour:  "wednesdaystarthour",
            startminute: "wednesdaystartminute",
            endhour:    "wednesdayendhour",
            endminute:  "wednesdayendminute"
          }
        },
      }
    },
    {
      slider: {
        instructable: "Thursday Availability",
        id: {
          id: "thursdayslider", 
          values: {
            starthour:  "thursdaystarthour",
            startminute: "thursdaystartminute",
            endhour:    "thursdayendhour",
            endminute:  "thursdayendminute"
          }
        },
      }
    },
    {
      slider: {
        instructable: "Friday Availability",
        id: {
          id: "fridayslider", 
          values: {
            starthour:  "fridaystarthour",
            startminute: "fridaystartminute",
            endhour:    "fridayendhour",
            endminute:  "fridayendminute"
          }
        },
      }
    },
    {
      slider: {
        instructable: "Saturday Availability",
        id: {
          id: "saturdayslider", 
          values: {
            starthour:  "saturdaystarthour",
            startminute: "saturdaystartminute",
            endhour:    "saturdayendhour",
            endminute:  "saturdayendminute"
          }
        }
      }
    },
    {
      slider: {
        instructable: "Sunday Availability",
        id: {
          id: "sundayslider", 
          values: {
            starthour:  "sundaystarthour",
            startminute: "sundaystartminute",
            endhour:    "sundayendhour",
            endminute:  "sundayendminute"
          }
        }
      }
    }]
  }];

  return S.path(['user', 'profile'], req).map(hasMotive).getOrElse(true) 
    ? R.merge({}) 
    : R.assoc('sections', sections); 

}


// routeGetSchedule :: Express -> Object -> Model -> Schema -> Schema -> 0
var routeGetGamelist = curry(function (expressApp, CheckLogin, account, nconf, page) {

	expressApp.get(slashString(page.url), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res){

    // dataOnLoad :: o -> o
    var dataOnLoad = (ajaxdata) => R.zipObj(['ajaxdata'], [ajaxdata]);

    // buildOnLoad :: o -> o
    var buildOnLoad = (ajaxdata) => R.zipObj(['onload'], [dataOnLoad(ajaxdata)]);

    var matchlist = {
      schedule:   {matchtypes: {types:['approve', 'host', 'invite', 'request'], user: req.user.email}},
      history:    {history: true, matchtypes: {types:['approve', 'host'], user: req.user.email}}
    }; 

    // buildURL :: s -> s
    const buildURL = (event) => page.url + '?event=' + event;

    // maybebuildNavs :: {o} => {m} 
    const maybeBuildNavs = (req) => R.lift(expressHelper.BuildNavbar)(S.prop('url', page), S.path(['user', 'email'], req));

    const maybeBuildNavsEvent = (req) => R.lift(expressHelper.BuildNavbarEvent(S.path(['user', 'email'], req)))(S.path(['query', 'event'], req), S.path(['query', 'event'], req).map(buildURL));

    const maybeNavs = (req) => (S.path(['query', 'event'], req).isJust) ? maybeBuildNavsEvent(req) : maybeBuildNavs(req);

    // mergePremium :: {r} => f({u} -> {o})
    const mergePremium = (req) => R.merge(S.path(['user', 'premium', 'isactive'], req).map(R.objOf('ispremium')).getOrElse({ispremium:false}));

    // buildCalendarUrl :: Maybe(e) -> Maybe(c) => f({u} -> {o})
    const buildCalendarUrl = R.lift((email, icalkey) => nconf.get('site:url')+ '/api/v1/accounts/' + email + '/calendars/' + icalkey + '/rollforgroup.ics');

    // mergeIcalkey :: {r} => f({u} -> {o})
    const mergeIcallink = curry((nconf, req) => R.merge(buildCalendarUrl(S.path(['user', 'email'], req), S.path(['user', 'icalkey'], req)).map(R.objOf('icalurl')).getOrElse({})))

    const buildConvHeader = (event) => 'Convention / ' + event;

		var composeUserData = R.composeP(
      renderData(res, page.url), 
      mergeIcallink(nconf, req), 
      mergePremium(req), 
      R.merge(expressHelper.BuildNavbarHead(S.path(['query', 'event'], req).map(buildConvHeader).getOrElse('Game List'))), 
      untilUpdatedAssocSchedules(req), 
      R.merge(maybeNavs(req).map(R.objOf('navbarul')).getOrElse({})), 
      R.merge(buildOnLoad(R.prop(page.url, matchlist))), 
      R.merge(page.handlebars), 
      accounts.BuildNavbars(account)
    );

    // note user activity
    S.path(['user', 'email'], req).chain(accounts.TaskRecordActivity(account));
    composeUserData(req.user.email);

	});
	return 0; // success

});
exports.RouteGetGamelist = routeGetGamelist;


// routeMatchLists :: Express -> Object -> Schema -> Route
var routeMatchLists = function(expressApp, CheckLogin, account, nconf) {

  var buildFaIcon = (faicon) => R.zipObj(['class'], ['fa fa-' + faicon + ' fa-fw']);
  // var buildImage = (classes, source) => R.zipObj(['class', 'src'], [classes, source]);
  var buildHeading = (name, icon, value) => R.zipObj(['name', icon], [name, value]); 
  var buildPage = (url, name, icon, value) => R.zipObj(['url', 'handlebars'], [url, {heading: buildHeading(name, icon, value)}]);

  var pages = [
    buildPage('schedule', 'Schedule', 'i', buildFaIcon('calendar')),
    buildPage('history', 'History', 'i', buildFaIcon('calendar')),
  ];

  var routePages = R.map(routeGetGamelist(expressApp, CheckLogin, account, nconf));
  routePages(pages);

};
exports.RouteMatchLists = routeMatchLists;
