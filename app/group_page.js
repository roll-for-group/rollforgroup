// function for the purpose of managing groups
var Promise = require('bluebird');
var R = require('ramda');
var S = require('./lambda.js');

var account   = require('../app/models/account.js');
var accounts  = require('../app/models/accounts.js');

var group   = require('../app/models/group.js');
var groups  = require('../app/models/groups.js');

var express = require('express');
var bodyParser = require('body-parser')
var util = require('util');
var expressHelper = require('./expresshelper');

var Task = require('data.task');
var Maybe = require('data.maybe');
var Either = require('data.either');

var log = require('./log.js');
var exports = module.exports = {};


// renderData :: Response -> String -> Object -> Render 
var renderData = R.curry((res, template, data) => res.render(template, data));

// maybeBuildNavs :: o -> o
const maybeBuildNavs = (req) => S
  .path(['user', 'email'], req)
  .map(expressHelper.BuildNavbar(''))
  .map(R.objOf('navbarul'));


// prepareGroup :: o -> -
const prepareGroup = R.compose(
  R.objOf('group'),
  R.merge({css:[{csshref: '/css/group.css'}]}),
  R.pick(['name', 'url', 'description', 'members'])
);
exports.prepareGroup = prepareGroup;


// routeGroup :: Express -> Object -> Model -> Promise() 
var routeGroup = R.curry(function (expressApp, CheckLogin) {

  expressApp.get('/groups/:groupid', function(req, res) {

    // TODO :: Ensure non-logged in users can view this page

    // composeUser :: ({u} -> e) => express.render
    const composeUser = R.curry((groupdata, email) => R.composeP(
      renderData(res, 'group'), 
      R.merge({css:[{csshref: '/css/group.css'}]}),
      R.merge(prepareGroup(groupdata.toObject())),
      R.merge(maybeBuildNavs(req).getOrElse({})), 
      accounts.BuildNavbars(account)
    )(email));

    group
      .findOne({url: req.params.groupid})
      .exec()
      .then((grp) => {

        if (R.isNil(grp)) {
          res.sendStatus(404) 
        } else {
          composeUser(grp, S.path(['user', 'email'], req).getOrElse(''));
        }

      });


  });
});
exports.routeGroup = routeGroup;
