var log = require('../app/log.js');

var R = require('ramda');
var assoc   = R.assoc,
    compose = R.compose,
    equals  = R.equals,
    merge   = R.merge,
    prop    = R.prop,
    when    = R.when;

const Maybe     = require('data.maybe');
const Either    = require('data.either');
const Task      = require('data.task');

var request = require('ajax-request');
var Promise = require('bluebird');


// ============== AJAX ===============
const ajaxPromise = R.curry(function(options) {
  return new Promise(function (fulfill, reject) {
    try {
      request(options, function(err, res, body) {
        if (err) {
          reject(err)
        } else {
          var datify = compose(assoc('body', R.__, {}), JSON.parse);
          var isokcode = compose(equals(200), prop('statusCode'));
          var fulfillify = ()=>{}

          if (prop('statusCode', res) == 200) {
            fulfillify = compose(fulfill, when(isokcode, merge(datify(body))), assoc('statusCode', R.__, {}), prop('statusCode'));
          } else {
            fulfillify = compose(fulfill, assoc('statusCode', R.__, {}), prop('statusCode'));
          }
          fulfillify(res);
        }
      });
    } catch(err) {
      log.ConsoleLogObjectSection('ajaxPromise:Error', err);
    }
  });
}); 
exports.AjaxPromise = ajaxPromise;

// ajaxTask 
const ajaxTask = R.curry(function(options) {
  return new Task(function (reject, resolve) {
    ajaxPromise(options)
      .then((x) => resolve(x))
      .catch((y) => reject(y))

  });

}); 
exports.AjaxTask = ajaxTask;
