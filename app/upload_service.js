var uuid    = require('node-uuid');
var R       = require('ramda');
const compose = R.compose, 
      curry = R.curry,
      has   = R.has,
      lift  = R.lift,
      map   = R.map,
      path  = R.path,
      prop  = R.prop,
      split = R.split,
      tail  = R.tail;

var log     = require('./log.js');
var fileUpload = require('express-fileupload');
const fs    = require('fs');

var fspath = require('path');

const S3FS = require('s3fs');

var Task = require('data.task');
var Maybe = require('data.maybe');
var Either = require('data.either');

var accounts = require('../app/models/accounts.js');

const sendStatus = curry((res, status) => res.sendStatus(status)); 

const eitherPath = curry((array, a) => (path(array, a) == undefined ? Either.Left(400) : Either.Right(path(array, a)))); 

const eitherBodykey = eitherPath(['body', 'userkey']);
const eitherUserkey = eitherPath(['user', 'key']);
const eitherEmail   = eitherPath(['body', 'email']);
const eitherFiles   = eitherPath(['files']);

const eitherKey     = (request) => (eitherBodykey(request).isRight) ? eitherBodykey(request) : eitherUserkey(request) 

const routeUploads = function(account, nconf, expressApp) {

    //const extension = compose(tail, split('.'));
    const taskUpdateAvatar = (account, email, avataricon, avatartype) => new Task((reject, resolve) => account.findOneAndUpdate({'email': email}, {$set: {'profile.avataricon':avataricon, 'profile.avataricontype':avatartype}}, {new:true}).exec().then(resolve).catch(reject));

    const saveFile = curry((res, account, email, key, files) => {

        new Task((reject, resolve) => {
            accounts.CheckValidEmail(account, email, key).then(resolve).catch(reject); 

        }).fork(
            err => res.sendStatus(401),
            data => {
                accounts.UnlinkAvatar(nconf, account, email).then(function(x) {

                    const imagename = uuid.v1() + fspath.extname(files.avataricon.name);
                    if (nconf.get('upload:type') == 'file') {
                        files.avataricon.mv(nconf.get('upload:directory') + imagename, function(err) {
                            if (err)
                                return reject(500);

                            taskUpdateAvatar(account, email, nconf.get('upload:urlbase') + imagename, 'file').fork(
                                err =>  reject(401),
                                data => res.send('File uploaded!')
                            );
                        }); 

                    } else if (nconf.get('upload:type') == 's3') {

                        const bucketPath = nconf.get('upload:bucketPath');
                        const s3Options = nconf.get('upload:s3Options');
                        const fsImpl = new S3FS(bucketPath, s3Options);

                        files.avataricon.mv(nconf.get('upload:directory') + imagename, function(err) {
                            if (err)
                                return res.sendStatus(500);

                            var rd = fs.createReadStream(nconf.get('upload:directory') + imagename);
                            rd.on('error', function(err) {
                                res.sendStatus(500);
                            });

                            var wr = fsImpl.createWriteStream(imagename);
                            wr.on('error', function(err) {
                                res.sendStatus(500);
                            });

                            wr.on('finish', function(ex) {
                                taskUpdateAvatar(account, email, nconf.get('upload:urlbase') + imagename, 'file').fork(
                                    err =>  reject(401),
                                    data => {
                                        fs.unlink(nconf.get('upload:directory') + imagename);
                                        res.send('File uploaded!');
                                    }
                                );
                            });
                            rd.pipe(wr);

                        }); 
                    };  

                }).catch(log.clos('saveFile.err'));
            }
        )
    });

    expressApp.use(fileUpload());
    expressApp.post('/api/v1/upload', function(req, res) {
        const checkUser = lift(saveFile(res, account));
        checkUser(eitherEmail(req), eitherKey(req), eitherFiles(req)).leftMap(sendStatus(res));
    });
}
exports.RouteUploads = routeUploads
