// expresshelper.js
var favicon = require('serve-favicon');
var express = require('express');
var exphbs = require('express-handlebars');

// functions that help setting up static webpages
var R = require('ramda');
var S = require('./lambda.js');

var assoc = R.assoc,
    curry = R.curry,
    lift = R.lift, 
    map = R.map,
    pathEq = R.pathEq,
    objOf = R.objOf,
    zipObj = R.zipObj;

var path = require('path');
var log = require('./log.js');

var exports = module.exports = {};

// =============== STATIC ROUTES ================
//	pass an express object and array of files and folders to StaticRouteFolders()
//		these files and folders from the array will be accessed via the website url
//		and will be served from the ./public/* directory
//
//	StaticRouteFolders(express, ["myfile"])
//	i.e. http://myurl/myfile ---served_from---> __dirname/public/myfile
//
// ==============================================

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task');

// slashFile :: String -> String
var slashString = function(file) {
	return "/" + file;
}
exports.slashString = slashString();

// publicFolder :: String -> String -> String
var publicFolder = R.curry(function (localpath, file) {
	return path.join(localpath, "/public/", file);
});

// localFilePath :: String -> String
var localFilePath = publicFolder(__dirname);

// routeAppRequests :: Express -> String -> a
var routeUseRequests = R.curry (function (expressApp, asset) {

	expressApp.use(slashString(asset), express.static(localFilePath(asset)));
	return 0;  // success
});

// routeFolderes :: Express -> Array -> Boolean
exports.StaticRouteFolders = function(expressApp, foldersArray) {

  // routeUse :: String -> a
  var routeUse = routeUseRequests(expressApp);

  // maps a set of folders from an array so that they are accessible
  foldersArray.map(routeUse);

  return true;

}

// routeGetRequestExtended :: Express -> s -> s -> s -> s -> a
exports.routeGetRequestExtended = R.curry(function (expressApp, url, handlebar_template, title, description) {
	expressApp.get(url, function(req, res) {
		res.render(handlebar_template, {webtitle: title, metadescription: description})
	});
	return 0; // success
});

// routeGetRequestExtendedCSSMap :: Express -> s -> s -> s -> s -> s -> [a]
exports.routeGetRequestExtendedCSSMap = R.curry(function (expressApp, url, handlebar_template, title, description, cssHREFarr) {

  // cssHREFify :: a -> o
  const cssHREFify = (a) => R.objOf('csshref', a);

	expressApp.get(url, function(req, res) {
		res.render(handlebar_template, {
      webtitle: title, 
      metadescription: description, 
      css: R.map(cssHREFify, cssHREFarr)
    })
	});
	return 0; // success

});

// routeGetRequestExtendedCSS :: Express -> s -> s -> s -> s -> s -> a
exports.routeGetRequestExtendedCSS = R.curry(function (expressApp, url, handlebar_template, title, description, csshref) {
	expressApp.get(url, function(req, res) {
		res.render(handlebar_template, {
      webtitle: title, 
      metadescription: description, 
      css: [{csshref:csshref}]
    })
	});
	return 0; // success

});

// routeGetRequestExtendedCSSWithRedirect :: Express -> s -> s -> s -> s -> s -> a
exports.routeGetRequestExtendedCSSWithRedirect = R.curry(function (expressApp, url, handlebar_template, title, description, csshref) {
	expressApp.get(url, function(req, res) {

    S.path(['query', 'redirect'], req)
      .cata({
        Just: (url) => req.session.returnTo = url,
        Nothing: () => {}
      });

		res.render(handlebar_template, {
      webtitle: title, 
      metadescription: description, 
      css:[{csshref:csshref}]
    })

	});
	return 0; // success

});



// routeGetRequestExtendedCSSWithEmail :: Express -> s -> s -> s -> s -> s -> a
exports.routeGetRequestExtendedCSSWithEmail = R.curry(function (expressApp, url, handlebar_template, title, description, csshref) {
	expressApp.get(url, function(req, res) {
		res.render(handlebar_template, {
      webtitle: title, 
      metadescription: description, 
      css:[{csshref:csshref}], 
      email:req.query.email 
    })
	});
	return 0; // success

});




// routeGetRequests :: Express -> String -> String -> a
exports.routeGetRequests = R.curry(function (expressApp, url, handlebar_template) {
	expressApp.get(url, function(req, res) {
		res.render(handlebar_template)
	});
	return 0; // success

});

// addReqUserToData :: JSONObject -> ReqObject -> JSONObject
var addReqUserToData = R.curry(function(req, data) {

  var newdata = data;
  try {
    if (typeof (req.user) != "undefined") {
      if (typeof (req.user.facebook) === "undefined") {
        // build a non-facebook user
        data.user = {
          email: req.user.email,
          name: req.user.profile.name
        };
      } else {
        // build a facebook user
        data.user = {
          email: req.user.facebook.email,
          name: req.user.facebook.name
        };
      };
      data.user.bggname = req.user.profile.bggname;
    };

  } catch (err) {
      console.log(err);
  }

  return newdata;

});
exports.AddReqUserToData = addReqUserToData;


// maybeProfile :: s -> s -> s -> s -> o -> maybe s
const maybeProfile = R.curry((profileKey, facebookKey, googleKey, defVal, user) => Maybe
  .of(S.path(['profile', profileKey], user)
  .cata({
    Nothing:  () => S.whichever(
      S.path(['facebook', facebookKey], user), 
      S.path(['google', googleKey], user)
    ).getOrElse(defVal),
    Just:     (uploadPic) => uploadPic
  })
));
exports.maybeProfile = maybeProfile;


// lookupUsername :: Object -> Object
var lookupUsername = function(userObject) {

  // builduser :: maybe s -> maybe s -> maybe s -> maybe s -> maybe o 
  const builduser = R.lift((email, name, avatar, pageslug) => zipObj(['name', 'email', 'avataricon', 'pageslug'], [name, email, avatar, pageslug]));

  // safeAvatar :: o -> s
  const safeAvatar = maybeProfile('avataricon', 'photo', 'photo', '/img/defaultavatar.jpg');

  // safeName :: o -> s
  const safeName = maybeProfile('name', 'name', 'name', 'Unknown User');

  // build a non-facebook user
  return builduser(
    S.path(['email'], userObject), 
    safeName(userObject),
    safeAvatar(userObject), 
    S.path(['pageslug'], userObject)
  ).getOrElse({});

};
exports.LookupUsername = lookupUsername


// setupWebpages :: Express -> Express
var setupWebpages = function(expressApp, passport, store) {

	expressApp.engine('handlebars', exphbs({defaultLayout: 'main'}));
	expressApp.set('view engine', 'handlebars');
	expressApp.use(require('morgan')('combined'));
	expressApp.use(require('cookie-parser')('just rolling for a cookie'));
	expressApp.use(require('body-parser').urlencoded({ 
    extended: true }
  ));
	expressApp.use(require('express-session')({ 
    secret: 'rolling for group session key', 
    store: store,
    resave: true, 
    saveUninitialized: true 
  }));

	expressApp.use(passport.initialize());
	expressApp.use(passport.session());
	expressApp.use(favicon(path.join(__dirname, '..', 'app', 'public','img','favicon.ico')));

    return expressApp;
}
exports.SetupWebpages = setupWebpages


// buildNavbarHead :: (s -> u) => {o}
const buildNavbarHead = curry(function (headtext) {
  return {
    navhead: {
      dropdown: {
        text: headtext,
        menu: [{
          text: 'Local Players',
          href: 'events'
        }]
      }
    }
  }
});
exports.BuildNavbarHead = buildNavbarHead;


// buildNavbarEvent :: (e -> s -> Maybe(u)) => {o}
const buildNavbarEvent = curry(function (maybeEmail, event, link) {

  //const buildEmailLink = (email) => 'users?email=' + email;
  const buildEmailLink = (email) => { 
    return {a: {link: 'users?email=' + email, text: 'Career'}};
  }

  const highlightSelected = curry((link, obj) => 
    (pathEq(['a', 'link'], link, obj)) 
      ? assoc('activeclass', true, obj) 
      : obj
  );

  const links = [ 
    {a: {link: 'events?event=' + event, text: 'Events'}},
    {a: {link: 'hostgame?event=' + event, text: 'Host Event'}},
    {a: {link: 'eventslibrary?event=' + event, text: 'Game Library'}},
    {a: {link: 'booking?event=' + event, text: 'Bookings'}},
    {a: {link: 'schedule?event=' + event, text: 'Schedule'}},
  ]

  return objOf('li', map(highlightSelected(link), links));

});
exports.BuildNavbarEvent = buildNavbarEvent;


// buildNavbar :: s -> u => {o}
const buildNavbar = curry(function (link, email) {

  const buildEmailLink = (email) => '/users?email=' + email;

  // const highlightSelected = (pageslug) => 'users?id=' + pageslug;
  const highlightSelected = curry((link, obj) => {

    // hasSublink :: s -> {o} -> bool
    const hasSublink = curry((link, obj) => S.path(['a', 'sublinks'], obj)
      .map(R.contains(link))
      .getOrElse(false)
    );

    return (pathEq(['a', 'link'], link, obj)) 
      ? assoc('activeclass', true, obj) 
      : (hasSublink(link, obj)) 
        ? assoc('activeclass', true, obj)
        : obj;
  });

  const links = [ 
    // {a: {link: '/groups', text: 'Groups'}},
    {a: {link: '/events', text: 'Events', sublinks:['events', 'schedule', 'history', 'hostgame']}},
    {a: {link: '/players', text: 'Find Players', sublinks:['players', 'lounge', 'mail', 'mail?mailtype=outbox']}},
    {a: {link: '/games', text: 'Board Games'}},
    {a: {link: buildEmailLink(email), text: 'Achievements'}},
    {a: {link: 'https://blog.rollforgroup.com', text: 'Blog'}}
  ];

  return objOf('li', map(highlightSelected(link), links));

});
exports.BuildNavbar = buildNavbar;
