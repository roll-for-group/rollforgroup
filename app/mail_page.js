var account   = require('../app/models/account.js');
var accounts  = require('../app/models/accounts.js');

const Task    = require('data.task');
const Maybe   = require('data.maybe');
const Either  = require('data.either');

var Promise   = require('bluebird');
var R         = require('ramda');
var S         = require('./lambda.js');

var append    = R.append;
var chain     = R.chain;
var compose   = R.compose;
var composeP  = R.composeP;
var curry     = R.curry;
var dissoc    = R.dissoc;
var hasIn     = R.hasIn;
var head      = R.head;
var isNil     = R.isNil;
var lift      = R.lift;
var map       = R.map;
var merge     = R.merge;
var not       = R.not;
var path      = R.path;
var prop      = R.prop;

var util = require('util');
var log = require('./log.js');
var expressHelper = require('./expresshelper');

// renderData :: Render -> String -> Object -> Render 
const renderData = curry((res, template, data) => res.render(template, data));

// slashString :: String -> String
const slashString = (endpoint) => "/" + endpoint;

// daysBetween :: Date -> Date -> Integer
const daysBetween = curry(function( date1, date2 ) {
    //Get 1 day in milliseconds
    var one_day=1000*60*60*24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms/one_day); 
})
exports.DaysBetween = daysBetween;


// updateWarning :: Object -> Object
const updateWarning = function(userObj) {

    const bggwarning = [{
        bstrapclass: 'warning',
        script: {
            warning: true
        },
        panelrow: {
            faicon:     'fa-exclamation-triangle',
            text:       'Please add your Boardgame Geek username to import your collection.',
            button:     {
                id:     'addbgg',
                text:   'Add',
                faicon: 'fa-plus'
            },
            input:  {
                id:         'bggname',
                class:      'form-control text',
                placeholder:'your bgg account name'
            } 
        }
    }];

    const success = (bgguser) => [
        {
            bstrapclass: 'success',
            script: {
                success: true,
                bggname: bgguser
            },
            panelrow: {
                faicon:     'fa-check',
                text:       'BGG user (' + bgguser + ') boardgame collection is up to date.',
                button:     {
                    id:     'forcesync',
                    text:   '',
                    faicon: 'fa-refresh'
                } 
            }
        }
    ];

    // dayswords :: i => s
    const daysword = function(x) {
        return (x == 1) ? '1 day' : x + ' days'
    }

    const info = (bgguser, days) => [
        {
            bstrapclass: 'info',
            script: {
                info: true,
                bggname: bgguser
            },
            panelrow: {
                faicon:     'fa-calendar',
                text:       'BGG user (' + bgguser + ') boardgame collection was updated ' + daysword(days) + ' ago.',
                button:     {
                    id:     'sync',
                    text:   'Update',
                    faicon: 'fa-refresh'
                } 
            }
        },
    ]

    const pathExist     = (objpath, obj) => ((path(objpath, obj) == undefined) || (path(objpath, obj) == ''))
    const eitherBggName = (user) => (pathExist(['profile', 'bggname'], user)) ? Either.Left(bggwarning) : Either.Right(path(['profile', 'bggname'], user));
    const eitherBggTime = (user) => (pathExist(['bgg', 'lastupdated'], user)) ? Either.Left(bggwarning) : Either.Right(path(['bgg', 'lastupdated'], user));

    const dayssince = (firstdate) => daysBetween(new Date(firstdate), new Date());
    const eitherDays = compose(map(dayssince), eitherBggTime);

    const validate = lift((bggname, days) => (days > 30) ? info(bggname, days) : success(bggname));

    return {warnings: validate(eitherBggName(userObj), eitherDays(userObj)).merge()};

};
exports.UpdateWarning = updateWarning;

// routeGetCollection :: Express -> Object -> Model -> Schema -> Schema -> 0
var routeGetCollection = R.curry(function (expressApp, CheckLogin, accounts, account, url) {

  const urlFail = "login";
	const lookupAccount = R.composeP(R.head, accounts.ReadAccountFromEmail(account));

  expressApp.get(slashString(url), CheckLogin.ensureLoggedIn(slashString(urlFail)), function(req, res){
    lookupAccount(req.user.email).then(function(data) {

      const mergeSlug = (mergeObj) => compose(merge(mergeObj), R.objOf('pageslug'), prop('pageslug'))(data);

      const mergeData = compose(merge(updateWarning(data)), merge(accounts.BuildNavObject(data)));

      // maybebuildNavs :: {o} => {m} 
      const maybeBuildNavs = (req) => S.path(['user', 'email'], req).map(expressHelper.BuildNavbar('mail')).map(R.objOf('navbarul'));

      const render = compose(renderData(res, url), mergeSlug, merge(expressHelper.BuildNavbarHead('Profile')), merge(maybeBuildNavs(req).getOrElse({})));

      // note user activity
      S.path(['user', 'email'], req).chain(accounts.TaskRecordActivity(account));

      render(mergeData([]));

    }, 
      err => res.redirect(urlFail)
    );

	});
	return 0; // success
});
exports.RouteGetCollection = routeGetCollection;
