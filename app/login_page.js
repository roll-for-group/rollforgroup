var R = require('ramda');

var log = require('./log.js');
var expressHelper = require('./expresshelper');
var crypto = require('crypto');

var LocalStrategy = require('passport-local');

var accounts = require('./models/accounts.js');
var emailer = require('./emailer.js');

var S = require('./lambda.js');

var Maybe = require('data.maybe');
var Task = require('data.task');

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

// ============== URL ROUTES =================

// routeLogins :: Express -> Passport -> Redirect
var routeLogins = function(expressApp, passport) {

  // routeGetsExtCSS :: (s -> t -> l -> d) => render
  var routeGetsExtCSS = expressHelper.routeGetRequestExtendedCSS(expressApp);

  // routeGetsExtCSSWithRedirect :: (s -> t -> l -> d) => render
  var routeGetsExtCSSWithRedirect = expressHelper.routeGetRequestExtendedCSSWithRedirect(expressApp);

  // routeGetsExtWithEmail :: (s -> t -> l -> d) => render
  var routeGetsExtWithEmail = expressHelper.routeGetRequestExtendedCSSWithEmail(expressApp);


  routeGetsExtCSSWithRedirect('/login', 'login', 'Log in for Roll for Group board gamer meet up site.', 'Board gamers can log in to Roll for Group to play board games, find local board game events and find board game players.', '/css/login.css');

  routeGetsExtCSS('/signup', 'signup', 'Create an account for the Roll for Group board gamer meet up site.', 'Sign up for an account to find other local board gamers, join board game events, and play board games.', '/css/signup.css');

  routeGetsExtCSS('/forgot', 'forgot', 'Recover your account on the Roll for Group board gamer meet up site.', 'Reset your account password to join board game events, and play board games.', '/css/forgot.css');

  routeGetsExtWithEmail('/email-verification', 'email-verification', 'Verify your email for the Roll for Group board gamer meet up site.', 'Verify your password is valid to join board game events, and play board games.', '/css/verify.css');


  expressApp.get('/login/facebook', 
    passport.authenticate('facebook', { 
      scope: [ 'email' ] }
    )
  );

  expressApp.get('/login/facebook/return', 
    passport.authenticate('facebook', { 
      failureRedirect: '/login?failed=true' 
    }), function(req, res) {
      res.redirect('/');
    }
  );

    /*
  expressApp.post('/login/facebook/return', function(req, res, next) {
    passport.authenticate('facebook', function(err, user, info) {
      if (err) { return next(err); }
      if(!user) { return res.redirect('/login?failed=true'); }
      req.logIn(user, function(err) {
        if (err) { return next(err); }
        res.redirect(S.path(['body', 'redirect'], req)
          .map(prefixEvent)
          .getOrElse('/')
        );
      });
    })(req, res, next)
  });
    */

	expressApp.get('/auth/google',
    passport.authenticate('google', { scope: [
      'https://www.googleapis.com/auth/plus.login',
      'https://www.googleapis.com/auth/plus.profile.emails.read' 
    ]}
	));
	
	expressApp.get('/auth/google/callback', 
		passport.authenticate( 'google', { 
			successRedirect: '/',
			failureRedirect: '/login?failed=true'
    })
  );

    /*
  expressApp.post('/auth/google/callback', function(req, res, next) {
    passport.authenticate('google', function(err, user, info) {
      if (err) { return next(err); }
      if(!user) { return res.redirect('/login?failed=true'); }
      req.logIn(user, function(err) {
        if (err) { return next(err); }
        res.redirect(S.path(['body', 'redirect'], req)
          .map(prefixEvent)
          .getOrElse('/')
        );
      });
    })(req, res, next)
  });
    */

};
exports.RouteLogins = routeLogins;


// getReset :: Express 
var getReset = function(expressApp, account) {
  expressApp.get('/reset', function(req, res, next) {

    const invalidRequestData = {message: 'Invalid or expired reset request. Please try again.'};

    // verifyAuth :: s -> s -> Promise
    const verifyAuth = (email, auth) => {

      // because if they got the password email reset token, its as good as email verified
      account.findOneAndUpdate({'email': email, 'resetPasswordToken':auth, 'resetPasswordExpires': { $gt: Date.now()} }, {isAuthenticated: true}, {new:true}).then(x =>
      {
        (x == null) 
          ? res.redirect('/forgot?error=invalid') 
          : res.render('reset', {
              heading: 'Please enter your new password', 
              message:'Enter your new password in the box below to create a new password', 
              email: email, 
              resetPasswordToken: auth, 
              showPassword: true
            });
        
      }).catch(err => log.clos('err', err)); 
    }; 

    var liftVerify = R.lift(verifyAuth);

    if (S.path(['query', 'password'], req).isNothing) {
      liftVerify(S.path(['query', 'email'], req), S.path(['query', 'authToken'], req)).cata({
        Nothing: a => res.redirect('/forgot'),
        Just: b => b 
      });

    } else {
      res.render('reset', {});
    }

  });
};
exports.GetReset = getReset;


// postReset :: Express
var postReset = function(expressApp, account, transporter, emailFrom, siteUrl) {
  expressApp.post('/reset', function(req, res, next) {

    // taskUpdatePassword :: (Schema(account) -> e -> t -> p) => Task();
    const taskUpdatePassword = R.curry((account, email, token, newpassword) => new Task((reject, resolve) => {
      account.findOne({'email': email, 'resetPasswordToken':token, 'resetPasswordExpires': {$gt: Date.now()} }).then(x => {
        if (x.length == 0) { 
          reject('no user found');

        } else {
          x.setPassword(newpassword, function(err) {
            x.resetPasswordToken = undefined;
            x.resetPasswordExpires = undefined;
            x.save();

            var signinUrl = siteUrl + '/signin';
            emailer.SendSMTPmail(transporter, emailFrom, email, 'RFG password changed', 
              'Congratulations. Your Roll for Group password has been successfully changed\n\n' + 
              'Please login on the <a target=_blank href="' + signinUrl + '">Login</a> page\n\n' + 
              'Meet gamers and play more games!');
            resolve(email);
          }); 
        }

      }).catch(reject);
    }));

    const updatePassword = R.lift(taskUpdatePassword(account));

    updatePassword(S.path(['body', 'email'], req), S.path(['body', 'token'], req), S.path(['body', 'formpassword'], req)).cata({
      Nothing: err => res.redirect('/forgot?error=invalid'),
      Just: taskUpdate => {
        taskUpdate.fork(
          err => err => res.redirect('/forgot?error=invalid'), 
          ok => res.render('reset', {heading:'Your password has been updated', info: 'Please return to the log in page to login.', showsignin: true})
        )
      }
    });

  });
};
exports.PostReset = postReset;


// postLogin :: Express -> Passport -> Redirect
var postLogin = function(expressApp, passport) {

  // prefixEvent :: s -> s
  const prefixEvent = (eventid) =>
    '/events/' + eventid;

  expressApp.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
      if (err) { return next(err); }
      if(!user) { return res.redirect('/login?failed=true'); }
      req.logIn(user, function(err) {
        if (err) { return next(err); }
        res.redirect(S.path(['body', 'redirect'], req)
          .map(prefixEvent)
          .getOrElse('/')
        );
      });
    })(req, res, next)
  }); 

};
exports.PostLogin = postLogin;


// cryptotoken :: number => Task(string) 
const cryptotoken = (size) => new Task((reject, resolve) => {
  crypto.randomBytes(size, function(err, buff) {
    if (err) reject(err)
      resolve(buff.toString('hex'));
  });
}); 
exports.CryptoToken = cryptotoken;


// sendEmail :: (SMTPTransport, Maybe(email), key) => boolean
const sendEmailTransport = R.curry((account, siteUrl, maybeEmail, token) => new Task((reject, resolve) => {

  // authenticationURL :: s -> s -> s
  const authenticationURL = (to, token) => siteUrl + '/reset?email=' + to + '&authToken=' + token;

  if (maybeEmail.isNothing) {
    reject('no email');

  } else {
    account.findOneAndUpdate(
      {'email': maybeEmail.get()}, 
      {$set: {resetPasswordToken: token, resetPasswordExpires: Date.now() + 3600000}}
    ).exec().then(x => {
      (R.isNil(x)) 
        ? reject('Invalid user')
        : emailer.sendForgotPassword(authenticationURL(maybeEmail.get(), token), {
          email:  maybeEmail.get(),
          name:   S.path(['profile', 'name'], x).getOrElse('Gamer')
        }).fork(
          err => reject(err),
          data => {
            resolve(maybeEmail.get())
          }
        );

    }).catch(reject);
  };

}));
exports.SendEmailTransport = sendEmailTransport;


// postForgot :: Express -> Passport -> Redirect
var postForgot = function(expressApp, account, siteUrl) {
  expressApp.post('/forgot', function(req, res, next) {

    var email = S.path(['body', 'email'], req); 

    accounts.TaskAccountFromEmail(account, email.getOrElse('')).fork(
      err => {
        log.clos('err', err);
      },
      data => {

        // sendEmail
        const sendEmail = R.lift(sendEmailTransport(account, siteUrl, email));

        sendEmail(cryptotoken(60)).fork(
          err => res.redirect('/forgot?error=invalid'),
          emailtask => {
            emailtask.fork(
              err => {
                log.clos('err', err);
                res.redirect('/forgot?error=invalid')
              },
              emailsent => res.render('reset', {info: 'An email has been sent to ' + emailsent + ' with further instructions'})
            )
          }
        ); 

      }
    );

  });
};
exports.PostForgot = postForgot;


// routeVerify :: Express -> Passport -> Redirect
var routeVerify = function(expressApp, account) {

  // not sure why this goes to the failure, when it passes
  expressApp.get('/verify', function(req, res) {
    accounts.verifyAccount(account, req.query.authToken);
    res.redirect('login');
  });

};
exports.RouteVerify = routeVerify;


// setupPassportStrategies :: Passport -> Account -> Passport
var setupPassportStrategies = function(passport, account) {

  passport.serializeUser(function(user, cb) {
    cb(null, user);
  });

  passport.deserializeUser(function(obj, cb) {
    cb(null, obj);
  });

  // setup passport
  passport.use(account.createStrategy());

  // use static authenticate method of model in LocalStrategy
  passport.use(new LocalStrategy({
    usernameField: 'form-username',
    passwordField: 'form-password',
    session: true
  },
    account.authenticate()
  ));

  passport.serializeUser(account.serializeUser());
  passport.deserializeUser(account.deserializeUser());

  return passport;

};
exports.SetupPassportStrategies = setupPassportStrategies;
