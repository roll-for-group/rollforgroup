var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var Promise = require('bluebird');
var R = require('ramda');

var chain   = R.chain;
var map     = R.map;

var util = require('util');
var log = require('./log.js');
var expressHelper = require('./expresshelper');

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var S = require('./lambda.js');

const moment = require('moment');

const clr = ['#EBF4FC', '#C9E4EC', '#96CCD4', '#52ACB4', '#32686C'];

// findProps :: ({obj} -> [props]) => [values];
const findProps = R.curry((data, fields) => R.map(R.prop(R.__, data), fields));
exports.FindProps = findProps;

//  :: ({res} -> t -> {d}) => f(Render) 
const renderData = R.curry((res, template, data) => res.render(template, data));

// slashString :: String -> String
const slashString = (endpoint) => "/" + endpoint;

// maybeDefined :: a -> Maybe(a || Nothing)
const maybeDefined = (value) => ((value == undefined) || (value == {})) ? Maybe.Nothing() : Maybe.of(value);

// maybeSomething :: (Maybe(a) -> Maybe(b)) => Maybe(a || b)
const maybeSomething = R.curry((a, b) => (b.isNothing) ? a : b);

// buildEmailLink :: s => u
const buildEmailLink = (pageslug) => '/games?userid=' + pageslug;
exports.BuildEmailLink = buildEmailLink;

// getUserEmail :: Obj -> Maybe(String)
const getUserEmail = S.path(['user', 'email']);

const routeUser = function (expressApp, CheckLogin) {

  // maybebuildNavs :: {o} => {m} 
  const maybeBuildNavs = R.curry((req) => R.lift(expressHelper.BuildNavbar)(S.path(['user', 'pageslug'], req).map(buildEmailLink), S.path(['user', 'pageslug'], req)).map(R.objOf('navbarul')));

  expressApp.get(slashString('games'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res) {

    const render = R.curry((user, data) => R.compose(
      renderData(res, 'games'), 
      R.merge({collectionsURL: nconf.get('service:collections:weburl')}),
      R.merge({boardgamesURL: nconf.get('service:boardgames:weburl')}),
      R.merge(S.path(['query', 'gameid'], req).map(R.objOf('gameid')).getOrElse({})),
      R.merge(S.path(['user', 'profile', 'bggname'], req).map(R.objOf('bggname')).getOrElse({})),
      R.merge(expressHelper.BuildNavbarHead('Profile')), 
      R.merge(maybeBuildNavs(req).getOrElse({}))
    )(accounts.BuildNavObject(user)));

    // taskAccountFromId :: {r} => Task(Model(Account))
    const taskAccountFromId = (req) => (S.path(['query', 'id'], req).isJust) 
      ? maybeDefined(req.query.id).chain(accounts.TaskAccountFromSlug(account)) 
      : maybeSomething(getUserEmail(req), maybeDefined(req.query.email)).chain(accounts.TaskAccountFromEmail(account));

    taskAccountFromId(req).fork(
      err  => res.sendStatus(500),
      data => accounts
        .ReadAnAccountFromEmail(account, getUserEmail(req).getOrElse(''))
          .then((x) => {
            render(req.user, data)
          })
    )

  });
};
exports.RouteUser = routeUser;
