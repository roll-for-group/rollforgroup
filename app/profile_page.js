var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var Promise = require('bluebird');
var R = require('ramda');
var chain   = R.chain,
    compose = R.compose, 
    composeP = R.composeP,
    curry   = R.curry, 
    has     = R.has,
    map     = R.map,
    merge   = R.merge,
    path    = R.path,
    prop    = R.prop;

var log = require('./log.js');
var S = require('./lambda.js');

var expressHelper = require('./expresshelper');

const Maybe = require('data.maybe')

// maybeDefined :: a => Maybe(a) || Maybe(Nothing)
const maybeDefined = (value) => ((value == undefined) || (value == {})) 
  ? Maybe.Nothing() 
  : Maybe.of(value);

// notNil :: {} -> Boolean
const notNil = R.compose(R.not, R.isNil);

// renderData :: Render -> String -> Object -> Render 
const renderData = R.curry((res, template, data) => res.render(template, data));

// slashString :: String -> String
const slashString = (endpoint) => "/" + endpoint;

// unlinkObj :: ObjA => ObjB
const unlinkObj = compose(JSON.parse, JSON.stringify);

// whenExistsMergeId :: s -> s -> {o} -> {o}
const whenExistsMergeId = curry((type, key, obj) => {
  return S.path([type, key], obj)
    .map(R.objOf('id'))
    .map(R.objOf(type))
    .map(R.mergeDeepRight(obj))
    .getOrElse(obj);
});
exports.whenExistsMergeId = whenExistsMergeId;

// mergeTextInputValue :: s -> {o} -> s -> {o} 
const mergeTextInputValue = curry((type, obj, val) => {

  const hasDatapath = compose(has('datapath'), prop(type));
  const propDatapath = compose(prop('datapath'), prop(type));
  const hasIndex = compose(has('index'), prop(type));
  const propIndex = compose(prop('index'), prop(type));

  // updateId :: {o} -> {o}
  const updateId = (obj) => whenExistsMergeId(type, (hasDatapath(obj) ? 'name' : 'id'), obj);

  var newObj = obj;

  if (notNil(val)) {
    newObj = updateId(obj);
    newObj[type].value = (hasDatapath(obj)) 
      ? path(propDatapath(newObj), val) 
      : val; 
  };

  return newObj;

});
exports.MergeTextInputValue = mergeTextInputValue;

// updateCheckOption :: ({vals} -> {optObj}) => {mergeObj}
const updateCheckOption = curry((vals, option) => {
  var result = unlinkObj(option);

  if (has(option.id, vals))
    if (vals[option.id])
      result.checked = true;

  return result;
});
exports.UpdateCheckOption = updateCheckOption;

// mergeCheckboxValue :: ({obj} -> [{optObj}] -> {values}) => mergeObj
const mergeCheckboxValue = curry((data, options, vals) => {

  var result = unlinkObj(data);
  if (notNil(options)) 
    result.checkbox.options = map(updateCheckOption(vals), options);

  return result;

});
exports.MergeCheckboxValue = mergeCheckboxValue;

// updateSelectOption :: ({values} -> idname -> {optObj}) => mergeObj
const updateSelectOption = curry((vals, id, option) => {
  var result = unlinkObj(option);
  if (has(id, vals))
    if (vals[id] === option.value) 
      result.default = true;

  return result;
});
exports.UpdateSelectOption = updateSelectOption;

// mergeSelectValue :: (obj -> [optObj] -> {values}) => mergeObj
const mergeSelectValue = curry((data, options, vals) => {

  var result = unlinkObj(data);
  if (notNil(options)) 
    result.select.options = map(updateSelectOption(vals, data.select.id), options);

  return result;
});
exports.MergeSelectValue = mergeSelectValue;

// mergeSliderValue :: ({obj} -> {sliderkeys} -> {values}) => mergeObj 
const mergeSliderValue = curry((obj, sliderid, vals) => {

    var result = unlinkObj(obj);
    if (notNil(sliderid)) {
        result.slider.datastarthour   = vals[sliderid.values.starthour];
        result.slider.datastartminute = vals[sliderid.values.startminute];
        result.slider.dataendhour     = vals[sliderid.values.endhour];
        result.slider.dataendminute   = vals[sliderid.values.endminute];
    }
    return result;
});
exports.MergeSliderValue = mergeSliderValue;

// mergeImageValue :: (boolean -> {dataObj} -> Maybe({imageObj})) => mergeObj 
const mergeImageValue = curry((hasfacebook, data, vals) => {

    const mergeSrc = merge(R.__, {src: vals.map(prop('avataricon')).get()});
    const mergeFbook = curry((hasfacebook, obj) => merge((hasfacebook) ? {hasfacebook: true} : {}, obj));
    const buildUpload = compose(mergeSrc, mergeFbook(hasfacebook));

    if (has('imageupload', data)) {
      data.imageupload = buildUpload(data.imageupload);
    }
    return data;

});
exports.MergeImageValue = mergeImageValue;


var routeProfile = function (expressApp, CheckLogin) {

    const profile_page = {
      pageURL: "profile",
      sections: [{
        title: "What's Your Gamer Style?",
        image: "/img/1_2_34.png",
        label: "Your gamer style will help define what games you will be matched to play.",
        icon: "fa fa-photo",
        canvas: "/img/carcassonne.jpg",
        questions: [
          {
            textinput: {
              label: "User Profile",
              instructable: "Display Name",
              id: "name"
            }
          }, 
          {
            textarea: {
              instructable: "About Me",
              id: "biography",
              rows: 4
            }
          },
          {
            imageupload: {
              src: "img/default-avatar.jpg",
              buttontext: "Upload Avatar",
              instructable: "Avatar Icon",
              id: "avataricon"
            }
          }, {
            textinput: {
              instructable: "Profession",
              id: "job"
            }
          }, {
            select: {
              label: "Your profile visiblity determines who can see your details.",
              instructable: "Who can see your profile?",
              id: "visibility",
              options: [
                {option: "Public, allow people to find my profile", value: 1},
                {option: "Private, only people I know and people I'm matched with can see my profile", value: 2}
              ]
            }
          }, {
            checkbox: {
              instructable: "What inspires you most to play boardgames?",
              id: "inspiration",
              options: [
                {option: 'Conflict. Such as stealing resources, blocking others or directly attacking other players.', id: 'motivecompetition'},
                {option: 'Social Manipulation. Playing mind games, bluffing and deceit and persuasion.', id: 'motivesocialmanipulation'},
                {option: 'Strategy. Thinking, planning, and decision making.', id: 'motivestrategy'},
                {option: 'Discovering New Boardgames.', id: 'motivediscovery'},
                {option: 'Aesthetics. High quality components, artwork, illustrations and deep theme.', id: 'motivedesign'},
                {option: 'Social. Spending time with others.', id: 'motivecommunity'},
                {option: 'Cooperation. Working together and teaming up.', id: 'motivecooperation'},
                {option: 'Experiencing a Great Story.',id: 'motivestory'}
              ]
            }
          }, {
            select: {
              instructable: "Would you like to teach and explain games to new players?",
              id: "isteacher",
              options: [
                {option: "No, I don't want to teach games", value: 1},
                {option: "Yes, I like teaching games to new players", value: 2}
              ]
            }
          }, {
            textinput: {
              label: "Location",
              instructable: "What is your location. Players will be matched around your area.",
              id: "localarea",
              datapath: ["name"],
              name: "playcity"
            }
          },
          {
              hiddeninput: {
                  id: "playcitylat",
                  name: "playcitylat",
                  value: ""
              }
          },
          {
              hiddeninput: {
                  id: "playcitylng",
                  name: "playcitylng",
                  value: ""
              }
          },
            {
              textinput: {
                instructable: "How far are you willing to travel to play a game? Games are found as a crow flies.",
                suffixaddon: 'km',
                id: "searchradius"
              }
            },
            {
              textinput: {
                label: "Social links",
                instructable: "Facebook Name",
                id: "socialfacebook"
              }
            },
            {
              textinput: {
                instructable: "Twitter",
                id: "socialtwitter"
              }
            },
            {
              textinput: {
                instructable: "Pinterest",
                id: "socialpinterest"
              }
            },
            {
              textinput: {
                instructable: "Youtube Channel",
                id: "socialyoutube"
              }
            },
            {
              textinput: {
                instructable: "Twitch",
                id: "socialtwitch"
              }
            },
            {
              textinput: {
                instructable: "Discord",
                id: "socialdiscord"
              }
            },
            {
              textinput: {
                instructable: "Website",
                id: "socialwebsite"
              }
            },
            {
              textinput: {
                instructable: "Google Plus",
                id: "socialgoogleplus"
              }
            },
            {
              checkbox: {
                  label: "Contact Methods",
                  instructable: "Do you want to hide your email address from others?",
                  id: "commemail",
                  options: [
                      {option: 'Hide my email address',  id: 'commemailvisible'},
                  ],
              }
            },
            {
              textinput: {
                instructable: "Steam",
                id: "commsteam"
              }
            },
            {
              textinput: {
                instructable: "Phone Number",
                id: "commphoneno"
              }
            },
            {
              textinput: {
                instructable: "Skype",
                id: "commskype"
              }
            },
            {
              textinput: {
                instructable: "Reddit Name",
                id: "commreddit"
              }
            },
            {
              textinput: {
                instructable: "Battle.net Account",
                id: "commbattlenet"
              }
            },
            {
              textinput: {
                instructable: "XBox Live Gamertag",
                id: "commxbox"
              }
            },
            {
              textinput: {
                instructable: "Playstation Live Gamertag",
                id: "commpsn"
              }
            },
            {
              checkbox: {
                label: "Scheduling Assistant",
                instructable: "What days of the week are you available?",
                id: "availability",
                  options: [
                    {option: 'Monday',      id: 'Monday'},
                    {option: 'Tuesday',     id: 'Tuesday'},
                    {option: 'Wednesday',   id: 'Wednesday'},
                    {option: 'Thursday',    id: 'Thursday'},
                    {option: 'Friday',      id: 'Friday'},
                    {option: 'Saturday',    id: 'Saturday'},
                    {option: 'Sunday',      id: 'Sunday'},
                  ]
              }
            },
            {
              slider: {
                instructable: "Monday Availability",
                id: {
                  id: "mondayslider", 
                  values: {
                    starthour:  "mondaystarthour",
                    startminute: "mondaystartminute",
                    endhour:    "mondayendhour",
                    endminute:  "mondayendminute"
                  }
                }
              }
            },
            {
              slider: {
                instructable: "Tuesday Availability",
                id: {
                  id: "tuesdayslider", 
                  values: {
                    starthour:  "tuesdaystarthour",
                    startminute: "tuesdaystartminute",
                    endhour:    "tuesdayendhour",
                    endminute:  "tuesdayendminute"
                  }
                }
                /*
                },
                datastarthour: 18,
                datastartminute: 30,
                dataendhour: 22,
                dataendminute: 00
                */
              }
            },
            {
              slider: {
                instructable: "Wednesday Availability",
                id: {
                  id: "wednesdayslider", 
                  values: {
                    starthour:  "wednesdaystarthour",
                    startminute: "wednesdaystartminute",
                    endhour:    "wednesdayendhour",
                    endminute:  "wednesdayendminute"
                  }
                },
              }
            },
            {
              slider: {
                instructable: "Thursday Availability",
                id: {
                  id: "thursdayslider", 
                  values: {
                    starthour:  "thursdaystarthour",
                    startminute: "thursdaystartminute",
                    endhour:    "thursdayendhour",
                    endminute:  "thursdayendminute"
                  }
                },
              }
            },
            {
              slider: {
                instructable: "Friday Availability",
                id: {
                  id: "fridayslider", 
                  values: {
                    starthour:  "fridaystarthour",
                    startminute: "fridaystartminute",
                    endhour:    "fridayendhour",
                    endminute:  "fridayendminute"
                  }
                },
              }
            },
            {
              slider: {
                instructable: "Saturday Availability",
                id: {
                  id: "saturdayslider", 
                  values: {
                    starthour:  "saturdaystarthour",
                    startminute: "saturdaystartminute",
                    endhour:    "saturdayendhour",
                    endminute:  "saturdayendminute"
                  }
                }
              }
            },
            {
              slider: {
                instructable: "Sunday Availability",
                id: {
                  id: "sundayslider", 
                  values: {
                    starthour:  "sundaystarthour",
                    startminute: "sundaystartminute",
                    endhour:    "sundayendhour",
                    endminute:  "sundayendminute"
                  }
                }
              }
            },
            {
              textinput: {
                label: "Collection Management",
                instructable: "Boardgame Geek Username",
                id: "bggname"
              }
            },
            {
              checkbox: {
                label: "Email Notifications",
                instructable: "Update me by email",
                id: "notifyupdates",
                options: [
                  {option: 'Site Notifications', id: 'notifyevents'},
                ]
              }
            }
          ]
        }
      ]
    };

    // updateText :: (string -> defObj -> dataObj) => mergeObj 
    const updateText = R.curry((type, defaults, data) => Maybe.of(data).map(path([type, 'id'])).map(prop).ap(defaults).map(mergeTextInputValue(type, data)).get());

    // updateTextArea :: (string -> defObj -> dataObj) => mergeObj 
    const updateTextArea = R.curry((type, defaults, data) => Maybe.of(data).map(path([type, 'id'])).map(prop).ap(defaults).map(mergeTextInputValue(type, data)).get());

    // updateCheckbox :: (defObj -> dataObj) => mergeObj 
    const updateCheckbox = R.curry((defaults, data) => Maybe.of(data).map(path(['checkbox', 'options'])).map(mergeCheckboxValue(data)).ap(defaults).get());

    // updateSelect :: (defObj -> dataObj) => mergeObj 
    const updateSelect = R.curry((defaults, data) => Maybe.of(data).map(path(['select', 'options'])).map(mergeSelectValue(data)).ap(defaults).get());

    // updateSlider :: (defObj -> dataObj) => mergeObj 
    const updateSlider = R.curry((defaults, data) => Maybe.of(data).map(path(['slider', 'id'])).map(mergeSliderValue(data)).ap(defaults).get());

    // updateImage :: (boolean -> defObj -> dataObj) => mergeObj 
    const updateImage = curry((hasFacebook, defaults, data) => mergeImageValue(hasFacebook, data, defaults));

    const readAccount = accounts.ReadAccountFromEmail(account);

    // userProfile :: email => Maybe(jsonObj) 
    const userProfile = composeP(
      map(unlinkObj), 
      chain(S.prop('profile')), 
      S.head, 
      readAccount
    );

    expressApp.get(slashString('profile'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res) {

      // note user activity
      S.path(['user', 'email'], req).chain(accounts.TaskRecordActivity(account));

      const resolveProfile = R.curry((data, x) => {
        x.then(function(interior) {

          const transformData = compose(
            updateImage(has('facebook', req.user), interior), 
            updateText('textinput', interior), 
            updateTextArea('textarea', interior), 
            updateCheckbox(interior), 
            updateSelect(interior), 
            updateSlider(interior)
          );

          var questionsdata = unlinkObj(data);
          questionsdata.sections[0].questions = map(transformData, data.sections[0].questions);
          
          // maybebuildNavs :: {o} => {m} 
          const maybeBuildNavs = (req) => S.path(['user', 'email'], req).map(expressHelper.BuildNavbar('profile')).map(R.objOf('navbarul'));

          const render = composeP(
            renderData(res, 'profile'), 
            merge(expressHelper.BuildNavbarHead('Profile')), 
            merge(maybeBuildNavs(req).getOrElse({})), 
            merge(questionsdata), 
            accounts.BuildNavbars(account)
          );
          render(req.user.email);

        }).catch(function(err) {
          res.status(401);
        });
      });

      maybeDefined(req.user.email).map(userProfile).map(resolveProfile(profile_page));
        
    });

};
exports.RouteProfile = routeProfile;
