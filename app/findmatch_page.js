var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var gameevent = require('../app/models/gameevent.js');
var gameevents = require('../app/models/gameevents.js');

var playersPage = require('../app/players_page.js');

var Promise = require('bluebird');
var R = require('ramda');
var assoc   = R.assoc,
    chain   = R.chain,
    compose = R.compose,
    countBy = R.countBy,
    curry   = R.curry,
    find    = R.find,
    filter  = R.filter,
    flatten = R.flatten,
    map     = R.map,
    merge   = R.merge,
    path    = R.path,
    pick    = R.pick,
    prop    = R.prop,
    propEq  = R.propEq,
    reverse = R.reverse,
    slice   = R.slice,
    sortBy  = R.sortBy,
    toPairs = R.toPairs,
    zipObj  = R.zipObj;

var log = require('./log.js');
var expressHelper = require('./expresshelper');

var S = require('./lambda.js');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

const clr = ['#EBF4FC', '#C9E4EC', '#96CCD4', '#52ACB4', '#32686C'];


// findProps :: ({obj} -> [props]) => [values];
const findProps = curry((data, fields) => map(prop(R.__, data), fields));
exports.FindProps = findProps;

// isPremiumUser :: o -> bool
const isPremiumUser = (req) => (
	S.pathEq(['user', 'premium', 'isstaff'], true, req).isJust ||
	S.pathEq(['user', 'premium', 'isactive'], true, req).isJust);
exports.isPremiumUser = isPremiumUser;

// slashString :: String -> String
var slashString = (endpoint) => "/" + endpoint;

// renderData :: Render -> String -> Object -> Render 
var renderData = curry((res, template, data) => res.render(template, data));

// matchlist :: (n, e) => {match} 
const matchlist = (distance, email) => R.zipObj(['events'], [{location: {distance: distance, nearby: email}}]);
exports.Matchlist = matchlist;

// buildOnLoad :: a => {o}
const buildOnLoad = R.compose(R.objOf('onload'), R.objOf('ajaxdata'));
exports.BuildOnLoad = buildOnLoad;

// buildFaIcon :: i => {o} 
const buildFaIcon = (faicon) => R.objOf('class', 'fa fa-' + faicon + ' fa-fw');
exports.BuildFaIcon = buildFaIcon;

// buildImage :: (c -> s) => {o}
const buildImage = (classes, source) => zipObj(['class', 'src'], [classes, source]);
exports.BuildImage = buildImage;

// buildHeading :: (n -> i -> v) => {o}
const buildHeading = (name, icon, faarea, area, value) => zipObj(['name', icon, 'h4'], [name, value, {faicon: faarea, text: area}]); 
exports.BuildHeading = buildHeading;

// buildPage = (u -> n -> i -> v) => {o}
const buildPage = curry((url, name, icon, faarea, area, value) => zipObj(['url', 'handlebars'], [url, {heading: buildHeading(name, icon, faarea, area, value)}]));
exports.BuildPage = buildPage;

// maybeDistance :: {u} => a 
const maybeDistance = (req) => S.path(['user', 'profile', 'searchradius'], req).getOrElse(50) * 1000;
exports.MaybeDistance = maybeDistance;

// createDisplayError :: string => {w}
const createDisplayError = (err) => ({warning: err});
exports.CreateDisplayError = createDisplayError;

// checkErrorMsg :: {r} => {w} || {}
const checkErrorMsg = (req) => S.path(['query', 'err'], req).map(createDisplayError).getOrElse({});
exports.CheckErrorMsg = checkErrorMsg;

// whenInDistance :: {u} => true || false
const whenInDistance = (user) => ((S.path(['profile', 'searchradius'], user).getOrElse(50) * 1000) > prop('distance', user)); 
exports.WhenInDistance = whenInDistance;

// lookupItem :: ([{x}] -> [a, b]) => {y} 
const lookupItem = curry((x, a) => assoc('count', a[1], find(propEq('id', Number(a[0])))(x)));
exports.LookupItem = lookupItem;

// makecount :: {o} => (f({o}) => {b}) 
const makecount = (item) => compose(map(lookupItem(item)), toPairs, countBy(prop('id')));
exports.Makecount = makecount;


// filterGamesOwned :: (s -> {u}) => [g]
const filterGamesOwned = curry((email, users) => {
    const findgames = compose(map(map(prop('id'))), map(filter(path(['matching', 'own']))), chain(S.prop('games')), S.head, filter(propEq('email', email))); 
    return(findgames(users).getOrElse([]));
});
exports.FilterGamesOwned = filterGamesOwned;

// checkOwnership :: ([n] -> {l}) => {l2} 
const checkOwnership = curry((ownlist, listitem) => (R.contains(listitem.id, ownlist)) ? assoc('own', true, listitem) : listitem)
exports.CheckOwnership = checkOwnership;

// buildGameList :: [{u1}..{u2}] => {m}
const buildGameList = curry(function(lng, lat, email, users) {

	// addcount :: {o} => {p}
	const addcount = (x) => makecount(x)(x);

    // addownership :: ([n] -> [{w}]) => [{o2}] 
    const addownership = curry((ownlist, wtplist) => map(checkOwnership(ownlist), wtplist));

    // gamelist
    const gamelist = compose(addownership(filterGamesOwned(email, users)), slice(0, 30), reverse, sortBy(prop('count')), addcount, map(pick(['id', 'name', 'yearpublished', 'thumbnail'])), filter(path(['matching', 'wanttoplay'])), flatten, map(prop('games')), filter(whenInDistance), map(playersPage.CalculatePlayerDistance(lat, lng)), map(pick(['email', 'profile', 'games']))); 

    return R.objOf('games', gamelist(users));

});
exports.BuildGameList = buildGameList;


// buildURL :: s -> s
const buildURL = (event) => 'events?event=' + event;

// buildCalendarUrl :: Maybe(e) -> Maybe(c) => f({u} -> {o})
const buildCalendarUrl = R.lift((nconf, email, icalkey) => nconf.get('site:url')+ '/api/v1/accounts/' + email + '/calendars/' + icalkey + '/rollforgroup.ics');

// mergeIcalkey :: {r} => f({u} -> {o})
const mergeIcallink = R.curry((nconf, req) => merge(buildCalendarUrl(Maybe.of(nconf), S.path(['user', 'email'], req), S.path(['user', 'icalkey'], req)).map(R.objOf('icalurl')).getOrElse({})))


// routeGamelist :: (Express -> Object -> Model) => Promise() 
// var routeGamelist = R.curry(function (expressApp, CheckLogin, account, nconf) {
var routeGamelist = R.curry(function (expressApp, account, nconf) {
  return new Promise(function(fulfill, reject) {
    expressApp.get(slashString('events'), function(req, res) {

      // findArea :: {o} => s
      const findArea = (req) => S.path(['user', 'profile', 'localarea', 'name'], req).getOrElse('Location Not Set');

      const routePages = R.compose(
        buildPage('events', 'Find Event', 'img', 'map-marker', findArea(req)), 
        buildImage
      );

      const page = routePages('brandlogo32image wow rollIn', 'img/rfg-logo-whiteglow-32.png')

      // maybebuildNavs :: {o} => {m} 
      const maybeBuildNavs = (req) => R.lift(expressHelper.BuildNavbar)((Maybe.of('events')), S.path(['user', 'email'], req));

      const maybeBuildNavsEvent = (req) => R.lift(expressHelper.BuildNavbarEvent(S.path(['user', 'email'], req)))(S.path(['query', 'event'], req), S.path(['query', 'event'], req).map(buildURL));

      // getTemplate :: n => s
      const getTemplate = (quad) => 'events';

      // safeFindTemplate :: {o} => Maybe.of(p) 
      const safeFindTemplate = R.compose(
        R.map(getTemplate), 
        R.map(accounts.GetTestQuadrant), 
        S.path(['user', 'key'])
      );

      // composeUserData :: ({u} -> e -> n) => express.render
      const composeUserData = R.curry((email) => R.composeP(
        renderData(res, safeFindTemplate(req).getOrElse('events')), 
        mergeIcallink(nconf, req), 
        R.assoc('ispremium', isPremiumUser(req)),
        R.merge(expressHelper.BuildNavbarHead(findArea(req))), 
        R.merge(maybeBuildNavs(req).map(R.objOf('navbarul')).getOrElse({})), 
        R.merge(buildOnLoad(prop(page.url, matchlist(maybeDistance(req), email)))),
        R.merge(checkErrorMsg(req)), 
        R.merge(page.handlebars), 
        accounts.BuildNavbars(account)
      )(email));

      // composeNonUserData :: ({u} -> e -> n) => express.render
      const composeNonUserData = R.curry((data) => R.compose(
        renderData(res, safeFindTemplate(req).getOrElse('events')), 
        R.merge(maybeBuildNavsEvent(req).map(R.objOf('navbarul')).getOrElse({})), 
        R.merge(checkErrorMsg(req)), 
        R.merge(page.handlebars)
      )(data));

      // composeUserErr :: ({u} -> e) => express.render
      /*
      const composeUserErr = R.curry((email) => R.composeP(
        renderData(res, safeFindTemplate(req).getOrElse('events')), 
        R.merge(maybeBuildNavsEvent(req).map(R.objOf('navbarul')).getOrElse({})), 
        R.merge(checkErrorMsg(req)), 
        R.merge(page.handlebars)
      )(email));
      */

      // no user
      var nouserdata = {navigation: {nouser: true}};

      // taskCheck :: o -> s -> s -> Task o 
      const liftCheckValidEmail = R.lift(accounts.CheckValidEmail(account));

      // log activity, but only if the user is logged in
      liftCheckValidEmail(S.path(['user', 'email'], req), S.path(['user', 'key'], req)).cata({
        Nothing:  ()=> composeNonUserData(nouserdata),
        Just:     (p)=>{
          p.then((user)=>{ 
            accounts.TaskRecordActivity(account, user.profile.email).fork()
            if (findArea(req) === 'Location Not Set') {
              composeUserData(user.profile.email);
            } else {
              composeUserData(user.profile.email);
              // composeUserErr(user.profile.email);
            }
          });
        }
      });

    });
  });
});
exports.RouteGamelist = routeGamelist;
