var accounts = require('../app/models/accounts.js');
var account = require('../app/models/account.js');

var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var userpanels = require('../app/users_page.js');

var Promise = require('bluebird');
var R = require('ramda');
const assoc   = R.assoc;
const chain   = R.chain;
const compose = R.compose;
const composeP = R.composeP;
const curry   = R.curry;
const gt      = R.gt;
const head    = R.head;
const nth     = R.nth;
const map     = R.map;
const mapAccum = R.mapAccum;
const merge   = R.merge;
const path    = R.path;
const prop    = R.prop;
const zipObj  = R.zipObj;

const S       = require('../app/lambda.js');

var log = require('./log.js');
var expressHelper = require('./expresshelper');

var geolib = require('geolib');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

const clr = ['#EBF4FC', '#C9E4EC', '#96CCD4', '#52ACB4', '#32686C'];

const fullmotivelist = [ 
  'motivecompetition', 
  'motivecommunity', 
  'motivestrategy', 
  'motivestory', 
  'motivedesign', 
  'motivediscovery', 
  'motivecooperation', 
  'motivesocialmanipulation' 
];


// slashString :: String -> String
var slashString = (endpoint) => "/" + endpoint;

// renderData :: Render -> String -> Object -> Render 
var renderData = curry((res, template, data) => res.render(template, data));


// calculatePlayerDistance :: Number -> Number -> Model(user) -> Model(user)
var calculatePlayerDistance = curry(function(latitude, longitude, player) {

  // pathCoord :: {o} => [lng, lat]
  const pathCoord = path(['profile', 'localarea', 'loc', 'coordinates']);

  // tolattitude :: {o} => lat 
  const tolatitude = compose(nth(1), pathCoord);

  // tolongitutde :: {o} => lng 
  const tolongitude = compose(nth(0), pathCoord);

  var distance = geolib.getDistanceSimple(
    {latitude: Number(latitude), longitude: Number(longitude)},
    {latitude: tolatitude(player), longitude: tolongitude(player)}
  );

  var prepMongo = compose(
    assoc('distance', distance)
  );

  return prepMongo(player);

});
exports.CalculatePlayerDistance = calculatePlayerDistance;


// whenInDistance :: {u} => true || false
const whenInDistance = (user) => ((S.path(['profile', 'searchradius'], user).getOrElse(50) * 1000) > prop('distance', user)); 
exports.WhenInDistance = whenInDistance;

// whenNotPrivate :: {u} => true || false
const whenNotPrivate = (user) => (path(['profile', 'visibility'], user) != 2);
exports.WhenNotPrivate = whenNotPrivate;

// readMotive :: ({u} -> s) => m
const readMotive = curry((user, motive) => path(['profile', motive], user) ? motive : '');
exports.ReadMotive = readMotive;

// calcMotive :: (d -> {u} -> s) => m
const calcMotive = curry((motiveDNA, user, motive) => path(['profile', motive], user) == prop(motive, motiveDNA) ? 12.5 : 0);
exports.CalcMotive = calcMotive;


// buildPlayerList :: (lng -> lat -> [{u1}..{u2}, [{g}]] -> {d}) => {m}
const buildPlayerList = curry(function(lng, lat, users, motiveDNA) {
  return {
    playerlist:[]
  }
});
exports.BuildPlayerList = buildPlayerList;

// buildareaData :: (Schema(account) -> e -> d -> {loc}) => Task({profiles});
const buildAreaData = curry(function(account, email, distance, loc) {
  return new Task((reject, resolve) => {
    resolve({playerlist:[]});
  });
});


// buildFaIcon :: i => {o} 
const buildFaIcon = (faicon) => R.objOf('class', 'fa fa-' + faicon + ' fa-fw');
exports.BuildFaIcon = buildFaIcon;

// buildImage :: (c -> s) => {o}
const buildImage = (classes, source) => zipObj(['class', 'src'], [classes, source]);
exports.BuildImage = buildImage;

// buildHeading :: (n -> i -> v) => {o}
const buildHeading = (name, icon, faarea, area, value) => zipObj(['name', icon, 'h4'], [name, value, {faicon: faarea, text: area}]); 
exports.BuildHeading = buildHeading;

// buildPage = (u -> n -> i -> a -> v) => {o}
const buildPage = curry((url, name, icon, faarea, area, value) => zipObj(['url', 'handlebars'], [url, {heading: buildHeading(name, icon, faarea, area, value)}]));
exports.BuildPage = buildPage;

// maybeDistance :: {u} => a 
const maybeDistance = (req) => S.path(['user', 'profile', 'searchradius'], req).getOrElse(50) * 1000;
exports.MaybeDistance = maybeDistance;

// assocDistance :: ({o} -> n) => {p} 
const assocDistance = curry((req, obj) => assoc('distance', maybeDistance(req), obj)); 
exports.assocDistance = assocDistance;

// checkVisibility :: ({o} -> n) => {p} 
const checkVisibility = curry((obj, y) => (y == 2) 
  ? assoc('novisibility', true, obj) 
  : obj
);
exports.CheckVisibility = checkVisibility;

// showwarning :: ({r} -> {o}) => {p}
const showWarning = curry((req, obj) => S.path(['user', 'profile', 'visibility'], req).map(checkVisibility(obj)).getOrElse(obj));
exports.ShowWarning = showWarning;

// untilUpdatedAssocMotives :: {r} => f()
const untilUpdatedAssocMotives = (req) => {

  const motives = ['motivediscovery', 'motivecommunity', 'motivestory', 'motivecooperation', 'motivecompetition', 'motivesocialmanipulation', 'motivestrategy', 'motivedesign'];

  // whenTrueOne :: b => n
  const whenTrueOne = (a) => a 
    ? 1 
    : 0

  // appender (a -> b) => [c, c]
  var appender = (a, b) => [a + b, a + b];

  // hasMotive :: [{o}} => {p}
  const hasMotive = compose(gt(R.__, 0), head, mapAccum(appender, 0), map(whenTrueOne), R.props(motives));

  const sections = [{
    questions: [{
      checkbox: {
        id: "inspiration",
        options: [
          {option: 'Discovering New Boardgames.',id: 'motivediscovery'},
          {option: 'Social. Spending time with others.', id: 'motivecommunity'},
          {option: 'Experiencing a Great Story.', id: 'motivestory'},
          {option: 'Cooperation. Working together and teaming up.', id: 'motivecooperation'},
          {option: 'Conflict. Such as stealing resources, blocking others or directly attacking other players.', id: 'motivecompetition'},
          {option: 'Social Manipulation. Playing mind games, bluffing and deceit and persuasion.', id: 'motivesocialmanipulation'},
          {option: 'Strategy. Thinking, planning, and decision making.', id: 'motivestrategy'},
          {option: 'Aesthetics. High quality components, artwork, illustrations and deep theme.', id: 'motivedesign'},
        ]
      }
    }]
  }];

  return S.path(['user', 'profile'], req).map(hasMotive).getOrElse(true) ? merge({}) : assoc('sections', sections); 

}


// mergePage :: {o} => f(s) => {q}
const mergePage = (req) => compose(merge, prop('handlebars'), buildPage('players', 'Nearby Players', 'i', 'users', S.path(['user', 'profile', 'localarea', 'name'], req).getOrElse('Location Not Set')), buildFaIcon);
exports.MergePage = mergePage;


// routePlayerList :: (Express -> Object -> Model) => Promise() 
var routePlayerList = curry(function (expressApp, CheckLogin, account) {
  return new Promise(function(fulfill, reject) {

    expressApp.get(slashString('players'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res){

      // maybebuildNavs :: {o} => {m} 
      const maybeBuildNavs = (req) => S.path(['user', 'email'], req)
        .map(expressHelper.BuildNavbar('players'))
        .map(R.objOf('navbarul'));

      // composeUserData :: ({u}) => express.render
      const composeUserData = curry((data) => composeP(
        renderData(res, 'players'), 
        R.merge({css:[{csshref:'/css/players.css'}]}),
        R.merge(S.path(['user', 'profile', 'localarea', 'name'], req).map(expressHelper.BuildNavbarHead).getOrElse('Location Not Set')), 
        R.merge(maybeBuildNavs(req).getOrElse({})), 
        untilUpdatedAssocMotives(req), 
        //prepareData(data), 
        assocDistance(req), 
        showWarning(req), 
        mergePage(req)('users'), 
        accounts.BuildNavbars(account)
      ));

      // composeUserErr :: ({u} -> e) => express.render
      const composeUserErr = composeP(
        renderData(res, 'players'), 
        assoc('setlocation', true), 
        untilUpdatedAssocMotives(req), 
        mergePage(req)('users'), 
        accounts.BuildNavbars(account)
      );

      // taskBuild :: {u} => express.render
      const taskBuild = compose(
        chain(buildAreaData(account, S.path(['user', 'email'], req).get(), maybeDistance(req))), 
        S.path(['user', 'profile', 'localarea', 'loc', 'coordinates'])
      );

      // note user activity
      S.path(['user', 'email'], req).chain(accounts.TaskRecordActivity(account));

      R.is(Task, taskBuild(req)) 
        ? taskBuild(req).fork(
          (err)       => S.path(['user', 'email'], req).map(composeUserErr),
          (usersdata) => S.path(['user', 'email'], req).map(composeUserData(usersdata))
        ) 
        : S.path(['user', 'email'], req).map(composeUserErr);

    });
  });
});
exports.RoutePlayerList = routePlayerList;
