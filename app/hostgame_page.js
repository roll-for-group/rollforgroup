var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var Promise = require('bluebird'); var R = require('ramda');
var append  = R.append,
    assocPath = R.assocPath,
    compose = R.compose,
    chain   = R.chain,
    curry = R.curry,
    isEmpty = R.isEmpty,
    head    = R.head,
    map     = R.map,
    merge   = R.merge,
    reject  = R.reject,
    tail    = R.tail,
    zipObj  = R.zipObj;

var log = require('./log.js');
var expressHelper = require('./expresshelper');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var S           = require('./lambda.js');

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

// renderData :: Render -> String -> Object -> Render 
var renderData = R.curry((res, template, data) => res.render(template, data));

// buildHandlebarTextImageUIObject :: String -> String -> String -> String -> String -> String -> String -> [String] -> JSONObject
var buildHandlebarTextImageUIObject = R.curry(function(fieldsetid, id, name, instructable, label, placeholder, prefixaddon) {

  var hiddenkeys = ['name', 'id'];
  var keys = ['id', 'name', 'instructable', 'label', 'placeholder', 'prefixaddon', 'hiddeninput', 'image'];
  var values = [id, name, instructable, label, placeholder, prefixaddon, zipObj(hiddenkeys, [name + 'inv', id + 'id']), R.objOf('id', id + 'img')];

  var baseObject = {
    fieldsetid: fieldsetid
  }

  var subObj = R.compose(assocPath(['textinputimage'], R.__, baseObject), reject(isEmpty), zipObj(keys)); 
  subObj(values);

  return subObj(values); 

});
exports.BuildHandlebarTextImageUIObject = buildHandlebarTextImageUIObject;


// buildHiddenUIObject :: (String -> String -> String -> String -> String) => JSONObject
var buildHiddenUIObject = R.curry(function(fieldsetid, inputtype, id, name, value ) {

  var keys = ['id', 'name', 'value'];
  var values = [id, name, value];

  var baseObject = {
     fieldsetid: fieldsetid
  }

  var subObj = R.compose(R.assocPath([inputtype], R.__, baseObject), R.reject(R.isEmpty), R.zipObj(keys)); 
  subObj(values);

  return subObj(values); 

});
exports.BuildHiddenUIObject = buildHiddenUIObject;


// buildHandlebarUIObject :: String -> String -> String -> String -> String -> String -> String -> [String] -> JSONObject
var buildHandlebarUIObject = R.curry(function(fieldsetid, inputtype, id, name, instructable, label, placeholder, prefixaddon, image, options) {

  var keys = ['id', 'name', 'instructable', 'label', 'placeholder', 'prefixaddon', 'image', 'rows'];
  var values = [id, name, instructable, label, placeholder, prefixaddon, image, 8];
  var mergeOptions = R.when(R.has('select'), R.assocPath(['select', 'options'], options));

  var baseObject = {
     fieldsetid: fieldsetid
  }

  var subObj = R.compose(mergeOptions, R.assocPath([inputtype], R.__, baseObject), R.reject(R.isEmpty), R.zipObj(keys)); 
  subObj(values);

  return subObj(values); 

});
exports.BuildHandlebarUIObject = buildHandlebarUIObject;

// creates options
var createOptions = function(option1, option2) {
  optionset1 = [{option: option1, value: 1, default: true}, {option: option2, value: 2}]; 
  optionset2 = [{option: option1, value: 1}, {option: option2, value: 2, default: true}]; 
  return([optionset1, optionset2]);
};
exports.CreateOptions = createOptions;


// getHostGameAddress :: Integer -> Promise(Object)
var getHostGameAddress = R.curry(function(account, email) {
  return new Promise(function (fulfill, reject) {
    accounts.ReadAccountFromEmail(account, email).then(function(x) {
      if (x.length == 1) {

        var alcOptions = createOptions("No alcohol thanks", "I don't mind if people want to drink");
        var alcValue = R.compose(R.nth(R.__, alcOptions));
        var alcohol = buildHandlebarUIObject('hostalcoholid', 'select', 'hostalcohol', '', 'Are you OK with people drinking alcohol during games?', '', '', '', '', alcValue(1));

        var smkOptions = createOptions("No smoking please", "Smoking is OK");
        var smkValue = R.compose(R.nth(R.__, smkOptions));
        var smoking = buildHandlebarUIObject('hostsmokingid', 'select', 'hostsmoking', '', 'Please select if you are comfortable with people smoking at your games.', 'Social Contracts', '', '', '', smkValue(0));

        fulfill([smoking, alcohol]);

      } else {
        reject('email not found');

      }
    });
  });
});
exports.GetHostGameAddress = getHostGameAddress;

// buildURL :: s => u
const buildURL = (event) => 'hostgame?event=' + event;
exports.BuildURL = buildURL


// flat :: ({o} -> [a]) => [b] 
const flat = function(req, x) {
  const extractUser = compose(head, tail);
  return (S.path(['query', 'event'], req).isNothing) ? R.flatten(x) : [{},{}, extractUser(x)];
}
exports.Flat = flat;


// buildPageData :: (s -> [q]) => {o}
const buildPageData = curry((label, questions) => zipObj(['pageURL', 'sections'], ["hostgame", R.of(
    {
        title: "Host Event?",
        label: label,
        icon: "fa fa-ticket",
        nosubmit: true,
        questions: questions 
    }
)]));
exports.BuildPageData = buildPageData;

// buildConvHeader :: s => u
const buildConvHeader = (event) => 'Convention / ' + event;
exports.BuildConvHeader = buildConvHeader;

// errortext :: s => u
const errorText = (error) => 'Can not host game: ' + error;
exports.ErrorText = errorText;

// routeHostGame :: Express -> Object -> Model -> Schema -> Schema -> 0
var routeHostgame = function (expressApp, CheckLogin, match, account) {

	var url = "hostgame";
	var urlFail = "login";

  var slashString = (file) => '/' + file;

  // buildEventKey :: {o} => {e}
  const buildEventKey = (req) => S.path(['query', 'event'], req).map(buildHiddenUIObject('div4eventid', 'hiddeninput', 'eventid', 'eventid')).getOrElse({});

  // buildValidPageData :: {r} => {o}
  const buildValidPageData = (req) => compose(
    buildPageData('Meet people who want to play your favourite games.'), 
    append(buildEventKey(req))
  );

  // maybebuildNavs :: {o} => {m} 
  const maybeBuildNavs = (req) => R.lift(expressHelper.BuildNavbar)((Maybe.of('hostgame')), S.path(['user', 'email'], req));
  const maybeBuildNavsEvent = (req) => R.lift(expressHelper.BuildNavbarEvent(S.path(['user', 'email'], req)))(S.path(['query', 'event'], req), S.path(['query', 'event'], req).map(buildURL));

  // maybeNavs :: {r} => {o}
  const maybeNavs = (req) => (S.path(['query', 'event'], req).isJust) ? maybeBuildNavsEvent(req) : maybeBuildNavs(req);

	expressApp.get(slashString(url), CheckLogin.ensureLoggedIn(slashString(urlFail)), function(req, res) {

    var email = R.path(['user', 'email'], req);
    var findlocation = getHostGameAddress(account, email);

    // note user activity
    S.path(['user', 'email'], req).chain(accounts.TaskRecordActivity(account));

    // build testkey
    const mergeTestKey = curry((req, x) => {

			// maybeQuad :: a => Maybe(a)
			const maybeQuad = (a) => (a == 1) ? Maybe.of(true) : Maybe.Nothing();

			// testKeyObj :: (r -> {o}) => {b}
			const testKeyObj = compose(map(R.objOf('testquadrant')), chain(maybeQuad), map(accounts.GetTestQuadrant), S.path(['user', 'key']));
			return testKeyObj(req).isJust ?  merge(testKeyObj(req).get(), x) : x;

    }); 

    Promise.all([findlocation, accounts.BuildNavbars(account, email)]).then(function(x) {

      const send = R.compose(
        renderData(res, url), 
        mergeTestKey(req), 
        R.merge({boardgameserviceurl: nconf.get('service:boardgames:weburl')}),
        R.merge({css:[{csshref:"/css/lightpick.css"}]}),
        R.merge(expressHelper.BuildNavbarHead(S.path(['query', 'event'], req).map(buildConvHeader).getOrElse('Create Game'))), 
        R.merge(maybeNavs(req).map(R.objOf('navbarul')).getOrElse({})), 
        R.merge(flat(req, x)[2]), 
        buildValidPageData(req)
      );
      send(x);

    }).catch(function(err) {
      const unavailableObj = compose(buildHandlebarUIObject('game', 'hiddeninput', '', '', '', R.__, '', '', '', ''), errorText);
      var errorpage = buildPageData("Something went wrong: " + err, unavailableObj(err));
      const senderr = compose(renderData(res, url), merge(maybeBuildNavs(req).getOrElse({})), R.merge(errorpage));
      accounts.BuildNavbars(account, email).then(senderr);

    });
       
	});
};
exports.RouteHostgame = routeHostgame;
