var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var Promise = require('bluebird');
var R = require('ramda');
var compose = R.compose, 
    composeP = R.composeP,
    map     = R.map,
    merge   = R.merge;
var util = require('util');
var log = require('./log.js');
var expressHelper = require('./expresshelper');

const Maybe = require('data.maybe')
var S = require('./lambda.js');

// maybeDefined :: a => Maybe(a) || Maybe(Nothing)
const maybeDefined = (value) => ((value == undefined) || (value == {})) 
  ? Maybe.Nothing() 
  : Maybe.of(value);

// renderData :: Render -> String -> Object -> Render 
const renderData = R.curry((res, template, data) => res.render(template, data));

// slashString :: String -> String
const slashString = (endpoint) => "/" + endpoint;

// unlinkObj :: ObjA => ObjB
const unlinkObj = compose(JSON.parse, JSON.stringify);

var routeGems = function (expressApp, CheckLogin) {

    const buygems_page = {
        pageURL: "buygems",
        sections: [ 
            {
                title: "",
                image: "/img/1_2_34.png",
                label: "",
                icon: "fa fa-photo",
                canvas: "/img/carcassonne.jpg",
                questions: [
					{
						label: {
							label: "Gems",
							instructable: "Gems can be used to reward other players after a successful game night. Gifting a player a gem gives them one months worth of premium membership, giving players a unique diffentiating color on thier avatar and a premium badge. Gems can be earned by hosting games for the community or purchased below.",
							id: "membershipperks"
						}
					},

                    {
                        select: {
                            label: "Purchase Gems",
                            //instructable: "Purchase gems once off for yourself, as an ongoing subscription, gifts for others.",
                            instructable: "Purchase gems once off for yourself or giftable gems for others.",
                            id: "plantype",
                            options: [
                                //{option: "Premium membership for yourself", value: 1},
                                //{option: "Ongoing membership subscription", value: 2},
                                {option: "Giftable gems for friends", value: 3},
                            ]
                        }
                    },

                    /*
                    {
                        listgroup: {
                            label: "Membership for Yourself",
                            instructable: "A once off purchase for yourself.",
                            id: "planonceprice",
                            options: [
                                {option: "One month - $5 AUD", value: 1, vclick:"clickOnce", id:'itemonce1'},
                                {option: "Three months - $15 AUD", value: 3, vclick:"clickOnce", id:'itemonce3'},
                                {option: "Twelve months (discounted) - $50 AUD", value: 12, default: true, vclick:"clickOnce", id:'itemonce12pack1'},
                                {option: "Twenty-four months (discounted) - $100 AUD", value: 24, vclick:"clickOnce", id:"itemonce12pack2"},
                                {option: "Thirty-six months (discounted) - $150 AUD", value: 36, vclick:"clickOnce", id:"itemonce12pack3"},
                            ]
                        }
                    },
                    */

                    /*
                    {
                        listgroup: {
                            label: "Ongoing plan",
                            instructable: "An ongoing plan continuously replenishes at the end of the period. How many months do you want to purchase?",
                            id: "planongoingprice",
                            options: [
                                {option: "Monthly - $5 AUD", value: 1, vclick:"clickOngoing", id:"itemongoing1"},
                                {option: "Every Three months - $15 AUD", value: 3, vclick:"clickOngoing", id:"itemongoing3"},
                                {option: "Annually (discounted) - $50 AUD", value: 12, default: true, vclick:"clickOngoing", id:"itemongoing12pack1"},
                            ]
                        }
                    },
                    */

                    {
                        listgroup: {
                            label: "",
                            instructable: "Gems are stored on your account and can be gifted to other players, or used yourself. Each gem is worth one month of membership perks. Gems need to be gifted within twelve months, otherwise they expire.",
                            id: "plancreditprice",
                            options: [
                                {option: "One gem - $5 AUD", value: 1, vclick: "clickCredit", id:"itemcredit1"},
                                {option: "Three gems - $15 AUD", value: 3, vclick: "clickCredit", id:"itemcredit3"},
                                {option: "Twelve gems (discounted) - $50 AUD", value: 12, default: true, vclick: "clickCredit", id:"itemcredit12pack1"},
                                {option: "Twenty-four gems (discounted) - $100 AUD", value: 24, vclick: "clickCredit", id:"itemcredit12pack2"},
                                {option: "Thirty-six gems (discounted) - $150 AUD", value: 36, vclick: "clickCredit", id:"itemcredit12pack3"},
                            ]
                        }
                    },

                ]
            }
        ]
    };

    const readAccount = accounts.ReadAnAccountFromEmail(account);

    // userProfile :: email => Maybe(jsonObj) 
    const userProfile = composeP(map(unlinkObj), S.prop('profile'), readAccount);

    expressApp.get(slashString('buygems'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res) {

        // note user activity
        S.path(['user', 'email'], req).chain(accounts.TaskRecordActivity(account));

        const resolveProfile = R.curry((data, x) => {
            x.then(function(interior) {

                // maybebuildNavs :: {o} => {m} 
                const maybeBuildNavs = (req) => S.path(['user', 'email'], req).map(expressHelper.BuildNavbar('profile')).map(R.objOf('navbarul'));

                const render = composeP(renderData(res, 'buygems'), merge(expressHelper.BuildNavbarHead('Profile')), merge(maybeBuildNavs(req).getOrElse({})), merge(unlinkObj(data)), accounts.BuildNavbars(account));
                render(req.user.email);

            }).catch(function(err) {
                res.status(401);
            });
        });

        maybeDefined(req.user.email).map(userProfile).map(resolveProfile(buygems_page));
        
    });

};
exports.RouteGems = routeGems;
