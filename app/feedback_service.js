var express = require('express');
var bodyParser = require('body-parser')

var util = require('util');
var R = require('ramda');
var allPass = R.allPass,
    assoc   = R.assoc,
    compose = R.compose,
    composeP = R.composeP,
    curry   = R.curry,
    dissoc  = R.dissoc,
    equals  = R.equals,
    flatten = R.flatten,
    has     = R.has,
    hasIn   = R.hasIn,
    length  = R.length,
    map     = R.map,
    of      = R.of,
    path    = R.path,
    prop    = R.prop,
    zipObj  = R.zipObj;

var Promise = require('bluebird');
var log = require('./log.js');
var exports = module.exports = {};

var account = require('./models/account.js');
var accounts = require('./models/accounts.js');
var feedback = require('./models/feedback.js');

// createFeedback :: String -> String -> String -> String -> String -> Obj
var createFeedback = function(useremail, userkey, likes, dislikes, suggestion) {

    var validify = composeP(equals(1), length, accounts.ReadVerifyAccountFromEmail(account));
    var newMongObj = (obj) => new feedback(obj).save();
    var feedbackObj = compose(newMongObj, zipObj(['useremail', 'likes', 'dislikes', 'suggestion']));

    return new Promise(function(fulfill, reject) {
        validify(useremail, userkey).then(function(isValid) {
            (isValid) ? fulfill(feedbackObj([useremail, likes, dislikes, suggestion])) : reject(401);
        });
    });
};
exports.CreateFeedback = createFeedback;


// setupAccountsPostListener :: express 
var setupRouter = function() {

    var router = express.Router();
    var jsonParser = bodyParser.json();

    var scrubMongo = compose(dissoc('_id'), dissoc('__v'), JSON.parse, JSON.stringify);

    router.route('/')
        .post(jsonParser, function(req, res) {

            if (!req.body) return res.sendStatus(400);  // malformed request

            var userkey = '';
            if (has('userkey', req.body)) {
                userkey = prop('userkey', req.body);
            } else {
                if (has('user', req)) {
                    var hasKey = compose(has('key'), prop('user'));
                    if (hasKey(req)) {
                        userkey = path(['user', 'key'], req);
                    } else {
                        res.sendStatus(401);
                    }
                } else {
                    res.sendStatus(401);
                }
            };

            createFeedback(prop('email', req.body), userkey, prop('likes', req.body), prop('dislikes', req.body), prop('suggestion', req.body)).then(function(x) {
                res.json(scrubMongo(x));

            }).catch(function(err) {
                res.sendStatus(401);

            });
        });
    return router;

}; 
exports.SetupRouter = setupRouter;
