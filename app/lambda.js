var maybe     = require('data.maybe');
var Task      = require('data.task');

var R         = require('ramda');
var log       = require('./log.js');

// taskFail => task n
const taskFail   = new Task((reject, resolve) => reject(500));
exports.taskFail = taskFail;

// prop :: s -> {s: a} -> maybe a
const prop = R.curry((x, o) => 
  (R.isEmpty(o) || R.isNil(o) || R.isNil(o[x])) 
    ? maybe.Nothing() 
    : maybe.of(o[x]));
exports.prop = prop;

// propDefaultTo :: a -> s -> {s: a} -> maybe a
const propDefaultTo = R.curry((defaultValue, x, o) => 
  (R.isEmpty(o) || R.isNil(o) || R.isNil(o[x])) 
    ? maybe.of(defaultValue) 
    : maybe.of(o[x]));
exports.propDefaultTo = propDefaultTo;

// path :: [s] -> o -> maybe a
const path = R.curry((x, o) => 
  (R.isEmpty(o) || R.isNil(o) || R.isEmpty(x) || R.isNil(R.path(x, o))) 
    ? maybe.Nothing() 
    : maybe.of(R.path(x, o)));
exports.path = path;

// pathDefaultTo :: a -> [s] -> o -> maybe a
const pathDefaultTo = R.curry((defaultValue, x, o) => 
  (R.isEmpty(o) || R.isNil(o) || R.isEmpty(x) || R.isNil(R.path(x, o))) 
    ? maybe.of(defaultValue) 
    : maybe.of(R.path(x, o)));
exports.pathDefaultTo = pathDefaultTo;

// whenPropEq :: s -> a -> o -> maybe o
const whenPropEq = R.curry((x, v, o) =>  {
  return  (!R.isNil(o))
    ? R.propEq(x, v, o)
      ? maybe.of(o)
      : maybe.Nothing()
    : maybe.Nothing()
});
exports.whenPropEq = whenPropEq;

// pathEq :: [a] -> a -> o -> maybe o
const pathEq = R.curry((ar, val, obj) => 
  path(ar, obj).isNothing 
    ? maybe.Nothing() 
    : R.path(ar, obj) === val 
      ? maybe.of(obj) 
      : maybe.Nothing()); 
exports.pathEq = pathEq;

// filter :: (f(a) => b) -> [a] -> maybe [a]
const filter = R.curry((a, b) => 
  (R.filter(a, b) === undefined || R.filter(a, b).length === 0) 
    ? maybe.Nothing() 
    : maybe.of(R.filter(a, b)));
exports.filter = filter;

// find :: (f(a) => b) -> [a] -> maybe a
const find = R.curry((a, b) => 
  (R.find(a, b) === undefined || R.find(a, b).length === 0) 
    ? maybe.Nothing() 
    : maybe.of(R.find(a, b)));
exports.find = find;

// head :: [a] -> maybe a
const head = (x) => R.type(x) === "Array"
  ? R.length(x) === 0 
    ? maybe.Nothing() 
    : maybe.of(x[0])
  : maybe.Nothing();
exports.head = head;

// last :: [a] -> maybe a
const last = (x) => 
  (x[0] === undefined) 
    ? maybe.Nothing() 
    : maybe.of(R.last(x));
exports.last = last;

// whichever :: maybe a -> maybe a -> maybe a
const whichever = R.curry((mX, mY) => mX.isNothing
  ? mY
  : mX);
exports.whichever = whichever;

// nth :: n -> [a] -> maybe a
const nth = R.curry((n, x) => 
  (x[n] === undefined) 
    ? maybe.Nothing() 
    : maybe.of(x[n])
);
exports.nth = nth;

// has :: s -> {s:x} -> maybe Bool 
const has = R.curry((s, o) =>
  R.has(s, o)
    ? maybe.of(true)
    : maybe.Nothing()
);
exports.has = has;

// gt :: n -> n -> maybe Bool 
const gt = R.curry((n, m) =>
  R.gt(n, m)
    ? maybe.of(true)
    : maybe.Nothing()
);
exports.gt = gt;

// equals :: a -> a -> maybe a 
const equals = R.curry((x, y) => 
  (x === y)
    ? maybe.of(x)
    : maybe.Nothing()
);
exports.equals = equals;


