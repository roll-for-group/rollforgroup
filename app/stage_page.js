var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var Promise = require('bluebird');
var R = require('ramda'),
    append  = R.append,
    assoc   = R.assoc,
    compose = R.compose,
    curry   = R.curry,
    dissoc  = R.dissoc,
    head    = R.head,
    map     = R.map,
    merge   = R.merge,
    prepend = R.prepend,
    prop    = R.prop;

var Maybe = require('data.maybe');

var util = require('util');
var log = require('./log.js');
var expressHelper = require('./expresshelper');

// slashString :: String -> String
var slashString = (endpoint) => "/" + endpoint;

// routeStage :: Express -> Object -> Model -> Schema
var routeStage = function (expressApp, CheckLogin) {

    
  expressApp.get(slashString('stage1'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res) {
		res.render('stage1', {
      name:     req.user.profile.name,
      email:    req.user.email,
      pageslug: req.user.pageslug,
      key:      req.user.key,
      webtitle: 'Lets help you play more games page 1', 
      metadescription: 'Understanding users', 
      css:[{csshref:'/css/stage.css'}]
    })
  });

  expressApp.get(slashString('stage2'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res) {
		res.render('stage2', {
      name:     req.user.profile.name,
      email:    req.user.email,
      pageslug: req.user.pageslug,
      key:      req.user.key,
      webtitle: 'Lets help you play more games - page 2', 
      metadescription: 'Understanding users', 
      css:[{csshref:'/css/stage.css'}]
    })
  });

  expressApp.get(slashString('stage3'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res) {
		res.render('stage3', {
      name:     req.user.profile.name,
      email:    req.user.email,
      pageslug: req.user.pageslug,
      key:      req.user.key,
      webtitle: 'Lets help you play more games - page 3', 
      metadescription: 'Understanding users', 
      css:[{csshref:'/css/stage.css'}]
    })
  });

  expressApp.get(slashString('stage4'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res) {
		res.render('stage4', {
      name:     req.user.profile.name,
      email:    req.user.email,
      pageslug: req.user.pageslug,
      key:      req.user.key,
      webtitle: 'Lets help you play more games - page 4', 
      metadescription: 'Understanding users', 
      css:[{csshref:'/css/stage.css'}]
    })
  });

};
exports.RouteStage = routeStage;


// nextStage :: String -> String
var nextStagePage = (x) => (x < 4) 
  ? "/stage" + (parseInt(x) + 1) 
  : "/events";
exports.NextStagePage = nextStagePage;
