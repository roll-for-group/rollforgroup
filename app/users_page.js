var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');

var Promise = require('bluebird');
var R = require('ramda');
var append  = R.append,
    apply   = R.apply,
    assoc   = R.assoc,
    assocPath = R.assocPath,
    compose = R.compose, 
    concat  = R.concat,
    curry   = R.curry, 
    evolve  = R.evolve,
    isEmpty = R.isEmpty,
    isNil   = R.isNil,
    map     = R.map,
    merge   = R.merge,
    not     = R.not,
    path    = R.path,
    pick	  = R.pick,
    prop    = R.prop,
    range   = R.range,
    zipObj  = R.zipObj;

var log = require('./log.js');
var expressHelper = require('./expresshelper');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var S = require('./lambda.js');

const moment = require('moment');

const clr = ['#EBF4FC', '#C9E4EC', '#96CCD4', '#52ACB4', '#32686C'];

// safePath :: ([a] -> b) => Maybe(b[a])
const safePath = curry((x, o) => (isEmpty(o) || isNil(path(x, o))) ? Maybe.Nothing() : Maybe.of(path(x, o)));

// findProps :: ({obj} -> [props]) => [values];
const findProps = curry((data, fields) => map(prop(R.__, data), fields));
exports.FindProps = findProps;

//  :: ({res} -> t -> {d}) => f(Render) 
const renderData = R.curry((res, template, data) => res.render(template, data));

// slashString :: String -> String
const slashString = (endpoint) => "/" + endpoint;

// maybeDefined :: a -> Maybe(a || Nothing)
const maybeDefined = (value) => ((value == undefined) || (value == {})) ? Maybe.Nothing() : Maybe.of(value);

// maybeSomething :: (Maybe(a) -> Maybe(b)) => Maybe(a || b)
const maybeSomething = curry((a, b) => (b.isNothing) ? a : b);

// getUserEmail :: Obj -> Maybe(String)
const getUserEmail = safePath(['user', 'email']);

// pushPanel :: ({a} -> {b}) => {c} 
const pushPanel = curry((masterobj, pushobj) => {
  var x = JSON.parse(JSON.stringify(masterobj));
  x.panels = append(pushobj, x.panels);
  return x;
});
exports.PushPanel = pushPanel;

// buildUserPanels :: (v -> t -> c -> f -> v) => {panel}
const buildUserPanel = curry(function(value, id, divclass, text, faicon) {
  return {
    userpanels: {
      id: id,
      divclass: divclass,
      heading: {
        text: text,
        faicon: faicon
      },
      body: {text: value}
    }
  };
});
exports.BuildUserPanel = buildUserPanel;

// assocSVGPanel (v -> t -> c -> f -> [l] -> {w}) => {w} || {n}
const assocSVGPanel = curry(function(value, id, divclass, text, faicon, svgdata, webdata) {
  const createPanel = compose(pushPanel(webdata), setSVG(svgdata), buildUserPanel);
  return createPanel(value, id, divclass, text, faicon);
});
exports.AssocSVGPanel = assocSVGPanel;


// mergeWhenRender :: o -> b -> o
const mergeWhenRender = curry((x, obj) => {
  return x
    ? R.merge(obj, {render: true})
    : obj
});
exports.mergeWhenRender = mergeWhenRender;


// assocTextPanel (v -> t -> c -> f -> {w} -> b) => {w} || {n}
const assocTextPanel = curry((value, id, divclass, text, faicon, render, webdata) => {

  const createPanel = compose(
    pushPanel(webdata), 
    mergeWhenRender(render), 
    buildUserPanel
  );

  return (value === "") 
    ? webdata 
    : createPanel(value, id, divclass, text, faicon);

});
exports.AssocTextPanel = assocTextPanel;

// setsvg ({list} -> {obj}) => {newobj}
const setSVG = curry((svgdata, panel) => {
  panel.userpanels.svg = svgdata;
  return panel;
});
exports.SetSVG = setSVG;

// setgroup ({list} -> {obj}) => {newobj}
const setGroup = curry((type, listdata, panel) => {
  return assocPath(['userpanels', type], listdata, panel);
});
exports.SetGroup = setGroup;

// assocTablePanel (v -> t -> c -> f -> [l] -> {w}) => {w} || {n}
const assocTablePanel = curry(function(value, id, divclass, text, faicon, listdata, webdata) {
  const createPanel = compose(pushPanel(webdata), setGroup('table', listdata), buildUserPanel);
  return createPanel(value, id, divclass, text, faicon);
});
exports.AssocTablePanel = assocTablePanel;

// assocListgroupPanel (v -> t -> c -> f -> [l] -> {w}) => {w} || {n}
const assocListgroupPanel = curry(function(value, id, divclass, text, faicon, listdata, webdata) {
  const createPanel = compose(pushPanel(webdata), setGroup('listgroup', listdata), buildUserPanel);
  return createPanel(value, id, divclass, text, faicon);
});
exports.AssocListgroupPanel = assocListgroupPanel;

// appendWhenExist :: (v -> k -> fa -> [a]) => [a] || [b]
const appendWhenExist = curry((value, text, faicon, array) => (value != '' && value != undefined) ? append({listitem: zipObj(['text', 'faicon'], [text + ': ' + value, faicon])}, array) : array); 
exports.AppendWhenExist = appendWhenExist;

// appendEmail :: (b -> e -> [a]) => [a] || [b]
const appendEmail = curry((showemail, email, array) => showemail ? array : appendWhenExist(email, 'Email', 'fa-envelope', array));
exports.AppendEmail = appendEmail;


// calculateLevel :: n => {m}
const calculateLevelLimits = function(xp) {

  var checklevel = 0;
  var nextxpbreak = 0;
  var lastxpbreak = 0;
  var xpinterval = 50;

  while (xp >= nextxpbreak) {
    checklevel ++;
    xpinterval = xpinterval * 1.1;
    lastxpbreak = nextxpbreak;
    nextxpbreak = nextxpbreak + xpinterval;
  };

  return {
    level:  checklevel -1, 
    xplast: Number(nextxpbreak - lastxpbreak).toFixed(0),
    xpnext: Number(nextxpbreak).toFixed(0),
    xppos: Number(xp - lastxpbreak).toFixed(0),
    xpvalue: Number(xp).toFixed(0),
    xpperc: Number(( xp - lastxpbreak ) / ( nextxpbreak - lastxpbreak ) * 100).toFixed(0)
  };

};
exports.CalculateLevelLimits = calculateLevelLimits;


// addHeaderData :: ({u} -> {w}) => {x}
const addHeaderData = curry((userdata, webdata) => {
  return merge(webdata, {
    userslug: userdata.pageslug,
    userheader: {
      userimage: {
        img: {
          class:  'img-rounded',
          src:    S.prop('avataricon', userdata).getOrElse('img/default-avatar.jpg'),
          alt:    userdata.name
        }
      },
      username: {
        div: {
          class:  'text-center',
          text:  userdata.name 
        }
      },
      userstats: {
        div: {
          class:  'text-center',
          text: "BGG User: ",
          a: {
            href: 'https://www.boardgamegeek.com/user/' + userdata.bggname,
            text: userdata.bggname,
            target:'_blank'
          }
        }
      }
    },
    experience: {
      hostxpbar: calculateLevelLimits(userdata.hostxp),
      playxpbar: calculateLevelLimits(userdata.playxp),
      hostlevel: userdata.hostlevel,
      playlevel: userdata.playlevel,
    }
  });
});
exports.AddHeaderData = addHeaderData;

// pushListitem :: ({lookup} -> {data} -> key -> [{list}]) => [{list}, {newitem}] || [{list}]
const pushListitem = curry((lookup, data, key, arr) => (prop(key, data)) 
  ? append(prop(key, lookup), arr) 
  : arr);
exports.PushListitem = pushListitem;

// pushListtitle :: ({lookup} -> {data} -> key -> [{list}]) => [{list}, {newitem}] || [{list}]
const pushTitleitem = curry((lookup, data, key, arr) => (prop(key, data)) 
  ? append(assoc('text', prop(key, data), prop(key, lookup)), arr) 
  : arr);
exports.PushTitleitem = pushTitleitem;

// pushALinkitem :: ({lookup} -> {data} -> key -> [{list}]) => [{list}, {newitem}] || [{list}]
const pushALinkitem = curry((lookup, data, key, arr) => (prop(key, data)) 
  ? append(assoc('a', { href: path([key, 'url'], lookup) + prop(key, data), text: prop(key, data) }, prop(key, lookup)), arr) 
  : arr);
exports.PushALinkitem = pushALinkitem;

// createMotiveList :: {obj} => [{motive}]
const createMotiveList = function(userdata) {

  const motives = {
    motivecompetition   : {faicon: 'fa-bullseye', text: 'Conflict'},
    motivecommunity     : {faicon: 'fa-users', text: 'Community'},
    motivestrategy      : {faicon: 'fa-cogs', text: 'Strategy'},
    motivestory         : {faicon: 'fa-book', text: 'Immersion'},
    motivedesign        : {faicon: 'fa-pencil', text: 'Design'},
    motivediscovery     : {faicon: 'fa-search-plus', text: 'Discovery'},
    motivecooperation   : {faicon: 'fa-handshake-o', text: 'Cooperation'},
    motivesocialmanipulation     : {faicon: 'fa-gavel', text: 'Social Manipulation'},
  }

  const pushMotiveData = pushListitem(motives, userdata);
  const makeMotiveArray = compose(pushMotiveData('motivecompetition'), pushMotiveData('motivecommunity'), pushMotiveData('motivestrategy'), pushMotiveData('motivestory'), pushMotiveData('motivedesign'), pushMotiveData('motivediscovery'), pushMotiveData('motivesocialmanipulation'), pushMotiveData('motivecooperation'));

  return makeMotiveArray([]);

};
exports.CreateMotiveList = createMotiveList;


// colourDay :: (hr, bool, doWeek, sHr, sMin, eHr, eMin) => colorIndex
const colourDay = curry((hour, day, starthour, startminute, endhour, endminute) => (day) && (starthour <= hour) && ((endhour + (endminute / 60)) > hour) ? 2 : 0);
exports.ColourDay = colourDay;


// buildHourColumn :: ({user}, h) => [x , mo, tu, we, th, fr, sa, su ]
const buildHourColumn = curry((userdata, hour) => {

  const hourColumn = [33, 50, 67, 84, 101, 118, 135, 152, 169, 186, 203, 220, 237, 254, 271, 288, 305, 322, 339, 356, 373, 390, 407, 424];

  const fields= [ 
    ['Monday', 'mondaystarthour', 'mondaystartminute', 'mondayendhour', 'mondayendminute'], 
    ['Tuesday', 'tuesdaystarthour', 'tuesdaystartminute', 'tuesdayendhour', 'tuesdayendminute'], 
    ['Wednesday', 'wednesdaystarthour', 'wednesdaystartminute', 'wednesdayendhour', 'wednesdayendminute'], 
    ['Thursday', 'thursdaystarthour', 'thursdaystartminute', 'thursdayendhour', 'thursdayendminute'], 
    ['Friday', 'fridaystarthour', 'fridaystartminute', 'fridayendhour', 'fridayendminute'], 
    ['Saturday', 'saturdaystarthour', 'saturdaystartminute', 'saturdayendhour', 'saturdayendminute'], 
    ['Sunday', 'sundaystarthour', 'sundaystartminute', 'sundayendhour', 'sundayendminute'] 
  ];

  // createSqures :: [[dayfields], [dayfields]...] => {svgcolumn} 
  const createSquares = compose(concat([hour, hourColumn[hour]]), map(apply(colourDay(hour))), map(findProps(userdata)));

  return createSquares(fields);

});
exports.BuildHourColumn = buildHourColumn;

// formatTime :: h => hhmmAPm
const formatTime = (hr) => ((hr < 13) ? hr : hr - 12) + ":00 " + ((hr < 12) ? 'AM': 'PM');
exports.FormatTime = formatTime;


// buildRect :: (x -> y -> w -> h -> color) => {obj}
const buildRect = curry((x, y, width, height, fill, tooltip) => zipObj(['x', 'y', 'width', 'height', 'fill', 'tooltip'], [x, y, width, height, fill, {title: tooltip}]));
exports.BuildRect = buildRect;

// buildGTransform :: (x -> y -> [[x, color, title]...]) => {svgTransform}
const buildGYTransform = function(left, top, rects) {

  // buildRectY :: (y -> color -> title) => {obj}
  const buildRectY = buildRect(0, R.__, 15, 15);
  return zipObj(['transform', 'rects'], [{left: left, top: top}, map(apply(buildRectY), rects) ]);

};
exports.BuildGYTransform = buildGYTransform;

// buildText :: (x -> y -> a -> c -> t) => {obj}
const buildText = curry((x, y, anchor, htmlclass, text) => zipObj(['x', 'y', 'anchor', 'class', 'text'], [x, y, anchor, htmlclass, text]));
exports.BuildText = buildText;

// buildGTransform :: (x -> y -> [[x, color, title]...]) => {svgTransform}
const buildGXTransform = function(left, top, rects) {

  // buildRectX :: (x -> color -> title) => {obj}
  const buildRectX = buildRect(R.__, 0, 15, 15);
  return zipObj(['transform', 'rects'], [{left: left, top: top}, map(apply(buildRectX), rects) ]);

};
exports.BuildGXTransform = buildGXTransform;

// buildGDirection :: (type -> [texts]) => {svgDirection}
const buildGDirection = function(type, texts) {

  // buildTextX :: (x -> time) => {obj}
  const buildTextX = buildText(R.__, 10, '', '');
  return zipObj(['direction', 'texts'], [{type: type}, map(apply(buildTextX), texts) ]);   

};
exports.BuildGDirection = buildGDirection;

// buildG :: ([[y, day]..]) => {svg}
const buildG = function(texts) {

  // buildTextV :: (y -> day) => {obj}
  const buildTextV = buildText(16, R.__, 'middle', '');
  return R.objOf('texts', map(apply(buildTextV), texts));

};
exports.BuildG = buildG;

// buildColumn :: (x -> m -> tu -> w -> th -> f -> sa -> su) => {svgTransform}
const buildColumn = curry((hr, x, mo, tu, we, th, fr, sa, su) => buildGYTransform(x,  18, [ 
  [0, clr[mo], 'Monday ' + formatTime(hr) + ((mo > 1) ? ' Available' : '')],
  [17, clr[tu], 'Tuesday ' + formatTime(hr) + ((tu > 1) ? ' Available' : '')],
  [34, clr[we], 'Wednesday ' + formatTime(hr) + ((we > 1) ? ' Available' : '')], 
  [51, clr[th], 'Thursday ' + formatTime(hr) + ((th > 1) ? ' Available' : '')], 
  [68, clr[fr], 'Friday ' + formatTime(hr) + ((fr > 1) ? ' Available' : '')], 
  [85, clr[sa], 'Saturday ' + formatTime(hr) + ((sa > 1) ? ' Available' : '')], 
  [102, clr[su], 'Sunday ' + formatTime(hr) + ((su > 1) ? ' Available' : '')] 
]));
exports.BuildColumn = buildColumn;

// createSVG :: {obj} => {SVG}
const createSVG = function(userdata) {

  // buildSVG :: ([x -> y -> class -> [g]]) => {svgObj}
  const buildSVG = compose(zipObj(['width', 'height', 'htmlclass', 'g']));

  // buildHour :: (h -> x) => [ x, mo, tu, we, th, fr, sa, su ]
  const buildHour = buildHourColumn(userdata);

  return buildSVG([447, 167, '', concat(map(apply(buildColumn), map(buildHour, range(0, 24))),
    [
      buildGDirection('ltr', [ [33, '0:00 AM'], [135, '6:00 AM'], [237, '12:00 PM'], [339, '6:00 PM']]),
      buildGXTransform(33, 152, [ [0, clr[0], 'No Availability'], [17, clr[1], ''], [34, clr[2], ''], [51, clr[3], ''], [68, clr[4], 'High Availability'] ]),
      buildG( [[31, 'Mon'], [65, 'Wed'], [99, 'Fri'], [133, 'Sun']] )
    ]
  )]);

};
exports.CreateSVG = createSVG;


// createCommunityList :: {obj} => [{motive}]
const createCommunityList = function(userdata) {

  const community = {
    ishost      :   {faicon: 'fa-home', text: 'Host', tooltip: {title: 'I have access to a venue for hosting games', placement: 'right'}},
    isteacher   :   {faicon: 'fa-graduation-cap', text: 'Game Teacher', tooltip: {title: 'I am willing to teach game rules to new players', placement: 'right'}},
    nodriving   :   {faicon: 'fa-car', text: 'Driver', tooltip: {title: 'I can drive to venues', placement: 'right'}},
    pubtransport :  {faicon: 'fa-train', text: 'Public Transport', tooltip: {title: 'I require games near public transport', placement: 'right'}},
    carpool     :   {faicon: 'fa-bus', text: 'Willing to Carpool', tooltip: {title: 'I am willing to drive with other players', placement: 'right'}},
    playeralcohol:  {faicon: 'fa-beer', text: 'Alcohol Friendly', tooltip: {title: 'I prefer venues where drinking is permitted', placement: 'right'}},
    playersmoking:  {faicon: 'fa-ban', text: 'No Smoking', tooltip: {title: 'I require non-smoking venues', placement: 'right'}}
  };

  const transform = {
    nodriving: not,
    ishost          : (x) => (x == 1) ? false : true,
    isteacher       : (x) => (x == 1) ? false : true,
    pubtransport    : (x) => (x == 1) ? false : true,
    playeralcohol   : (x) => (x != 1) ? true : false,
    playersmoking   : (x) => (x == 1) ? true : false
  }

  const pushCommunityData = pushListitem(community, evolve(transform, userdata));
  const makeCommunityArray = compose(pushCommunityData('playersmoking'), pushCommunityData('playeralcohol'), pushCommunityData('pubtransport'), pushCommunityData('carpool'), pushCommunityData('nodriving'), pushCommunityData('isteacher'), pushCommunityData('ishost'));

  return makeCommunityArray([]);

};
exports.CreateCommunityList = createCommunityList;


// createContactList :: {obj} => [{motive}]
const createContactList = function(userdata) {

  //commemailvisible : {faicon: 'fa-mail', title: 'Email'}
  const contacts = {
    commpsn : {faicon: 'fa-gamepad', title: 'PSN'},
    commxbox : {faicon: 'fa-gamepad', title: 'XBox Live'},
    commbattlenet : {faicon: 'fa-gavel', title: 'Battle.net'},
    commreddit : {faicon: 'fa-reddit', title: 'Reddit'},
    commskype : {faicon: 'fa-skype', title: 'Skype'},
    commphoneno : {faicon: 'fa-phone', title: 'Phone No'},
    commsteam : {faicon: 'fa-steam-square', title: 'Steam'},
  }

  const pushContactData = pushTitleitem(contacts, userdata);

  // makeContactArray :: [] => [{items}]
  const makeContactArray = compose(
    pushContactData('commpsn'),
    pushContactData('commxbox'),
    pushContactData('commbattlenet'),
    pushContactData('commreddit'),
    pushContactData('commskype'),
    pushContactData('commphoneno'),
    pushContactData('commsteam'),
    pushContactData('commemailvisible')
  );

  return makeContactArray([]);

};
exports.CreateContactList = createContactList;


// createContactList :: {obj} => [{motive}]
const createSocialList = function(userdata) {

  const socials = {
    socialfacebook      : {faicon: 'fa-facebook-square', url:'http://www.facebook.com/', title: 'Facebook'},
    socialtwitter       : {faicon: 'fa-twitter-square', url:'http://www.twitter.com/', title: 'Twitter'},
    socialpinterest     : {faicon: 'fa-pinterest-square', url:'http://www.pinterest.com/', title: 'Pinterest'},
    socialtwitch        : {faicon: 'fa-twitch', url:'https://www.twitch.tv/', title: 'Twitch'},
    socialdiscord       : {faicon: 'fa-gamepad', url:'https://www.discordapp.com/users/', title: 'Discord'},
    socialwebsite       : {faicon: 'fa-globe', url:'http://', title: 'Web'},
    socialyoutube       : {faicon: 'fa-youtube-play', url:'https://www.youtube.com/user/', title: 'Youtube'},
    socialgoogleplus    : {faicon: 'fa-google-plus-square', url:'https://plus.google.com/+', title: 'Google +'},
  }

  const pushSocialLink = pushALinkitem(socials, userdata);

  // makeContactArray :: [] => [{items}]
  const makeSocialArray = compose(
    pushSocialLink('socialfacebook'),
    pushSocialLink('socialtwitter'),
    pushSocialLink('socialpinterest'),
    pushSocialLink('socialtwitch'),
    pushSocialLink('socialyoutube'),
    pushSocialLink('socialdiscord'),
    pushSocialLink('socialgoogleplus'),
    pushSocialLink('socialwebsite')
  );

  return makeSocialArray([]);

};
exports.CreateSocialList = createSocialList;

const createWTPList = function (wtpData) {

	// addText :: {o} => {t}
	const addText = (o) => assoc('text', prop('name', o) + " (" + prop('yearpublished', o) + ")", o); 

	// addText :: {o} => {t}
	const addImg = (o) => assocPath(['img', 'src'], prop('thumbnail', o), o); 
	const addHref = (o) => assocPath(['img', 'href'], "https://boardgamegeek.com/boardgame/" + prop('id', o), o); 

	// convertList :: {o} => [a]
	const convertList = compose(map(map(addHref)), map(map(addImg)), map(map(addText)), map(map(pick(['name', 'thumbnail', 'yearpublished', 'id']))), S.prop('wanttoplay'));

  return (convertList(wtpData).getOrElse([]));

};
exports.CreateWTPList = createWTPList;


// PrepareData :: ({u} -> {w} -> Promise({d})) => {x}
const prepareData = curry((userdata, webdata, wtpdata) => {

  //const assocWanttoplay = assocTablePanel('', 'motive', 'col-lg-12', 'Want To Play', 'fa-child', createWTPList(wtpdata));

  const assocBiography = assocTextPanel(userdata.biography, 'biography', 'col-lg-8 text-centered', 'About Me', 'fa-user', true );

  const assocProfession = assocTextPanel(userdata.job, 'profession', 'col-lg-4', 'Profession', 'fa-briefcase', false);

  const assocMotive = assocListgroupPanel('', 'motive', 'col-lg-4', 'Gaming Interests', 'fa-handshake-o', createMotiveList(userdata));

  const assocSchedule = assocSVGPanel('Weekly Availability', 'schedule', 'col-lg-8 schedule', 'Schedule', 'fa-calendar-o', createSVG(userdata));

  const assocCommunity = assocListgroupPanel('', 'community', 'col-lg-4', 'Community', 'fa-cube', createCommunityList(userdata));

  const assocContact = assocListgroupPanel('Other Accounts', 'contact', 'col-lg-4', 'Contact', 'fa-address-book', createContactList(userdata));

  const assocSocial = assocListgroupPanel('Social Links', 'social', 'col-lg-4', 'Social', 'fa-share-alt', createSocialList(userdata));

  const addUserData = compose(
    assocContact, 
    assocSocial, 
    assocCommunity, 
    assocProfession, 
    assocBiography, 
    assocMotive, 
    assocSchedule, 
    addHeaderData(userdata)
  );

  return addUserData(webdata);

});
exports.PrepareData = prepareData;


// userProfile :: {u} => Maybe({o})
const userProfile = (user) => {

  // mergeSlug :: {u1} => ({u2} -> {p})
  const mergeSlug = (user) => merge({pageslug:S.prop('pageslug', user).getOrElse('')});

  // buildProfile :: {u} => (Maybe({o}))
  const buildProfile = (user) => compose(map(mergeSlug(user)), map(JSON.parse), map(JSON.stringify), S.prop('profile'))(user);

  return buildProfile(user);
};
exports.UserProfile = userProfile;

// buildEmailLink :: s => u
const buildEmailLink = (email) => 'users?email=' + email;
exports.BuildEmailLink = buildEmailLink;

// isOwnProfile :: ({o} -> {p}) => Bool
const isOwnProfile = (req, data) => (getUserEmail(req).getOrElse('y') == S.prop('email', data).getOrElse('x'));
exports.IsOwnProfile = isOwnProfile;

// safePlayerName :: {o} => s 
const safePlayerName = (userdata) => safePath(['profile', 'name'], userdata).getOrElse('Player');
exports.SafePlayerName = safePlayerName;

// buildGemCount :: n => {o}
const buildGemCount = (gemcount) => (gemcount > 0) ? {gemcount: gemcount} : {};
exports.BuildGemCount = buildGemCount;

// safeGemCount :: {r} -> {o} => n
const safeGemCount = (req, dbUser, userdata) => (isOwnProfile(req, userdata)) 
  ? safePath(['premium', 'creditsshare'], dbUser).map(buildGemCount).getOrElse({}) 
  : (Number(safePath(['premium', 'creditsshare'], dbUser).getOrElse(0)) > 0) 
    ? merge({btnGiftGems: true}, safePath(['premium', 'creditsshare'], dbUser).map(buildGemCount).getOrElse({})) 
    : {}
exports.SafeGemCount = safeGemCount

// mergeWhnOwnProfile :: 
const mergeWhenOwnProfile = curry((req, userdata, data) => isOwnProfile(req, userdata) 
  ? merge(data) 
  : merge({}));
exports.MergeWhenOwnProfile = mergeWhenOwnProfile;


const routeUser = function (expressApp, CheckLogin) {

  // maybebuildNavs :: {o} => {m} 
  const maybeBuildNavs = R.curry((req) => R.lift(expressHelper.BuildNavbar)(safePath(['user', 'email'], req).map(buildEmailLink), safePath(['user', 'email'], req)).map(R.objOf('navbarul')));

  // safePremiumText :: {o} => s 
  const safePremiumText = accounts.SafeWhenPremium('Extend Membership', 'Upgrade to Premium');

  // safePremiumImg :: {o} => {p}
  const safePremiumImg = accounts.SafeWhenPremium({imgPremium: true}, {});

  // expiresWhen :: d => s
  const expiresWhen = (date) => {return {expires:moment(date).fromNow()};}

  // safeMembershipDuration :: {o} => n
  const safeMembershipDuration = (req) => (safePath(['premium', 'isactive'], req).getOrElse(false) == true) 
    ? safePath(['premium', 'activetodate'], req).map(expiresWhen).getOrElse({}) 
    : {};

  // mergePremium
  const mergePremium = curry((req, userdata, dbUser, data) => {

    // mergeWhenSelf :: f({a})({b}) => {o}
    const mergeWhenSelf = mergeWhenOwnProfile(req, userdata);

    // buildProfileObj :: {d} => {o}
    const buildProfileObj = compose(merge(safePremiumImg(userdata)), mergeWhenSelf({btnPurchaseGems: {text: "Buy Gems", class:"btn btn-default card-1 premium-purchase"}}), mergeWhenSelf({btnPurchaseMembership: {text: safePremiumText(userdata), class:"btn btn-success card-1 premium-purchase"}}), merge(safeGemCount(req, dbUser, userdata)), mergeWhenSelf(safeMembershipDuration(dbUser))); 
    
    return buildProfileObj(data);

  });

  expressApp.get(slashString('users'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res) {

    const render = curry((user, data, wtpdata, dbuser) => compose(
      renderData(res, 'users'), 
      merge(expressHelper.BuildNavbarHead('Profile')), 
      merge(maybeBuildNavs(req).getOrElse({})), 
      mergePremium(req, data, dbuser),
      prepareData(userProfile(data).getOrElse({}), accounts.BuildNavObject(user))
    )(wtpdata));

    // note user activity
    safePath(['user', 'email'], req).chain(accounts.TaskRecordActivity(account));

    // taskAccountFromId :: {r} => Task(Model(Account))
    const taskAccountFromId = (req) => (safePath(['query', 'id'], req).isJust) 
      ? maybeDefined(req.query.id).chain(accounts.TaskAccountFromSlug(account)) 
      : maybeSomething(getUserEmail(req), maybeDefined(req.query.email)).chain(accounts.TaskAccountFromEmail(account));

    taskAccountFromId(req).fork(
      err  => res.sendStatus(500),
      data => accounts.ReadAnAccountFromEmail(account, getUserEmail(req).getOrElse('')).then(render(req.user, data, []))
    )

  });
};
exports.RouteUser = routeUser;
