var mongo = require('../mongoconnect.js'),
    mongoose = mongo.Mongoose,
    Schema = mongoose.Schema;

const shortid = require('shortid');
const uuid = require('uuid');

var CityChat = new Schema({
    name: String,
    pageslug: {type: String, index: {unique: true}},
    key: {type: String, default: uuid.v4, index: {unique: true}},
    locname     : String,
    loc         : {
        type: {
            type: String,
            enum: ['Point', 'LineString', 'Polygon'],
            default : 'Point'
        }, 
        coordinates : [{
            type   : [Number]
        }] 
    },
    publicforums : [{
        thread: {
            useremail:  String,
            username:   String,
            avataricon: String,
            comment:    String,
            postdate:   Date,
            threadid:   {type: String, default: shortid.generate},
        },
        hash:       String
    }],
}, {
    timestamps: true
});

CityChat.index({loc: '2dsphere'});
module.exports = mongoose.model('CityChat', CityChat);
