var mongo = require('../mongoconnect.js');
var mongoose = mongo.Mongoose;
var Schema = mongoose.Schema;

var uuid = require('node-uuid');
var shortid = require('shortid');

var Achievement = new Schema({
  userkey     : {
    type    : String,
    index   : 1
  },
  name        : String,
  description : String,
  setname     : {
    type    : String,
    index   : 1
  },
  image       : String,
  public      : {
    type    : Boolean,
    default : true,
    index   : 1
  },
  achieved  : {
    type    : Boolean,
    default : true,
    index   : 1
  },
  showcase  : Boolean,
  data        : {},
  datetime    : [Date]
},
{
  timestamps: true
});

Achievement.index({userkey: 1});

module.exports = mongoose.model('Achievement', Achievement);
