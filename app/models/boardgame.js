var mongo = require('../mongoconnect.js'),
  mongoose = mongo.Mongoose,
  Schema = mongoose.Schema;

var Boardgame = new Schema({
  id          : Number, 
  name        : String,
  type        : String,
  description : String,
  thumbnail   : String,
  yearpublished: Number,
  subtype     : String,
  objecttype  : String,
  active      : Boolean,
  activestate : String,
  minplaytime : Number,
  maxplaytime : Number,
  minplayers  : Number,
  maxplayers  : Number,
  playercounts : [{
    playercount:  String,
    status:       String
  }],
  bggrank     : String,
  avgrating   : Number,
  weight      : Number,
  family      : [String],
  categories  : [String],
  mechanics   : [String],
  expansions  : [{value: String, id: Number}],
  artists     : [String],
  publishers  : [String],
  designers   : [String],
  implementation : [{value: String, id: Number}],
  wanttoplay  : {type: Number, default: 0},
  own         : {type: Number, default: 0},
  teach       : {type: Number, default: 0},
  matchcount  : {type: Number, default: 0},
  avoid       : {type: Number, default: 0},
  active      : Boolean,
  activestatus : String,
  updatecache : {type: Boolean, default: false},
  bggcache    : Schema.Types.Mixed,
  link        : Schema.Types.Mixed,
  videos      : [{
    id: Number,
    title: String,
    category: String,
    language: String,
    link: String,
    username: String,
    userid: Number,
    postdate: Date 
  }],
  asin        : String,
  confirmasin : {type: Boolean, default: false},
  amazon: [{
    domain  : String,
    lastupdated: Date,
    rrp:        Number,
    saleprice:  Number,
    formatrrp:  String,
    formatsaleprice: String,
    currency:   String,
    title   :   String,
    pageUrl :   String,
    offerListingId: String,
    quantity:   Number,
    asin    :   String 
  }],
  updatecounter:  Number
},
{
  timestamps: true
});

Boardgame.index({id: 1}, {unique: true});
Boardgame.index({name: 1});
Boardgame.index({type: 1});
Boardgame.index({name: 1, yearpublished: 1}, {unique: true});
Boardgame.index({name: 'text', description: 'text'});

module.exports = mongoose.model('Boardgame', Boardgame);
