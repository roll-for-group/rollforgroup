var mongo = require('./../mongoconnect.js'),
    mongoose = mongo.Mongoose,
    Schema = mongoose.Schema,
    uuid = require('node-uuid');

var Transaction = new Schema({
    purchaser   : {type: String, index: 1},
    to          : {type: String, index: 1},
    item        : String,
    tickets     : Number,
    payment     : {
        method:     String,
        currency:   String,
        credit:     Number,
        debit:      Number,
        reference:  String
    },
    promotion   : String,
    coupon      : String
},
{
    timestamps: true
});

module.exports = mongoose.model('Transaction', Transaction);
