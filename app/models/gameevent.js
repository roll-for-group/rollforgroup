var mongo = require('../mongoconnect.js'),
    mongoose = mongo.Mongoose,
    Schema = mongoose.Schema;

const shortid = require('shortid');
const uuid = require('uuid');

var GameEvent = new Schema({
    name: String,
    pageslug: {type: String, index: {unique: true}},
    key: {type: String, default: uuid.v4, index: {unique: true}},
    users : [{
        email       : String,
        type        : {
            type: String,
            enum: ['admin', 'player']
        },
        avataricon  : String,
        playerkey   : String,
        name        : String
    }],
    games : [{
        id          : Number,
        name        : String,
        thumbnail   : String,
        description : String,
        minplaytime : Number,
        maxplaytime : Number,
        minplayers  : Number,
        maxplayers  : Number,
        status  : {
            type: String,
            enum: ['inuse', 'returned'],
            default: 'returned'
        }
    }],
    bookings: [{
      gameid      : Number,
      email       : String,
      date        : Date,
      timeend     : Date,
      bookingref  : String,
      code: {type: String, default: shortid.generate},
      status  : {
        type: String,
        enum: ['waiting', 'inuse', 'returned'],
        default: 'waiting'
      },
    }],
    locname     : String,
    loc         : {
        type: {
            type: String,
            enum: ['Point', 'LineString', 'Polygon'],
            default : 'Point'
        }, 
        coordinates : [{
            type   : [Number]
        }] 
    },
    impressioncount: {type: Number, default: 0},
    viewcount:  {type: Number, default: 0}
}, {
    timestamps: true
});

GameEvent.index({loc: '2dsphere'});
module.exports = mongoose.model('Gameevent', GameEvent);
