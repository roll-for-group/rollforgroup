var mongo = require('./../mongoconnect.js'),
    mongoose = mongo.Mongoose,
    Schema = mongoose.Schema;

var Feedback = new Schema({
    useremail   : {type: String, index: 1},
    likes       : String,
    dislikes    : String,
    suggestion  : String,
    webpage     : String
},
{
    timestamps: true
});

module.exports = mongoose.model('Feedback', Feedback);
