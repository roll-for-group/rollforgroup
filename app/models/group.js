var mongo = require('../mongoconnect.js'),
    mongoose = mongo.Mongoose,
    Schema = mongoose.Schema;

const uuid = require('uuid');
const shortid = require('shortid');

var Group = new Schema({
  url           : {type: String, default: shortid.generate, index: {unique: true}},
  name          : String,
  description   : String,
  locname     : String,
  locarea     : String,
  loccountry  : String,
  loc         : {
    type      : {
      type    : String,
      enum    : ['Point', 'LineString', 'Polygon'],
      default : 'Point'
    }, 
    coordinates : [{
      type    : [Number]
    }] 
  },
  isprivate   : {type: Boolean, default: false},
  autoaccept  : {type: Boolean, default: false},
  status        : String,
  members       : [{
		email     : String,
    pageslug  : String,
    status    : {
      type  : String, 
      enum  : ['host', 'invite', 'request', 'interseted',  'approve', 'ban'],
      default: 'reqeust'
    },
    avataricon: String,
    memberkey : {type: String, default: shortid.generate, index: 1},
    name      : String
	}],
  key         : {type: String, default: uuid.v4, index: 1}
},
{
  timestamps: true
});

Group.index({loc: '2dsphere'});
module.exports = mongoose.model('Group', Group);
