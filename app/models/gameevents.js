var Promise = require('bluebird');
var R = require('ramda');

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task');

var bgg     = require('../bgg.js');
var util    = require('util');
var log     = require('../log.js');

const chain       = R.chain,
    compose     = R.compose,
    curry       = R.curry,
    dissoc      = R.dissoc,
    filter      = R.filter,
    find        = R.find,
    head        = R.head,
    isEmpty     = R.isEmpty,
    isNil       = R.isNil,
    map         = R.map,
    merge       = R.merge,
    prop        = R.prop,
    propEq      = R.propEq,
    sortBy      = R.sortBy,
    zipObj      = R.zipObj;

var moment = require('moment');

const wipeSchema = R.compose(JSON.parse, JSON.stringify);

// safeProp :: (a -> b) => Maybe(b[a])
const safeProp = curry((x, o) => (isEmpty(o) || isNil(o[x])) ? Maybe.Nothing() : Maybe.of(o[x]));


// TaskCreateEvent :: (Model(Event) -> {e}) => Schema({e})
const taskCreateEvent = function(gameEvent, obj) {

  // {o} => {p}
  return new Task((reject, resolve) => {
    var newEvent = new gameEvent(obj);
    newEvent.save().then(resolve).catch(reject);
  });

};
exports.TaskCreateEvent = taskCreateEvent;


// TaskReadEvent :: (Model(Event) -> s) => [Schema{e}]
const taskReadEvent = curry(function(gameEvent, key) {
    return new Task((reject, resolve) => {
        gameEvent.findOne({key: key}).exec().then(resolve);
    });
});
exports.TaskReadEvent = taskReadEvent;

// TaskDeleteEventBookingsBySlug :: (Model(Event) -> s -> c) => [Schema{e}]
const taskDeleteEventBookingsBySlug = curry(function(gameEvent, email, gameslug, code) {

  const safeFind = curry((a, b) => (find(a, b) == undefined) ?  Maybe.Nothing() : Maybe.of(find(a, b)));

  // hasAdminUser :: {o} -> Maybe({u})
  const hasAdminUser = compose(chain(safeFind(propEq('email', email))), safeProp('users'));

  // findBooking :: {o} -> s 
  const findBooking = compose(prop('email'), find(propEq('code', code)), prop('bookings'));

  return new Task((reject, resolve) => {
    taskReadEventBySlug(gameEvent, gameslug).fork(
      err => reject,
      event => {
        if (hasAdminUser(event).isJust) {
          gameEvent.update({pageslug: gameslug}, {$pull: {bookings:{code: code}}}, {new: true}).exec().then(resolve)
        } else {
          (findBooking(event) == email) ? gameEvent.update({pageslug: gameslug}, {$pull: {bookings:{code: code, email: email}}}, {new: true}).exec().then(resolve) : reject('Unauthorised')
        } 
      }
    )
  });

});
exports.TaskDeleteEventBookingsBySlug = taskDeleteEventBookingsBySlug;


// TaskReadEventBySlug :: (Model(Event) -> s) => [Schema{e}]
const taskReadEventBySlug = curry(function(gameEvent, slug) {
  return new Task((reject, resolve) => {
    gameEvent.findOne({pageslug: slug}).exec().then(resolve);
  });
});
exports.TaskReadEventBySlug = taskReadEventBySlug;

// TaskReadEvents :: (Model(Event)) => [Schema{e}]
const taskReadEvents = function(gameEvent) {
    return new Task((reject, resolve) => {
        gameEvent.find({}).exec().then(resolve).catch(reject);
    });
};
exports.TaskReadEvents = taskReadEvents;


// TaskBookGameBySlug :: (Model(Event) -> k ->  e -> id -> df -> dt -> r ) => (Schema{e});
const taskBookGameBySlug = R.curry(function(gameEvent, slug, useremail, gameid, bookfrom, bookto, reference) {

    // zipBooking[field] => {o}
  const zipBooking =  R.compose(R.objOf('bookings'), zipObj(['gameid', 'email', 'date', 'timeend', 'bookingref']));

    return new Task((reject, resolve) => {
        taskVerifyBookingTimesBySlug(gameEvent, slug, gameid, bookfrom, bookto).fork(
            reject,
            data => {

                if (data) {
                    gameEvent.findOneAndUpdate({pageslug: slug}, {$push: zipBooking([gameid, useremail, bookfrom, bookto, reference ])}, {new: true}).exec().then(function(ge) {
                        resolve(ge); 
                    }).catch(reject);
                } else {
                    reject('Double Booked')
                }

            }
        )
    });
});
exports.TaskBookGameBySlug = taskBookGameBySlug;


// TaskBookGame :: (Model(Event) -> k ->  e -> id -> df -> dt -> r ) => (Schema{e});
const taskBookGame = R.curry(function(gameEvent, key, useremail, gameid, bookfrom, bookto, reference) {

    // zipBooking[field] => {o}
    const zipBooking =  R.compose(R.objOf('bookings'), zipObj(['gameid', 'email', 'date', 'timeend', 'bookingref']));

    return new Task((reject, resolve) => {
        taskVerifyBookingTimes(gameEvent, key, gameid, bookfrom, bookto).fork(
            reject,
            data => {

                if (data) {
                    gameEvent.findOneAndUpdate({key: key}, {$push: zipBooking([gameid, useremail, bookfrom, bookto, reference ])}, {new: true}).exec().then(function(ge) {
                        resolve(ge); 
                    }).catch(reject);
                } else {
                    reject('Double Booked')
                }

            }
        )
    });
});
exports.TaskBookGame = taskBookGame;

// TaskReadEventBookings :: (Model(Event) -> k -> id) => [Schema{e}]
const taskReadEventBookings = curry(function(gameEvent, key, gameid) {

    const filterId = R.compose(R.objOf('bookings'), filter(R.propEq('gameid', Number(gameid))), prop('bookings'));

    const findGame = R.compose(R.objOf('game'), find(R.propEq('id', Number(gameid))), prop('games'))

    return new Task((reject, resolve) => {
        gameEvent.findOne({key: key}, {bookings: 1, games: 1, users: 1}).exec().then(function(x) {
            (x == null) ? reject('no record') : resolve(merge(filterId(x), findGame(x)));
        }).catch(reject);
    });

});
exports.TaskReadEventBookings = taskReadEventBookings;


// TaskReadEventBookingsBySlug :: (Model(Event) -> s -> id) => [Schema{e}]
const taskReadEventBookingsBySlug = curry(function(gameEvent, eventslug, gameid) {

    const filterId = R.compose(R.objOf('bookings'), sortBy(prop('date')), filter(R.propEq('gameid', Number(gameid))), prop('bookings'));

    const findGame = R.compose(R.objOf('game'), find(R.propEq('id', Number(gameid))), prop('games'))

    const addUsers = R.compose(R.objOf('users'), prop('users'));

    return new Task((reject, resolve) => {
        gameEvent.findOne({pageslug: eventslug}, {bookings: 1, games: 1, users: 1}).exec().then(function(x) {
            (x == null) ? reject('no record') : resolve(merge(filterId(x), merge(addUsers(x), findGame(x))));
        }).catch(reject);
    });

});
exports.TaskReadEventBookingsBySlug = taskReadEventBookingsBySlug;


// TaskReadEventBookingsPlayerBySlug :: (Model(Event) -> s -> id) => [Schema{e}]
const taskReadEventPlayerBookingsBySlug = curry(function(gameEvent, eventslug, useremail) {

    // findGame ([g] -> i) => {g}
    const findGame = curry((games, gameid) => compose(find(R.propEq('id', Number(gameid))))(games))

    // transGame ([g] -> {o}) => {p}
    const transGame = curry((games, x) => compose(dissoc('_id'), merge(x), dissoc('_id'), findGame(games), prop('gameid'))(x));

    // pullGames {o} => {p}
    const pullGames = (x) => R.compose(R.objOf('bookings'), sortBy(prop('date')), map(transGame(prop('games', x))), filter(R.propEq('email', useremail)), prop('bookings'))(x)

    return new Task((reject, resolve) => {
        gameEvent.findOne({pageslug: eventslug}, {bookings: 1, games: 1}).exec().then(function(x) {
            (x == null) ? reject('no record') : resolve(pullGames(wipeSchema(x)));
        }).catch(reject);
    });

});
exports.TaskReadEventPlayerBookingsBySlug = taskReadEventPlayerBookingsBySlug;


const isTimeBlocked = curry((from, to, f2, t2) => (((t2 > from) && (f2 > to)) || ((to > f2) && (from > t2))));
exports.IsTimeBlocked = isTimeBlocked;


// TaskVerifyBookingTimes :: (Model(Event) -> k -> id -> f -> t) => [Schema{e}]
const taskVerifyBookingTimes = curry(function(gameEvent, key, gameid, from, to) {

    const propFrom  = prop('date');
    const propTo    = prop('timeend');
    const checkObjTime = (obj) => isTimeBlocked(moment(from).format('x'), moment(to).format('x'), moment(propFrom(obj)).format('x'), moment(propTo(obj)).format('x'));

    const appender = (a, b) => [R.and(a, b), R.and(a, b)];
    const accumBlocked = compose(head, R.mapAccum(appender, true), map(checkObjTime), prop('bookings'))

    return new Task((reject, resolve) => {

        if (to < from) {
            reject('To time must be after form time')
        } else {
            taskReadEventBookings(gameEvent, key, gameid).fork(
                reject,
                data => {
                    resolve(accumBlocked(data)); 
                }
            );
        }
    });

});
exports.TaskVerifyBookingTimes = taskVerifyBookingTimes;


// TaskVerifyBookingTimesBySlug :: (Model(Event) -> k -> id -> f -> t) => [Schema{e}]
const taskVerifyBookingTimesBySlug = curry(function(gameEvent, slug, gameid, from, to) {

    const propFrom  = prop('date');
    const propTo    = prop('timeend');

    const checkObjTime = (obj) => isTimeBlocked(moment(from).format('x'), moment(to).format('x'), moment(propFrom(obj)).format('x'), moment(propTo(obj)).format('x'));

    const appender = (a, b) => [R.and(a, b), R.and(a, b)];
    const accumBlocked = compose(head, R.mapAccum(appender, true), map(checkObjTime), prop('bookings'))

    return new Task((reject, resolve) => {

        if (to < from) {
            reject('To time must be after form time')
        } else {
            taskReadEventBookingsBySlug(gameEvent, slug, gameid).fork(
                reject,
                data => {
                    resolve(accumBlocked(data)); 
                }
            );
        }
    });

});
exports.TaskVerifyBookingTimesBySlug = taskVerifyBookingTimesBySlug;


// TaskIsUserAdmin :: (Model(Event) -> s -> e) => b 
const taskIsUserAdmin = curry(function(gameEvent, slug, user) {

    const safeFind = curry((a, b) => (find(a, b) == undefined) ?  Maybe.Nothing() : Maybe.of(find(a, b)));

    // hasAdminUser :: {o} -> Maybe({u})
    const hasAdminUser = compose(chain(safeFind(propEq('email', user))), safeProp('users'));

    return new Task ((reject, resolve) => {
        taskReadEventBySlug(gameEvent, slug).fork(
            reject,
            event => {
                resolve(hasAdminUser(event).isJust);
            }
        );
    });
});
exports.TaskIsUserAdmin = taskIsUserAdmin;



// TaskBookingCheckOut :: (Model(Event) -> e -> s -> c -> b) => [Schema{e}]
const taskBookingCheckOut = curry(function(gameEvent, email, gameslug, gameid, code, newstatus) {

    const safeFind = curry((a, b) => (find(a, b) == undefined) ?  Maybe.Nothing() : Maybe.of(find(a, b)));

    // hasAdminUser :: {o} -> Maybe({u})
    const hasAdminUser = compose(chain(safeFind(propEq('email', email))), safeProp('users'));

    // findBooking :: {o} -> s 
    const findBooking = compose(prop('email'), find(propEq('code', code)), prop('bookings'));

    return new Task((reject, resolve) => {
        taskReadEventBySlug(gameEvent, gameslug).fork(
            err => reject,
            event => {

                if (hasAdminUser(event).isJust) {
                    gameEvent.findOneAndUpdate({pageslug: gameslug, 'bookings.code': code}, {$set:{'bookings.$.status': newstatus}}, {new: true}).exec().then(function(x) {
                        gameEvent.findOneAndUpdate({pageslug: gameslug, 'games.id': Number(gameid)}, {$set:{'games.$.status': newstatus}}, {new: true}).exec().then(function(y) {
                            resolve(y);
                            
                        }).catch((err)  => { 
                            reject('Could not Update');
                        })

                    }).catch(err => {
                        reject('Could not Update')
                    });
                } else {
                    reject('Unauthorised')
                } 

            }
        )
    });

});
exports.TaskBookingCheckOut = taskBookingCheckOut;



