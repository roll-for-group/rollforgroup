var Promise = require('bluebird');
var R = require('ramda');

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task');

var request = require('ajax-request');
var log     = require('../log.js');
var S = require('../lambda.js');

var express = require('express');
var bodyParser = require('body-parser')

var exports = module.exports = {};
var moment = require('moment');

var account = require('../models/account.js');
var accounts = require('../models/accounts.js');

var achievement = require('../models/achievement.js');

// maybeGetQuery :: o -> o
const maybeGetQuery = (req) =>
  S.path(['params', 'userkey'], req) 
    .map(R.objOf('userkey'))
    .map(R.merge({public: true, achieved: true}));
exports.maybeGetQuery = maybeGetQuery;

// maybeGetShowcase :: o -> o
const maybeGetShowcase = (req) =>
  S.path(['params', 'userkey'], req) 
    .map(R.objOf('userkey'))
    .map(R.merge({public: true, achieved: true, showcase: true}));
exports.maybeGetShowcase = maybeGetShowcase;

// maybeGetInProgress :: o -> o
const maybeGetInProgress = (req) =>
  S.path(['params', 'userkey'], req) 
    .map(R.objOf('userkey'))
    .map(R.merge({public: true, achieved: false}));
exports.maybeGetInProgress = maybeGetInProgress;


// setupAccountsPostListener :: express 
var setupRouter = function(router) {

  // var router = express.Router();
  var jsonParser = bodyParser.json();

  router.route('/:userkey/achievements')
    .get(function(req, res) {

      if (S.path(['query', 'inprogress'], req).getOrElse(false)) {
        achievement
          .find(maybeGetInProgress(req).getOrElse({userkey:'a'}))
          .exec()
          .then((data) => res.json(data));

      } else {
        if (S.path(['query', 'showcase'], req).getOrElse(false)) {
          achievement
            .find(maybeGetShowcase(req).getOrElse({userkey:'a'}))
            .exec()
            .then((data) => res.json(data));

        } else {
          achievement
            .find(maybeGetQuery(req).getOrElse({userkey:'a'}))
            .exec()
            .then((data) => res.json(data));

        }
      }

    })

  return router;

}; 
exports.SetupRouter = setupRouter;
