var Promise = require('bluebird');
var R = require('ramda');

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task');

var bgg     = require('../bgg.js');
var util    = require('util');
var log     = require('../log.js');

