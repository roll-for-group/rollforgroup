var mongo = require('../mongoconnect.js'),
    mongoose = mongo.Mongoose,
    Schema = mongoose.Schema;
 
const uuid = require('uuid');
const shortid = require('shortid');

var Conversation = new Schema({
    key         : {type: String, default: uuid.v4, index: {unique: true}},
    name        : String,
    topic       : String,
    type        : String,
    users       : [{
        email       : String,
        userslug    : String 
    }],
    chatlog     : [{
        chatid  : {type: String, default: shortid.generate, index: {unique: true}},
        from    : String,
        text    : String,
        created : Date,
    }]
});

module.exports = mongoose.model('Conversation', Conversation);
