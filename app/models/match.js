var mongo = require('../mongoconnect.js'),
    mongoose = mongo.Mongoose,
    Schema = mongoose.Schema;

const shortid = require('shortid');

var Match =  new Schema({
  key         : {
    type: String, 
    default: shortid.generate, 
    index: 1
  },
  games       : [{
    id        : Number,
    name      : String,
    thumbnail : String,
    pageslug  : {type: String, default: shortid.generate},
    status    : {
      type    : String,
      enum    : ['autosuggest', 'playersuggest', 'select'],
      default : 'select' 
    },
    playorder : Number,
    vote      : [{
      email     : String, 
      userkey   : String, 
      username  : String, 
      avataricon  : String, 
      votetype  : {type: String, default: 'wtpplay'}, 
      direction : {type: Number, default: 1},
    }],
    join      : [{
      email     : String, 
      userkey   : String, 
      username  : String, 
      avataricon  : String, 
      jointype  : {type: String, default: 'wtpplay'}, 
      direction : {type: Number, default: 1},
    }],
    wanttoplay: [{email: String}],
    wanttobuy : [{email: String}],
    description: String,
    minplaytime: Number,
    maxplaytime: Number,
    minplayers: Number,
    maxplayers: Number,
    weight    : Number,
    bggrank   : String,
    avgrating : Number,
    playercounts : [{
      playercount:  String,
      status:       String
    }],
    username  : String,
    avataricon: String,
    email     : String,
    expansions: [{
      id:     Number,
      value:  String
    }],
    gamekey   : {type: String, default: shortid.generate}
  }],
  assignedexpansions : [{
    id        : Number,
    name      : String,
    thumbnail : String,
    pageslug      : {
      type: String, 
      default: shortid.generate
    },
    gamepageslug  : String,
    userpageslug  : String,
    description: String,
    minplaytime: Number,
    maxplaytime: Number,
    minplayers: Number,
    maxplayers: Number,
    weight    : Number,
    bggrank   : String,
    avgrating : Number,
    playercounts : [{
      playercount:  String,
      status:       String
    }],
    email     : String,
    userkey   : String, 
  }],
  venuename   : String,
  locname     : String,
  locarea     : String,
  loccountry  : String,
  title       : String,
  description : String,
  loc         : {
    type      : {
      type    : String,
      enum    : ['Point', 'LineString', 'Polygon']
    }, 
    coordinates : [{
      type    : [Number]
    }] 
  },
  tableref    : String,
  eventid     : String,
  impressioncount: {type: Number, default: 0},
  viewcount   : {type: Number, default: 0},
  date        : Date,
  timestart   : String,
  timeend     : Date,
  activeTimeslotKey : String,
  timeslots: [{
    id: {
      type: String,
      default: shortid.generate
    },
    startdatetime : Date,
    enddatetime   : Date,
    votes   : [{
      email       : String, 
      username    : String, 
      avataricon  : String, 
      userkey     : String, 
      votetype    : {type: String, default: 'available'}, 
      direction   : {type: Number, default: 1},
    }],
  }],
  reoccur     : {
    enable    : Boolean,
    end       : Date,
    interval  : {
      type    : String,
      enum    : [
        'never', 'day', 'week', 'month', 'year', 'dayOfWeek', 'dayOfMonth', 'weekOfMonth', 'weekOfYear', 'monthOfYear', 'weekOfMonthByDay'
      ]
    },
    every     : Number
  },
  seriesid    : {
    type: String, 
    default: shortid.generate, 
    index: true 
  },
  status      : {
    type      : String, 
    enum      : ['draft', 'open', 'full', 'inprogress', 'postgame', 'played', 'expired', 'closed'], 
    default   : 'draft'
  },
  isprivate   : {type: Boolean, default: false},
  isfull      : {type: Boolean, default: false},
  seatmin     : Number,
  seatmax     : Number,
  seatavailable : Number,
	autoaccept: {
		type: Boolean,
		default: false
	},
  players     : [{
    email     : String,
    type      : {
      type    : String,
      enum    : ['host', 'standby', 'invite', 'request', 'interseted',  'approve', 'decline', 'reject', 'complete', 'absent', 'review']
    },
    avataricon: String,
    playerkey : String,
    name      : String,
    extraplayers : {type: Number, default: 0}
  }],
  price       : {type: Number, default: 0},
  currency    : {type: String, default: 'AUD'},
  smoking     : {type: Number, default: 1},
  alcohol     : {type: Number, default: 1},
  experience  : {type: Number, default: 1},
  forumthread : [{
    thread: {
      useremail:  String,
      username:   String,
      avataricon: String,
      comment:    String,
      postdate: 	{type: Date, default: Date.now}
    },
    hash      :   String
  }],
  publicforums: [{
    thread    : {
      useremail:  String,
      username: String,
      avataricon: String,
      comment : String,
      postdate: {type: Date, default: Date.now}
    },
    hash      : String
  }],
  utcoffset   : Number,
  timezone    : String
}, {
  timestamps  : true
});

Match.pre('save', function (next) {
  if (this.isNew && Array.isArray(this.loc.coordinates) && 0 === this.loc.coordinates.length) {
    this.loc = undefined;
  }
  next();
})

Match.index({loc: '2dsphere'});
module.exports = mongoose.model('Match', Match);
