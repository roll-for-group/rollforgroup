var Promise = require('bluebird');
var bgg = require('../bgg.js');
var util = require('util');
var R = require('ramda');
var compose = R.compose,
    curry   = R.curry
    zipObj  = R.zipObj;
var log = require('../log.js');

// lambdas
const newMongObj = curry((mngObj, jsonObj) => new mngObj(jsonObj).save());
const paymentObj = (type) => zipObj(['method', 'currency', type, 'reference']); 


// Purchase :: Schema -> String -> String -> Number -> String -> String -> Number -> Number -> String -> String -String -> Promise(Schema)
const purchase = R.curry(function(transSchema, purchaser, item, tickets, paymethod, paycurrency, payamount, payreference, promotion, coupon) {

    const creditObj = paymentObj('credit'); 
    const transactionObj = compose(newMongObj(transSchema), zipObj(['purchaser', 'to', 'item', 'tickets', 'payment', 'promotion', 'coupon']));

    return new Promise(function(fulfill, reject) {

        fulfill(transactionObj([purchaser, purchaser, item, tickets, creditObj([paymethod, paycurrency, payamount, payreference]), promotion, coupon]));

    });
});
exports.Purchase = purchase;

// Refund :: Schema -> String -> String -> Number -> String -> String -> Number -> Number -> String -> String -String -> Promise(Schema)
const refund = R.curry(function(transSchema, purchaser, item, tickets, paymethod, paycurrency, payamount, payreference, promotion, coupon) {

    const debitObj = paymentObj('debit'); 
    const transactionObj = compose(newMongObj(transSchema), zipObj(['purchaser', 'to', 'item', 'tickets', 'payment', 'promotion', 'coupon']));

    return new Promise(function(fulfill, reject) {

        fulfill(transactionObj([purchaser, purchaser, item, tickets, debitObj([paymethod, paycurrency, payamount, payreference]), promotion, coupon]));

    });
});
exports.Refund = refund;
