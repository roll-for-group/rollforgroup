var Promise = require('bluebird');
var R         = require('ramda'); 
var chain     = R.chain;
var compose   = R.compose; 
var curry     = R.curry; 
var dec       = R.dec;
var head      = R.head;
var is        = R.is;
var isEmpty   = R.isEmpty;
var isNil     = R.isNil;
var lift      = R.lift;
var lte       = R.lte;
var map       = R.map;
var merge     = R.merge;
var prop      = R.prop;
var propEq    = R.propEq;
var sequence  = R.sequence;

var maybe     = require('data.maybe');
var Task      = require('data.task');
const { Success, Failure, collect } = require('folktale/validation');

var log       = require('../log.js');
var schemaMatch = require('../models/match.js');

var S         = require('../lambda.js');

var moment    = require('moment');
require('moment-recur');

var emailer   = require('../emailer.js');
var rp        = require('request-promise');

var account   = require('../models/account.js');
var accounts  = require('../models/accounts.js');

var aguid     = require('aguid');
var geolib    = require('geolib');

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

var currencyFormatter = require('currency-formatter');

// scrub :: obj -> obj;
var scrub = R.compose(
  R.dissoc('_id'), 
  R.dissoc('__v'), 
  JSON.parse, 
  JSON.stringify
);

// liftEquals :: maybe a -> maybe b -> maybe bool
const liftEquals = lift(R.equals);

// liftSubtract :: maybe n -> maybe n -> maybe n 
const liftSubtract = lift(R.subtract);

// maybeInt :: a -> n
const maybeInt = (x) => (parseInt(x) === NaN)
  ? maybe.Nothing()
  : maybe.of(parseInt(x));

// safePropEq :: s -> a -> o -> maybe b
const safePropEq = curry((x, v, o) => 
  (isEmpty(o) || isNil(o) || isNil(o[x])) 
    ? maybe.Nothing() 
    : maybe.of(propEq(x, v, o)));

// hasUser :: (s, {players:[{email: s}]}) -> bool 
const hasUser = (email, match) => 
  R.compose(
    R.contains(email), 
    R.map(R.toLower), 
    R.map(R.prop('email')), 
    R.prop('players')
  )(match);

// whenMatchHasUser :: o -> s -> bool
const whenMatchHasUser = R.curry((match, playerkey) => R.compose(
  R.contains(playerkey), 
  R.map(R.prop('playerkey')), 
  R.prop('players')
)(match));

// whenGameIsFull :: {seatavailable: n} -> bool
const whenGameIsFull = compose(lte(R.__, 0), prop('seatavailable'));

// safeEmail :: [{email:s}] -> maybe s
const safeEmail = (user) => 
  S.head(user)
    .chain(S.prop('email'));

// safeSpamUser :: [{email:s}] -> maybe s
const safeSpamUser = (user) => 
  S.head(user)
    .chain(S.prop('spamuser'))
    .getOrElse(false);

// maybeAvatar :: [{profile:{avataricon:s}}] -> maybe s 
const maybeAvatar = (user) => 
  S.head(user)
    .chain(S.path(['profile', 'avataricon']))
    .getOrElse('/img/default-avatar-sq.jpg');
exports.MaybeAvatar = maybeAvatar;

// maybeName :: [{profile:{name:s}}] -> s 
const maybeName = (user) => 
  S.head(user)
    .chain(S.path(['profile', 'name']))
    .getOrElse('Unknown Name');
exports.MaybeName = maybeName;


// maybeFirst :: (maybe a -> maybe a) -> maybe a
const maybeFirst = (maybeX, maybeY) => 
  (maybeX.isJust) 
    ? maybeX 
    : maybeY;
exports.MaybeFirst = maybeFirst;

// maybeData :: {s:a} -> {s:a} -> s -> maybe a 
const maybeData = R.curry((updateObj, data, key) => 
  maybeFirst(S.prop(key, updateObj), S.prop(key, data)));
exports.MaybeData = maybeData;

// playerAndFriendCount :: o -> 
const playerAndFriendCount = (o) => S
  .prop('extraplayers', o)
  .map(R.add(1))
  .getOrElse(1);
exports.playerAndFriendCount = playerAndFriendCount;

// countPlayers :: {players:[{type: s}]} -> maybe n
const countPlayers = (match) => {
  return compose(
    map(R.sum),
    map(map(playerAndFriendCount)),
    map(R.filter(R.converge(R.or, [R.propEq('type', 'host'), R.propEq('type', 'approve')]))), 
    S.prop('players')
  )(match);
};
exports.CountPlayers = countPlayers;

// createHostUser :: s -> s -> s -> {email:s, avataricon:s, name:s}
const createHostUser = curry((avatar, name, email) => 
  R.zipObj(['email', 'avataricon', 'name'], [email, avatar, name]));
exports.CreateHostUser = createHostUser;

// whenFull :: o -> bool -> o 
const whenFull = R.curry((updateObj, a) => merge(updateObj, {isfull: (a)}));
exports.WhenFull = whenFull;

// findHost :: {m} => maybe e
const findHostUser = R.compose(
	R.chain(S.find(R.propEq('type', 'host'))), 
	S.prop('players')
);
exports.findHostUser = findHostUser;


// testHostUser :: s -> {o} -> bool
const testHostUser = R.curry((hostemail, players) => {
  const findHost = R.compose(
    R.chain(S.equals(hostemail)), 
    R.chain(S.prop('email')), 
		findHostUser
  )
  return findHost(players).isJust;
});
exports.testHostUser = testHostUser

// checkFull : o -> o -> o 
const checkFull = (data, updateObj) => {
  return merge(liftSubtract(
    S.prop('seatmax', updateObj), countPlayers(data))
      .map((seat) => merge(updateObj, {seatavailable:seat}))
      .getOrElse(updateObj), 
    liftEquals(countPlayers(data), S.prop('seatmax', updateObj).chain(maybeInt))
      .map(whenFull(updateObj))
      .getOrElse(updateObj)
  );
};
exports.CheckFull = checkFull;

// checkSpamTitle :: o -> bool
var checkSpamTitle = (matchObj) => {
  return S.prop('title', matchObj)
    .map(R.test(/钱/))
    .getOrElse(false);
}
exports.checkSpamTitle = checkSpamTitle;

// checkSpamGooglePlay :: o -> bool
var checkSpamGooglePlay = (matchObj) => {
  return S.prop('description', matchObj)
    .map(R.test(/play.google.com/))
    .getOrElse(false);
}
exports.checkSpamGooglePlay = checkSpamGooglePlay;

// checkSpamAppleStore :: o -> bool
var checkSpamAppleStore = (matchObj) => {
  return S.prop('description', matchObj)
    .map(R.test(/itunes.apple.com/))
    .getOrElse(false);
}
exports.checkSpamAppleStore = checkSpamAppleStore;

// findMatches :: s -> Promise [ o ]
const findMatches = (searchquery) => schemaMatch 
  .find(searchquery)
  .sort({date:-1})
  .limit(1000)
  .exec();

// findMatchesRecentFirst :: s -> Promise [ o ]
const findMatchesRecentFirst = (searchquery) => schemaMatch 
  .find(searchquery)
  .sort({date:1})
  .limit(1000)
  .exec();



const REGEX_JAPANESE = /[\u3000-\u303f]|[\u3040-\u309f]|[\u30a0-\u30ff]|[\uff00-\uff9f]|[\u4e00-\u9faf]|[\u3400-\u4dbf]/;

// checkSpamJapaneseCharacters
var checkSpamJapaneseCharacters = (matchObj) => {
  return S.prop('description', matchObj)
    .map(R.test(REGEX_JAPANESE))
    .getOrElse(false);
}
exports.checkSpamJapaneseCharacters = checkSpamJapaneseCharacters;


const REGEX_CHINESE = /[\u4e00-\u9fff]|[\u3400-\u4dbf]|[\u{20000}-\u{2a6df}]|[\u{2a700}-\u{2b73f}]|[\u{2b740}-\u{2b81f}]|[\u{2b820}-\u{2ceaf}]|[\uf900-\ufaff]|[\u3300-\u33ff]|[\ufe30-\ufe4f]|[\uf900-\ufaff]|[\u{2f800}-\u{2fa1f}]/u;

// checkSpamChineseCharacters
var checkSpamChineseCharacters = (matchObj) => {
  return S.prop('description', matchObj)
    .map(R.test(REGEX_CHINESE))
    .getOrElse(false);
}
exports.checkSpamChineseCharacters = checkSpamChineseCharacters;

// hasTimeslots :: o -> maybe o
const hasTimeslots = R.compose(
	R.chain(S.head),
	S.prop('timeslots')
);
exports.hasTimeslots = hasTimeslots;


// hostMatch :: o -> a -> s -> s -> o -> Promise o 
var hostMatch = R.curry(function(matchSchema, accountSchema, hostemail, userkey, matchObj) {

  return new Promise (function(fulfill, reject) {

    // taskFail :: task o 
    var taskFail = new Task((reject, resolve) => 
      reject({
        code: 401, 
        description: "Host " + hostemail + " does not exist"
      }));

    // look up boardgame and add detail
    accounts
      .ReadVerifyAccountFromEmail(accountSchema, hostemail, userkey)
      .then(function(host) {

        // hostUser :: maybe s -> maybe {email:s}
        const hostUser = R.lift(createHostUser(maybeAvatar(host), maybeName(host)));

        // createHostMatch :: o -> task o
        const createHostMatch = (hostObj) => new Task((reject, resolve) =>
          createMatch(matchSchema, hostObj, S.pathEq(['reoccur', 'enable'], true, matchObj).isJust, matchObj)
            .then(resolve)
            .catch(reject));

        if (safeSpamUser(host)) {
          reject(500);

        } else if (checkSpamTitle(matchObj) || checkSpamGooglePlay(matchObj) || checkSpamAppleStore(matchObj) || checkSpamJapaneseCharacters(matchObj) || checkSpamChineseCharacters(matchObj)) {
          accounts.spamUser(accountSchema, hostemail)
            .fork(
              () => reject(500),
              () => reject(500)
            );

        } else {

          hostUser(safeEmail(host))
            .map(createHostMatch)
            .getOrElse(taskFail).fork(
              (err) => {
                reject({
                  code: 401, 
                  description: "Host " + hostemail + " does not exist"
                })
              },
              (data) => {
								fulfill(data);
							}
            )

        }

    }, function(err) {
      reject(err);
    });

  });
});
exports.HostMatch = hostMatch;

// hasPath :: a -> o -> bool
const hasPath = R.curry((p, obj) => R.compose(
  R.not,
  R.isNil, 
  R.path(p)
)(obj));
exports.hasPath = hasPath;

// getCoord :: ind -> o -> num 
const getCoord = R.curry((ind, obj) => R.compose( 
  R.nth(ind), 
  R.path(['loc', 'coordinates'])
)(obj));
exports.getCoord = getCoord;

// calculateMatchDistance :: n -> n -> o -> o 
const calculateMatchDistance = R.curry(function(latitude, longitude, match) {

  // distance :: n -> n -> o -> n 
  const distance = R.curry((latitude, longitude, match) => 
    geolib.getDistanceSimple({
      latitude:   Number(latitude), 
      longitude:  Number(longitude),
    }, {
      latitude:   getCoord(1, match), 
      longitude:  getCoord(0, match),
    })
  );

  // prepMongo :: o -> o
  const prepMongo = (match) => hasPath(['loc', 'coordinates'], match)
    ? R.assoc('distance', distance(latitude, longitude, match), match)
    : match;

  return prepMongo(match);

});
exports.CalculateMatchDistance = calculateMatchDistance;


// approveUser :: (Schema(account) -> Schema(match) -> mkey -> user -> ukey -> pkey) => Model(Match) 
var approveUser = curry(function(accountSchema, matchSchema, matchkey, useremail, userkey, playerkey) {
  return new Promise (function(fulfill, reject) {
    verifyHostKey(accountSchema, matchSchema, matchkey, useremail, userkey).then(function(x) {
      if (hasUser(useremail, x)) {

        // findtype :: [{m}] -> s
        const findtype = compose(
          prop('type'), 
          head, 
          R.filter(R.pipe(prop('playerkey'), R.equals(playerkey))), 
          prop('players')
        );

        const isAccept = compose(
          R.equals('approve'), 
          findtype
        );

        const isRequest = compose(
          R.equals('request'), 
          findtype
        );

        if (isAccept(x)) {
          fulfill(x);  // no change as user is already approved 

        } else {
          if (isRequest(x)) { 
            var update = updateMatchPlayerkeyType(matchSchema, matchkey, playerkey, 'approve');
            fulfill(update);
          } else {
            reject({code: 422, description: 'Only players of invite and request state can be approved'});
          }
        }

      } else {
        reject({code: 409, description: 'User is not in the match'});
      }
    }).catch(reject);
  });
});
exports.ApproveUser = approveUser;


// RejectUser :: AccountMatch -> SchemaMatch -> String -> String -> String -> String -> Model(Match) 
var rejectUser = curry(function(accountSchema, matchSchema, matchkey, useremail, userkey, playerkey) {
  return new Promise (function(fulfill, reject) {
    verifyHostKey(accountSchema, matchSchema, matchkey, useremail, userkey).then(function(x) {

      // look up user email
      playerChange(matchSchema, x, playerkey, ['request'], 'reject')
        .then(fulfill)
        .catch(reject);

    }).catch(reject);
  });
});
exports.RejectUser = rejectUser;


// DeclineInvite :: SchemaAccount -> SchemaMatch -> String -> String -> String -> Model(Match) 
var declineInvite = curry(function(accountSchema, matchSchema, matchkey, useremail, userkey) {
  return new Promise (function(fulfill, reject) {
    verifyPlayerKey(accountSchema, matchSchema, matchkey, useremail, userkey).then(function(x) {
      var playerkey = findPlayerkey(x, 'email', useremail); 
      playerChange(matchSchema, x, playerkey, ['invite', 'approve', 'interested'], 'decline')
        .then(fulfill)
        .catch(reject);
    }).catch(reject);
  });
});
exports.DeclineInvite = declineInvite;


// acceptInviteWithPlayerKey :: SchemaAccount -> SchemaMatch -> String -> String -> String -> Model(Match) 
var acceptInviteWithPlayerKey = curry(function(accountSchema, matchSchema, matchkey, username, useremail, playerkey) {
  return new Promise (function(fulfill, reject) {
    findMatchByKey(matchSchema, matchkey).then(function(x) {

      // getPlayerkey :: s -> {o} -> s
      const getPlayerkey = R.curry((email, x) => findPlayerKeys(x, 'playerkey', 'email', email)); 

      // fulfillWhenEqual :: s -> s -> f(s)
      const fulfillWhenEqual = R.curry((a, b) => (a === b) 
        ? playerChange(matchSchema, x[0], a, ['invite', 'decline'], 'approve')
          .then(fulfill)
          .catch(reject)
        : reject ('badkey')
      )

      S.head(x).map(getPlayerkey(useremail)).map(fulfillWhenEqual(playerkey)); 

    }).catch(reject);
  });
});
exports.acceptInviteWithPlayerKey = acceptInviteWithPlayerKey;


// AcceptInvite :: SchemaAccount -> SchemaMatch -> String -> String -> String -> Model(Match) 
var acceptInvite = curry(function(accountSchema, matchSchema, matchkey, useremail, userkey) {
  return new Promise (function(fulfill, reject) {
    verifyPlayerKey(accountSchema, matchSchema, matchkey, useremail, userkey).then(function(x) {
      var playerkey = findPlayerkey(x, 'email', useremail); 
      playerChange(matchSchema, x, playerkey, ['invite', 'decline'], 'approve')
        .then(fulfill)
        .catch(reject);

    }).catch(reject);
  });
});
exports.AcceptInvite = acceptInvite;

// whenPlayerIsType :: o -> s -> s -> bool
var whenPlayerIsType = R.curry((match, playerkey, type) => {
  return R.compose(
    R.chain(S.equals(type)),
    R.chain(S.prop('type')),
    R.chain(S.head),
    R.chain(S.filter(R.pipe(R.prop('playerkey'), R.equals(playerkey)))),
    S.prop('players')
  )(match).isJust;
});
exports.whenPlayerIsType = whenPlayerIsType;


// PlayerChange :: o -> o -> s -> s -> [s] -> promsie o 
var playerChange = R.curry(function(matchSchema, match, playerkey, fromtypes, totype) {
  return new Promise (function(fulfill, reject) {

    var errors = {
      'E422': {code: 422, description: 'Only players in the invite state can be declined'},
      'E409': {code: 409, description: 'User is not in the match'}
    };

    // validifyType :: o -> s -> Promsie o
    var validifyType = R.curry((x, totype, fromtypes) => {

      // appender
      const appender = (a, b) => [a || b, a || b];

      // mapAccumWhenMatchIsType :: o -> [s] -> bool
      const mapAccumWhenMatchIsType = R.curry((m, fromtypes) => R.compose(
        R.head,
        R.mapAccum(appender, false),
        R.map(whenPlayerIsType(m, playerkey))
      )(fromtypes));

      return (mapAccumWhenMatchIsType(match, fromtypes)) 
        ? fulfill(updateMatchPlayerkeyType(matchSchema, match.key, playerkey, totype))
        : reject(errors.E422);

    });

    // fulfillWhenMatchTypeOk :: [s] -> s -> o -> Promise o
    var fulfillWhenMatchTypeOk = (playerkey, fromtypes, totype, match) => {
      if (whenPlayerIsType(match, playerkey, totype)) {
        fulfill(match)
      } else {
        validifyType(match, totype, fromtypes)
      }
    };

    whenMatchHasUser(match, playerkey) 
      ? fulfillWhenMatchTypeOk(playerkey, fromtypes, totype, match) 
      : reject(errors.E409); 


  });
});
exports.PlayerChange = playerChange;

// readFirstkey :: s -> o -> s
const readFirstkey = (keyname, obj) => R.compose(
	R.path(['profile', keyname]), 
	R.head
)(obj);
exports.readFirstkey = readFirstkey;


// createUserObj :: s -> [o] -> o
const createUserObj = R.curry((newtype, user) => R.zipObj(['email', 'type', 'avataricon', 'name', 'playerkey'], [user[0].email, newtype, readFirstkey('avataricon', user), readFirstkey('name', user), aguid(user.email)]));
exports.createUserObj = createUserObj;

// unlessAutoAcceptRequest bool -> s
const unlessAutoAcceptRequest = (x) => x
	? 'approve'
	: 'request';
exports.unlessAutoAcceptRequest = unlessAutoAcceptRequest;

// joinMatch :: SchemaMatch -> String -> String -> String -> Model(Match) 
const joinMatch = R.curry(function(accountSchema, matchSchema, matchkey, useremail, userkey) {

  // getQuery :: bool -> s
  const getQuery = (autoaccept, user) => autoaccept
    ? { $push: {players: createUserObj('approve', user)}, $inc:{seatavailable: -1} }
    : { $push: {players: createUserObj('request', user)} }

	// promiseApprove :: s -> Promise {o}
	const promiseApprove = (playerkey) => updateMatchPlayerkeyType(matchSchema, matchkey, playerkey, 'approve');

	// promiseRequest :: s -> Promise {o}
	const promiseRequest = (playerkey) => updateMatchPlayerkeyType(matchSchema, matchkey, playerkey, 'request');

	// addUserToMatch :: schema -> o -> Promise o
  const addUserToMatch = (match, user) => matchSchema.findOneAndUpdate(
    { key: match.key },
    getQuery(match.autoaccept, user),
    {new:true}
  ).exec(); 

  return new Promise (function(fulfill, reject) {
    accounts.ReadVerifyAccountFromEmail(accountSchema, useremail, userkey).then(function(user) {

      if (user.length == 1) {		// there should be a valid user logged in
        findMatchByKey(matchSchema, matchkey).then(function(x) {

          if (x.length == 1) {	// the match key should be for a valid match 
            if (whenGameIsFull(x[0])) {	// full games shouldn't allow new players 
              reject({code: 409, description: 'Match is full'});

            } else {	// there is room for a new player 
							if (hasUser(useremail, x[0])) {	// if the player is part of the match, status needs to be checked 
								(findPlayerType(x[0], 'email', useremail) !== 'interested') && 
                (findPlayerType(x[0], 'email', useremail) !== 'standby') && 
                (findPlayerType(x[0], 'email', useremail) !== 'decline')		// unless the user is in the interested or declined state, they will double up 
									? reject({code: 409, description: 'User is already in the match'})
                  : (x[0].autoaccept) 
                    ? fulfill(promiseApprove(findPlayerkey(x[0], 'email', useremail)))
                    : fulfill(promiseRequest(findPlayerkey(x[0], 'email', useremail)));

							} else {	// players who weren't part of the match can join 
								fulfill(addUserToMatch(x[0], user));

							}
            }

          } else {
            reject({code: 404, description: 'Match does not exist'});
            
          }
        });

      } else {
        reject({code: 401, description: 'Unauthorized user'});
      }
    });
  });
});
exports.JoinMatch = joinMatch;

// interestedInMatch :: SchemaMatch -> String -> String -> String -> Promise Model(Match) 
var interestedInMatch = R.curry(function(accountSchema, matchSchema, matchkey, useremail, userkey) {

  // addInterestedUserToMatch :: o -> s -> Promise o
  const addInterestedUserToMatch = (match, user) => matchSchema.findOneAndUpdate(
    {key: match.key}, 
    {$push: {players: createUserObj('interested', user)}}, 
    {new:true}
  ).exec(); 

  return new Promise (function(fulfill, reject) {
    accounts.ReadVerifyAccountFromEmail(accountSchema, useremail, userkey).then(function(user) {
      if (user.length == 1) {
        findMatchByKey(matchSchema, matchkey).then(function(x) {
          if (x.length == 1) {
            var playerkey = findPlayerkey(x[0], 'email', useremail); 

						hasUser(useremail, x[0])
              ? playerChange(matchSchema, x[0], playerkey, ['standby', 'decline'], 'interested')
                .then((x) => fulfill(x))
                .catch((err) => reject(err))
							: addInterestedUserToMatch(x[0], user).then((m) => {
								fulfill(m);
							});

          } else {
            // look up match
            reject({code: 404, description: 'Match does not exist'});
            
          }
        });

      } else {
        reject({code: 401, description: 'Unauthorized user'});
      }
    });
  });
});

exports.interestedInMatch = interestedInMatch;


// buildMatchKey :: s -> s
const buildMatchUrl = (key) => '/events/' + key;
exports.buildMatchUrl = buildMatchUrl;

// userObj s -> maybe o -> o
const userObj = R.curry((status, invitee, user) => ({
	email: R.toLower(invitee), 
	type: status, 
	avataricon: R.chain(S.path(['profile', 'avataricon']), user).getOrElse('/img/default-avatar-sq.jpg'),
	name:  			R.chain(S.path(['profile', 'name']), user).getOrElse(R.toLower(invitee)),
	playerkey: 	aguid(R.toLower(invitee))
}));
exports.userObj = userObj;

// findUserDetails :: schema -> s -> promise maybe {u}
const findUserDetails = R.curry((account, email) => R.composeP(
  S.head, 
  accounts.ReadAccountFromEmail(account)
)(email));


// addStandbyForMatch :: AccountModel -> MatchModel -> s -> s -> s -> s -> Model(Match) 
var addStandbyForMatch = R.curry(function(accountSchema, matchSchema, matchkey, invitee) {

  return new Promise (function(fulfill, reject) {
    findMatchByKey(matchSchema, matchkey).then(function(x) {
      if (x.length == 0) {  // match does not exist
        reject({code: 403, description: 'Match does not exist'})

      } else {
        findUserDetails(accountSchema, invitee).then(function(user) {
          if (hasUser(invitee, x[0])) {
            reject({code: 409, description: 'User is already in the match'});

          } else {
            fulfill (matchSchema.findOneAndUpdate(
              {key: matchkey}, 
              {$push: {players: userObj('standby', invitee, user)}}, 
              {new:true}
            ).exec())
          }
        }).catch(reject);

      }
    })
  });

});
exports.addStandbyForMatch = addStandbyForMatch;


// standbyMatchByKey :: model account -> model match -> s -> s -> s -> s -> model match 
var standbyMatchByKey = R.curry(function(accountSchema, matchSchema, hostemail, hostkey, matchkey, inviteeKey) {

  return new Promise (function(fulfill, reject) {
    verifyHostKey(accountSchema, matchSchema, matchkey, hostemail, hostkey).then(function(x) {

      if (findPlayerkey(x, 'playerkey', inviteeKey) != false ) {
        playerChange(matchSchema, x, inviteeKey, ['approve', 'invite', 'interested'], 'standby')
          .then((x) => fulfill(x))
          .catch((err) => reject(err));

      } else {
        accounts.TaskAccountFromSlug(accountSchema, inviteeKey)
          .fork(
            err =>  reject({code: 404, description: 'User does not exist'}),
            data => addStandbyForMatch(accountSchema, matchSchema, matchkey, S.prop('email', data).getOrElse(''))
              .then(fulfill)
              .catch(reject)
          )

      }

    }).catch(reject);
  });

});
exports.standbyMatchByKey = standbyMatchByKey;


// inviteMatch :: AccountModel -> MatchModel -> s -> s -> s -> s -> Model(Match) 
var inviteMatch = R.curry(function(accountSchema, matchSchema, hostemail, hostkey, matchkey, invitee) {

	// only send invite if the user exists
  // notifyUser :: o -> s -> o -> task o
	var notifyUser = R.curry((x, invitee, user) => {
		if (R.chain(S.prop('email'), user).isJust) {
			accounts.TaskNotifyUser(
				account, 
				R.chain(S.prop('name'), findHostUser(x)).getOrElse('A gamer ') + ' has invited you to a board game event: ' + x.title,
				invitee,
				R.chain(S.prop('avataricon'), findHostUser(x)).getOrElse('/img/default-avatar-sq.jpg'),
				buildMatchUrl(x.key), 
				'eventinvite', 
				true	
			).fork(err => {}, data => {});
		}
	});

  return new Promise (function(fulfill, reject) {
    verifyHostKey(accountSchema, matchSchema, matchkey, hostemail, hostkey).then(function(x) {

      if (testHostUser(hostemail, x)) {
        if (whenGameIsFull(x)) {
          reject({code: 409, description: 'Match is full'});

        } else {
					findUserDetails(accountSchema, invitee).then(function(user) {

						// inviteMongoUpdate :: bool -> s -> Promise o
						const inviteMongoUpdate = R.curry((userExists, invitee) => {
							if (userExists) {
								return matchSchema.findOneAndUpdate(
                  {key: matchkey, 'players.email': invitee, $or:[{'players.type': 'interested'}, {'players.type': 'standby'}]}, 
									{$set: {'players.$.type': 'invite'}}, 
                  {new:true}
								).exec()
							} else {
								return matchSchema.findOneAndUpdate(
									{key: matchkey}, 
									{$push: {players: userObj('invite', invitee, user)}}, 
									{new:true}
								).exec()
							}
						});

						inviteMongoUpdate(hasUser(invitee, x), invitee).then((y) => {
              if (y != null) 
                notifyUser(y, invitee, user);

              fulfill(y);
						});
					})

        }
      } else {
        reject({code: 401, description: 'Invalid host user'});
      }
    }).catch(reject);
  });

});
exports.InviteMatch = inviteMatch;


// inviteMatchByKey :: model account -> model match -> s -> s -> s -> s -> model match 
var inviteMatchByKey = R.curry(function(accountSchema, matchSchema, hostemail, hostkey, matchkey, inviteeKey) {
  return new Promise (function(fulfill, reject) {
    accounts.TaskAccountFromSlug(accountSchema, inviteeKey)
      .fork(
        err =>  reject({code: 404, description: 'User does not exist'}),
        data => inviteMatch(accountSchema, matchSchema, hostemail, hostkey, matchkey, S.prop('email', data).getOrElse(''))
          .then(fulfill)
          .catch(reject)
      )
  });

});
exports.InviteMatchByKey = inviteMatchByKey;


// inviteMatchByPlayerKey :: model account -> model match -> s -> s -> s -> s -> model match 
var inviteMatchByPlayerKey = R.curry(function(accountSchema, matchSchema, hostemail, hostkey, matchkey, playerkey) {

  // emailFromPlayerkey :: {o} -> s -> s
  var emailFromPlayerkey = R.curry((data, playerkey) => R.compose(
    R.chain(S.prop('email')), 
    R.chain(S.find(R.propEq('playerkey', playerkey))), 
    S.prop('players')
  )(data));

  return new Promise (function(fulfill, reject) {
		findMatchByKey(matchSchema, matchkey).then(
			data => inviteMatch(accountSchema, matchSchema, hostemail, hostkey, matchkey, emailFromPlayerkey(data[0].toObject(), playerkey).getOrElse(''))
				.then(fulfill)
				.catch(reject)
		);
  });

});
exports.InviteMatchByPlayerKey = inviteMatchByPlayerKey;


// revokeMatch :: AccountModel -> MatchModel -> String -> String -> String -> String -> Model(Match) 
var revokeMatch = R.curry(function(accountSchema, matchSchema, hostemail, hostkey, matchkey, playerkey) {

  return new Promise (function(fulfill, reject) {
    verifyHostKey(accountSchema, matchSchema, matchkey, hostemail, hostkey).then(function(x) {
      (testHostUser(hostemail, x))
        ? (whenMatchHasUser(x, playerkey)) 
          ? fulfill(removeMatchPlayerkey(matchSchema, matchkey, playerkey))
          : reject({code: 409, description: 'User is not in the match'})
        : reject({code: 401, description: 'Invalid host user'});

    }).catch(reject);

  });
});
exports.RevokeMatch = revokeMatch;


// leaveMatch :: AccountModel -> MatchModel -> String -> String -> String -> String -> Model(Match) 
var leaveMatch = curry(function(accountSchema, matchSchema, useremail, userkey, matchkey) {
  return new Promise (function(fulfill, reject) {
    verifyPlayerKey(accountSchema, matchSchema, matchkey, useremail, userkey).then(function(x) {
      removeMatchPlayerkey(matchSchema, matchkey, findPlayerkey(x, 'email', useremail)).then(fulfill).catch(reject);
    }).catch(reject);
  });
});
exports.LeaveMatch = leaveMatch;


// updateMatchPlayerkeyType :: String -> String -> String -> Object
var updateMatchPlayerkeyType = R.curry(function(matchSchema, matchkey, playerkey, newstate) {
  return new Promise (function(fulfill, reject) {
    var setQuery = '';
    if (newstate == 'approve') {
      findMatchByKey(matchSchema, matchkey).then(function(x) {
        if (x[0].seatavailable === 1) {
          setQuery = {$set: {'players.$.type': newstate, isfull: true}, $inc:{seatavailable: -1}};
          fulfill(matchSchema.findOneAndUpdate({key: matchkey, 'players.playerkey': playerkey}, setQuery, {new:true}).exec());
        } else if (x[0].seatavailable > 1) {
          setQuery = {$set: {'players.$.type': newstate}, $inc:{seatavailable: -1}};
          fulfill(matchSchema.findOneAndUpdate({key: matchkey, 'players.playerkey': playerkey}, setQuery, {new:true}).exec());
        } else {
          reject({code: 422, description: "The match is full"});
        }
      });
    } else {
      setQuery = {$set: {'players.$.type': newstate}};
      fulfill(matchSchema.findOneAndUpdate({key: matchkey, 'players.playerkey': playerkey}, setQuery, {new:true}).exec());
    }
  });
});
exports.UpdateMatchPlayerkeyType = updateMatchPlayerkeyType;


// approveAnonymousPlayer :: {o} -> s -> s -> s -> Promise({o})
var approveAnonymousPlayer = curry(function(matchSchema, matchkey, playerkey, newname) {
  return new Promise (function(fulfill, reject) {
    var setQuery = '';
    var findQuery = {key: matchkey, 'players.playerkey': playerkey, 'players.type':'invite'};
    findMatchByKey(matchSchema, matchkey).then(function(x) {
      if (x[0].seatavailable === 1) {
        setQuery = {$set: {'players.$.type': 'approve', 'players.$.name': newname, isfull: true}, $inc:{seatavailable: -1}};
        fulfill(matchSchema.findOneAndUpdate(findQuery, setQuery, {new:true}).exec());
      } else if (x[0].seatavailable > 1) {
        setQuery = {$set: {'players.$.type': 'approve', 'players.$.name': newname}, $inc:{seatavailable: -1}};
        fulfill(matchSchema.findOneAndUpdate(findQuery, setQuery, {new:true}).exec());
      } else {
        reject({code: 422, description: "The match is full"});
      };
    });
  });
});
exports.approveAnonymousPlayer = approveAnonymousPlayer;


// removeMatchPlayerkey :: Model(match) -> String -> String -> Promise Object
var removeMatchPlayerkey = R.curry(function(matchSchema, matchkey, playerkey) {
  return new Promise (function(fulfill, reject) {
    findMatchByKey(matchSchema, matchkey).then(function(x) {
      const whenApproved = (match, key) => findPlayerType(match, 'playerkey', playerkey) == 'approve';
      const setQuery = (match) => whenApproved(match, playerkey) 
        ? {$pull: {players: {playerkey: playerkey}}, $inc:{seatavailable: 1}, $set:{isfull:false}} 
        : {$pull: {players: {playerkey: playerkey}}};

      matchSchema
        .findOneAndUpdate({key: matchkey}, setQuery(x[0]), {new:true})
        .exec()
        .then(
          (m2) => fulfill(
            matchSchema.findOneAndUpdate(
              {key: matchkey}, 
              checkFull(m2, {seatmax: m2.seatmax}), 
              {new:true}
            ).exec())
        ).catch(reject);

    });
  });
});
exports.RemoveMatchPlayerkey = removeMatchPlayerkey;

// updateRemovePlayerQty :: s -> bool -> o
const updateRemovePlayerQry = R.curry((email, approve) => (approve)
  ? {$pull: {players: {email: email}}, $inc:{seatavailable: 1}, $set:{isfull:false}} 
  : {$pull: {players: {email: email}}}
);
exports.updateRemovePlayerQry = updateRemovePlayerQry;

// buildMachkey :: o -> maybe o
const buildMatchkey = R.compose(
  map(R.objOf('key')),
  S.prop('key')
);

// removePlayerByEmail :: Model(match) -> String -> task Object
var removePlayerEmail = curry(function(matchSchema, email) {

  // updateApproved => model m -> bool -> Task.of(a, b)
  const updateApproved = R.curry((ms, match, isapprove) => 
    new Task((reject, resolve) => {
      if (buildMatchkey(match).isJust) {
        matchSchema.findOneAndUpdate(buildMatchkey(match).get(), updateRemovePlayerQry(email, isapprove), {new:true})
          .exec()
          .then(function(x) {
            resolve(x);
          }
        ).catch((err) => reject(err));
      } else {
        reject('bad match key')
      }
    })
  );

  // isPlayerApproved :: s -> [o] -> Task m
  const isPlayerApproved = R.curry((ms, email, match) => compose(
    chain(updateApproved(ms, match)),
    map(R.equals('approve')),
    chain(S.prop('type')),
    chain(S.find(R.propEq('email', email))),
    S.prop('players')
  )(match));

  return new Task((reject, resolve) => 
    matchSchema.find({
      'players.email': email, 
      status:'open'
    }).then((x) => {
      R.sequence(Task.of, x.map(isPlayerApproved(matchSchema, email))).fork(
        err => log.clos('err'),
        data => {
          resolve(data)
        }
      )
    })
  );
});
exports.removePlayerEmail = removePlayerEmail;


// updateMatchPlayerType :: Model(match) -> String -> String -> String -> Object
var updateMatchPlayerType = curry(function(matchSchema, matchkey, useremail, newstate) {
  return matchSchema.findOneAndUpdate({key: matchkey, 'players.email': useremail}, {$set: {'players.$.type': newstate }}, {new:true}).exec();
});
exports.UpdateMatchPlayerType = updateMatchPlayerType;

// findMatchByUser :: Model(match) -> String => Task([Match]);
var findMatchByUser = R.curry((matchSchema, useremail) => 
	new Task((reject, resolve) => 
		matchSchema
			.find({'players.email': useremail})
			.sort({date:-1})
			.limit(100)
			.exec()
			.then((x) => 
				R.isNil(x) 
					? reject('no matches found') 
					: resolve(x))
			.catch(reject)
	)
);
exports.FindMatchByUser = findMatchByUser;


// updateMatchStatus :: Model(match) -> String -> String -> Object
var updateMatchStatus = curry(function(matchSchema, matchkey, newstate) {
    return matchSchema.findOneAndUpdate({key: matchkey}, {status: newstate}, {new:true}).exec();
});
exports.UpdateMatchStatus = updateMatchStatus;

// whichName :: s -> o -> s
const whichProfile = curry((prop, user) => S.whichever(
  S.prop(prop, user), 
  S.path(['profile', prop], user)
));
exports.whichProfile = whichProfile;

// hostify :: u => h 
//const hostify = compose(R.assoc('type', 'host'), pick(['email', 'avataricon', 'name']));
const hostify = (user) => compose(
  merge(whichProfile('avataricon', user).map(R.objOf('avataricon')).getOrElse({})),
  merge(whichProfile('name', user).map(R.objOf('name')).getOrElse({})),
  merge(S.prop('email', user).map(R.objOf('email')).getOrElse({}))
)({type:'host'}); 
  
  //R.assoc('type', 'host'), 
  //pick(['email', 'avataricon', 'name'])
exports.Hostify = hostify;

// seatavailable :: {o} => number
const seatavailable = (obj) => S.prop('maxplayers', obj).map(dec).getOrElse(0);
exports.Seatavailable = seatavailable;

// userplayers :: {h} || String => {host} 
const userplayers = (hostuser) => (is(String, hostuser)) 
  ? [{email: hostuser, type: 'host'}] 
  : [hostify(hostuser)];
exports.Userplayers = userplayers;

// zipmatch = {o} => {m}
const zipmatch = (obj) => {

  const liftzipmatch = R.lift((id, name, thumb, description, minplaytime, maxplaytime, minplayers, maxplayers) => 
    R.zipObj(
      ['thumbnail', 'status', 'playorder', 'description', 'minplaytime', 'maxplaytime', 'minplayers', 'maxplayers', 'playercounts', 'id', 'name', 'bggrank', 'avgrating', 'weight', 'expansions'], 
      [
        thumb, 
        'select', 
        1, 
        description, 
        minplaytime, 
        maxplaytime, 
        minplayers, 
        maxplayers, 
        S.prop('playercounts', obj).getOrElse([]),
        id, 
        name,
        S.prop('bggrank', obj).getOrElse(''),
        S.prop('avgrating', obj).getOrElse(''),
        S.prop('weight', obj).getOrElse(''),
        S.prop('expansions', obj).getOrElse([]),
      ]
    )
  );

  return liftzipmatch(
    S.prop('id', obj), 
    S.prop('name', obj),
    S.prop('thumbnail', obj), 
    S.prop('description', obj), 
    S.prop('minplaytime', obj), 
    S.prop('maxplaytime', obj), 
    S.prop('minplayers', obj), 
    S.prop('maxplayers', obj)
  ).getOrElse({});

}
exports.Zipmatch = zipmatch;

// lookUpGame :: s -> model {o} -> n -> Promise {g}
const lookUpGame = R.curry((url, gameId) => new Promise((fulfill, reject) => {

  var options = {
    uri: url,
    qs: { id: gameId },
    headers: { 'User-Agent': 'Request-Promise' },
    json: true    // Automatically parses the JSON string in the response
  };

  const fulfillGame = R.compose(
    fulfill,
    zipmatch, 
    R.head
  );

  rp(options)
    .then((gameObject) => {
      (gameObject.length == 1) 
        ? fulfillGame(gameObject)
        : reject("Game Id " + gameId + " Not Found");
    })
    .catch((err) => {
      reject("Game Id " + gameId + " Not Found")
    })

  })
);
exports.LookUpGame = lookUpGame

// mapMergeEmail :: s -> [{o}] => [{g}]
const mapMergeEmail = curry((email, username, avataricon, gameObjects) => gameObjects.map(merge({email: email, username: username, avataricon: avataricon})))
exports.MapMergeEmail = mapMergeEmail

// checkDateOrder :: d1 -> d2 => Bool
const checkDateOrder = curry((from, to) => moment(to).diff(moment(from)) > 0);
exports.CheckDateOrder = checkDateOrder;

// mergeWhenEvent :: {m} => f({o} -> {p})
const mergeWhenEvent = (matchObj) => R.merge(S.prop('eventid', matchObj).map(R.objOf('eventid')).getOrElse({}));
exports.MergeWhenEvent = mergeWhenEvent;

// emptyWhenUndefined :: a -> [] 
const emptyWhenUndefined = (x) => (x == undefined) 
  ? []
  : x;
exports.emptyWhenUndefined = emptyWhenUndefined;

// mapPropId :: [o] -> [n]
const mapPropId = (arr) => map(prop('id'), arr);
exports.mapPropId = mapPropId;

// getGameIds :: o -> maybe [a]
const getGameIds = compose(
  map(mapPropId), 
  map(emptyWhenUndefined), 
  S.prop('games')
);
exports.getGameIds = getGameIds;


// statusOnTimeslot :: o -> s
const statusOnTimeslot = R.curry((status, obj) => 
	(status == 'draft')
		? (hasTimeslots(obj).isJust)
			? 'draft'
			: 'open'
		: status
);
exports.statusOnTimeslot = statusOnTimeslot;


// whenPrivate :: bool -> o -> bool
const whenPrivate = (reccuring, x) => reccuring
  ? x.isprivate
  : (hasTimeslots(x).isJust)
    ? true
    : R.has('isprivate', x)
      ? x.isprivate
      : true;
exports.whenPrivate = whenPrivate;

// assocIfExists s -> val -> obj -> obj
const assocWhenExist = R.curry((prop, val, obj) => {
  return R.isNil(val)
    ? obj
    : R.assoc(prop, val, obj);
});
exports.assocWhenExist = assocWhenExist;

// buildPoint :: a -> a
const buildPoint = (coords) => R.isNil(coords)
  ? undefined
  : {type: 'Point', coordinates: coords};
exports.buildPoint = buildPoint;


// matchfields :: s -> o -> bool -> o -> o
const matchfields = R.curry((hu, mo, isReoccuring, go) => {

  // zipAssoc :: o -> o
  const zipAssoc = R.compose(
    assocWhenExist('seriesid', mo.seriesid),
    assocWhenExist('locname', mo.locname),
    assocWhenExist('loc', buildPoint(mo.coords)),
    R.zipObj([
      'games', 'title', 'description', 'experience', 
      'status', 'isprivate', 'seatmin', 'seatmax', 
      'seatavailable', 'players', 'currency', 
      'price', 'utcoffset', 'timezone', 'reoccur', 
      'timeslots', 'date', 'timeend', 'autoaccept'
    ]) 
  );

  return zipAssoc([go, mo.title, mo.description, mo.experience, statusOnTimeslot(mo.status, mo), whenPrivate(isReoccuring, mo), mo.minplayers, mo.maxplayers, seatavailable(mo), userplayers(hu), mo.currency, mo.price, mo.utcoffset, mo.timezone, mo.reoccur, mo.timeslots, mo.gamedate, mo.timeend, mo.autoaccept]);

});
exports.matchfields = matchfields;


// maybeHeadTimeslotProp :: s -> {o} -> maybe dt 
const maybeHeadTimeslotProp = R.curry((propname, newpropname, obj) => R.compose(
	R.map(R.objOf(newpropname)),
	R.chain(S.prop(propname)),
	R.chain(S.head),
	S.prop('timeslots')
)(obj));
exports.maybeHeadTimeslotProp = maybeHeadTimeslotProp;


// mergeWhenHeadTimeSlot :: {o} -> {o} -> maybe {o}
const mergeWhenHeadTimeSlot = R.curry((timeslotobj, mainobj) => {
	return R.mergeAll([
		mainobj,
		maybeHeadTimeslotProp('startdatetime', 'date', timeslotobj).getOrElse({}),
		maybeHeadTimeslotProp('enddatetime', 'timeend', timeslotobj).getOrElse({}),
		maybeHeadTimeslotProp('id', 'timeslotobj', timeslotobj).getOrElse({})
	]);
});
exports.mergeWhenHeadTimeSlot = mergeWhenHeadTimeSlot;

// dateProp :: s -> obj -> dt
const dateProp = R.curry((propName, obj) => {
  return new Date(R.prop(propName, obj));
});


// verifyDateOrder :: o -> bool
const verifyDateOrder = (matchObj) => {

  // checkDates :: obj -> bool
  const checkDates = R.compose(
    R.reduce(R.and, true),
    R.map(R.gt(R.__, 0)),
    R.map(R.converge(R.subtract, [dateProp('enddatetime'), dateProp('startdatetime')])),
    R.prop('timeslots')
  );

  if (R.has('timeslots', matchObj)) {
    return checkDates(matchObj);

  } else if (R.has('gamedate', matchObj)) {
    return ((matchObj.timeend - matchObj.gamedate) > 0);

  } else {
    return false;

  }

}
exports.verifyDateOrder = verifyDateOrder;


// verifyDateDuration :: o -> bool
const verifyDateDuration = (matchObj) => {

  // checkDate :: dt -> dt -> bool
  const checkDate = R.curry((startdate, enddate) => {
    return (((enddate - startdate) <= 86400000) && ((enddate - startdate) > 1800000));
  });

  // checkDates :: obj -> bool
  const checkDates = R.compose(
    R.reduce(R.and, true),
    R.map(R.converge(checkDate, [dateProp('startdatetime'), dateProp('enddatetime')])),
    R.prop('timeslots')
  );

  if (R.has('timeslots', matchObj)) {
    return checkDates(matchObj);

  } else if (R.has('gamedate', matchObj)) {
    return checkDate(matchObj.gamedate, matchObj.timeend);

  } else {
    return false;

  }

}
exports.verifyDateDuration = verifyDateDuration;


// validReoccur :: o -> validation bool  
const validReoccur = (matchObj) =>
  S.pathEq(['reoccur', 'enable'], true, matchObj).cata({
    Just: () => Success(matchObj),
    Nothing: () => Failure(['Repeating campaign does not have reoccur enable set to true'])
  });
exports.validReoccur = validReoccur;

// validHasGame :: o -> validation bool  
const validHasGame = (matchObj) =>
  S.prop('games', matchObj).cata({
    Just: (a) => a.length > 0
      ? Success(matchObj)
      : Failure(['Event does not have a valid game']),
    Nothing: () => Failure(['Event does not have a valid game'])
  });
exports.validHasGame = validHasGame;


// validGameExperience :: o -> either 
const validGameExperience = R.curry(function(match) {

  switch (match.experience) {
    case '10':
      return collect([validReoccur(match), validHasGame(match)]);
      // break;

    default:
      return Success(match);

  }

})
exports.validGameExperience = validGameExperience; 


// createMatch :: match -> o -> bool -> o -> Promise match
const createMatch = R.curry(function(matchSchema, hostuser, isReoccuring, matchObj) {

  return new Promise (function(fulfill, reject) { 

    // sociafy :: {o} -> {o} => {o}
    const sociafy = (gameObjects) => R.compose(
      mergeWhenHeadTimeSlot(matchObj),
      mergeWhenEvent(matchObj), 
      R.merge(matchfields(hostuser, matchObj, isReoccuring, mapMergeEmail(hostuser.email, whichProfile('name', hostuser).getOrElse(''), whichProfile('avataricon', hostuser).getOrElse(''), gameObjects))), 
      R.pick(['smoking', 'alcohol'])
    );

    // log.clos('createMatch', matchObj);

    validGameExperience(matchObj).matchWith({
      Success: () => {
        if (verifyDateOrder(matchObj)) {
          if (verifyDateDuration(matchObj)) {

            Promise
              .all(R.map(lookUpGame(nconf.get('service:boardgames:url')), getGameIds(matchObj).getOrElse([])))
              .then(function(gameObjects) {
                const newMatch = new matchSchema(sociafy(gameObjects)(matchObj.social));
                fulfill(newMatch.save());
              })
              .catch(err => {
                if ((R.slice(0, 8, err)==='Game Id ') && (R.slice(err.length - 10, err.length, err) ===' Not Found')) {
                  const newMatch = new matchSchema(sociafy(S.prop('games', matchObj).getOrElse([]))(matchObj.social));
                  fulfill(newMatch.save());

                } else {
                  reject('Failed to build match ' + err);

                }
              });

          } else {
            reject("The event should be more than thirty minutes and less than 24hrs duration.");
          }

        } else {
          reject("The start time must be after the end time.");
        }

      },
      Failure: (err) => {
        reject(err);
      }
    });


  });

});
exports.CreateMatch = createMatch;


// findHistoricMatchesByPlayerType :: o -> s -> [s] -> model o
var findHistoricMatchesByPlayerType = R.curry((matchSchema, email, types) => {
  return new Promise (function(fulfill, reject) {

    var checkType = compose(
      R.converge(R.and, [R.pipe(R.isEmpty, R.not), Array.isArray])
    );

    if (checkType(types)) {
      var typify = compose(R.assoc('type', R.__, {}));
      var searchquery = {
        players: {$elemMatch: {email: email, $or: R.map(typify, types)}},
        $or: [{status: 'played'}, {status:'postgame'}]
      }; 

      fulfill(findMatches(searchquery));

    } else {
      reject({code:400, message:'types must be a non empty array'});
    }

  });
});
exports.FindHistoricMatchesByPlayerType = findHistoricMatchesByPlayerType;


// findMatchesByPlayerType :: schemaMatch -> String -> [String] -> [Model(Match)]
var findMatchesByPlayerType = R.curry(function(matchSchema, email, types) {

  return new Promise (function(fulfill, reject) {

    const checkType = R.converge(R.and, [R.pipe(R.isEmpty, R.not), Array.isArray]);

    if (checkType(types)) {
      const typify = R.assoc('type', R.__, {});

      var searchquery = {
        players: {$elemMatch: {email: email, $or: R.map(typify, types)}},
        timeend: {$gte : moment().utc().valueOf()}
      }; 

      fulfill(findMatches(searchquery));

    } else {
      reject({code:400, message:'types must be a non empty array'});
    }

  });
});
exports.FindMatchesByPlayerType = findMatchesByPlayerType;

// buildStatusQuery :: s -> {o}
const buildStatusQuery = R.curry((email, status) => ({
  'status': status, 
  players: {
    $elemMatch:{ 
      email: email, 
      type: {
        $ne:'standby'
      }
    }
  }
}));
exports.buildStatusQuery = buildStatusQuery;

// playerfind :: s -> {o}
const playerfind = (email) => ({
  $or: ['draft', 'open', 'full'].map(buildStatusQuery(email))
});
exports.playerfind = playerfind;

// geofind :: n -> n -> n -> o
const geofind = R.curry((distance, lng, lat) => ({
  $or: [
    {status:'open',   isprivate: false},
    {status:'full',   isprivate: false}
  ],
  experience: {$ne: 6, $ne: 11},
  loc: {
    $near: {
      $geometry: {
        type: "Point" ,
        coordinates: [Number(lng), Number(lat)]
      },
      $maxDistance: Number(distance),
      $minDistance: 0 
    }
  }
}));
exports.geofind = geofind;

// findBoargameMatchesByDistance :: schemaMatch -> Number -> Number -> Number -> [Model(Match)]
var findBoardgameMatchesByDistance = R.curry(function(matchSchema, distance, email, lat, lng) {

  return new Promise (function(fulfill, reject) {

    // updateDistance :: n -> n -> {o} -> {o}
    const updateDistance = calculateMatchDistance(lat, lng);

    // hasSameKey :: {o} -> {o} -> bool
    const hasSameKey = (x, y) => R.prop('key', x) === R.prop('key', y);

    Promise.all([findMatches(playerfind(email)), findMatches(geofind(distance, lng, lat))])
      .then((x) => {
        fulfill(R.sortBy(
          R.prop('date'),
          R.unionWith(hasSameKey, x[0], x[1])
            .map(updateDistance)
        ))
      })

  });
});
exports.FindBoardgameMatchesByDistance = findBoardgameMatchesByDistance;

// buildPublicExperienceQuery :: a -> obj
const buildPublicExperienceQuery = (experienceNos) => {
  return S
    .head(experienceNos)
    .cata({
      Just: () => ({
        $and: [
          {"$or": [
            {status:'open',   isprivate: false},
            {status:'full',   isprivate: false}
          ]},
          {"$or": R.map(R.objOf("experience"), experienceNos)}
        ]
      }),
      Nothing: () => ({
        $and: [
          {"$or": [
            {status:'open',   isprivate: false},
            {status:'full',   isprivate: false}
          ]},
          R.objOf("experience", experienceNos)
        ]
      }) 
    })
};
exports.buildPublicExperienceQuery = buildPublicExperienceQuery;


// findPublicBoardgameByExperience :: n -> Task a 
const findPublicBoardgameByExperience = function(experienceNos) {
  return new Task (function(reject, fulfill) {
    schemaMatch 
      .find(buildPublicExperienceQuery(experienceNos))
      .sort({'date': -1})
      .limit(100)
      .exec()
      .then((x) => {
        fulfill(x)
      });
  });
};
exports.findPublicBoardgameByExperience = findPublicBoardgameByExperience;


// findPublicMatches :: schemaMatch -> String -> [Model(Match)]
var findPublicMatches = R.curry(function(email) {
  return new Promise (function(fulfill, reject) {

    // searchquery
    var searchquery = {
      $or: [
        {status: "open"}, 
        {status: "full"}, 
        {
          status: 'draft',
          players: {
            '$elemMatch': { email: email, type: { '$ne': 'standby' } }
          }
        }
      ],
      isprivate: false 
    }; 
    fulfill(findMatchesRecentFirst(searchquery));

  });
});
exports.findPublicMatches = findPublicMatches;


// FindBoardgameMatchesNearby :: schemaMatch -> String -> [Model(Match)]
var findBoardgameMatchesNearby = R.curry(function(matchSchema, userEmail, distance) {
  return new Promise (function(fulfill, reject) {

    var findbydistance = findBoardgameMatchesByDistance(matchSchema, distance, userEmail, R.__, R.__);

    // findgamesnearby :: e => maybe Model(match)
    const findgamesnearby = R.compose(
      R.map(R.converge(findbydistance, [R.nth(1), R.nth(0)])), 
      R.chain(S.path(['profile', 'localarea', 'loc', 'coordinates'])), 
      S.head 
    );

    // incremenatMatches :: ({m} => Task([Model(Match)])
    const incrementMatches = R.compose(
      R.sequence(Task.of), 
      R.map(taskIncrementStatWhenNotHost('impressioncount', matchSchema, userEmail)), 
      R.map(R.prop('key'))
    ); 

    accounts.ReadAccountFromEmail(account, userEmail).then((user) => {
      findgamesnearby(user).cata({
        Just: (m)=>{
          incrementMatches(m).fork(
            err => reject(err),
            () => fulfill(m)
          );
        },
        Nothing:()=>{
          fulfill([]);
        }
      });
    }).catch((err) => {
      fulfill([]);
    });

    
  });
});
exports.FindBoardgameMatchesNearby = findBoardgameMatchesNearby;


// validifyAccess :: schemaMatch -> String -> String -> [Model(Match)]
var validifyAccess = R.curry(function(matchSchema, matchkey, useremail) {
  return new Promise(function(fulfill, reject) {
    var events = matchSchema.find({key: matchkey}).limit(100).exec(); 
    events.then(function(m) {
      const matchHasUser = compose(
        R.not, 
        R.equals(-1), 
        R.findIndex(R.propEq('email', useremail)), 
        prop('players'), 
        head
      );
      fulfill(matchHasUser(m, useremail));
    }); 
  });
});
exports.ValidifyAccess = validifyAccess;

// findMatchByKey :: schemaMatch -> String -> [Model(Match)]
var findMatchByKey = R.curry(function(matchSchema, matchkey) {
  return matchSchema.find({key: matchkey}).limit(100).exec(); 
});
exports.FindMatchByKey = findMatchByKey;

// FindAllMatchByReoccurSeries :: schemaMatch -> String -> Promise [(Match)]
var FindAllMatchByReoccurSeries = R.curry(function(matchSchema, serieskey) {
  return matchSchema
    .find({'seriesid': serieskey})
    .sort({'date': -1})
    .limit(100)
    .exec(); 
});
exports.FindAllMatchByReoccurSeries = FindAllMatchByReoccurSeries;

// FindLastMatchByReoccurSeries :: schemaMatch -> String -> Promise [(Match)]
var FindLastMatchByReoccurSeries = R.curry(function(matchSchema, serieskey) {
  return new Promise(function(fulfill, reject) {
    matchSchema
      .find({'seriesid': serieskey})
      .sort({'date': -1})
      .limit(1)
      .exec()
      .then((data) => {
        (data.length == 1)
          ? fulfill(R.last(data))
          : reject('bad series key');
      })
      .catch((err) => reject(err))
  });
});
exports.FindLastMatchByReoccurSeries = FindLastMatchByReoccurSeries;



// verifyPlayerKey ::  Model(Account) -> String -> String -> String -> Promise(Account)
var verifyPlayerKey = R.curry(function(account, match, matchkey, email, userkey) {
    const verifyFunction = (match, email) => findPlayerType(match, 'email', email) != 'host';
    return verifyKey(account, match, matchkey, verifyFunction, email, userkey);
});
exports.VerifyPlayerKey = verifyPlayerKey;


// verifyHostKey :: Model(Account) -> Model(Match) -> String -> String -> String -> Promise(Account)
var verifyHostKey = R.curry(function(account, match, matchkey, email, userkey) {
    const verifyFunction = (match, email) => findPlayerType(match, 'email', email) == 'host';
    return verifyKey(account, match, matchkey, verifyFunction, email, userkey);
});
exports.VerifyHostKey = verifyHostKey;


// verifyKey ::  Model(Account) -> String -> String -> String -> Promise(Account)
var verifyKey = curry(function(account, match, matchkey, whenTestFunction, email, userkey) {
  return new Promise(function(fulfill, reject) { 

    var errors = {
      'E401': {code: 401, message: 'Invalid user'},
      'E403': {code: 403, message: 'User does not have permissions'},
    }

    var whenHasPlayerkey = curry((req, keyname, value) => compose(
      R.equals(1), 
      R.length, 
      R.filter(R.pipe(prop(keyname), R.equals(value))), 
      prop('players')
    )(req));

    var fulfillOnTestFunction = (match, email) => whenTestFunction(match, email) 
      ? fulfill(match) 
      : reject(errors.E403);

    var isPlayerIsInMatch = (match, email) => whenHasPlayerkey(match, 'email', email) 
      ? fulfillOnTestFunction(match, email) 
      : reject(errors.E403);

    var whenKeyisFound = curry((matches, email) => matches.length == 1 
      ? isPlayerIsInMatch(matches[0], email) 
      : reject(errors.E403));

    account.find({'email': email,'key': userkey}).exec().then(function(a) {
      a.length == 1 
        ? match.find({key: matchkey}).limit(100).exec().then(whenKeyisFound(R.__, email)) 
        : reject(errors.E401);

    });
  });
});
exports.VerifyKey = verifyKey;


// findPlayerkey :: Model(Match) -> String -> String -> String
var findPlayerkey = curry(function(match, keyname, value) {
  return findPlayerKeys(match, 'playerkey', keyname, value);
});
exports.FindPlayerkey = findPlayerkey


// findPlayerType :: (Model(Match) -> String -> String) => String
var findPlayerType = R.curry(function(match, keyname, value) {
  return findPlayerKeys(match, 'type', keyname, value);
});
exports.FindPlayerType = findPlayerType


// disband :: Model(Match) -> String -> String -> String -> Promise(String)
const disband = curry(function(accountSchema, matchSchema, matchkey, hostemail, hostkey) {

    const filterplayers = (type) => compose(R.filter(R.pipe(prop('type'), R.equals(type))), prop('players'));
    const refundHost = compose(map(prop('email')), filterplayers('host'));
    const refundPlayers = compose(map(prop('email')), filterplayers('request'));
    //const releaseTicket = R.curry((match, email) => accounts.ReleaseTicket(account, email, 'paper', match));

    return new Promise(function(fulfill, reject) {
      var errors = {
        'E403': {code: 403, message: 'User does not have permissions'},
        'E405': {code: 405, message: 'Game can not be disbanded once it starts'}
      }
      verifyHostKey(accountSchema, matchSchema, matchkey, hostemail, hostkey).then(function(m) {  
        if (m.status == 'full' || m.status == 'open' || m.status=='draft') { 
          matchSchema.findOneAndRemove({'key': matchkey}).exec().then(fulfill);
        }
      }).catch(function(err) {
        return reject(errors.E403);
      });
    });
});
exports.Disband = disband; 


// findPlayerKeys :: Model(Match) -> String -> String -> String -> String
const findPlayerKeys = R.curry(function(match, returnkey, findkey, value) {

  // filterplayer :: o -> o
  const filterplayer = R.compose(
    R.filter(R.pipe(R.prop(findkey), R.equals(value))), 
    R.prop('players')
  );

  // playertype :: [o] -> s
  const playertype = R.compose(
    R.prop(returnkey), 
    R.head
  );

  // unlessEmpty :: [o] -> bool
  const unlessEmpty = R.compose(
    R.equals(1), 
    R.length, 
    filterplayer
  );

  // foundtype :: [o] -> s 
  const foundtype = R.compose(
    playertype, 
    filterplayer
  );

  return (unlessEmpty(match)) 
    ? foundtype(match) 
    : false; 

});
exports.FindPlayerKeys = findPlayerKeys


// newMessages :: (Model(Match) -> String -> [Object]) =>  Promise([{t}]);
const newMessages = curry(function(matchSchema, matchkey, hash) {

  const forum = compose(head, R.map(prop('forumthread')));
  const unlesshash = curry((hash, thread) => (thread.hash != hash));

  return new Promise(function(fulfill, reject) {
    findMatchByKey(matchSchema, matchkey).then(function(m) {

      const pruneThreads = compose(R.drop(1), R.dropWhile(unlesshash(hash)), forum);
      fulfill(pruneThreads(m));

    });
  });

});
exports.NewMessages = newMessages

// {o} => maybe(n)
const safeCalcTime = compose(map(R.divide(R.__, 60)), map(R.divide(R.__, 1000)), R.converge(lift(R.subtract), [S.prop('timeend'), S.prop('date')]));

// readPlayer :: {o} => [e, bool];
const readPlayer = (player) => sequence(maybe.of, [S.prop('email', player), safePropEq('type', 'host', player)]);
exports.ReadPlayer = readPlayer;

// gamePlayers :: {o} => [[maybe(e), maybe(bool)])]
const gamePlayers = compose(sequence(maybe.of), chain(map(readPlayer)), S.prop('players'));
exports.GamePlayers = gamePlayers;

// {o} => maybe([[email, bool, xp]])
const gamePlayerXP = (updatelist) => {
  const appendList = curry((val, list) => list.map(R.append(val)));
  return lift(appendList)(safeCalcTime(updatelist), gamePlayers(updatelist));
};
exports.GamePlayerXP = gamePlayerXP;

// addWeek :: maybe d -> maybe n -> maybe d
const addWeek = lift((date, num) => moment(date).add(num, 'week').toDate());
exports.addWeek = addWeek;

// addWeekOfMonth :: maybe d -> maybe n -> maybe moment 
const addWeekOfMonth = lift((date, num) => {

  // firstDateTry :: n -> d -> moment
  const firstDateTry = R.curry((numDayofWeek, originalDate) => moment(originalDate)
    .recur()
    .every(numDayofWeek)
    .daysOfWeek()
    .every([num - 1])
    .weeksOfMonthByDay()
    .next(1)[0]
    .add(moment(date).format('H'), 'hours')
    .add(moment(date).format('m'), 'minutes'));

  // adjustDayOfWeek :: d -> d -> n
  const adjustDayOfWeek = (origdate, newdate) => R.modulo(moment(origdate).diff(newdate, 'days'), 7);

  // testDateMax :: d -> n -> bool
  const testDateMax = (mdate, weeks) => Number(mdate.format('D')) > 7 * weeks;

  // adjustedDate :: d -> moment
  const adjustedDate = (date) => firstDateTry(
    Number(moment(date).format('e')) + adjustDayOfWeek(date, firstDateTry(moment(date).format('e'), date)), 
    date
  );

  return testDateMax(adjustedDate(date), num)
    ? adjustedDate(date).add(-7, 'days').toDate()
    : adjustedDate(date).toDate(); 

});
exports.addWeekOfMonth = addWeekOfMonth;

// recurDateObj :: s -> s -> o -> maybe o
const recurDateObj = curry((propNameFrom, propNameTo, obj) => 
  (S.prop('status', obj).getOrElse('draft') !== 'draft')
    ? S.path(['reoccur', 'enable'], obj)
      .getOrElse(false)
        ? (S.path(['reoccur', 'interval'], obj).getOrElse('') === 'week')
          ? addWeek(
              S.prop(propNameFrom, obj),
              S.path(['reoccur', 'every'], obj)
            )
            .map(R.objOf(propNameTo))
            .map(merge(obj))
          : (S.path(['reoccur', 'interval'], obj).getOrElse('') === 'weekOfMonth')
            ? addWeekOfMonth(
                S.prop(propNameFrom, obj),
                S.path(['reoccur', 'every'], obj)
              )
              .map(R.objOf(propNameTo))
              .map(merge(obj))
            : maybe.of(obj)
        : maybe.of(obj)
    : maybe.of(obj)
);
exports.recurDateObj = recurDateObj;

// buildSocial :: o -> o
const buildSocial = (obj) => R.compose(
  R.omit(['smoking', 'alcohol']),
  R.merge({
    social: { 
      smoking: S.prop('smoking', obj).getOrElse('1'), 
      alcohol: S.prop('alcohol', obj).getOrElse('1') 
    }
  })
)(obj);
exports.buildSocial = buildSocial;

// buildLoc :: o -> o
const buildLoc = (obj) => R.compose(
  R.map(R.omit(['loc'])),
  R.map(merge(obj)),
  R.map(R.objOf('coords')),
  S.path(['loc', 'coordinates'])
)(obj).getOrElse(obj);
exports.buildLoc = buildLoc;

// renameProp :: s -> s -> o -> o
const renameProp = curry((oldName, newName, obj) => R.compose(
  R.map(R.omit(oldName)),
  R.map(merge(obj)),
  R.map(R.objOf(newName)),
  S.prop(oldName)
)(obj).getOrElse(obj));
exports.renameProp = renameProp;

// transformDate :: o -> o
const transformDate = (obj) => recurDateObj('date', 'gamedate', obj)
  .chain(recurDateObj('timeend', 'timeend'))
  .map(R.omit(['date']))
  .getOrElse(obj);
exports.transformDate = transformDate;

// respawnGames :: o -> o
const respawnGames = (matchObj) => {
  return S.whenPropEq('experience', 10, matchObj).cata({
    Just: (a) => a, 
    Nothing: () => R.assoc('games', [], matchObj)
  });
};
exports.respawnGames = respawnGames;


// taskRespawnMatch :: o -> o -> o -> task o
const taskRespawnMatch = R.curry((match, account, data) => 
  new Task ((reject, resolve) => {

    // taskGetHost :: o -> task o
    const taskGetHost = (account, matchObj) => 
      S.prop('players', matchObj)
        .chain(S.head)
        .chain(S.prop('email'))
        .chain(accounts.TaskAccountFromEmail(account))

    // cleanMatchObj :: o -> o
    const cleanMatchObj = R.compose( 
      R.merge(R.__, {status:'open'}),
      renameProp('seatmax', 'maxplayers'),
      renameProp('seatmin', 'minplayers'),
      respawnGames,
      buildLoc,
      buildSocial,
      transformDate,
      R.pick([
        'title', 'locname', 'date',
        'timeend', 'seatmin', 'seatmax',
        'timezone', 'experience', 'alcohol',
        'smoking', 'currency', 'price', 
        'isprivate', 'loc', 'reoccur', 
        'status', 'description', 'autoaccept',
        'seriesid', 'games'
      ]),
      scrub
    );

    // notTypeHosts :: o -> bool
    const notTypeHost = R.compose(
      R.not,
      R.propEq('type', 'host')
    );

    // filterHosts :: o -> [s]
    const filterHosts = R.compose(
      R.sequence(maybe.of),
      R.chain(R.map(S.prop('email'))),
      R.chain(S.filter(notTypeHost)),
      S.prop('players')
    );

    taskGetHost(account, data).fork(
      err =>  log.clos('taskRespawnMatch.err', err),
      user => {
        if (user.spamuser === true) {         // spam user
          reject('An error occured while creating the event');
        } else {
          createMatch(match, user, true, cleanMatchObj(data)).then(
            (newdata) => {

              filterHosts(data).cata({
                Just: (players) => {  // add back in players as standby status
                  Promise.all(R.map(addStandbyForMatch(account, match, newdata.key), players)).then(x => {
                    findMatchByKey(match, newdata.key).then(y => resolve(y[0]))
                  })
                },
                Nothing: () => resolve(newdata)
              });

              // add all players as potential invites

            },
            (err)  => reject(err)
          )
        }
      }
    );
  })
);
exports.taskRespawnMatch = taskRespawnMatch;

// taskPlayMatch :: (Model(Match) -> Model(Account)) -> o -> s -> Task({o})
const taskPlayMatch = R.curry((matchSchema) => new Task((reject, resolve) => {
      
  // TODO: status:'full' is redundant and can be removed later
  var searchquery = {
    $and: [
      {timeend: {$lt : moment().utc().valueOf()}}, 
      {$or:[{status: 'open'}, {status: 'full'}]}
    ]
  }; 

  var updatequery = {$set: {status: 'postgame'}}; 
  var options = {new: true};

  // isReoccur :: o -> maybe bool
  const isReoccur = S.path(['reoccur', 'enable']);

  // isEvery :: o -> maybe n 
  const isEvery = S.path(['reoccur', 'every']);

  matchSchema
    .findOneAndUpdate(searchquery, updatequery, options)
    .exec()
    .then(data => {
      if (isNil(data)) {
        reject('no match');
      } else {
        if (isReoccur(data).getOrElse(false))
          if (isEvery(data).isJust) {
            taskRespawnMatch(schemaMatch, account, data)
              .fork(
                err => log.clos('err.taskRespawnmatch: ' + err), 
                respawned => {} 
              );
          }
        resolve (data);
      }
    }).catch(reject);
  })
);
exports.TaskPlayMatch = taskPlayMatch;


// taskFinaliseMatch :: (Model(Match) -> Model(Account)) -> Task({o})
const taskFinaliseMatch = curry((matchSchema, accountSchema) => new Task((reject, resolve) => {
      
  var searchquery = {
    $and: [
      {timeend: {$lt : moment().subtract({hours: 6}).utc().valueOf()}}, 
      {status: 'postgame'}
    ]
  }; 

  var updatequery = {$set: {status: 'played'}}; 
  var options = {new: true};

  // taskXP :: (id -> s -> bool) => Task(game)
  const taskXP = accounts.TaskAddGameExperience(accountSchema);

  // {o} => Task([{account}])
  const addXP = compose(
    R.sequence(Task.of), 
    R.chain(R.map(R.apply(taskXP))), 
    gamePlayerXP
  );

  matchSchema
    .findOneAndUpdate(searchquery, updatequery, options)
    .exec()
    .then(matchData => {
      if (isNil(matchData)) {
        reject('no match');
      } else {
        addXP(matchData).fork(reject, () => resolve (matchData));
      }
    }).catch(reject);

}));
exports.taskFinaliseMatch = taskFinaliseMatch;


// taskReadByEvent :: (Model(Match) -> s) => [{a}]
const taskReadByEvent = curry(function(match, eventslug) {
  return new Task((reject, resolve) => {
    match.find({eventid: eventslug}).sort({date:1}).exec().then(function(m) {
      resolve(m);
    }).catch(reject);
  });
});
exports.TaskReadByEvent = taskReadByEvent;


// taskIncrementStatWhenNotHost :: (Model(match) -> e -> k) => {o}
const taskIncrementStatWhenNotHost = curry(function(field, match, email, key) {

  // findHost :: {m} => maybe e
  const findHost = R.compose(
		R.chain(S.prop('email')), 
		R.map(R.find(R.propEq('type', 'host'))), 
		S.prop('players')
	);

  return new Task((reject, resolve) => {
    match.findOne({key: key}).then(function(m) {
      if (findHost(m).getOrElse('') == email) { 
        resolve(m) 
      } else {
        match.findOneAndUpdate({key: key}, {$inc: R.objOf(field, 1)}, {new:true})
          .then(resolve)
          .catch(reject);
      }
    });
  });

});
exports.TaskIncrementStatWhenNotHost = taskIncrementStatWhenNotHost


// taskFindMatch :: k => Task({m})
const taskFindMatch = R.curry((match, key) => 
	new Task((reject, resolve) => 
		match.findOne({key:key}).then(a => {
			R.isNil(a) 
				? reject(404) 
				: resolve(a);
		}).catch(reject)));
exports.TaskFindMatch = taskFindMatch;

// checkLocName :: o -> o 
const checkLocName = (data) => S.prop('playarea', data)
  .map(R.objOf('locname'))
  .map(R.merge(data))
  .map(R.omit(['playarea']))
  .getOrElse(data);
exports.checkLocName = checkLocName;

// coords :: n -> l -> o 
const coords = R.curry((lng, lat) => R.objOf('loc', {type:'Point', coordinates:[lng, lat]}));
exports.coords = coords;

// checkLocLatLng :: o -> o
const checkLocLatLng = (data) => R.lift(coords)(
  S.prop('playarealng', data)
    .map(Number), 
  S.prop('playarealat', data)
    .map(Number)
  )
    .map(R.merge(data))
    .map(R.omit(['playarealat', 'playarealng']))
    .getOrElse(data);
exports.checkLocLatLng = checkLocLatLng;

// whenHasDateAssoc :: s -> s -> obj
const whenHasDateAssoc = R.curry((propFrom, propTo, obj) => {
  return R.has(propFrom, obj)
    ? R.assoc(propTo, new Date(R.prop(propFrom, obj)), obj)
    : obj;
});
exports.whenHasDateAssoc = whenHasDateAssoc;

// dateify :: o -> o
const dateify = (obj) => R.compose(
  whenHasDateAssoc('date', 'gamedate'),
  whenHasDateAssoc('timeend', 'timeend')
)(obj);
exports.dateify = dateify;


// taskUpdateMatch :: (Model(match) -> k -> {o}) => {b}
const taskUpdateMatch = R.curry(function(match, key, updateObj) {
  return new Task((reject, resolve) => {

    const numberfyLte = (a, b) => lte(Number(a), Number(b));
    const liftLte = lift(numberfyLte);
    const liftMax = lift(R.max);

    taskFindMatch(match, key).fork(
      err  => reject(404),
      data => {

        // maybeProp :: {u} -> {d} -> s => val 
        const maybeProp = maybeData(updateObj, data);

        // minMax :: {d} => maybe(l)
        const minMax = (data) => liftMax(countPlayers(data), maybeProp('seatmin'));

        // buildSet :: ({d} -> {o}) => {p}
        const buildSet = R.compose(
          checkLocLatLng, 
          checkLocName, 
          checkFull
        );

        liftLte(minMax(data), maybeProp('seatmax')).map(function(minLtMax) {
          minLtMax 
            ? (!R.has('date', updateObj) || (verifyDateOrder(dateify(updateObj)) && verifyDateDuration(dateify(updateObj))))
              ? match.findOneAndUpdate({key: key}, {$set: buildSet(data, updateObj)}, {new:true}).exec().then(resolve).catch(reject) 
              : reject(400)
            : reject(400);
        });
      }
    )
  });
});
exports.TaskUpdateMatch = taskUpdateMatch;

// defaultGame :: n -> s -> s -> {o}
const defaultGame = (gameid, gamename, gamethumbnail) => ({
  id: gameid,
  name: gamename,
  thumbnail: gamethumbnail,
  description: 'Pending Bgg Lookup...', 
  minplaytime: 30, 
  maxplaytime: 60, 
  minplayers: 2, 
  maxplayers: 4
});
exports.defaultGame = defaultGame;

// addGameObj :: {u} -> s -> s -> {o} 
const addGameObj = (user, email, status) => R.compose(
  R.objOf('$push'), 
  R.objOf('games'), 
  R.merge({
    username: S.path(['profile', 'name'], user).getOrElse('No Name'), 
    avataricon: S.path(['profile', 'avataricon'], user).getOrElse('/img/default-avatar-sq.jpg'), 
    email: email,
    vote: [{
      email:      email,
      userkey:    S.prop('pageslug', user).getOrElse('No Name'),
      username:   S.path(['profile', 'name'], user).getOrElse('No Name'),
      avataricon: S.path(['profile', 'avataricon'], user).getOrElse('/img/default-avatar-sq.jpg')
    }]
  }), 
  R.merge({status:status}), 
  R.pick(['id', 'name', 'thumbnail', 'description', 'minplaytime', 'maxplaytime', 'minplayers', 'maxplayers', 'email', 'bggrank', 'avgrating', 'weight', 'playercounts', 'bggrank', 'expansions'])
);
exports.addGameObj = addGameObj;


// taskAddGameToMatch :: account -> match -> s -> n -> s -> s -> Task match
const taskAddGameToMatch = curry(function(account, match, key, status, gameid, gamename, gamethumbnail, email) {

  // updateMatch :: Model match -> {u} -> s -> s -> {g} -> f(Model account) -> f(Model Account) 
  const updateMatch = (match, key, user, email, game) => 
    match
      .findOneAndUpdate(
        {key: key}, 
        addGameObj(user, email, status)(game), 
        {new: true})
      .exec()

  return new Task((reject, resolve) => accounts.TaskAccountFromEmail(account, email).fork(
    err => reject,
    user => {
      lookUpGame(nconf.get('service:boardgames:url'), gameid).then(
        game => updateMatch(match, key, user, email, game).then(resolve).catch(reject),
        err =>  updateMatch(match, key, user, email, defaultGame(gameid, gamename, gamethumbnail)).then(resolve).catch(reject)
      )
    }
  ));

});
exports.TaskAddGameToMatch = taskAddGameToMatch;


// taskBringSuggestedGameToMatch :: account -> match -> s -> s -> s -> s -> Model match
const taskBringSuggestedGameToMatch = R.curry(function(account, match, matchkey, gameslug, email) {

  // updateMatch :: Model match -> {u} -> s -> s -> {g} -> f(Model account) -> f(Model Account) 
  const updateMatch = (match, key, user, email, gameslug, fxresolve, fxreject) => 
    match
      .findOneAndUpdate(
        {key: matchkey, 'games.pageslug': gameslug}, {
          $set: {
            'games.$.status':     'select',
            'games.$.username':   S.path(['profile', 'name'], user).getOrElse('No Name'), 
            'games.$.avataricon': S.path(['profile', 'avataricon'], user).getOrElse('/img/default-avatar-sq.jpg'), 
            'games.$.email':      email
          }
        },
        {new: true})
      .exec()
      .then(fxresolve)
      .catch(fxreject)

  return new Task((reject, resolve) => accounts.TaskAccountFromEmail(account, email).fork(
    err =>  {
      reject
    },
    user => updateMatch(match, matchkey, user, email, gameslug, resolve, reject)
  ));

});
exports.taskBringSuggestedGameToMatch = taskBringSuggestedGameToMatch;


// taskRemoveGameToMatch :: (Model(match) -> k -> s) => Model(match)
const taskRemoveGameFromMatch = curry(function(match, key, gameslug) {

  // removeGameObj :: id => {o}
  const removeGameObj = R.compose(
    R.objOf('$pull'), 
    R.objOf('games'), 
    R.objOf('pageslug'), 
    R.objOf('$eq')
  );

  // removeGameObj :: id => {o}
  const removeExpansionObj = R.compose(
    R.objOf('$pull'), 
    R.objOf('assignedexpansions'), 
    R.objOf('gamepageslug'), 
    R.objOf('$eq')
  );

  match.findOneAndUpdate({key: key}, removeExpansionObj(gameslug)).exec().then();

  return new Task((reject, resolve) => 
    match.findOneAndUpdate({key: key}, R.merge(removeExpansionObj(gameslug), removeGameObj(gameslug)), {new: true}).exec().then(resolve).catch(reject));


});
exports.TaskRemoveGameFromMatch = taskRemoveGameFromMatch;


// entryFee :: n -> s -> p 
const entryFee = (price, currency) => currencyFormatter.format(price, {code: currency});
exports.EntryFee = entryFee;

// findPlayerInMatch :: s -> o => maybe Bool 
const findPlayerInMatch = (email) => R.compose(
  R.map(R.converge(R.or, [R.propEq('type', 'host'), R.propEq('type', 'approve')])),
  R.chain(S.find(propEq('email', email))), 
  R.chain(S.prop('players')), 
  S.head
);
exports.findPlayerInMatch = findPlayerInMatch;

// findHostInMatch :: s -> o => maybe Bool 
const findHostInMatch = (email) => compose(
  chain(S.whenPropEq('email', email)), 
  chain(S.head),
  chain(S.prop('players')), 
  S.head
);
exports.findHostInMatch = findHostInMatch;

// whenMatchHasVote :: s -> n -> maybe bool
const whenMatchHasVote = curry((email, gameid) => compose(
  map(R.propEq('email', email)), 
  chain(S.find(propEq('email', email))), 
  chain(S.prop('vote')), 
  chain(S.find(propEq('id', Number(gameid)))), 
  chain(S.prop('games')), 
  S.head
));
exports.whenMatchHasVote = whenMatchHasVote;


// pushVote :: s -> n -> [a] => [a]
const pushVote = R.curry((value, email, userkey, username, avataricon, arr) => R.append({email: email, userkey: userkey, username: username, avataricon: avataricon, direction: value}, arr));
exports.pushVote = pushVote;


// addMatchVotes :: s -> n -> maybe [a] 
const addMatchVotes = R.curry((email, userkey, username, avataricon, value, gameid) => R.compose(
  R.map(R.objOf('$set')), 
  R.map(R.objOf('games.$.vote')), 
  R.map(pushVote(value, email, userkey, username, avataricon)), 
  R.chain(S.prop('vote')), 
  R.chain(S.find(propEq('id', Number(gameid)))), 
  R.chain(S.prop('games')), 
  S.head)
);
exports.addMatchVotes = addMatchVotes;


// voteForGame :: account -> match -> s -> n -> s -> s -> task o
const voteForGame = curry((account, match, matchid, gameid, email, key) => {
  return new Task((reject, resolve) => {
    accounts.TaskVerifyAnAccountFromEmail(account, email, key).fork(
      err => reject(401),
      user => {
        findMatchByKey(match, matchid)
          .then(function(m) {
            if (findPlayerInMatch(email)(m).getOrElse(false)) {
              if (whenMatchHasVote(email, gameid)(m).getOrElse(false)) {
                reject(409);

              } else {
                match.findOneAndUpdate({key: matchid, 'games.id': Number(gameid)}, addMatchVotes(email, user.toObject().pageslug, user.toObject().profile.name, user.toObject().profile.avataricon, 1, gameid)(m).getOrElse({}), {new:true}).exec().then((m2) => {
                  resolve(S.prop('games', m2).map(R.objOf('games')).getOrElse([]));

                })
              }

            } else {
              reject(401);

            };

          })
          .catch(err => reject(401));

      }
    );
  });
}); 
exports.voteForGame = voteForGame;


// removeMatchVotes :: s -> n -> maybe [a] 
const removeMatchVotes = R.curry((email, gameid) => R.compose(
  map(R.objOf('$set')), 
  map(R.objOf('games.$.vote')), 
  map(R.reject(propEq('email', email))), 
  chain(S.prop('vote')), 
  chain(S.find(propEq('id', Number(gameid)))), 
  chain(S.prop('games')), 
  S.head)
);
exports.removeMatchVotes = removeMatchVotes;


// unvoteForGame :: account -> match -> s -> n -> s -> s -> Task o
const unvoteForGame = curry((account, match, matchid, gameid, email, key) => {
  return new Task((reject, resolve) => {
    accounts.TaskVerifyAnAccountFromEmail(account, email, key).fork(
      err => reject(401),
      user => findMatchByKey(match, matchid).then(function(m) {
        if (findPlayerInMatch(email)(m).getOrElse(false)) {
          if (whenMatchHasVote(email, gameid)(m).getOrElse(false)) {
            match.findOneAndUpdate({key: matchid, 'games.id': Number(gameid)}, removeMatchVotes(email, gameid)(m).getOrElse({}), {new:true}).exec().then((m2) => {
              resolve(S.prop('games', m2).map(R.objOf('games')).getOrElse([]));
            })
          } else {
            reject(409);
          }
        } else {
          reject(401);
        };
      })
      .catch(err => reject(401))
    );
  });
}); 
exports.unvoteForGame = unvoteForGame;

// publishMatch :: account -> match -> s -> s -> s -> {o} -> Task o
const publishMatch = R.curry((account, match, matchid, email, key, data) => {

  // readIsprivate :: {o} -> {o}
  const readIsprivate = (req) => S
    .prop('isprivate', req)
    .map(R.objOf('isprivate'))
    .getOrElse({isprivate: false})

  // readdate :: {o} -> {o}
  const readdata      = R.curry((key, req) => S
    .prop(key, req)
    .map(R.objOf(key))
    .getOrElse({}))

  return new Task((reject, resolve) => {
    accounts.TaskVerifyAnAccountFromEmail(account, email, key).fork(
      err => reject(401),
      user => findMatchByKey(match, matchid).then(function(m) {
        if (findHostInMatch(email)(m).getOrElse(false)) {
          match
            .findOneAndUpdate({key: matchid}, R.mergeAll([
              {status: 'open'}, 
              readIsprivate(data), 
              readdata('date', data), 
              readdata('timeend', data), 
            ]), {new:true})
            .exec()
            .then((m2) => {

							// TODO Make this notify nearby users
							// accounts.taskNotifyNewNearbyEvent(account, m2.toObject());

							/*
							accounts.TaskNotifyUser(
								account, 
								m2.players[0].email + ' is hosting a new board game event nearby.',
								m2.players[0].email,
								m2.players[0].avataricon,
								buildMatchKey(m2.key), 
								'nearbyevent', 
							  true	
							).fork(err => {}, data => {});
							*/

              resolve(m2);
            }
          )
        } else {
          reject(401);
        }
      })
      .catch(err => reject(401))
    );
  });
});
exports.publishMatch = publishMatch;


// findDate :: s -> [a] -> s -> dt 
const findDate = (timeslotkey, timeslots, key) => {
  return S.find(R.propEq('id', timeslotkey), timeslots)
    .chain(S.prop(key));
};
exports.findDate = findDate;


// assignDates :: account -> match -> s -> s -> s -> s -> Task o
const assignDates = curry((account, match, matchid, timeslotkey, email, key) => {
  return new Task((reject, resolve) => {
    accounts.TaskVerifyAnAccountFromEmail(account, email, key).fork(
      err => reject(401),
      user => findMatchByKey(match, matchid).then(function(m) {
        if (findHostInMatch(email)(m).getOrElse(false)) {
          if (findDate(timeslotkey, m[0].timeslots, 'startdatetime').isJust) {
            match
              .findOneAndUpdate(
                {key: matchid}, 
                {
                  date: findDate(timeslotkey, m[0].timeslots, 'startdatetime').get(),
                  timeend: findDate(timeslotkey, m[0].timeslots, 'enddatetime').get(),
                  activeTimeslotKey: timeslotkey 
                }, 
                {new:true}
              )
              .exec()
              .then((m2) => {
                resolve(m2);
              });
          };

        } else {
          reject(401);
        };
      })
      .catch(err => reject(401))
    );
  });
});
exports.assignDates = assignDates;


// taskIncrementExtraPlayers :: account -> match -> s -> s -> n -> task model match
const taskIncrementExtraPlayers = R.curry((account, match, matchid, email, key, direction) => {
  return new Task((reject, resolve) => {
    accounts.TaskVerifyAnAccountFromEmail(account, email, key).fork(
      err => {
        reject(401);
      },
      user => findMatchByKey(match, matchid).then(function(m) {

        if (direction <= m[0].seatavailable) {
          match.findOneAndUpdate(
            {key: matchid, 'players.email': email}, 
            {$inc:{'players.$.extraplayers': direction, 'seatavailable': direction * -1}}, 
            {new:true}
          ).exec().then((m2) => {
            match.findOneAndUpdate(
              {key: matchid, 'players.email': email}, 
              checkFull(m2, {seatmax: m2.seatmax}), 
              {new:true}
            ).exec().then((m3) => {
              resolve(m3);
            }).catch(err => {
              reject(401);
            })

          }).catch(err => {
            reject(401);
          })
        } else {
          reject(401)
        }

      })
    );
  });

});
exports.taskIncrementExtraPlayers = taskIncrementExtraPlayers;

// findExpansions :: {o} -> s -> [{o}]
const findExpansions = R.curry((gameslug, matchobj) => {
  return R.compose(
    R.chain(S.filter(R.propEq('gamepageslug', gameslug))),
    R.chain(S.prop('assignedexpansions')),
    S.head
  )(matchobj).getOrElse([])
});
exports.findExpansions = findExpansions;

// taskAddExpansion :: schema -> schema -> s -> s -> s -> s -> o -> task [o]
const taskAddExpansion = R.curry((account, match, matchkey, email, userkey, userslug, gameslug, expansion) => {

  // pickMergeExpansion :: o -> o
  const pickMergeExpansion = R.compose(
    R.objOf('$push'),
    R.objOf('assignedexpansions'),
    R.merge({email: email}),
    R.merge({userpageslug: userslug}),
    R.merge({gamepageslug: gameslug}),
    R.pick(['id', 'name', 'thumbnail', 'status', 'description', 'minplaytime', 'maxplaytime', 'minplayers', 'maxplayers', 'weight', 'bggrank', 'avgwating', 'playercounts'])
  );

  return new Task((reject, resolve) => {
    accounts.ReadVerifyAccountFromEmail(account, email, userkey).then(function(user) {
      if (user.length == 1) {
        findMatchByKey(match, matchkey)
          .then(function(m) {
            if (findPlayerInMatch(email)(m).getOrElse(false)) {
              match.findOneAndUpdate(
                {key: matchkey}, 
                pickMergeExpansion(expansion),
                {new:true}
              )
                .exec()
                .then(function(match) {
                  resolve (findExpansions(gameslug, [match]));
                })
                .catch(function(err) {
                  resolve([])
                })

            } else {
              reject(401);

            };

          })
          .catch(err => reject(401));

      } else {
        reject(401);
      }
    });
  });
})
exports.taskAddExpansion = taskAddExpansion;


// taskRemoveExpansion :: schema -> schema -> s -> s -> s -> s -> n -> task [o]
const taskRemoveExpansion = R.curry((account, match, matchkey, email, userkey, gameslug, expansionid) => {

  return new Task((reject, resolve) => {
    accounts.ReadVerifyAccountFromEmail(account, email, userkey).then(function(user) {

      if (user.length == 1) {
        match.findOneAndUpdate(
          {key: matchkey}, 
          {$pull: {assignedexpansions: {id: expansionid, gamepageslug: gameslug}}},
          {new:true}
        )
          .exec()
          .then(function(match) {
            resolve (findExpansions(gameslug, [match]));
          })
          .catch(function(err) {
            resolve([])
          })

      } else {
        reject(401);
      }
    });
  });

})
exports.taskRemoveExpansion = taskRemoveExpansion;

// taskReadExpansions :: schema -> s -> s -> [o]
const taskReadExpansions = R.curry((match, matchkey, gameslug) => {

  return new Task((reject, resolve) => {
    findMatchByKey(match, matchkey).then(function(event) {
      resolve(findExpansions(gameslug, event));
    });
  });

})
exports.taskReadExpansions = taskReadExpansions;

// objectify :: mongo -> o
const objectify = (a) => {
	try {
		return a.toObject();
	} catch (err) {
		return a;
	}
}

// findInterestedEmail :: o -> maybe [o]
const findInterestedEmail = (data) => R.compose(
	R.map(R.map(R.prop('email'))),
	R.chain(S.filter(R.propEq('type', 'interested'))),
	R.map(objectify),
	S.prop('players')
)(data);
exports.findInterestedEmail = findInterestedEmail;


// findEventReminderEmails :: m -> n -> task [a]
var findEventReminderEmails = R.curry(function(match, textDuration, durationHours) {

	// taskSendInfo :: o -> [ task o ]
	const taskSendInfo = (anevent) => {
		return findInterestedEmail(anevent)
			.getOrElse([])
			.map(emailer.sendInterestReminderEmails(textDuration, anevent));
	};

	return new Task(function(reject, resovle) {
		match.find({
			'players.type':'interested',
			'date': {
				'$lt'	: moment().add(durationHours + 1, 'h').format(),
				'$gte': moment().add(durationHours, 'h').format()
			}
		}).exec().then((events) => {
			R.sequence(Task.of, R.flatten(events.map(taskSendInfo)))
				.fork(err=>{}, data=>{});

		});
	});
});
exports.findEventReminderEmails = findEventReminderEmails;


// addJoinGame :: s -> n -> maybe [a] 
const addJoinGame = R.curry((email, userkey, username, avataricon, value, gamekey) => R.compose(
  R.map(R.objOf('$set')), 
  R.map(R.objOf('games.$.join')), 
  R.map(pushVote(value, email, userkey, username, avataricon)), 
  R.chain(S.prop('join')), 
  R.chain(S.find(propEq('gamekey', gamekey))), 
  R.chain(S.prop('games')), 
  S.head)
);
exports.addJoinGame = addJoinGame;

// joinLength :: a -> n
const joinLength = R.compose(
  R.length,
  R.prop('join')
);
exports.joinLength = joinLength;


// TODO write tests for the functions below, used in isGameFull
//
// calcBaseGamePlayers :: o -> maybe bool
const calcBaseGamePlayers = R.curry((gamekey, eventobj) => R.compose(
  R.map(R.converge(R.gte, [joinLength, R.prop('maxplayers')])),
  R.chain(S.find(R.propEq('gamekey', gamekey))),
  S.prop('games')
)(eventobj));
exports.calcBaseGamePlayers = calcBaseGamePlayers;

// findBaseGameJoinLength :: o -> maybe n 
const findBaseGameJoinLength = R.curry((gamekey, eventobj) => R.compose(
  R.map(joinLength),
  R.chain(S.find(R.propEq('gamekey', gamekey))),
  S.prop('games')
)(eventobj));
exports.findBaseGameJoinLength = findBaseGameJoinLength;

// findBaseGamePageSlug :: o -> maybe n 
const findBaseGamePageSlug = R.curry((gamekey, eventobj) => R.compose(
  R.chain(S.prop('pageslug')),
  R.chain(S.find(R.propEq('gamekey', gamekey))),
  S.prop('games')
)(eventobj));
exports.findBaseGamePageSlug = findBaseGamePageSlug;

// calcExpansionGamePlayers :: s -> o -> maybe n
const calcExpansionGamePlayers = R.curry((slug, eventobj) => R.compose(
  R.map(R.apply(Math.max)),
  R.chain(R.sequence(maybe.of)),
  R.map(R.chain(S.prop('maxplayers'))),
  R.chain(S.filter(R.propEq('gamepageslug', slug))),
  S.prop('assignedexpansions')
)(eventobj));
exports.calcExpansionGamePlayers = calcExpansionGamePlayers;


// isGameFull :: s -> [o] -> maybe bool
const isGameFull = R.curry((gamekey, arr) => {
  return S.head(arr).cata({
    Nothing: () => maybe.Nothing(), 
    Just: (a) => (R.has('assignedexpansions', a))
      ? findBaseGamePageSlug(gamekey, a).cata({
          Nothing: () =>  maybe.Nothing(),
          Just: (slug) => R.lift(R.gte)(findBaseGameJoinLength(gamekey, a), calcExpansionGamePlayers(slug, a)).cata({
            Nothing: () =>  calcBaseGamePlayers(gamekey, a), // no expansions for this game
            Just: (bool) => maybe.of(bool)
          })
        })
      : calcBaseGamePlayers(gamekey, a)  // there are no expansions
  });

});
exports.isGameFull = isGameFull;


// joinGame :: account -> match -> s -> s -> s -> s -> task o
const joinGame = R.curry((account, match, matchid, gamekey, email, key) => {
  return new Task((reject, resolve) => {
    accounts.TaskVerifyAnAccountFromEmail(account, email, key).fork(
      err => reject(401),
      user => {
        findMatchByKey(match, matchid)
          .then(function(m) {
            if (findPlayerInMatch(email)(m).getOrElse(false)) {
              if (whenHasJoinGame(email, gamekey)(m).getOrElse(false)) {
                reject(409);

              } else {
                if (isGameFull(gamekey, m).getOrElse(false)) {
                  reject(405);

                } else {
                  match.findOneAndUpdate(
                    {key: matchid, 'games.gamekey': gamekey}, 
                    addJoinGame(email, user.toObject().pageslug, user.toObject().profile.name, user.toObject().profile.avataricon, 1, gamekey)(m).getOrElse({}), 
                    {new:true}
                  ).exec().then((m2) => {
                    resolve(S.prop('games', m2)
                      .map(R.objOf('games'))
                      .getOrElse({games:[]})
                    );

                  })
                }
              }

            } else {
              reject(401);

            }

          })
          .catch(err => reject(401));

      }
    );
  });
}); 
exports.joinGame = joinGame;

// removeJoinGame :: s -> s -> maybe [a] 
const removeJoinGame = R.curry((email, gamekey) => R.compose(
  R.map(R.objOf('$set')), 
  R.map(R.objOf('games.$.join')), 
  R.map(R.reject(propEq('email', email))), 
  R.chain(S.prop('join')), 
  R.chain(S.find(propEq('gamekey', gamekey))), 
  R.chain(S.prop('games')), 
  S.head)
);
exports.removeJoinGame = removeJoinGame;

// getOrNothing :: maybe a -> a || nothing
const getOrNothing = (a) => a.getOrElse(maybe.Nothing());

// whenHasJoinGame :: s -> s -> maybe bool
const whenHasJoinGame = R.curry((email, gamekey) => R.compose(
  R.chain(S.find(R.propEq('email', email))), 
  R.map(R.flatten),
  getOrNothing,
  R.map(R.sequence(maybe.of)),
  R.map(R.map(S.prop('join'))),
  R.chain(S.prop('games')), 
  S.head
));
exports.whenHasJoinGame = whenHasJoinGame;

// leaveGame :: account -> match -> s -> s -> s -> s -> Task o
const leaveGame = R.curry((account, match, matchid, gamekey, email, key) => {
  return new Task((reject, resolve) => {
    accounts.TaskVerifyAnAccountFromEmail(account, email, key).fork(
      err => reject(401),
      user => findMatchByKey(match, matchid).then(function(m) {
        if (findPlayerInMatch(email)(m).getOrElse(false)) {
          if (whenHasJoinGame(email, gamekey)(m).getOrElse(false)) {
            match.findOneAndUpdate(
              {key: matchid, 'games.gamekey': gamekey}, 
              removeJoinGame(email, gamekey)(m).getOrElse({}), 
              {new:true}
            ).exec().then((m2) => {
              resolve(S.prop('games', m2).map(R.objOf('games')).getOrElse([]));
            })
          } else {
            reject(409);
          }
        } else {
          reject(401);
        }
      })
      .catch(err => reject(401))
    );
  });
}); 
exports.leaveGame = leaveGame;
