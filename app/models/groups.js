// function for the purpose of managing groups
var Promise = require('bluebird');
var R = require('ramda');
var S = require('../lambda.js');

var account = require('./account.js');
var accounts = require('./accounts.js');
var matches_service = require('./../matches_service.js');

var group = require('./group.js');

var express = require('express');
var bodyParser = require('body-parser')

var Task = require('data.task');
var Maybe = require('data.maybe');
var Either = require('data.either');

var log = require('../log.js');
var geolib = require('geolib');
var exports = module.exports = {};

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// safeBody :: k => maybe o 
const safeBody = R.curry((key, req) => S.path(['body', key], req));

// maybeMatch :: o -> Maybe a
const maybeMatch = S.path(['params', 'match']);
exports.MaybeMatch = maybeMatch;

// maybeGetQuery :: o -> o
const maybeGetQuery = (req) =>
  S.path(['params', 'groupurl'], req) 
    .map(R.objOf('url'));
exports.maybeGetQuery = maybeGetQuery;

// maybeKey :: obj => Maybe(key);
const maybeKey = (req, pathkey) => (S.path([pathkey, 'key'], req).isJust) 
  ? S.path([pathkey, 'key'], req) 
  : S.path(['user', 'key'], req); 
exports.MaybeKey = maybeKey;

// updateLoc :: o -> maybe o
const updateLoc = (data) => {

  if (R.has('playarea', data) &&
    R.has('playarealng', data) &&
    R.has('playarealat', data)) {

    return R.compose(
      Maybe.of, 
      R.merge({locname: data.playarea}),
      R.merge({loc: {
        type: 'Point',
        coordinates: [
          Number(data.playarealng),
          Number(data.playarealat)
        ]
      }})
    )(data);

  } else {
    return Maybe.Nothing();

  };


};
exports.updateLoc = updateLoc;


// updateIfExists :: o -> o
var updateIfExists = function(body) {
  if (R.has('playarea', body)) {
    return(updateLoc(body));
  } else {
    return {}
  };
};
exports.updateIfExists = updateIfExists;


// createGroup :: o -> o -> promise schema 
const createGroup = R.curry((groupdata) => {

  // check location ehre and add
  return new Promise(function(fulfill, reject) {
    group.create(updateLoc(groupdata).getOrElse({}), function(err, newgroup) {
      (err)
        ? reject(err)
        : fulfill(newgroup);
    });

  });
});
exports.createGroup = createGroup;

// cleanObj :: o -> o
const cleanObj = R.compose(
  R.pick(['name', 'description', 'url', 'members', 'playarea', 'playarealat', 'playarealng', 'locname']) // fields returned on query
                                                    // and allowed on creation (members prolly shouldn't be ?)
);
exports.cleanObj = cleanObj;

// buildCreateData :: s -> s -> s -> o 
const maybeBuildUserData = R.curry((avataricon, pageslug, name) => ({
  members: [
    { 
      pageslug:  pageslug, 
      avataricon: avataricon, 
      name:   name,
      status: 'host'
    }
  ]
}));
exports.maybeBuildUserData = maybeBuildUserData;

// isHostSlug :: s -> o -> bool
const isHostSlug = R.curry((slug, memberObj) => 
  R.propEq('status', 'host', memberObj) && R.propEq('pageslug', slug, memberObj)
);
exports.isHostSlug = isHostSlug;

// buildObj :: o -> s -> o 
const buildObj = R.curry((req, key) =>
  S.path(['body', key], req).map(R.objOf(key)).getOrElse({})
);
exports.buildObj = buildObj;

/*
// getHostEmail :: [o] -> maybe s 
const getHostEmail = R.compose(
  R.chain(S.prop('email')),
  S.find(R.propEq('status', 'host')),
);
exports.getHostEmail = getHostEmail;

// isPlayerTypeHost :: s => [o] -> maybe bool 
const isPlayerTypeHost = (email) => R.compose(
  R.map(R.propEq('type', 'host')),
  S.find(R.propEq('email', email))
);
exports.isPlayerTypeHost = isPlayerTypeHost;

// filterPlayer :: e -> {o} -> Bool
const filterPlayer = R.curry((email, player) => (
  (S.prop('email', player).getOrElse('bademail') == email) || 
  (S.prop('type', player).getOrElse('') == 'host') || 
  (S.prop('type', player).getOrElse('') == 'approve') ||
  (S.prop('type', player).getOrElse('') == 'invite') ||
  (S.prop('type', player).getOrElse('') == 'request') 
));
exports.filterPlayer = filterPlayer;

// filterPlayers :: e -> {g} -> [{o}]
const filterPlayers = R.curry((email, match) => S
  .prop('players', match)
  .map(R.filter(filterPlayer(email)))
  .getOrElse([]));
exports.filterPlayers = filterPlayers;
*/

// taskTransformUsers :: s -> o -> Task({p})
const taskTransformUsers = R.curry((pageslug, u) => new Task((reject, resolve) => {

  const userSettings = {
    'host':     'fa fa-home',
    'approve':  'fa fa-handshake-o',
    'request':  'fa fa-hand-paper-o',
    'interested'	:	'fa fa-bell',
    'invite':   'fa fa-envelope-o',
    'reject':   'fa fa-gavel'
  };

  S.prop('pageslug', u)
    .map(accounts.TaskAccountFromSlug(account))
    .getOrElse(taskFail())
    .fork(
      err => log.clos('err', err), 
      data => {
        if (R.isNil(data) && u.type !== 'invite') {
          resolve({});

        } else {
          resolve({
            name:         matches_service.GetName(u, data),
            nameclass:    matches_service.BuildNameClass(data),
            imgtype:      'img',
            imgclass:     matches_service.BuildProfileClass(data),
            playlevel:    S.path(['profile', 'playlevel'], data).getOrElse(0),
            playerkey:    S.prop('playerkey', u).getOrElse(0),
            status:       S.prop('status', u).getOrElse(''),
            statusfaicon: S.prop('status', u).chain(S.prop(R.__, userSettings)).getOrElse(''),
            imgsrc:       S.path(['profile', 'avataricon'], data)
              .map(matches_service.whenUploadLocaliseURL)
              .getOrElse('/img/default-avatar-sq.jpg'), 
            link:         matches_service.BuildLink(data),
            badges:       matches_service.BuildBadges(data),
            pageslug:     S.prop('pageslug', data).getOrElse(''),
            activeuser:   S.prop('pageslug', u).chain(S.equals(pageslug)).isJust
          })

        }
      }
    )

}));




// objectify :: o -> o;
const objectify = (obj) => obj.toObject();

// liftVerify :: (Maybe(u) -> Maybe(ky) => Task(res.json) 
const liftVerify = R.lift(accounts.TaskVerifyAnAccountFromEmail(account));

// geofindQuery :: [n -> n] -> obj
const geofindQuery = (lng, lat) => ({
  'loc': {
    $near: {
      $geometry: {
        type: "Point" ,
        coordinates: R.map(Number, [lng, lat])
      },
      $maxDistance: 5000,
      $minDistance: 0 
    }
  }
});


// setupRouter 
const setupRouter = function() {

  const router = express.Router();
  const jsonParser = bodyParser.json();

  // liftMaybeBuildUserData :: maybe s -> maybe s -> maybe o
  const liftMaybeBuildUserData = (user) => R.lift(
    maybeBuildUserData(S.path(['profile', 'avataricon'], user).getOrElse('/img/default-avatar-sq.jpg'))
  );

  // buildCreateData :: o -> maybe o
  const maybeBuildCreateData = R.curry((req, user) => {
    return R.lift(R.merge) (
      liftMaybeBuildUserData(user)(S.prop('pageslug', user), S.path(['profile', 'name'], user)),
      S.prop('body', req).map(cleanObj)
    );
  });

  const liftGeoFindQuery = R.lift(geofindQuery);

  router.route('/')
    .get(jsonParser, function(req, res) {
      if (S.path(['query', 'coordinates'], req).isJust) {
        group
          //.find(geofindQuery(S.path(['query', 'lng'], req).getOrElse([])))
          .find(liftGeoFindQuery(S.path(['query', 'lng'], req), S.path(['query', 'lat'], req)))
          .limit(50)
          .exec()
          .then((grp) => {
            R.isNil(grp)
              ? res.sendStatus(404) 
              : res.json(grp.map(objectify).map(cleanObj))
          }).catch((err) => {
            res.sendStatus(404);
          });

      } else {
        liftVerify(S.path(['query', 'email'], req), maybeKey(req, 'query'))
          .getOrElse(taskFail()).fork(
            (err) => {

              if (S.path(['query', 'email'], req).isJust) {
                res.sendStatus(401);

              } else {
                group
                  .find({isprivate: false})
                  .limit(50)
                  .exec()
                  .then((grp) => {
                    R.isNil(grp)
                      ? res.sendStatus(404) 
                      : res.json(grp.map(objectify).map(cleanObj))
                  });

              }

            },
            (user) => {
              group
                .find({'members.pageslug': user.pageslug})
                .limit(50)
                .exec()
                .then((grp) => {
                  R.isNil(grp)
                    ? res.sendStatus(404) 
                    : res.json(grp.map(objectify).map(cleanObj))
                });

            }
          )
      }

      // is there a valid user?
      //  yes -> show the users data + nearby
      //  no  -> show top activity groups
      //  allow -> show your location

    })
    .post(jsonParser, function(req, res) {
      liftVerify(S.path(['body', 'email'], req), maybeKey(req, 'body'))
        .getOrElse(taskFail()).fork(
          (err) => res.sendStatus(401),
          (user) => {

            createGroup(maybeBuildCreateData(req, user.toObject()).getOrElse({}))
              .then((grp) => {
                res.json(cleanObj(grp.toObject()))
              })
              .catch((e) => {
                res.sendStatus(500);
              }); 
          }
        )

    });

  router.route('/:groupurl')
    .get(function(req, res) {
      group
        .findOne({url: req.params.groupurl})
        .exec()
        .then((grp) => {
          if (R.isNil(grp)) {
            res.sendStatus(404) 
          } else {

            // getPageSlug :: o -> o
            const getPageSlug = (req) => 
              S.path(['user', 'pageslug'], req)
                .getOrElse(S.path(['body', 'pageslug'], req).getOrElse('BADKEY'));

            R.sequence(Task.of, R.map(taskTransformUsers(getPageSlug(req)), grp.members)).fork(
              err => res.sendStatus(500),
              data => res.json( R.merge( cleanObj(grp.toObject()), {members: data} ))
            );

          }
        });
    })
    .patch(jsonParser, function(req, res) {

      // editable fields
      const fields = ['name', 'description', 'url']

      liftVerify(S.path(['body', 'email'], req), maybeKey(req, 'body'))
        .getOrElse(taskFail()).fork(
          (err) => res.sendStatus(401),
          (user) => {

            group
              .findOneAndUpdate(
                {url: req.params.groupurl}, 
                {$set: R.reduce(R.merge, updateLoc(req.body).getOrElse({}), fields.map(buildObj(req)))}, 
                {new: true}
              )
              .exec()
              .then((grp) => {

                (R.length(R.filter(isHostSlug(user.pageslug), grp.members)) > 0)
                  ? R.isNil(grp)
                    ? res.sendStatus(404) 
                    : res.json(cleanObj(grp.toObject()))
                  : res.sendStatus(401);

              });
          }
        );
    })
    .delete(jsonParser, function(req, res) {

      liftVerify(S.path(['body', 'email'], req), maybeKey(req, 'body'))
        .getOrElse(taskFail()).fork(
          (err) => res.sendStatus(401),
          (user) => {
            group
              .findOne({url: req.params.groupurl})
              .exec()
              .then((grp) => {
                (R.length(R.filter(isHostSlug(user.pageslug), grp.members)) > 0)
                  ? R.isNil(grp)
                    ? res.sendStatus(404) 
                    : group
                        .deleteOne({url: req.params.groupurl})
                        .exec()
                        .then((result) => (result.deletedCount > 0)
                          ? res.json({status:'ok'})
                          : res.sendStatus(500)
                        )
                  : res.sendStatus(401);

              });
          }
        );
      
    })
    

  return router;

}; 
exports.SetupRouter = setupRouter;
