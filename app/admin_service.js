/**************************************************
**
** an ajax service for all things to do with admin stats and abilities
**
*************************************************/

var express     = require('express');
var bodyParser  = require('body-parser');
var log         = require('./log.js');
var jsonParser  = bodyParser.json();

var account   = require('./models/account.js');
var accounts  = require('./models/accounts.js');
var R = require('ramda');
var S = require('./lambda.js');


// maybeEmail :: o -> s -> maybe s;
const maybeEmail = (req, pathkey) => 
  (S.path([pathkey, 'email'], req).isJust) 
    ? S.path([pathkey, 'email'], req) 
    : S.path(['user', 'email'], req); 

// maybeKey :: o -> s -> maybe s;
const maybeKey = (req, pathkey) => 
  (S.path([pathkey, 'key'], req).isJust) 
    ? S.path([pathkey, 'key'], req) 
    : S.path(['user', 'key'], req); 


// verifyUser :: Maybe s -> Maybe s -> Maybe Task o
const verifyUser = R.lift(accounts.TaskVerifyAnAccountFromEmail(account));

// setupRouter :: express 
var setupRouter = function() {

  var router = express.Router();

  router.route('/netpromoterscore')
    .get(function(req, res) {
      verifyUser(
        maybeEmail(req, 'query'),
        maybeKey(req, 'query')
      ).cata({
        Nothing:  () => res.sendStatus(401),
        Just:     (u)=> u.fork(
          ()  => res.sendStatus(401),
          someuser => (someuser.premium.isstaff)
            ? accounts.getUserCountsForAdmin(account).fork(
                ()  =>res.sendStatus(401),
                data=>res.json(data)
              )
            : res.sendStatus(401)
        )
      });
    });

  router.route('/netpromoterscore/monthly')
    .get(function(req, res) {
      verifyUser(
        maybeEmail(req, 'query'),
        maybeKey(req, 'query')
      ).cata({
        Nothing:  () => res.sendStatus(401),
        Just:     (u)=> u.fork(
          ()  => res.sendStatus(401),
          someuser => (someuser.premium.isstaff)
            ? accounts.getUserCountsForAdminMonthly(account).fork(
                ()  =>res.sendStatus(401),
                data=>{
                  res.json(data)
                }
              )
            : res.sendStatus(401)
        )
      });

    });



  return router;

};
exports.SetupRouter = setupRouter;
