var accounts = require('../app/models/accounts.js');
var account = require('../app/models/account.js');

var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var Promise = require('bluebird');
var R = require('ramda');
var assoc   = R.assoc,
    compose = R.compose,
    composeP = R.composeP,
    contains = R.contains,
    curry   = R.curry,
    dissoc  = R.dissoc,
    find    = R.find,
    filter  = R.filter,
    gt      = R.gt,
    head    = R.head,
    isEmpty = R.isEmpty,
    isNil   = R.isNil,
    lift    = R.lift,
    nth     = R.nth,
    map     = R.map,
    mapAccum = R.mapAccum,
    merge   = R.merge,
    path    = R.path,
    pick    = R.pick,
    prop    = R.prop,
    propEq  = R.propEq,
    reject  = R.reject,
    sortBy  = R.sortBy,
    sum     = R.sum,
    zipObj  = R.zipObj;

var log = require('./log.js');
var expressHelper = require('./expresshelper');

var geolib = require('geolib');

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

const clr = ['#EBF4FC', '#C9E4EC', '#96CCD4', '#52ACB4', '#32686C'];

const fullmotivelist = [ 'motivecompetition', 'motivecommunity', 'motivestrategy', 'motivestory', 'motivedesign', 'motivediscovery', 'motivecooperation', 'motivesocialmanipulation' ];

// safeProp :: (a -> b) => Maybe(b[a])
const safeProp = curry((x, o) => (isEmpty(o) || isNil(o) || isNil(o[x])) ? Maybe.Nothing() : Maybe.of(o[x]));

// safePath :: ([a] -> b) => Maybe(b[a])
const safePath = curry((x, o) => (isEmpty(o) || isNil(o) || isNil(path(x, o))) ? Maybe.Nothing() : Maybe.of(path(x, o)));

// safeFind :: (f(a) -> [b]) => Maybe(c) 
const safeFind = curry((a, b) => (find(a, b) == undefined) ?  Maybe.Nothing() : Maybe.of(find(a, b)));

// findProps :: ({obj} -> [props]) => [values];
const findProps = curry((data, fields) => map(prop(R.__, data), fields));
exports.FindProps = findProps;

// slashString :: String -> String
var slashString = (endpoint) => "/" + endpoint;

// renderData :: Render -> String -> Object -> Render 
var renderData = curry((res, template, data) => res.render(template, data));

// profileToFa :: (p -> f -> {o} -> t) => {f}
const faiconWhenTwo = curry((faval, tooltip, val) => (val == 2) ? {fa: {faicon: faval, faclass:'fatooltip', tooltip: tooltip}} : {});
exports.FaiconWhenTwo = faiconWhenTwo;

// buildMotive :: ([l] -> s) => {o}
const buildMotive = curry((motivelist, motive) => {

    // motives
    const motives = {
        motivecompetition   : {faicon: 'fa-bullseye', tooltip: 'Conflict. Such as stealing resources, blocking others or directly attacking other players.', id: 'motivecompetition'},
        motivecommunity     : {faicon: 'fa-users', tooltip: 'Spending Time With Others'},
        motivestrategy      : {faicon: 'fa-cogs', tooltip: 'Strategy. Thinking, planning, and decision making.'},
        motivestory         : {faicon: 'fa-book', tooltip: 'Experiencing a Great Story'},
        motivedesign        : {faicon: 'fa-pencil', tooltip: 'Aesthetics. High quality components, artwork, illustrations and deep theme.'},
        motivediscovery     : {faicon: 'fa-search-plus', tooltip: 'Discovering New Boardgames'},
        motivecooperation   : {faicon: 'fa-handshake-o', tooltip: 'Cooperation. Working together and teaming up.'},
        motivesocialmanipulation     : {faicon: 'fa-gavel', tooltip: 'Social Manipulation. Playing mind games, bluffing and deceit and persuasion.'}
    }

    return contains(motive, motivelist) ? compose(R.objOf('fa'), assoc('faclass', 'fatooltip'), prop(R.__, motives))(motive) : {}

});
exports.BuildMotive = buildMotive;

// buildPlayLevel :: a => s
const buildPlayLevel = (x) => (x == 0) ? '' : 'Play Level: ' + x;
exports.BuildPlayLevel = buildPlayLevel;

// showCompatibility :: n => {s}
const showCompatibility = (compatibility) => 
    (compatibility <= 65) ? {} : 
    (compatibility <= 80) ? {fa: {faicon: 'fa-thumbs-o-up', tooltip: 'Player compatibility ' + compatibility +'%', faclass: 'fatooltip'}}
    : {fa: {faicon: 'fa-thumbs-up', tooltip: 'High player compatibility ' + compatibility +'%', faclass: 'fatooltip'}}
exports.ShowCompatibility = showCompatibility;

// buildTooltip :: (n -> y -> b1 -> b2) => s
const buildTooltip = (name, year, userown, viewerown) => name + " (" + year + ") " + ((userown) ? "The player owns this game. " : "") + ((viewerown) ? "You own this game.": "");
exports.BuildTooltip = buildTooltip;

// buildClass :: (b1 -> b2) => s
const buildClass = (userown, viewerown) => {
   var tip = "fatooltip wtpimages"; 
   if (userown) {
       tip = (viewerown) ? "imglistsquareBothOwn " + tip : "imglistsquareUserOwn " + tip;
   } else {
       tip = (viewerown) ? "imglistsquareViewerOwn wow wobble " + tip : "imglistsquare " + tip;
   }
   return tip;

}
exports.BuildClass = buildClass;

// makeImg {x} => {y};
const makeImg = (data) => {

    const propname = prop('name');
    const year = prop('yearpublished');
    const userown = prop('userown');
    const viewerown = prop('viewerown');

    const zipImg = compose(R.objOf('img'), zipObj(['src', 'class', 'tooltip']));

    return zipImg([prop('thumbnail', data), buildClass(userown(data), viewerown(data)), buildTooltip(propname(data), year(data), userown(data), viewerown(data))]);

};
exports.MakeImg = makeImg;

// userPage :: (s1 -> s2) => {p}
const userPage = curry((aclass, name, pageslug) => ({a: {text: name, href: 'users?id=' + pageslug, class: aclass}}));
exports.UserPage = userPage;

// buildPageslug :: ({o} -> s) => {p} 
const buildPageslug = (obj) => 
  (safePropOrFalse('showstaff', obj)) 
    ? lift(userPage('a-name-staff'))(safePath(['profile', 'name'], obj), safeProp('pageslug', obj)).getOrElse({}) 
    : (safePropOrFalse('showpremium', obj))  
      ? lift(userPage('a-name-premium'))(safePath(['profile', 'name'], obj), safeProp('pageslug', obj)).getOrElse({}) 
      : lift(userPage(''))(safePath(['profile', 'name'], obj), safeProp('pageslug', obj)).getOrElse({})
exports.BuildPageslug = buildPageslug;

// compatibility :: {u} => {f}
const compatibility = (userObj) => safeProp('compatibility', userObj).map(showCompatibility).getOrElse({});
exports.Compatibility = compatibility;

// avataricon :: {u} => s
const avataricon = (userObj) => safePath(['profile', 'avataricon'], userObj).getOrElse('/img/default-avatar-sq.jpg');
exports.Avataricon = avataricon;

// getLevel :: (s -> {o}) => n 
const getLevel = curry((prop, obj) => safePath(['profile', prop], obj).getOrElse(0));
exports.GetLevel = getLevel;

// hostlevel :: {o} => {l}
const hostlevel = compose(merge({fa: {faicon: 'fa-home', tooltip: 'Hosting Level', faclass: 'fatooltip'}}), R.objOf('text'), getLevel('hostlevel'));
exports.HostLevel = hostlevel;

// playlevel :: {o} => {p}
const playlevel = compose(buildPlayLevel, getLevel('playlevel'));
exports.PlayLevel = playlevel;

// safeArr :: (s -> {o}) => [a]
const safeArr = curry((name, obj) => safeProp(name, obj).getOrElse([]))
exports.SafeArr = safeArr;

// isTeacher :: {o} => {f}
const safeTeacher = (obj) => safePath(['profile', 'isteacher'], obj).map(faiconWhenTwo('fa-graduation-cap', 'Likes Teaching Games to New Players')).getOrElse({});
exports.SafeTeacher = safeTeacher;

// safePropOrFalse :: (s -> {o}) => bool
const safePropOrFalse = curry((propname, obj) => safeProp(propname, obj).getOrElse(false));
exports.SafePropOrFalse = safePropOrFalse;


// sortGames :: {u} -> n
const sortGames = (u) => {
  const getId = prop('id');
  const getUserOwn = safeProp('userown');
  const getViewerOwn = safeProp('viewerown');
  return getId(u) + getUserOwn(u).map(R.add(getId(u))).getOrElse(0) + getViewerOwn(u).map(R.add(getId(u))).getOrElse(0);
}
exports.SortGames = sortGames;


// oneIftrue :: b => 1 || 0
const oneIftrue = (value) => (value) ? 1 : 0;
exports.OneIftrue = oneIftrue;


// calculatePlayerDistance :: Number -> Number -> Model(user) -> Model(user)
var calculatePlayerDistance = curry(function(latitude, longitude, player) {

    // pathCoord :: {o} => [lng, lat]
    const pathCoord = path(['profile', 'localarea', 'loc', 'coordinates']);

    // tolattitude :: {o} => lat 
    const tolatitude = compose(nth(1), pathCoord);

    // tolongitutde :: {o} => lng 
    const tolongitude = compose(nth(0), pathCoord);

    var distance = geolib.getDistanceSimple(
        {latitude: Number(latitude), longitude: Number(longitude)},
        {latitude: tolatitude(player), longitude: tolongitude(player)}
    );

    var prepMongo = compose(assoc('distance', distance));
    return prepMongo(player);

});
exports.CalculatePlayerDistance = calculatePlayerDistance;

// whenInDistance :: {u} => true || false
const whenInDistance = (user) => ((safePath(['profile', 'searchradius'], user).getOrElse(50) * 1000) > prop('distance', user)); 
exports.WhenInDistance = whenInDistance;

// whenNotPrivate :: {u} => true || false
const whenNotPrivate = (user) => (path(['profile', 'visibility'], user) != 2);
exports.WhenNotPrivate = whenNotPrivate;

// readMotive :: ({u} -> s) => m
const readMotive = curry((user, motive) => path(['profile', motive], user) ? motive : '');
exports.ReadMotive = readMotive;

// calcMotive :: (d -> {u} -> s) => m
const calcMotive = curry((motiveDNA, user, motive) => path(['profile', motive], user) == prop(motive, motiveDNA) ? 12.5 : 0);
exports.CalcMotive = calcMotive;


// buildPlayerList :: (lng -> lat -> [{u1}..{u2}, [{g}]] -> {d}) => {m}
const buildPlayerList = curry(function(lng, lat, users, motiveDNA) {

    // mergeIfPremium :: f({x}); 
    const mergeIfPremium = (user) => compose( merge(accounts.SafeWhenStaff({showstaff: true}, {}, user)), merge(accounts.SafeWhenPremium({showpremium: true}, {}, user)) )(user);

    // arraifyMotives :: {o} => {b}; 
    const arraifyMotives = (user) => assoc('motives', reject(isEmpty, map(readMotive(user), fullmotivelist)), user);

    // calcCompatibility :: ({d} -> {o}) => {b}; 
    const calcCompatibility = curry((motivedna, user) => assoc('compatibility', sum(map(calcMotive(motivedna, user), fullmotivelist)), user));

    // playerlist :: [{u1}] => [{u2}]
    const playerlist = R.compose(R.objOf('playerlist'), filter(whenNotPrivate), filter(whenInDistance), map(calculatePlayerDistance(lat, lng)), map(arraifyMotives), map(calcCompatibility(motiveDNA)), map(pick(['pageslug', 'profile', 'wtpplay', 'showpremium', 'showstaff'])), map(mergeIfPremium)); 

    return playerlist(users);

});
exports.BuildPlayerList = buildPlayerList;

// buildareaData :: (Schema(account) -> e -> d -> {loc}) => Task({profiles});
const buildAreaData = curry(function(account, email, distance, loc) {
    return new Task((reject, resolve) => {
        accounts.FindPlayersNearby(account, email, distance).then(function(allusers) {

            // baseMotives :: (e -> [{u}]) => f([Model(users)] => [{m}])
            const baseMotives = curry((email, allusers) => compose(pick(fullmotivelist), prop('profile'), head, filter(propEq('email', email)))(allusers))

            // isGameOwned :: {o} -> b
            const isGameOwned = R.path(['matching', 'own'])

            // isGameWTP :: {o} -> b
            const isGameWTP = R.path(['matching', 'wanttoplay'])

            // userOwnsGames :: (e -> [{u}] -> g) => Maybe({m})
            const userOwnsGames = curry((email, allusers, gameid) => compose(safeFind(propEq('id', gameid)), filter(isGameOwned), prop('games'), find(propEq('email', email)))(allusers))

            // transAddHas :: {a} => {b}
            const transAddViewerOwn = curry((email, allusers, o) => userOwnsGames(email, allusers, o.id).isJust ? assoc('viewerown', true, o) : o);

            // transAddOwnership :: {a} => {b}
            const transAddUserOwn = (o) => (path(['matching', 'own'], o)) ? assoc('userown', true, o) : o;

            // wtpGames :: (e -> [{u}]) => f([Model(users)] => {m})
            const userWTPGames = compose(R.head, R.splitAt(4), R.reverse, sortBy(sortGames), map(pick(['name', 'yearpublished', 'thumbnail', 'id', 'numplays', 'userown', 'viewerown'])), map(transAddViewerOwn(email, allusers)), map(transAddUserOwn), R.filter(isGameWTP), prop('games'))

            // evolveUsers :: [Model({u})] => [{p}]
            const evolveUsers = compose(map(dissoc('games')), map(R.converge(assoc('wtpplay'), [userWTPGames, R.identity])), JSON.parse, JSON.stringify);
            
            resolve(buildPlayerList(loc[0], loc[1], evolveUsers(allusers), baseMotives(email, allusers)));

        }).catch(reject);
    });
});


// buildFaIcon :: i => {o} 
const buildFaIcon = (faicon) => R.objOf('class', 'fa fa-' + faicon + ' fa-fw');
exports.BuildFaIcon = buildFaIcon;

// buildImage :: (c -> s) => {o}
const buildImage = (classes, source) => zipObj(['class', 'src'], [classes, source]);
exports.BuildImage = buildImage;

// buildHeading :: (n -> i -> v) => {o}
const buildHeading = (name, icon, faarea, area, value) => zipObj(['name', icon, 'h4'], [name, value, {faicon: faarea, text: area}]); 
exports.BuildHeading = buildHeading;

// buildPage = (u -> n -> i -> a -> v) => {o}
const buildPage = curry((url, name, icon, faarea, area, value) => zipObj(['url', 'handlebars'], [url, {heading: buildHeading(name, icon, faarea, area, value)}]));
exports.BuildPage = buildPage;

// maybeDistance :: {u} => a 
const maybeDistance = (req) => safePath(['user', 'profile', 'searchradius'], req).getOrElse(50) * 1000;
exports.MaybeDistance = maybeDistance;

// checkVisibility :: ({o} -> n) => {p} 
const checkVisibility = curry((obj, y) => (y == 2) ? assoc('novisibility', true, obj) : obj);
exports.CheckVisibility = checkVisibility;

// showwarning :: ({r} -> {o}) => {p}
const showWarning = curry((req, obj) => safePath(['user', 'profile', 'visibility'], req).map(checkVisibility(obj)).getOrElse(obj));
exports.ShowWarning = showWarning;

// untilUpdatedAssocMotives :: {r} => f()
const untilUpdatedAssocMotives = (req) => {

    const motives = ['motivediscovery', 'motivecommunity', 'motivestory', 'motivecooperation', 'motivecompetition', 'motivesocialmanipulation', 'motivestrategy', 'motivedesign'];

    // whenTrueOne :: b => n
    const whenTrueOne = (a) => a ? 1 : 0

    // appender (a -> b) => [c, c]
    var appender = (a, b) => [a + b, a + b];

    // hasMotive :: [{o}} => {p}
    const hasMotive = compose(gt(R.__, 0), head, mapAccum(appender, 0), map(whenTrueOne), R.props(motives));

    const sections = [{
        questions: [{
            checkbox: {
                id: "inspiration",
                options: [
                    {option: 'Discovering New Boardgames.',id: 'motivediscovery'},
                    {option: 'Social. Spending time with others.', id: 'motivecommunity'},
                    {option: 'Experiencing a Great Story.', id: 'motivestory'},
                    {option: 'Cooperation. Working together and teaming up.', id: 'motivecooperation'},
                    {option: 'Conflict. Such as stealing resources, blocking others or directly attacking other players.', id: 'motivecompetition'},
                    {option: 'Social Manipulation. Playing mind games, bluffing and deceit and persuasion.', id: 'motivesocialmanipulation'},
                    {option: 'Strategy. Thinking, planning, and decision making.', id: 'motivestrategy'},
                    {option: 'Aesthetics. High quality components, artwork, illustrations and deep theme.', id: 'motivedesign'},
                ]
            }
        }]
    }];

    return safePath(['user', 'profile'], req).map(hasMotive).getOrElse(true) ? merge({}) : assoc('sections', sections); 

}

// mergePage :: {o} => f(s) => {q}
const mergePage = (req) => compose(merge, prop('handlebars'), buildPage('lounge', 'Nearby Players', 'i', 'users', safePath(['user', 'profile', 'localarea', 'name'], req).getOrElse('Location Not Set')), buildFaIcon);
exports.MergePage = mergePage;

// routePlayerList :: (Express -> Object -> Model) => Promise() 
var routePlayerList = curry(function (expressApp, CheckLogin, account) {
    return new Promise(function(fulfill, reject) {

        expressApp.get(slashString('lounge'), CheckLogin.ensureLoggedIn(slashString('login')), function(req, res){

            // maybebuildNavs :: {o} => {m} 
          const maybeBuildNavs = (req) => safePath(['user', 'email'], req).map(expressHelper.BuildNavbar('lounge')).map(R.objOf('navbarul'));

            // composeUserData :: ({u}) => express.render
            const composeUserData = curry((data) => composeP(renderData(res, 'lounge'), merge(safePath(['user', 'profile', 'localarea', 'name'], req).map(expressHelper.BuildNavbarHead).getOrElse('Location Not Set')), merge(maybeBuildNavs(req).getOrElse({})), untilUpdatedAssocMotives(req), showWarning(req), mergePage(req)('users'), accounts.BuildNavbars(account)));

            // note user activity
            safePath(['user', 'email'], req).chain(accounts.TaskRecordActivity(account));
            safePath(['user', 'email'], req).map(composeUserData({}))

        });
    });
});
exports.RoutePlayerList = routePlayerList;
