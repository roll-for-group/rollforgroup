var log = require('../app/log.js');
var R = require('ramda');

var curry   = R.curry,
    map     = R.map,
    lift    = R.lift;

var Maybe   = require('data.maybe');
var Either  = require('data.either');
var Task    = require('data.task'); 
var Promise = require('bluebird');

var accounts = require('./models/accounts.js');
var playersPage = require('./players_page.js');
var S = require('./lambda.js');

var match_api = require('./matches_service.js');

// removeUser :: [{u}] -> s => [{o}]
const removeUser = (iousers, socket) => R.reject(R.propEq('socketid', socket), iousers);
exports.RemoveUser = removeUser;

// buildUser :: s -> n -> a -> n -> p => {o}
const buildUser = R.curry((socket, avataricon, imgclass, textclass, link, name, playlevel, pageslug) => R.zipObj(['socketid', 'name', 'avataricon', 'playlevel', 'pageslug', 'imgclass', 'textclass', 'link'], [socket, name, avataricon, playlevel, pageslug, imgclass, textclass, link]));
exports.BuildUser = buildUser;

// safeBuildUser :: s -> {u} => Maybe({o})
const safeBuildUser = curry((socket, user) => {
    return lift(buildUser(socket, S.path(['profile', 'avataricon'], user).getOrElse('/img/default-avatar-sq.jpg'), match_api.BuildProfileClass(user), match_api.BuildNameClass(user), match_api.BuildLink(user)))(S.path(['profile', 'name'], user), S.path(['profile', 'playlevel'], user), S.prop('pageslug', user))
});
exports.SafeBuildUser = safeBuildUser;

// sendUserList :: [{ul}] => [{ul}]
const uniqueUserList = R.compose(map(R.omit(['socket'])), R.uniqBy(R.prop('pageslug')));
exports.UniqueUserList = uniqueUserList;

// buildChatAdminObj [{u}] => {o}
const buildChatAdminObj = (ioconnections) => ({users: ioconnections, playercount: ioconnections.length});
exports.BuildChatAdminObj = buildChatAdminObj;

// buildMsgPayLoad :: {d} -> {u} -> s => {o}
const buildMsgPayload = curry((user, discussion) => ({
    avataricon: S.path(['profile', 'avataricon'], user).getOrElse('/img/default-avatar-sq.jpg'),
    imgclass:   match_api.BuildProfileChatClass(user),
    textclass:  match_api.BuildNameClass(user),
    link:       match_api.BuildLink(user),
    thread: {
        username:   S.path(['profile', 'name'], user).getOrElse('No Name Set'),
        comment:    discussion,
        postdate:   new Date()
    },
}));
exports.BuildMsgPayload = buildMsgPayload;
