/**************************************************
**
** an ajax service for all things to do with the matches/timeslots 
**
*************************************************/

var express = require('express');
var bodyParser = require('body-parser')

var Promise   = require('bluebird');
var log       = require('./log.js');
var exports = module.exports = {};

var S         = require('./lambda.js');

var account   = require('./models/account.js');
var accounts  = require('./models/accounts.js');

var match = require('./models/match.js');
var matches = require('./models/matches.js');

var R         = require('ramda'); 

var Task      = require('data.task');
var Maybe     = require('data.maybe');
var Either    = require('data.either');


// safeMail :: o => maybe a;
const safeMail = S.path(['body', 'email']);

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// isPlayerType :: s -> s => [{o}] -> maybe Bool 
const isPlayerType = R.curry((type, email) => R.compose(
  R.chain(S.whenPropEq('type', type)), 
  R.chain(S.find(R.propEq('email', email))), 
  S.prop('players')
));
exports.isPlayerType = isPlayerType;

// scrub :: mongoObj -> obj;
const scrub = R.compose(
  R.dissoc('_id'), 
  R.dissoc('__v'), 
  JSON.parse, 
  JSON.stringify
);
exports.Scrub = scrub;

const liftAccountVerify = R.lift(accounts.TaskVerifyAnAccountFromEmail(account));


// removeMatchVotes :: s -> n -> maybe [a] 
const removeMatchVotes = R.curry((email, timeslotid) => R.compose(
  R.map(R.objOf('$set')), 
  R.map(R.objOf('timeslots.$.votes')), 
  R.map(R.reject(R.propEq('email', email))), 
  R.chain(S.prop('votes')), 
  R.chain(S.find(R.propEq('id', timeslotid))), 
  R.chain(S.prop('timeslots')), 
  S.head
));
exports.removeMatchVotes = removeMatchVotes;


// whenMatchHasVote :: s -> s -> maybe bool
const whenMatchHasVote = R.curry((email, voteid) => R.compose(
  R.map(R.propEq('email', email)), 
  R.chain(S.find(R.propEq('email', email))), 
  R.chain(S.prop('votes')), 
  R.chain(S.find(R.propEq('id', voteid))), 
  R.chain(S.prop('timeslots')), 
  S.head
));
exports.whenMatchHasVote = whenMatchHasVote;

// pushVote :: s -> n -> [a] => [a]
const pushVote = R.curry((value, email, username, avataricon, userkey, arr) => R.append({
  email:      email, 
  username:   username,
  avataricon: avataricon,
  userkey:    userkey,
  direction:  value
}, arr));
exports.pushVote = pushVote;

// addTimeslotVotes :: s -> n -> maybe [a] 
const addTimeslotVotes = R.curry((email, username, avataricon, userkey, value, timeslotid) => R.compose(
  R.map(R.objOf('$set')), 
  R.map(R.objOf('timeslots.$.votes')), 
  R.map(pushVote(value, email, username, avataricon, userkey)),
  R.chain(S.prop('votes')), 
  R.chain(S.find(R.propEq('id', timeslotid))), 
  R.chain(S.prop('timeslots')), 
  S.head
));
exports.addTimeslotVotes = addTimeslotVotes;

// findMatchByKey :: schemaMatch -> String -> [Model(Match)]
var findMatchByKey = R.curry(function(matchSchema, matchkey) {
  return matchSchema.find({key: matchkey}).limit(100).exec(); 
});
exports.findMatchByKey = findMatchByKey;

// findPlayerInMatch :: s -> o => maybe Bool 
const findPlayerInMatch = (email) => R.compose(
  R.chain(S.whenPropEq('email', email)), 
  R.chain(S.find(R.propEq('email', email))), 
  R.chain(S.prop('players')), 
  S.head
);
exports.findPlayerInMatch = findPlayerInMatch;

// voteForTimeslot :: account -> match -> s -> s -> s -> s -> Task o
const voteForTimeslot = R.curry((match, matchid, timeslotid, email, username, avatar, pageslug, key) => {
  return new Task((reject, resolve) => {

    findMatchByKey(match, matchid)
      .then(function(m) {
        if (findPlayerInMatch(email)(m).getOrElse(false)) {
          if (whenMatchHasVote(email, timeslotid)(m).getOrElse(false)) {
            reject(409);

          } else {
            match
              .findOneAndUpdate(
                {key: matchid, 'timeslots.id': timeslotid}, 
                addTimeslotVotes(email, username, avatar, pageslug, 1, timeslotid)(m).getOrElse({}), 
                {new:true}
              ).exec().then((m2) => {
                resolve(S.prop('timeslots', m2).getOrElse([]));
              }
            )
          }

        } else {
          reject(401);

        };

      })
      .catch(err => reject(401));

  });
}); 
exports.voteForTimeslot = voteForTimeslot;


// unvoteForTimeslot :: account -> match -> s -> n -> s -> s -> Task o
const unvoteForTimeslot = R.curry((account, match, matchid, timeslotid, email, key) => {
  return new Task((reject, resolve) => {
    accounts.TaskVerifyAnAccountFromEmail(account, email, key).fork(
      err => reject(401),
      user => findMatchByKey(match, matchid).then(function(m) {
        if (findPlayerInMatch(email)(m).getOrElse(false)) {
          if (whenMatchHasVote(email, timeslotid)(m).getOrElse(false)) {
            match
              .findOneAndUpdate(
                {key: matchid, 'timeslots.id': timeslotid}, 
                removeMatchVotes(email, timeslotid)(m).getOrElse({}), 
                {new:true}
              ).exec().then((m2) => {
                resolve(S.prop('timeslots', m2).getOrElse([]));

            })
          } else {
            reject(409);
          }
        } else {
          reject(401);
        };
      })
      .catch(err => reject(401))
    );
  });
}); 
exports.unvoteForTimeslot = unvoteForTimeslot;


// maybeTimeslotId :: o => maybe a;
const maybeTimeslotId = S.path(['params', 'timeslotid']);

// maybeMatch :: o -> Maybe a
const maybeMatch = S.path(['params', 'match']);
exports.MaybeMatch = maybeMatch;

// maybeKey :: obj => Maybe(key);
const maybeKey = (req, pathkey) => (S.path([pathkey, 'key'], req).isJust) 
  ? S.path([pathkey, 'key'], req) 
  : S.path(['user', 'key'], req); 
exports.MaybeKey = maybeKey;


// readTimeslots :: match -> s -> task [{o}]
const readTimeslots = R.curry((match, matchkey) => {
  return new Task((reject, resolve) => {
    match
      .findOne({key: matchkey})
      .exec()
      .then((m) => {
        resolve(S.prop('timeslots', m)
          .map(R.map(scrub))
          .getOrElse([]));
      });
  });
});
exports.readTimeslots = readTimeslots;


// addTimeslots :: match -> s -> task [{o}]
const addTimeslots = R.curry((match, matchkey, starttime, endtime) => {

  // buildAddQuery :: start -> end -> {o}
  var buildAddQuery = (startdatetime, enddatetime) => ({
    '$push': {
      timeslots: {
        startdatetime:  startdatetime,
        enddatetime:    enddatetime,
      }
    }
  });

  return new Task((reject, resolve) => {
    match
      .findOneAndUpdate({key: matchkey}, buildAddQuery(starttime, endtime), {new:true})
      .exec()
      .then((m) => {
        resolve(S.prop('timeslots', m)
          .map(R.map(scrub))
          .getOrElse([]));
      });
  });

});
exports.addTimeslots = addTimeslots;


// removeTimeslots :: match -> s -> s -> task [{o}]
const removeTimeslots = R.curry((match, matchkey, id) => {

  // buildRemoveQuery :: start -> end -> {o}
  var buildRemoveQuery = (id) => ({
    '$pull': {
      timeslots: {
        id: id 
      }
    }
  });

  return new Task((reject, resolve) => {
    match
      .findOneAndUpdate({key: matchkey}, buildRemoveQuery(id), {new:true})
      .exec()
      .then((m) => {
        resolve(S.prop('timeslots', m)
          .map(R.map(scrub))
          .getOrElse([]));
      });
  });

});
exports.removeTimeslots = removeTimeslots;


// findOneMatchByKey :: schemaMatch -> String -> [Model(Match)]
var findOneMatchByKey = R.curry(function(matchSchema, matchkey) {
  return new Task((reject, resolve) => matchSchema
    .findOne({key: matchkey})
    .exec()
    .then(resolve)
    .catch(reject)
  ); 
});
exports.findOneMatchByKey = findOneMatchByKey;


// updateRemoveVotes :: {o} -> {o}
const updateRemoveVotes = (a) => {

  // udpateVotes :: {o} -> {o}
  const updateVotes = R.compose(
    R.map(R.dissoc('email')),
    R.map(scrub),
    R.prop('votes')
  )
  a.votes = updateVotes(a);
  return a;
}


// setupRouter :: o -> o -> express 
var setupRouter = R.curry(function(emailSiteFrom, transporter, router) {

  var jsonParser = bodyParser.json();
  router.route('/:match/timeslots/')
    .get(jsonParser, function(req, res) {
      maybeMatch(req)
        .map(readTimeslots(match))
        .getOrElse(taskFail).fork(
          (err) => reject(500),
          (data) => {
            res.json(data
              .map(scrub)
              .map(updateRemoveVotes)
            );
          });

    })
    .post(jsonParser, function(req, res) {

      // liftAddTimeslots :: s -> Task [{o}]
      const liftAddTimeslots = R.lift(addTimeslots(match));
      liftAccountVerify(safeMail(req), maybeKey(req, 'body'))
        .getOrElse(taskFail())
        .fork(
          err   => res.sendStatus(err),
          user  => {
            maybeMatch(req)
              .map(findOneMatchByKey(match))
              .getOrElse(taskFail()).fork(
                err => res.sendStatus(500),
                m   => {
                  if ((isPlayerType('host', safeMail(req).getOrElse(''))(m).isJust)) {
                    liftAddTimeslots(maybeMatch(req), S.path(['body', 'startdatetime'], req), S.path(['body', 'enddatetime'], req))
                      .getOrElse(taskFail()).fork(
                        err => res.sendStatus(500),
                        data  => {
                          res.json(data
                            .map(scrub)
                            .map(updateRemoveVotes)
                          );
                        }
                      );

                  } else {
                    res.sendStatus(500);
                  }

            })
          }
        )
    });

  router.route('/:match/timeslots/:timeslotid')
    .delete(jsonParser, function(req, res) {

      // liftRemoveTimeslots :: s -> Task [{o}]
      const liftRemoveTimeslots = R.lift(removeTimeslots(match));

      liftAccountVerify(safeMail(req), maybeKey(req, 'body'))
        .getOrElse(taskFail())
        .fork(
          err   => res.sendStatus(err),
          user  => {
            maybeMatch(req)
              .map(findOneMatchByKey(match))
              .getOrElse(taskFail()).fork(
                err => res.sendStatus(500),
                m   => {
                  if ((isPlayerType('host', safeMail(req).getOrElse(''))(m).isJust)) {
                    liftRemoveTimeslots(maybeMatch(req), S.path(['params', 'timeslotid'], req))
                      .getOrElse(taskFail()).fork(
                        err => res.sendStatus(500),
                        data  => {
                          res.json(data
                            .map(scrub)
                            .map(updateRemoveVotes)
                          );
                        }
                      );

                  } else {
                    res.sendStatus(500);
                  }

            })
          }
        )

    });

  router.route('/:match/timeslots/:timeslotid/votes')
    .post(jsonParser, function(req, res) {

      const liftVote = R.lift(voteForTimeslot(match));

      // look up boardgame and add detail
      liftAccountVerify(safeMail(req), maybeKey(req, 'body'))
        .getOrElse(taskFail())
        .fork(
          err   => res.sendStatus(err),
          user  => {
            liftVote(
              maybeMatch(req), 
              maybeTimeslotId(req), 
              safeMail(req), 
              S.path(['profile', 'name'], user),
              Maybe.of(S.path(['profile', 'avataricon'], user).getOrElse('')),
              S.prop('pageslug', user),
              maybeKey(req, 'body')
            ).getOrElse(taskFail()).fork(
              err =>  {
                res.sendStatus(err);
              }, 
              data => {
                res.json(data
                  .map(scrub)
                  .map(updateRemoveVotes)
                );

              }
            )
          }
        );


    })
    .delete(jsonParser, function(req, res) {
      const liftunVote = R.lift(unvoteForTimeslot(account, match));

      liftunVote(
        maybeMatch(req), 
        maybeTimeslotId(req), 
        safeMail(req), 
        maybeKey(req, 'body') 
      ).getOrElse(taskFail()).fork(
        err => res.sendStatus(err),
        data => {
          res.json(data
            .map(scrub)
            .map(updateRemoveVotes)
          );
        }
      );
      
    });

  return router;

});
exports.SetupRouter = setupRouter;
