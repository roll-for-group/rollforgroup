var account = require("../app/models/account.js");
var accounts = require("../app/models/accounts.js");
var match = require("../app/models/match.js");
var matches = require("../app/models/matches.js");

var Promise = require("bluebird");
var R = require("ramda");

var compose = R.compose, 
    map     = R.map,
    merge   = R.merge;
var log = require('./log.js');
var expressHelper = require('./expresshelper');
var S = require('./lambda');

var md = require('markdown-it')({linkify:true});

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

// renderData :: Render -> String -> Object -> Render 
var renderData = R.curry((res, template, data) => res.render(template, data));


// maybebuildNavs :: {o} => {m} 
const maybeBuildNavs = (req) => S.path(['user', 'email'], req).map(expressHelper.BuildNavbar('')).map(R.objOf('navbarul'));

// getHostButtons :: s => {b}
const getHostButtons = (status) => (status === 'played') 
  ? {
    buttons: [{
      text:'Delete History', 
      faicon: 'ban', 
      style:'danger', 
      id: 'disband',
      title: 'Delete this event'
    }]
  } 
  : (status === 'open') 
    ? {
      buttons: [{
        text:'Disband', 
        faicon: 'ban', 
        style:'danger', 
        id: 'disband',
        title: 'Disband this event'
      }]
    }
    : (status === 'draft') 
      ? {
        buttons: [
          {
            text:'Cancel', 
            faicon: 'ban', 
            style:'danger', 
            id: 'disband',
            title: 'Cancel this event'
          }
        ]
      }
      : {};
exports.GetHostButtons = getHostButtons;


// matchHasUser :: s -> o -> b
const matchHasUser = R.curry((email, obj) => {

	// lookupUser :: [p] -> bool
	const lookupUser = R.compose(
		R.chain(S.find(R.propEq('email', email))),
		S.prop('players')
	);

	return lookupUser(obj).isJust

});
exports.MatchHasUser = matchHasUser;

// matchIsJoinable :: [{m}] => b
const matchIsJoinable = R.converge(R.and, [
  R.pipe(R.prop('isfull'), R.not), 
  R.pipe(R.prop('status'), R.equals('postgame'), R.not)
]);
exports.MatchIsJoinable = matchIsJoinable;

// mergeQuadrant :: {o} => {p} 
const mergeQuadrant = (data) => {
  const objWhenOne = (x) => {
    return (x == 0) 
      ? R.objOf('gameQuad0', true) 
      : (x == 1) 
        ? R.objOf('gameQuad1', true) 
        : (x == 2) 
          ? R.objOf('gameQuad2', true) 
          : (x == 3) 
            ? R.objOf('gameQuad3', true) 
            : {};
  }
  const addQuad = compose(
    map(objWhenOne), 
    map(accounts.GetTestQuadrant), 
    S.prop('key')
  );
  return merge(addQuad(data).getOrElse({}), data);
};
exports.MergeQuadrant = mergeQuadrant;

// findReoccurEveryVal :: o -> n 
const findReoccurEveryVal = (obj) => S.path(['reoccur', 'enable'], obj)
  .getOrElse(false)
    ? S.path(['reoccur', 'every'], obj).getOrElse('-1')
    : '-1';
exports.findReoccurEveryVal = findReoccurEveryVal;

// findReoccurIntervalVal :: o -> n 
const findReoccurIntervalVal = (obj) => S.path(['reoccur', 'enable'], obj)
  .getOrElse(false)
    ? S.path(['reoccur', 'interval'], obj).getOrElse('never')
    : 'never';
exports.findReoccurIntervalVal = findReoccurIntervalVal;

// assocMarkdown :: o -> o 
const assocMarkdown = (x) => S.prop('description', x)
  .map(R.assoc('markdown', R.__, x))
  .getOrElse(x);
exports.assocMarkdown = assocMarkdown;

// isPremiumUser :: o -> bool
const isPremiumUser = (req) => (
	S.pathEq(['user', 'premium', 'isstaff'], true, req).isJust ||
	S.pathEq(['user', 'premium', 'isactive'], true, req).isJust);
exports.isPremiumUser = isPremiumUser;

// routegame :: Express -> Object -> Model -> Schema -> Schema -> 0
var routegame = function (expressApp, CheckLogin, match, account) {

	var url = "events/series";
  var slashString = (file) => '/' + file;

	expressApp.get(slashString(url) + '/:seriesid', function(req, res) {

    // assocReoccurEvery :: o -> o 
    const assocReoccurEvery = (x) => 
      R.assoc('reoccurevery', findReoccurEveryVal(x), x);

    // assocReoccurInterval :: o -> o 
    const assocReoccurInterval = (x) => 
      R.assoc('reoccurinterval', findReoccurIntervalVal(x), x);

    // mdrend :: a => <b />
    const mdrend = (x) => md.render(x);
    const transDescription = R.objOf('description', mdrend);

    // objectify :: o -> o
    const objectify = (x) => 
      x.toObject();

    // findThumbnail :: a -> maybe a
    const findThumbnail = R.compose(
      R.map(R.objOf('image')),
      R.chain(S.prop('thumbnail')),
      R.chain(S.head),
      R.map(objectify),
      S.prop('games')
    );

    // sendCleanedPage :: o -> s -> o -> b -> o -> f => o -> o
    var sendCleanedPage = R.curry((res, url, hostdata, isPremium, pagedata, fprepare, navbars) => R.compose(
      renderData(res, 'reoccurevents'), 
      R.merge({css: [ { csshref: '/css/matchlogin.css' } ]}),
      R.merge(findThumbnail(pagedata).getOrElse({})),
      R.merge({boardgameserviceurl: nconf.get('service:boardgames:weburl')}),
      R.merge(maybeBuildNavs(req).getOrElse({})), 
      R.merge(navbars), 
      R.merge(hostdata), 
      fprepare, 
      R.evolve(transDescription), 
      assocReoccurInterval, 
      assocReoccurEvery, 
      assocMarkdown, 
			R.assoc('ispremium', isPremium),
      mergeQuadrant, 
      R.dissoc('__v'), 
      R.dissoc('_id'), 
      JSON.parse, 
      JSON.stringify
    )(pagedata));

    if (R.hasIn('seriesid', req.params)) {

      var email = R.path(['user', 'email'], req);

      // lookup match
      matches.FindLastMatchByReoccurSeries(match, req.params.seriesid).then(function(x) {

        // playersByType :: [o] -> [o]
        const playersByType = R.compose(
          R.groupBy(R.prop('type')), 
          R.map(R.dissoc('__v')), 
          R.map(R.dissoc('_id')), 
          R.prop('players'), 
          JSON.parse, 
          JSON.stringify
        );

        const hostify = R.compose(
          R.assoc('host', R.__, {}), 
          R.head, 
          R.prop('host'), 
          R.map(R.dissoc('type')), 
          playersByType
        );

        const typify = R.curry((type, match) => R.compose(
          R.ifElse(R.hasIn(type), R.pipe(R.prop(type), R.map(R.dissoc('type')), R.assoc(type, R.__, {})), R.empty), 
          playersByType
        )(match));  

        var approves = {approves: typify('approve', x)};

        // liftEntryFee :: {o} => Maybe(s)
        const liftEntryFee = (x) => R.lift(matches.EntryFee)(S.prop('price', x), S.prop('currency', x));

        // safeGtZero :: a => Maybe(a);
        const safeGtZero = (a) => R.gt(a, 0)
          ? Maybe.of(a) 
          : Maybe.Nothing;

        // entryFee :: {o} => {p}
        const entryFee = (S.prop('price', x).chain(safeGtZero)) 
          ? R.objOf('entryFee', liftEntryFee(x).getOrElse(0)) 
          : R.objOf('entryFee', 0);

        var matchpointlist = [
          {id: 'status', text: x.status, faicon: 'fa-bell'}
        ];

        var whenSocial = R.curry((key, text, faicon, data) => (R.prop(key, x) == 2) 
          ? R.append({text: text, faicon: faicon}, data) 
          : data
        ); 

        var socialise = R.compose(
          whenSocial('alcohol', 'Drinking', 'fa-beer'), 
          whenSocial('smoking', 'Smoking', 'fa-fire')
        );

        var matchpoints = {points: socialise(matchpointlist)};
        var prepareData = R.identity;

        var buttons = matchIsJoinable(x) 
          ? { buttons: [
              {text:'Interested', faicon:'bell', style:'primary', id:'interested'},
              {text:'Join Event', faicon:'hand-paper-o', style:'success', id:'join'}
            ]}
          : {};

        if (!matchHasUser(email)(x)) {

          // checkLogin :: ({r} -> {o}) => {p}
          const checkLogin = R.curry((req, obj) => R.has('user', req) 
            ? obj 
            : R.merge(obj, {navigation: {nouser: true}}));

          prepareData = R.compose(
            R.merge(entryFee), 
            R.merge({showJoin:matchIsJoinable(x)}), 
            R.merge(approves), 
            checkLogin(req), 
            R.merge(matchpoints), 
            R.merge(buttons)
          );

          matches.TaskIncrementStatWhenNotHost('viewcount', match, '', x.key).fork();

        } else { 

          var matchPlayerType = R.compose(
            R.prop('type'), 
            R.head, 
            R.filter(R.propEq('email', email)), 
            R.prop('players')
          );

          const mergeRequests = (x) => (R.propEq("status", "played", x))
            ? R.merge({}) 
            : R.compose(merge, R.objOf('requests'), typify('request'))(x); 

          switch (matchPlayerType(x)) {
            case 'host':
              buttons = getHostButtons(x.status);
              var showReach = {reach: true, enableEdit: true};
              prepareData = R.compose(
                R.merge(entryFee), 
                R.merge(showReach), 
                R.merge(matchpoints), 
                R.merge(approves), 
                R.unless(R.pipe(R.prop('seatavailable'), R.equals(0)), R.pipe(mergeRequests(x))), 
                R.merge(buttons)
              );
              break;

            case 'invite':
              buttons = {buttons: [
                {text:'Accept', faicon:'thumbs-up', style:'success', id:'accept'},
                {text:'Decline', faicon: 'sign-out', style: 'warning', id:'decline'}
              ]};
              prepareData = R.compose(
                R.merge(entryFee), 
                R.merge(matchpoints), 
                R.merge(buttons), 
                R.merge(approves)
              );
              matches.TaskIncrementStatWhenNotHost('viewcount', match, '', x.key).fork();
              break;

            case 'decline':
              buttons = {buttons: [
                {text:'Interested', faicon:'bell', style:'primary', id:'interested'},
                {text:'Join Event', faicon:'thumbs-up', style:'success', id:'accept'},
                {text:'Leave Group', faicon: 'times', style: 'danger', id:'leave'}
              ]};
              prepareData = R.compose(
                R.merge(entryFee), 
                R.merge(matchpoints), 
                R.merge(buttons), 
                R.merge(approves)
              );
              matches.TaskIncrementStatWhenNotHost('viewcount', match, '', x.key).fork();
              break;

            case 'request':
              buttons = {buttons: []};
              var pending = {pending:{
                avataricon: req.user.profile.avataricon,
                name: req.user.profile.name,
                email: req.user.email}
              };
              prepareData = R.compose(
                R.merge(entryFee), 
                R.merge(pending), 
                R.merge(matchpoints), 
                R.merge(buttons), 
                R.merge(approves)
              );
              matches.TaskIncrementStatWhenNotHost('viewcount', match, '', x.key).fork();
              break;

            case 'approve':
              buttons = {buttons: [{text:'Leave Event', faicon: 'sign-out', style: 'warning', id:'decline'}]};
              approves = {approves: typify('approve', x)};
              prepareData = R.compose(
                R.merge(entryFee), 
                R.merge(matchpoints), 
                R.merge(approves), 
                R.merge(buttons)
              );
              break;

            case 'interested':
              buttons = { buttons: [
                {text:'Join Event', faicon:'thumbs-up', style:'success', id:'join'},
                {text:'Leave Event', faicon: 'sign-out', style: 'warning', id:'decline'}
              ]};

              prepareData = R.compose(
                R.merge(entryFee), 
                R.merge({showJoin:matchIsJoinable(x)}), 
                R.merge(approves), 
                R.merge(matchpoints), 
                R.merge(buttons)
              );
              break;

            case 'standby':
              prepareData = R.compose(
                R.merge(entryFee), 
                R.merge({showJoin:matchIsJoinable(x)}), 
                R.merge(approves), 
                R.merge(matchpoints), 
                R.merge(buttons)
              );
              break;
            }
          }

          accounts
            .taskClearMatchNotifications(account, email, '/events/series/' + req.params.seriesid)
            .fork(data=>{}, err=>{});

          accounts
            .BuildNavbars(account, email)
            .then(sendCleanedPage(res, url, hostify(x), isPremiumUser(req), x, prepareData));

      }).catch(function(err) {
        res.redirect('/events?err=matchnotfound');
      });

    } else {
      res.redirect('/events?err=matchnotfound');

    }

	});


};
exports.Routegame = routegame;
