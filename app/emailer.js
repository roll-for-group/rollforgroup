var Promise = require('bluebird');
var R = require('ramda');
var chain   = R.chain,
    compose = R.compose,
    converge = R.converge,
    curry   = R.curry,
    find    = R.find,
    filter  = R.filter,
    findLast = R.findLast,
    head    = R.head,
    isEmpty = R.isEmpty,
    isNil   = R.isNil,
    init    = R.init,
    join    = R.join,
    last    = R.last,
    length  = R.length,
    lift    = R.lift,
    not     = R.not,
    map     = R.map,
    path    = R.path,
    prop    = R.prop,
    propEq  = R.propEq,
    sequence = R.sequence,
    zipObj  = R.zipObj;

var util = require('util');
var log = require('./log.js');

var moment = require('moment-timezone');

var request = require('ajax-request');
var accounts = require('./models/accounts.js');
var matches = require('./models/matches.js');

var S = require('./lambda.js');

var smtpSettings = function(nconfig) { 
  return {
    host: nconfig.get("smtp:host"),
    port: nconfig.get("smtp:port"),
    secure:true,
    auth: {
      user: nconfig.get("smtp:user"),
      pass: nconfig.get("smtp:pass")
    },
    authMethod: "PLAIN"
  };
};
exports.SMTPSettings = smtpSettings;

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

// ============== AJAX ===============
var ajaxTask = R.curry(function(options) {
  return new Task(function (reject, resolve) {
    request(options, function(err, res, body) {
      if (err) {
        reject(err)
      } else {
        resolve(body)
      }
    });
  });
}); 


const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

const url_email_api = '/api/v1/emailer';
const url_email_base = nconf.get('service:emailer:http:url');
const url_email_port = nconf.get('service:emailer:http:port');

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// safeFilter :: (f -> Bool) -> [a] => Maybe(a)
const safeFilter = curry((a, b) => (filter(a, b) == undefined) ?  Maybe.Nothing() : Maybe.of(filter(a, b)));

// safeReject :: (f -> Bool) -> [a] => Maybe(a)
const safeReject = curry((a, b) => (filter(a, b) == undefined) ?  Maybe.Nothing() : Maybe.of(R.reject(a, b)));


// from :: u -> e
const from = (siteuser) => 'Roll for Group <' + siteuser + '>';
exports.From = from;

// seats :: n -> s
const seats = (num) => (num == 1) 
  ? 'is only 1 seat' 
  : 'are ' + num + ' seats';
exports.Seats = seats;

const isEmailEqual = curry((email, x) => (prop('email', x) == email));
exports.IsEmailEqual = isEmailEqual;

// sendWithSMTP :: Transport -> Object -> (Promise -> info)
var sendWithSMTP = R.curry(function(MailTransport, mailOptions) {
	return MailTransport.sendMail(mailOptions);
});
exports.SendWithSMTP = sendWithSMTP;


// sendSMTPmail :: Transport -> String -> String -> String -> String -> (Promise -> info)
var sendSMTPmail = R.curry(function(MailTransport, from, to, subject, html) {

	var mailOptions = {
		from: from,
		to: to,
		subject: subject,
		html: html 
	};
  return sendWithSMTP(MailTransport, mailOptions);

});
exports.SendSMTPmail = sendSMTPmail;


// buildGamelist :: [g] => s
const buildGamelist = (gamelist) => {
  const firstgame = compose(chain(S.prop('name')), S.head);
  const endgames = compose(last, map(S.prop('name')))
  const commarise = compose(map(join(', ')), sequence(Maybe.of), init, map(S.prop('name')));

  return (gamelist.length == 0) 
    ? 'boardgames' 
    : (gamelist.length == 1) ? firstgame(gamelist).getOrElse('Unknown Game ') : join(' and ', [commarise(gamelist).get(), endgames(gamelist).get()]) 
};
exports.BuildGamelist = buildGamelist


// buildInviteEmailUserData :: s -> s ->  {o}
const buildInviteEmailUserData = R.curry((playeremail, hostname, playername, games, eventname, eventlocation, eventtime, url) => ({
	email:    playeremail,
	mailplan: [
		{
			template: 'invite-event-001',
			hours: -1 
		}
	],
	userdata: {
		email: playeremail,
		playername: playername,
		hostname: 	hostname,
		games: games,
		eventname: eventname,
		eventlocation: eventlocation,
		eventtime: eventtime,
		url: url,
	}
}));
exports.buildInviteEmailUserData = buildInviteEmailUserData;


// sendInviteGuestEmail -> String -> String -> String -> JSONObject
var sendInviteGuestEmail = R.curry(function(account, match, guestemail, matchkey) {

  // findName :: e => (f(obj) => n)
  const findName = (email) => R.compose(
		R.prop('name'), 
		findLast(isEmailEqual(email)), 
		R.prop('players')
	);

  return new Promise(function(fulfill, reject) {
    accounts.ReadAnAccountFromEmail(account, guestemail).then(function(u) { 
      matches.TaskFindMatch(match, matchkey).fork(
				err => log.clos('errx', err),
				m => {
					ajaxTask({
						url: 		url_email_base + ':' + url_email_port + url_email_api + '/event-invite',
						method: 'POST',
						data: 	buildInviteEmailUserData(
							guestemail, 
							getMatchHost('name', [m]).getOrElse('host'), 
							S.path(['profile', 'name'], u).getOrElse('there'), 
							buildGamelist(m.games),
							m.title,
							m.locname,
							moment(m.date).tz(m.timezone).format('dddd MMM Do'),
							'https://www.rollforgroup.com/events/' + m.key 
						)
					})
						.fork(reject, fulfill);

				}
			).catch(err => log.clos('err', err));;
    });
  });

}); 
exports.sendInviteGuestEmail = sendInviteGuestEmail;

// buildHostAcceptEmail :: (Model(Match) -> String -> String) => JSONObject
var buildHostAcceptEmail = curry(function(match, sitefrom, hostemail, useremail) {

  // findName :: e => (f(obj) => n);
  const findName = (email) => compose(prop('name'), findLast(isEmailEqual(email)), prop('players'));

  // subject :: (u -> g) => s
  const subject = (gametitle) => 'Accepted - ' + gametitle; 

  const text = (match) => 'Great news ' + findName(useremail)(match) + ',\n\n' + findName(hostemail)(match) + ' approved your request to join "' + match.title + '" on Roll for Group at ' + match.locname + '. View the event page to view what other players are bringing. You now have access to show others what games you want to bring, and vote on what games other players games on the Roll for Group event page.\n\nInvite a friend to join, by forwarding them the event page web url link: https://www.rollforgroup.com/events/' + match.key + ' or share the link on Facebook.\n\n\n\nHappy Gaming,\n\nGame Knight\n\nhttps://www.rollforgroup.com\n\nFollow us on Facebook: http://www.facebook.com/rollforgroup\n\nTwitter: http://www.twitter.com/rollforgroup\n\n\n\nYou received this email because you requested to join an event on Roll for Group.' 

  return zipObj(['from', 'to', 'subject', 'text'], [sitefrom, useremail, subject(match.title), text(match)]); 

}); 
exports.BuildHostAcceptEmail = buildHostAcceptEmail;


// degreesToRadians :: d -> r
const degreesToRadians = function(degrees) {
  return degrees * Math.PI / 180;
}
exports.DegreesToRadians = degreesToRadians;


// distanceInKmBetweenEarthCoordinates :: ([lng1, lat1], [lat2, lng2]) => km 
const distanceInKmBetweenEarthCoordinates = function(lnglat1, lnglat2) {

    var earthRadiusKm = 6371;

    var dLat = degreesToRadians(lnglat2[1]-lnglat1[1]);
    var dLon = degreesToRadians(lnglat2[0]-lnglat1[0]);

    lat1 = degreesToRadians(lnglat1[1]);
    lat2 = degreesToRadians(lnglat2[1]);

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
              Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    return earthRadiusKm * c;

}
exports.DistanceInKmBetweenEarthCoordinates = distanceInKmBetweenEarthCoordinates;


// isInRange :: (Model(match) -> Model(user)) => bool
const isInRange = curry((match, user) => {

    const search = lift((radius, from, to) => radius >= distanceInKmBetweenEarthCoordinates(from, to)); 
    return search(S.path(['profile', 'searchradius'], user), S.path(['loc', 'coordinates'], match), S.path(['profile', 'localarea', 'loc', 'coordinates'], user)).getOrElse(false);

});
exports.IsInRange = isInRange;


// FindEventNotifyUsers :: (e -> Model(match) -> [u]) => [vu]
const filterEventNotifyUsers = curry(function(hostemail, match, users) {

    // TODO: add in filter for times
    const notHost = compose(not, isEmailEqual(hostemail));
    const isNotify = (user) => S.path(['profile', 'notifyevents'], user).getOrElse(false);
    const eventfilter = compose(map(prop('email')), filter(isNotify), filter(isInRange(match)), filter(notHost));

    return eventfilter(users);

});
exports.FilterEventNotifyUsers = filterEventNotifyUsers;

// buildMessage :: n => s
const buildMessage = (playername) => playername + ' is hosting an open board game event nearby.'
exports.BuildMessage = buildMessage;

// safeMessage :: {m} -> Maybe(s)
const safeMessage = compose(map(buildMessage), chain(S.prop('name')), chain(S.head), S.prop('players'));
exports.SafeMessage = safeMessage;

// buildMatchURL :: s1 -> s2
const buildMatchUrl = (key) => '/events/' + key;
exports.BuildMatchUrl = buildMatchUrl;

// safeUrl :: {m} -> Maybe(s)
const safeUrl = R.compose(R.map(buildMatchUrl), S.prop('key'));
exports.SafeUrl = safeUrl;

// safeAvatarIcon :: {m} -> Maybe(s)
const safeAvatarIcon = compose(chain(S.prop('avataricon')), chain(S.head), S.prop('players'));
exports.SafeAvatarIcon = safeAvatarIcon;

// getMatchTitle :: [o] -> maybe s
const getMatchTitle = R.compose(
  R.chain(S.prop('title')), 
  S.head
);

// getMatchAutoAccept :: [o] -> maybe s
const getMatchAutoAccept = R.compose(
  R.chain(S.prop('autoaccept')), 
  S.head
);

// buildInterestedEmailUserData :: s -> s ->  {o}
const buildInterestedEmailUserData = R.curry((hostemail, hostname, playername, playeravatar, eventname, url) => ({
	email:    hostemail,
	mailplan: [
		{
			template: 'interested-match-001',
			hours: -1 
		}
	],
	userdata: {
		email:    	hostemail,
		avataricon: playeravatar,
		name:   		hostname,
		url:    		url,
		playername: playername,
		eventname:  eventname
	}
}));
exports.buildInterestedEmailUserData = buildInterestedEmailUserData;


// buildJoinEmailUserData :: s -> s ->  {o}
const buildJoinEmailUserData = R.curry((hostemail, hostname, playername, playeravatar, eventname, url) => ({
	email:    hostemail,
	mailplan: [
		{
			template: 'request-joinmatch-001',
			hours: -1 
		}
	],
	userdata: {
		email:    	hostemail,
		avataricon: playeravatar,
		name:   		hostname,
		url:    		url,
		playername: playername,
		eventname:  eventname
	}
}));
exports.buildJoinEmailUserData = buildJoinEmailUserData;

// getMatchHost :: s -> [o] -> s
const getMatchHost = R.curry((propname, matchobj) => R.compose(
	R.chain(S.prop(propname)), 
	R.chain(S.head), 
	R.chain(S.prop('players')), 
	S.head
)(matchobj));
exports.getMatchHost = getMatchHost;


// sendInterestNotifyEmails :: account -> match -> string -> string -> Promise {o}
var sendInterestNotifyEmails = R.curry(function(account, match, matchkey, emailRequestFrom) {
  return new Promise(function(fulfill, reject) {

		// buildMessage :: s -> s
		const buildMessage = R.curry((title, user) => user + ' is interested in ' + title + '.');

		// getVisitorAvatar :: [o] -> Maybe s
		const getVisitorAvatar = S.path(['profile', 'avataricon']);

    matches.FindMatchByKey(match, matchkey).then(function(m) {
      accounts.ReadAnAccountFromEmail(account, emailRequestFrom).then((u) => {

				R.sequence(Task.of, [
					accounts.TaskNotifyUser(
						account, 
						lift(buildMessage)(getMatchTitle(m), S.path(['profile', 'name'], u))
							.getOrElse('A player is interested in your event.'),
						getMatchHost('email', m).getOrElse('gaming@rollforgroup.com'), 
						getVisitorAvatar(u).getOrElse('/img/default-avatar-sq.jpg'), 
						buildMatchUrl(matchkey),
						'interested',
						false
					),
					ajaxTask({
						url: 		url_email_base + ':' + url_email_port + url_email_api + '/event-interested',
						method: 'POST',
						data: 	buildInterestedEmailUserData(
							getMatchHost('email', m).getOrElse('gaming@rollforgroup.com'), 
							getMatchHost('name', m).getOrElse('host'), 
							S.path(['profile', 'name'], u).getOrElse('A player'), 
							getVisitorAvatar(u).getOrElse('/img/default-avatar-sq.jpg'), 
							getMatchTitle(m).getOrElse(''), 
							'https://www.rollforgroup.com/events/' + matchkey
						)
					})
				]).fork(reject, fulfill);

      });
    });
  });
});
exports.sendInterestNotifyEmails = sendInterestNotifyEmails;


// sendRequestNotifyEmails :: account -> match -> string -> string -> Promise {o}
var sendRequestNotifyEmails = R.curry(function(account, match, matchkey, emailRequestFrom) {
  return new Promise(function(fulfill, reject) {

		// buildMessage :: s -> s
		const buildMessage = R.curry((title, user) => user + ' requested to join ' + title + '.');

		// buildAutoJoinMessage :: s -> s
		const buildAutoJoinMessage = R.curry((title, user) => user + ' was auto joined to your event ' + title + '.');

		// getVisitorAvatar :: [o] -> Maybe s
		const getVisitorAvatar = S.path(['profile', 'avataricon']);

    matches.FindMatchByKey(match, matchkey).then(function(m) {
      accounts.ReadAnAccountFromEmail(account, emailRequestFrom).then((u) => {

        if (getMatchAutoAccept(m).getOrElse(true)) {
					accounts.TaskNotifyUser(
						account, 
						lift(buildAutoJoinMessage)(getMatchTitle(m), S.path(['profile', 'name'], u))
							.getOrElse('A player has autojoined your event.'),
						getMatchHost('email', m).getOrElse('gaming@rollforgroup.com'), 
						getVisitorAvatar(u).getOrElse('/img/default-avatar-sq.jpg'), 
						buildMatchUrl(matchkey), 
						'joinautorequest', 
						false
					).fork(reject, fulfill);

        } else {  // match is autojoin
         R.sequence(Task.of, [
            accounts.TaskNotifyUser(
              account, 
              lift(buildMessage)(getMatchTitle(m), S.path(['profile', 'name'], u))
                .getOrElse('A player has requested to join your event.'),
              getMatchHost('email', m).getOrElse('gaming@rollforgroup.com'), 
              getVisitorAvatar(u).getOrElse('/img/default-avatar-sq.jpg'), 
              buildMatchUrl(matchkey), 
              'joinrequest', 
              false
            ),
            ajaxTask({
              url: 		url_email_base + ':' + url_email_port + url_email_api + '/event-request-join',
              method: 'POST',
              data: 	buildJoinEmailUserData(
                getMatchHost('email', m).getOrElse('gaming@rollforgroup.com'), 
                getMatchHost('name', m).getOrElse('host'), 
                S.path(['profile', 'name'], u).getOrElse('A player'), 
                getVisitorAvatar(u).getOrElse('/img/default-avatar-sq.jpg'), 
                getMatchTitle(m).getOrElse(''), 
                'https://www.rollforgroup.com/events/' + matchkey
              )
            })
          ]).fork(reject, fulfill);

        };

      });
    });
  });
});
exports.SendRequestNotifyEmails = sendRequestNotifyEmails;


// sendApproveNotifyEmails :: (Transport -> Model(match) -> String -> String -> String) => ? 
var sendApproveNotifyEmails = R.curry(function(MailTransport, account, match, matchkey, sitefrom, hostemail, keyto) {
  return new Promise(function(fulfill, reject) {
    matches.FindMatchByKey(match, matchkey).then(function(m) {

      // getUserProp :: s => ([o] -> Maybe s)
      const getUserProp = (propName) => compose(
        chain(S.prop(propName)), 
        map(R.find(propEq('playerkey', keyto))), 
        chain(S.prop('players')),
        S.head
      );

      // getHostProp :: s => ([o] -> Maybe s)
      const getHostProp = (propName) => compose(
        chain(S.prop(propName)), 
        chain(S.head), 
        chain(S.prop('players')),
        S.head
      );

      const findUserTo = compose(
        prop('email'), 
        R.find(propEq('playerkey', keyto)), 
        prop('players'), 
        head
      );

      sendWithSMTP(MailTransport, buildHostAcceptEmail(m[0], from(sitefrom), hostemail, findUserTo(m))); 

      // buildMessage :: s -> s
      const buildMessage = curry((location, user) => user + ' accepted your request to join ' + location + '.');

      accounts.TaskNotifyUser(
        account, 
        lift(buildMessage)(getMatchTitle(m), getHostProp('name')(m))
          .getOrElse('A player has accepted your event invitation.'),
        findUserTo(m),
        getUserProp('avataricon')(m).getOrElse('/img/default-avatar-sq.jpg'), 
        buildMatchUrl(matchkey), 
        'acceptrequest', 
        false
      ).fork(reject, fulfill);

    });
  });
});
exports.SendApproveNotifyEmails = sendApproveNotifyEmails;

// getPluralNotification :: {o} -> maybe n
const getPluralNotification = (notificationNumber) => 
	(notificationNumber == 1) 
		? 'a new notification'
		: notificationNumber + ' new notifications'
exports.getPluralNotification = getPluralNotification;


// buildNotificationCountEmailUserData :: s -> s -> s -> n -> s -> {o}
const buildNotificationCountEmailUserData = R.curry((playeremail, playerkey, playername, notificationcount, url) => ({
	email:    playeremail,
	mailplan: [
		{
			template: 'notifications-001',
			hours: -1 
		}
	],
	userdata: {
		email:    	playeremail,
		unsubscribeurl: 'https://www.rollforgroup.com/unsubscribe?email=' + playeremail + '&key=' + playerkey,
		userkey:		playerkey,
		name:   		playername,
		url:    		url,
		notificationcount: 	notificationcount,
		notificationtext: 	getPluralNotification(notificationcount)
	}
}));
exports.buildNotificationCountEmailUserData = buildNotificationCountEmailUserData;


// sendNotificationCountEmail
var sendNotificationCountEmail = R.curry(function(userdata) {
  return new Promise(function(fulfill, reject) {

		// notificationlength :: {o} -> maybe n
		const notificationlength = R.compose(
		  R.map(R.length),	
			S.prop('notifications')
		);

		if (notificationlength(userdata).getOrElse(0) > 0) {
			ajaxTask({
				url: 		url_email_base + ':' + url_email_port + url_email_api + '/notificationcount',
				method: 'POST',
				data: 	buildNotificationCountEmailUserData(
					S.prop('email', userdata).getOrElse('play+notificationemailerror@rollforgroup.com'),
					S.prop('key', 	userdata).getOrElse('abc'),
					S.path(['profile', 'name'], userdata).getOrElse('gamer'),
					notificationlength(userdata).getOrElse(1),
					'https://www.rollforgroup.com'
				)
			}).fork(reject, fulfill);
		} else {
			fulfill({})
		}

  });
});
exports.sendNotificationCountEmail = sendNotificationCountEmail;


// buildInterestedEmailUserData :: s -> s ->  {o}
const buildInterestedEmailReminderUserData = R.curry((email, name, untilwhen, eventname, url) => ({
	email:    email,
	mailplan: [
		{
			template: 'interested-match-reminder-001',
			hours: -1 
		}
	],
	userdata: {
		email:    	email,
		name:   		name,
		untilwhen:	untilwhen,
		url:    		url,
		eventname:  eventname
	}
}));
exports.buildInterestedEmailReminderUserData = buildInterestedEmailReminderUserData;


// findPlayerNameByEmail :: s -> o -> s
const findPlayerNameByEmail = R.curry((email, data) => R.compose(
	R.chain(S.prop('name')),
	R.chain(S.find(propEq('email', email))),
	R.chain(S.prop('players')),
	S.head
)(data));
exports.findPlayerNameByEmail = findPlayerNameByEmail;


// sendInterestReminderEmails = s -> o -> task o 
var sendInterestReminderEmails = R.curry(function(textDuration, anevent, email) {

	return ajaxTask({
		url: 		url_email_base + ':' + url_email_port + url_email_api + '/event-interested-reminder',
		method: 'POST',
		data: 	buildInterestedEmailReminderUserData(
			email,
			findPlayerNameByEmail(email, anevent).getOrElse('a player'),
			textDuration,	
			S.prop('title', anevent).getOrElse('an event'), 
			'https://www.rollforgroup.com/events/' + S.prop('key', anevent).getOrElse('') 
		)
	});

});
exports.sendInterestReminderEmails = sendInterestReminderEmails;


// sendInviteTemplateEmailData :: s -> s ->  {o}
const sendInviteTemplateEmailData = R.curry((matchobj) => {
	// safeHostProp :: s -> {o} -> maybe s
	const safeHostProp = R.curry((prop, obj) => R.compose(
		R.chain(S.prop(prop)),
		R.chain(S.head),
		S.prop('players')
	)(obj));

	// momentifyData :: {o} -> s
	const momentifyData = (obj) => {
		return (S.prop('date', matchobj).isJust && S.prop('timezone', matchobj).isJust)
			? moment(matchobj.date).tz(matchobj.timezone).format('dddd MMM Do')
			: '';
	};

	return ({
		url: 		url_email_base + ':' + url_email_port + url_email_api + '/send-invitetemplate',
		method: 'POST',
		data:  {	
			email:    safeHostProp('email', matchobj).getOrElse(''),
			mailplan: [
				{
					template: 'invite-hosttemplate-001',
					hours: -1 
				}
			],
			userdata: {
				email			:	safeHostProp('email', matchobj).getOrElse(''),
				hostname	: safeHostProp('name', matchobj).getOrElse('host'),
				eventlocation: S.prop('locname', matchobj).getOrElse(''),
				eventtime : momentifyData(matchobj),
				eventname : S.prop('title', matchobj).getOrElse(''),
				url				: 'https://www.rollforgroup.com/events/' + S.prop('key', matchobj).getOrElse('')
			}
		}
	})
});
exports.sendInviteTemplateEmailData = sendInviteTemplateEmailData;

// sendInviteTemplateEmail = s -> o -> task o 
var sendInviteTemplateEmail = R.curry(function(matchobj) {
	return ajaxTask(sendInviteTemplateEmailData(matchobj));
});
exports.sendInviteTemplateEmail = sendInviteTemplateEmail;


// sendCreateErrUsernameExistsData :: s -> s ->  {o}
const sendCreateErrUsernameExistsData = R.curry((userobj) => {
	return ({
		url: 		url_email_base + ':' + url_email_port + url_email_api + '/createuser-err-emailexists',
		method: 'POST',
		data:  {	
			email:    S.prop('email', userobj).getOrElse(''),
			mailplan: [
				{
					template: 'create-err-username-exists-001',
					hours: -1 
				}
			],
			userdata: {
				email			:	S.prop('email', userobj).getOrElse(''),
				name	    : S.path(['profile', 'name'], userobj).getOrElse('')
			}
		}
	})
});
exports.sendCreateErrUsernameExistsData = sendCreateErrUsernameExistsData;

// sendCreateErrUsernameExists = s -> o -> task o 
var sendCreateErrUsernameExists = R.curry(function(matchobj) {
	return ajaxTask(sendCreateErrUsernameExistsData(matchobj));
});
exports.sendCreateErrUsernameExists = sendCreateErrUsernameExists;


// sendForgotEmailData :: s -> s ->  {o}
const sendForgotEmailData = R.curry((userobj) => {
	return ({
		url: 		url_email_base + ':' + url_email_port + url_email_api + '/createuser-err-emailexists',
		method: 'POST',
		data:  {	
			email:    S.prop('email', userobj).getOrElse(''),
			mailplan: [
				{
					template: 'create-err-username-exists-001',
					hours: -1 
				}
			],
			userdata: {
				email			:	S.prop('email', userobj).getOrElse(''),
				name	    : S.path(['profile', 'name'], userobj).getOrElse('')
			}
		}
	})
});
exports.sendForgotEmailData = sendForgotEmailData;

// sendForgotEmail = s -> o -> task o 
var sendForgotEmail = R.curry(function(userobj) {
	return ajaxTask(sendForgotEmailData(userobj));
});
exports.sendForgotEmail = sendForgotEmail;


// sendForgotPasswordData :: s -> s ->  {o}
const sendForgotPasswordData = R.curry((resetlink, userobj) => {
	return ({
		url: 		url_email_base + ':' + url_email_port + url_email_api + '/reset-password-request',
		method: 'POST',
		data:  {	
			email:    S.prop('email', userobj).getOrElse(''),
			mailplan: [
				{
					template: 'reset-password-request-001',
					hours: -1 
				}
			],
			userdata: {
				email			:	S.prop('email', userobj).getOrElse(''),
				name	    : S.prop('name', userobj).getOrElse(''),
        resetlink : resetlink
			}
		}
	})
});
exports.sendForgotPasswordData = sendForgotPasswordData;

// sendForgotPassword = s -> o -> task o 
var sendForgotPassword = R.curry(function(resetlink, userobj) {
	return ajaxTask(sendForgotPasswordData(resetlink, userobj));
});
exports.sendForgotPassword = sendForgotPassword;
