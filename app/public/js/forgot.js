var vuLogin = new Vue({
  el: '.login-form',
  data: {
    errortext: '',
    email: ''
  },
  methods: {
    resetPassword: function(e) {

      if (R.trim(this.email) === "") {
        alert('Please enter a valid email address'); 
        $('#email').addClass('input-error');
        $('#email').focus();
        e.preventDefault();
      };

    }
  }
});
