var vuProfile = new Vue({
  el: '#profile',
  data: {
		saveDisabled: false,
		useremail: '',
		userkey: '',
		pressUnsubscribe: false
  },
  methods: {
    saveProfile: function(email) {

      // prevent the tutorial from showing
      var userdata = vuProfile.savedata;
			submitUserData(vuProfile.useremail, vuProfile.userkey);

    }
  }
});

// submitUserData :: JSONObj
const submitUserData = R.curry(function(email, key) {

  // Show full page LoadingOverlay
  var ajaxCall = $.ajax({
    type: "PATCH",
    url: "/api/v1/accounts/" + email + '/unsubscribe',
    timeout: 60000,
    dataType: "json",
    data: {
			key: key
		},
    cache: false
  });

  ajaxCall.done(function(result) {
		vuProfile.pressUnsubscribe = true;
  });

  ajaxCall.fail(function(e, errortext) {
		alert('Unauthrozied user');
  });

});

//setupData
const setupData = function (email, key) {
	vuProfile.useremail = email;
	vuProfile.userkey = key;
};
