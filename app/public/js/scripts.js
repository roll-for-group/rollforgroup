
jQuery(document).ready(function() {
    /*
        Login form validation
    */
    $('.login-form input[type="email"], .login-form input[type="password"]').on('focus', function() {
        $(this).closest('.form-group').removeClass('has-error');
    });

    $('.login-form').on('submit', function(e) {
        $(this).find('input[type="email"], input[type="password"]').each(function() {
            if( $(this).val().trim() == '' ) {
                e.preventDefault();
                $(this).closest('.form-group').addClass('has-error');
            }
            else {
                $(this).closest('.form-group').removeClass('has-error');
            }
        });
    });

    /*
        Registration form validation
    */
    $('.registration-form input[type="email"], input[type="password"]').on('focus', function() {
        $(this).closest('.form-group').removeClass('has-error');
    });

    $('.registration-form').on('submit', function(e) {
        $(this).find('input[type="email"], input[type="password"]').each(function() {
            if( $(this).val().trim() == '' ) {
                e.preventDefault();
                $(this).closest('.form-group').addClass('has-error');
            }
            else {
                $(this).closest('.form-group').removeClass('has-error');
            }
        });
    });
});
