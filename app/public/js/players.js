// vuPlayers
var vuPlayers = new Vue({
  el:       '#players',
  data:     {
    preload: true,
    viewby: 'playxp',
    collectionurl: 'collection',
    players:[],
    networksize: 0,
    loading: true
  },
  methods:  {
    loadNetwork: function(email) {

      var ajaxNetwork = $.ajax({
        type: "GET",
        url: "/api/v1/accounts/" + email + "/networkmap",
        dataType: "json",
        data: {}
      });

      ajaxNetwork.done(function(data) {

        // create an array with nodes
        var nodes = new vis.DataSet(data.nodes);

        // create an array with edges
        var edges = new vis.DataSet(data.edges);

        vuPlayers.networksize = data.nodes.length;

        // create a network
        var container = document.getElementById("playernetwork");
        var data = {
          nodes: nodes,
          edges: edges
        };
        var options = {
          nodes: {
            borderWidth: 4,
            size: 30,
            color: {
              border: "#222222",
              background: "#acacac"
            },
            font: { color: "#666666" }
          },
          edges: {
            color: "#acacac"
          }
        };

        var network = new vis.Network(container, data, options);

      });

      ajaxNetwork.fail(function(err) {
        alert("ERR:" + JSON.stringify(err));
      });

    },
    playPillsClass: function(viewby) {
      return (viewby === 'playxp')
        ? 'active'
        : ''
    },
    hostPillsClass: function(viewby) {
      return (viewby === 'hostxp')
        ? 'active'
        : ''
    },
    hostSortClass: function(viewby) {
      return (viewby === 'hostxp')
        ? 'fa fa-home fatooltip'
        : 'fa fa-trophy fatooltip'
    },
    playPillsClick: function(email, distance) {
      vuPlayers.viewby = 'playxp';
      vuPlayers.showByPlayXP('play', email, distance);
    },
    hostPillsClick: function(email, distance) {
      vuPlayers.viewby = 'hostxp';
      vuPlayers.showByPlayXP('host', email, distance);
    },
    buildSlug: function(slug) {
      return 'users?id=' + slug;
    },
    buildCollectionSlug: function(slug) {
      return 'collection?id=' + slug;
    },
    buildCollectionOwn: function(slug) {
      window.location.href =  'collection?id=' + slug;
    },
    buildCollectionWtp: function(slug) {
      window.location.href = 'collection?listtype=wanttoplay&id=' + slug;
    },
    buildSendMail: function(slug) {
      window.location.href = 'mail?userid=' + slug;
    },
    defaultAvatar: function(avataricon) {
      return avataricon === undefined
        ? '/img/default-avatar-sq.jpg'
        : avataricon;
    },
    showXP: function(playxp, hostxp, viewby) {
      return viewby === 'hostxp'
        ? hostxp
        : playxp
    },
    showByPlayXP: function(xptype, email, distance) {

      var options = {
        timeout: 5000,
        maximumAge: 0
      };

      // $('#spinner').show();
      vuPlayers.loading=true;
      var ajaxPlayers;

      const mergeGame = R.merge({games: [], gamesloaded:false});

      var finishAjax = function(ajaxPlayersDone) {
        ajaxPlayersDone.done(function(data) {

          vuPlayers.preload = false;
          vuPlayers.players = R.map(mergeGame, data);

          for (var i=0;i<vuPlayers.players.length;i++) {
            vuPlayers.lookUpGames(i, vuPlayers.players[i].pageslug);
          }

          // $('#spinner').hide();
          vuPlayers.loading=false;
          setTimeout(function() {
            $('.fatooltip').tooltip();
          }, 1000);
        });

        ajaxPlayersDone.fail(function(data) {
          vuPlayers.preload = false;
          vuPlayers.players = [];
          vuPlayers.loading=false;
          // $('#spinner').hide();
        });
      }

      var findPlayers = function(position) {
        ajaxPlayers = $.ajax({
          type: "GET",
          url: "api/v1/accounts/nearby/coordinates",
          dataType: "json",
          data: {
            lat: position.coords.latitude, 
            lng: position.coords.longitude,
            xptype: xptype,
            distance: distance,
          }
        });
        finishAjax(ajaxPlayers);
      }

      var errorPosition = function(err) {
        ajaxPlayers = $.ajax({
          type: "GET",
          url: "api/v1/accounts/nearby/email/" + email,
          dataType: "json",
          data: {
            distance: distance,
            xptype: xptype
          }
        });
        finishAjax(ajaxPlayers);
      };
      navigator.geolocation.getCurrentPosition(findPlayers, errorPosition, options);
    },
    lookUpGames: function (arrayid, playerslug) {
      var ajaxPlayerGames = $.ajax({
        type: "GET",
        url: "api/v1/collections/listtype/wanttoplay",
        dataType: "json",
        data: {
          userid: playerslug,
          gamelimit: 10 
        }
      });

      ajaxPlayerGames.done(function(data) {
        if (data.games.length==0) {
          var ajaxPlayerGamesOwn = $.ajax({
            type: "GET",
            url: "api/v1/collections/listtype/own",
            dataType: "json",
            data: {
              userid: playerslug,
              gamelimit: 4
            }
          });

          ajaxPlayerGamesOwn.done(function(data) {
            vuPlayers.players[arrayid].games = data.games;
            vuPlayers.players[arrayid].listkey = data.listkey;
            vuPlayers.players[arrayid].gamesloaded = true;
          });

          ajaxPlayerGamesOwn.fail(function(data) {
            vuPlayers.players[arrayid].games = [];
            vuPlayers.players[arrayid].gamesloaded = true;
          });

        } else {
          vuPlayers.players[arrayid].games = data.games;
          vuPlayers.players[arrayid].listkey = data.listkey;
          vuPlayers.players[arrayid].gamesloaded = true;
        }

      });

      ajaxPlayerGames.fail(function(data) {
        vuPlayers.players[arrayid].games = [];
        vuPlayers.players[arrayid].gamesloaded = true;
      });

    },
    lookUpPlayerCollection: function (playerslug) {
      var ajaxPlayerGames = $.ajax({
        type: "GET",
        url: "api/v1/collections/listtype/wanttoplay",
        dataType: "json",
        data: {
          userid: playerslug,
          gamelimit: 4
        }
      });

      ajaxPlayerGames.done(function(data) {
        vuPlayers.collectionurl = vuPlayers.buildCollectionSlug(data.listkey);
      });

      ajaxPlayerGames.fail(function(data) {});

    }
  }
});

$('#setlocation').typeahead(
  {
    hint: true,
    highlight: true,
    minLength: 1
  },
  {
    source: function(query, sync, process) {
      return $.ajax({
        type: "GET",
        url: "api/v1/locations",
        dataType: "json",
        data: {
          search: query
        },
        success: function(data) {
          cityResults = data;
          return process(R.map(R.path(['place','address']), data));
        }
      });
    }
  }
);

$('#savelocation').on('click', function() {
  saveLocation($('#setlocation').val());
});

// setup the page
const setuppage = function(email, distance, slug) {
  vuPlayers.showByPlayXP('play', email, distance);
  vuPlayers.lookUpPlayerCollection(slug);
  vuPlayers.loadNetwork(slug);
}

