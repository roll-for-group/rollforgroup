var vuGroups = new Vue({
  el: '#maincontent',
  data: {
    name        : '',
    description : 'Come and play board games with us!',
    locname     : ''
  },
  methods: {
    wrapChars: function(mustselect, before, after) {
      var editor = document.getElementById("notes");

      //var editorHTML = editor.value;
      var selectionStart = 0, selectionEnd = 0;

      if (editor.selectionStart) selectionStart = editor.selectionStart;
      if (editor.selectionEnd) selectionEnd = editor.selectionEnd;

      if ((selectionStart != selectionEnd) || !mustselect) {
        var editorCharArray = vuGroups.description.split("");
        editorCharArray.splice(selectionEnd, 0, after);
        editorCharArray.splice(selectionStart, 0, before); //must do End first
        vuGroups.description = editorCharArray.join("");
      }

    },
    renderDescription: function(desc) {

      const defaults = {
        html:         false,        // Enable HTML tags in source
        xhtmlOut:     false,        // Use '/' to close single tags (<br />)
        breaks:       true,        // Convert '\n' in paragraphs into <br>
        langPrefix:   'language-',  // CSS language prefix for fenced blocks
        linkify:      true,         // autoconvert URL-like texts to links
        typographer:  true,        // Enable smartypants and other sweet transforms
      };

      var mdHtml = window.markdownit(defaults).use(window.markdownitEmoji);

      return mdHtml.render(desc);
    },
    pressHeadDesc: function() {
      vuGroups.wrapChars(false, "\n## ", "\n");
    },
    pressBoldDesc: function() {
      vuGroups.wrapChars(true, "**", "**");
    },
    pressItalDesc: function() {
      vuGroups.wrapChars(true, "*", "*");
    },
    pressQuotDesc: function() {
      vuGroups.wrapChars(false, "\n> ", "\n\n");
    },
    pressListUlDesc: function() {
      vuGroups.wrapChars(false, "\n* ", "\n\n");
    },
    pressListOlDesc: function() {
      vuGroups.wrapChars(false, "\n1. ", "\n\n");
    },
    pressLinkDesc: function() {
      var url = prompt("What is the URL?");
      vuGroups.wrapChars(false, "[", "](http://" + url + ")");
    },
    pressImagDesc: function() {
      var url = prompt("What is the picture URL?");
      vuGroups.wrapChars(false, "![", "](http://" + url + ")");
    },
    pressYtubeDesc: function() {
      var url = prompt("What is the youtube video id?");
      if (url.length == 11) {
        vuGroups.wrapChars(false, "[![", "](https://img.youtube.com/vi/" + url + "/0.jpg)](https://www.youtube.com/watch?v=" + url + ")");
      } else {
        alert("A youtube id can only be 11 characters long. Copy the youtube video id from the end of any youtube video url");
      }
    },
    formatHelp: function() {
      window.open("http://commonmark.org/help/", "_blank");
    },
    bgglink: function(id) {
      return 'games?gameid=' + id;
    },
    cancel: function() {
      window.location.href = '/groups';
    }
  }
});


// if there is anything to setup
var setuppage = function(useremail) {
};

const creategroup = function(useremail) {

  const chosenAddress = R.compose(R.propEq('address', $('#playarea').val()), R.prop('place'));
  const chosenCity = R.compose(R.path(['place', 'coords']), R.find(chosenAddress));
  const cityFound = R.compose(R.not, R.isNil, chosenCity);
  const getLat = R.compose(R.prop('lat'), chosenCity);
  const getLng = R.compose(R.prop('lng'), chosenCity);

  const notNil = R.compose(R.not, R.isNil);
  const notEmpty = R.compose(R.not, R.isEmpty);

  // submitUserData :: JSONObj
  const submitUserData = function(senddata) {

    $.ajax({
      type: "POST",
      url: "/api/v1/groups",
      dataType: "json",
      data: senddata,
      cache: false
    }).done(function(x) {
      $("#nextverify").prop('disabled', false);
      window.location.href = '/groups/' + x.url;
    }).fail(function(err) {
      alert(JSON.stringify(err));
      alert('Match could not be created, please try again');
      $("#nextverify").prop('disabled', false);
    });

  };


  $("#nextverify").prop('disabled', true);

  var ajaxCall = $.ajax({
    type: "GET",
    url: "/api/v1/locations",
    dataType: "json",
    data: {search: $('#playarea').val()}
  });

  ajaxCall.done(function(data) {

    if ($('#name').val().length > 0) {
      if (($('#playarea').val().length > 0) && notNil(data) && cityFound(data)) {

        var formdata = {
          name: 				vuGroups.name,
          description: 	vuGroups.description,
          locname:      $('#playarea').val(),
          status: 			'draft', 
          isprivate: 		($('#isprivate').val() == 1), 
          email: 				useremail,
          playarea: 		$('#playarea').val(),
          playarealat: 	getLat(data),
          playarealng: 	getLng(data),
          autoaccept:		$('#autoaccept').prop('checked')
        };

        submitUserData(formdata);

      } else {
        alert('Please select a valid location');
        $('#div4playarea').addClass('has-warning');
        $('#playarea').focus();
        $('#nextverify').prop('disabled', false);
      }
    } else {
      alert('Please enter a valid group name');
      $('#div4name').addClass('has-warning');
      $('#name').focus();
      $('#nextverify').prop('disabled', false);
    }

  });

  ajaxCall.fail(function(data) {
    alert('The location could not be geolocated, please ensure the location is valid');
    $('#div4playarea').addClass('has-warning');
    $('#playarea').focus();
    $("#nextverify").prop('disabled', false);
  });

};


