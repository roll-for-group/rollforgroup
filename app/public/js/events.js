var locationdata = [];

const alertify = function(c) {
  alert(JSON.stringify(c));
  return c;
};

// verifyHasLocality :: o -> bool 
const verifyHasLocality = function(data) {
  return R.has('place', data) && 
    R.has('locality', data.place) &&
    R.has('country', data.place)
};

// buildLocality :: o -> s
const buildLocality = function(data) {
  return data.place.locality + ", " + data.place.country;
};

// splitCommaTrim :: s -> [a]
const splitCommaTrim = function(commatext) {
  return R.split(',', commatext).map(R.trim);
};

// verifyLocatoinCountry :: o -> s -> Bool
const verifyLocalityCountry = R.curry(function(text, placeObj) {
  return (splitCommaTrim(text).length == 2) &&
    splitCommaTrim(text)[0] == placeObj.place.locality &&
    splitCommaTrim(text)[1] == placeObj.place.country;
});

// reduceverifyLocality :: s -> o -> bool
const reduceVerifyLocality = function(locality, data) {
  return R.compose(
    R.reduce(R.or, true),
    R.map(verifyLocalityCountry(locality))
  )(data);
};


var vuMatchList = new Vue({
  el: '#vumatchlist',
  data: {
    ispremium: false,
    url: '',
    haslocation: true,
    preload:  true,
    matchdates: [],
    icaltext: "Show iCal URL Link",
    icallinkvisible: false,
    activeLocation: "",
    hasSelectedLocation: false,
    selectedLocation: {}
  },
  methods:{
    openEvent: function(pageslug) {
      window.location.href = "/events/" + pageslug;
    },
    loadTooltips: function() {
      $('.vutooltip').tooltip();
      $('.img-circle').tooltip();
    },
    hostgame: function() {
      window.location.href = "/hostgame" 
    },
    purchasemembership: function() {
      window.location.href = "/buymembership" 
    },
    showicallink: function() {
      if (this.icallinkvisible) {
        this.icallinkvisible = false;
        this.icaltext = "Show iCal URL Link";
      } else {
        this.icallinkvisible = true;
        this.icaltext = "Hide iCal URL Link";
      }
    },
    locationCancel: function() {
      $("#setlocationDiv").show();
      $("#setlocation").focus();
      $("#setlocation").typeahead("close");
      $("#setlocation").val("");
      this.activeLocation = "";
      this.hasSelectedLocation = false; 

      /*
      this.marginhack = this.marginhack - 1;
      $("span.twitter-typeahead").css('margin-top', this.marginhack + "px");
      */
      setTimeout(function() { 
        // setupTypeahead('#setlocation');
        vuMatchList.loadTooltips();
      }, 500);

      this.loadAllEvents();

    }, 
    loadGames: function(gamesList) {

      // lookupXPLevel :: n => {o}
      const lookupXPLevel = function(x) {
        return R.objOf('xp', (x == 1) 
          ? {} 
          : (x == 2) 
            ? {text: 'Learn to Play', class: 'label label-success'} 
            : (x == 3) 
              ? {text: 'Game Demo', class: 'label label-danger'} 
              : (x == 4) 
                ? {text: 'For Experienced', class: 'label label-info'} 
                : (x == 5) 
                  ? {text: 'Role Play', class: 'label label-warning'} 
                  : (x == 6) 
                    ? {text: 'Online Board Games', class: 'label label-primary'} 
                    : (x == 7) 
                      ? {text: 'Tournament', class: 'label label-info'} 
                      : (x == 8) 
                        ? {text: 'Miniature Painting', class: 'label label-info'} 
                        : (x == 9) 
                          ? {text: 'Convention Event', class: 'label label-warning'} 
                          : (x == 10) 
                            ? {text: 'Repeating Campaign', class: 'label label-info'}
                            : {text: 'Online Role Play', class: 'label label-primary'}) 
      };

      // applyXPLevel :: o -> o
      const applyXPLevel = function(x) {
        return R.merge(lookupXPLevel(x.experience), x);
      };

      // applyStatus :: o -> s -> o
      const applyStatus = function(event) {
        return R.merge(event, {
          containerClass: (event.status === 'open')
            ? (event.issignedup)
              ? 'row eventcontainer'
              : 'row eventcontainer isnotsignedup'
            : 'row drafteventcontainer',
          statusLabel: (event.status === 'draft') 
            ? {text: 'Draft - not visible', class: 'label label-danger'}
            : {}
          }
        );
      };

      // datify :: dt -> dt
      const datify = function(date) {
        return moment(date).format('dddd') + ', ' + moment(date).format('LL');
      };

      const addday = function(gameObject) {
        var date = R.prop('date', gameObject);
        return R.merge({'day': datify(date), 'time': moment(date).format('LT')}, gameObject);
      };

      const addUTCday = function(gamearray) {
        var convertutc = function(date) { 
          moment(date).format('x');
        };
        return R.append(Number(convertutc(gamearray[0])), gamearray);
      };

      const buildVuList = R.compose(
        R.map(R.objOf('matchday')), 
        R.sort(R.nth(2)), 
        R.map(addUTCday), 
        R.toPairs, 
        R.groupBy(R.prop('day')), 
        R.map(applyXPLevel), 
        R.map(addday), 
        R.map(applyStatus),
        R.prop('matches')
      ); 

      // show game type status ('host', 'invite', ...etc)
      vuMatchList.matchdates = buildVuList(gamesList);
      vuMatchList.preload = false;
      setTimeout(function() {
        $('.vutooltip').tooltip();
      }, 1000);

    },
    loadOnlineEvents: function() {
      $.ajax({
        type: "GET",
        url: "/api/v1/matches/",
        dataType: "json",
        data: {experience:[6,11]}
      }).done(function(x) {
        vuMatchList.loadGames(x);

      }).fail(function(err) {
        alert("An error occured while loading events");

      });
    },
    loadAllEvents: function() {
      $.ajax({
        type: "GET",
        url: "/api/v1/matches/",
        dataType: "json",
        data: {}
      }).done(function(x) {
        vuMatchList.loadGames(x);

      }).fail(function(err) {
        alert("An error occured while loading events");

      });
    },
    loadDefaultEvents: function() {
      $.ajax({
        type: "GET",
        url: "/api/v1/matches/",
        dataType: "json",
        data: {}
      }).done(function(x) {
        vuMatchList.loadGames(x);

      }).fail(function(err) {
        alert("An error occured while loading events");

      });
    },
    setByCoordinates: function(position) {
      $.ajax({
        type    : "GET",
        url     : "/api/v1/locations",
        dataType: "json",
        data    : {
          lat: position.coords.latitude, 
          lng: position.coords.longitude
        },
        success : function(data) {

          R.compose(
            vuMatchList.showlocation,
            R.path(['place', 'locality']),
            R.head,
            R.filter(verifyHasLocality)
          )(data)
        }
      });

      $.ajax({
        type: "GET",
        url:  "/api/v1/matches",
        dataType: "json",
        data: {
          latitude: position.coords.latitude, 
          logitude: position.coords.longitude,
          distance: 30000
        }
      }).done(function(x) {
        vuMatchList.loadGames(x);

      }).fail(function(err) {
        alert("An error occured while loading nearby events");

      });
    },
    locationSet: function() {
      var options = {
        timeout: 5000,
        maximumAge: 0
      };

      navigator.geolocation.getCurrentPosition(vuMatchList.setByCoordinates, this.loadAllEvents, options);

    },
    showlocation: function(location) {
      $("#setlocationDiv").hide();
      this.activeLocation = location;
      this.hasSelectedLocation = true; 
      this.loadTooltips();

    },
    updateLocation: function() {
      saveLocation(vuMatchList.selectedLocation);

    }
  }
});

const setupTypeahead = function(divname) {

  $(divname).typeahead(
    {
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      source: function(query, sync, process) 
      {
        return $.ajax({
          type    : "GET",
          url     : "/api/v1/locations",
          dataType: "json",
          data    : {search: query},
          success : function(data) {
            locationdata = R.filter(verifyHasLocality, data)
            setTimeout(function() {
              return process(R.map(buildLocality, locationdata));
            }, 300);
          }
        });
      }
    }
  );

  $(divname).bind('typeahead:selected', function(obj, datum) {
    try {

      if (reduceVerifyLocality(datum, locationdata)) {

        vuMatchList.showlocation(R.find(verifyLocalityCountry(datum), locationdata).place.locality);

        vuMatchList.selectedLocation = R.find(verifyLocalityCountry(datum), locationdata);

        var findlocation = function(coords) {
          $.ajax({
            type: "GET",
            url:  "/api/v1/matches",
            dataType: "json",
            data: {
              latitude: coords.lat, 
              longitude: coords.lng,
              distance: 30000
            }
          }).done(function(x) {
            vuMatchList.loadGames(x);

          }).fail(function(err) {
            alert("An error occured while loading nearby events");

          });
        }
        findlocation(R.find(verifyLocalityCountry(datum), locationdata).place.coords);

      }
    } catch(err) {
      alert("ERR: " + err);
    }
  })


};

// setupPage :: bool -> 
const setupPage = function(haslocation, ispremium, icalurl) {

  vuMatchList.haslocation = haslocation;
  vuMatchList.ispremium = ispremium;
  vuMatchList.url = icalurl;

  // vuMatchList.setByCoordinates

  setupTypeahead('#setlocation');
  setTimeout(function() { 
    vuMatchList.loadTooltips(); 
  }, 200);

};
