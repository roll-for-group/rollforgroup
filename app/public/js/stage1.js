var vuStage = new Vue({
  el: '.containerx',
  data: {
    pageslug: '',
    whyfindlocalgamers  : false, 
    whyfindlocalevents  : false, 
    whymanageboardgames : false, 
    whymanagemyevent    : false,
    whyother            : "" 

  },
  methods: {
    nextclick: function(email, userkey, whyfindlocalgamers, whyfindlocalevents, whymanageboardgames, whymanagemyevent, whyother) {

      // Show full page LoadingOverlay
      $.LoadingOverlay("show", {'z-index': 9999});

      ajaxNext = $.ajax({
        type: "PATCH",
        url: "/api/v1/accounts/" + email,
        timeout: 5000,
        dataType: "json",
        data: {
          stage:        1,
          key:          userkey,
          whyfindlocalgamers  : whyfindlocalgamers, 
          whyfindlocalevents  : whyfindlocalevents, 
          whymanageboardgames : whymanageboardgames, 
          whymanagemyevent    : whymanagemyevent,
          whyother:     whyother
        },
        cache: false
      });

      ajaxNext.done(function(result) {
        $.LoadingOverlay("hide");
        window.location.href = '/stage2';
      });

      ajaxNext.fail(function(e, errortext) {
        $.LoadingOverlay("hide");
        alert(errortext);
      });

    }
  }
});
