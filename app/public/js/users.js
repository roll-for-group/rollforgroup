new WOW().init();

var vuUserList = new Vue({
  el: '#maincontent',
  data: {
    userslug: '',
    username: '',
    activeUserEmail: '',
    collections: [],
    showcollections: false,
    icollection: 0
  },
  methods: {
    giftgems: function() {
      if (confirm("Are you sure you want to gift gems to " + vuUserList.username + "?")) {
        $.LoadingOverlay("show");
        var ajaxCall = $.ajax({
          type: "PATCH",
          url: "/api/v1/accounts/" + vuUserList.activeUserEmail + "/purchase",
          timeout: 60000,
          dataType: "json",
          data: {transferto: vuUserList.userslug },
          cache: false
        });

        ajaxCall.done(function(result) {
          $.LoadingOverlay("hide");
          window.location.href = "giftthankyou";
        });

        ajaxCall.fail(function(e, errortext) {
          $.LoadingOverlay("hide");
          window.location.href = "buymembership";
        });

      }

    },
    loadUser: function(fromemail, pageslug) {

      vuUserList.activeUserEmail = fromemail;
      vuUserList.userslug = pageslug;

      var ajaxCall = $.ajax({
        type: "GET",
        url: "/api/v1/accounts/" + pageslug,
        timeout: 10000,
        dataType: "json",
        data: {},
        cache: false
      });

      ajaxCall.done(function(result) {
        vuUserList.username = R.prop('name', result);
        var bio = R.prop('biography', result)
        if (bio.length > 0) {
          $('.rendermarkdown').html(
            mdHtml.render(bio )
          );
        }
        $.LoadingOverlay("hide");
      });

      ajaxCall.fail(function(e, errortext) {
        $.LoadingOverlay("hide");
        window.location.href = "events";
      });

    },
    sendMail: function() {
      window.location.href = "mail?userid=" + vuUserList.userslug;
    },
    loadCollectionGames: R.curry(function(i, collectionid) {

      var ajaxCollectionCall = $.ajax({
        type: "GET",
        url: "/api/v1/collections/" + collectionid + "/games",
        timeout: 10000,
        dataType: "json",
        data: {},
        cache: false
      });

      ajaxCollectionCall.done(function(result) {
        vuUserList.collections[i].games = result;

        vuUserList.icollection = vuUserList.icollection - 1;
        if (vuUserList.icollection == 0) {

          vuUserList.showcollections = true;

          setTimeout(function() {
            $(".thumbnail-list").mThumbnailScroller({
              type: "click-50",
              theme: "buttons-in"
            })
          },
          200);

          setTimeout(function() {
            $('.js-tooltip').tooltip({container:'body'});
          }, 1000)

        };

      });

      ajaxCollectionCall.fail(function(e, errortext) {
        vuUserList.collections[i].games = [];
      });

    }),
    loadCollections: function(userid) {

      var ajaxCollectionCall = $.ajax({
        type: "GET",
        url: "/api/v1/collections",
        timeout: 10000,
        dataType: "json",
        data: {userid: userid},
        cache: false
      });

      // assocGames :: o -> o
      const assocGames = function(a) {
        return R.assoc('games', [], a);
      };

      ajaxCollectionCall.done(function(result) {

        vuUserList.icollection = result.length;
        vuUserList.collections = R.map(assocGames, result);

        for (var i=0;i<vuUserList.collections.length;i++) {
          vuUserList.loadCollectionGames(i, vuUserList.collections[i].key);
        }


      });

    },
    openPageUrl: function(collectionkey) {
      window.open("/collection?id=" + collectionkey, "_target");
    },
    openGameUrl: function(gameid) {
      window.open("/games?gameid=" + gameid, "_target");
    }
  }
});

var defaults = {
  html:         false,        // Enable HTML tags in source
  xhtmlOut:     false,        // Use '/' to close single tags (<br />)
  breaks:       true,        // Convert '\n' in paragraphs into <br>
  langPrefix:   'language-',  // CSS language prefix for fenced blocks
  linkify:      true,         // autoconvert URL-like texts to links
  typographer:  true,        // Enable smartypants and other sweet transforms
};

var mdHtml = window.markdownit(defaults).use(window.markdownitEmoji);

// setuppage :: id
var setuppage = function (email, userslug) {

  vuUserList.loadUser(email, userslug);
  if (userslug !== '') {
    vuUserList.loadCollections(userslug);
  };

  $('#buyMembership').on('click', function() {
    window.location.href = "buymembership";
  });
  $('#buyGems').on('click', function() {
    window.location.href = "buygems";
  });

  $('.squaretime').tooltip({container:'body'});
  $('.js-tooltip').tooltip({container:'body'});

};
