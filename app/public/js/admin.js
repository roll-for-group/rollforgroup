
// alertify :: o -> o
const alertify = (x) => {
  alert(JSON.stringify(x));
  return x;
} 

// buildAjaxObj :: {o} -> {p}
const buildAjaxObj = R.curry(function(endpoint) {
  return R.zipObj(
    ['type', 'url', 'dataType', 'data', 'cache'], 
    ['GET', '/api/v1/admin/' + endpoint, 'json', {}, false]
  );
});

var vuAdmin = new Vue({
  el: '.admin',
  data: {
    netpromoterscore: 0,
    promoters: 0,
    detractors: 0,
    userscount: 0,
    usercounts: [
      { x: "2014-06-11", y: 10 },
      { x: "2014-06-12", y: 25 },
      { x: "2014-06-13", y: 30 },
      { x: "2014-06-14", y: 10 },
      { x: "2014-06-15", y: 15 },
      { x: "2014-06-16", y: 30 }
    ],
    whymanagemyevent    : 0,
    whyfindlocalgamers  : 0,
    whyfindlocalevents  : 0,
    whymanageboardgames : 0,

  },
  methods: {
    loadDataset: function() {

      $.ajax(
        buildAjaxObj('netpromoterscore')
      ).done(function(x) {

        vuAdmin.netpromoterscore = x.netpromoterscore;
        vuAdmin.promoters = x.promoters;
        vuAdmin.detractors = x.detractors;
        vuAdmin.userscount = x.userscount;

        vuAdmin.whymanagemyevent = x.whymanagemyevent;
        vuAdmin.whyfindlocalgamers = x.whyfindlocalgamers;
        vuAdmin.whyfindlocalevents = x.whyfindlocalevents;
        vuAdmin.whymanageboardgames = x.whymanageboardgames;

      }).fail(function(err) {
        alert('Not authorised');
        window.location.href = '/login';
      });

    },
    loadHistoricDataset: function() {

      $.ajax(
        buildAjaxObj('netpromoterscore/monthly')
      ).done(function(x) {

        // buildUserCountPoint :: o -> o
        const buildUserCountPoint = function(x) {

          var dateObj   = new Date(x.date);

          // zeroNumber :: n -> s
          const zeroNumber = function(myNumber) {
            return ("0" + myNumber).slice(-2)
          };

          const dMonth  = zeroNumber(dateObj.getUTCMonth() + 1);
          const dDay    = zeroNumber(dateObj.getUTCDate());
          const dYear   = dateObj.getUTCFullYear();

          // return {x: x.date, y: x.userscount, group: 0 }
          return {x: dYear + "-" + dMonth + "-" + dDay, y: x.userscount}

        };

        var container = document.getElementById('visualization');
        vuAdmin.usercounts = R.map(buildUserCountPoint, x);

        var dataset = new vis.DataSet(vuAdmin.usercounts);

        var startdate = new Date();
        startdate = new Date(startdate.setMonth(startdate.getMonth()-25)); 

        var enddate = new Date();
        enddate = new Date(enddate.setMonth(enddate.getMonth()+1)); 

        var options = {
          style:'bar',
          barChart: {width:50, align:'center'}, // align: left, center, right
          drawPoints: false,
          orientation:'top',
          start: startdate,
          end: enddate 
        };

        var graph2d = new vis.Graph2d(container, vuAdmin.usercounts, options);
        // var graph2d = new vis.Graph2d(container, dataset, options);


      }).fail(function(err) {
        alert('Not authorised');
        window.location.href = '/login';
      });

    }
  }


});
