// buildAjaxObj :: {o} -> {p}
const buildAjaxObj = R.curry(function(key, data) {
  return R.zipObj(['type', 'url', 'dataType', 'data', 'cache'], ['PATCH', '/api/v1/matches/' + key, 'json', data, false])
});

// buildAjaxData :: k -> d -> {o}
const buildAjaxData = R.curry(function(email, key, keydata) {
  return R.zipObj(['email', key], [email, keydata]);
});

// showLoginWindow ::
function showLoginWindow() {
  $("#popUpWindow").modal();
  setTimeout(function() {$(".modal-backdrop").fadeOut()}, 100);
}

// showJoinupWindow ::
function showJoinupWindow() {
  $("#joinUpWindow").modal();
  setTimeout(function() {$(".modal-backdrop").fadeOut()}, 100);
}

// momentify :: [dt, dt] -> moment
var momentify = R.compose(
  moment, 
  R.join(' ')
);


var vuJoinModal = new Vue({
  el: '#joinUpWindow',
  data: {},
  methods: {
    interested: function() {
      vuUserButtons.userButtonPress('interested');
      $('#joinUpWindow').modal('toggle');
    },
    join: function() {
      vuUserButtons.userButtonPress('join');
      $('#joinUpWindow').modal('toggle');
    },
    close: function() {
      $('#joinUpWindow').modal('toggle');
    }
  }
});

var vuPublic = new Vue({
  el: '#makepublic',
  data: {
    isprivate: false,
    showEdit: false,
    reoccurenable: false, 
    eventid : '',
    seriesid: ''
  },
  methods: {
    geturl: function(reoccurenable, eventid, seriesid) {
      return (reoccurenable)
        ? 'https://www.rollforgroup.com/events/series/' + seriesid
        : 'https://www.rollforgroup.com/events/' + eventid
    },
    publishPublic: function(key, useremail) {
      $.ajax({
        type: "PATCH",
        url: "/api/v1/matches/" + key + "/publish",
        dataType: "json",
        data: {
          email: useremail, isprivate: false
        },
        cache: false
      }).done(function(x) {
        alert("This event is now publically listed and visible on Roll for Group.");
        window.location.href = '/events/' + x.key;
      }).fail(function(err) {
        showLoginWindow();
      })

    }
  }
});

// getQueryString :: s -> s
const getQueryString = function (key) {
  var key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
  var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
  return match && decodeURIComponent(match[1].replace(/\+/g, " "));
};


var vuLogin = new Vue({
  el: '.login-form',
  data: {
    eventid: '',
    showpassword: false,
    whenCapsIsOn: false,
    showLogonError: false,
    showLogonFailure: (getQueryString('failed')=='true'),
    errortext: '',
    formusername: '',
    formpassword: '',
  },
  methods: {
    getPasswordType: function(show) {
      return show 
        ? 'text' 
        : 'password'
    },
    detectCaps: function(e) {
      this.whenCapsIsOn = (e.getModifierState("CapsLock")); 
    },
    submitLoginForm: function(e) {
      this.errortext = '';

      if (this.formusername == '') {
        $('#form-username').focus();
        this.errortext = this.errortext + 'Please enter your email address. ';
        this.showLogonError = true;
      };

      if (this.formpassword == '') {
        this.showLogonError = true;
        if (this.errortext == '')  {
          $('#form-password').focus();
        };
        this.errortext = this.errortext + 'Please enter a password. ';
      };

      if (this.errortext != '') {
        e.preventDefault();
      }
    }
  }
});


var vuTitle = new Vue({
  el: '#headTitle',
  data: {
    textTitle: "",
    oldTitle: "",
    showTitle: true,
    showEdit: false,
    showSave: false,
    eventStatus: "",
    isprivate: false,
    key: '',
    useremail: ''
  },
  methods: {
    editTitle: function() {
      this.showTitle = false;
      this.showEdit = false;
      this.showSave = true;
      this.oldTitle = this.textTitle;
    },
    saveTitle: function(title) {

      const buildAjaxDataObj = R.compose(
        buildAjaxObj(this.key), 
        buildAjaxData(this.useremail)
      );

      $.ajax(
        buildAjaxDataObj('title', title)
      ).done(function(x) {
        vuTitle.oldTitle = title; 
        vuTitle.showTitle = true;
        vuTitle.showEdit = true;
        vuTitle.showSave = false;

      }).fail(function(err) {
        window.location.href = '/login';

      });
    },
    cancelTitle: function() {
      this.showTitle = true;
      this.showEdit = true;
      this.showSave = false;
      this.textTitle = this.oldTitle;
    },
    isDraft: function(status) {
      return (status === 'draft');
    }
  }
});


var vuLabel = new Vue({
  el: '#label',
  data: {
    showEdit: false,
    experience: 0,
    label:  {lblText:'Convention Event', lblClass:'warning', value:9},
    allLabels: [
      {lblText:'Convention Event', lblClass:'warning', value:9},
      {lblText:'Game Demo', lblClass:'danger', value:3},
      {lblText:'For Experienced Players', lblClass:'info', value:4},
      {lblText:'For Beginners', lblClass:'info', value:2},
      {lblText:'Meet Up', lblClass:'default', value:1},
      {lblText:'Miniature Painting', lblClass:'info', value:8},
      {lblText:'Online', lblClass:'primary', value:6},
      {lblText:'Repeating Campaign', lblClass:'info', value:10},
      {lblText:'Role Play', lblClass:'warning', value:5},
      {lblText:'Tournament', lblClass:'info', value:7},
    ],
    key: '',
    useremail: ''
  },
  methods: {
    labelClassify: function(lbl) {
      return "label label-" + lbl;
    },
    selectItem: function(exp) {
      this.experience = exp;
      this.label = R.find(R.propEq('value', exp), this.allLabels);
    },
    saveItem: function(exp) {

      this.selectItem(exp);

      // buildAjaxDataObj :: (k -> d) -> {p}
      const buildAjaxDataObj = R.compose(
        buildAjaxObj(this.key),
        buildAjaxData(this.useremail)
      );

      $.ajax(
        buildAjaxDataObj('experience', exp)
      ).done().fail(function(err) {
        window.location.href = '/login';
      });
    }
  }
});

var vuDescription = new Vue({
  el: '.matchdescription',
  data: {
    showEdit: false,
    showSave: false,
    description: '',
    key: '',
    useremail: ''
  },
  methods: {
    wrapChars: function(mustselect, before, after) {
      var editor = document.getElementById("textDescription");

      var selectionStart = 0, selectionEnd = 0;

      if (editor.selectionStart) selectionStart = editor.selectionStart;
      if (editor.selectionEnd) selectionEnd = editor.selectionEnd;

      if ((selectionStart != selectionEnd) || !mustselect) {
        var editorCharArray = vuDescription.description.split("");
        editorCharArray.splice(selectionEnd, 0, after);
        editorCharArray.splice(selectionStart, 0, before); //must do End first
        vuDescription.description = editorCharArray.join("");
      }

    },
    pressHeadDesc: function() {
      this.wrapChars(false, "\n## ", "\n");
    },
    pressBoldDesc: function() {
      this.wrapChars(true, "**", "**");
    },
    pressItalDesc: function() {
      this.wrapChars(true, "*", "*");
    },
    pressQuotDesc: function() {
      this.wrapChars(false, "\n> ", "\n\n");
    },
    pressListUlDesc: function() {
      this.wrapChars(false, "\n* ", "\n*\n");
    },
    pressListOlDesc: function() {
      this.wrapChars(false, "\n1. ", "\n2.\n");
    },
    pressLinkDesc: function() {
      var url = prompt("What is the URL?");
      this.wrapChars(false, "[", "](http://" + url + ")");
    },
    pressImagDesc: function() {
      var url = prompt("What is the picture URL?");
      this.wrapChars(false, "![", "](http://" + url + ")");
    },
    pressYtubeDesc: function() {
      var url = prompt("What is the youtube video id?");
      if (url.length == 11) {
        this.wrapChars(false, "[![", "](https://img.youtube.com/vi/" + url + "/0.jpg)](https://www.youtube.com/watch?v=" + url + ")");
      } else {
        alert("A youtube id can only be 11 characters long. Copy the youtube video id from the end of any youtube video url");
      }
    },
    formatHelp: function() {
      window.open("http://commonmark.org/help/", "_blank");
    },
    renderDescription: function(desc) {
      return mdHtml.render(desc);
    },
    editVisible: function() {
      this.showEdit = false;
      this.showSave = true;
    },
    cancelSave: function() {
      this.showEdit = true;
      this.showSave = false;
    },
    saveDescription: function(desc) {

      const buildAjaxDataObj = R.compose(
        buildAjaxObj(this.key), 
        buildAjaxData(this.useremail)
      );

      $.ajax(
        buildAjaxDataObj('description', desc)
      ).done(function(x) {
        vuDescription.cancelSave();
      }).fail(function(err) {
        window.location.href = '/login';
      });

    }
  }
});


var vuTimeslots = new Vue({
  el: '#timeslots',
  data: {
    startdate:  moment().add(7, 'days').format('DD MMM YYYY'),
    starttime:  '19:00',
    enddate:    moment().add(7, 'days').format('DD MMM YYYY'),
    endtime:    '22:30',
    timeslots:  [],
    userstatus: '',
    votevisible: true,
    playercount: 1,
    key: '',
    useremail: '',
    userpageslug: ''
  },
  methods: {
    voteStyle: function(timeslot, players) {
      var perc = 100 * timeslot.votes.length / players;
      return "width: " + perc + "%;"
    },
    whenVotedForTimeslot: function(timeslot) {
      return R.merge(timeslot, R.find(R.propEq('userkey', this.userpageslug), timeslot.votes) 
        ? {votetitle: 'You want to play at this time', ischecked: true, ischeckedtrack: true}
        : {votetitle: 'Vote for this time', ischecked: false, ischeckedtrack: false}
      ); 
    },
    voteForTimeslot: function(timeslot) {
      $.ajax({
        type: (!timeslot.ischeckedtrack) 
          ? 'POST'
          : 'DELETE',
        url: "/api/v1/matches/" + this.key + "/timeslots/" + timeslot.id + "/votes",
        dataType: "json",
        data: {
          email: this.useremail 
        },
        cache: false
      }).done(function(x) {

        const incrementVote = R.curry(function(timeslotid, timeslot) {
          if (timeslotid === timeslot.id) {
            timeslot.votes.push({email:this.useremail, direction: 1}); 
            timeslot.votetitle = 'You want to play at this time';
            timeslot.ischecked = true;
            timeslot.ischeckedtrack = true;
          }
          return timeslot;
        });

        const deincrementVote = R.curry(function(timeslotid, timeslot) {
          if (timeslotid === timeslot.id) {
            timeslot.votes = R.init(timeslot.votes); 
            timeslot.votetitle = 'Vote for this time';
            timeslot.ischecked = false;
            timeslot.ischeckedtrack = false;
          }
          return timeslot;
        });

        if (!timeslot.ischeckedtrack) {
          vuTimeslots.timeslots = R.map(incrementVote(timeslot.id), vuTimeslots.timeslots);
        } else {
          vuTimeslots.timeslots = R.map(deincrementVote(timeslot.id), vuTimeslots.timeslots);
        }

      }).fail(function(err) {
        alert('Failed to vote for timeslot. Please ensure you are logged in and joined the event.');
      })
    },

    mergeDateTimes: function(timeslot) {
      timeslot.startdate = moment(R.prop('startdatetime', timeslot)).format('DD MMM YYYY');
      timeslot.starttime = moment(R.prop('startdatetime', timeslot)).format('HH:mm');
      timeslot.enddate = moment(R.prop('enddatetime', timeslot)).format('DD MMM YYYY');
      timeslot.endtime = moment(R.prop('enddatetime', timeslot)).format('HH:mm');
      return timeslot;
    },
    removeTimeslot: function(id) {
      $.ajax({
        type: "DELETE",
        url: "/api/v1/matches/" + this.key + "/timeslots/" + id,
        dataType: "json",
        data: {
          email     : this.useremail 
        },
        cache: false
      }).done(function(x) {
        vuTimeslots.timeslots = x
          .map(vuTimeslots.mergeDateTimes)
          .map(vuTimeslots.whenVotedForTimeslot);
      }).fail(function(err) {
        alert('You must be the event host to suggest a time slot');
      });
    },
    // areDatesValid :: dt -> dt -> dt -> dt -> bool 
    areDatesValid: function(startdate, starttime, enddate, endtime) {
      var err = '';
      if (!moment(startdate + ' ' + starttime, 'DD MMM YYYY HH:mm', true).isValid())
        err = 'The start date time is not valid. ';

      if (!moment(enddate + ' ' + endtime, 'DD MMM YYYY HH:mm', true).isValid())
        err = err + 'The end date time is not valid. ';

      if ((moment(enddate + ' ' + endtime).diff(moment(startdate + ' ' + starttime), "h")) > 24)
        err = err + 'The event is longer than 24 hours, please revise the start and end date times. '; 

      if (moment(enddate + ' ' + endtime).isBefore(moment(startdate + ' ' + starttime)))
        err = err + 'The end date time should be after the start date time. '; 

      return err;
    },
    pushTimeslot: function(startdate, starttime, enddate, endtime) {
      var err = vuTimeslots.areDatesValid(startdate, starttime, enddate, endtime);
      if (err == '') {    // dates pass the validity check
        $.ajax({
          type: "POST",
          url: "/api/v1/matches/" + this.key + "/timeslots",
          dataType: "json",
          data: {
            email     : this.useremail,
            startdatetime: moment(startdate + ' ' + starttime).format(),
            enddatetime: moment(enddate + ' ' + endtime).format()
          },
          cache: false
        }).done(function(x) {
          vuTimeslots.timeslots = x
            .map(vuTimeslots.mergeDateTimes)
            .map(vuTimeslots.whenVotedForTimeslot);
        }).fail(function(err) {
          alert('You must be the event host to suggest a time slot');

        });

      } else {
        alert(err);

      }
    },
    loadTimeslots: function(matchkey) {
      $.ajax({
        type: "GET",
        url: "/api/v1/matches/" + matchkey + "/timeslots",
        dataType: "json",
        data: {},
        cache: false
      }).done(function(x) {
        vuTimeslots.timeslots = x
          .map(vuTimeslots.mergeDateTimes)
          .map(vuTimeslots.whenVotedForTimeslot);
      });
    },
    publishTimeslot: function(matchkey, date, timeend) {
      $.ajax({
        type: "PATCH",
        url: "/api/v1/matches/" + matchkey + "/publish",
        dataType: "json",
        data: {
          email: this.useremail, 
          isprivate: true,
          date: date,
          timeend: timeend
        },
        cache: false
      }).done(function(x) {
        vuShowDate.loadDates(true, date, timeend);
        vuTitle.isprivate = true;
        vuUsers.isprivate = true;
        vuPublic.isprivate = true;
        vuTitle.eventStatus = 'open';
      }).catch(function(err) {
        alert('Please make sure you are the game host, and you are logged into the event');
      });

    }
  }
});


var vuUsers = new Vue({
  el: '#userspanel',
  data: {
    showEdit: false,
    showInvite: true,
    showInviteByEmail: false,
    modalPremiumPopup: false,
    showJoin: false,
    noUser: false,
    seatavailable: 3,
    seatmin: 1,
    seatmax: 4,
    users: [],
    autoaccept: true,
    preloadTypeahead: true,
    userstatus: 'host',
    isprivate: false,
    reoccurenable: false, 
    eventid : '',
    seriesid: '',
    eventStatus: ''
  },
  methods: {
    isDraft: function(status) {
      return (status === 'draft');
    },
    publishPrivate: function(key, useremail) {
      $.ajax({
        type: "PATCH",
        url: "/api/v1/matches/" + key + "/publish",
        dataType: "json",
        data: {
          email: useremail, isprivate: true 
        },
        cache: false
      }).done(function(x) {
        window.location.href = '/events/' + x.key;
      }).fail(function(err) {
        showLoginWindow();
      })
    },
    friendlyStatus: function(status) {
      if (status == 'host') {
        return 'host';
      } else if (status == 'approve') {
        return '';
      } else if (status == 'invite') {
        return 'pending..';
      } else if (status == 'interested') {
        return 'maybe..';
      } else if (status == 'decline') {
        return 'declined';
      } else {
        return status;
      }
    },
    avatarClass: function(status) {
      if (status == 'decline') {
        return 'playeravatar declineavatar';
      } else {
        return 'playeravatar';
      }
    },
    playerAndFriendCount: function(o) {
      return Number(R.prop('extraplayers', o)) + 1
    },
    countPlayers: function(matchplayers) {
      return R.compose(
        R.sum,
        R.map(this.playerAndFriendCount),
        R.filter(R.converge(R.or, [R.propEq('status', 'host'), R.propEq('status', 'approve')]))
      )(matchplayers);
    },
    showTooltips: function() {
      $('.vutips').tooltip();
      $('.initials').materialAvatar({shape: 'circle'});
    },
    loadUsers: function(email, matchkey, eventstatus) {

      $.ajax({
        type: "GET",
        url: "/api/v1/matches/" + matchkey + "/users",
        dataType: "json",
        data: {email: email},
        cache: false
      }).done(function(x) {
        vuUsers.users = x;
        vuGames.playercount = vuUsers.users.length;
        vuTimeslots.playercount = vuUsers.users.length;
        vuGames.playercountWithExtras = vuUsers.countPlayers(x);

        if (R.find(R.propEq('activeuser', true), x) == undefined) {
          vuTimeslots.userstatus = '';
          vuGames.userstatus = '';
          vuDetails.userstatus = '';
          vuUsers.userstatus = '';
          this.preloadTypeahead = false;
        } else {
          vuTimeslots.userstatus = R.find(R.propEq('activeuser', true), x).status;
          vuGames.userstatus = R.find(R.propEq('activeuser', true), x).status;
          vuDetails.userstatus = R.find(R.propEq('activeuser', true), x).status;
          vuUsers.userstatus = R.find(R.propEq('activeuser', true), x).status;
          this.preloadTypeahead = false;
        }
        vuUsers.showInvite = ((eventstatus === "draft" || eventstatus === "open") && ((vuTimeslots.userstatus=='host') || this.preloadTypeahead));

        Vue.nextTick(function() { 
          vuUsers.showTooltips();
        });

      }).fail(function(err) {
        this.showInvite = false;
      })
    },
    isStandbyUsers: function(users) {

      // orAppender :: bool -> bool -> [bool, bool]
      const orAppender = function(a, b) {
        return [a || b, a || b];
      }

      // findStandbyUsers :: [o] -> bool
      const findStandbyUsers = R.compose(
        R.head,
        R.mapAccum(orAppender, false),
        R.map(R.equals("standby")),
        R.map(R.prop("status"))
      );

      return findStandbyUsers(users);

    },
    processAjax: function(type, data, errmsg, joinState, matchkey) {
      $.ajax({
        type: type,
        url: "/api/v1/matches/" + matchkey + "/users",
        dataType: "json",
        data: data,
        cache: false
      }).done(function(x) {
        vuUsers.users = x;
        vuGames.playercount = vuUsers.users.length;
        vuTimeslots.playercount = vuUsers.users.length;
        vuGames.playercountWithExtras = vuUsers.countPlayers(x);

        Vue.nextTick(function() { 
          vuUsers.showTooltips();
        });

        $('.bottom-buttongroup').hide();
        vuUsers.showJoin = joinState;

      }).fail(function(err) {
        alert(errmsg);
      })
    },
    userAction: function(fromEmail, type, playerkey, matchkey) {
      if (type == "Remove") {
        vuUsers.processAjax("POST", {host: fromEmail, playerkey: playerkey, type:'standby'}, 'Failed to remove the user. Check that you are logged in, and you are the host.', false, matchkey);
      } else if (type == "Leave") {
        vuUsers.processAjax("DELETE", {user: fromEmail}, 'Failed to leave, check that you are still logged in.', true, matchkey);
      } else if (type == "Unreject") {
        vuUsers.processAjax("DELETE", {user: fromEmail}, 'Failed to leave, check that you are still logged in.', true, matchkey);
      } else if (type == "Accept") {
        vuUsers.processAjax("PATCH", {user: fromEmail, type:'approve', playerkey:playerkey}, 'Failed to accept user. Check that you are logged in, and the game is not full.', false, matchkey);
      } else if (type == "Reject") {
        vuUsers.processAjax("PATCH", {user: fromEmail, type:'reject'}, 'Failed to reject user. Check that you are logged in.', false, matchkey);
      } else if (type == "Decline") {
        vuUsers.processAjax("PATCH", {user: fromEmail, type:'decline'}, 'Failed to decline the invitation. Check that you are logged in.', false, matchkey);
      } else if (type == "Approve") {
        vuUsers.processAjax("PATCH", {host: fromEmail, playerkey: playerkey, type:'approve'}, 'Failed to approve user. Checked that your are logged in, and the game is not full.', false, matchkey);
      } else if (type == "Deny") {
        vuUsers.processAjax("PATCH", {host: fromEmail, playerkey:playerkey, type:'reject'}, 'Failed to deny user. Check that you are logged in.', false, matchkey);
      } else if (type == "Delete") {
        vuUsers.processAjax("DELETE", {host: fromEmail, playerkey: playerkey}, 'Failed to remove the player, check that you are still logged in.', true, matchkey);
      } else if (type == "Unban") {
        vuUsers.processAjax("DELETE", {host: fromEmail, playerkey: playerkey}, 'Failed to remove the player, check that you are still logged in.', true, matchkey);
      } else if (type == "Request Join") {
        vuUsers.processAjax("POST", {email: fromEmail, type:'request'}, 'Failed the request to join. Check that you are logged in, and the game is not full.', false, matchkey);
      } else if (type == "Invite") {
        vuUsers.processAjax("POST", {host: fromEmail, playerkey: playerkey, type:'invite'}, 'Failed the invite the user. Check that you are logged in, and you are the host.', false, matchkey);
      } else if (type == "Maybe") { // TODO Make this work
        vuUsers.processAjax("DELETE", {host: fromEmail, playerkey: playerkey}, 'Failed to remove the player, check that you are still logged in.', true, matchkey);
      }
    },
    incrementFriendCount: function(matchkey, useremail, direction) {
      $.ajax({
        type: "PATCH",
        url: "/api/v1/matches/" + matchkey + "/users",
        dataType: "json",
        data: {
          email: useremail,
          extraplayers: direction
        },
        cache: false
      }).done(function(x) {
        vuUsers.users = x;
        vuGames.playercount = vuUsers.users.length;
        vuTimeslots.playercount = vuUsers.users.length;
        vuGames.playercountWithExtras = vuUsers.countPlayers(x);

        Vue.nextTick(function() { 
          vuUsers.showTooltips();
        });

      }).fail(function(err) {
        alert('Failed to change friend count, please check that you are logged in and there is enough player positions available.');
      });
    }, 
    notifyHost: function(matchkey, useremail) {
      $.ajax({
        type: "POST",
        url: "/api/v1/matches/" + matchkey + "/sendhosttemplate",
        dataType: "json",
        data: {
          email: useremail
        },
        cache: false
      }).done(function(x) {
        alert("A template email with the event link has been sent to your email.");

      }).fail(function(err) {
        alert('Failed to send the invite link, please ensure you are the host and are logged in');
      });
    },
    popupPremiumInvite: function () {
      document.getElementById('id01').style.display='block'
      //alert("Only premium members can send email invites");
      //modalPremiumPopup = true;
    },
    inviteUser: function(matchkey, hostEmail, inviteEmail) {
      const validateEmail = function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      };
      if (validateEmail(inviteEmail)) {
        $.ajax({
          type: "POST",
          url: "/api/v1/matches/" + matchkey + "/users",
          dataType: "json",
          data: {
            host: hostEmail,
            email: inviteEmail,
            type: 'invite'
          },
          cache: false
        }).done(function(x) {
          vuUsers.users = x;
          $('#invite-email').focus();
          $('#invite-email').val('');
          vuUsers.showTooltips();
        }).fail(function(err) {
          alert('Failed to invite user ' + inviteEmail + '. ' + JSON.stringify(err));
        })
      } else {
        alert('Invalid email format, "' + inviteEmail + '". Please check the email and try again');
      }
    },
    inviteUserByKey: function(matchkey, hostEmail, inviteKey) {
      $.ajax({
        type: "POST",
        url: "/api/v1/matches/" + matchkey + "/users",
        dataType: "json",
        data: {
          host: hostEmail,
          userkey: inviteKey,
          type: 'invite'
        },
        cache: false
      }).done(function(x) {
        vuUsers.users = x;
        $('#invite-name').focus();
        $("#invite-name").typeahead('val', '');
        vuUsers.showTooltips();
      }).fail(function(err) {
        alert('Failed to invite user ' + inviteEmail + '. ' + JSON.stringify(err));
      })
    },
    toggleAutoaccept: function(matchkey, useremail, newvalue) {

      // buildAjaxData :: k -> d -> {o}
      const buildAjaxData = function(key, keydata) {
        return R.zipObj(['email', key], [useremail, keydata]);
      };

      // buildAjaxObj :: {o} -> {p}
      const buildAjaxObj = function(data) {
        return R.zipObj(['type', 'url', 'dataType', 'data', 'cache'], ['PATCH', '/api/v1/matches/' + matchkey, 'json', data, false])
      };

      const buildAjaxDataObj = R.compose(buildAjaxObj, buildAjaxData);

      $.ajax(
        buildAjaxDataObj('autoaccept', newvalue)

      ).done(function(x) {
        vuUsers.autoaccept = newvalue; 

      }).fail(function(err) {
        window.location.href = '/login';

      });
    },
    setupTypeAhead: function(matchkey, useremail) {

      const setupUserTypeahead = function(matchkey, useremail, inpName) {
        var users = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          remote: {
            url: "/api/v1/accounts/nearby/email/" + useremail + "?distance=75000&name=%QUERY",
            wildcard: '%QUERY'
          }
        });

        $(inpName).typeahead({
          highlight: true,
          minlength: 2,
        }, {
          source:  users,
          display: function(item) {
            return item.profile.name;
          },
          limit: 5,
          templates: {
            suggestion: function(item) {
              return '<div>' + 
                '<table class="typeahead-table"><tr class="typeahead-row">' +
                  '<td class="typeahead-avatar"><img src="' + item.profile.avataricon + '" onerror="this.src=\'/img/default-avatar-sq.jpg\'" class="' + item.imgclass + '" style="max-height: 45px;"></td>' + 
                  '<td class="typeahead-trophy"><i class="fa fa-trophy"></i> ' + item.profile.playlevel + '</td>' +
                  '<td><span class="' + item.aclass + '"> ' + item.profile.name + '</span></td>' +
                '</tr></table>' +
              '</div>';

            }
          } 
        });

        $(inpName).bind('typeahead:selected', function(obj, datum, name) {
          vuUsers.inviteUserByKey(matchkey, useremail, datum.pageslug); 
        })

      };

      setupUserTypeahead(matchkey, useremail, '#invite-name');

    }
  }
});

var vuUserButtons = new Vue({
  el:   '#userbuttons',
  data: {
    noUser: false,
    key: '',
    useremail: '',
    buttons: []
  },
  methods: {
    buttonClass: function(style) {
      return "btn btn-" + style + " vutips";
    },
    faClass: function(style) {
      return "fa fa-" + style + " fa-fw";
    },
    userButtonPress:  function(actionType) {
      if (this.noUser) {
        if (confirm("You need to create an account to join the event. Do you want to create an account now?"))
          window.location.href = '/signup?matchkey=' + this.key;

      } else {

        switch (actionType) {
          case "interested":
            $.ajax({
              type: "POST",
              url: "/api/v1/matches/" + this.key + "/users",
              dataType: "json",
              data: {
                email: this.useremail,
                type: 'interested'
              },
              cache: false
            }).done(function(x) {
              vuUsers.users = x;
              vuUsers.showJoin = false;
              vuGames.showJoin = false;
              vuPublicForum.showJoin = false;

              Vue.nextTick(function() { 
                vuUsers.showTooltips();
              });

              vuUserButtons.buttons = [
                {text:'Join Event', faicon:'thumbs-up', style:'success', id:'join'},
                {text:'Leave Event', faicon: 'sign-out', style: 'warning', id:'decline'}
              ];

            }).fail(function(err) {
              alert('Failed to join the game. ' + JSON.stringify(err));
            })
            break;

          case "join":
            $.ajax({
              type: "POST",
              url: "/api/v1/matches/" + this.key + "/users",
              dataType: "json",
              data: {
                email: this.useremail,
                type: 'request'
              },
              cache: false
            }).done(function(x) {
              vuUsers.users = x;
              vuUsers.showJoin = false;
              vuGames.showJoin = false;
              vuPublicForum.showJoin = false;

              Vue.nextTick(function() { 
                vuUsers.showTooltips();
              });

              vuUserButtons.buttons = [
                {text:'Leave Event', faicon: 'sign-out', style: 'warning', id:'decline'}
              ];

            }).fail(function(err) {
              alert('Failed to join the game. ' + JSON.stringify(err));
            });
            break;

          case "accept":
            $.ajax({
              type: "PATCH",
              url: "/api/v1/matches/" + this.key + "/users",
              dataType: "json",
              data: {
                user: this.useremail,
                type: 'approve'
              },
              cache: false
            }).done(function(x) {
              vuUsers.users = x;
              vuUsers.showJoin = false;
              Vue.nextTick(function() { 
                vuUsers.showTooltips();
              });

              vuUserButtons.buttons = [
                {text:'Leave Event', faicon: 'sign-out', style: 'warning', id:'decline'}
              ];

            }).fail(function(err) {
              alert('Failed to join the game. ' + JSON.stringify(err));

            })
            break;

          case "decline":
            $.ajax({
              type: "PATCH",
              url: "/api/v1/matches/" + this.key + "/users",
              dataType: "json",
              data: {
                user: this.useremail,
                type: 'decline'
              },
              cache: false
            }).done(function(x) {
              vuUsers.users = x;
              vuUsers.showJoin = false;
              Vue.nextTick(function() { 
                vuUsers.showTooltips();
              });

              vuUserButtons.buttons = [
                {text:'Interested', faicon:'bell', style:'primary', id:'interested'},
                {text:'Join Event', faicon:'thumbs-up', style:'success', id:'accept'},
                {text:'Leave Group', faicon: 'times', style: 'danger', id:'leave'}
              ];

            }).fail(function(err) {
              alert('Failed to decline joining the game. ' + JSON.stringify(err));

            })
            break;

          case "leave":
            if (window.confirm("Are you sure you want to leave this group? You won't get notices for future occurences of this event if you leave.")) {
              event.preventDefault();
              $.ajax({
                type: "DELETE",
                url: "/api/v1/matches/" + this.key + "/users",
                dataType: "json",
                data: {
                  user: this.useremail 
                },
                cache: false
              }).done(function(x) {
                window.location.href = '/events';

              }).fail(function(err) {
                alert('Failed to leave match. ' + JSON.stringify(err));
              })
            };
            break;

          case "disband":
            if (window.confirm("Are you sure you want to disband the group?")) {
              event.preventDefault();
              $.ajax({
                type: "DELETE",
                url: "/api/v1/matches/" + this.key,
                dataType: "json",
                data: {
                  email: this.useremail 
                },
                cache: false
              }).done(function(x) {
                window.location.href = '/events';
              }).fail(function(err) {
                alert('Failed to disband match. ' + JSON.stringify(err));
              })
            }
            break;

          default: 
            alert('unhandled ' + actionType);

        }
      };

    }
  }
});


var vuShowDate = new Vue({
  el: '#showdate',
  data: {
    startmonth: 'Nov',
    startday:   '02',
    startdow:   'Wednesday',
    startyear:    '2018',
    starttime: '03:00 am',
    endtime: '03:00 am',
    style: 'background-color: #42acb4;',
    countdowndays: '03',
    countdownhours: '03',
    countdownminutes: '03',
    countdownseconds: '03',
    visible: false,
    startdatetime:  new Date(),
    enddatetime:    new Date(),
  },
  methods: {
    loadDates: function(isvisible, startdatetime, enddatetime) {

      this.visible = isvisible; 
      this.startdatetime = startdatetime;
      this.enddatetime = enddatetime;
      vuTimeslots.votevisible = !isvisible; 

      if (isvisible) {
        this.startday = moment(startdatetime).format('DD');
        this.startmonth = moment(startdatetime).format('MMM');
        this.startyear = moment(startdatetime).format('YYYY');

        this.starttime = moment(startdatetime).format('hh:mm a');
        this.endtime = moment(enddatetime).format('hh:mm a');

        var indays = Math.floor(moment(startdatetime).diff(moment(), 'days', true));

        if (indays < 0) {

          this.countdowndays = 0;
          this.countdownhours = 0;
          this.countdownminutes = 0;
          this.countdownseconds = 0;

          if (Math.floor(moment(enddatetime).diff(moment(), 'seconds', true)) < 0) { 
            this.style = 'background-color: #777777;'; 
            this.startdow = moment(startdatetime).format('dddd');
          } else {
            this.style = 'background-color: #5cb85c;';
            this.startdow = 'In progress';
          }

        } else {

          this.countdowndays = indays;
          this.countdownhours = Math.floor(moment(startdatetime).diff(moment(), 'hours', true)) % 24;
          this.countdownminutes = Math.floor(moment(startdatetime).diff(moment(), 'minutes', true)) % 60;
          this.countdownseconds = Math.floor(moment(startdatetime).diff(moment(), 'seconds', true)) % 60;

          if ((moment().dayOfYear()) !== (moment(startdatetime).dayOfYear())) {
            this.startdow = moment(startdatetime).format('dddd');
            this.style = 'background-color: #42acb4;' 

          } else {
            this.startdow = "Today";
            this.style = 'background-color: #f0ad4e;' 

          }

          Vue.nextTick(function() { 
            setTimeout(function() {
              vuShowDate.loadDates(true, startdatetime, enddatetime);
            }, 1000);
          });

        };
      };

    }
  }
});

var vuGames = new Vue({
  el: '#gamespanel',
  data: {
    showJoin: false,
    showEdit: false,
    isUser: false,
    games: [],
    playercount: 5,
    playercountWithExtras: 5,
    playerToInvite: {},
    userstatus: '',
    useremail: '',
    userpageslug: '',
    username: '',
    useravatar: '',
    matchkey: '',
    serviceurl: ''
  },
  methods: {
    showTooltips: function() {
      $('.vutips').tooltip();
    },
    hideTooltips: function() {
      $('.vutips').tooltip('hide');
    },
    clickBggLink: function(id) {
      window.open("/games?gameid=" + id, "_blank");
    },
    buildLink: function(id) {
      return "/games?gameid=" + id;
    },
    whenOwnsGame: R.curry(function(useremail, game) {
      return R.merge(game, {'showremove': (game.email == useremail)}); 
    }),
    whenVotedForGame: R.curry(function(useremail, game) {
      return R.merge(game, R.find(R.propEq('email', useremail), game.vote) 
        ? {ischecked: true, ischeckedtrack: true}
        : {ischecked: false, ischeckedtrack: false}
      ); 
    }),
    alertify: function(x) {
      alert(JSON.stringify(x));
      return x;
    },
    maybeAvatar: function(vote) {

      // propAvatar :: o -> s
      const propAvatar = R.prop('avataricon');

      return R.has('avataricon', vote) 
        ? (propAvatar(vote).split(":")[0] == "https")
          ? propAvatar(vote)
          : (R.head(propAvatar(vote)) == "/")
            ? propAvatar(vote)
            : "/" + propAvatar(vote)
        : '/img/default-avatar-sq.jpg'

    },
    sortGames: function(game) { 
      return R.compose(
        R.reverse,
        R.map(vuGames.whenVotedForGame(vuGames.useremail)),
        R.map(vuGames.whenOwnsGame(vuGames.useremail)),
        R.sortBy(function VoteCount (x) {return x.vote.length}),
        R.reverse,
        R.sortBy(function GameName (x) {return x.name})
      )(game);
    },
    voteStyle: function(game, players) {
      var perc = 100 * game.join.length / game.maxplayers;
      return "width: " + perc + "%;"
    },
    getProgressClass: function(game) {

      if (game.minplayers > game.join.length) {
        return "progress-bar progress-bar-warning";
      } else {
        try {
          return (R.propEq('status', 'Best', R.find(R.propEq('playercount', game.join.length.toString()), game.playercounts)))
            ? "progress-bar progress-bar-info"
            : "progress-bar progress-bar-success"
        } catch(err) {
          return "progress-bar progress-bar-info";
        }

      }
    },
    getChairCount: function(game) {
      return R.min((vuGames.readmaxexpansiondata(vuGames.games, game.expansions, 'maxplayers', game.pageslug) - game.join.length), 8);
    },
    getChairImage: function(game) {
      return (R.modulo(parseInt(Math.random() * (10)), 2) == 0)
        ? '/img/femuser.png'
        : '/img/maleuser.png';
    },
    isGameFull: function(game) {
      return (game.join.length >= game.maxplayers);
    },
    styleIdealPlayerCountClass: function(playersize, gamecount) {
      if (playersize == gamecount) {
        return "vutips label label-primary"
      } else {
        return "vutips label label-info"
      }
    },
    findexpansionuser: R.curry(function(gameslug, expansionid, x) {
      return R.compose(
        R.prop('userpageslug'),
        R.find(R.propEq('id', expansionid)),
        R.prop('hasexpansions'),
        R.find(R.propEq('pageslug', gameslug))
      )(x)
    }),
    expansionListClass: function(userpageslug, gameslug, expansionid, isuse) {
      try {
        if (userpageslug == "") {
          return "disabled";

        } else {
          return ((vuGames.findexpansionuser(gameslug, expansionid, vuGames.games) !== userpageslug) && isuse)
            ? "disabled"
            : "showpointer";

        }
      } catch(err) {
        return "disabled";

      }
    },
    whenUseExpansion: R.curry(function(a, gameexpansion) {
      return R.compose(
        R.merge(gameexpansion),
        R.objOf('use'),
        R.not,
        R.isNil,
        R.find(R.propEq('id', gameexpansion.id))
      )(a)
    }),
    lookupGameValues: function (prop, gameslug, games) {
      return R.compose(
        R.prop(prop),
        R.find(R.propEq('pageslug', gameslug))
      )(games)
    },
    ifhasexpansions: function (gameslug, games) { 
      return R.compose(
        R.has('hasexpansions'),
        R.find(R.propEq('pageslug', gameslug))
      )(games)
    },
    executeIfDefined: R.curry(function(fx, a, b) {
      return (R.isNil(a)) 
        ? b
        : (R.isNil(b))
          ? undefined
          : fx(a, b);
    }),
    readminexpansiondata: function (games, expansions, prop, gameslug) {
      return vuGames.ifhasexpansions(gameslug, games)
        ? R.reduce(vuGames.executeIfDefined(R.min), vuGames.lookupGameValues(prop, gameslug, games), vuGames.lookupGameValues('hasexpansions', gameslug, games).map(R.prop(prop)))
        : vuGames.lookupGameValues(prop, gameslug, games);
    },
    readmaxexpansiondata: function (games, expansions, prop, gameslug) {
      return vuGames.ifhasexpansions(gameslug, games)
        ? R.reduce(vuGames.executeIfDefined(R.max), vuGames.lookupGameValues(prop, gameslug, games), vuGames.lookupGameValues('hasexpansions', gameslug, games).map(R.prop(prop)))
        : vuGames.lookupGameValues(prop, gameslug, games);
    },
    lookupExpansionGames: R.curry(function(matchkey, games, igame, x) {
      $.ajax({
        type: "GET",
        url: "/api/v1/matches/" + matchkey + "/games/" + igame.pageslug + "/expansions/",
        dataType: "json",
        data: {},
        cache: false
      }).done(function(expansions) {
        vuGames.$set(igame, 'expansions', x.expansions.map(vuGames.whenUseExpansion(expansions))); 
        igame.hasexpansions = expansions;
        Vue.nextTick(function() { 
          vuUsers.showTooltips();
        });

      });
    }),
    lookupExpansion: R.curry(function(matchkey, serviceurl, games, igame) {
      $.ajax({
        type: "GET",
        url:  serviceurl + "/" + igame.id + "/expansions",
        dataType: "json",
        data: {},
        cache: false
      }).done(function(x) {
        vuGames.lookupExpansionGames(matchkey, games, igame, x);
      })

    }),
    sortGamesApply: function(matchkey, serviceurl, games) {
      vuGames.games = vuGames.sortGames(games);
      Vue.nextTick(function() { 
        vuGames.games.map(vuGames.lookupExpansion(matchkey, serviceurl, vuGames.games))
        vuGames.showTooltips();
      });
    },
    loadGames: function(matchkey, serviceurl) {
      $.ajax({
        type: "GET",
        url: "/api/v1/matches/" + matchkey + "/games",
        dataType: "json",
        data: {},
        cache: false
      }).done(function(x) {
        vuGames.sortGamesApply(matchkey, serviceurl, x);
      })
    },
    removeGame: function(matchkey, serviceurl, useremail, slug) {
      $.ajax({
        type: "DELETE",
        url: "/api/v1/matches/" + matchkey + "/games/" + slug,
        dataType: "json",
        data: {
          email: useremail 
        },
        cache: false
      }).done(function(x) {
        vuGames.sortGamesApply(matchkey, serviceurl, x);

      }).fail(function(err) {
        alert('Failed to remove game. Please ensure you are logged in.');
      })

    },
    addGame: function(matchkey, serviceurl, useremail, gameToAdd, status) {

      if (vuGames.isUser) {
        if (vuGames.showJoin) {
          showJoinupWindow();
        } else {
          if (gameToAdd == {} || gameToAdd == undefined) {
            alert('Failed to add game. Please ensure a valid game is selected, and your join requested for the event has been accepted.'); 
            $('#addgameid').focus();

          } else {
            $.ajax({
              type: "POST",
              url: "/api/v1/matches/" + matchkey + "/games",
              dataType: "json",
              data: {
                email: useremail,
                gameid: gameToAdd.id,
                name: gameToAdd.name,
                thumbnail: gameToAdd.thumbnail,
                status: status 
              },
              cache: false
            }).done(function(x) {
              $('#addgameid').val('');
              $('#addgameid').focus();

              vuGames.sortGamesApply(matchkey, serviceurl, x);
              Vue.nextTick(function() { 
                $('#addgameid').val('');
              });

            }).fail(function(err) {
              alert('Failed to add game. Please ensure a valid game is selected, and your join requested for the event has been accepted.'); 
            })
          }
        }
      } else {
        showLoginWindow();
      }
    },
    bringGame: function(matchkey, serviceurl, useremail, gameslug) {
      if (vuGames.isUser) {
        if (vuGames.showJoin) {
          alert('You can not add a game until you have signed up and been approved for the event.');
        } else {
          $.ajax({
            type: "PATCH",
            url: "/api/v1/matches/" + matchkey + "/games/" + gameslug,
            dataType: "json",
            data: {
              email: useremail,
              status: 'select' 
            },
            cache: false
          }).done(function(x) {
            vuGames.sortGamesApply(matchkey, serviceurl, x);

            Vue.nextTick(function() { 
              $('#addgameid').val('');
            });

          }).fail(function(err) {
            alert('Failed to add game. Please ensure a valid game is selected, and your join requested for the event has been accepted.'); 
          })

        }
      } else {
        showLoginWindow();
      }
    },
    voteForGame: function(matchkey, useremail, username, useravatar, game) {
      $.ajax({
        type: (!game.ischeckedtrack) 
          ? 'POST'
          : 'DELETE',
        url: "/api/v1/matches/" + matchkey + "/games/" + game.id + "/votes",
        dataType: "json",
        data: {
          email: useremail,
          gamename: game.name 
        },
        cache: false
      }).done(function(x) {

        const incrementVote = R.curry(function(gameid, game) {
          if (gameid === game.id) {
            game.vote.push({email:useremail, username:username, avataricon:useravatar, direction: 1}); 
            game.ischecked = true;
            game.ischeckedtrack = true;
          }
          return game;
        });

        const deincrementVote = R.curry(function(gameid, game) {
          if (gameid === game.id) {
            game.vote = R.init(game.vote); 
            game.ischecked = false;
            game.ischeckedtrack = false;
          }
          return game;
        });

        vuGames.games = (!game.ischeckedtrack) 
          ? vuGames.games = R.map(incrementVote(game.id), vuGames.games)
          : vuGames.games = R.map(deincrementVote(game.id), vuGames.games);

        setTimeout(function() {
          vuGames.hideTooltips();
          vuGames.showTooltips()
        }, 1000);

      }).fail(function(err) {
        alert('Failed to vote for game. Please ensure you are logged in and joined the event.');
      })
    },
    playerNotJoinedInGame: function(userkey, games) {

      // checkGames :: [a] -> bool
      const checkGames = R.compose(
        R.reduce(R.and, true),
        R.map(vuGames.isPlayerInGame(userkey))
      );

      return checkGames(games);

    },
    isPlayerInGame: R.curry(function(userkey, game) {
      return R.find(R.propEq('userkey', userkey), game.join) === undefined; 
    }),
    joinGame: function(matchkey, useremail, userpageslug, username, useravatar, isjoining, game) {

      $.ajax({
        type: (isjoining) 
          ? 'POST'
          : 'DELETE',
        url: "/api/v1/matches/" + matchkey + "/games/" + game.gamekey + "/joins",
        dataType: "json",
        data: {
          email: useremail,
          gamename: game.name
        },
        cache: false
      }).done(function(x) {

        // incrementJoin :: s -> o -> o
        const incrementJoin = R.curry(function(gamekey, game) {
          if (gamekey === game.gamekey) {
            game.join.push({email:useremail, userkey:userpageslug, username:username, avataricon:useravatar, direction: 1}); 
          }
          return game;
        });

        // deincrementJoin :: s -> o -> o
        const deincrementJoin = R.curry(function(gamekey, game) {
          if (gamekey === game.gamekey) {
            game.join = R.init(game.join); 
          }
          return game;
        });

        vuGames.hideTooltips();
        vuGames.games = isjoining
          ? R.map(incrementJoin(game.gamekey), vuGames.games)
          : R.map(deincrementJoin(game.gamekey), vuGames.games);

        setTimeout(function() {
          vuGames.showTooltips()
        }, 300);

      }).fail(function(err) {
        if (err.status == 405) {
          alert('The game is full. Please join another game.');
        } else {
          alert('Failed to join the game. Please ensure you are logged, and not joined to another game.');
        };
      })
    },
    getVoteButtonClassArrow: function(game) {
      return (game.ischecked)
        ? 'vote-button-uservoted'
        : 'vote-button';
    },
    getVoteButtonClassFaIcon: function(game) {
      return (game.ischecked)
        ? 'fa fa-thumbs-o-up'
        : '';
    },
    voteCount: function(game) {
      return (game.vote.length == 0)
        ? '-'
        : game.vote.length;
    },
    getVoteTitle: function(game) {

      // getPlayerNames :: o -> s
      const getVotingPlayerNames = R.compose(
        R.join(', '),
        R.map(R.prop('username')),
        R.prop('vote')
      );

      return (game.vote.length == 0)
        ? 'Vote for ' + game.name
        : 'Voted for by ' + getVotingPlayerNames(game);
    },
    whenGameRunFunctionifyExpansions: R.curry(function(bggid, expansionid, expansionFx, game) {
      if (game.id == bggid) {
        game.expansions = game.expansions.map(expansionFx(expansionid));
      }
      return game;
    }),
    toggleUseExpansion: function(serviceurl, matchkey, useremail, userpageslug, basegameslug, expansionid, isused) {

      if (isused) {
        vuGames.unUseExpansion(serviceurl, matchkey, useremail, userpageslug, basegameslug, expansionid);
      } else {
        $.ajax({
          type: "GET",
          url: serviceurl + "/" + expansionid,
          dataType: "json",
          data: {},
          cache: false
        }).done(function(x) {
          $.ajax({
            type: "POST",
            url: "/api/v1/matches/" + matchkey + "/games/" + basegameslug + "/expansions/",
            dataType: "json",
            data: {
              email: useremail,
              userslug: userpageslug,
              expansiondata: R.pick(["id", "name", "thumbnail", "minplaytime", "maxplaytime", "minplayers", "maxplayers", "weight", "avgrating"], x)
            },
            cache: false
          }).done(function(expansions) {
            vuGames.games.map(vuGames.lookupExpansion(matchkey, serviceurl, vuGames.games))
          })

        }).catch(function(err) {
          alert("You must be logged in and joined to an event to add an expansion");
        });
      }

    },
    unUseExpansion: function(serviceurl, matchkey, useremail, userpageslug, basegameslug, expansionid) {
      if (vuGames.findexpansionuser(basegameslug, expansionid, vuGames.games) == userpageslug) {
        $.ajax({
          type: "DELETE",
          url: "/api/v1/matches/" + matchkey + "/games/" + basegameslug + "/expansions/" + expansionid,
          dataType: "json",
          data: {
            email: useremail 
          },
          cache: false
        }).done(function(expansions) {
          vuGames.games.map(vuGames.lookupExpansion(matchkey, serviceurl, vuGames.games))

        })
      } else {
        alert("You can not remove this expansion, because another player is bringing it");
      }
    },
    setupTypeAhead: function(matchkey, useremail, serviceurl) {

      const setupGameTypeahead = function(inpName) {

        var boardGames = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          remote: {
            url: serviceurl + '/?name=%QUERY',
            wildcard: '%QUERY'
          }
        });

        $(inpName).typeahead({
          hint: true,
          highlight: true,
          minlength: 1,
        }, {
          source:  boardGames,
          display: function(item) {
            return item.name;

          },
          limit: 15,
          templates: {
            suggestion: function(item) {
              var playerCountHtml = function (playercountobj) {
                if (playercountobj.status==='Best') {
                  return '<span class="' + vuGames.styleIdealPlayerCountClass(vuGames.playercountWithExtras, playercountobj.playercount) + '">' + playercountobj.playercount + '</span>';
                } else {
                  return '';
                }
              };

              return '<div>' + 
                '<table class="typeahead-table"><tr class="typeahead-row">' +
                '<td class="typeahead-gamebox"><img src="' + item.thumbnail + '" style="max-height: 48px;"></td>' +
                '<td class="addgametext"><span> ' + item.name + ' <small>(' + item.yearpublished + ')</small></span></td>' +
                '<td class="typeahead-players hide-small-screen"><i class = "fa fa-users"></i> <span>' + item.minplayers + '-' + item.maxplayers + '</span> ' +
                R.join('', item.playercounts.map(playerCountHtml)) + '</td>' + 
                '<td class="typeahead-time hide-small-screen"><i class = "fa fa-clock-o"></i> <span>' + item.minplaytime + '-' + item.maxplaytime + ' mins</span> <span class="' + vuGames.classifyWeight(item.weight) + '" title="' + vuGames.titleWeight(item.weight) + '">' + vuGames.labelfyWeight(item.weight) + '</span></td>' +
                '</tr></table>' +
              '</div>';

            }
          } 
        });

        $(inpName).bind('typeahead:selected', function(obj, datum, name) {
         (inpName == '#suggestgameid')
           ? vuGames.addGame(matchkey, serviceurl, useremail, datum, 'playersuggest')
           : vuGames.addGame(matchkey, serviceurl, useremail, datum, 'select');
          $(inpName).typeahead('val', '');
        })

      };

      setupGameTypeahead('#addgameid');
      setupGameTypeahead('#suggestgameid');

    },
    labelfyWeight: function(weight) {
      return (weight <= 1.8) 
        ? 'Light'
        : (weight <= 2.6)
          ? 'Med Light'
          : (weight <= 3.4)
            ? 'Medium'
            : (weight <= 4.2)
              ? 'Med Heavy'
              : 'Heavy'
    },
    classifyWeight: function(weight) {
      return (weight <= 1.8) 
        ? 'vutips label label-success'
        : (weight <= 2.6)
          ? 'vutips label label-success'
          : (weight <= 3.4)
            ? 'vutips label label-info'
            : (weight <= 4.2)
              ? 'vutips label label-warning'
              : 'vutips label label-warning'
    },
    titleWeight: function(weight) {
      return 'BGG Avg Weight: ' + weight
    }

  }
});




// alcoholText :: l -> s
const alcoholText = function(alcohol) {
  return (alcohol == 1) 
    ? 'No Drinking' 
    : 'Drinking Friendly'
};

// SmokingText :: l -> s
const smokingText = function(smoking) {
  return (smoking == 1) 
    ? 'No Smoking' 
    : 'Smoking Friendly'
};


var loadPlayAreaTypeahead = function() {
  $('#playarea').typeahead(
    {
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      source: function(query, sync, process) 
      {
        return $.ajax({
          type: "GET",
          url: "/api/v1/locations",
          dataType: "json",
          data: {search: query},
          success: function(data) {
            return process(R.map(R.path(['place','address']), data));
          }
        });
      }
    }
  )
};

var vuDetails = new Vue({
  el: '#venuedetails',
  data: {
    showDetails: true,
    showEdit: false,
    showSave: false,
    seatmin: 1,
    seatmax: 50,
    alcohol: false,
    smoking: false,
    alcoholtext: alcoholText(false),
    smokingtext: smokingText(false),
    price: 0,
    currency: 'AUD',
    entryfee: '$0.00',
    userstatus: '',
    isOnline: false
  },
  methods: {
    updateFee: function(price, currency) {
      return (currency == "AUD")
        ? "$ " + Number(price).toFixed(2) 
        : (currency == "CAD")
          ? "$ " + Number(price).toFixed(2) 
          : (currency == "USD")
            ? "$ " + Number(price).toFixed(2) 
            : (currency == "NZD")
              ? "$ " + Number(price).toFixed(2) 
              : (currency == "GBP")
                ? "£ " + Number(price) 
                : (currency == "EUR")
                  ? Number(price).toFixed(2) + " €"
                  : "0.00" 
    },
    editVisible: function() {
      this.showEdit = false;
      this.showSave = true;
      Vue.nextTick(function() { 
        if (!vuDetails.typeaheadLoaded) {
          vuDetails.typeaheadLoaded = true;
        };
      });
    },
    cancelSave: function() {
      this.showEdit = true;
      this.showSave = false;
    },
    saveDetails: function(key, useremail) {

      // checkmaxplayers
      const checkmaxplayers = R.gte(R.__, 2);

      if (checkmaxplayers(Number(vuDetails.seatmax))) {
        if (R.lte(Number(vuDetails.seatmin), Number(vuDetails.seatmax))) {

          const defaultZero = function(x) {
            return (x === undefined)
              ? 0
              : x;
          };

          $.ajax(buildAjaxObj(key, {
            email: useremail,
            seatmin: vuDetails.seatmin,
            seatmax: vuDetails.seatmax,
            price: vuDetails.price,
            currency: vuDetails.currency,
            smoking: $('#smoking').val(),
            alcohol: $('#alcohol').val()
          })).done(function(x) {
            vuDetails.alcohol = $('#alcohol').val();
            vuDetails.smoking = $('#smoking').val();
            vuDetails.entryfee = vuDetails.updateFee(vuDetails.price, vuDetails.currency);
            vuDetails.alcoholtext = ($('#alcohol').val() == '1') ? 'No Drinking' : 'Drinking Friendly';
            vuDetails.smokingtext = ($('#smoking').val() == '1') ? 'No Smoking' : 'Smoking Friendly';
            vuDetails.showEdit = true;
            vuDetails.showSave = false;

          }).fail(function(err) {
            if (err.status == 400) {
              alert('Failed to save. Please check the date and times are valid, the maximum number of players is greater than the minimum number, and greater than the number of players currently accepted to play');
            } else { 
              window.location.href = '/login';
            }
          });

        } else {
          alert("The mininum number of players should be equal to or less than the maximum");
          $('#seatmin').focus()
        };

      } else {
        alert("The maximum number of players should be at least two");
        $('#seatmax').focus()
      }
    }
  }
});


var vuTime = new Vue({
  el: '#timepanel',
  data: {
    showDetails: true,
    showEdit: false,
    showSave: false,
    playdateedit: moment().format('DD MMM YYYY'),
    playdateformat: moment().format('dddd, Do MMM YYYY'),
    timestartedit: moment().format('HH:mm'),
    timestartformat: moment().format('h:mm a'),
    timeendedit: moment().format('HH:mm'),
    timeendformat: moment().format('h:mm a'),
    dateendedit: moment().format('DD MMM YYYY'),
    dateendformat: moment().format('dddd, Do MMM YYYY'),
    isstartdatevalid: moment().isValid(),
    isenddatevalid: moment().isValid(),
    reoccurevery: -1,
    reoccurinterval: 'never',
    userstatus: '',
    seriesid: ''
  },
  methods: {
    buildSeriesUrl: function(series) {
      return "https://www.rollforgroup.com/events/series/" + series;
    },
    editVisible: function() {
      this.showEdit = false;
      this.showSave = true;
      Vue.nextTick(function() { 
        $('.clockpicker').clockpicker({
          donetext: 'OK'
        });

        // date range picker
        var picker = new Lightpick({ 
          field: document.getElementById('playdateedit'),
          secondField: document.getElementById('dateendedit'),
          minDate: moment(),
          selectForward: true,
          maxDays: 2,
          singleDate: false,
          onSelect: function(start, end){
            vuTime.playdateedit = start.format('DD MMM YYYY');
            if (!R.isNil(end)) {
              vuTime.dateendedit    = end.format('DD MMM YYYY');
            }
          }
        }); 

        $('#playdateedit').val(vuTime.playdateedit);
        $('#dateendedit').val(vuTime.dateendedit);

        if (!vuTime.typeaheadLoaded) {
          vuTime.typeaheadLoaded = true;
        }
      });
    },
    cancelSave: function() {
      this.showEdit = true;
      this.showSave = false;
    },
    saveDetails: function(key, useremail) {

      // defaultZero :: n -> bool
      const defaultZero = function(x) {
        return (x === undefined)
          ? 0
          : x;
      };

      $.ajax(buildAjaxObj(key, {
        email: useremail,
        reoccurevery: defaultZero($('#reoccurevery').val()),
        reoccurinterval: $('#reoccurinterval').val(),
        date: momentify([vuTime.playdateedit, $('#timestartedit').val()]).format(), 
        timeend: momentify([vuTime.dateendedit, $('#timeendedit').val()]).format(), 
      })).done(function(x) {
        vuTime.showEdit = true;
        vuTime.showSave = false;

        vuTime.playdateedit = moment($('#playdateedit').val()).format('DD MMM YYYY');
        vuTime.playdateformat = moment($('#playdateedit').val()).format('dddd, Do MMM YYYY');
        vuTime.timestartedit = momentify([$('#playdateedit').val(), $('#timestartedit').val()]).format('HH:mm');
        vuTime.timestartformat = momentify([$('#playdateedit').val(), $('#timestartedit').val()]).format('h:mm a');
        vuTime.timeendedit = momentify([$('#dateendedit').val(), $('#timeendedit').val()]).format('HH:mm');
        vuTime.timeendformat = momentify([$('#dateendedit').val(), $('#timeendedit').val()]).format('h:mm a');
        vuTime.dateendedit = moment($('#dateendedit').val()).format('DD MMM YYYY');
        vuTime.dateendformat = momentify([$('#dateendedit').val(), $('#timeendedit').val()]).format('dddd, Do MMM YYYY');
        vuTime.reoccurevery = $('#reoccurevery').val();
        vuTime.reoccurinterval = $('#reoccurinterval').val();

      }).fail(function(err) {
        if (err.status == 400) {
          alert('Failed to save. Please check the date and times are valid, the maximum number of players is greater than the minimum number, and greater than the number of players currently accepted to play');
        } else { 
          window.location.href = '/login';
        }
      });


    },
    setReoccurEvery: function(a) {
      if (this.reoccurinterval != 'never') {
        if (this.reoccurevery == -1) {
          this.reoccurevery = 1;
        }
      };
    },
    showReoccur:  function(recurObj){
      return (recurObj != 'never'); 
    },
    showReoccurText: function(every, interval) {
      return (interval === 'never')
        ? 'Never'
        : (interval === 'week')
          ? (every === '1') 
            ? 'Weekly'
            : 'Every ' + every + ' weeks'
          : (every === '1')
            ? 'Monthly'
            : 'Every ' + every + ' months'
    },
    showReoccurClass: function(interval) {
      return 'label label-success'
    },
    reoccurEveryLabelVisible: function(interval) {
      return (interval != 'never')
    },
    reoccurEveryLabel: function(interval) {
      return (interval === 'week')
        ? 'Every ( x ) week(s)'
        : 'On the ( x ) week of the month'
    },
    isValidDate: function(d) {
      return d instanceof Date && !isNan(d);
    }
  }
});

var vuLocation = new Vue({
  el: '#mappanel',
  data: {
    locationName: '',
    encodeLocation: '',
    showEdit: false,
    showSave: false,
    isOnline: false,
    key: '',
    useremail: ''
  },
  methods: {
    saveLocation: function(key, useremail, locname) {

      const notNil = R.compose(R.not, R.isNil);
      const notEmpty = R.compose(R.not, R.isEmpty);

      // buildAjaxDataObj :: (k -> d) -> {p}
      const buildAjaxDataObj = R.compose(
        buildAjaxObj(key),
        buildAjaxData(useremail)
      );

      // getLocProp :: (p -> s) -> ([{d}] => l) 
      const getLocProp = function(propname, area) {
        return R.compose(R.prop(propname), chosenCity(area));
      };

      // chosenAddress :: (s) -> ({d} => Bool)
      const chosenAddress = function(area) {
        return R.compose(R.propEq('address', area), R.prop('place'));
      }

      // chosenCity :: (s) -> ([{d}] => {o})
      const chosenCity = function(area) {
        return R.compose(R.path(['place', 'coords']), R.find(chosenAddress(area)));
      };


      // cityFound :: s -> ([{d}] => bool)
      const cityFound = function(area) {
        return R.compose(notNil, chosenCity(area));
      };

      var ajaxLocation = $.ajax({
        type: "GET",
        url: "/api/v1/locations",
        dataType: "json",
        data: {search: $('#playarea').val()}
      });

      ajaxLocation.done(function(data) {

        if (($('#playarea').val().length > 0) && notNil(data) && (cityFound($('#playarea').val())(data))) {
          $.ajax(buildAjaxObj(key, {
            email: useremail,
            playarea: $('#playarea').val(),
            playarealat: getLocProp('lat', $('#playarea').val())(data),
            playarealng: getLocProp('lng', $('#playarea').val())(data)

          })).done(function(x) {
            vuLocation.setLocation($('#playarea').val());
            vuLocation.showEdit = true;
            vuLocation.showSave = false;
            $('#playareadiv').hide();

          }).fail(function(err) {
            window.location.href = '/login';

          });

        } else {
          alert('The location could not be geo-coded, please check the text matches the dropdown exactly');
          $('#playarea').focus();
        }
      });

      ajaxLocation.fail(function(data) {
        alert('The location could not be geolocated, please ensure the location is valid');
        $('#playarea').focus();
      });

    },
    setLocation: function(locname) {
      vuLocation.locationName = R.replace(/&amp;/, '&', locname);
      vuLocation.encodeLocation = "https://www.google.com/maps/embed/v1/place?q=" +
        R.replace(/&/g, '%26', R.replace(/#/g, '%23', encodeURI(locname))) +
        "&key=AIzaSyDZGpB9hMYMo9pAg6mydAR0PXYcUh-77QQ";

    },
    editLocation: function() {
      this.showEdit = false;
      this.showSave = true;
    },
    cancelLocation: function() {
      this.showEdit = true;
      this.showSave = false;
      $('#playareadiv').hide();
    }
  }
});

// publicforum
var vuPublicForum = new Vue({
  el: '#publicforum',
  data: {
    publicForums: [],
    eventStatus: '',
    useremail: '',
    showJoin: false
  },
  methods: {
    calcFromNow: function(threadObj) {
      const localTime = function (gmttime) {
        return moment(gmttime).fromNow();
      };
      return R.compose(
        R.merge(threadObj), 
        R.objOf('fromNow'), 
        localTime, 
        R.path(['thread', 'postdate'])
      )(threadObj);

    },
    isDraft: function(status) {
      return status === 'draft';
    },
    publicDiscussion: function(matchkey, discussion) {
      if (this.useremail != '') {

        $('#btn-chat').prop('disabled', true);
        if ($('#btn-input').val() == "") {
          alert("Please type something to post");
          $('#btn-input').focus();

        } else {
          $.ajax({
            type: "POST",
            url: "/api/v1/matches/" + matchkey + "/publicforums",
            dataType: "json",
            data: {
              user: this.useremail,
              discussion: discussion
            },
            cache: false

          }).done(function(x) {
            vuPublicForum.publicForums = R.map(vuPublicForum.calcFromNow, x);
            $('#btn-input').val('');
            Vue.nextTick(function() { 
              $('.threadcomments').each(function() {
                $(this).html(mdHtml.render($(this).data('thread')));
              })
              $('.chat-panel').scrollTop($('.chat-panel')[0].scrollHeight);
            });
            $('#btn-chat').prop('disabled', false);

            if (vuPublicForum.showJoin) showJoinupWindow();


          }).fail(function(err) {
            showLoginWindow();

          })
        }

        $('#previewcontainer').hide();
        $('#preview').html('');

      } else {
        showLoginWindow();

      };

    },
    previewDiscussion: function(discussion) {
      $('#previewcontainer').show();
      $('#preview').html(mdHtml.render(discussion));
    },
    loadDiscussion: function(matchkey) {
      $.ajax({
        type: "GET",
        url: "/api/v1/matches/" + matchkey + "/publicforums",
        dataType: "json",
        data: {},
        cache: false

      }).done(function(x) {
        var whenNewItem = (vuPublicForum.publicForums.length != x.length);
        vuPublicForum.publicForums = R.map(vuPublicForum.calcFromNow, x);
        if (whenNewItem) {

          Vue.nextTick(function() { 
            $('.threadcomments').each(function() {
              $(this).html(mdHtml.render($(this).data('thread')));
            })
            $('.chat-panel').scrollTop($('.chat-panel')[0].scrollHeight);
          });

        }
      }).fail(function(err) {
      })
    },
    deletePost: function(matchkey, threadid) {
      $.ajax({
        type: "DELETE",
        url: "/api/v1/matches/" + matchkey + "/publicforums/" + threadid,
        dataType: "json",
        data: {
          user: this.useremail 
        },
        cache: false
      }).done(function(x) {
        const mergeIfHash = R.curry(function(hash, obj) {
          return (obj.hash === hash) 
            ? R.merge(obj, {hide:true})
            : obj;
        });

        vuPublicForum.publicForums = R.map(mergeIfHash(threadid), vuPublicForum.publicForums);
        vuPublicForum.loadDiscussion(matchkey);

      }).fail(function(err) {
      });

    }
  }
});

const locationEdit = function() {
  vuLocation.editLocation();
};

const locationCancel = function() {
  vuLocation.cancelLocation();
};

const setupPages = function(matchkey, useremail, userpageslug, username, useravatar, enableEdit, ispremium, showJoin, navigationNouser, serviceurl) {

  $.ajax({
    type: "GET",
    url: "/api/v1/matches/" + matchkey,
    dataType: "json",
    data: {
      email: useremail
    },
    cache: false
  }).done(function(x) {

    vuLogin.eventid = matchkey;

    vuPublic.isprivate = x.isprivate;
    vuPublic.showEdit = enableEdit;
    vuPublic.reoccurenable = x.reoccur.enable;
    vuPublic.eventid = matchkey;
    vuPublic.seriesid = x.seriesid;

		vuTitle.textTitle = x.title;
    vuTitle.showEdit = enableEdit;
    vuTitle.eventStatus = x.status;
    vuTitle.isprivate = x.isprivate;
    vuTitle.key = matchkey;
    vuTitle.useremail = useremail;

    vuTimeslots.key = matchkey;
    vuTimeslots.useremail = useremail;

    vuLabel.showEdit = enableEdit;
    vuLabel.selectItem(x.experience);
    vuLabel.key = matchkey;
    vuLabel.useremail = useremail;

    vuDescription.showEdit = enableEdit;
    vuDescription.description = x.description;
    vuDescription.key = matchkey;
    vuDescription.useremail = useremail;

    vuUserButtons.noUser  = navigationNouser;
    vuUserButtons.key     = matchkey;
    vuUserButtons.useremail = useremail;

		vuUsers.autoaccept = x.autoaccept;
		vuUsers.seatmin = x.seatmin;
		vuUsers.seatavailable = x.seatavailable;
		vuUsers.seatmax = x.seatmax;
    vuUsers.showInviteByEmail = ispremium;
    vuUsers.showJoin = showJoin;
    vuUsers.noUser = navigationNouser;
    vuUsers.showTooltips();
    vuUsers.isprivate = x.isprivate;
    vuUsers.showEdit = enableEdit;
    vuUsers.reoccurenable = x.reoccur.enable;
    vuUsers.eventid = matchkey;
    vuUsers.eventStatus = x.status;
    vuUsers.seriesid = x.seriesid;

    vuDetails.showEdit = enableEdit;
		vuDetails.seatmin = x.seatmin;
    vuDetails.seatmax = x.seatmax;
    vuDetails.alcohol = x.alcohol;
    vuDetails.smoking = x.smoking;
    vuDetails.alcoholtext = alcoholText(x.alcohol);
    vuDetails.smokingtext = smokingText(x.smoking);
    vuDetails.price = Number(x.price);
    vuDetails.currency = x.currency;
    vuDetails.entryfee = vuDetails.updateFee(x.price, x.currency);

    if ((x.experience == 6) || (x.experience == 11)) {
      vuDetails.isOnline = true;
      vuLocation.isOnline = true;

    } else {
      vuDetails.isOnline = false;
      vuLocation.isOnline = false;
      vuLocation.showEdit = enableEdit;
      vuLocation.setLocation(R.replace(/&amp;/, '&', x.locname));
    }

    vuTime.showEdit = enableEdit;
    vuTime.playdateedit = moment(x.date).format('DD MMM YYYY');
    vuTime.playdateformat = moment(x.date).format('dddd, Do MMM YYYY');
    vuTime.timestartedit = moment(x.date).format('HH:mm');
    vuTime.timestartformat = moment(x.date).format('h:mm a');
    vuTime.timeendedit = moment(x.timeend).format('HH:mm');
    vuTime.timeendformat = moment(x.timeend).format('h:mm a');
    vuTime.dateendedit = moment(x.timeend).format('DD MMM YYYY');
    vuTime.dateendformat = moment(x.timeend).format('dddd, Do MMM YYYY');
    vuTime.isstartdatevalid = moment(x.date).isValid();
    vuTime.isenddatevalid = moment(x.timeend).isValid();
    vuTime.seriesid = x.seriesid;

    if (x.reoccur.enable) {
      vuTime.reoccurevery = x.reoccur.every;
      vuTime.reoccurinterval = x.reoccur.interval;
    }

    vuGames.showEdit = enableEdit;
    vuGames.showJoin = showJoin;
    vuGames.useremail = useremail;
    vuGames.userpageslug = userpageslug;
    vuGames.username = username;
    vuGames.useravatar = useravatar;
    vuGames.matchkey = matchkey;
    vuGames.serviceurl = serviceurl;
    vuGames.isUser = useremail != "";

    vuPublicForum.useremail = useremail;
    vuPublicForum.showJoin = showJoin;

    vuUsers.setupTypeAhead(matchkey, useremail);
    vuUsers.loadUsers(useremail, matchkey, x.status);
    vuPublicForum.loadDiscussion(matchkey);
    vuTimeslots.loadTimeslots(matchkey);

    vuGames.setupTypeAhead(matchkey, useremail, serviceurl);
    vuGames.loadGames(matchkey, serviceurl);


    if ((R.has('date', x)) && (R.has('timeend', x)) && (x.status !== "draft")) {
      vuShowDate.loadDates(true, x.date, x.timeend);
    };

    loadPlayAreaTypeahead();

    // fix the setIntervals
    window.setInterval(function() {
      vuUsers.loadUsers(useremail, matchkey, x.status);
      vuPublicForum.loadDiscussion(matchkey);
      if (vuTimeslots.votevisible)
        vuTimeslots.loadTimeslots(matchkey);
    }, 20000);

  }).fail(function(err) {
    alert('Failed to load the match.');
  });

  $('#hostgame').on('click', function(event) {
    window.location.href = "/hostgame";
  }); 

};

const setupMore = function(status, buttons) {
  vuPublicForum.eventStatus = status;
  vuUserButtons.buttons = buttons;
};
