var vuStage = new Vue({
  el: '.containerx',
  data: {
    notifyevents    : false,
    encodeLocation  : "https://www.google.com/maps/embed/v1/place?q=477%20Princes%20Hwy,%20Woonona%20NSW%202517,%20Australia&key=AIzaSyDZGpB9hMYMo9pAg6mydAR0PXYcUh-77QQ",
    locationName    : "",
    playcache       : [],
    searchradius    : 50,
    units           : ""
  },
  methods: {

    // initValues :: s
    initValues: function() {

      Vue.nextTick(function() { 
        setTimeout(function() {
          $('#playarea').typeahead(
            {
              hint: true,
              highlight: true,
              minLength: 1
            },
            {
              source: function(query, sync, process) 
              {
                return $.ajax({
                  type: "GET",
                  url: "/api/v1/locations",
                  dataType: "json",
                  data: {search: query},
                  success: function(data) {
                    vuStage.playcache = data;
                    vuStage.setLocation(R.head(R.map(R.path(['place','address']), data)));
                    return process(R.map(R.path(['place','address']), data));
                  }
                });
              }
            }
          )
        });
      });
    },
    setLocation: function(locname) {

      // findCountry :: s -> Boolean
      const findCountry = R.compose(
        R.last,
        R.split(' ')
      );

      this.locationName = R.replace(/&amp;/, '&', locname);
      this.encodeLocation = "https://www.google.com/maps/embed/v1/place?q=" +
        R.replace(/&/g, '%26', R.replace(/#/g, '%23', encodeURI(locname))) +
        "&key=AIzaSyDZGpB9hMYMo9pAg6mydAR0PXYcUh-77QQ";

      if (findCountry(locname) == "USA") {
        this.units = "mi"
      } else {
        this.units = "km"
      };

    },
    submituser: function(email, userkey, units, searchradius, playcity, notifyevents ) {

      // convertMiles :: s -> n -> n
      const convertMiles = function(units, distance) {
        if (units == "mi") {
          return Math.round(distance * 1.60934);
        } else {
          return Math.round(distance);
        }
      };

      // Show full page LoadingOverlay
      $.LoadingOverlay("show", {'z-index': 9999});

      ajaxNext = $.ajax({
        type: "PATCH",
        url: "/api/v1/accounts/" + email,
        timeout: 5000,
        dataType: "json",
        data: {
          stage:        2,
          key:          userkey,
          notifyevents: notifyevents,
          playcity:     playcity,
          playcitylat:  R.find(R.pathEq(['place', 'address'], playcity), this.playcache).place.coords.lat,
          playcitylng:  R.find(R.pathEq(['place', 'address'], playcity), this.playcache).place.coords.lng,
          searchradius: convertMiles(units, searchradius),
          newplayernotify: true
        },
        cache: false
      });

      ajaxNext.done(function(result) {
        $.LoadingOverlay("hide");
        window.location.href = '/stage3';
      });

      ajaxNext.fail(function(e, errortext) {
        $.LoadingOverlay("hide");
        alert(errortext);
      });

    },
    verifyLatLng: function(latlng) {
      return !isNaN(latlng) && (latlng > 0);
    },
    verifyLocName: function(locname) {
      return R.trim(locname) !== "";
    },
    nextclick: function(email, userkey, units, searchradius, playcity, notifyevents ) {

      if (!this.verifyLocName(playcity)) {
        alert("Please select a valid location.");
        $('#playarea').focus();

      } else {
        this.submituser(email, userkey, units, searchradius, playcity, notifyevents );

      }

    }
  }
});
