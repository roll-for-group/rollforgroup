var vuPage = new Vue({
  el: '#wrapper',
  data: {
    reoccurenable : false,
    eventid       : '',
    seriesid      : ''
  },
  methods: {
    geturl: function(reoccurenable, eventid, seriesid) {
      return (reoccurenable)
        ? 'https://www.rollforgroup.com/events/series/' + seriesid
        : 'https://www.rollforgroup.com/events/' + eventid
    },

  }
});

const setupPages = function(matchkey, useremail) {
  $.ajax({
    type: "GET",
    url: "/api/v1/matches/" + matchkey,
    dataType: "json",
    data: {
      email: useremail
    },
    cache: false

  }).done(function(x) {

    vuPage.eventid = matchkey;
    vuPage.seriesid = x.seriesid;
    vuPage.reoccurenable = x.reoccur.enable;

  }).fail(function(err) {
    alert('Failed to load the match.');

  });
};
