var vuStage = new Vue({
  el: '.containerx',
  data: {
    bggname:    '',
    isteacher:  1,
    ishost:     1
  },
  methods: {
    // initValues :: s
    initValues: function(username, pageslug) {
      this.name = username;
      this.pageslug = pageslug;

    },
    submituser: function(email, userkey, isteacher, ishost, bggname) {

      ajaxNext = $.ajax({
        type: "PATCH",
        url: "/api/v1/accounts/" + email,
        timeout: 5000,
        dataType: "json",
        data: {
          stage:        4,
          key:          userkey,
          isteacher:    isteacher,
          ishost:       ishost,
          bggname:      bggname
        },
        cache: false
      });

      ajaxNext.done(function(result) {
        $.LoadingOverlay("hide");
        window.location.href = '/events';
      });

      ajaxNext.fail(function(e, errortext) {
        $.LoadingOverlay("hide");
        alert(errortext);
      });

    },
    nextclick: function(pageslug, email, userkey, isteacher, ishost, bggname) {

      // Show full page LoadingOverlay
      $.LoadingOverlay("show", {'z-index': 9999});
      vuStage.submituser(email, userkey, isteacher, ishost, bggname);

    }
  }
});
