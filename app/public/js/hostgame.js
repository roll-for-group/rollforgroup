var vuHostGame = new Vue({
  el: '#maincontent',
  data: {
    serviceurl: '',
    description:  'Come and play board games with us!',
    experience: 6,
    showReoccurEvery: false,
    intervalLabel: '',
    games: [],
    selectedGame: {},
    hostsmoking: 1,
    hostalcohol: 1,
    hostcurrency: 'AUD',
    reoccurevery: 1,
    reoccurinterval: 'never',
    userpageslug: '',
    startdate: moment().add(7, 'days').format('DD MMM YYYY'),
    starttime: '19:00',
    enddate: moment().add(7, 'days').format('DD MMM YYYY'),
    endtime: '22:30',
    timeslots: [],
    isonline: true,
    public: 1 
  },
  methods: {
    clickIsOnline: function(valWas) {
      (valWas == true) 
        ? this.experience = 1
        : this.experience = 6;
    },
    selectEventType: function(event) {
      switch (event.target.value) {
        case "6":
          this.isonline = true;
          break;

        case "11":
          this.isonline = true;
          break;

        default:
          this.isonline = false;
      }
    },
    showTooltips: function() {
      $('.vutips').tooltip();
    },
    clickBggLink: function(id) {
      window.open("/games?gameid=" + id, "_blank");
    },
    lookupGameValues: function (prop, gameslug, games) {
      return R.compose(
        R.prop(prop),
        R.find(R.propEq('pageslug', gameslug))
      )(games)
    },
    ifhasexpansions: function (gameslug, games) { 
      return R.compose(
        R.has('hasexpansions'),
        R.find(R.propEq('pageslug', gameslug))
      )(games)
    },
    executeIfDefined: R.curry(function(fx, a, b) {
      return (R.isNil(a)) 
        ? b
        : (R.isNil(b))
          ? undefined
          : fx(a, b);
    }),
    readminexpansiondata: function (games, expansions, prop, gameslug) {
      return vuHostGame.ifhasexpansions(gameslug, games)
        ? R.reduce(vuHostGame.executeIfDefined(R.min), vuHostGame.lookupGameValues(prop, gameslug, games), vuHostGame.lookupGameValues('hasexpansions', gameslug, games).map(R.prop(prop)))
        : vuHostGame.lookupGameValues(prop, gameslug, games);
    },
    readmaxexpansiondata: function (games, expansions, prop, gameslug) {
      return vuHostGame.ifhasexpansions(gameslug, games)
        ? R.reduce(vuHostGame.executeIfDefined(R.max), vuHostGame.lookupGameValues(prop, gameslug, games), vuHostGame.lookupGameValues('hasexpansions', gameslug, games).map(R.prop(prop)))
        : vuHostGame.lookupGameValues(prop, gameslug, games);
    },
    expansionListClass: function(userpageslug, gameslug, expansionid, isuse) {
      try {
        if (userpageslug == "") {
          return "disabled";

        } else {
          return "showpointer";

        }
      } catch(err) {
        return "disabled";

      }
    },
    buildLink: function(id) {
      return "/games?gameid=" + id;
    },
    labelfyWeight: function(weight) {
      return (weight <= 1.8) 
        ? 'Light'
        : (weight <= 2.6)
          ? 'Med Light'
          : (weight <= 3.4)
            ? 'Medium'
            : (weight <= 4.2)
              ? 'Med Heavy'
              : 'Heavy'
    },
    classifyWeight: function(weight) {
      return (weight <= 1.8) 
        ? 'vutips label label-success'
        : (weight <= 2.6)
          ? 'vutips label label-success'
          : (weight <= 3.4)
            ? 'vutips label label-info'
            : (weight <= 4.2)
              ? 'vutips label label-warning'
              : 'vutips label label-warning'
    },
    titleWeight: function(weight) {
      return 'BGG Avg Weight: ' + weight
    },
    removeGame(bggid) {
      // notPropEq :: o -> bool
      const notPropEq = R.compose(
        R.not,
        R.propEq('id', bggid)
      );
      this.games = this.games.filter(notPropEq); 
    },
    setupTypeAhead: function(matchkey, useremail, serviceurl) {

      const setupGameTypeahead = function(inpName) {

        var boardGames = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          remote: {
            url: serviceurl + '/?name=%QUERY',
            wildcard: '%QUERY'
          }
        });

        $(inpName).typeahead({
          hint: true,
          highlight: true,
          minlength: 1,
        }, {
          source:  boardGames,
          display: function(item) {
            return item.name;

          },
          limit: 15,
          templates: {
            suggestion: function(item) {
              var playerCountHtml = function (playercountobj) {
                if (playercountobj.status==='Best') {
                  return '<span class="vutips label label-info">' + playercountobj.playercount + '</span>';
                } else {
                  return '';
                }
              };

              return '<div>' + 
                '<table class="typeahead-table"><tr class="typeahead-row">' +
                '<td class="typeahead-gamebox"><img src="' + item.thumbnail + '" style="max-height: 48px;"></td>' +
                '<td class="addgametext"><span> ' + item.name + ' <small>(' + item.yearpublished + ')</small></span></td>' +
                '<td class="typeahead-players hide-small-screen"><i class = "fa fa-users"></i> <span>' + item.minplayers + '-' + item.maxplayers + '</span> ' +
                R.join('', item.playercounts.map(playerCountHtml)) + '</td>' + 
                '<td class="typeahead-time hide-small-screen"><i class = "fa fa-clock-o"></i> <span>' + item.minplaytime + '-' + item.maxplaytime + ' mins</span> <span class="' + vuHostGame.classifyWeight(item.weight) + '" title="' + vuHostGame.titleWeight(item.weight) + '">' + vuHostGame.labelfyWeight(item.weight) + '</span></td>' +
                '</tr></table>' +
              '</div>';

            }
          } 
        });

        $(inpName).bind('typeahead:selected', function(obj, datum, name) {
         
          // associateExpansions :: o -> o
          const associateExpansions = R.compose(
            R.assoc("expansions", []),
            R.assoc("hasexpansions", [])
          );

          vuHostGame.games.push(
            associateExpansions(datum)
          );

          $(inpName).typeahead('val', '');

        })
      };

      setupGameTypeahead('#addgameid');

    },
    wrapChars: function(mustselect, before, after) {
      var editor = document.getElementById("notes");

      //var editorHTML = editor.value;
      var selectionStart = 0, selectionEnd = 0;

      if (editor.selectionStart) selectionStart = editor.selectionStart;
      if (editor.selectionEnd) selectionEnd = editor.selectionEnd;

      if ((selectionStart != selectionEnd) || !mustselect) {
        var editorCharArray = vuHostGame.description.split("");
        editorCharArray.splice(selectionEnd, 0, after);
        editorCharArray.splice(selectionStart, 0, before); //must do End first
        vuHostGame.description = editorCharArray.join("");
      }

    },
    renderDescription: function(desc) {

      const defaults = {
        html:         false,        // Enable HTML tags in source
        xhtmlOut:     false,        // Use '/' to close single tags (<br />)
        breaks:       true,        // Convert '\n' in paragraphs into <br>
        langPrefix:   'language-',  // CSS language prefix for fenced blocks
        linkify:      true,         // autoconvert URL-like texts to links
        typographer:  true,        // Enable smartypants and other sweet transforms
      };

      var mdHtml = window.markdownit(defaults).use(window.markdownitEmoji);

      return mdHtml.render(desc);
    },
    // areDatesValid :: dt -> dt -> dt -> dt -> bool 
    areDatesValid: function(startdate, starttime, enddate, endtime) {

      var err = '';
      if (!moment(startdate + ' ' + starttime, 'DD MMM YYYY HH:mm', true).isValid())
        err = 'The start date time is not valid. ';

      if (!moment(enddate + ' ' + endtime, 'DD MMM YYYY HH:mm', true).isValid())
        err = err + 'The end date time is not valid. ';

      if ((moment(enddate + ' ' + endtime).diff(moment(startdate + ' ' + starttime), "h")) > 24)
        err = err + 'The event is longer than 24 hours, please revise the start and end date times. '; 

      if (moment(enddate + ' ' + endtime).isBefore(moment(startdate + ' ' + starttime)))
        err = err + 'The end date time should be after the start date time. '; 

      return err;
    },
    pushTimeslot: function(startdate, starttime, enddate, endtime) {

      // generateUID -> s
      function generateUID() {
        var firstPart = (Math.random() * 46656) | 0;
        var secondPart = (Math.random() * 46656) | 0;
        firstPart = ("000" + firstPart.toString(36)).slice(-3);
        secondPart = ("000" + secondPart.toString(36)).slice(-3);
        return firstPart + secondPart;
      };

      var err = vuHostGame.areDatesValid(startdate, starttime, enddate, endtime);

      if (err == '') {
        vuHostGame.timeslots.push({id: generateUID(), startdate: startdate, starttime: starttime, enddate: enddate, endtime: endtime});
      } else {
        alert(err);
      }
    },
    removeTimeslot: function(id) {
      var removeId = R.propEq('id', id);
      vuHostGame.timeslots = R.reject(removeId, vuHostGame.timeslots);
    },
    pressHeadDesc: function() {
      vuHostGame.wrapChars(false, "\n## ", "\n");
    },
    pressBoldDesc: function() {
      vuHostGame.wrapChars(true, "**", "**");
    },
    pressItalDesc: function() {
      vuHostGame.wrapChars(true, "*", "*");
    },
    pressQuotDesc: function() {
      vuHostGame.wrapChars(false, "\n> ", "\n\n");
    },
    pressListUlDesc: function() {
      vuHostGame.wrapChars(false, "\n* ", "\n\n");
    },
    pressListOlDesc: function() {
      vuHostGame.wrapChars(false, "\n1. ", "\n\n");
    },
    pressLinkDesc: function() {
      var url = prompt("What is the URL?");
      vuHostGame.wrapChars(false, "[", "](http://" + url + ")");
    },
    pressImagDesc: function() {
      var url = prompt("What is the picture URL?");
      vuHostGame.wrapChars(false, "![", "](http://" + url + ")");
    },
    pressYtubeDesc: function() {
      var url = prompt("What is the youtube video id?");
      if (url.length == 11) {
        vuHostGame.wrapChars(false, "[![", "](https://img.youtube.com/vi/" + url + "/0.jpg)](https://www.youtube.com/watch?v=" + url + ")");
      } else {
        alert("A youtube id can only be 11 characters long. Copy the youtube video id from the end of any youtube video url");
      }
    },
    formatHelp: function() {
      window.open("http://commonmark.org/help/", "_blank");
    },
    bgglink: function(id) {
      return 'games?gameid=' + id;
    }
  }
});

/*
// setupGameTypeahead :: s
const setupGameTypeahead = function(serviceUrl, inpName) {
  var boardGames = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: serviceUrl + '/?name=%QUERY',
      wildcard: '%QUERY'
    }
  });

  $(inpName).typeahead({
    highlight: true,
    minlength: 2,
  }, {
    source:  boardGames,
    display: function(item) {
      return item.name;
    },
    limit: 15,
    templates: {
      suggestion: function(item) {
        return '<div>' + 
          '<table class="typeahead-table"><tr class="typeahead-row">' +
          '<td class="typeahead-gamebox"><img src="' + item.thumbnail + '" style="max-height: 48px;"></td>' +
          '<td class="addgametext"><span> ' + item.name + ' <small>(' + item.yearpublished + ')</small></span></td>' +
          '<td class="typeahead-players hide-small-screen"><i class = "fa fa-users"></i> <span>' + item.minplayers + '-' + item.maxplayers + '</span></td>' +
          '<td class="typeahead-time hide-small-screen"><i class = "fa fa-clock-o"></i> <span>' + item.minplaytime + '-' + item.maxplaytime + ' mins</span> <span class="' + vuHostGame.classifyWeight(item.weight) + '" title="' + vuHostGame.titleWeight(item.weight) + '">' + vuHostGame.labelfyWeight(item.weight) + '</span></td>' +
          '</tr></table>' +
        '</div>';
      }
    } 
  });

  $(inpName).bind('typeahead:selected', function(obj, datum, name) {
    vuHostGame.selectedGame = datum;
  })

};
 */


var setuppage = function(useremail, boardgameServiceURL) {

  vuHostGame.serviceurl = boardgameServiceURL;
  vuHostGame.setupTypeAhead('', useremail, boardgameServiceURL);
  // setupGameTypeahead(boardgameServiceURL, '#addgameid');

  $(document.body).on('change', '.clockpicker', function (){
    vuHostGame.starttime = $('#timestart').val();
    vuHostGame.endtime = $('#timeend').val();
  });

  $('.clockpicker').clockpicker({
		donetext: 'OK'
  });

  $('#nextverify').click(function() {
    hostgame(useremail);
  });

  // date range picker
  var picker = new Lightpick({ 
    field: document.getElementById('playdate'),
    secondField: document.getElementById('playdateend'),
    minDate: moment(),
    selectForward: true,
    maxDays: 2,
    singleDate: false,
    onSelect: function(start, end){
      vuHostGame.startdate = start.format('DD MMM YYYY');
      if (!R.isNil(end))
        vuHostGame.enddate = end.format('DD MMM YYYY');
    }
  }); 

};


// hostgame :: s -> 
const hostgame = function(useremail) {

  const chosenAddress = R.compose(R.propEq('address', $('#playarea').val()), R.prop('place'));
  const chosenCity = R.compose(R.path(['place', 'coords']), R.find(chosenAddress));
  const cityFound = R.compose(R.not, R.isNil, chosenCity);
  const getLat = R.compose(R.prop('lat'), chosenCity);
  const getLng = R.compose(R.prop('lng'), chosenCity);

	const alertify = (x) => {
		alert(x);
		return x;
	};

  const checknumber = R.compose(R.not, R.test(/^\d+(?:\.\d{0,2})$/));

  const checkplayers = R.gte(R.__, 2);

  const checkplayersdef2 = function (val) {
    return checkplayers(val) 
      ? val 
      : 2;
  };

  const notNil = R.compose(R.not, R.isNil);
  const notEmpty = R.compose(R.not, R.isEmpty);

  $("#nextverify").prop('disabled', true);

  // momentifyTimeData :: o -> o 
  var momentifyTimeData = function(obj) {
    return {
      id: obj.id,
      startdatetime: moment(obj.startdate + ' ' + obj.starttime).format(), 
      enddatetime: moment(obj.enddate + ' ' + obj.endtime).format()
    };
  };


  if (checknumber($('#price').val())) {
    if (checkplayers($('#hostmax').val())) {
      if ($('#title').val().length > 0) {

        var reoccurevery = (vuHostGame.reoccurevery)
          ? vuHostGame.reoccurevery
          : 1;

        var formdata = {
          games: 				vuHostGame.games,
          title: 				$('#title').val(),
          description: 	vuHostGame.description,
          status: 			'draft', 
          isprivate: 		(vuHostGame.public == 1),
          timeslots: 		R.map(momentifyTimeData, vuHostGame.timeslots),
          date: 				moment(vuHostGame.startdate + ' ' + $('#timestart').val()).format(), 
          timeend: 			moment(vuHostGame.enddate + ' ' + $('#timeend').val()).format(), 
          reoccurevery: reoccurevery,
          reoccurinterval: vuHostGame.reoccurinterval,
          minplayers: 	checkplayersdef2($('#hostmin').val()),
          maxplayers: 	$('#hostmax').val(),
          price: 				$('#price').val(),
          currency: 		$('#currency').val(),
          email: 				useremail,
          utcoffset: 		moment().utcOffset(),
          timezone:  		moment.tz.guess(),
          smoking: 			vuHostGame.hostsmoking,
          alcohol: 			vuHostGame.hostalcohol,
          experience: 	vuHostGame.experience,
          autoaccept:		$('#autoaccept').prop('checked')
        };

        // is the event online 
        if ((vuHostGame.experience == 6) || (vuHostGame.experience == 11)) {
          submitUserData(formdata);

        } else {
          var ajaxCall = $.ajax({
            type: "GET",
            url: "/api/v1/locations",
            dataType: "json",
            data: {search: $('#playarea').val()}
          });

          ajaxCall.done(function(data) {
            if (($('#playarea').val().length > 0) && notNil(data) && cityFound(data)) {

              formdata = R.merge(formdata, {
                playarea: 		$('#playarea').val(),
                playarealat: 	getLat(data),
                playarealng: 	getLng(data),
              });
              submitUserData(formdata);

            } else {
              alert('Please select a valid location');
              $('#div4playarea').addClass('has-warning');
              $('#playarea').focus();
              $('#nextverify').prop('disabled', false);
            }
          })

          ajaxCall.fail(function(data) {
            alert('The location could not be geolocated, please ensure the location is valid');
            $('#div4playarea').addClass('has-warning');
            $('#playarea').focus();
            $("#nextverify").prop('disabled', false);
          });
            
        }

      } else {
        alert('Please enter a valid title');
        $('#div4title').addClass('has-warning');
        $('#title').focus();
        $('#nextverify').prop('disabled', false);
      }

    } else {
      alert('The number of people joining should be greater than 1');
      $('#hostmax').focus();
      $("#nextverify").prop('disabled', false);
    }

  } else {
    alert('The amount should be a number');
    $('#charge').focus();
    $("#nextverify").prop('disabled', false);
  }

};

// submitUserData :: JSONObj
const submitUserData = function(senddata) {

  $.ajax({
    type: "POST",
    url: "/api/v1/matches",
    dataType: "json",
    data: senddata,
    cache: false
  }).done(function(x) {
    $("#nextverify").prop('disabled', false);
    window.location.href = '/eventsthankyou?matchkey=' + x.key;
  }).fail(function(err) {
    alert('Match could not be created, please try again');
    $("#nextverify").prop('disabled', false);
  });

};
