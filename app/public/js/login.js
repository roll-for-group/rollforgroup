// getQueryString :: s -> s
const getQueryString = function (key) {
  var key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
  var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
  return match && decodeURIComponent(match[1].replace(/\+/g, " "));
};


var vuLogin = new Vue({
  el: '.login-form',
  data: {
    showpassword: false,
    whenCapsIsOn: false,
    showLogonError: false,
    showLogonFailure: (getQueryString('failed')=='true'),
    errortext: '',
    formusername: '',
    formpassword: '',
  },
  methods: {
    getPasswordType: function(show) {
      return show 
        ? 'text' 
        : 'password'
    },
    detectCaps: function(e) {
      this.whenCapsIsOn = (e.getModifierState("CapsLock")); 
    },
    submitLoginForm: function(e) {
      this.errortext = '';

      if (this.formusername == '') {
        $('#form-username').focus();
        this.errortext = this.errortext + 'Please enter your email address. ';
        this.showLogonError = true;
      };

      if (this.formpassword == '') {
        this.showLogonError = true;
        if (this.errortext == '')  {
          $('#form-password').focus();
        };
        this.errortext = this.errortext + 'Please enter a password. ';
      };

      if (this.errortext != '') {
        e.preventDefault();
      }
    }
  }
});
