var vuGroups = new Vue({
  el: '#maincontent',
  data: {
    groups: [],
    typeaheadCache: []
  },
  methods: {
    newgroup: function() {
      window.location.href = '/groupsnew';
    },
    buildurl: function(url) {
      return "/groups/" + url;
    },
    usecurrentlocation: function(position) {
      ajaxPlayers = $.ajax({
        type: "GET",
        url: "api/v1/groups",
        dataType: "json",
        data: {
          lat: position.coords.latitude, 
          lng: position.coords.longitude
        }
      });
      ajaxPlayers.done(function(x) {
        vuGroups.groups = x;
        $('#playarea').val('My Location');
      });
    },
    errorposition: function(err) {
      alert("The website needs permission to use your location.")
    },
    geolocate: function() {
      var options = {
        timeout: 5000,
        maximumAge: 0
      };
      navigator.geolocation.getCurrentPosition(vuGroups.usecurrentlocation, vuGroups.errorPosition, options);

    }
  }
});


const cacheTypeaheadData = function(data) {
  vuGroups.typeaheadCache = data;
};

// findCoords :: s -> o -> n 
const findCoords = function(key, data, placename) {
  return R.path(['place', 'coords', key], R.find(R.pathEq(['place', 'address'], placename), data));
};

// loadArea :: s -> 
const loadArea = function(area) {

  ajaxPlayers = $.ajax({
    type: "GET",
    url: "api/v1/groups",
    dataType: "json",
    data: {
      lat: findCoords('lat', vuGroups.typeaheadCache, area),
      lng: findCoords('lng', vuGroups.typeaheadCache, area)
    }
  });
  ajaxPlayers.done(function(x) {
    vuGroups.groups = x;
  });

};


// getData :: s -> o
const getData = function(useremail) {
  return R.isNil(useremail)
    ? {email: useremail}
    : {};
};

// if there is anything to setup
const setuppage = function(useremail) {

  $.ajax({
    type: "GET",
    url: "/api/v1/groups",
    dataType: "json",
    data: getData(useremail),
    cache: false
  }).done(function(x) {
    vuGroups.groups = x;
  });


};
