var momentify = R.compose(moment, R.join(' '));


const formatMinutes = function(minute) {
  return (minute < 10) 
    ? "0" + minute 
    : minute;
};

const formatAmPm = function(hour) {
  return (hour < 12) 
    ? " am" 
    : " pm";
};

Vue.component('basic-textinput', {
  props: ['id', 'label', 'value', 'before'],
  data: function() {
    return {
      content: this.value
    }
  },
  methods: {
    builddiv4: function(nameprop) {
      return 'div4' + nameprop;
    },
    updateValue: function(e) {
      this.value = e.target.value;
      this.$emit('input', e.target.value);
    }
  },
  template: `<fieldset class="form-group col-xs-6">
    <div :id="builddiv4(id)" class="input-group text col-xs-12">
      <label class="form-label" :for="id">{{label}}</label>
      <input :id="id" :name="id" :value="value" v-on:input="updateValue" class="form-control text" type="text" :data-before="before">
    </div>
  </fieldset>`
});

Vue.component('scheduleday', {
  props: ['id', 'label', 'value', 'starthour', 'startminute', 'endhour', 'endminute'],
  data: function() {
    return {
      value: true
    }
  },
  watch: {
    value: function(val) {
      if (val) {
        $("#div4" + this.id).slideDown('slow');
      } else {
        $("#div4" + this.id).slideUp('slow');
      }
    },
    endminute: function(val) {
      $(this.jqueryify(this.id)).dateRangeSlider(
        "values", 
        new Date(2010, 0, 1, Number(this.starthour), Number(this.startminute)), 
        new Date(2010, 0, 1, Number(this.endhour), Number(this.endminute))
      );
    },
  },
  mounted: function() {
    this.$nextTick(() => {
    });
  },
  methods: {
    builddiv4: function(nameprop) {
      return 'div4' + nameprop;
    },
    jqueryify: function(id) {
      return '#' + id;
    },
    updateValue: function(e) {
      this.value = e.target.checked;
      this.$emit('input', e.target.checked);
    }
  },
  template: `<div>
    <div :id="builddiv4(label)" class="col-xs-12">
      <div class="checkbox">
        <label><input v-model="value" @click="updateValue" type="checkbox" :id="label" :name="label">{{label}}</label>
      </div>
    </div>
    <div :id="builddiv4(id)" class="col-xs-12 textbar">
      <div class = "row">
        <div class = "col-xs-12">
          <div :id="id" class="hourslider" :data-starthour="starthour" :data-startminute="startminute" :data-endhour="endhour" :data-endminute="endminute"></div>
        </div>
      </div>
    </div>
  </div>`
});


var vuProfile = new Vue({
  el: '#profile',
  data: {
    savedata: {
      name: '',
      job: '',
      biography: '',
      visibility: "1",
      motivecompetition: false,
      motivesocialmanipulation: false, 
      motivestrategy: false, 
      motivediscovery: false, 
      motivedesign: false,
      motivecommunity: false,
      motivecooperation: false,
      motivestory: false,
      notifyevents: false
    },
    beforedata: {
    },
    avataricon: '',
    hasfacebook: false,
    saveDisabled: false,
  },
  methods: {
    setFacebookImage: function(email) {

      vuProfile.saveDisabled = true;
      $.LoadingOverlay("show");

      var ajaxCall = $.ajax({
        type: "PATCH",
        url: "/api/v1/accounts/" + email + "/updatefacebook",
        timeout: 60000,
        contentType: false,
        processData: false,
        cache: false
      });

      ajaxCall.done(function(result) {
        vuProfile.avataricon = result.icon;
        vuProfile.saveDisabled = false;
        $.LoadingOverlay("hide");
      });

      ajaxCall.fail(function(e, errortext) {
        vuProfile.saveDisabled = false;
        $.LoadingOverlay("hide");
        alert('error:' + JSON.stringify(e));
      });

    },
    readURL: function(input) {
      if (input.files && input.files[0]) {
        if (input.files[0].size < 1000000) {
          var reader = new FileReader();
          reader.onload = function(e) {
            vuProfile.avataricon = e.target.result;
          };
          reader.readAsDataURL(input.files[0]);
          return true;

        } else {
          alert ('The file is too big, please select a smaller image, less than 1MB');
          input.val('');
          return false;
        }
      }
    },
    saveProfile: function(email) {

      // prevent the tutorial from showing
      var userdata = vuProfile.savedata;

      // TODO ensure time-date and city save

      $('.hourslider').each(function() {
        const weekday = R.compose(R.concat(R.__, 'day'), R.head, R.split('day'));
        var dateSliderBounds = $(this).dateRangeSlider("values");
        const buildData = R.compose( 
          R.assoc(weekday($(this).attr('id')) + "starthour", dateSliderBounds.min.getHours()), 
          R.assoc(weekday($(this).attr('id')) + "startminute", dateSliderBounds.min.getMinutes()), 
          R.assoc(weekday($(this).attr('id')) + "endhour", dateSliderBounds.max.getHours()), 
          R.assoc(weekday($(this).attr('id')) + "endminute", dateSliderBounds.max.getMinutes())
        );
        userdata = buildData(userdata);
         
      });

      var chosenAddress = R.compose(R.propEq('address', $('#playcity').val()), R.prop('place'));
      var chosenCity = R.compose(R.path(['place', 'coords']), R.find(chosenAddress));
      var cityFound = R.compose(R.not, R.isNil, chosenCity);
      var getLat = R.compose(R.prop('lat'), chosenCity);
      var getLng = R.compose(R.prop('lng'), chosenCity);

      const notNil = R.compose(R.not, R.isNil);

      //if ($('#playcity').val().length > 0) {
      if (vuProfile.savedata.playcity.length > 0) {

        var ajaxCall = $.ajax({
          type: "GET",
          url: "/api/v1/locations",
          dataType: "json",
          data: {search: $('#playcity').val()}
        });

        ajaxCall.done(function(data) {

          if (notNil(data) && cityFound(data)) {

            userdata.playcitylat = getLat(data);
            userdata.playcitylng = getLng(data);

            if ($.isNumeric(vuProfile.savedata.searchradius)) {
              if ((vuProfile.savedata.searchradius > 0) && (vuProfile.savedata.searchradius)) {
                submitUserData(email, userdata);

              } else {
                alert("Please select a radius betwen 1 and 99 km");
                $('#searchradius').focus();
              };

            } else {
              // $('#searchradius').val(50);
              vuProfile.savedata.searchradius == 50;
              submitUserData(email, userdata);
            }

          } else {
            alert("In order to find nearby players, please ensure you have entered a valid location");
            $('#playcity').focus();

          };
        });

        ajaxCall.fail(function(data) {
          alert('The location could not be geolocated, please ensure the location is valid');
          $('#div4playcity').addClass('has-warning');
          $('#playcity').focus();
        });

      } else {
        alert('Please enter a valid location');
        $('#div4playcity').addClass('has-warning');
        $('#playcity').focus();

      }

    }
  }
});

// sumbitUserData :: JSONObj
const submitUserData = R.curry(function(email, userdata) {

  // Show full page LoadingOverlay
  $.LoadingOverlay("show");
  var ajaxCall = $.ajax({
    type: "PATCH",
    url: "/api/v1/accounts/" + email,
    timeout: 60000,
    dataType: "json",
    data: userdata,
    cache: false
  });

  ajaxCall.done(function(result) {
    $.LoadingOverlay("hide");
    $('#saved').modal()
    window.location.href = "users?email=" + email;
  });

  ajaxCall.fail(function(e, errortext) {
    $.LoadingOverlay("hide");
    if (e.status == 404) {
      alert('The BGG username was not found');
      $('#bggname').focus();
    } else { 
      alert('error:' + JSON.stringify(e));
    }
  });

});

//setupData
const setupData = function (userdata) {
  vuProfile.savedata = userdata.savedata;
  vuProfile.beforedata = userdata.beforedata;
  vuProfile.avataricon = userdata.avataricon;
  vuProfile.hasfacebook = userdata.hasfacebook;
};
