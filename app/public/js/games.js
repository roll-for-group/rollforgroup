var vuGames = new Vue({
  el: '#maincontent',
  data: {
    focus: {
      name: '',
      description: '',
      id: '',
      boardgamesURL: ''
    },
    vid_show:   true,
    subvideos:  [],
    gamelists: []

  },
  methods: {
    showTooltips: function() {
      $('.vutips').tooltip();
    },
    bgglinkify: function(gameid) {
      return 'https://www.boardgamegeek.com/boardgame/' + gameid
    },
    updateName: function(game) {
      location.href="#maincontent";

      var elispify = function(a) {
        return a.length > 400
          ? R.take(400, a) + "..."
          : a;
      }

      // descripto :: s -> s
      var descripto = R.compose(
        elispify,
        R.replace(/\&apos\;/g, "'"),
        R.replace(/\&amp\;rsquo\;/g, "'"),
        R.replace(/\&amp\;hellip\;/g, "..."),
        R.replace(/\&amp\;ndash\;/g, "-"),
        R.replace(/\&amp\;mdash\;/g, "-"),
        R.replace(/\&amp\;quot\;/g, '"'),
        R.replace(/\&amp\;amp\;/g, '&'),
        R.replace(/\&amp\;ouml\;/g, "ö"),
        R.replace(/\&amp\;\&amp\;\#35\;40\;/g, "("),
        R.replace(/\&amp\;\&amp\;\#35\;41\;/g, ")"),
        R.replace(/\&amp\;\&amp\;\#35\;10\;/g, " "),
        R.replace(/\&amp\;ldquo\;/g, '"'),
        R.replace(/\&amp\;rdquo\;/g, '"')
      );

      vuGames.focus.name = game.name;
      vuGames.focus.description = descripto(game.description);
      vuGames.focus.id = game.id;

      var ajaxCall = $.ajax({
        type: "GET",
        url: vuGames.boardgamesURL + "/" + game.id + "/videos/",
        dataType: "json",
        data: {},
        cache: false
      });

      ajaxCall.done(function(x) {

        // alertify
        var alertify = function(x) {
          alert(JSON.stringify(x));
          return x;
        };

        // findIfYoutube :: {o} -> s
        var findIfYoutube = function(videoObj) {
          return (R.prop('link', videoObj) == undefined)
            ? ''
            : R.last(R.split("=", R.prop('link', videoObj)))

        };

        // pickAVideo :: [o] -> o
        var pickAVideo = R.compose(
          R.head,
          R.reject(R.equals('')),
          R.map(findIfYoutube),
          R.filter(R.propEq('language', 'English')),
          R.prop('videos')
        );

        // findSubVideos :: s -> s -> [a] -> s 
        var pickAVideoByCategory = R.curry(function(category, list) {
          return R.compose(
            R.head,
            R.reject(R.equals('')),
            R.map(findIfYoutube),
            R.filter(R.propEq('category', category)),
            R.filter(R.propEq('language', 'English')),
            R.prop('videos')
          )(list);
        });

        // findSubVideos :: s -> s -> [a] -> {o}
        var findSubVideos = R.curry(function(category, buttonname, list, sublist) {
          return (pickAVideoByCategory(category, list) !== undefined)
            ? R.append({ buttonname: buttonname, videoid: pickAVideoByCategory(category, list) }, sublist)
            : sublist 

        })

        if (pickAVideo(x) !== undefined) {
          vuGames.playVideo(pickAVideo(x, false));
          vuGames.vid_show = true;

          var buildButtons = R.compose(
            findSubVideos('review', 'Review', x),
            findSubVideos('session', 'Playthrough', x),
            findSubVideos('instructional', 'How to', x)
          );

          vuGames.subvideos = buildButtons([]);

        } else {
          vuGames.vid_show = false;
        };
      });

      window.locations.href = "#maincontent";

    },
    playVideo: (function(videoid, playnow) {
      videoLoadDone = playnow;
      player.loadVideoById(videoid);
    }),
    loadCollections: function(userid, userbggid, autoloadVideo, collectionsURL, boardgamesURL) {

      var ajaxCollectionCall = $.ajax({
        type: "GET",
        url: collectionsURL,
        timeout: 10000,
        dataType: "json",
        data: {userid: userid},
        cache: false
      });

      var ajaxCollectionDiscover = $.ajax({
        type: "GET",
        url: collectionsURL,
        timeout: 10000,
        dataType: "json",
        data: {
          discover: 1, 
          bggid: userbggid
        },
        cache: false
      });

      ajaxCollectionDiscover.done(function(result) {
        vuGames.loadGames(collectionsURL, boardgamesURL, result.key, "Discover", 0, autoloadVideo);
        ajaxCollectionCall.done(function(result) {
          for (i=0; i<result.length; i++) {
            vuGames.loadGames(collectionsURL, boardgamesURL, result[i].key, result[i].name, i + 1 , false);
          };
        });
      }).fail(function(err) {
        alert(JSON.stringify(err));
      });


    },
    openPageUrl: function(collectionkey) {
      window.open("/collection?id=" + collectionkey, "_self");
    },
    loadGames: function(collectionsURL, boardgamesURL, listkey, listname, index, autoloadVideo) {

      var ajaxCall = $.ajax({
        type: "GET",
        url: collectionsURL + "/" + listkey + "/games",
        dataType: "json",
        data: {},
        cache: false
      });

      ajaxCall.done(function(x) {
        const numify = function (a) {
          return R.has('bggrank', a)
            ? parseInt(R.prop('bggrank', a))
            : 99999;

        };

        // topTwenty :: [a] -> [a]
        const topTwenty = R.compose(
          R.take(20),
          R.sortBy(numify)
        );

        // add game
        Vue.set(vuGames.gamelists, index, {
          name: listname,
          key: listkey,
          games: topTwenty(x)
        });

        if (autoloadVideo && (x.length > 0)) {
          setTimeout(function() {
            vuGames.updateName(vuGames.gamelists[0].games[0]);
          }, 1000);
        };

        setTimeout(function() {
          $(".thumbnail-list").mThumbnailScroller({
            type: "click-50",
            theme: "buttons-in"
          })
        },
        800);

        setTimeout(function() {
          vuGames.showTooltips();
        }, 2000)

      });

    }

  }
});

// setuppage :: id
var setuppage = function (email, userslug, userbggid, gameid, boardgamesURL, collectionsURL) {

  vuGames.boardgamesURL = boardgamesURL;

  var lookupGame = function(lookupGameId) {

    if (player == undefined) {
      setTimeout(function() {lookupGame(gameid)}, 300);

    } else {

      ajaxLookupGame = $.ajax({
        type: "GET",
        url: boardgamesURL + "/" + lookupGameId,
        dataType: "json",
        data: {},
        cache: false
      });

      ajaxLookupGame.done(function(result) {
        setTimeout(function() {
          vuGames.updateName(result)
        }, 1000);
      });

      ajaxLookupGame.fail(function(err) {
        alert(err);
      });

    };

  }

  if (userslug !== '') {
    vuGames.loadCollections(userslug, userbggid, R.isEmpty(gameid), collectionsURL, boardgamesURL);
  };

  if (gameid !== '') {
    lookupGame(gameid);
  };

};
