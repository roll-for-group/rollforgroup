// buildAjaxObj :: {o} -> {p}
const buildAjaxObj = R.curry(function(key, data) {
  return R.zipObj(['type', 'url', 'dataType', 'data', 'cache'], ['PATCH', '/api/v1/groups/' + key, 'json', data, false])
});

// buildAjaxData :: k -> d -> {o}
const buildAjaxData = R.curry(function(email, key, keydata) {
  return R.zipObj(['email', key], [email, keydata]);
});


var vuGroups = new Vue({
  el: '#maincontent',
  data: {
    showSave: false,
    showEdit: false,

    showTitle: true,
    showSaveTitle: false,
    showEditTitle: false,
    oldTitle: false,

    showDescription: true,
    showSaveDescription: false,
    showEditDescription: false,
    oldDescription: false,

    showLocation: true,
    showSaveLocation: false,
    showEditLocation: false,
    oldLocation: '',

    name: 'Invalid Group',
    description: '',
    locname: '',
    encodeLocation: '',
    members: [],
    isprivate: true,
    bTypeaheadRendered: false

  },
  methods: {
    editTitle: function() {
      this.showTitle = false;
      this.showEditTitle = false;
      this.showSaveTitle = true;
      this.oldTitle = this.name;
    },
    saveTitle: function(useremail, key, title) {
      const buildAjaxDataObj = R.compose(
        buildAjaxObj(key), 
        buildAjaxData(useremail)
      );

      $.ajax(
        buildAjaxDataObj('name', title)
      ).done(function(x) {
        vuGroups.oldTitle = title; 
        vuGroups.showTitle = true;
        vuGroups.showEditTitle = true;
        vuGroups.showSaveTitle = false;

      }).fail(function(err) {
        window.location.href = '/login';

      });
    },    
    cancelTitle: function() {
      this.showTitle = true;
      this.showEditTitle = true;
      this.showSaveTitle = false;
    },
    countPlayers: function(matchplayers) {
      var alertify = function(data) {
        alert(JSON.stringify(data))
        return data;
      };
      return R.compose(
        R.length,
        R.filter(R.converge(R.or, [R.propEq('status', 'host'), R.propEq('status', 'approve')]))
        // alertify
      )(matchplayers);
    },
    friendlyStatus: function(status) {
      if (status == 'host') {
        return 'host';
      } else if (status == 'approve') {
        return '';
      } else if (status == 'invite') {
        return 'pending..';
      } else if (status == 'interested') {
        return 'maybe..';
      } else {
        return status;
      }
    },
    editDescription: function() {
      this.showDescription = false;
      this.showEditDescription = false;
      this.showSaveDescription = true;
      this.oldDescription = this.name;
    },
    saveDescription: function(useremail, key, description) {
      const buildAjaxDataObj = R.compose(
        buildAjaxObj(key), 
        buildAjaxData(useremail)
      );

      $.ajax(
        buildAjaxDataObj('description', description)
      ).done(function(x) {
        vuGroups.oldDescription = description; 
        vuGroups.showDescription = true;
        vuGroups.showEditDescription = true;
        vuGroups.showSaveDescription = false;

      }).fail(function(err) {
        window.location.href = '/login';

      });
    },    
    cancelSaveDescription: function() {
      this.showDescription = true;
      this.showEditDescription = true;
      this.showSaveDescription = false;
    },

    setIsPrivate: function(useremail, key, value) {
       const buildAjaxDataObj = R.compose(
        buildAjaxObj(key), 
        buildAjaxData(useremail)
      );

      $.ajax(
        buildAjaxDataObj('isprivate', value)
      ).done(function(x) {
        vuGroups.isprivate = value; 
      }).fail(function(err) {
        window.location.href = '/login';

      });
    },
    setLocation: function(locname) {
      this.locname = R.replace(/&amp;/, '&', locname);
      this.encodeLocation = "https://www.google.com/maps/embed/v1/place?q=" +
        R.replace(/&/g, '%26', R.replace(/#/g, '%23', encodeURI(locname))) +
        "&key=AIzaSyDZGpB9hMYMo9pAg6mydAR0PXYcUh-77QQ";
    },
    wrapChars: function(mustselect, before, after) {
      var editor = document.getElementById("textEditDescription");

      var selectionStart = 0, selectionEnd = 0;

      if (editor.selectionStart) selectionStart = editor.selectionStart;
      if (editor.selectionEnd) selectionEnd = editor.selectionEnd;

      if ((selectionStart != selectionEnd) || !mustselect) {
        var editorCharArray = vuGroups.description.split("");
        editorCharArray.splice(selectionEnd, 0, after);
        editorCharArray.splice(selectionStart, 0, before); //must do End first
        vuGroups.description = editorCharArray.join("");
      }

    },
    pressHeadDesc: function() {
      this.wrapChars(false, "\n## ", "\n");
    },
    pressBoldDesc: function() {
      this.wrapChars(true, "**", "**");
    },
    pressItalDesc: function() {
      this.wrapChars(true, "*", "*");
    },
    pressQuotDesc: function() {
      this.wrapChars(false, "\n> ", "\n\n");
    },
    pressListUlDesc: function() {
      this.wrapChars(false, "\n* ", "\n\n");
    },
    pressListOlDesc: function() {
      this.wrapChars(false, "\n1. ", "\n\n");
    },
    pressLinkDesc: function() {
      var url = prompt("What is the URL?");
      this.wrapChars(false, "[", "](http://" + url + ")");
    },
    pressImagDesc: function() {
      var url = prompt("What is the picture URL?");
      this.wrapChars(false, "![", "](http://" + url + ")");
    },
    pressYtubeDesc: function() {
      var url = prompt("What is the youtube video id?");
      if (url.length == 11) {
        this.wrapChars(false, "[![", "](https://img.youtube.com/vi/" + url + "/0.jpg)](https://www.youtube.com/watch?v=" + url + ")");
      } else {
        alert("A youtube id can only be 11 characters long. Copy the youtube video id from the end of any youtube video url");
      }
    },
    formatHelp: function() {
      window.open("http://commonmark.org/help/", "_blank");
    },
    renderDescription: function(desc) {

      const defaults = {
        html:         false,        // Enable HTML tags in source
        xhtmlOut:     false,        // Use '/' to close single tags (<br />)
        breaks:       true,        // Convert '\n' in paragraphs into <br>
        langPrefix:   'language-',  // CSS language prefix for fenced blocks
        linkify:      true,         // autoconvert URL-like texts to links
        typographer:  true,        // Enable smartypants and other sweet transforms
      };

      var mdHtml = window.markdownit(defaults).use(window.markdownitEmoji);

      return mdHtml.render(desc);
    },
    loadUser(members, userslug) {

      // isHost
      const isHost = R.compose(
        R.equals(1),
        R.length, 
        R.filter(R.__, members),
        R.propEq('pageslug')
      );

      this.showEdit = isHost(userslug);
      this.showEditTitle = isHost(userslug);
      this.showEditDescription = isHost(userslug);
      this.showEditLocation = isHost(userslug);

    },
    editVisible() {
    },
    editLocation() {

      this.showLocation = false;
      this.showEditLocation = false;
      this.showSaveLocation = true;
      this.oldLocation = this.locname;

      $('#playareadiv').show();

      if (!vuGroups.bTypeaheadRendered) {
        Vue.nextTick(function() { 
          setTimeout(function() {
            $('#playarea').typeahead(
              {
                hint: true,
                highlight: true,
                minLength: 1
              },
              {
                source: function(query, sync, process) 
                {
                  return $.ajax({
                    type: "GET",
                    url: "/api/v1/locations",
                    dataType: "json",
                    data: {search: query},
                    success: function(data) {
                      vuGroups.setLocation(R.head(R.map(R.path(['place','address']), data)));
                      return process(R.map(R.path(['place','address']), data));
                    }
                  });
                }
              }
            )
          });
        });

      } else {
        vuGroups.bTypeaheadRendered = true;
      }

      $('#playarea').focus();

    },
    saveLocation: function(useremail, key, location) {

      const notNil = R.compose(R.not, R.isNil);
      const notEmpty = R.compose(R.not, R.isEmpty);

      // buildAjaxDataObj :: (k -> d) -> {p}
      const buildAjaxDataObj = R.compose(
        buildAjaxObj(key), 
        buildAjaxData(useremail)
      );

      // getLocProp :: (p -> s) -> ([{d}] => l) 
      const getLocProp = function(propname, area) {
        return R.compose(R.prop(propname), chosenCity(area));
      };

      // chosenAddress :: (s) -> ({d} => Bool)
      const chosenAddress = function(area) {
        return R.compose(R.propEq('address', area), R.prop('place'));
      }

      // chosenCity :: (s) -> ([{d}] => {o})
      const chosenCity = function(area) {
        return R.compose(R.path(['place', 'coords']), R.find(chosenAddress(area)));
      };

      // cityFound :: s -> ([{d}] => bool)
      const cityFound = function(area) {
        return R.compose(notNil, chosenCity(area));
      };

      var ajaxLocation = $.ajax({
        type: "GET",
        url: "/api/v1/locations",
        dataType: "json",
        data: {search: $('#playarea').val()}
      });

      ajaxLocation.done(function(data) {
        if (($('#playarea').val().length > 0) && notNil(data) && (cityFound($('#playarea').val())(data))) {
          $.ajax(buildAjaxObj(key, {
            email: useremail,
            playarea: $('#playarea').val(),
            playarealat: getLocProp('lat', $('#playarea').val())(data),
            playarealng: getLocProp('lng', $('#playarea').val())(data)

          })).done(function(x) {
            vuGroups.setLocation($('#playarea').val());
            vuGroups.oldLocation = this.location; 
            vuGroups.showLocation = true;
            vuGroups.showEditLocation = true;
            vuGroups.showSaveLocation = false;
            $('#playareadiv').hide();

          }).fail(function(err) {
            window.location.href = '/login';

          });

        } else {
          alert('The location could not be geo-coded, please check the text matches the dropdown exactly');
          $('#playarea').focus();
        }
      });

      ajaxLocation.fail(function(data) {
        alert('The location could not be geolocated, please ensure the location is valid');
        $('#playarea').focus();
      });

    },    
    cancelLocation: function() {
      this.locname = this.oldLocation;
      this.showLocation = true;
      this.showEditLocation = true;
      this.showSaveLocation = false;
      $('#playareadiv').hide();
    },

  }
});


// setuppage :: id
var setuppage = function (groupkey, userslug) {

  $.ajax({
    type: "GET",
    url: "/api/v1/groups/" + groupkey,
    dataType: "json",
    data: {},
    cache: false
  }).done(function(x) {
    if (R.isNil(x)) {
      window.location.href = "/groups";
    } else {
      vuGroups.name = x.name;
      vuGroups.description = x.description;
      vuGroups.setLocation(R.replace(/&amp;/, '&', x.locname));
      vuGroups.members = x.members;
      vuGroups.isprivate = x.isprivate;
      vuGroups.loadUser(x.members, userslug);
    };
  });

};
