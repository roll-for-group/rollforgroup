var vuStage = new Vue({
  el: '.containerx',
  data: {
    name: '',
    age: '',
    visibility: 1
  },
  methods: {

    // initValues :: s
    initValues: function(username) {
      this.name = username;
    },
    verifyname: function(name) {
      return (!R.isNil(name) && R.trim(name).length > 2);
    },
    verifyage: function(age) {
      return (!isNaN(age) && (age >= 8) && (age < 120));
    },
    verifypublicage: function(age) {
      return (!isNaN(age) && (age >= 18) && (age < 120));
    },
    verifyvisible: function(visible, age) {
      if (visible == 1) {
        return this.verifypublicage(age);
      } else {
        return this.verifyage(age);
      }
    },
    textclass: function(valid) {
      if (valid) {
        return "form-control text";
      } else {
        return "form-control text alert alert-warning";
      }
    },
    submituser: function(email, userkey, name, age, visibility) {

      ajaxNext = $.ajax({
        type: "PATCH",
        url: "/api/v1/accounts/" + email,
        timeout: 5000,
        dataType: "json",
        data: {
          stage:        3,
          key:          userkey,
          name:         name,
          age:          age,
          visibility:   visibility
        },
        cache: false
      });

      ajaxNext.done(function(result) {
        $.LoadingOverlay("hide");
        window.location.href = '/stage4';
      });

      ajaxNext.fail(function(e, errortext) {
        $.LoadingOverlay("hide");
        alert(errortext);
      });

    },
    nextclick: function(email, userkey, name, age, visibility) {

      if (!this.verifyname(name)) {
        alert("Your name should be at least three characters long.");
        $('#name').focus();

      } else if (!this.verifyage(age)) {
        alert("Please enter a valid age, older than 8.");
        $('#age').focus();

      } else if (!this.verifyvisible(visibility, age)) {
        alert("You must be eighteen or over to have ap public profile on Roll for Group. Please change your profile to private.");
        $('#visiblity').focus();

      } else {

        // Show full page LoadingOverlay
        $.LoadingOverlay("show", {'z-index': 9999});
        vuStage.submituser(email, userkey, name, age, visibility);

      };
    }
  }
});
