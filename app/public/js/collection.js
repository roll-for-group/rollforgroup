new WOW().init();

var vuGameList = new Vue({
  el: '#maincontent',
  data: {
    name:         '',
    description:  '',
    // bggname:      '',
    bggtype:      '',
    bgguser:      '',
    showTitle:    true,
    showEdit:     true,
    showSave:     false,
    showDesc:     true,
    showEditDesc: true,
    showSaveDesc: false,
    showCreateList: false,
    showShare: false,
    showAdmin: false,
    showRandomGame: false,
    randomspin: true,
    activeListLoaded: false,
    updatespinclass: 'fa fa-refresh',
    loading: false,
    userslug: '',
    adminkey: '',
    gamesBase: [],
    gamesRandom: [],
    games: [],
    bgglisttext: 'Own (Default)',
    bgglistname: 'own',
    bgglistnumber: 1,
    viewcount: 0,
    showcollections: false,
    sortby: 'name',
    sortasc: true,
    collections: [],
    filterBestWith: '',
    filterPlays: '',
    filterMinTime: '',
    filterMaxTime: '',
    filterCpxLight: false,
    filterCpxMedLight: false,
    filterCpxMedium: false,
    filterCpxMedHeavy: false,
    filterCpxHeavy: false,
    errBestWith: false,
    errPlays: false,
    errMinTime: false,
    errMaxTime: false,
  },
  methods: {
    toggleFilterCpxLight: function() {
      vuGameList.filterCpxLight = !vuGameList.filterCpxLight;
      vuGameList.runFilter();
    },
    toggleFilterCpxMedLight: function() {
      vuGameList.filterCpxMedLight = !vuGameList.filterCpxMedLight;
      vuGameList.runFilter();
    },
    toggleFilterCpxMedium: function() {
      vuGameList.filterCpxMedium = !vuGameList.filterCpxMedium;
      vuGameList.runFilter();
    },
    toggleFilterCpxMedHeavy: function() {
      vuGameList.filterCpxMedHeavy = !vuGameList.filterCpxMedHeavy;
      vuGameList.runFilter();
    },
    toggleFilterCpxHeavy: function() {
      vuGameList.filterCpxHeavy = !vuGameList.filterCpxHeavy;
      vuGameList.runFilter();
    },
    runFilter: function() {

      // isInTime :: n -> n -> {o} -> bool
      var isInTime = R.curry(function(minMinutes, maxMinutes, arrValue) {
        var lowerBound = 0;
        var upperBound = 50000;

        vuGameList.errMinTime = isNaN(minMinutes) && (minMinutes != '');
        vuGameList.errMaxTime = isNaN(maxMinutes) && (maxMinutes != '');

        if (!isNaN(minMinutes) && (minMinutes !='')) {
          lowerBound = Number(minMinutes);
        };
        if (!isNaN(maxMinutes) && (maxMinutes !='')) {
          upperBound = Number(maxMinutes);
        };
        return ((upperBound >= arrValue.maxplaytime) && (arrValue.minplaytime >= lowerBound));

      });

      // isBestWith :: n -> {o} -> bool
      var isBestWith = R.curry(function(bestWith, arrValue) {
        vuGameList.errBestWith = isNaN(bestWith) && (bestWith != '');

        if (isNaN(bestWith) || (bestWith == '')) {
          return true;
        } else {
          var playerCount = R.find(R.propEq('playercount', bestWith), arrValue.playercounts);
          if (playerCount == undefined) {
            return false;
          } else {
            return playerCount.status == "Best";
          }
        }

      });


      // playsWith :: n -> {o} -> bool
      var playsWith = R.curry(function(playsWith, arrValue) {

        vuGameList.errPlays = isNaN(playsWith) && (playsWith != '');
        if (isNaN(playsWith) || (playsWith == '')) {
          return true;
        } else {
          return ((arrValue.minplayers <= playsWith) && (playsWith <= arrValue.maxplayers));
        }

      });


      // notLight :: {o} -> bool
      var notLight = function(arrValue) {
        return !(arrValue.weight <= 1.8) 
      };

      // notMedLight :: {o} -> bool
      var notMedLight = function(arrValue) {
        return !((arrValue.weight > 1.8) && (arrValue.weight <= 2.6)) 
      };

      // notMedium :: {o} -> bool
      var notMedium = function(arrValue) {
        return !((arrValue.weight > 2.6) && (arrValue.weight <= 3.4)) 
      };

      // notMedHeavy :: {o} -> bool
      var notMedHeavy = function(arrValue) {
        return !((arrValue.weight > 3.4) && (arrValue.weight <= 4.2)) 
      };

      // notHeavy :: {o} -> bool
      var notHeavy = function(arrValue) {
        return !(arrValue.weight > 4.2) 
      };

      // filterFx :: (n -> bool) -> [a] -> [a]
      var filterFx = R.curry(function(fx, arr) {
        return R.filter(fx, arr);
      }); 

      // filterCpx :: (n -> bool) -> bool -> [a] -> [a]
      var filterCpx = R.curry(function(fx, remove, arr) {
        if (remove) {
          return arr;
        } else {
          return R.filter(fx, arr);
        }
      }); 

      // filterCpx :: [a] -> [a]
      var filterCpx = R.compose(
        filterCpx(notHeavy, vuGameList.filterCpxHeavy),
        filterCpx(notMedHeavy, vuGameList.filterCpxMedHeavy),
        filterCpx(notMedium, vuGameList.filterCpxMedium),
        filterCpx(notMedLight, vuGameList.filterCpxMedLight),
        filterCpx(notLight, vuGameList.filterCpxLight)
      );

      // filterComplex :: [a] -> [a]
      var filterComplex = function(arr) {
        if (!vuGameList.filterCpxLight && !vuGameList.filterCpxMedLight && !vuGameList.filterCpxMedium && !vuGameList.filterCpxMedHeavy && !vuGameList.filterCpxHeavy) {
          return arr;
        } else {
          return filterCpx(arr);
        }
      };

      // filterAll :: [a] -> [a]
      var filterAll = R.compose( 
        vuGameList.sortGames(R.__, vuGameList.sortby, false),
        filterFx(isBestWith(vuGameList.filterBestWith)), 
        filterFx(playsWith(vuGameList.filterPlays)), 
        filterFx(isInTime(vuGameList.filterMinTime, vuGameList.filterMaxTime)),
        filterComplex
      );

      vuGameList.games = filterAll(vuGameList.gamesBase);

    },
    filterClass: function(filter) {
      if (filter) {
        return 'btn btn-info';
      } else {
        return 'btn btn-default';
      }
    },
    filterClassGreen: function(filter) {
      if (filter) {
        return 'btn btn-success';
      } else {
        return 'btn btn-default';
      }
    },
    filterClassOrange: function(filter) {
      if (filter) {
        return 'btn btn-warning';
      } else {
        return 'btn btn-default';
      }
    },
    changeSort: function(prop) {
      vuGameList.games = vuGameList.sortGames(vuGameList.games, prop, true);
    },
    sortGames: function(gamelist, prop, changedirection) {
      var numberfy = function(x) {
        return Number(x)
      };
      var sortByNameCaseInsensitive = R.sortBy(R.compose(R.toLower, R.prop('name')));
      var sortByNumber =              R.sortBy(R.compose(numberfy, R.prop(prop  )));

      if ((vuGameList.sortby == prop) && changedirection) {
        vuGameList.sortasc = !vuGameList.sortasc
      } else {
        vuGameList.sortby = prop;
      };
      if (prop == "name") {
        if (vuGameList.sortasc) {
          return sortByNameCaseInsensitive(gamelist);
        } else {
          return R.reverse(sortByNameCaseInsensitive(gamelist));
        }
      } else {
        if (vuGameList.sortasc) {
          return sortByNumber(gamelist);
        } else {
          return R.reverse(sortByNumber(gamelist));
        }
      }
    },
    getlength: function(col) {
      return col.length;
    },
    getCollectionUrl: function(collectionobj) {
      if (R.has('adminkey', collectionobj)) {
        return "collection?id=" + collectionobj.key + "&adminkey=" + collectionobj.adminkey;
      } else {
        return "collection?id=" + collectionobj.key;
      }
    },
    copy: function(id) {
      var copyText = document.getElementById(id).select();
      document.execCommand("Copy");
    },
    updatebgg: function(key) {
      vuGameList.loading = true;

      // buildAjaxObj :: {o} -> {p}
      const buildAjaxDataObj = function(data) {
        return R.zipObj(['type', 'url', 'dataType', 'data', 'cache', 'timeout'], ['PATCH', '/api/v1/collections/' + key + '/games' , 'json', data, false, 20000])
      };
      vuGameList.updatespinclass = 'fa fa-refresh fa-spin';

      $.ajax(
        buildAjaxDataObj({})
      ).done(function(x) {
        vuGameList.updatespinclass = 'fa fa-refresh';
        vuGameList.gamesBase = x; 
        vuGameList.runFilter();
        vuGameList.loading = false;
      }).fail(function(err) {
        vuGameList.loading = false;
        vuGameList.updatespinclass = 'fa fa-refresh';
        alert('List could not be updated from BGG, please try again later.');
      });

    },
    findRandomGame: function() {
      vuGameList.showRandomGame = true;
      vuGameList.randomspin = true;
      setTimeout(function() {
        vuGameList.randomspin = false; 
        if (vuGameList.games.length == 0) {
          alert("There are no games to select from.");
        } else {
          vuGameList.gamesRandom = R.of(vuGameList.games[
            Math.floor(Math.random() * (vuGameList.games.length))
          ]);
        }
      }, 1000)
    },
    decodeDescription: function(description) {
      return(description
        .replace(/&amp;quot;/g, '\"')
        .replace(/&amp;&amp;#35;10;/g, '<br/>')
        .replace(/&apos;/g, '\'')
        .replace(/&amp;&amp;#35;40;/g, '(')
        .replace(/&amp;&amp;#35;41;/g, ')')
        .replace(/&amp;ndash;/g, '-')
        .replace(/&amp;ecirc;/g, 'ê')
        .replace(/&amp;agrave;/g, 'à')
      );
    },
    shareVisible: function() {
      vuGameList.showShare = !vuGameList.showShare;
    },
    bggList:   function(listtext, listname, listnumber) {
      vuGameList.bgglisttext = listtext;
      vuGameList.bgglistname = listname;
      vuGameList.bgglistnumber = listnumber;
      $('#newBggName').focus();
    },
    validateText: function(text) {
      return (text !== '');
    },
    createList: function() {

      vuGameList.loading = true;
      $("#newlist :input").prop("disabled", true)

      // buildAjaxData :: k -> d -> {o}
      const buildAjaxData = function(name, description, thumbnail, bggname, bggtype, bgglistnumber) {
        if (vuGameList.userslug === '') {
          return R.zipObj(
            ['name', 'description', 'thumbnail', 'bggtype', 'bggname', 'bgglistnumber'], 
            [name, description, thumbnail, bggtype, bggname, bgglistnumber]
          );
        } else {
          return R.zipObj(
            ['userkey', 'name', 'description', 'thumbnail', 'bggtype', 'bggname', 'bgglistnumber'], 
            [vuGameList.userslug, name, description, thumbnail, bggtype, bggname, bgglistnumber]
          );
        }
      };

      // buildAjaxObj :: {o} -> {p}
      const buildAjaxObj = function(data) {
        return R.zipObj(
          ['type', 'url', 'dataType', 'data', 'cache'], 
          ['POST', 'api/v1/collections', 'json', data, false]
        )
      };

      const buildAjaxDataObj = R.compose(buildAjaxObj, buildAjaxData);

      var title = document.getElementById('newTitle').value;
      var description = document.getElementById('newDescription').value;
      // var bggname = document.getElementById('newBggName').value;

      if (vuGameList.validateText(title)) {

        $.ajax(
          buildAjaxDataObj(title, description, '', vuGameList.bgguser, vuGameList.bgglistname, vuGameList.bgglistnumber)
        ).done(function(x) {
          window.location.href = 'collection?id=' + x.key
            + '&adminkey=' + x.adminkey;

        }).fail(function(err) {
          $("#newlist :input").prop("disabled", false);
          vuGameList.loading = false;
          alert('List could not be created, check the bgg user exists and try again.');

        });

      } else {
        alert("Please enter a valid title for the list");
        $('#newTitle').focus();

      }

    },
    deleteThisList: function(key) {

      // buildAjaxData :: k -> d -> {o}
      const buildAjaxData = R.curry(function(adminkey, userslug) {
        if (vuGameList.userslug === '') {
          return R.zipObj(
            ['adminkey'], 
            [adminkey]
          );
        } else {
          return R.zipObj(
            ['userkey'], 
            [userslug]
          );
        }
      });

      // buildAjaxObj :: {o} -> {p}
      const buildAjaxObj = R.curry(function(key, data) {
        return R.zipObj(
          ['type', 'url', 'dataType', 'data', 'cache'], 
          ['DELETE', 'api/v1/collections/' + key, 'json', data, false]
        )
      });

      const buildAjaxDataObj = R.compose(buildAjaxObj(key), buildAjaxData(vuGameList.adminkey));

      $.ajax(
        buildAjaxDataObj(vuGameList.userslug)
      ).done(function(x) {
        alert('The list has been deleted.');
        window.location.reload();
      }).fail(function(err) {
        alert('There was a problem deleting the list.');
      });

    },
    editTitle: function() {
      vuGameList.showTitle = false;
      vuGameList.showEdit = false;
      vuGameList.showSave = true;
    },
    saveTitle: function(key) {

      // buildAjaxData :: k -> d -> {o}
      const buildAjaxData = function(key, keydata) {
        if (vuGameList.userslug === '') {
          return R.zipObj(
            ['adminkey', key], 
            [vuGameList.adminkey, keydata]
          );
        } else {
          return R.zipObj(
            ['userkey', key], 
            [vuGameList.userslug, keydata]
          );
        }
      };

      // buildAjaxObj :: {o} -> {p}
      const buildAjaxObj = R.curry(function(key, data) {
        return R.zipObj(
          ['type', 'url', 'dataType', 'data', 'cache'], 
          ['PATCH', 'api/v1/collections/' + key, 'json', data, false]
        )
      });

      const buildAjaxDataObj = R.compose(buildAjaxObj(key), buildAjaxData);

      $.ajax(
        buildAjaxDataObj('name', $('#textTitle').val())
      ).done(function(x) {
        vuGameList.name = $('#textTitle').val(); 
        vuGameList.showTitle = true;
        vuGameList.showEdit = true;
        vuGameList.showSave = false;
      }).fail(function(err) {
        alert(JSON.stringify(err));
        //window.location.href = '/login';
      });

    },
    cancelTitle: function() {
      vuGameList.showTitle = true;
      vuGameList.showEdit = true;
      vuGameList.showSave = false;
    },
    editDescription: function() {
      vuGameList.showDesc = false;
      vuGameList.showEditDesc = false;
      vuGameList.showSaveDesc = true;
    },
    saveDescription: function(collectionid) {

      // buildAjaxData :: k -> d -> {o}
      const buildAjaxData = function(key, keydata) {
        if (vuGameList.userslug === '') {
          return R.zipObj(
            ['adminkey', key], 
            [vuGameList.adminkey, keydata]
          );
        } else {
          return R.zipObj(
            ['userkey', key], 
            [vuGameList.userslug, keydata]
          );
        }
      };

      // buildAjaxObj :: {o} -> {p}
      const buildAjaxObj = function(data) {
        return R.zipObj(
          ['type', 'url', 'dataType', 'data', 'cache'], 
          ['PATCH', 'api/v1/collections/' + collectionid, 'json', data, false]
        )
      };

      const buildAjaxDataObj = R.compose(buildAjaxObj, buildAjaxData);

      $.ajax(
        buildAjaxDataObj('description', $('#textDescription').val())
      ).done(function(x) {
        vuGameList.description = $('#textDescription').val(); 
        vuGameList.showDesc = true;
        vuGameList.showEditDesc = true;
        vuGameList.showSaveDesc = false;
        setTimeout(function() { 
          $('#notes').html(mdHtml.render(vuGameList.description));
        });
      }).fail(function(err) {
        alert("Error saving description");
        //window.location.href = '/login';
      });

    },
    cancelDescription: function() {
      vuGameList.description = $('#textDescription').val(); 
      vuGameList.showDesc = true;
      vuGameList.showEditDesc = true;
      vuGameList.showSaveDesc = false;
      setTimeout(function() { 
        $('#notes').html(mdHtml.render(vuGameList.description));
      }, 100);
    },
    addNewList: function() {
      vuGameList.showCreateList = true;
    },
    cancelNew: function() {
      if (vuGameList.activeListLoaded) {
        vuGameList.showTitle = true;
        vuGameList.showEdit = true;
        vuGameList.showSave = false;
        vuGameList.showDesc = true;
        vuGameList.showSaveDesc = false;
        vuGameList.showCreateList = false;
        setTimeout(function() { 
          $('#notes').html(mdHtml.render(vuGameList.description));
          $('.loading-table').hide();
        }, 200);
      }
    },
    hideWhenUnplayed: function(plays) {
      if (plays == 0) {
        return '';
      } else {
        return plays;
      }
    },
    loadCollection: function(colid) {
      var ajaxCall = $.ajax({
        type: "GET",
        url: "api/v1/collections/" + colid,
        dataType: "json",
        data: {},
        cache: false
      });

      ajaxCall.done(function(x) {
        vuGameList.name = x.name;
        vuGameList.description = x.description;
        vuGameList.bgguser = x.bggname;
        vuGameList.bggtype = x.bggtype;
        vuGameList.viewcount = x.viewcount;

        var ajaxType = "GET";
        if (x.isbgg && !x.bgghassearched) {
          ajaxType = "PATCH";
          vuGameList.updatespinclass = 'fa fa-refresh fa-spin';
        };

        var ajaxGames = $.ajax({
          type: ajaxType,
          url: "/api/v1/collections/" + colid + '/games',
          dataType: "json",
          data: {},
          cache: false,
          timeout: 20000
        });

        $('#notes').html(mdHtml.render(x.description));
        setTimeout(function() { 
          vuGameList.showTooltips();
        }, 200);

        ajaxGames.done(function(x) {
          vuGameList.activeListLoaded = true;
          vuGameList.gamesBase = x; 
          // vuGameList.games = vuGameList.sortGames(x, vuGameList.sortby, false);
          vuGameList.runFilter();
          vuGameList.updatespinclass = 'fa fa-refresh';
          $('.loading-table').hide();
        });

        ajaxGames.fail(function(data) {
          // vuGameList.addNewList();
          vuGameList.activeListLoaded = true;
          vuGameList.updatespinclass = 'fa fa-refresh';
          $('.loading-table').hide();
        });

      });

      ajaxCall.fail(function(data) {
        vuGameList.addNewList();
      });

    },
    setupTypeAhead: function(collectionId, inpId, url) {

      const setupGameTypeahead = function(inpName) {
        var boardGames = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          remote: {
            url: url + '?name=%QUERY&expansions=true',
            wildcard: '%QUERY'
          }
        });

        $(inpName).typeahead({
          highlight: true,
          minlength: 2,
        }, {
          source:  boardGames,
          display: function(item) {
            return item.name;
          },
          limit: 15,
          templates: {
            suggestion: function(item) {
              return '<div>' + 
                '<table class="typeahead-table"><tr class="typeahead-row">' +
                '<td class="typeahead-gamebox"><img src="' + item.thumbnail + '" style="max-height: 48px;"></td>' +
                '<td class="addgametext"><span> ' + item.name + ' <small>(' + item.yearpublished + ')</small></span></td>' +
                '<td class="typeahead-players hide-small-screen"><i class = "fa fa-users"></i> <span>' + item.minplayers + '-' + item.maxplayers + '</span></td>' +
                '<td class="typeahead-time hide-small-screen"><i class = "fa fa-clock-o"></i> <span>' + item.minplaytime + '-' + item.maxplaytime + ' mins</span> <span class="' + vuGameList.classifyWeight(item.weight) + '" title="' + vuGameList.titleWeight(item.weight) + '">' + vuGameList.labelfyWeight(item.weight) + '</span></td>' +
                '</tr></table>' +
              '</div>';
            }
          } 
        });

        $(inpName).bind('typeahead:selected', function(obj, datum, name) {
          vuGameList.addGameToList(collectionId, datum.id);
          $(inpName).typeahead('val', '');
        })

      };
      setupGameTypeahead(inpId);

    },
    showTooltips: function() {
      $('.vutips').tooltip();
    },
    labelfyWeight: function(weight) {
      return (weight <= 1.8) 
        ? 'Light'
        : (weight <= 2.6)
          ? 'Med Light'
          : (weight <= 3.4)
            ? 'Medium'
            : (weight <= 4.2)
              ? 'Med Heavy'
              : 'Heavy'
    },
    classifyWeight: function(weight) {
      return (weight <= 1.8) 
        ? 'vutips label label-success'
        : (weight <= 2.6)
          ? 'vutips label label-success'
          : (weight <= 3.4)
            ? 'vutips label label-info'
            : (weight <= 4.2)
              ? 'vutips label label-warning'
              : 'vutips label label-warning'
    },
    titleWeight: function(weight) {
      return 'BGG Avg Weight: ' + weight
    },
    addGameToList: function(collectionid, gameid) {

      // TODO: purify function and combine them
      // buildAjaxData :: k -> d -> {o}
      const buildAjaxData = R.curry(function(key, keydata) {
        if (vuGameList.userslug === '') {
          return R.zipObj(
            ['adminkey', key], 
            [vuGameList.adminkey, keydata]
          );
        } else {
          return R.zipObj(
            ['userkey', key], 
            [vuGameList.userslug, keydata]
          );
        }
      });

      // buildAjaxObj :: {o} -> {p}
      const buildAjaxObj = R.curry(function(collecionid, data) {
        return R.zipObj(
          ['type', 'url', 'dataType', 'data', 'cache'], 
          ['POST', '/api/v1/collections/' + collectionid + '/games/', 'json', data, false]
        )
      });

      const buildAjaxDataObj = R.compose(buildAjaxObj(collectionid), buildAjaxData);

      $.ajax(
        buildAjaxDataObj('gameid', gameid)
      ).done(function(x) {
        vuGameList.gamesBase = x; 
        vuGameList.runFilter();
        $('#addgameid').val('');
        $('#addgameid').focus();
        setTimeout(function() { 
          $('#addgameimg').hide();
        }, 200);

      }).fail(function(err) {
        alert("Error adding game");
      });

    },
    removeGameFromList: function(collectionid, gameid) {

      // buildAjaxData :: k -> d -> {o}
      const buildAjaxData = function(key, keydata) {
        if (vuGameList.userslug === '') {
          return R.zipObj(
            ['adminkey', key], 
            [vuGameList.adminkey, keydata]
          );
        } else {
          return R.zipObj(
            ['userkey', key], 
            [vuGameList.userslug, keydata]
          );
        }
      };

      // buildAjaxObj :: {o} -> {p}
      const buildAjaxObj = R.curry(function(collecionid, gameid, data) {
        return R.zipObj(['type', 'url', 'dataType', 'data', 'cache'], ['DELETE', '/api/v1/collections/' + collectionid + '/games/' + gameid, 'json', data, false])
      });

      const buildAjaxDataObj = R.compose(buildAjaxObj(collectionid, gameid), buildAjaxData);

      $.ajax(
        buildAjaxDataObj()
      ).done(function(x) {
        vuGameList.gamesBase = x; 
        vuGameList.runFilter();
      }).fail(function(err) {
        alert(JSON.stringify(err));
      });

    },
    bgglink: function(id) {
      return '/games?gameid=' + id;
    },
    pillsClass: function(type) {
      return '';
    },
    loadCollections: function(userid) {
      var ajaxCollectionCall = $.ajax({
        type: "GET",
        url: "api/v1/collections",
        timeout: 10000,
        dataType: "json",
        data: {userid: userid},
        cache: false
      });

      ajaxCollectionCall.done(function(result) {
        vuGameList.collections = result;
        if (vuGameList.collections.length > 0 && (vuGameList.showCreateList)) {
          window.location.href = vuGameList.getCollectionUrl(vuGameList.collections[0]);
        }
      });

    },
    wrapChars: function(mustselect, before, after) {
      var editor;
      if (vuGameList.showSaveDesc) {
        editor = document.getElementById("textDescription");
      } else {
        editor = document.getElementById("newDescription");
      }

      editor.focus();
      setTimeout(function() {

        var editorHTML = editor.value;
        var selectionStart = 0, selectionEnd = 0;

        if (editor.selectionStart) selectionStart = editor.selectionStart;
        if (editor.selectionEnd) selectionEnd = editor.selectionEnd;

        if ((selectionStart != selectionEnd) || !mustselect) {
         
          var editorCharArray = editorHTML.split("");
          editorCharArray.splice(selectionEnd, 0, after);
          editorCharArray.splice(selectionStart, 0, before); //must do End first
          editorHTML = editorCharArray.join("");
          editor.value = editorHTML;

        }

      }, 100);
    },
    pressHeadDesc: function() {
      vuGameList.wrapChars(false, "\n## ", "\n");
    },
    pressBoldDesc: function() {
      vuGameList.wrapChars(true, "**", "**");
    },
    pressItalDesc: function() {
      vuGameList.wrapChars(true, "*", "*");
    },
    pressQuotDesc: function() {
      vuGameList.wrapChars(false, "\n> ", "\n\n");
    },
    pressListUlDesc: function() {
      vuGameList.wrapChars(false, "\n* ", "\n\n");
    },
    pressListOlDesc: function() {
      vuGameList.wrapChars(false, "\n1. ", "\n\n");
    },
    pressLinkDesc: function() {
      var url = prompt("What is the URL?");
      vuGameList.wrapChars(false, "[", "](http://" + url + ")");
    },
    pressImagDesc: function() {
      var url = prompt("What is the picture URL?");
      vuGameList.wrapChars(false, "![", "](http://" + url + ")");
    },
    pressYtubeDesc: function() {
      var url = prompt("What is the youtube video id?");
      if (url.length == 11) {
        vuGameList.wrapChars(false, "[![", "](https://img.youtube.com/vi/" + url + "/0.jpg)](https://www.youtube.com/watch?v=" + url + ")");
      } else {
        alert("A youtube id can only be 11 characters long. Copy the youtube video id from the end of any youtube video url");
      }
    },
    formatHelp: function() {
      window.open("http://commonmark.org/help/", "_blank");
    }
  },

});

var defaults = {
  html:         false,        // Enable HTML tags in source
  xhtmlOut:     false,        // Use '/' to close single tags (<br />)
  breaks:       true,        // Convert '\n' in paragraphs into <br>
  langPrefix:   'language-',  // CSS language prefix for fenced blocks
  linkify:      true,         // autoconvert URL-like texts to links
  typographer:  true,        // Enable smartypants and other sweet transforms
};
var mdHtml = window.markdownit(defaults).use(window.markdownitEmoji);

// setuppage :: id
var setuppage = function (colid, userslug, adminkey, bgguser, boardgamesURL) {

  vuGameList.loadCollection(colid);
  vuGameList.showAdmin = (adminkey !== '');
  vuGameList.adminkey = adminkey;
  vuGameList.userslug = userslug;
  // vuGameList.bgguser = bgguser;

  if (userslug !== '') {
    vuGameList.showcollections = true;
    vuGameList.loadCollections(userslug);
  };

  setTimeout(function() { 
    vuGameList.setupTypeAhead(colid, '#addgameid', boardgamesURL);
  }, 200);

  setTimeout(function() { 
    vuGameList.games = vuGameList.sortGames(vuGameList.games, 'name', false);
  }, 1000);

};

var addbggUser = function(buttonname, bggname) {
  $(buttonname).prop('disabled', true);
  $(buttonname + ' > i').removeClass('fa-add');
  $(buttonname + ' > i').addClass('fa-refresh');
  $(buttonname + ' > i').addClass('fa-spin');
};

var hostGame = function(id, labelName, isOwned) {
  window.location.href = "hostgame?" + labelName + "=" + id + "&isowned=" + isOwned;
};
