var vuSignup = new Vue({
  el: '.signup-form',
  data: {
    showpassword: false,
    whenCapsIsOn: false,
    showLogonError: false,
    errortext: '',
    formemail: '',
    formpassword: ''
  },
  methods: {
    getPasswordType: function(show) {
      return show 
        ? 'text' 
        : 'password'
    },
    detectCaps: function(e) {
      this.whenCapsIsOn = (e.getModifierState("CapsLock")); 
    },
    closeWindow: function() {
      window.location.href = "home";
    },
    submitSignupForm: function(e) {
      this.errortext = '';

      if (this.formemail == '') {
        $('#form-email').focus();
        this.errortext = this.errortext + 'Please enter your email address. ';
        this.showLogonError = true;
      };

      if (this.formpassword == '') {
        this.showLogonError = true;
        if (this.errortext == '')  {
          $('#form-password').focus();
        };
        this.errortext = this.errortext + 'Please enter a password. ';
      };

      if (this.errortext != '') {
        e.preventDefault();
      }
    }

  }
});
