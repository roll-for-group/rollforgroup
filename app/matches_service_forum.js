/**************************************************
**
** an ajax service for all things to do with the matches/forumposts 
**
*************************************************/

var express = require('express');
var bodyParser = require('body-parser')
var R = require('ramda');

var Promise = require('bluebird');
var log = require('./log.js');
var exports = module.exports = {};

var S = require('./lambda.js');

var account = require('./models/account.js');
var accounts = require('./models/accounts.js');

var match = require('./models/match.js');
var matches = require('./models/matches.js');

var moment    = require('moment');
var hash      = require('object-hash');

var Task = require('data.task');
var Maybe = require('data.maybe');
var Either = require('data.either');

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// postThread model m -> -> s -> s -> Promise Match({m})
const postThread = R.curry((matchSchema, matchkey, forum, post) => 
  matchSchema.findOneAndUpdate(
    {key: matchkey}, 
    {$push: R.objOf(forum, post)}, 
    {new:true}
  ).exec()
);
exports.postThread = postThread;

// hashify :: (k -> {o}) => {h}
const hashify = R.curry((key, obj) => 
  S.prop(key, obj)
    .map(hash)
    .map(R.objOf('hash'))
    .map(R.merge(obj))
    .getOrElse(obj)
);
exports.hashify = hashify;

// readAvatar :: [o] => n
const readUsername = R.compose(
  R.path(['profile', 'name']), 
  R.head
);

// readAvatar :: [o] => a
const readAvatar = R.compose(
  R.path(['profile', 'avataricon']), 
  R.head
);

// createPostObj :: (e -> c -> a -> u) => {p}
const createPostObj = R.curry((email, comment, avataricon, username) => R.zipObj(['useremail', 'comment', 'avataricon', 'username'], [email, comment, avataricon, username])); 

// createThreadObj :: s => {t}
const createThreadObj = (thread) => R.zipObj(['thread'], [thread]); 

// hashedThreadObj :: (e -> c -> a -> u) => {h}
const hashedThreadObj = R.compose(
	hashify('thread'), 
	createThreadObj, 
	createPostObj
);

// maybeKey :: obj => Maybe(key);
const maybeKey = (req, pathkey) => (S.path([pathkey, 'key'], req).isJust) 
  ? S.path([pathkey, 'key'], req) 
  : S.path(['user', 'key'], req); 
exports.MaybeKey = maybeKey;

// whenUploadLocaliseURL
const whenUploadLocaliseURL = (url) =>
  (R.startsWith('upload', url))
    ? '/' + url
    : url;
exports.whenUploadLocaliseURL = whenUploadLocaliseURL;

// maybeThreadAvatar :: {thread:{avataricon:a}} -> {avataricon:a} 
const maybeThreadAvatar = (y) => 
  R.merge(y, {
    avataricon: S
      .path(['thread', 'avataricon'], y)
      .map(whenUploadLocaliseURL)
      .getOrElse('/img/default-avatar-sq.jpg')
  }
);
exports.MaybeThreadAvatar = maybeThreadAvatar;

// getfromnow :: dt -> s
const getfromnow = (time) => moment(time).fromNow();

// maybeFromNow :: {thread:{avataricon:a}} -> {avataricon:a} 
const maybeFromNow = (y) => 
  R.merge(y, {
    fromnow: S
      .path(['thread', 'postdate'], y)
			.map(getfromnow)
      .getOrElse('unknown time')
  }
);
exports.maybeFromNow = maybeFromNow;

// getAvatar :: s -> s
const getAvatar = (avatar) => Maybe.of(avatar.getOrElse('/img/default-avatar-sq.jpg'));

// scrub :: mongoObj -> obj;
const scrub = R.compose(
  R.dissoc('_id'), 
  R.dissoc('__v'), 
  JSON.parse, 
  JSON.stringify
);
exports.Scrub = scrub;

// isNotNill :: a -> Bool 
const isNotNil = R.compose(R.not, R.isNil);

// liftVerifyUser :: (Maybe(u) -> Maybe(ky) -> Task(res.json) 
const liftVerifyUser = R.lift(accounts.TaskVerifyAnAccountFromEmail(account));

// allPlayersExcept :: e -> {o} => maybe [{p}]
const allPlayersExcept = R.curry((email, matchdata) => S
  .prop('players', matchdata)
  .map(R.reject(R.propEq('email', email)))
);
exports.allPlayersExcept = allPlayersExcept;

// getUserEmail :: {o} => s
const getUserEmail = (req) => S.path(['user', 'email'], req).getOrElse('');
exports.getUserEmail = getUserEmail;

// buildMatchURL :: s1 => s2
const buildMatchURL = (matchid) => '/events/' + matchid; 
exports.buildMatchURL = buildMatchURL;

// maybeMatch :: o -> Maybe a
const maybeMatch = S.path(['params', 'match']);
exports.MaybeMatch = maybeMatch;

// findMatchByKey :: schemaMatch -> String -> Number -> [Model(Match)]
const findMatchByKey = R.curry((matchSchema, matchkey) =>
  matchSchema
    .find({key: matchkey})
    .limit(100)
    .exec()
);
exports.FindMatchByKey = findMatchByKey;

// threadUserEmail :: {u} => s
const threadUserEmail = (y) => S.path(['thread', 'useremail'], y).getOrElse('');
exports.ThreadUserEmail = threadUserEmail;

// updateUserProp :: c -> s1 -> s2 -> s3 -> {u} => (f([{o}]) -> maybe({o}))
const updateUserProp = R.curry((txtClass, valStandard, valStaff, valPremium, userObj) => R.compose(
  R.map(R.objOf(txtClass)), 
  R.map(accounts.SafeWhenType(valStandard, valStaff, valPremium)), 
  S.find(R.propEq('email', threadUserEmail(userObj)))),
  R.filter(isNotNil)
);
exports.UpdateUserProp = updateUserProp;

// const updateForumStatus = (userlist) => R.compose(
const updateForumStatus = (userlist) => R.compose(
  R.map(R.map(updateUserClass(userlist))), 
  R.map(R.map(maybeFromNow)), 
  R.map(R.map(maybeThreadAvatar)), 
  R.map(R.map(scrub)), 
  S.prop('publicforums')
);
exports.UpdateForumStatus = updateForumStatus;

// updateUserClass :: [{u}] -> {o} => {s}
const updateUserClass = R.curry((playerlist, user) => {

  // updateUserImage  :: {u} -> [{o}] => maybe({o})
  const updateUserImage = (user, playerlist) => updateUserProp('imgclass', 'img-circle user-chat-avatar imglistcircle-standard imglistcircle', 'img-circle user-chat-avatar imglistcircle-staff imglistcircle', 'img-circle user-chat-avatar imglistcircle-premium imglistcircle', user)(playerlist).getOrElse({'imgclass': 'img-circle user-chat-avatar imglistcircle-standard imglistcircle'});

  // updateUserText   :: {u} -> [{o}] => maybe({o})
  const updateUserText = (user, playerlist) => updateUserProp('textclass', 'primary-font text-standard', 'primary-font text-staff', 'primary-font text-premium', user)(playerlist).getOrElse({'textclass' : 'primary-font text-standard'});

  // buildUser :: {u} => {p} 
  const buildUser = R.compose(
    R.merge(updateUserText(user, R.filter(isNotNil, playerlist))), 
    R.merge(updateUserImage(user, R.filter(isNotNil, playerlist)))
  );

  return buildUser(user);

});
exports.UpdateUserClass = updateUserClass;


// taskUpdatePlayers :: [{o}] => [Task({u})];
const taskLookupPlayers = R.compose(
  R.sequence(Task.of), 
  R.chain(R.map(accounts.TaskAccountFromEmail(account))), 
  R.map(R.uniq), 
  R.map(R.map(R.path(['thread', 'useremail']))), 
  R.chain(S.prop('publicforums')), 
  S.head 
);
exports.TaskLookupPlayers = taskLookupPlayers;


// newPublicMessages :: (Model(Match) -> String -> [Object]) =>  Promise([{t}]);
const newPublicMessages = R.curry(function(matchSchema, matchkey, hash) {

  // forum :: {o} -> {o}
  const forum = R.compose(
    R.head, 
    R.map(R.prop('publicforums'))
  );

  // unlesshash :: s -> s -> bool
  const unlesshash = R.curry((hash, thread) => (thread.hash != hash));

  return new Promise(function(fulfill, reject) {
    findMatchByKey(matchSchema, matchkey).then(function(m) {

      const pruneThreads = R.compose(R.drop(1), R.dropWhile(unlesshash(hash)), forum);
      fulfill(pruneThreads(m));

    });
  });
});
exports.NewPublicMessages = newPublicMessages;

// liftTaskNewMssages :: ({m} -> i -> a) => Promise([{n}])
const liftTaskNewMessages = R.lift((match, matchid, after) => 
  new Task((reject, resolve) => 
    newPublicMessages(match, matchid, after)
      .then(resolve)
      .catch(reject)
  )
);


// allPublicMessages :: (Model(Match) -> String -> [Object]) =>  Promise([{t}]);
const allPublicMessages = R.curry(function(matchSchema, matchkey) {
  return new Promise(function(fulfill, reject) {
    findMatchByKey(matchSchema, matchkey).then(function(m) {
      taskLookupPlayers(m).fork(
        err => log.clos('err', err), 
        data => {
          fulfill(updateForumStatus(data)(m[0]).getOrElse([]));
        }
      );
    });
  });
});
exports.AllPublicMessages = allPublicMessages;


// liftTaskAllMessages :: ({m} -> i) => Promise([{n}])
const liftTaskAllMessages = R.lift((match, matchid) => 
  new Task((reject, resolve) => 
    allPublicMessages(match, matchid)
      .then(resolve)
      .catch(reject)
  )
);

// liftComments :: {r} :: Maybe(Task([c]))
const liftComments = (req) => S.path(['query', 'after'], req).isJust 
  ? liftTaskNewMessages(Maybe.of(match), maybeMatch(req), S.path(['query', 'after'], req))
  : liftTaskAllMessages(Maybe.of(match), maybeMatch(req))


// publicForumPost :: Model(Account) -> Model(Match) -> String -> String -> comment -> Promise(match)
var publicForumPost = R.curry(function(accountSchema, matchSchema, useremail, userkey, matchkey, comment) {

  var errors = {
    'E401': {code: 401, message: 'Invalid user or key'},
    'E403': {code: 403, message: 'User does not have permissions'},
  }

  return new Promise(function(fulfill, reject) {
    accounts.ReadVerifyAccountFromEmail(accountSchema, useremail, userkey).then(function(user) {

      if (user.length == 0) {
        reject(errors.E401);
      } else {
        findMatchByKey(matchSchema, matchkey).then(function(m) {
          (m.length == 1) 
            ? postThread(matchSchema, matchkey, 'publicforums', hashedThreadObj(useremail, comment, readAvatar(user), readUsername(user))).then((x) => {
                taskLookupPlayers(m).fork(
                  err => log.clos('err', err), 
                  data => {
                    fulfill(updateForumStatus(data)(x).getOrElse([]));
                  }
                );
              }) 
            : reject(errors.E403);

        });
      }

    }).catch(function(err) {
      reject(err);

    });
  });
});
exports.PublicForumPost = publicForumPost;


// liftForumPost ::
const liftForumPost = R.lift((account, match, user, key, matchid, discussion) => 
  new Task((reject, resolve) => 
    publicForumPost(account, match, user, key, matchid, discussion)
      .then(resolve)
      .catch(reject)
  )
); 

// propNotEq :: s -> s -> o -> bool
const propNotEq = R.curry((propname, propval, obj) => R.compose(
  R.not,
  R.propEq(propname, propval)
)(obj));

// buildMessage :: s1 -> s2
const buildMessage = (playername, eventname, discussion) => playername + "@" + eventname + ": " + discussion; 

// liftNotify :: o -> s -> maybe task o 
const liftNotify = (account, text) => R.lift(accounts.TaskNotifyUser(account, text));

// notifyMatch :: o -> s -> s -> s -> [o] -> task o 
const notifyMatch = R.curry((account, text, avataricon, matchid, messageObj) => liftNotify(account, text)(S.prop('email', messageObj), getAvatar(avataricon), matchid.map(buildMatchURL), Maybe.of('forumpost'), Maybe.of(false)).getOrElse(taskFail()));

// notifyMatchUsers :: Maybe(s1) -> Maybe(s2) -> {o} => Task()
const notifyMatchUsers = R.curry((text, avataricon, matchid, messageObjs) => {
  return new Task((reject, resolve) => R.sequence(Task.of, R
    .filter(propNotEq('type', 'standby'), messageObjs)
    .map(notifyMatch(account, text, avataricon, matchid))
  ).fork(
    (err) => log.clos('err', err),
    (data) => resolve(data) 
  ));
});
exports.notifyMatchUsers = notifyMatchUsers;

// findMatch :: {o} => Task({m})
const findMatch = R.compose(
  R.chain(matches.TaskFindMatch(match)), 
  maybeMatch
);

// maybeThread :: o -> Maybe a
const maybeThread = S.path(['params', 'thread']);

// maybeEmail :: {o} -> maybe s
const maybeEmail = S.path(['body', 'user']);

// taskFindPostByKey model m -> s -> s -> s -> task Bool
const taskFindPostByKey = R.curry((matchSchema, matchkey, postuser, posthash) => 
  new Task((reject, resolve) => {

    // checkHash {o} -> maybe {o}
    const checkHash = R.compose(
      R.chain(S.whenPropEq('useremail', postuser)),
      R.chain(S.prop('thread')),
      R.chain(S.find(R.propEq('hash', posthash))),
      S.prop('publicforums')
    );

    matchSchema
      .findOne({key: matchkey})
      .exec()
      .then((data) => {
        resolve (checkHash(data).isJust)
      })
      .catch(reject)

  })
);
exports.taskFindPostByKey = taskFindPostByKey;

// taskDeletePost model m -> s -> s -> task Match({m})
const taskDeletePost = R.curry((matchSchema, matchkey, hash) => 
  new Task((reject, resolve) => {
    matchSchema
      .findOneAndUpdate(
        {key: matchkey}, 
        {$pull: R.objOf('publicforums', R.objOf('hash', hash))}, 
        {new:true}
      )
      .exec()
      .then(resolve)
      .catch(reject)
  })
);
exports.taskDeletePost = taskDeletePost;

// liftFindPost :: s -> s -> s -> task Bool
const liftFindPost = R.lift(taskFindPostByKey(match));

// liftDeletePost :: s -> s -> task Bool
const liftDeletePost = R.lift(taskDeletePost(match));

// cleanPublicForums :: {o} -> [{a}] 
const cleanPublicForums = R.compose(
  R.map(R.map(scrub)),
  S.prop('publicforums')
);

// setupRouter :: s -> o -> express 
var setupRouter = R.curry(function(emailSiteFrom, transporter, router) {

  // trimTo30chars :: s -> s
  const trimTo30chars = (a) => a.substr(0, 30); 

  // lengthGreaterThan30 :: s -> bool
  const lengthGreaterThan30 = (a) => a.length > 30

  // discussionTrimmed: o -> maybe s 
  const discussionTrimmed = R.compose(
    R.when(lengthGreaterThan30, R.map(trimTo30chars)),
    S.path(['body', 'discussion'])
  );

  var jsonParser = bodyParser.json();
  router.route('/:match/publicforums')
    .get(function(req, res) {
      liftComments(req).getOrElse(taskFail()).fork(
        (err) => res.sendStatus(404),
        (m) => {
					res.json(R.map(scrub, m))
				}
      )
    })
    .post(jsonParser, function(req, res) {
      liftVerifyUser(S.path(['body', 'user'], req), maybeKey(req, 'body')).getOrElse(taskFail()).fork(
        (err) => res.sendStatus(401),
        (user) => liftForumPost(Maybe.of(account), Maybe.of(match), S.path(['body', 'user'], req), maybeKey(req, 'body'), maybeMatch(req), S.path(['body', 'discussion'], req))
          .getOrElse(taskFail)
          .fork(
            (err) => res.sendStatus(err.code),
            (discussion) => findMatch(req).fork(
              (err) => res.sendStatus(err.code),
              (data) => allPlayersExcept(getUserEmail(req), data)
              .chain(
                notifyMatchUsers(
                  buildMessage(
                    S.path(['user', 'profile', 'name'], req).getOrElse('A player'),
                    S.prop('title', data).getOrElse('🎲'),
                    discussionTrimmed(req).getOrElse('🙂')
                  ),
                  S.path(['user', 'profile', 'avataricon'], req), 
                  maybeMatch(req))
              ).fork(
                (err) => res.sendStatus(err.code),
                (notice) => res.json(R.map(scrub, discussion))
              )
            )
          )
      )
    }); 

  router.route('/:match/publicforums/:thread')
    .delete(jsonParser, function(req, res) {
      liftVerifyUser(S.path(['body', 'user'], req), maybeKey(req, 'body')).getOrElse(taskFail()).fork(
        (err) => res.sendStatus(401),
        (user) => {
          liftFindPost(maybeMatch(req), maybeEmail(req), maybeThread(req)).getOrElse(taskFail()).fork(
            (err) => res.sendStatus(403),
            (data) => {
              (data)
                ? liftDeletePost(maybeMatch(req), maybeThread(req)).getOrElse(taskFail()).fork(
                  (err) =>  res.sendStatus(403),
                  (data) => res.json(cleanPublicForums(data).getOrElse({})))
                : res.sendStatus(403)
            }
          )
        }
      );
    }); 

  return router;

}); 
exports.SetupRouter = setupRouter;
