/**************************************************
**
** an ajax service for all things to do with the account
**
** todo: seperate into microservice
** documentation as api
**
*************************************************/

var express = require('express');

var R = require('ramda');
var Promise = require('bluebird');
var log = require('./log.js');
var S = require('./lambda.js');
var exports = module.exports = {};

// google map location services
var googleMapsClient;

// containsCountry :: s -> o -> s
const containsType = R.curry((type, obj) => R.compose(
  R.contains(type),
  R.prop('types')
)(obj));
exports.containsType = containsType;

// findTypeName s -> o -> maybe s
const findTypeName = R.curry((type, obj) => R.compose(
  R.prop('long_name'),
  R.find(containsType(type)),
  R.prop('address_components')
)(obj));
exports.findTypeName = findTypeName;

// mapToResults :: f o -> o
const mapToResults = R.curry((fx, o) => {
  return S.path(['json', 'results'], o).cata({
    Just:   (a) => {return R.map(fx, a)},
    Nothing: () => {return ""}
  });
});
exports.mapToResults = mapToResults;

const getAddress = R.prop('formatted_address');
const getCoords = R.path(['geometry','location']);

// buildResponseObj :: [s] -> [o]
const buildResponseObj = (response) => R.compose( 
    R.map(R.objOf('place')),
    R.zipWith(R.assoc('country'), mapToResults(findTypeName('country'), response)),
    R.zipWith(R.assoc('locality'), mapToResults(findTypeName('locality'), response)),
    R.zipWith(R.assoc('coords'), mapToResults(getCoords, response)),
    R.map(R.assoc('address', R.__, {})),
    mapToResults(getAddress)
  )(response);
exports.buildResponseObj = buildResponseObj;
 
// lookupAddress :: String -> promise [o]
var lookupAddress = function(address) {
	return new Promise(function (fulfill, reject) {
    googleMapsClient.geocode({
      address: address
    }, function(err, response){
      if (!err) {
        fulfill(buildResponseObj(response));
      } else {
        reject(err);
      }

    })
  });
};
exports.lookupAddress = lookupAddress;


// lookupCoords :: String -> promise [o]
var lookupCoords = function(lat, lng) {
	return new Promise(function (fulfill, reject) {
    googleMapsClient.reverseGeocode({
      latlng: parseFloat(lat) + "," + parseFloat(lng)
    }, function(err, response){
      if (!err) {
        fulfill(buildResponseObj(response));
      } else {
        reject(err);
      }
    })
  });
};
exports.lookupCoords = lookupCoords;


// setupAccountsPostListener :: express 
var setupRouter = function(mapskey) {

  var router = express.Router();

  // google map location services
  googleMapsClient = require('@google/maps').createClient({
    key: mapskey 
  });

  const liftLookupAddress = R.lift(lookupAddress);
  const liftLookupCoords = R.lift(lookupCoords);

  router.route('/')
    .get(function(req, res) {
      liftLookupAddress(S.path(['query', 'search'], req)).cata({
        Just: (promiseAddress)=> promiseAddress
          .then((x) =>res.send(x))
          .catch(() =>res.sendStatus(500)),
        Nothing:()=> {
          liftLookupCoords(S.path(['query', 'lat'], req), S.path(['query', 'lng'], req)).cata({
            Just: (promiseData)=> promiseData
              .then((x) =>res.send(x))
              .catch(() =>res.sendStatus(500)),
            Nothing:()=>res.sendStatus(500)
          });
        }
      });

    })

  return router;

}; 
exports.SetupRouter = setupRouter;
