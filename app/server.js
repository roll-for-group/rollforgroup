var session = require('express-session');
var schedule = require('node-schedule'); 
var R = require('ramda');

var lift = R.lift;

var passport = require('passport');

var nconf = require('nconf');
nconf.argv().env().file({file: 'config.json'});

var Promise = require('bluebird');
var S         = require('./lambda.js');

var Account = require('./models/account.js');
var Accounts = require('./models/accounts.js');

var login = require('connect-ensure-login');

var Group = require('./models/group.js');
var Groups = require('./models/groups.js');

var Match = require('./models/match.js');
var Matches = require('./models/matches.js');

var Achievement = require('./models/achievement.js');
var Achievements = require('./models/achievements.js');

var Transaction = require('./models/transaction.js');

var GoogleStrategy = require('passport-google-oauth20').Strategy;
var FacebookStrategy = require('passport-facebook');
var facebook = require('./facebook.js')

var emailer = require('./emailer.js');
var sitemap = require('./sitemap.js');

// setup sending email
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport(emailer.SMTPSettings(nconf));

var log = require('./log.js');

// setup webpages
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var expressHelper = require('./expresshelper');

var accountsAjax = require('./accounts_service.js');
var locationsAjax = require('./locations_service.js');
var adminAjax = require('./admin_service.js');
var matchesAjax = require('./matches_service.js');
var feedbackAjax = require('./feedback_service.js');
var uploadAjax = require('./upload_service.js');

var favicon = require('serve-favicon');
var exports = module.exports = {};

var buygems_page = require('./buygems_page.js');
var buymembership_page = require('./buymembership_page.js');
var login_page = require('./login_page.js');
var hostgame_page = require('./hostgame_page.js');
var events_page = require('./events_page.js');
var reoccurevents_page = require('./reoccurevents_page.js');

var findmatch_page = require('./findmatch_page.js');
var matchlist_page = require('./matchlist_page.js');
var purchase_page = require('./purchase_page.js');
var profile_page = require('./profile_page.js');
var unsubscribe_page = require('./unsubscribe_page.js');
var users_page = require('./users_page.js');
var games_page = require('./games_page.js');
var players_page = require('./players_page.js');
var admin_page = require('./admin_page.js');
var lounge_page = require('./lounge_page.js');
var eventsthankyou_page = require('./eventsthankyou_page.js');
var giftthankyou_page = require('./giftthankyou_page.js');
var buygemsthankyou_page = require('./buygemsthankyou_page.js');
var buymembershipthankyou_page = require('./buymembershipthankyou_page.js');
// var eventslibrary_page = require('./eventslibrary_page.js');
var stage_page = require('./stage_page.js');
var mail_page = require('./mail_page.js');

var group_page = require('./group_page.js');
var groups_page = require('./groups_page.js');
var groupsnew_page = require('./groupsnew_page.js');

var sh = require('./sockethelper.js');

var mongo = require('./mongoconnect.js');

const mongoStore = require('connect-mongo')(session);
const redisStore = require('connect-redis')(session);

/*
var awsClient = amazon.createClient({
  awsId: nconf.get('amazon:awsid'), 
  awsSecret: nconf.get('amazon:awssecret'), 
  awsTag: nconf.get('amazon:awstag')
});
*/

var dbUri = nconf.get('database:uri');
mongo.ConnectDatabase(dbUri);

login_page.SetupPassportStrategies(passport, Account);

expressHelper.SetupWebpages(
  app, 
  passport, 
  (nconf.get('session:type') == "redis")
    ? new redisStore({url: nconf.get('session:uri')})
    : new mongoStore({url: nconf.get('session:uri')})
);

// session config for redis:
/* 
{
  session: {
    "type": "redis",
    "uri": "localhost"
  }
}
*/

expressHelper.StaticRouteFolders(app, ["img", "css", "js", "vue", "fonts", "font-awesome", "assets", "less", "upload", "docs"]);

// slashString :: String -> String
var slashString = (endpoint) => "/" + endpoint;

const Maybe     = require('data.maybe')
const Task      = require('data.task')

// ============== URL ROUTES =================

// routeGets :: (s -> t) => render
var routeGets = expressHelper.routeGetRequests(app);

// routeGetsExt :: (s -> t -> l -> d) => render
var routeGetsExt = expressHelper.routeGetRequestExtended(app);

// routeGetIfLoggedInWithData :: Express -> EnsureLoggedIn -> String -> String ->  a
var routeGetIfLoggedInWithData = R.curry(function (expressApp, CheckLogin, urlFail, pageData, url) {

  expressApp.get(slashString(url), CheckLogin.ensureLoggedIn(slashString(urlFail)), function(req, res) {
		var data = pageData;
      data.user = expressHelper.LookupUsername(req.user);
		res.render(url, data);
	});
	return 0;

});

// routeGetWhenLoggedIn :: String -> a
var routeGetsWhenLoggedIn = routeGetIfLoggedInWithData(app, login, 'login', {});

app.get('/', login.ensureLoggedIn('/home'), function(req, res) {

  S.path(['user', 'email'], req).cata({
    Nothing:  ()=>{},
    Just:     email => Accounts.TaskRecordLogin(Account, email).fork(()=>{}, ()=>{})
  });

  var stage = "/stage1";
  if (req.user.hasOwnProperty("profile")) {
    if (req.user.profile.hasOwnProperty("stage")) {
      stage = stage_page.NextStagePage(req.user.profile.stage);
    }
  }

  // defaultStage :: s1 => s2
  const defaultstage = (page) => (page == '/') 
    ? stage 
    : page;

  const redirect = R.curry((obj, page) => obj.redirect(page));

  redirect(res, S.path(['session', 'returnTo'], req)
    .map(defaultstage)
    .getOrElse(stage));

});

app.use(function (req, res, next) {
  if ('/robots.txt' == req.url) {
    res.type('text/plain')
    res.send("User-agent: *\nDisallow:\nSitemap: https://www.rollforgroup.com/sitemap.xml");
  } else {
    next();
  }
});

app.set('trust proxy', true);
uploadAjax.RouteUploads(Account, nconf, app);

routeGetsExt('/home', 'home', 'Get your boardgame group rolling.', 'Roll for Group is a board game event organisation and board gamer meet up site, dedicated to helping players find and join local organised board game nights to play board games.');

routeGetsExt('/homerpg', 'homerpg', 'Play roleplay games and meet up with tabletop gamers on Roll for Group', 'Roll for Group is a tabletop gamer meet up site dedicated to players looking to find organise either boardgame or roleplay sessions.');

routeGetsExt('/privacy', 'privacy', 'Terms and condtions for the Roll for Group web site.', 'The terms and conditions for signing up and attending board game event meet ups organised on the Roll for Group website.');

routeGetsExt('/about', 'about', 'The mission and vision for Roll for Group.', 'To make board gaming an accessible and enjoyable experience for everyone. By embracing accessibility, community and fun.');

["gameplays", "acheivements", "account"].map(routeGetsWhenLoggedIn);

matchlist_page.RouteMatchLists(app, login, Account, nconf);
findmatch_page.RouteGamelist(app, Account, nconf);
admin_page.RouteAdminList(app, login, Account);
players_page.RoutePlayerList(app, login, Account);
lounge_page.RoutePlayerList(app, login, Account);
group_page.routeGroup(app, login);
groups_page.routeNewGroupPage(app, login);
groupsnew_page.routeNewGroupPage(app, login);

mail_page.RouteGetCollection(app, login, Accounts, Account, 'mail');

hostgame_page.RouteHostgame(app, login, Match, Account);
eventsthankyou_page.RouteEventsThankyou(app, login, Match, Account);
giftthankyou_page.RouteGiftThankyou(app, login, Match, Account);
buygemsthankyou_page.RouteBuyGemsThankyou(app, login, Match, Account);
buymembershipthankyou_page.RouteBuyMembershipThankyou(app, login, Match, Account);
events_page.Routegame(app, login, Match, Account);
reoccurevents_page.Routegame(app, login, Match, Account);
purchase_page.RoutePurchase(app, login);
buygems_page.RouteGems(app, login);
buymembership_page.RouteMembership(app, login);
profile_page.RouteProfile(app, login);
unsubscribe_page.RouteProfile(app, login);
users_page.RouteUser(app, login);
games_page.RouteUser(app, login);
stage_page.RouteStage(app, login);

login_page.RouteLogins(app, passport);
login_page.RouteVerify(app, Account);
login_page.GetReset(app, Account);
login_page.PostForgot(app, Account, nconf.get('site:url'));
login_page.PostLogin(app, passport);
login_page.PostReset(app, Account, transporter, "Game Knight <" + nconf.get("smtp:from") + ">", nconf.get('site:url'));

var accountsrouter = accountsAjax.SetupRouter(nconf, Account, Transaction);
accountsrouter = Achievements.SetupRouter(accountsrouter);
app.use('/api/v1/accounts', accountsrouter);

var adminrouter = adminAjax.SetupRouter();
app.use('/api/v1/admin', adminrouter);
 
var locationsrouter = locationsAjax.SetupRouter(nconf.get('googlemapsclient:key'));
app.use('/api/v1/locations', locationsrouter);

var activateTwitter = nconf.get("twitterbot:active");
var matchesrouter = matchesAjax.SetupRouter(nconf.get('smtp:from'), transporter, {}, activateTwitter);
app.use('/api/v1/matches', matchesrouter);

var feedbackrouter = feedbackAjax.SetupRouter();
app.use('/api/v1/feedbacks', feedbackrouter);

var grouprouter = Groups.SetupRouter();
app.use('/api/v1/groups', grouprouter);


app.get('/logout', function(req, res) {
	req.logout();
	res.redirect('/');
});

// sendSMTPmail :: String -> String -> String -> (Promise -> info)
app.post('/signup', function(req, res) {

  if (
    S.path(['body', 'formemail'], req).isJust && 
    S.path(['body', 'formpassword'], req).isJust
  ) {
    Accounts.registerAccount(Account, req.body.formemail, req.body.formpassword)
      .then(emailToken, signupError)
      .then(res.redirect('/email-verification' + '?email=' + S.path(['body', 'formemail'], req).get()))
  } else {
    res.redirect('/signup')
  }
  
});

sitemap.RouteSiteMap(app);

// signupError :: err
var signupError = function(err) {
	return new Promise(function (fulfill, reject) {
		reject(err);
	})
};

var sendPlayMail = emailer.SendSMTPmail(transporter, "Game Knight <" + nconf.get("smtp:from") + ">");

// sendToken :: (Promise -> info)
var emailToken = function(account) {
	var authenticationURL = nconf.get('site:url') +  '/verify?authToken=' + account.authToken;
	sendPlayMail(account.email, "You are nearly ready to play more board games",'"Please confirm your email address: <a target=_blank href=\"' + authenticationURL + '\">Confirm your email</a>');
};


// ============== PASSPORT STRATEGIES ===============
if ((nconf.get('facebook:clientID') !== undefined) && (nconf.get('facebook:clientID') !== '')) {
	passport.use(new FacebookStrategy({
			clientID: 			nconf.get('facebook:clientID'),
			clientSecret: 	nconf.get('facebook:clientSecret'),
			callbackURL: 		nconf.get('site:url') + '/login/facebook/return',
			profileFields: 	['id', 'emails', 'name', 'picture.type(large)']
		},
		function(accessToken, refreshToken, profile, done){
			facebook.facebookLogin(Account, accessToken, profile).then(
				function(User){done(null, User)},
				done
			);
		}
	));
}

if ((nconf.get('googleauth:clientID') !== undefined) && (nconf.get('googleauth:clientID') !== '')) {
	passport.use(new GoogleStrategy({
			clientID:     nconf.get('googleauth:clientID'),
			clientSecret: nconf.get('googleauth:clientSecret'),
			callbackURL: nconf.get('site:url') + "/auth/google/callback",
			passReqToCallback   : true
		},
		function(request, accessToken, refreshToken, profile, done) {
			facebook.googleLogin(Account, accessToken, profile).then(
				function(User){done(null, User)},
				done
			);
		}
	));
}


// schedule
schedule.scheduleJob('0 * * * * *', function(){

  Matches.TaskPlayMatch(Match).fork(()=> {}, ()=> {}); 
  Matches.taskFinaliseMatch(Match, Account).fork(()=> {}, ()=> {}); 
  Accounts.unPremiumMemberships(Account).fork(()=> {}, ()=> {});

});

schedule.scheduleJob('0 0 * * *', function(){

	Accounts.checkNotificationCount(Account).fork(()=> {}, ()=> {});

	Matches.findEventReminderEmails(Match, 'a few hours', 3).fork(()=> {}, ()=> {});
	Matches.findEventReminderEmails(Match, '24 hours', 24).fork(()=> {}, ()=> {});
	Matches.findEventReminderEmails(Match, '2 days', 48).fork(()=> {}, ()=> {});
	Matches.findEventReminderEmails(Match, '3 days', 72).fork(()=> {}, ()=> {});
	Matches.findEventReminderEmails(Match, '5 days', 120).fork(()=> {}, ()=> {});
	Matches.findEventReminderEmails(Match, '8 days', 192).fork(()=> {}, ()=> {});
	Matches.findEventReminderEmails(Match, 'less than 2 weeks', 312).fork(()=> {}, ()=> {});

});


// error 404
app.use(function(req, res, next){

  res.status(404);

  // respond with html page
  if (req.accepts('html')) {
    res.render('error404', { url: req.url });
    return;
  }

  // respond with json
  if (req.accepts('json')) {
    res.send({ error: 'Not found' });
    return;
  }

  // default to plain-text. send()
  res.type('txt').send('Not found');

});


var iomessages      = [];
var ioconnections   = [];

io.on('connection', function (socket) {

  // emitConnections :: [{u}] => io.emit([{u}])
  const emitData = (data) => io.emit('citychatadmin', data);

  // emitConnections :: [{u}] => io.emit([{u}])
  const emitConnections = R.compose(emitData, sh.BuildChatAdminObj, sh.UniqueUserList);

  socket.on('disconnect', function(){
    ioconnections = sh.RemoveUser(ioconnections, socket.id);
    emitConnections(ioconnections)
  });

  // lookupUser :: {u} => Task(Model({u}))
  const lookupUser = R.compose(R.chain(Accounts.TaskAccountFromEmail(Account)), S.prop('user'));
 
  if (iomessages.length > 250) 
    iomessages = R.drop((iomessages.length - 250), iomessages);

  var i;

  for (i = 0; i < iomessages.length; i++) { 
    io.to(socket.id).emit('citychat', iomessages[i]);
  } 

  emitConnections(ioconnections)

  socket.on('citychat', function(data){
    lookupUser(data).fork(
      err => log.clos('err', err),
      user => {
        io.emit('citychat', lift(sh.BuildMsgPayload(user))(S.prop('discussion', data)).getOrElse({}));
        iomessages = R.append(lift(sh.BuildMsgPayload(user))(S.prop('discussion', data)).getOrElse({}), iomessages); 
      }
    )
  });

  socket.on('citychatadmin', function(data){
    lookupUser(data).fork(
      err => log.clos('err', err),            
      user => {

        // addIoUser :: [{i}] -> s -> {d} -> {u} => Maybe([{o}])
        const addIoUser = (ioconnections, socket, data, user) => R.propEq('status', 'connect', data) 
          ? R.compose(R.map(R.append(R.__, ioconnections)), sh.SafeBuildUser)(socket, user) 
          : Maybe.Nothing();
        
        if (S.path(['profile', 'visibility'], user).getOrElse(1) == 1)
          ioconnections = addIoUser(ioconnections, socket.id, data, user).getOrElse(ioconnections);

        emitConnections(ioconnections)

      }
    )
  });

});


// ============== START SERVER ===============
http.listen(3000, function(){
  console.log('listening on *:3000');
});


// ============== END SERVER FOR TESTS =======
const closeServer = function() {
  console.log("stopping webserver...");
  http.close(function() {

    console.log("stopping mongoose now...");
    mongo.CloseDatabase();

  });
};

exports.closeServer = closeServer;

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', closeServer);
