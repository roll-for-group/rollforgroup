var account = require('../app/models/account.js');
var accounts = require('../app/models/accounts.js');
var match = require('../app/models/match.js');
var matches = require('../app/models/matches.js');

var Promise = require('bluebird');
var R = require('ramda');
var assoc   = R.assoc,
    compose = R.compose, 
    converge = R.converge,
    curry   = R.curry, 
    dissoc  = R.dissoc,
    empty   = R.empty,
    equals  = R.equals, 
    evolve  = R.evolve,
    has     = R.has,
    hasIn   = R.hasIn,
    head    = R.head,
    isEmpty = R.isEmpty,
    isNil   = R.isNil,
    findIndex = R.findIndex,
    last    = R.last,
    map     = R.map,
    merge   = R.merge,
    objOf   = R.objOf,
    or      = R.or,
    not     = R.not,
    path    = R.path,
    pipe    = R.pipe,
    prop    = R.prop,
    propEq  = R.propEq,
    unless  = R.unless,
    zipObj  = R.zipObj;
var util = require('util');
var log = require('./log.js');
var expressHelper = require('./expresshelper');

var md = require('markdown-it')({linkify:true});

const Maybe     = require('data.maybe')
const Either    = require('data.either')
const Task      = require('data.task')

var S = require('./lambda.js');

var moment = require('moment');

// renderData :: Render -> String -> Object -> Render 
var renderData = R.curry((res, template, data) => res.render(template, data));

// maybebuildNavs :: {o} => {m} 
const maybeBuildNavs = (req) => S.path(['user', 'email'], req).map(expressHelper.BuildNavbar('')).map(R.objOf('navbarul'));


// routeBuyMembershipThankyou :: Express -> Object -> Model -> Schema -> Schema -> 0
var routeBuyMembershipThankyou = function (expressApp, CheckLogin, match, account) {

	var url = "buymembershipthankyou";
	var urlFail = "login";
    var useremail = '';

	var data = {};
    var slashString = (file) => '/' + file;

	expressApp.get(slashString(url), CheckLogin.ensureLoggedIn(slashString(urlFail)), function(req, res) {

       var sendCleanedPage = curry((res, url, navbars) => compose(renderData(res, url), merge(maybeBuildNavs(req).getOrElse({})))(navbars));
    
       var email = R.path(['user', 'email'], req);

       accounts.BuildNavbars(account, email).then(sendCleanedPage(res, url));

	});
};

exports.RouteBuyMembershipThankyou = routeBuyMembershipThankyou;
