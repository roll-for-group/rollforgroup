/**************************************************
**
** an ajax service for all things to do with the matches 
**
** todo: seperate into microservice
** documentation as api
**
*************************************************/

var express = require('express');
var bodyParser = require('body-parser');

var R = require('ramda');

var append  = R.append,
    curry   = R.curry,
    dissoc  = R.dissoc,
    has     = R.has,
    isNil   = R.isNil,
    lift    = R.lift,
    map     = R.map,
    merge   = R.merge,
    of      = R.of,
    path    = R.path,
    pick    = R.pick,
    prop    = R.prop;

var Promise = require('bluebird');
var log = require('./log.js');
var exports = module.exports = {};

var S = require('./lambda.js');

var account = require('./models/account.js');
var accounts = require('./models/accounts.js');

var match             = require('./models/match.js');
var matches           = require('./models/matches.js');
var serviceForum      = require('./matches_service_forum.js');
var serviceTimeslots  = require('./matches_service_timeslots.js');

var emailer = require('./emailer.js');

var Task = require('data.task');
var Maybe = require('data.maybe');

const userButtons = {
  'invite':   [{
    btnclass:'btn btn-warning btn-sm',
    btnfaicon:'fa fa-ban',
    btntext:'Remove',
  }],
  'request':  [{
    btnclass:'btn btn-success btn-sm',
    btnfaicon:'fa fa-check-square-o',
    btntext:'Approve',
  },{
    btnclass:'btn btn-default btn-sm',
    btnfaicon:'fa fa-times',
    btntext:'Deny',
  }],
	'interested': [{
    btnclass:'btn btn-info btn-sm',
    btnfaicon:'fa fa-envelope',
    btntext:'Invite',
  },{
    btnclass:'btn btn-default btn-sm',
    btnfaicon:'fa fa-times',
    btntext:'Remove',
	}],
	'standby': [{
    btnclass:'btn btn-default btn-sm',
    btnfaicon:'fa fa-envelope',
    btntext:'Invite',
  },
  {
    btnclass:'btn btn-danger btn-sm',
    btnfaicon:'fa fa-times',
    btntext:'Delete',
  }],
  'approve': [{
    btnclass:'btn btn-warning btn-sm',
    btnfaicon:'fa fa-ban',
    btntext:'Remove',
  }],
  'reject': [{
    btnclass:'btn btn-default btn-sm',
    btnfaicon:'fa fa-times',
    btntext:'Unban',
  }]

};

const myUserButtons = {
  'invite':   [{
    btnclass:'btn btn-success btn-sm',
    btnfaicon:'fa fa-check-square-o',
    btntext:'Accept',
  }, {
    btnclass:'btn btn-danger btn-sm',
    btnfaicon:'fa fa-sign-out',
    btntext:'Leave',
  }], 
  'interested': [{
    btnclass:'btn btn-success btn-sm',
    btnfaicon:'fa fa-hand-paper-o',
    btntext:'Request Join'
	}, {
    btnclass:'btn btn-warning btn-sm',
    btnfaicon:'fa fa-sign-out',
    btntext:'Leave',
  }],
  'request': [{
    btnclass:'btn btn-warning btn-sm',
    btnfaicon:'fa fa-sign-out',
    btntext:'Leave',
  }],
  'approve': [{
    btnclass:'btn btn-danger btn-sm',
    btnfaicon:'fa fa-sign-out',
    btntext:'Leave',
  }]
};

const userSettings = {
  'host':     'fa fa-home',
  'approve':  'fa fa-handshake-o',
  'standby'	:	'fa fa-clock-o',
  'request':  'fa fa-question-circle-o',
  'interested'	:	'fa fa-bell',
  'invite':   'fa fa-envelope-o',
  'reject':   'fa fa-gavel',
  'decline':  'fa fa-hand-paper-o'
};

// isNotNil :: a -> Bool 
const isNotNil = R.compose(R.not, R.isNil);

// taskFail => Task.of(err)
const taskFail = () => new Task((reject, resolve) => reject('bad params'));

// safeLast :: [a] -> Maybe(a)
const safeLast = (xs) => (xs.length > 0) ? Maybe.of(R.last(xs)) : Maybe.Nothing();

// maybeKey :: obj => Maybe(key);
const maybeKey = (req, pathkey) => (S.path([pathkey, 'key'], req).isJust) 
  ? S.path([pathkey, 'key'], req) 
  : S.path(['user', 'key'], req); 
exports.MaybeKey = maybeKey;

// readUserType :: ({m} -> {r}) => s
const readUserType = R.curry((datapath, match, req) => matches.FindPlayerType(match, 'email', path([datapath, 'user'], req)));
exports.ReadUserType = readUserType;

// taskVerifyHostKey :: (Maybe(account) -> Maybe(match) -> Maybe(k) -> Maybe(e) -> Maybe (s) => Task(match) 
const taskVerifyHostKey = R.lift((account, match, matchkey, useremail, userkey) => new Task((reject, resolve) => matches.VerifyHostKey(account, match, matchkey, useremail, userkey).then(resolve).catch(reject))); 

// getAvatar :: s -> s
const getAvatar = (avatar) => Maybe.of(avatar.getOrElse('/img/default-avatar-sq.jpg'));

// scrub :: mongoObj -> obj;
const scrub = R.compose(
  R.dissoc('_id'), 
  R.dissoc('__v'), 
  JSON.parse, 
  JSON.stringify
);
exports.Scrub = scrub;

// cleanPlayer :: {o} -> {o}
const cleanPlayer = R.compose(
  R.flatten, 
  R.map(dissoc('_id')), 
  R.prop('players'), 
  JSON.parse, 
  JSON.stringify
);
exports.cleanPlayer = cleanPlayer;

// assocPlayers :: o -> o
const assocPlayers = (match) => R.compose(
  R.assoc('players', cleanPlayer(match)), 
  JSON.parse, 
  JSON.stringify
)(match);
exports.assocPlayers = assocPlayers;

// preparyMatchForAPI = JSONObject -> JSONObject
const prepareMatchForAPI = (match) => R.has('distance', match) 
  ? R.assoc('distance', R.prop('distance', match), assocPlayers(match)) 
  : assocPlayers(match);
exports.PrepareMatchForAPI = prepareMatchForAPI;


// getName :: ({o} -> {d}) => [a]
const getName = (u, data) => 
  S.path(['profile', 'name'], data)
  .getOrElse(S.prop('type', u)
  .map(R.equals('invite'))
  .getOrElse(false) 
    ? S.prop('name', u).getOrElse('') 
    : 'No Name Set'
)
exports.GetName = getName;

// findLastInvitedEmail :: {o} -> maybe s
const findLastInvitedEmail = R.compose(
  R.chain(S.prop('email')),
  R.chain(S.whenPropEq('type', 'invite')),
  R.chain(S.last),
  S.prop('players')
);
exports.findLastInvitedEmail = findLastInvitedEmail

// buildProfileChatClass :: {d} => s
const buildProfileChatClass = (data) => accounts.SafeWhenType(
  'img-circle imglistcircle-standard-thin user-chat-avatar vutips', 
  'img-circle imglistcircle-staff-thin user-chat-avatar vutips', 
  'img-circle imglistcircle-premium-thin user-chat-avatar vutips', 
  data
);
exports.BuildProfileChatClass = buildProfileChatClass;


// buildProfileClass :: {d} => s
const buildProfileClass = (data) => accounts.SafeWhenType(
  'img-circle imglistcircle-standard-thin user-players-avatar vutips', 
  'img-circle imglistcircle-staff-thin user-players-avatar vutips', 
  'img-circle imglistcircle-premium-thin user-players-avatar vutips', 
  data
);
exports.BuildProfileClass = buildProfileClass;

// bulidNameClass :: {d} => s
const buildNameClass = (data) => accounts.SafeWhenType(
  'a-name-standard', 
  'a-name-staff', 
  'a-name-premium', 
  data
)
exports.BuildNameClass = buildNameClass;
    
// buildBadges :: {o} => [a]
const buildBadges = (u) => accounts.SafeWhenPremium([{showImg: true, imgsrc:'/img/u_roll_icon1-01.png', faicon:'', tooltip:'Premium Player'}], [], u) 
exports.BuildBadges = buildBadges;

// buildLink :: {u} => s
const buildLink = (u) => S.prop('pageslug', u).isJust 
  ? '/users?id=' + u.pageslug 
  : ''; 
exports.BuildLink = buildLink;

// allPlayersExcept :: e -> {o} => maybe [{p}]
const allPlayersExcept = R.curry((email, matchdata) => S.prop('players', matchdata).map(R.reject(R.propEq('email', email))));
exports.allPlayersExcept = allPlayersExcept;

// propNotEq :: s -> s -> o -> bool
const propNotEq = R.curry((propname, propval, obj) => R.compose(
  R.not,
  R.propEq(propname, propval)
)(obj));

// getUserEmail :: {o} => s
const getUserEmail = (req) => S.path(['user', 'email'], req).getOrElse('');
exports.getUserEmail = getUserEmail;

// buildMatchURL :: s1 => s2
const buildMatchURL = (matchid) => '/events/' + matchid; 
exports.buildMatchURL = buildMatchURL;

// getHostEmail :: [e] -> s 
const getHostEmail = R.compose(
  R.chain(S.prop('email')),
  S.head 
);
exports.getHostEmail = getHostEmail;

// selectButtons :: (s -> e) => {o}
const selectButtons = R.curry((hostemail, email1, email2) => 
  R.equals(email1, email2) 
    ? myUserButtons 
    : R.equals(email1, hostemail)
      ? userButtons
      : []
);

// buildButtons :: s -> s -> {u} -> [{o}]
const buildButtons = (hostemail, email, u) => {

  // TODO add hostemail
  return lift(S.prop)(S.prop('type', u), S.prop('email', u).map(selectButtons(hostemail, email))).getOrElse([]);

};

//whenUploadLocaliseURL 
const whenUploadLocaliseURL = (url) =>
  (R.startsWith('upload', url))
    ? '/' + url
    : url;
exports.whenUploadLocaliseURL = whenUploadLocaliseURL;

// taskTransformUsers :: s -> s -> -> Task({p})
const taskTransformUsers = R.curry((hostemail, email, u) => new Task((reject, resolve) => {

  S.prop('email', u)
    .map(accounts.TaskAccountFromEmail(account))
    .getOrElse(taskFail())
    .fork(
      err => log.clos('err', err), 
      data => {
        if (R.isNil(data) && u.type !== 'invite') {
          resolve({});

        } else {
          resolve({
            name:         getName(u, data),
            nameclass:    buildNameClass(data),
            imgtype:      'img',
            imgclass:     buildProfileClass(data),
            playlevel:    S.path(['profile', 'playlevel'], data).getOrElse(0),
            playerkey:    S.prop('playerkey', u).getOrElse(0),
            status:       S.prop('type', u).getOrElse(''),
            statusfaicon: S.prop('type', u).chain(S.prop(R.__, userSettings)).getOrElse(''),
            imgsrc:       S.path(['profile', 'avataricon'], data)
              .map(whenUploadLocaliseURL)
              .getOrElse('/img/default-avatar-sq.jpg'), 
            link:         buildLink(data),
            badges:       buildBadges(data),
            slug:         S.prop('pageslug', data).getOrElse(''),
            buttons:      buildButtons(hostemail, email, u).getOrElse([]),
            activeuser:   S.prop('email', u).chain(S.equals(email)).isJust,
            extraplayers: S.prop('extraplayers', u).getOrElse('')
          })

        }
      }
    )

}));
exports.TaskTransformUsers = taskTransformUsers;

// maybeMatch :: o -> Maybe a
const maybeMatch = S.path(['params', 'match']);
exports.MaybeMatch = maybeMatch;

// validUser :: (s -> {m} -> {r}) => Bool 
const validUser = curry((type, match, req) => ((readUserType(type, match, req) == 'host') || (readUserType(type, match, req) == 'approve')));
exports.ValidUser = validUser;

// liftVerify :: (Maybe(u) -> Maybe(ky) => Task(res.json) 
const liftVerify = lift(accounts.TaskVerifyAnAccountFromEmail(account));


// buildReoccur :: n -> s -> o
const buildReoccur = curry((every, interval) => {
  return R.objOf('reoccur',
    ((interval === 'never') || every <= 0)
      ? R.objOf('enable', false) 
      : R.zipObj(['enable', 'every', 'interval'], [true, every, interval]))
});
exports.buildReoccur = buildReoccur;

// findPlayerByEmail :: [{p}] -> s -> Maybe {p}
const findPlayerByEmail = R.curry((players, email) => S.find(R.propEq('email', email), R.filter(isNotNil, players)));
exports.findPlayerByEmail = findPlayerByEmail;

// playerTypeObj :: s => {o}
const playerTypeObj = (type) => (type == 'host') 
	? {class: 'label label-primary', text:'Hosting'} 
	: (type == 'approve') 
		? {class: 'label label-success', text: 'Approved'} 
		: (type == 'invite') 
			? {class: 'label label-warning', text: 'Invited'} 
			: (type == 'request') 
				? {class: 'label label-warning', text: 'Requesting Join'} 
				: (type == 'interested') 
					? {class: 'label label-info', text: 'Interested'} 
          : (type == 'decline') 
            ? {class: 'label label-warning', text: 'Declined'} 
            : {class: '', text: ''};
exports.playerTypeObj = playerTypeObj;


// ensurePlayerKeys :: o -> o
const ensurePlayerKeys = (a) => {
  return ({
    name: (S.prop('name', a).map(R.test(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)).getOrElse(false)) 
      ? 'Player name not set'
      : S.prop('name', a).getOrElse('Player name not set'),
    avataricon: S.prop('avataricon', a).getOrElse('/img/default-avatar-sq.jpg'),
    imgclass: "img-circle",
    type: S.prop("type", a).getOrElse('Unknown')
  });
};
exports.ensurePlayerKeys = ensurePlayerKeys;

// isPlayerAttending :: o -> Bool
const isPlayerAttending = (x) => S.prop('type', x)
  .map((x) => (x === 'approve' || x === 'host'))
  .getOrElse(false);
exports.isPlayerAttending = isPlayerAttending;

// headEventTitle :: o -> maybe s
const headEventTitle = R.compose(
  R.chain(S.prop('title')),
  S.head
);
exports.headEventTitle = headEventTitle;

// notifyMatchUsers :: s -> maybe s -> maybe s -> s -> -> o -> task o 
const notifyMatchUsers = R.curry((text, avataricon, matchid, noticetype, messageObjs) => {

  // liftNotify :: Maybe(e) -> Maybe(n) -> Maybe(u) => Task(Notification)
  const liftNotify = (account, text) => R.lift(accounts.TaskNotifyUser(account, text));

  // notifyMatch :: {m} => Task(Notification)
  const notifyMatch = R.curry((account, avataricon, matchid, messageObj) => 
    liftNotify(account, text)(S.prop('email', messageObj), getAvatar(avataricon), matchid.map(buildMatchURL), Maybe.of(noticetype), Maybe.of(false)).getOrElse(taskFail()));

  return new Task((reject, resolve) => R.sequence(Task.of, R
    .filter(propNotEq('type', 'standby'), messageObjs)
    .map(notifyMatch(account, avataricon, matchid))
  ).fork(
      (err) => log.clos('err', err),
      (data) => resolve(data) 
    )
  );

});
exports.notifyMatchUsers = notifyMatchUsers;

// filterPlayer :: (e -> {o}) => Bool)
const filterPlayer = R.curry((email, player) => 
  S.whenPropEq('email', email, player).isJust || 
  S.whenPropEq('type', 'host', player).isJust || 
  S.whenPropEq('type', 'approve', player).isJust || 
  S.whenPropEq('type', 'invite', player).isJust || 
  S.whenPropEq('type', 'request', player).isJust
);
exports.filterPlayer = filterPlayer;

// filterPublicPlayers :: [{a}] => [{b}]
const filterPublicPlayers = (email, players) => R.filter(filterPlayer(email), players);
exports.filterPublicPlayers = filterPublicPlayers;

// isPlayerTypeHostInPlayers :: e -> [o] -> maybe bool
const isPlayerTypeHostInPlayers = R.curry((email, players) => 
  S.find(R.converge(R.and, [R.propEq('email', email), R.propEq('type', 'host')]), players)); 
exports.isPlayerTypeHostInPlayers = isPlayerTypeHostInPlayers;

// safeBodyObj :: o -> s -> maybe o 
const safeBodyObj = R.curry((req, key) => S.path(['body', key], req));
exports.safeBodyObj = safeBodyObj;

// safeReoccur :: o -> s -> n -> maybe o
const safeReoccur = R.curry((req, interval, every) => 
  R.lift(buildReoccur)(safeBodyObj(req, every), safeBodyObj(req, interval)));

// isNearbySearch :: o -> Bool
const isLocationSearch = R.allPass([
  R.has('longitude'), 
  R.has('latitude'), 
  R.has('distance')
]);
exports.isLocationSearch = isLocationSearch;

// isNearbySearch :: o -> Bool
const isNearbySearch = R.allPass([
  R.has('nearby'), 
  R.has('distance')
]);
exports.isNearbySearch = isNearbySearch;

// isTypeSearch :: o -> Bool
const isTypeSearch = R.allPass([
  R.has('types'), 
  R.has('user')
]);
exports.isTypeSearch = isTypeSearch;

// isHistorySearch :: o -> Bool
const isHistorySearch = R.allPass([
  R.prop('history'), 
  R.has('types'), 
  R.has('user')
]);
exports.isHistorySearch = isHistorySearch;

// isExperienceSearch :: o -> Bool
const isExperienceSearch = (obj) => {
  return (R.has('experience', obj))
    ? (R.type(obj.experience) === "Array")
      ? obj.experience.length > 0 
      : true
    : false
}
exports.isExperienceSearch = isExperienceSearch;

// isEventSearch :: o -> Bool
const isEventSearch = R.allPass([
  R.has('event')
]);
exports.isEventSearch = isEventSearch;


// setupRouter 
var setupRouter = function(emailSiteFrom, transporter, twitterbot, activateTwitter) {

  var router = express.Router();
  var jsonParser = bodyParser.json();

  // liftAddGame :: (Maybe(k) -> Maybe(s)) => Maybe(Task)
  const liftAddGame = R.lift(matches.TaskAddGameToMatch(account, match));

  // liftDeleteGame :: (Maybe(k) -> Maybe(s)) => Maybe(Task)
  const liftDeleteGame = R.lift(matches.TaskRemoveGameFromMatch(match));

  // liftFindGame :: maybe s -> maybe task o
  const liftFindGame = R.lift(matches.TaskFindMatch(match));

  // pathMatchId :: o -> a
  const pathMatchId = R.path(['params', 'match']);

  // transformPlayersSend :: ({r} -> {s} -> {d}) => res
  const transformPlayersSend = R.curry((req, res, email, players) => {

    // getPlayers :: (s -> s -> [{o}]) => [{p}] 
    const getPlayers = (email, players) => (isPlayerTypeHostInPlayers(email, players).isJust) 
      ? players 
      : filterPublicPlayers(email, players);

    return R.sequence(Task.of, R.map(taskTransformUsers(getHostEmail(players).getOrElse(''), email), getPlayers(email, players))).fork(
      err => res.sendStatus(500),
      data => res.json(data)
    )

  });

  // filterPlayers :: (e -> {g}) => [{o}])
  const filterPlayers = R.curry((email, match) => S.prop('players', match)
    .map(R.filter(filterPlayer(email)))
    .getOrElse([]));

  // filteredPlayers :: {r} => [{o}]
  const filteredPlayers = (req, res, email) => liftFindGame(maybeMatch(req)).getOrElse(taskFail()).fork(
    err => res.sendStatus(500),
    data => transformPlayersSend(req, res, email, filterPlayers(email, data))
  )

  // checkHostSendPlayers :: {r} -> {s} -> s -> q
  const checkHostSendPlayers = (req, res, email, query) => taskVerifyHostKey(Maybe.of(account), Maybe.of(match), maybeMatch(req), Maybe.of(email), maybeKey(req, query)).getOrElse(taskFail()).fork(
    err =>  filteredPlayers(req, res, email),
    data => transformPlayersSend(req, res, email, prop('players', data))
  );

  var sendUserResponseFrom = curry((req, res, email, x) => S
    .prop('players', x)
    .map(transformPlayersSend(req, res, email))
    .getOrElse([]));

  var sendError = curry((res, err) => res.sendStatus(err.code));
  var whenBodyHas = (data, key) => (has(key, data.body)); 

  router.route('/:match')
    .get(function(req, res) {

      // cleanFields :: {[x]} => {m}
      const cleanFields = R.compose(
        R.map(R.omit(['_id', '__v', 'players', 'games', 'assignedexpansions', 'forumthread', 'publicforums'])),
        R.map(JSON.parse),
        R.map(JSON.stringify),
        S.head
      );

      // findMatch :: {{r} -> k} => Promise({m})
      const findMatch = R.curry((res, matchkey) => matches.FindMatchByKey(match, matchkey).then(function(x) { 
        res.json(cleanFields(x).getOrElse({}))
      }).catch(function(err) {
        res.sendStatus(404)
      }));

      R.map(findMatch(res), maybeMatch(req));

    }).patch(jsonParser, function(req, res) {

      // safeMail :: o => maybe e;
      const safeMail = S.path(['body', 'email']);

      // mergeReoccur o -> o
      const mergeReoccur = (obj) => {
        return safeReoccur(req, 'reoccurinterval', 'reoccurevery')
          .map(merge(obj))
          .map(R.omit(['reoccurinterval']))
          .map(R.omit(['reoccurevery']))
          .getOrElse(obj);
      };

      // validkeys o -> maybe o
      const validkeys = R.compose(
        map(mergeReoccur),
        map(pick(['title', 'experience', 'description', 'seatmin', 'seatmax', 'date', 'timeend', 'utcoffset', 'timezone', 'playarea', 'playarealat', 'playarealng', 'smoking', 'alcohol', 'price', 'currency', 'reoccurevery', 'reoccurinterval', 'autoaccept', 'sessionid'])), 
        S.prop('body')
      );

      const liftUpdate = lift(matches.TaskUpdateMatch(match));

      taskVerifyHostKey(Maybe.of(account), Maybe.of(match), maybeMatch(req), safeMail(req), maybeKey(req, 'body')).getOrElse(taskFail()).fork(
        err => res.sendStatus(401),
        data => liftUpdate(maybeMatch(req), validkeys(req)).getOrElse(taskFail()).fork(
          err => res.sendStatus(400),
          data => (isNil(data)) 
            ? res.sendStatus(400) 
            : res.json({status:'ok'})
        )
      )


    }).delete(jsonParser, function(req, res) {

      // maybeEmail :: o -> maybe s
      const maybeEmail = S.path(['body', 'email']);

      const disband = matches.Disband(account, match);

      const disbandMatch = lift((res, email, userkey, matchkey) => {
        disband(matchkey, email, userkey).then(function(y) {
          return res.json({matches:y});
        }).catch(function(err) {
          return sendError(res, err);
        });
      });

      disbandMatch(Maybe.of(res), maybeEmail(req), maybeKey(req, 'body'), maybeMatch(req));

    }); 


  router.route('/:match/publish')
    .patch(jsonParser, function(req, res) {

      // readIsprivate :: {o} -> {o}
      const readIsprivate = (req) => S
        .path(['body', 'isprivate'], req)
        .map(R.objOf('isprivate'))
        .getOrElse({isprivate: false})

      // readdate :: {o} -> {o}
      const readdata      = R.curry((key, req) => S
        .path(['body', key], req)
        .map(R.objOf(key))
        .getOrElse({}))

      lift(matches.publishMatch(account, match))(
        maybeMatch(req, 'match'), 
        S.path(['body', 'email'], req), 
        maybeKey(req, 'body'),
        Maybe.of(
          R.mergeAll([
            readIsprivate(req), 
            readdata('date', req), 
            readdata('timeend', req), 
          ])
        )
      ).getOrElse(taskFail()).fork(
        err => log.clos('err', err),
        data => {
          res.json(scrub(data))
        }
      );

    });

  // emails the host an email template
  router.route('/:match/sendhosttemplate')
    .post(jsonParser, function(req, res) {
      matches.TaskFindMatch(match, req.params.match)
        .fork(
          err  => res.sendStatus(401),
          activeevent => {
            if (matches.findHostInMatch(req.body.email)([activeevent.toObject()]).isJust) {

              // liftVerifyAccount :: maybe s -> maybe s -> maybe task
              const liftVerifyAccount = R.lift(accounts.TaskVerifyAnAccountFromEmail(account));

              liftVerifyAccount(S.path(['body', 'email'], req), maybeKey(req, 'body')).getOrElse(taskFail()).fork(
                err => res.sendStatus(500),
                data => {
                  emailer.sendInviteTemplateEmail(activeevent.toObject())
                    .fork(
                      err  => {
                        res.sendStatus(500);
                      },
                      data => {
                        res.json({status:'ok'});
                      }
                    );
                });

            } else {
              res.sendStatus(500);

            }
          }
        );

    });

  router.route('/:match/users')
    .get(function(req, res) {

      // safeMail :: obj => Maybe(e);
      const safeMail = S.path(['query', 'email']);
      checkHostSendPlayers(req, res, safeMail(req).getOrElse(''), 'query');

    })
    .post(jsonParser, function(req, res) {

      // unlessUndefined :: fx -> s -> f => s
      var unlessUndefined = R.curry((action, x) => 
        (x === undefined) 
          ? sendError(res, 401) 
          : action(x)
      ); 

      // updateUser :: f -> s -> o -> f => s
      var updateUser = (transform, key, data) => R.compose(
        unlessUndefined(transform), 
        findUserKey(key)
      )(data);

      var join = (x) => matches
        .JoinMatch(account, match, pathMatchId(req), req.body.email, x)
        .then(sendUserResponseFrom(req, res, req.body.email))
        .catch(sendError(res));

      var joininterested = (x) => matches
        .interestedInMatch(account, match, pathMatchId(req), req.body.email, x)
        .then(sendUserResponseFrom(req, res, req.body.email))
        .catch(sendError(res));

      // invite :: s -> Promise model match 
      var invite = (x) => matches
        .InviteMatch(account, match, req.body.host, x, pathMatchId(req), req.body.email)

      // inviteByKey :: s -> Promise model match 
      var inviteByKey = (x) => matches
        .InviteMatchByKey(account, match, req.body.host, x, pathMatchId(req), req.body.userkey)

      // inviteByPlayerKey :: s -> Promise model match 
      var inviteByPlayerKey = (x) => matches
        .InviteMatchByPlayerKey(account, match, req.body.host, maybeKey(req).getOrElse(''), pathMatchId(req), req.body.playerkey)

      // standbyUserByPlayerkey :: s -> Promise model match
      var standbyUserByPlayerkey = (x) => matches
        .standbyMatchByKey(account, match, req.body.host, maybeKey(req, 'body').getOrElse(''), pathMatchId(req), req.body.playerkey)

      // standbyUserByUserkey :: s -> Promise model match
      var standbyUserByUserkey = (x) => matches 
          .standbyMatchByKey(account, match, req.body.host, maybeKey(req, 'body').getOrElse(''), pathMatchId(req), req.body.userkey)

      if (!req.body) return res.sendStatus(400);  // malformed request

      if (has('type', req.body)) {
        if (req.body.type == 'request') {

          // pathRequestFrom :: o -> s
          const pathRequestFrom = R.path(['body', 'email']);
          updateUser(join, 'userkey', req);
          emailer.SendRequestNotifyEmails(account, match, pathMatchId(req), pathRequestFrom(req));

				} else if (req.body.type == 'interested') {

          // pathRequestFrom :: o -> s
          const pathRequestFrom = R.path(['body', 'email']);
          updateUser(joininterested, 'userkey', req);
          emailer.sendInterestNotifyEmails(account, match, pathMatchId(req), pathRequestFrom(req));

        } else if (req.body.type == 'invite') {
          if (S.path(['body', 'userkey'], req).isJust) {
            updateUser(inviteByKey, 'hostkey', req)
              .then((m) => {
                if (!has('notsend', req.body) && findLastInvitedEmail(m).isJust) {
									emailer.sendInviteGuestEmail(account, match,findLastInvitedEmail(m).get(), pathMatchId(req));
                };
                return sendUserResponseFrom(req, res, req.body.host, m);
              })
              .catch(sendError(res));

					} else if (S.path(['body', 'playerkey'], req).isJust) {
            updateUser(inviteByPlayerKey, 'playerkey', req)
              .then((m) => {
                if (!has('notsend', req.body) && findLastInvitedEmail(m).isJust) {
									emailer.sendInviteGuestEmail(account, match,findLastInvitedEmail(m).get(), pathMatchId(req));
                };
                return sendUserResponseFrom(req, res, req.body.host, m);
              })
              .catch(sendError(res));

          } else {
						Promise.all([
							accounts.ReadAnAccountFromEmail(account, S.path(['body', 'host'], req).getOrElse('')),
							accounts.ReadAnAccountFromEmail(account, S.path(['body', 'email'], req).getOrElse(''))
						]).then((users) => {
							if (
								S.pathEq(['premium', 'isstaff'], true, users[0]).isJust 	||
								S.pathEq(['premium', 'isactive'], true, users[0]).isJust 	||
								isNotNil(users[1])

							) {		// host is premium or user is registered
								updateUser(invite, 'hostkey', req)
									.then((m) => {
										if (!has('notsend', req.body) && findLastInvitedEmail(m).isJust) {
											emailer.sendInviteGuestEmail(account, match, findLastInvitedEmail(m).get(), pathMatchId(req));
										};
										return sendUserResponseFrom(req, res, req.body.host, m)

									})
									.catch(sendError(res));

							} else {
								res.sendStatus(400);
								
							}

						});
          }

        } else if (req.body.type == 'standby') {

					if (S.path(['body', 'userkey'], req).isJust) {
            updateUser(standbyUserByUserkey, 'userkey', req)
              .then((m) => {
                return sendUserResponseFrom(req, res, req.body.host, m);
              })
              .catch(() => {
                sendError(res)
              });

          } else if (S.path(['body', 'playerkey'], req).isJust) {
            updateUser(standbyUserByPlayerkey, 'playerkey', req)
              .then((m) => {
                return sendUserResponseFrom(req, res, req.body.host, m);
              })
              .catch(sendError(res));

          } else {
            res.sendStatus(400);
          }

        } else {
          res.sendStatus(400);
        }

      } else {
        res.sendStatus(400);
      }
      return 0;

    })
    .patch(jsonParser, function(req, res) {

      if (!req.body) {
        return res.sendStatus(400);  // malformed request

      } else {

        var acceptInvite = (key) => 
          matches.AcceptInvite(account, match, pathMatchId(req), req.body.user, key)
            .then(sendUserResponseFrom(req, res, req.body.user))
            .catch(sendError(res));

        // TODO this is the new accept function... call it when there is an invitekey, newname fields;
        var acceptAnonymousInvite = (key, invitekey, newname) => 
          matches.approveAnonymousPlayer(match, pathMatchId(req), invitekey, newname)
            .then(sendUserResponseFrom(req, res, req.body.user))
            .catch(sendError(res));

        var approveUser = (key) => 
          matches.ApproveUser(account, match, pathMatchId(req), req.body.host, key, req.body.playerkey)
            .then(sendUserResponseFrom(req, res, req.body.host))
            .catch(sendError(res)); 

        var declineInvite = (key) => 
          matches.DeclineInvite(account, match, pathMatchId(req), req.body.user, key)
            .then(sendUserResponseFrom(req, res, req.body.user))
            .catch(sendError(res));

        var rejectUser = (key) => 
          matches.RejectUser(account, match, pathMatchId(req), req.body.host, key, req.body.playerkey)
            .then(sendUserResponseFrom(req, res, req.body.host))
            .catch(sendError(res)); 

        var unlessUndefined = R.curry((action, x) => (x == undefined) 
          ? sendError(res, 401) 
          : action(x)
        ); 

        var updateUser = (transform, key, data) => 
          R.compose(
            unlessUndefined(transform), 
            findUserKey(key)
          )(data);

        var transformUser = (data, userFunction, hostFunction) => 
          whenBodyHas(data, 'user') 
            ? updateUser(userFunction, 'userkey', data) 
            : updateUser(hostFunction, 'hostkey', data);

        var unlessNoKeyTransform = (data, userFunction, hostFunction) => 
          (!whenBodyHas(req, 'user') && !whenBodyHas(req, 'host')) 
            ? sendError(res, 401) 
            : transformUser(data, userFunction, hostFunction)


        if (has('type', req.body)) {
          if (req.body.type === 'approve') {

            matches.TaskFindMatch(match, pathMatchId(req)).fork(
              err => {},
              data => {
                unlessNoKeyTransform(req, acceptInvite, approveUser);

                if (S.path(['body', 'host'], req).isJust) {
                  emailer.SendApproveNotifyEmails(transporter, account, match, pathMatchId(req), emailSiteFrom, data.players[0].email, req.body.playerkey);

                } else {
                  accounts.TaskNotifyUser(
                    account, 
                    'A player has accepted your event invitation.',
                    data.players[0].email,
                    '/img/u_roll_icon1-01.png', 
                    emailer.BuildMatchUrl(pathMatchId(req)), 
                    'acceptinvite', 
                    false
                  ).fork(err => {}, data => {});

                } 
              }
            );
            
          } else if (req.body.type == 'reject') {
            (!whenBodyHas(req, 'host'))
              ? sendError(res, 401) 
              : updateUser(rejectUser, 'hostkey', req)

          } else if (req.body.type == 'decline') {
            (!whenBodyHas(req, 'user'))
              ? sendError(res, 401) 
              : updateUser(declineInvite, 'userkey', req)

          } else {
            res.sendStatus(400);

          }

        } else if (R.has('extraplayers', req.body)) {

          // maybe s -> maybe s -> maybe s -> maybe s -> maybe task {o} 
          const increment = R.lift(matches.taskIncrementExtraPlayers(account, match));

          // buildMessage :: s -> s -> s
          const buildMessage = (pn, matchname) => pn + " is bringing a plus one to " + matchname.getOrElse(' a match you joined');

          increment( 
            S.path(['params', 'match'], req),
            S.path(['body', 'email'], req),
            maybeKey(req, 'body'),
            S.path(['body', 'extraplayers'], req)
          )
            .getOrElse(taskFail())
            .fork(
              err => {
                res.sendStatus(400)
              },
              data => {

                // notify players if increment is + 1
                if (S.path(['body', 'extraplayers'], req).get() == 1) {
                  allPlayersExcept(getUserEmail(req), data)
                    .chain(notifyMatchUsers(
                      buildMessage(
                        S.path(['user', 'profile', 'name'], req).getOrElse('A player'), 
                        S.prop('title', data)
                      ), 
                      S.path(['user', 'profile', 'avataricon'], req), 
                      maybeMatch(req), 
                      'plusone') 
                    ).fork(
                      (err) => {},
                      (datanotify) => {
                        transformPlayersSend(req, res, S.path(['body', 'email'], req).getOrElse(''), data.players);
                      } 
                    );

                } else {
                  transformPlayersSend(req, res, S.path(['body', 'email'], req).getOrElse(''), data.players);
                }

              }
            )

        } else {
          res.sendStatus(400);
        }
      }

    }).delete(jsonParser, function(req, res) {

      // leaveMatch :: s => Promise(Model({m}))
      var leaveMatch = (key) => 
        matches.LeaveMatch(account, match, req.body.user, key, pathMatchId(req))
          .then(sendUserResponseFrom(req, res, req.body.user))
          .catch(sendError(res));

      // revokeMatch :: s => Promise(Model({m}))
      var revokeMatch = (key) => 
        matches.RevokeMatch(account, match, req.body.host, key, pathMatchId(req), req.body.playerkey)
          .then(sendUserResponseFrom(req, res, req.body.host))
          .catch(sendError(res)); 

      // unlessUndefined :: x => a -> a -> a 
      var unlessUndefined = R.curry((action, x) => (x == undefined) 
        ? sendError(res, 401) 
        : action(x)
      ); 

      var updateUser = (transform, key, data) => {
        return maybeKey(data, 'body')
          .map(unlessUndefined(transform));
      }

      // transformUser :: o -> f => x -> f => x -> a
      var transformUser = (data, userFunction, hostFunction) => 
        whenBodyHas(data, 'user') 
          ? updateUser(userFunction, 'playerkey', data) 
          : updateUser(hostFunction, data.body.playerkey, data);

      // transformUser :: o -> f => x -> f => x -> a
      var unlessNoKeyTransform = (data, userFunction, hostFunction) => 
        (!whenBodyHas(req, 'user') && !whenBodyHas(req, 'host')) 
          ? sendError(res, 401) 
          : transformUser(data, userFunction, hostFunction);

      if (!req.body) return res.sendStatus(400);  // malformed request

      unlessNoKeyTransform(req, leaveMatch, revokeMatch);

    });

  // setupRubRouters :: express -> express
  const setupSubRouters = R.compose(
    serviceForum.SetupRouter(emailSiteFrom, transporter),
    serviceTimeslots.SetupRouter(emailSiteFrom, transporter)
  );
  router = setupSubRouters(router);

  // bggLinkify :: n => s
  const bggLinkify = (id) => 'http://www.boardgamegeek.com/boardgame/' + id;

  // safeBgglink {o} => {p}
  const safeBgglink = (a) => R.compose(map(R.objOf('bgglink')), map(bggLinkify), S.prop('id'))(a);

  // mergeLink :: {a} => {b}
  const mergeLink = (a) => merge(scrub(a), safeBgglink(a).getOrElse({}));

  router.route('/:match/games')
    .get(function(req, res) {

      liftFindGame(maybeMatch(req))
        .getOrElse(taskFail())
        .fork(
          err   => res.sendStatus(500),
          data  => {
            res.json(R.map(mergeLink, data.games))
          }
        ) 

    })
    .post(jsonParser, function(req, res) {

      // maybeGameId :: {o} => k
      const maybeGameId = S.path(['body', 'gameid']); 

      // maybeGameName :: {o} => k
      const maybeGameName = S.path(['body', 'name']); 

      // maybeGameStatus :: {o} => k
      const maybeGameStatus = S.path(['body', 'status']); 

      // maybeGameThumbnail :: {o} => k
      const maybeGameThumbnail = S.path(['body', 'thumbnail']); 

      // safeMail :: obj => Maybe(e);
      const safeMail = S.path(['body', 'email']);

      // lastGame :: {o} => {g} 
      const lastGame = R.compose(
        R.map(R.map(scrub)), 
        S.prop('games'), 
        JSON.parse, 
        JSON.stringify
      ); 

      // verifyPlayer :: s -> {o} => Bool
      const verifyPlayer = (email) => R.compose(
        R.map(isPlayerAttending), 
        R.chain(S.find(R.propEq('email', email))), 
        S.prop('players')
      );

      // buildMessage :: s1 -> s2
      const buildMessage = R.curry((playername, game, eventname) => playername + " is planning to bring " + game + " to " + eventname); 

      // buildSuggestMessage :: s1 -> s2
      const buildSuggestMessage = R.curry((playername, game, eventname) => playername + " wants to play " + game + " at " + eventname); 

      // findGameName :: {d} => s
      const findGameName = R.compose(
        R.chain(S.prop('name')), 
        R.chain(safeLast), 
        S.prop('games')
      );

      // findGameCover :: {d} => s
      const findGameCover = R.compose(
        R.chain(S.prop('thumbnail')), 
        R.chain(safeLast), 
        S.prop('games')
      );

      // verify the user is valid
      liftVerify(safeMail(req), maybeKey(req, 'body')).getOrElse(taskFail()).fork(
        (err) => res.sendStatus(401),
        (user) => matches.FindMatchByKey(match, maybeMatch(req).getOrElse('')).then((x) => {
          R.chain(verifyPlayer, safeMail(req))(x[0]).getOrElse(false) 
            ? liftAddGame(maybeMatch(req), maybeGameStatus(req), maybeGameId(req), maybeGameName(req), maybeGameThumbnail(req), safeMail(req))
              .getOrElse(taskFail()).fork(
                err => res.sendStatus(500),
                (data) => {
                  if (S.pathEq(['body', 'status'], 'playersuggest', req).isJust) {
                    allPlayersExcept(getUserEmail(req), data)
                      .chain(notifyMatchUsers(
                        buildSuggestMessage(
                          S.path(['user', 'profile', 'name'], req).getOrElse('A player'),
                          findGameName(data).getOrElse(''),
                          headEventTitle(x).getOrElse('an event you are attending.')
                        ), 
                        findGameCover(data), 
                        maybeMatch(req), 
                        'suggestgame' 
                      )).fork(
                        (err) => res.sendStatus(err.code),
                        (notify) => res.json(lastGame(data).getOrElse({}))
                      )

                  } else {
                    allPlayersExcept(getUserEmail(req), data)
                      .chain(notifyMatchUsers(
                        buildMessage(
                          S.path(['user', 'profile', 'name'], req).getOrElse('A player'),
                          findGameName(data).getOrElse(''),
                          headEventTitle(x).getOrElse('an event you are attending.')
                        ), 
                        findGameCover(data), 
                        maybeMatch(req), 
                        'bringgame' 
                      )).fork(
                        (err) => res.sendStatus(err.code),
                        (notify) => res.json(lastGame(data).getOrElse({}))
                      )
                  } 

                }
            ) 
            : res.sendStatus('403')
        })
      );

    }
  );


  router.route('/:match/games/:gameslug/')
    .delete(jsonParser, function(req, res) {

      // maybeGameSlug :: {o} => k
      const maybeGameSlug = S.path(['params', 'gameslug']); 

      // safeMail :: obj => Maybe(e);
      const safeMail = S.path(['body', 'email']);

      // isGameOwner :: [{m}] => Bool
      const isGameOwner = R.compose(
        R.propEq('email', safeMail(req).get()), 
        R.find(R.propEq('pageslug', maybeGameSlug(req).getOrElse(0))), 
        R.prop('games'), 
        R.head
      );

      // isPlayerTypeHost :: [{o}] -> s -> maybe bool
      const isPlayerTypeHost = R.curry((matchObj, email) => R.compose(
        R.map(R.equals('host')), 
        R.chain(S.prop('type')), 
        R.chain(S.find(R.propEq('email', email))),
        R.chain(S.prop('players')),
        S.head
      )(matchObj));

      // verify the user is valid
      liftVerify(safeMail(req), maybeKey(req, 'body'))
        .getOrElse(taskFail())
        .fork(
          err => res.sendStatus(500),
          data => matches.FindMatchByKey(
            match, 
            maybeMatch(req).getOrElse('')
          )
            .then((m) => isGameOwner(m) || safeMail(req).map(isPlayerTypeHost(m)).isJust
              ? liftDeleteGame(maybeMatch(req), maybeGameSlug(req))
                .getOrElse(taskFail())
                .fork(
                  err   => res.sendStatus(500),
                  data  => res.json(map(mergeLink, data.games))
                )
              : res.sendStatus(500)
            )
        );

    })
    .patch(jsonParser, function(req, res) {

      // maybeGameSlug :: {o} => k
      const maybeGameSlug = S.path(['params', 'gameslug']); 

      // safeMail :: obj => Maybe(e);
      const safeMail = S.path(['body', 'email']);

      // suggestGame :: s -> s -> s -> task [o]
      const suggestGame = R.lift(matches.taskBringSuggestedGameToMatch(account, match));

      // findGameName :: s -> o -> s
      const findGameName = R.curry((slug, data) => R.compose(
        R.chain(S.prop('name')), 
        R.chain(S.find(R.propEq('pageslug', slug))), 
        S.prop('games')
      )(data));

      // findGameCover :: s -> o -> s
      const findGameCover = R.curry((slug, data) => R.compose(
        R.chain(S.prop('thumbnail')), 
        R.chain(S.find(R.propEq('pageslug', slug))), 
        S.prop('games')
      )(data));

      // buildMessage :: s1 => s2
      const buildMessage = R.curry((playername, game, eventname) => playername + " is planning to bring " + game + " to " + eventname); 

      // verify the user is valid
      liftVerify(safeMail(req), maybeKey(req, 'body'))
        .getOrElse(taskFail())
        .fork(
          err => res.sendStatus(500),
          data => {
            if (S.pathEq(['body', 'status'], 'select', req).isJust) {
              suggestGame(maybeMatch(req), maybeGameSlug(req), safeMail(req)).getOrElse(taskFail()).fork(
                err => res.sendStatus(err),
                data => {

                  try {
                    allPlayersExcept(getUserEmail(req), data)
                      .chain(notifyMatchUsers(
                        buildMessage(
                          S.path(['user', 'profile', 'name'], req).getOrElse('A player'),
                          maybeGameSlug(req).chain(findGameName(R.__, data)).getOrElse(''),
                          S.prop('title', data).getOrElse('an event you are attending')
                        ),
                        maybeGameSlug(req).map(findGameCover(R.__, data)).getOrElse('/img/default-avatar-sq.jpg'),
                        maybeMatch(req),
                        'bringgame'
                      )).fork(
                        (err) => res.sendStatus(err.code),
                        (notify) => {
                          res.json(R.prop('games', data))
                        }
                      )

                  } catch (err) {
                    res.json(R.prop('games', data))
                  }
                }
              );

                 

            } else {
              res.sendStatus(500);
            }
          }
        )

    });


  // safeMail :: o => maybe a;
  const safeMail = S.path(['body', 'email']);

  // maybeGameId :: o => maybe a;
  const maybeGameId = S.path(['params', 'gameid']);

  router.route('/:match/games/:gameid/votes')
    .post(jsonParser, function(req, res) {
			
			// liftVote :: maybe s -> maybe n -> maybe s -> maybe s -> maybe task o 
      const liftVote = R.lift(matches.voteForGame(account, match));

      liftVote(
        maybeMatch(req), 
        maybeGameId(req), 
        safeMail(req), 
        maybeKey(req, 'body') 
      ).getOrElse(taskFail()).fork(
        err => res.sendStatus(err),
        data => res.json(data)
      );

    })
    .delete(jsonParser, function(req, res) {

			// unvoteForGame :: maybe s -> maybe n -> maybe s -> maybe s -> maybe task o
      const liftunVote = R.lift(matches.unvoteForGame(account, match));
      liftunVote(
        maybeMatch(req), 
        maybeGameId(req), 
        safeMail(req), 
        maybeKey(req, 'body') 
      ).getOrElse(taskFail()).fork(
        err => res.sendStatus(err),
        data => res.json(data)
      );
      
    });

  // maybeGameKey :: a -> s
  const maybeGameKey = S.path(['params', 'gamekey']);

  router.route('/:match/games/:gamekey/joins')
    .post(jsonParser, function(req, res) {
			
			// buildMessage :: s -> s -> s
      const buildMessage = R.curry((playername, game, eventname) => playername + " is joining you to play " + game + ' at ' + eventname); 

			maybeMatch(req)
				.map(matches.TaskFindMatch(match))
				.getOrElse(taskFail())
				.fork(
					err => log.clos('err', err),
					data => {

						var emaillist = S.prop('games', data.toObject())
							.chain(S.find(R.propEq('gamekey', maybeGameKey(req).getOrElse(''))))
							.map(R.prop('join'))
							.getOrElse()

            // findGameAvatar :: o -> maybe s
            const findGameAvatar = R.curry((gamekey, m) => R.compose( 
              R.chain(R.prop('avataricon')),
              R.chain(S.find(R.propEq('gamekey', gamekey))),
              S.prop('games')
            )(m));

						if (emaillist != undefined) {

							notifyMatchUsers(
                buildMessage(
                  S.path(['user', 'profile', 'name'], req).getOrElse('A player'),
                  S.path(['body', 'gamename'], req).getOrElse("a board game"), 
                  S.prop('title', data).getOrElse('at an event you are attending')
                ),
                maybeGameKey(req).map(findGameAvatar(R.__, data)),
								maybeMatch(req),
                'playgame',
								emaillist	
							)
								.fork((err) => { log.clos('err', err) }, (data) => {});

						}
					}
				)


			// liftJoin :: maybe s -> maybe n -> maybe s -> maybe s -> maybe task o 
      const liftJoinGame = R.lift(matches.joinGame(account, match));

      liftJoinGame(
        maybeMatch(req), 
        maybeGameKey(req),
        safeMail(req), 
        maybeKey(req, 'body') 
      ).getOrElse(taskFail()).fork(
        err => res.sendStatus(err),
        data => res.json(data)
      );

    })
    .delete(jsonParser, function(req, res) {

			// leaveGame :: maybe s -> maybe n -> maybe s -> maybe s -> maybe task o
      const liftLeaveGame = R.lift(matches.leaveGame(account, match));
      liftLeaveGame(
        maybeMatch(req), 
        maybeGameKey(req),
        safeMail(req), 
        maybeKey(req, 'body') 
      ).getOrElse(taskFail()).fork(
        err => res.sendStatus(err),
        data => res.json(data)
      );
      
    });



  // maybeExpansionId :: o => maybe a;
  const maybeExpansionId = S.path(['params', 'expansionid']);

  // maybeExpansion :: o => maybe {o};
  const maybeExpansion = S.path(['body', 'expansiondata']);

  // maybeUserSlug :: o => maybe {o};
  const maybeUserSlug = S.path(['body', 'userslug']);

  // maybeGameSlug :: o => maybe a;
  const maybeGameSlug = S.path(['params', 'gameslug']);

  router.route('/:match/games/:gameslug/expansions/')
    .get(function(req, res) {

      // liftReadExpansions :: maybe s -> maybe s -> maybe [{o}]
      const liftReadExpansions = R.lift(matches.taskReadExpansions(match));

      liftReadExpansions(maybeMatch(req), maybeGameSlug(req))
        .getOrElse(taskFail())
        .fork(
          err => res.json([]),
          data => res.json(data)
        );

    })
    .post(jsonParser, function(req, res) {

      // liftAddExpansion :: maybe s -> maybe s -> maybe s -> maybe s -> maybe {o}
      const liftAddExpansion = R.lift(matches.taskAddExpansion(account, match));

      liftAddExpansion(maybeMatch(req), safeMail(req), maybeKey(req, 'body'), maybeUserSlug(req), maybeGameSlug(req), maybeExpansion(req))
        .getOrElse(taskFail())
        .fork(
          err => res.json([]),
          data => {
            res.json(data)
          }
        );

    })

  router.route('/:match/games/:gameslug/expansions/:expansionid')
    .delete(jsonParser, function(req, res) {

      // liftRemoveExpansion :: maybe s -> maybe s -> maybe s -> maybe s -> maybe id 
      const liftRemoveExpansion = R.lift(matches.taskRemoveExpansion(account, match));
      liftRemoveExpansion(maybeMatch(req), safeMail(req), maybeKey(req, 'body'), maybeGameSlug(req), maybeExpansionId(req))
        .getOrElse(taskFail())
        .fork(
          err => res.json([]),
          data => {
            res.json(data)
          }
        );
    })

  router.route('/')
    .get(function(req, res) {

      var scrubMongo = R.compose(
        R.dissoc('_id'), 
        R.dissoc('__v'), 
        prepareMatchForAPI
      );

      var userkey = maybeKey(req, 'query');

      // propWhenDefined :: s -> {o} -> s
      const propWhenDefined = R.curry((key, obj) => S.prop(key, obj).getOrElse(''));

      // findEmail :: s -> [o] -> o
      const findEmail = R.curry((email, playerlist) => R.find(R.propEq('email', email), playerlist)); 

      // getPlayerTypeObj :: s -> o -> o 
      const getPlayerTypeObj = R.curry((email, match) => R.compose(
        playerTypeObj, 
        propWhenDefined('type'), 
        findEmail(email), 
        R.prop('players')
      )(match));

      // getPlayerType :: (s -> {o}) => t
      const getPlayerType = R.curry((email, match) => R.compose(
        propWhenDefined('type'), 
        findEmail(email), 
        R.prop('players')
      )(match));

      // typify :: {m} => Maybe(val)
      const typify = (m) => R.lift(getPlayerType)(S.path(['query', 'user'], req))(Maybe.of(m)).map(R.objOf('usertype')).map(merge(m)).getOrElse(m);

      // typifyObj :: {m} => Maybe(val)
      const typifyObj = (m) => R.lift(getPlayerTypeObj)(S.path(['query', 'user'], req))(Maybe.of(m)).map(R.objOf('usertypeObj')).map(merge(m)).getOrElse(m);

      // maybeIsMany :: {o} => Maybe(bool);
      const maybeIsMany = R.compose(R.map(R.lte(R.__, 2)), S.prop('seatavailable'));

      // mergeAvailability :: {o} => {p} 
      const mergeAvailability = (x) =>  merge((S.prop('isfull', x).getOrElse(true)) 
        ? {badge: {class:'eventmatchfull', text:'No spots left. This event is full.'}} 
        : (maybeIsMany(x).getOrElse(false)) 
          ? {badge: {class:'eventbadgelow wow flash', text: 'Event nearly full. Limited seats left.'}} 
          : {badge: {class:'', text:''}} , x)

      // liftEntryFee :: {o} => Maybe(s)
      const liftEntryFee = (x) => R.lift(matches.EntryFee)(S.prop('price', x), S.prop('currency', x));

      // safeGtZero :: a => Maybe(a);
      const safeGtZero = (a) => R.gt(a, 0) 
        ? Maybe.of(a) 
        : Maybe.Nothing;

      // mergeEntryFee :: {o} => {p}
      const mergeEntryFee = (x) => (S.prop('price', x).chain(safeGtZero).isJust) 
        ? R.merge(R.objOf('entryFee', "Entry Fee: " + liftEntryFee(x).getOrElse('')), x) 
        : R.merge(R.objOf('entryFee', ''), x) 

      // hasPlayer e -> maybe o
      const hasPlayer = R.curry((email, x) => R.compose(
        R.map(R.length),
        R.chain(S.filter(R.propEq('email', email))),
        S.prop('players')
      )(x));


      // mergeIsSignedUp :: s -> {o} -> {p}
      const mergeIsSignedUp = R.curry((email, x) => {
        return hasPlayer(email, x).cata({
          Just: (a) => ((a > 0)
            ? R.merge({issignedup: true}, x)
            : x
          ),
          Nothing: () => x 
        });
      });

      const transformEvent = {
        games:    (g) => R.map((x) => R.pick(['thumbnail', 'name', 'id', 'expansions', 'status'], x), g),
        players:  (p) => R.filter(isPlayerAttending, p).map(ensurePlayerKeys)
      };

      // buildMatches :: s -> [m] => {o}
      const buildMatches = R.curry((user, x) => R.compose(
        R.objOf('matches'), 
        R.map(R.pick(['title', 'description', 'locname', 'date', 'timeend', 'seatmin', 'seatmax', 'seatavailable', 'experience', 'alcohol', 'smoking', 'status', 'isprivate', 'loc', 'games', 'key', 'distance', 'day', 'usertype', 'entryFee', 'players', 'badge', 'usertypeObj', 'price', 'currency', 'issignedup'])), 
        R.map(R.evolve(transformEvent)), 
        R.map(mergeAvailability),
        R.map(mergeIsSignedUp(user)),
        R.map(mergeEntryFee),
        R.map(typifyObj),
        R.map(typify),
        R.map(scrubMongo)
      )(x));

      // validify :: s -> Promise o
      const validify = R.composeP(
        R.equals(1), 
        R.length, 
        accounts.ReadVerifyAccountFromEmail(account)
      );

      if (isHistorySearch(req.query)) {
        validify(req.query.user, userkey.getOrElse('')).then(function(isValid) {
          if (isValid) {
            matches.FindHistoricMatchesByPlayerType(match, req.query.user, req.query.types).then(function(x) {
              res.json(buildMatches(req.query.user, x));

            }).catch(function(err) {
              res.sendStatus(err.code);

            });
          } else {
            res.sendStatus(401);
          }

        });

      } else if (isTypeSearch(req.query)) {
        validify(req.query.user, userkey.getOrElse('')).then(function(isValid) {
          if (isValid) {
            matches.FindMatchesByPlayerType(match, req.query.user, req.query.types).then(function(x) {
              res.json(buildMatches(req.query.user, x));

            }).catch(function(err) {
              res.sendStatus(err.code);

            });
          } else {
            res.sendStatus(401);
          }

        });

      } else if (isExperienceSearch(req.query)) {
        matches.findPublicBoardgameByExperience(req.query.experience).fork(
          err => res.sendStatus(500),
          data => {
            res.json(buildMatches(req.query.user, data))
          }
        )

      } else if (isEventSearch(req.query)) {
        matches.TaskReadByEvent(match, req.query.event).fork(
          err => res.sendStatus(500),
          data => res.json({matches: R.map(scrubMongo, data)})
        )
         
      } else if (isLocationSearch(req.query)) {
        matches.FindBoardgameMatchesByDistance(match, req.query.distance, '', req.query.latitude, req.query.longitude).then(function(x) { 
          res.json(buildMatches(req.query.user, x));
        });

      } else if (isNearbySearch(req.query)) {
        validify(req.query.nearby, userkey.getOrElse('')).then(function(pass) {
          if (pass) {
            matches.FindBoardgameMatchesNearby(match, req.query.nearby, req.query.distance).then(function(x) { 
              res.json(buildMatches(req.query.nearby, x));
            }).catch((err) => log.clos('err', err));

          } else {
            res.sendStatus(401);
          }

        }).catch(function(y) {
          res.sendStatus(500);
        });

      } else {
        matches.findPublicMatches(req.query.user).then(function(x) {
          res.json(buildMatches(req.query.user, x));
        }).catch(function(err) {
          res.sendStatus(err.code);
        });

      }
    }) 
    .post(jsonParser, function(req, res) {

      // isTwo :: n -> n 
      const isTwo = (val) => (Number(val) !== 2) 
        ? 1 
        : 2;

      // safeObj :: s -> s -> maybe o 
      const safeObj = R.curry((keyfrom, keyto) => 
        safeBodyObj(req, keyfrom).map(R.objOf(keyto)));

      // safeObjOfDefault :: o -> s -> s -> o -> maybe obj
      const safeObjOrDefault = R.curry((defaultval, propfrom, propto) => {
        return safeObj(propfrom, propto).cata({
          Just: (a)=>   Maybe.of(a),
          Nothing: ()=> Maybe.of(R.objOf(propto, defaultval))
        });
      });

      // safeObjOrDefault :: s -> s -> o -> maybe obj
      const safeObjOrDefaultPrice = R.curry((propfrom, propto) => {
        return safeObjOrDefault(0, propfrom, propto).chain(safeDefault0)
      }); 

      // create social object 
      const safesmoking = safeBodyObj(req, 'smoking')
        .map(isTwo)
        .map(R.objOf('smoking'));

      const safealcohol = safeBodyObj(req, 'alcohol')
        .map(isTwo)
        .map(R.objOf('alcohol'));

      const safesocial = safealcohol
        .map(merge).ap(safesmoking)
        .map(R.objOf('social'));

      // create coords object 
      const safeplayarealng = safeBodyObj(req, 'playarealng');
      const safeplayarealat = safeBodyObj(req, 'playarealat');

      const safelatlng = safeplayarealat
        .map(append)
        .ap(safeplayarealng.map(of))
        .map(R.objOf('coords'));

      // safeDefault0 :: a -> Maybe 0
      const safeDefault0 = (a) => (S.prop('price', a).getOrElse('') == '') 
        ? Maybe.of({price:0}) 
        : Maybe.of(a);

      // maybeOnline :: o -> maybe n 
      const maybeOnline = (req) => S.whichever(
        S.pathEq(['body', 'experience'], '6', req),
        S.pathEq(['body', 'experience'], '11', req)
      );

      // getLocation :: o -> maybe o
      const getLocation = (req) => maybeOnline(req).cata({
        Just:   () => {
          return Maybe.of(
            R.mergeAll([
              safeObj('experience', 'experience').getOrElse({}),
              safeObj('playarea', 'locname').getOrElse({}),
              safelatlng.getOrElse({})
            ])
          );
        },
        Nothing:() => {
          return R.compose(
            R.lift(R.merge)(safeObj('experience', 'experience')),
            R.lift(R.merge)(safeObj('playarea', 'locname'))
          )(safelatlng);
        }
      });

      const safeHostObj = safeObjOrDefault([], 'timeslots', 'timeslots')
        .map(merge).ap(safeReoccur(req, 'reoccurinterval', 'reoccurevery'))
        .map(merge).ap(safeObj('date', 'gamedate'))
        .map(merge).ap(safeObj('timeend', 'timeend'))
        .map(merge).ap(safeObj('maxplayers', 'maxplayers'))
        .map(merge).ap(safeObj('minplayers', 'minplayers'))
        .map(merge).ap(safeObj('autoaccept', 'autoaccept'))
        .map(merge).ap(safeObj('status', 'status'))
        .map(merge).ap(safeObj('isprivate', 'isprivate'))
        .map(merge).ap(safesocial)
        .map(merge).ap(safeObj('title', 'title'))
        .map(merge).ap(safeObj('description', 'description'))
        .map(merge).ap(getLocation(req))
        .map(merge).ap(safeObjOrDefault([], 'games', 'games'))
        .map(merge).ap(safeObjOrDefaultPrice('price', 'price'))
        .map(merge).ap(safeObjOrDefault('AUD', 'currency', 'currency'))
        .map(merge).ap(safeObj('utcoffset', 'utcoffset'))
        .map(merge).ap(safeObj('timezone', 'timezone'));

			// hostmatch :: s -> s -> o -> o
      const hostmatch = R.lift((email, key, matchobj) => {
        matches.HostMatch(match, account, email, key, matchobj).then(function(x) {
          res.send(scrub(x));
          return {};

        }, function(err) {
          res.sendStatus(404);  // resource not found

        });
      });
      hostmatch(safeBodyObj(req, 'email'), maybeKey(req, 'body'), safeHostObj);
      
    });

  return router;

}; 
exports.SetupRouter = setupRouter;


// findUserKey :: (k -> {d}) => u
var findUserKey = R.curry(function(keyProperty, reqData) {

  // findKey :: o => maybe s
  const findKey = R.converge(S.whichever, [
    S.path(["body", keyProperty]),
    S.path(["user", "key"])
  ]);

  return findKey(reqData).getOrElse(undefined);

});
exports.FindUserKey = findUserKey;
