#API Guides

## Source
http://www.restapitutorial.com/lessons/httpmethods.html
http://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api


## General
Using CRUD
* POST to create
* GET to read
* PUT to update/replace
* PATCH to update/modify
* DELETE to delete


## Response Codes
* 200 OK - A successful call that does not add a record (GET, PUT, PATCH, DELETE)
* 201 Created - Successful creation of a record (POST or PUT)
* 204 No Content - Successful creation that will not return a record, usually a delete (DELETE or PUT)
* 304 Not Modified - Caching headers
* 400 Bad Request - Malformed request, can not parse data
* 401 Unauthorized - None or invalid authorisation token
* 403 Forbidden - Valid user does not have permission for resource
* 404 Not found - A non existant record is requested
* 405 Method not allowed - The user does not have permission for the method
* 409 Conflict - Record creation would result in a duplicate record
* 410 Gone - The resource is no longer avaialble, such as with an old API
* 415 Unsupported Media Type - content type is not correct
* 422 Unprocessable Entity - data is not valid 
* 500 Internal server error - catch all fail 
