#boardgames
Access via `URL/api/v1/boardgames`

## Search Function
Use the search query to look up boardgames
    `URL/api/v1/boardgames?name=game_name`


### Return Object
The return object will be a json array object that contains the following schema:
```javascript
    {
        id: boardgameid,
        name: gamename,
        year: game_year_published,
        thumbnail: gamethumbnail
    }
```

#### id Key
id key is the boardgame geek id of the boardgame

#### name Key
name key is the official boardgame

#### year Key
year is the year the boardgame was created 

#### thumbnail Key
the thumbnail is the url of the icon to display
