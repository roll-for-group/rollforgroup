API implementation

# accounts
Access the via `URL/api/v1/accounts`

## Return Object
The return object will either be json value with the key 'profile' or 'error'. The 'profile' key indicates a successful call.

## Get User Profile
### access the locally logged in user
    `URL/api/v1/accounts`

If the user is logged in, then the user details will be returned. If no user is logged in, then an error 301, 'invalid user profile' will be returned.

## Other user data 
Find basic information about another player, based on their pageslug.
    `URL/api/v1/accounts/pageslug

### access another user
    `URL/api/v1/accounts/someuser@email.com?key=someaccesskey`

Shows the profile of someuser@email.com.

If the key is not included, an error 302, 'no key specified' error will be returned.
If the user and key does match, the error 301, invalid user profile is returned.

### Return schema format
Data is returned in the following format:
```javascript
    profile: {
        email: 'someuser@email.com'
    }
```

## Update bgguser

### access
Use the PATCH method with accounts accessor
    `URL/api/v1/accounts/someuser@email.com`

### data format
Set bgguser and key values in the data
    `{bgguser: 'yourBggAccount', key: 'key-xxxx-yyy'}`

Set other values as a list and key values in the data
    `{bgguser: 'yourBggAccount', notifyevents:true, name:'My name', key: 'key-xxxx-yyy'}`

If the 'key' is not included the error 302, 'no key specified' is returned.
When updating the field 'bgguser', if the user is not found in the boardgamegeek api, then an error 404, 'invalid username specified' is returned.

If the return is successful, then a result succes object is returned.
    `{result: 'succes'}`

Valid values are:
* avataricon    - String
* bggname       - String
* name          - String
* biography     - String
* job           - String
* ishost        - Boolean
* playeralcohol - Boolean
* playersmoking - Boolean
* hostalcohol   - Boolean
* hostsmoking   - Boolean
* usebggcollection - Boolean
* notifyevents  - Boolean
* notifyplayers - Boolean
* notifyupdates - Boolean
* notifyjoin    - Boolean

The following values are used for schedules
* Monday        - Boolean
* Tuesday       - Boolean 
* Wednesday     - Boolean
* Thursday      - Boolean
* Friday        - Boolean
* Saturday      - Boolean
* Sunday        - Boolean

The following values allow 0-23 for hours, and 0-63 for minutes
* mondaystarthour
* mondaystartminute
* mondayendhour   
* mondayendminute
* tuesdaystarthour
* tuesdaystartminute
* tuesdayendhour
* tuesdayendminute 
* wednesdaystarthour
* wednesdaystartminute
* wednesdayendhour 
* wednesdayendminute
* thursdaystarthour
* thursdaystartminute 
* thursdayendhour
* thursdayendminute
* fridaystarthour
* fridaystartminute
* fridayendhour
* fridayendminute
* saturdaystarthour 
* saturdaystartminute
* saturdayendhour
* saturdayendminute
* sundaystarthour
* sundaystartminute
* sundayendhour
* sundayendminute


### UpdateAvatarIcon 
If a user has their facebook linked, to update their user profile image with their new facebook picture use the facebookupdate edge
    `URL/api/v1/accounts/someuser@email.com/facebookupdate`


## Notifications
New player notifications can be read with a GET request to the following URL
    `URL/api/v1/accounts/someuser@email.com/notifications`

The query string should include the users api key
* `key`: the users api key


### Creating a notification
A notification can be created from one player to another by using a POST request to the following URL
    `URL/api/v1/accounts/someuser@email.com/notifications`

The post data should contain the following fields
* `key`: the users api key
* `subject`: the subject of the notice
* `text`: the text of the notice
* `gameid`: the bgg game id the notice refers to 
* `emailto`: the users the notice is going to, the notice will be from the user specified in the URL (i.e. somone@email.com in the above example) 


### Marking notifications as read
Each notification has a unique notifyid, the notification can be marked as read with a PATCH request to the notifyid, setting the status to `read`
    `URL/api/v1/accounts/someuser@email.com/notifications/notifyid`
    `{status: 'read', key: 'key-xxxx-yyy'}`
* `key`
* `status`


## PrivateMails 
New player private mails can be read with a GET request to the following URL
    `URL/api/v1/accounts/someuser@email.com/privatemails`

The query string should include the users api key
* `key`: the users api key
* `type` : 'new' if included will only show read emails, otherwise shows all mail

### Creating a privatemail 
A notification can be created from one player to another by using a POST request to the following URL
    `URL/api/v1/accounts/someuser@email.com/privatemails`

The post data should contain the following fields
* `key`: the users api key
* `subject`: the subject of the mail 
* `text`: the text of the mail 
* `pageslug`: the users the mail is going to, the mail will be from the user specified in the URL (i.e. somone@email.com in the above example) 


### Marking mail as read
Each privatemail has a unique mailid, the notification can be marked as read with a PATCH request to the notifyid, setting the status to `read`
    `URL/api/v1/accounts/someuser@email.com/notifications/notifyid`
    `{status: 'read', key: 'key-xxxx-yyy'}`
* `key`
* `status`


## Collections 
Player collections can be read with a GET request to the following URL
    `URL/api/v1/accounts/pageslug/games`
* `email`
* `key`

The email and key requirements are to verify the requsting user. The pageslug needs to be a valid users pageslug value. Pageslugs can be found through the website by clicking on a user and viewing the URL or querying an account by email address.

## Achievements 
Player achievements can be read with a GET request to the following URL
    `URL/api/v1/accounts/pageslug/achievements`

To view the showcased acheivements use
    `URL/api/v1/accounts/pageslug/achievements?showcase=true`

To view the acheivements in progres, but not yet completed use 
    `URL/api/v1/accounts/pageslug/achievements?inprogress=true`

Player Acheivements are public, thereofre no login information is required.


## Error Codes
Error codes are returned in the following format:
    `error: {code: number, description: string}`


Valid error codes are

* 301 - 'invalid user profile'
* 302 - 'no key specified'
* 303 - 'unknown error'
* 305 - 'no update key'
* 306 - 'invalid user name specified'
