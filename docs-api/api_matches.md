# Matches
Access the via `URL/api/v1/matches`
Note matches are now referred to as events, for the purpose of this documentation the two values are interchangeable.

# Read matches
Read matches by providing a GET request to the matches API.

## Read matches close to a goelocations
All matches that are close to a location can be searched, by searching for distance, latitude and longitude. Distance is specified in meters.
* `URL/api/v1/matches?distance=50000&latitude=30.23023020&longitude=20.30202302`

## Read matches nearby a player location
Searching for games nearby a player, will look up the players location and show games that are available near that player. All matches that are close to a location can be searched. Distance is specified in meters. The users apikey must be specified, otherwise an error 401 will be returned
* `URL/api/v1/matches?distance=50000&nearby=user@email.com&userkey=api_key`

## Read historical matches
Add the key history: true to show games that the player participated in.
* `URL/api/v1/matches?history=true&user@email.com&userkey=api_key`

## Read matches a player is going to participate in 
To show only certain types of game, use the type filter. The following example will show all games the user is invited to.
* `URL/api/v1/matches?user=user@email.com&userkey=api_key?types=['invite']&user=user@email.com&userkey=api_key`

A combination of multiple types can be used to show game of multiple types. For example the follwoing will show games the user both hosts and is accepted to join.
* `URL/api/v1/matches?user=user@email.com&userkey=api_key?types=['host', 'accept']&user=user@email.com&userkey=api_key`

Valid types are
* host - the user created the match
* invite - the user has been invited to join by the host 
* request - the user has requested to join a public game 
* approve - the user is confirmed to be joining the match 
* reject - the user can not join the game 
* decline - the user can not join the game 
* complete - the game was completed and the user attended 
* absent - the game was completed, but the user did not attend 
* review - the game was completed and the user left a review 

# Read matches at an event
To show matches at an event, make sure the game is created with an eventid property, which matches the pageslug for a valid event
* `URL/api/v1/matches?event=validevent`

## Specify match
Use a match key to receive just one match, replace match_id below with the returned matchkey property.
`URL/api/v1/matches/match_id`

# Match workflows and status
## Creating a Match
When a match is created it can be in the `open` or `private` states. A game in the open state can be viewed by all players, however a game in the `private` state can only be viewed by the host any players who are invited to the match.

The status of the relationship a player has to a match is called the player `type`. The player host who created the game has the initiated a player invitation, they are added to the game with the type of `host`.

## Adding New Members
If a game is `open` then players can request to join the match and the game is visible to all players. When a new player wants to join a match, they are added to the game with the player type `request`. When a player requests to join a match, the host can change the type to `approve` or `reject`.

The host has the option to ask a player to join an `open` or `private` match. When the host invites a player to join they are added with the player type `invite`, and the player then has visibility over the match. When a player is invited to the match, then the player can either change their type to `approve` or `reject`.

## Removing Members
Players who have not been approved or rejected can revoke their invitation or request. A host can revoke an invitation, and a player can revoke their join request. To revoke an invitation send the `DELETE` HTML verb to the matchkey, with the users credentials and the playerkey. If a player has been approved or rejected, they can no longer remove their request.

## Filling the Game
The `seatmax` value of the match is used to track when the game is full. An unlimited number of players can be invited, or request to join a match while it is `open` or `private`. However once the number of players plus the host have accepted to join equals the `seatmax` value, then the game state changes to `full`.

Once a game is `full`, then the game is only visible to the host and members who ave accepted the game. Any other players who were invited, or requesting to join the match are removed.

## Playing the Game
Once the date and time of the game occurs, then the game is changed to the `played` state.

Once a game is in the `played` state, players have a chance to rate the game, at which time their player type changes to `reviewed`.

## Expiring the Game
If the game play date occurs and the game is not in the `full` state, then the game changes to `expired`, signifying a failed game.

# Create Game
A game can be created by sending a POST request to the matches end point. A successful request requires the following data fields:
`URL/api/v1/matches`

## Send
* title: short title for the match
* description: additional notes from the host
* status: 'open' or 'private'
* date: the date when the match will occur
* timeend: the end of the match
* reoccurevery: either `never`, `week`, or `weekOfMonth` 
* reoccurinterval: if `reoccurevery` is not `never`, a number representing the reoccurance frequency
* minplayers: the least number of players
* maxplayers: number of players at the table
* email: host users email address
* key: host users api key
* smoking: either 1 or 2, 1 - No smoking, 2 - smoking allowed
* alcohol: either 1 or 2, 1 - No alcohol, 2 - alcohol allowed
* experience: 1 - Anyone, 2 - Beginners, 3 - Request Teacher, 4 - Experienced Players Only
* playarea - the location address
* playarealat - latitude of the location
* playarealng - longitude of the location 
* games - an array of BGG game id's
* price - the entry fee 
* currency - the countries currency code

Smoking, and Alcohol represent whether the hosted game allows drinking or smoking.

Any players nearby to the lat & lng will be emailed when the game is created.


# Join a match
To add a user to a match, POST the users details to match/match_id/users
`URL/api/v1/matches/matchid/users`

The matchid will be returned when a game is created or GET request, as the matchkey property

## Request Join 
When a user wants to request to join an open public match, the following data is sent with `POST`.
* user: the user to add to the match
* userkey: the users key
* type: `request` 
The match must be `open` for a user to be able to request to join. When the match is `private` the user will not be able to join. 

## Reject User 
When a host wants to deny a users request to join a match, the following data is sent with `PATCH`.
* host: the user to add to the match, who needs to be the host of the game
* hostkey: the hosts user key
* user: the user who will be invited to the match 
* type: `reject` 
Once a user is in the `decline` state they can no longer be invited or request to join the game.

## Decline User 
An invited user can decline a match invitation.
* user: the user invited to the match
* userkey: the user key
* type: `decline` 

## Approve User 
When a host wants to permit a users request to join a match, the following data is sent with `PATCH`.
* host: the user to add to the match, who needs to be the host of the game
* hostkey: the hosts user key
* user: the user who will be invited to the match 
* type: `approve` 

An invited user can approve an invitation initated by the host.
* user: the user invited to the match
* userkey: the user key
* type: `approve` 
Once a user is in the `approve` state they, they are considered to be an active member of the match, and attending the event.

## Invite User 
When a host invites another user to join a private match and gives the user visibility of the match, the following data is sent with `POST`. 
* host: the user to add to the match, who needs to be the host of the game
* hostkey: the hosts user key
* user: the user who will be invited to the match 
* type: invite 
The host can invite another user to the game when it is in the `open` or `private` state. When the user is not registered an email request will be sent to them to join the game, on behalf of the requesting user.

## Disband User 
By sending a `DELETE` request, the specified user will be removed from the match. Unlike the `reject` status, when a user is disbanded there is no longer a record of them being rejected from the match and they can request to join or be invited again. 


# Errors
## Unknown game id
The boardgame being hosted needs to be owned by the hosting user. If the game is unknown to then a statusCode 404 will be returned. To add a game to the database, add it to the users BGG profile,and update the user.

## User doesn't host
The host user must have signified that they are willing to host, and have a valid hosting location associated with their account. If the user has not chosen to host then an status 404 will be returned.

## Users api key is not valid
A statusCode error 404 will be returned. (TODO: change to 401)


# ForumThread 
Each match has a discussion portion on the board, members can participate to the discussion if they are either the host, or an approved member. The match discussion can be accessed via:
`URL/api/v1/matches/matchid/forumthread` for the private thread  or `URL/api/v1/matches/matchid/publicforums` for the public thread

## Read new forum discussions
To read the forum discussion use GET with the following values 
* user 
* userkey

Each forum post has a hash key associated with it. Use an existing posts hashkey in the after field to find new posts. To read the forum discussion use GET with the following values: 
* user 
* userkey
* after

## Add to forum discussion
To add to the forum discussion, send the following as POST data 
* user 
* userkey
* discussion

## Delete a forum discussion
Only hosts or authors can remove forum discussions, send a DELETE request to `URL/api/v1/matches/matchid/forumthread/threadid`
* user
* userkey

# Disband Match
A host can disband a match when the match is open or private. Disbanding a match will remove all players from the match, for that reason matches that are full can not be disbanded. To disband a match send a DELETE html request to the match URL `URL/api/v1/matches/matchid`
The host email and user key are required to be sent in the request data.
* user 
* key


# Games 
Each game has various games which can be added or removed. The match games can be accessed via:
`URL/api/v1/matches/matchid/games`

## View games in a match
Send a get request to the URL above to see the list of games associated with that event

## Add a game from a match 
To add to the match, send the following as POST data. The gameid must be the games boardgame geek id. The user must be a host of the game, otherwise a failure will return. 
* user 
* key 
* gameid 

The return will be an object representing the game which was just added.

## Delete a game from a match
A host can delete games from a match. To delete games from a match send a DELETE request with the following data. The host email and user key are required to be sent in the request data.
* user 
* key
* gameslug

The gameslug is a unique id created when a game is added to a match. It is different to the boardgamegeekid. 

# Publish event 
Publishing a event changes an event from private to public. To publish an event, send a PATCH request to the following url, with isprivate set to false. Date and Timeend are optional values that can be used to set the start and end times, when publishing the event.
`URL/api/v1/matches/matchid/publish`
* email
* key
* isprivate
* date
* timeend

# Email event template to host 
To help get the event out, the host can send themselves a preformatted email ready to forward to others. To host can request this email with a POST request to:
`URL/api/v1/matches/matchid/sendhosttemplate`
* email
* key
