Groups API implementation

# groups 
Access the via `/api/v1/groups`

## GET Multiple Group Details
Pass no paramaters to get the top public groups
    `/api/v1/groups`

Pass a valid email and userkey to get all the groups a specific member belongs to
    `/api/v1/groups`

## POST create group 
    `/api/v1/groups`

Valid keys are: 'name', 'description', 'url', 'email', 'key'
The 'url' must be unique, otherwise a 500 error is thrown
The 'email' and 'key' must be a valid register users, with their associated api key. If the user is logged in they do not have to supply the api key.

## GET Group Details
Where the groupurl is a unique url belonging to the group
    `/api/v1/groups/groupurl`

## PATCH Group Details
Where the groupurl is a unique url belonging to the group
    `/api/v1/groups/groupurl`

### Valid keys are: 
For account permissions
* 'url', 
* 'email', 
* 'key', 

The 'email' and 'key' must be a valid register user who is a host of the group, with their associated api key. If the user is logged in they do not have to supply the api key.

To update values
* 'name', 
* 'description', 
* 'playarea',
* 'playarealat', 
* 'playarealng'

The playarea fields are used to indicate the location as can be identified on google map, along with the lattitude and longitude.
Any other fields will update the group details. As groups must have unique URL's, changing the URL to a value another group owns results in an error.

## DELETE Group Details
Where the groupurl is a unique url belonging to the group
    `/api/v1/groups/groupurl`

Valid keys are: 'name', 'description', 'url', 'email', 'key'
The 'email' and 'key' must be a valid register user who is a host of the group, with their associated api key. If the user is logged in they do not have to supply the api key.
If successful, the group will be deleted, with a {status: ok} response..


### Return schema format
```javascript
    group: {
      name: 'The Group Name',
      description: 'Markdown text used to describe the group',
      url:  'groupurl'
    }
```
