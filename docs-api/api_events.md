#events
Access via `URL/api/v1/events`

## Search Function
Use the search query to lookup events
    `URL/api/v1/events?key=eventkey

### Return Object
The return object will be a json array object that contains the following schema
```javascript
    {
        name: eventname,
        plageslug: page_slug_for_the_event,
        loc: location_object, 
        key: the_event_key,
        dates: [
            array showing which date and times the event will run
        ]
    }
```

### name
name is the name of the event

### pageslug
pageslug is used as a url for the event

### loc
loc is an object showing the event location

### key
the key for the event

### dates
an array showing the dates and start, finish times for each day
