# locations
Access via `URL/api/v1/locations`

## Search Function
Use the search query to look up addresses in google maps.
    `URL/api/v1/locations?search=my_test_address`

### Return Object
The return object will be a json array objects that contain the following schema:
```javascript
    {
        place: {
            address: "This is the textual address information",
            coords : {lat: xxxx.xx, lng: yyyy.yy}
        }
    }
```

#### Address Key
The address key contains the textual human readable address information.

#### Coords Key
Coords is the map coordinates of the location, with an object showing a latitude and longitude pair.

## Error Codes
There are no error codes, however a empty array will be returned if there are no addresses.
