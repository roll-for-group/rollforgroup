% Join an Event 

## Opening an Event 
Hosted events are displayed for nearby players to view on the event list page. Alternatively, players may have received an email or an event URL link from another player.

![Event Listing](../img/docs/hostevent_titlelisted.png)

By clicking on the match or event URL, players can see the match details and request to join the event. The match details include the start and stop time, the location of the event on a google map, and any other social conditions.

![Event Details](../img/docs/joinevent_details.png)

### Asking the Group A Question
Each event page features a public forum, where players can ask the host and other players questions about the event. Asking a question requires a valid Roll for Group account. Type any questions into the *Ask the group a question* box in the *Public Event Chat* window and press *Post*.

![Event Details](../img/docs/premiumplayerpublicchat.png)

If a player is online, they may respond while the page open, otherwise check back to the event page later for a response.

### What are we Playing? 
The host may enter the scheduled games to play on the night, which may influence your decision to join. The event page shows a listing of games with their associated estimated play times and player counts.  Some game events are open, and in such cases, the host may not enter any games. 

![Game Details](../img/docs/joinevent_games.png)

### Who else is attending? 
Players confirmed to be attending the match display under the game detail listings. The XP level accumulates by participating in events on the Roll for Group.

![Players](../img/docs/joinevent_players.png)

### Requesting to Join 
Press the *Join Event* green button to request to join the event. The joining player's avatar shows in the player list as a request to join. The player may press *leave* at any stage if they change their mind. Requests to join the event are only visible to the game host and the requesting player, and not to other attending players.

![Join Request](../img/docs/joinevent_request.png)

### Accepting Players 
The game host will receive a notification email regarding any request to join the event. On viewing the page, the game host will see player requests to join the game. The host can then *Approve* or *Deny* the players request to join.

![Join Request](../img/docs/joinevent_accept.png)

Approved players are notified by email and join the event list.

* Any denied players are banned from entering the event. They are not permitted to request to join the event again.

### Player Contact Details 
When a player has their contact information in their profile, clicking on that player's name will show their contact information. It's recommended for game hosts to have contact information visible in their user profile.

In addition, the event page includes a private forum section, where attending players can share personal contact information, without having it visible to non-attending players.

![Private Forum](../img/docs/joinevent_private.png)
