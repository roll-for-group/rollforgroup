% Linking Your BoardGameGeek Account to Your Roll for Group User Account

 BoardGameGeek Account or BGG Account is your username on the [BoardGameGeek website](http://www.boardgamegeek.com). The BoardGameGeek website is used by many players to track their board game collections, and games they want to play. Roll for Group can synchronise owned and want to play games from BoardgameGeek user accounts.

![BoardgameGeek](../img/docs/linking_bggsite)

Roll for group imports BoardGameGeek account information to track which games players own and want to play. The Roll for group website links players who own games, to others who want to play those games. 

There are two ways to link your board game geek account as described below.

1. Linking on Sign up
2. Linking after Sign up

## Linking BGG Account on Sign up
When signing up to Roll for Group for the first time, the sign in wizard requests an optional BoardGameGeek username. On entering a valid BoardGameGeek username, the Roll for Group will import the data from the username as part of the setup.

![Signup Page](../img/docs/linking_signup)

## Linking BGG Account after Sign up 
When a user account is not added during sign up, it can be added later on through the Collection or Want to Play pages. 

On the top right of the screen is the logged in players username with a drop down arrow. When the username is selected, a drop down menu appears with options for both the "Collection" and "Want to Play" pages.

![User Dropdown](../img/docs/linking_userdropdown)

### Collection & Want to Play Pages 
The "Collection" and "Want to Play" page show the logged in users games in a list. However if the player has not entered a username, an information window appears asking for the users BoardGameGeek username. 

By entering the user's BoardGameGeek username and pressing "Add" the game collections import. The information box requesting the users BoardGameGeek username will no longer show once the BGG account is linked, and instead, a synchronise button will appear.

![BGG User Request](../img/docs/linking_addbgg)

## Syncing Roll for Group User and BGG Account
On loading the "Collection" or "Want to Play" pages as described above, a synchronisation button will appear shown as two arrows in a circular shape. By pressing the Synchronisation button, the players linked BGG information will be updated. Whenever syncing occurs, both the list of owned games and want to play games are added.

![BGG User Request](../img/docs/linking_syncbgg)

## Changing BGG User Accounts
The profile page allows a member to change their linked BGG User account on the profile page. To navigate to the User Profile page, select the player's username at the top right. From the drop down select "Player Profile".

![User Dropdown](../img/docs/linking_userdropdown)

The profile page allows the player to change their BoardGameGeek user name under the "Collection Management" heading.

![BGG Name Profile Page](../img/docs/linking_profilebgg)

After changing the username, press "Save" at the bottom of the page to ensure the changes update.

## Timeout Issues with the BGG API 
When the BGG account has a substantial number of games attached to it, it may take a while to import. The Roll for Group import routine will attempt to retry. However, the BoardGameGeek will sometimes need a few minutes to load a larger account into its local cache. When receiving a time out message, try again in a few minutes, and the user account should be able to import into the Roll for Group.
