% Adding Games to an Event

Hosts can choose to add games to an event up front, or allow players to add their own games.

When creating a public event, it is recommended to add a few board games before publishing the event, as the game list will be included in player invite emails.

## Adding Board Games
Any attending player can add a game to an event. To add a game, type its title into the "Games I am Bringing" text box. The drop down will indicate information about the game, including number of players, playing time, and what player counts work best with the game.

![Board game selection](../img/docs/hostevent_boardgames.png)

## Adding Expansions
Once a game is added, it will display in the "Players are bringing games" section. If the game has expansions, a plus icon will display next to the game.

Pressing the plus button will display a drop down of the available expansions for that game. Selecting an expansion will add the expansion to the game, displaying the games thumbnail, and adjusting the player counts and playing time according to the expansions information.

![Expansion](../img/docs/hostevent_expansion.png)

### Publishing a Private Event
Private events are not visible to other players, and they are not automatically notified by email.

Instead the host must either invite players, triggering the system to send them an email or share the match link url with them.

[Invite Players to an Event](inviteevent.html)

Create a private event by pressing the "Private" button.

![Event email](../img/docs/hostevent_publishprivate.png)

### Publishing a Public Event
Publishing a public event, will enable the event to be visible to other players, and emails all nearby players to notify them of the event.

![Event email](../img/docs/hostevent_email.png)

To publish and event public, press the "Publish Open" button on a draft event.

![Event email](../img/docs/hostevent_publishopen.png)

The event will now be visible in the find events list.

[Invite Players](inviteevent.html)
