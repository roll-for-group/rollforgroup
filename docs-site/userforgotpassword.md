% Recovering an Account Password 

## Reset or Recover Your Account Password
### Use Facebook
After signing up with an email address, a verified Facebook account with a matching email address can be used to log into the account. When the Facebook email matches the Roll for Group email sign in, Facebook will verify the account without your Roll for Group password.

### Use Google 
Simiarly to recovering an account with Facebook, a verified Google account with a matching email address can be used to log into an account. When the Google email matches the Roll for Group email sign in, Google will verify the account without your Roll for Group password.

### Email Recovery 
To reset and change the password through email, visit the [forgot password](../forgot) page and complete the following steps.

![Recover Password Page](../img/docs/recoverpassword.png)

1. Type the matching email address
2. Press the "Reset Password" button

An email with a password reset link is sent within a few seconds. The link is only valid for thirty minutes, if the email is not received in one minute check the spam email folder.

If the email is not received, double check the email address is correct and try again. If the email address still does not work, send an email to [gaming@rollforgroup.com](mailto:gaming@rollforgroup.com) for support.
