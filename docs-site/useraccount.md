% Create an Account

User accounts are used to identify each player and are required to host events and join events. User accounts are free to create, and can be upgraded to a premium account for additional perks.

## Create a User Account
To create a free account visit the Roll for Group [signup](https://www.rollforgroup.com/signup) page. There are three options for creating an account, either using Google, Facebook or Email.

![Sign Up Page](../img/docs/signin.png)

### Sign in with Google or Facebook
Sign in with Google quick easy and secure. There is no need to create a password for your Roll for Group account. Google or Facebook will confirm the users identity on sign up without requiring a password.

After visiting the Roll for Group [signup](../signup) page:

1. Press the Continue with Facebook button or Sign in Roll for Group
2. If asked by Facebook or Google, confirm permission for Roll for Group to authenticate

Google or Facebook will validate the account and log the user in.

### Sign up with Email and Password
After visiting the Roll for Group [signup](../signup) page:

1. Type a valid email address you have access to
2. Create a password
3. Press the "Join the Table" button

A confirmation email with an authentication link is sent to the entered email address and should arrive within one minute. Pressing on the link will verify the account for use. Once an account is verified, the user may log in through the [log in](../login) page.  

The authentication link is only valid for thirty minutes. If the email does not arrive, check the spam email folder. If you still have not received a confirmation email, try again and double check the spelling of the email. If the issue persists, send a support request to gaming@rollforgroup.com.


## Account Setup 

When signing up with email, ensure you keep your password safe. On logging in for the first time, each player is shown the [welcome](userwelcome.html) page.

![Welcome Screen](../img/docs/welcomepage.png)

### Game Teaching
By indicating a player likes teaching games, a teaching icon will appear next to the user account profile. The likes teaching games icon helps other players identify which players enjoy, or do not enjoy teaching games. 

![Game Teaching](../img/docs/gameteaching.png)

This value can be changed later in the player [profile](../profile) page.

### Profile Visibility
Profile visibility determines whether other players can see the user profile. When visibility is off, the user account can not be seen by other players in the local area. The user profile will be visible in the following circumstances:
* When requesting to join a match, the host player will be able to see the profile to evaluate whether they add the player.
* Once joining a game, other players in the game can see each other's profiles.

![Profile Visibility](../img/docs/profilevisibility.png)

A profile that is not visible  will display a warning to the user. The visibility setting is changed on the player [profile](../profile) page.

### Age Verification
Age verification is used to ensure all players are over eighteen. Enter the player's age as a number. Values under 18 will not be accepted. 

![Age Verification](../img/docs/confirmage.png)

### Board Game Geek Account 
When a player has an account on [board game geek](http://www.boardgamegeek.com), games owned by the player will show in the player's [collection](../collection), and games the player [wants to play](../wanttoplay) are visible on the player's profile. Enter the player's Board Game Geek username if they have one.

![Board Game Geek Account](../img/docs/boardgamegeekusername.png)

Leaving this field blank will prevent the site from importing the user account. The bgg username can be added later in the player [profile](../profile) page.

### Game Notifications
By enabling game notifications, players will receive emails when a match is started nearby. The email will contain the name of the host, the games played, and the time and date. Players can then decide whether they wish to participate.

![Game Notifications](../img/docs/nearbyplayers.png)

This setting can be changed on the player [profile](../profile) page.
