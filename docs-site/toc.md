% Support Table of Contents

# Roll for Group - Support Documentation

[Roll for Group Home](https://www.rollforgroup.com)

![Roll for group match making user manual](../img/rfg-logo-whiteglow-64.png)

## Hosting
1. [Host an Event](hostevent.html)
2. [Add Games](addgameevent.html)
3. [Invite Players](inviteenvent.html)
4. [Join Event](joinevent.html)
5. [Suggest and Vote for Games](votingevent.md)

## User Account
1. [User Accounts](useraccount.html)
2. [User Welcome](userwelcome.html)
3. [Password Recovery](userforgotpassword.html)
4. [Link BoardgameGeek Account](userlinkbgg.html)

## Premium Membership
1. [Membership](membership.html)
2. [Gems](premiumgems.html)
3. [Link Calendar](linkcalendar.html)
