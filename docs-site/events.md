% Events

Events are set at specific time and dates, for players to get together and play games.

# Player Workflow
Events are created by the host, and players can join or leave the event. The workflow for players joing events is as follows:

## Host invites a player
. players
+-- invite 
|   +-- interested
|   +-- approve

## A player requests to join
. players
+-- request 
|   +-- interested
|       +-- approve
|   +-- approve
