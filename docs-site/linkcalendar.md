% Linking a Calendar

Roll for Group can synchronize board game events to calendar applications on phones, tablets or computers. Keep track of the board game events joined across multiple regular board game group meetups and ad-hoc board game sessions.

## What Shows in the Calendar? 
When a calendar synchronizes with Roll for Group, all the joined and hosted events show automatically. The date, time, location and match description are visible on the calendar, with a public link to the Roll for Group event page.

![Sample Calendar](../img/docs/calendarsync_show.png)

After a calendar is linked, any additional joined events will automatically appear on the calendar. On leaving an event on Roll for Group, the event will be removed from the calendar. Requested events are not shown until the hosts accepts the user. 

### Finding the Calendar Subscription Details
After logging into Roll for Group, the calendar subscription details are available on either the *schedule* or *history* pages, accessible from the top menu.

![Calendar Menu](../img/docs/calendarsync_menu.png)

At the top of the page, a panel will show *Premium Calendar Synching* with a unique URL that can be used by third party calendar software.

* **Note:** Calendar synchronization is only available to premium users. Without the premium upgrade, the calendar subscription URL will not be visible. To upgrade an account to Premium see the [membership](../docs/membership.html) page.

### Loading the Subscription Link into Your Calendar
There is a text box with a *copy* button in the Premium Calendar Synching window. The contents of the window contains a URL begining with *https://www.rollforgroup.com/...*, containing the logged in users email address and ends in *rollforgroup.ics*. The URL is a unique link that only the user has access to.

![Find Calendar](../img/docs/calendarsync_find.png)

As the link is quite long, a *copy* button is provided to help make copying the link to the clipboard easy. Copy the URL, and then depending on the calendar program, add the URL as a calendar subscription.  

### Sync to iPhone or iPad
To sync to an iPad or iPhone calendar;

* Select the *Settings* app
* Select *Calendar* on the left-hand side of the settings
* Select *Accounts >* on the right-hand side

![iPad/iPhone Calendar](../img/docs/calendarsync_ipad.png)

* Select *Add Account >*
* Select *Other* when presented with a list of calendar providers
* Select *Add Subscribed Calendar*
* Paste the Roll for Group URL into the new text box labelled *Server* and press *Next*
* Press *Save*

When a device has email, it may be easier to email the long URL link from a computer to the mobile device or tablet.

### Google Calendar & Android
Once the google calendar is synchronised, events will show on both the Google calendar and Android device.

* Log into the google calendar website with a web browser.
* On the left-hand side, press the down arrow next to *Other Calendars*
* Select *Add by URL* from the drop-down menu.

![Google Calendar](../img/docs/calendarsync_google.png)

* A popup window displays with the text box marked *URL*, paste the Roll for Group Events
* Press *Add Calendar*

The events will now show in the google calendar. To ensure the events show further on an Android device;

* Open up the google app on the mobile device, a list of calendars from the google calendar should be visible
* Select the *Roll for Group* calendar
* Ensure the *Sync* setting is turned on

### Office365
To add a calendar to Office365

* Login to the Office365 website
* Open the *calendar* application
* On the top menu, select *Add calendar*
* From the drop-down menu, select *From Internet*

![Office365 Calendar](../img/docs/calendarsync_office365.png)

* Paste the Roll for Group URL into the box marked *Link to the calendar* and add a calendar name such as *Roll for Group*
* Press *Save* to add the calendar.

### Mac Calendar
To sync a Mac calendar;

* Start the calendar application on the Mac
* Select *New Calendar Subscription...* under the *File* menu. A window will appear requesting the user to *Enter the URL of the calendar you want to subscribe to.*

![Mac Calendar](../img/docs/calendarsync_mac.png)

* Paste the URL from Roll for Group above into the *Calendar URL:* box and press *Subscribe*.
