% Inviting People to an Event

After hosting an event, the host may want to invite other players to join the event.

There are two ways to invite players:
1. a direct invitation from the host
2. sharing the event link with other players

## Host Invitation
Players who are hosting an event can send player invitations. As the host, an *Invite a local player by name* box will appear under the *Attendees* panel. Type in your friends name, and as you type nearby locally registered players names will appear. Your friend will receive a personalised event invitation from the Roll for Group website when their name is selected.

![Invite Friends](../img/docs/inviteplayers_name.png)

* Invited players are visible as pending to other members of the group, until they accept or decline their invitation.

### Revoking the Invitation
* If circumstances change and the invited the player is no longer requested to play, the host can press the *Remove* button. The player is removed from the event..

### Player Invitation Email
The invited player will receive a customised email invitation from Roll for Group. The invitation email will include the event time and a URL link to the event. When the invited player clicks on the URL in the email, they will see the event details.

![Invitation Email](../img/docs/inviteplayers_invitation.png)

### Accepting the Invitation
If the invited player decides to join the event, they press the *Accept* button. At this point, the invited user will be asked to log into the website and join. If they have a valid Roll for Group account and are logged into the website, pressing the "Accept" button will add them to the event.

![Accept](../img/docs/inviteplayers_accept.png)

* If the invited player presses *Reject*, they will be removed from the player list.

## Sharing the Event Link
If the host wants to invite a player who has not registered on Roll for Group, or if a non-host player wants to invite their friends to the event, they can do so by sharing the event link. The event link is a unique public URL that will open the event page directly, without needing a login to the website.

![Copy](../img/docs/inviteplayers_link.png)

Press the *copy* button to save the URL to the clipboard, and then share the link on social media, or send the link via email to invite a friend.

When sharing the URL on social media, the link will convert into a social media card using the cover art of the first game in the event. Pressing the URL will open up the Roll for Group event invitation page for players who can then [request](docs/joinevent.html) to join the event.

![Social Card](../img/docs/inviteplayers_socialcard.png)

[Suggest and Vote for Games](votingevent.html)
