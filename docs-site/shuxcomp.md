% Roll for Group - Shux 2017 general admission ticket competition 

![](../img/rfg-logo-whiteglow.png)

# Win a Shux 2017 general admission ticket. 

Roll for Group is giving away one of the limited 700 SHUX2017 General Admission Ticket worth $150.00. To be eligible for the prize, all you need to do is organise and join board game meet ups on the Roll for Group website.

[Shux2017](https://www.shutupandsitdown.com/shux2017/)

The player who hosts or joins board game events with the *most number of different people through the Roll for Group website* by the 27th September 2017 will win the ticket. The deadline to play board games with other people is the 27th September 2017. Events created on or after the 28th will not count.

## How to Enter:
1. Sign up for Roll for Group website account. Roll for Group accounts are free, there is no costs with hosting or joining a game.
* [Sign up page](../signup)

2. Host and join board game events on Roll for Group
You may publicise games at your local game group, your meetup, local game store event, or host your own events at a public space.
* [How to host an event on Roll for Group](https://rollforgroup.com/docs/hostevent.html)

3. The player who plays board games with the most amount of other gamers, wins the ticket!

### SHUX Video 
[![SHUX2017](https://img.youtube.com/vi/HyZS_QLMrJw/0.jpg)](https://www.youtube.com/watch?v=HyZS_QLMrJw)

## Competition Terms and Conditions:
1. Entries open on the 12th September 2017.
2. Roll for Group is free to use, there is no charge to sign up, join or host an event.
3. You may join multiple events, but each member you play with will only count once. For example, if you host a game on Wednesday with Bob, Sarah and John attending you have played with three people. If you then host an event on Friday and Sarah, Mary and Fred attend, then you only played with five members in total, because you already played with Sarah.
4. Each game played must be registered on the Roll for Group site as a public match. Private events are hidden from others, and will not count.
5. Each attending player must sign up and be accepted to your event through the Roll for Group website. If members attend your event and they do not sign up through the site, it will not count.
6. Roll for Group will contact the winner on 28th September via email. The winner must respond with their legal name within 24 hours to receive the ticket, otherwise the ticket maybe passed to the next winner. If a winner does not respond by the 29th September, the ticket can not be transferred into their name.
7. If there is a tie for most people played with, the player who hosted the most number of games on the Roll for Group website wins. If there is still a tie, the player who finished their last game earliest wins. If there is still a tie, the player who joined the Roll for Group website first wins.

### Contact 
* Contact: gaming@rollforgorup.com
* Mail: PO Box 465 Unanderra, NSW 2526, Australia 

### Roll for Group 
* [Website](../home)
* [About](../about)
* [Documentation](toc.html)
* [Privacy Policy](../privacy)
