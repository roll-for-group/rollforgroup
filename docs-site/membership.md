% Premium Membership 

Users can sign up and use Roll for Group for with basic functionality at no cost. Purchasing membership helps support the Roll for Group site, giving the player unique visuals on their avatar and name as shown below. 

### Premium Player Listing
Premium players will have a distinct color on their Avatar and player name in the player listings. The golden coloring shows the player.

![Premium Listing](../img/docs/premiumplayerlist.png)

### Premium Join Game Coloring & Badge
When a premium player is invited to, or requesting to join a game, the Premium Badge and golden coloring will show for the player.

![Premium Join](../img/docs/premiumplayerjoingame.png)

### Premium Public Chat Coloring 
When a premium player chats in the public forum their name coloring and avatar portrait is golden, giving premium players a unique look to other players.

![Premium Chat](../img/docs/premiumplayerpublicchat.png)

### Premium Calendar Synchronisation 
Premium players can synchronizes their calendar with Roll for Group, all the joined and hosted events show automatically. The date, time, location and match description are visible on the calendar, with a public link to the Roll for Group event page.

![Sample Calendar](../img/docs/calendarsync_show.png)

## Purchasing Membership  
A regular account can upgrad from either a users career page, or the membership menu under the user's name. 

![Premium Upgrade](../img/docs/premiumupgrade.png)

## Selecting a Plan  
Different plans are available, based on the number of months. A plan can be purchased for either one, three, twelve, twenty-four or thirty-six months. 

![Premium Plans](../img/docs/premiumplans.png)

Once an account is upgraded, the career page will indicate the player is a premium member.

Prices are set to $5.00 AUD per month of membership, with discounts for buying in lots of twelve months.

![Premium Confirmation](../img/docs/premiumconfirm.png)

The player now has the premium membership for the purchased period and will receive associated premium perks.
