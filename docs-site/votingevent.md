% Suggest and Vote for Games to Play at an Event

When players join an event, they can indicate what games they want to play, and what games they are bringing to an event.

## Indicating What Players Want to Play
To indicate what games a player wants to play, games can be added to the "Want to Play" section. Start typing the name of the game to add, and the system will display games containing the word you're typing.

Additionally the game time, number of players, and recommended number of players will be displayed.

![Want to play](../img/docs/hostevent_wanttoplay.png)

## Voting For Games
Once games are added to the want to play section, attending players can vote to indicate they want to join the game. Each player can vote by pressing on the up arrow next to any game they want top play.

![Vote to play](../img/docs/hostevent_votearrow.png)

For any games a player wants to play, the up arrow will be displayed as an orange. Games that have received the most votes, will be listed first in the game list.

## Bringing a Game
If a game has many votes attached to it, then a player can select the "Bring Game" button to indicate they are bringing the game. The game with its current votes value will then appear in the "Players are bringing games" section, with the person pressed the "Bring Game" button as the owner.

![Bring Game](../img/docs/hostevent_bringgame.png)

## Joining a Game
If a player has indicated they are bringing a game, to assist with knowing which players want to play a particular game together Roll for Group creates virtual seats up to the number of players for every game. Players can join any game, by pressing on the *Join Game* button. Once the number of players joining the event meets the maximum number of allowable players, no more players can join that game.

Using this tool, players can reserve seats to play a particular game that another player is bringing to game night.

![Join a Game](../img/docs/hostevent_jointable.png)
